import 'dart:developer';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/repository/retrofit/Logging.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class AppRepository {
  static Dio dio = Dio(

    BaseOptions(

        baseUrl: AppConstants.base_api_url,
        connectTimeout: Duration(seconds: 10000), //10000
        receiveTimeout: Duration(seconds: 10000),
        contentType: "application/json"),
  )..interceptors.addAll([
    Logging()
  ]);

}
