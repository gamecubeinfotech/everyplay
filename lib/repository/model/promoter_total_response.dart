import 'package:json_annotation/json_annotation.dart';
part 'promoter_total_response.g.dart';

@JsonSerializable()
class PromoterTotalResult{
  var winning;
  var deposit;
  var aff_bal;
  var matches;
  var team_join;


  PromoterTotalResult({
      this.winning, this.deposit, this.aff_bal, this.matches, this.team_join});

  factory PromoterTotalResult.fromJson(Map<String, dynamic> json) => _$PromoterTotalResultFromJson(json);
  Map<String, dynamic> toJson() => _$PromoterTotalResultToJson(this);
}

@JsonSerializable()
class PromoterTotalResponse{
  int? status;
  String? message;
  PromoterTotalResult? result;

  PromoterTotalResponse({this.status, this.message, this.result});

  factory PromoterTotalResponse.fromJson(Map<String, dynamic> json) => _$PromoterTotalResponseFromJson(json);
  Map<String, dynamic> toJson() => _$PromoterTotalResponseToJson(this);

}