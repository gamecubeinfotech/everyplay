
import 'package:json_annotation/json_annotation.dart';
part 'login_request.g.dart';
@JsonSerializable()
class LoginRequest {
  String? email;
  String? password;
  String? deviceId;
  String? fcmToken;
  String? type;
  String? social_id;
  String? name;
  String? image;
  String? idToken;
  String? socialLoginType;
  String? authorizationCode;


  LoginRequest({
      this.email,
      this.password,
      this.deviceId,
      this.fcmToken,
      this.type,
      this.social_id,
      this.name,
      this.image,
      this.idToken,
      this.socialLoginType,
      this.authorizationCode});

  factory LoginRequest.fromJson(Map<String, dynamic> json) => _$LoginRequestFromJson(json);
  Map<String, dynamic> toJson() => _$LoginRequestToJson(this);
}

