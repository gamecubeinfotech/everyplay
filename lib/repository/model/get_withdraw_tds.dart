import 'package:json_annotation/json_annotation.dart';
part 'get_withdraw_tds.g.dart';

@JsonSerializable()
class TdsWithdrawItem{
  var actual_amount;
  var amount;
  var tds_amount;


  TdsWithdrawItem({this.actual_amount, this.amount, this.tds_amount});

  factory TdsWithdrawItem.fromJson(Map<String, dynamic> json) => _$TdsWithdrawItemFromJson(json);
  Map<String, dynamic> toJson() => _$TdsWithdrawItemToJson(this);
}

@JsonSerializable()
class TdsWithdrawResponse{
  int? status;
  TdsWithdrawItem? result;

  TdsWithdrawResponse({this.status, this.result});

  factory TdsWithdrawResponse.fromJson(Map<String, dynamic> json) => _$TdsWithdrawResponseFromJson(json);
  Map<String, dynamic> toJson() => _$TdsWithdrawResponseToJson(this);

}