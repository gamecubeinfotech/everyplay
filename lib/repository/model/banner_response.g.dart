// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'banner_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BannerListItem _$BannerListItemFromJson(Map<String, dynamic> json) =>
    BannerListItem(
      id: json['id'] as int?,
      image: json['image'] as String?,
      title: json['title'] as String?,
      link: json['link'] as String?,
      type: json['type'] as String?,
      offer_code: json['offer_code'] as String?,
      series_id: json['series_id'],
      match_details: json['match_details'] == null
          ? null
          : MatchDetails.fromJson(
              json['match_details'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$BannerListItemToJson(BannerListItem instance) =>
    <String, dynamic>{
      'id': instance.id,
      'image': instance.image,
      'title': instance.title,
      'link': instance.link,
      'type': instance.type,
      'offer_code': instance.offer_code,
      'series_id': instance.series_id,
      'match_details': instance.match_details,
    };

MatchDetails _$MatchDetailsFromJson(Map<String, dynamic> json) => MatchDetails(
      id: json['id'] as int?,
      series: json['series'] as int?,
      match_status_key: json['match_status_key'] as int?,
      joined_count: json['joined_count'] as int?,
      team_count: json['team_count'] as int?,
      winning_total: json['winning_total'],
      lineup: json['lineup'] as int?,
      is_leaderboard: json['is_leaderboard'] as int?,
      is_amount_show: json['is_amount_show'] as int?,
      name: json['name'] as String?,
      short_name: json['short_name'] as String?,
      format: json['format'] as String?,
      seriesname: json['seriesname'] as String?,
      toss: json['toss'] as String?,
      team1display: json['team1display'] as String?,
      team2display: json['team2display'] as String?,
      team1name: json['team1name'] as String?,
      team2name: json['team2name'] as String?,
      matchkey: json['matchkey'] as String?,
      winnerstatus: json['winnerstatus'] as String?,
      launch_status: json['launch_status'] as String?,
      match_status: json['match_status'] as String?,
      sport_key: json['sport_key'] as String?,
      final_status: json['final_status'] as String?,
      url: json['url'] as String?,
      banner_image: json['banner_image'] as String?,
      team1logo: json['team1logo'] as String?,
      team2logo: json['team2logo'] as String?,
      matchopenstatus: json['matchopenstatus'] as String?,
      matchindexing: json['matchindexing'] as String?,
      start_date: json['start_date'] as String?,
      time_start: json['time_start'] as String?,
      highlights: json['highlights'],
      team1_color: json['team1_color'] as String?,
      team2_color: json['team2_color'] as String?,
      amount: json['amount'],
      total_earned: json['total_earned'] as String?,
      giveaway_amount: json['giveaway_amount'] as String?,
      is_visible_total_earned: json['is_visible_total_earned'] as int?,
      is_giveaway_visible: json['is_giveaway_visible'] as int?,
      reversefantasy: json['reversefantasy'] as int?,
      secondinning: json['secondinning'] as int?,
      bowlingfantasy: json['bowlingfantasy'] as int?,
      battingfantasy: json['battingfantasy'] as int?,
      livefantasy: json['livefantasy'] as int?,
      is_fixture: json['is_fixture'] as int?,
      locktime: json['locktime'] == null
          ? null
          : Locktime.fromJson(json['locktime'] as Map<String, dynamic>),
      slotes: (json['slotes'] as List<dynamic>?)
          ?.map((e) => Slots.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MatchDetailsToJson(MatchDetails instance) =>
    <String, dynamic>{
      'id': instance.id,
      'series': instance.series,
      'match_status_key': instance.match_status_key,
      'joined_count': instance.joined_count,
      'team_count': instance.team_count,
      'winning_total': instance.winning_total,
      'lineup': instance.lineup,
      'is_leaderboard': instance.is_leaderboard,
      'is_amount_show': instance.is_amount_show,
      'name': instance.name,
      'short_name': instance.short_name,
      'format': instance.format,
      'seriesname': instance.seriesname,
      'toss': instance.toss,
      'team1display': instance.team1display,
      'team2display': instance.team2display,
      'team1name': instance.team1name,
      'team2name': instance.team2name,
      'matchkey': instance.matchkey,
      'winnerstatus': instance.winnerstatus,
      'launch_status': instance.launch_status,
      'match_status': instance.match_status,
      'sport_key': instance.sport_key,
      'final_status': instance.final_status,
      'url': instance.url,
      'banner_image': instance.banner_image,
      'team1logo': instance.team1logo,
      'team2logo': instance.team2logo,
      'matchopenstatus': instance.matchopenstatus,
      'matchindexing': instance.matchindexing,
      'start_date': instance.start_date,
      'time_start': instance.time_start,
      'highlights': instance.highlights,
      'team1_color': instance.team1_color,
      'team2_color': instance.team2_color,
      'amount': instance.amount,
      'total_earned': instance.total_earned,
      'giveaway_amount': instance.giveaway_amount,
      'is_visible_total_earned': instance.is_visible_total_earned,
      'is_giveaway_visible': instance.is_giveaway_visible,
      'reversefantasy': instance.reversefantasy,
      'secondinning': instance.secondinning,
      'bowlingfantasy': instance.bowlingfantasy,
      'battingfantasy': instance.battingfantasy,
      'livefantasy': instance.livefantasy,
      'is_fixture': instance.is_fixture,
      'locktime': instance.locktime,
      'slotes': instance.slotes,
    };

SportType _$SportTypeFromJson(Map<String, dynamic> json) => SportType(
      json['sport_name'] as String?,
      json['sport_key'] as String?,
    );

Map<String, dynamic> _$SportTypeToJson(SportType instance) => <String, dynamic>{
      'sport_name': instance.sport_name,
      'sport_key': instance.sport_key,
    };

Slots _$SlotsFromJson(Map<String, dynamic> json) => Slots(
      id: json['id'] as int?,
      type: json['type'] as String?,
      min_over: (json['min_over'] as num?)?.toDouble(),
      max_over: (json['max_over'] as num?)?.toDouble(),
      inning: json['inning'] as int?,
    );

Map<String, dynamic> _$SlotsToJson(Slots instance) => <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'min_over': instance.min_over,
      'max_over': instance.max_over,
      'inning': instance.inning,
    };

Locktime _$LocktimeFromJson(Map<String, dynamic> json) => Locktime(
      date: json['date'] as String?,
      timezone: json['timezone'] as String?,
      timezone_type: json['timezone_type'] as int?,
    );

Map<String, dynamic> _$LocktimeToJson(Locktime instance) => <String, dynamic>{
      'date': instance.date,
      'timezone': instance.timezone,
      'timezone_type': instance.timezone_type,
    };

BannerListResponse _$BannerListResponseFromJson(Map<String, dynamic> json) =>
    BannerListResponse(
      status: json['status'] as int?,
      refer_url: json['refer_url'] as String?,
      is_popup_redirect: json['is_popup_redirect'] as int?,
      version: json['version'] as int?,
      is_visible_affiliate: json['is_visible_affiliate'] as int?,
      is_visible_promote: json['is_visible_promote'] as int?,
      is_visible_promoter_leaderboard: json['is_visible_promoter_leaderboard'],
      is_visible_promoter_requested:
          json['is_visible_promoter_requested'] as int?,
      message: json['message'] as String?,
      popup_link: json['popup_link'] as String?,
      version_code: json['version_code'] as int?,
      version_changes: json['version_changes'] as String?,
      app_download_url: json['app_download_url'] as String?,
      base_url: json['base_url'] as String?,
      api_base_url: json['api_base_url'] as String?,
      popup_status: json['popup_status'] as int?,
      popup_image: json['popup_image'] as String?,
      poup_time: json['poup_time'] as int?,
      notification: json['notification'] as int?,
      team: json['team'] as String?,
      state: json['state'] as String?,
      popup_type: json['popup_type'] as String?,
      popup_redirect_type: json['popup_redirect_type'] as String?,
      popup_series_id: json['popup_series_id'],
      popup_offer_code: json['popup_offer_code'],
      match_details: json['match_details'] == null
          ? null
          : MatchDetails.fromJson(
              json['match_details'] as Map<String, dynamic>),
      visible_sports: (json['visible_sports'] as List<dynamic>?)
          ?.map((e) => SportType.fromJson(e as Map<String, dynamic>))
          .toList(),
      result: (json['result'] as List<dynamic>?)
          ?.map((e) => BannerListItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BannerListResponseToJson(BannerListResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'is_popup_redirect': instance.is_popup_redirect,
      'version': instance.version,
      'is_visible_affiliate': instance.is_visible_affiliate,
      'is_visible_promote': instance.is_visible_promote,
      'is_visible_promoter_leaderboard':
          instance.is_visible_promoter_leaderboard,
      'is_visible_promoter_requested': instance.is_visible_promoter_requested,
      'message': instance.message,
      'popup_link': instance.popup_link,
      'version_code': instance.version_code,
      'version_changes': instance.version_changes,
      'app_download_url': instance.app_download_url,
      'refer_url': instance.refer_url,
      'base_url': instance.base_url,
      'api_base_url': instance.api_base_url,
      'popup_status': instance.popup_status,
      'popup_image': instance.popup_image,
      'poup_time': instance.poup_time,
      'notification': instance.notification,
      'team': instance.team,
      'state': instance.state,
      'popup_type': instance.popup_type,
      'popup_redirect_type': instance.popup_redirect_type,
      'popup_series_id': instance.popup_series_id,
      'popup_offer_code': instance.popup_offer_code,
      'match_details': instance.match_details,
      'visible_sports': instance.visible_sports,
      'result': instance.result,
    };
