// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginRequest _$LoginRequestFromJson(Map<String, dynamic> json) => LoginRequest(
      email: json['email'] as String?,
      password: json['password'] as String?,
      deviceId: json['deviceId'] as String?,
      fcmToken: json['fcmToken'] as String?,
      type: json['type'] as String?,
      social_id: json['social_id'] as String?,
      name: json['name'] as String?,
      image: json['image'] as String?,
      idToken: json['idToken'] as String?,
      socialLoginType: json['socialLoginType'] as String?,
      authorizationCode: json['authorizationCode'] as String?,
    );

Map<String, dynamic> _$LoginRequestToJson(LoginRequest instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
      'deviceId': instance.deviceId,
      'fcmToken': instance.fcmToken,
      'type': instance.type,
      'social_id': instance.social_id,
      'name': instance.name,
      'image': instance.image,
      'idToken': instance.idToken,
      'socialLoginType': instance.socialLoginType,
      'authorizationCode': instance.authorizationCode,
    };
