// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_balance_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MyBalanceResultItem _$MyBalanceResultItemFromJson(Map<String, dynamic> json) =>
    MyBalanceResultItem(
      total: json['total'] as String?,
      balance: json['balance'] as String?,
      winning: json['winning'] as String?,
      totalamount: json['totalamount'] as String?,
      expireamount: json['expireamount'] as int?,
      total_match_play: json['total_match_play'] as int?,
      bonus: json['bonus'] as String?,
      total_league_play: json['total_league_play'] as int?,
      total_winning: json['total_winning'] as String?,
      total_contest_win: json['total_contest_win'] as int?,
      email_verify: json['email_verify'] as int?,
      bank_verify: json['bank_verify'] as int?,
      mobile_verify: json['mobile_verify'] as int?,
      pan_verify: json['pan_verify'] as int?,
      level: json['level'] as int?,
    );

Map<String, dynamic> _$MyBalanceResultItemToJson(
        MyBalanceResultItem instance) =>
    <String, dynamic>{
      'total': instance.total,
      'balance': instance.balance,
      'winning': instance.winning,
      'totalamount': instance.totalamount,
      'expireamount': instance.expireamount,
      'total_match_play': instance.total_match_play,
      'bonus': instance.bonus,
      'total_league_play': instance.total_league_play,
      'total_winning': instance.total_winning,
      'total_contest_win': instance.total_contest_win,
      'email_verify': instance.email_verify,
      'bank_verify': instance.bank_verify,
      'mobile_verify': instance.mobile_verify,
      'pan_verify': instance.pan_verify,
      'level': instance.level,
    };

MyBalanceResponse _$MyBalanceResponseFromJson(Map<String, dynamic> json) =>
    MyBalanceResponse(
      status: json['status'] as int?,
      message: json['message'] as String?,
      result: (json['result'] as List<dynamic>?)
          ?.map((e) => MyBalanceResultItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MyBalanceResponseToJson(MyBalanceResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'result': instance.result,
    };
