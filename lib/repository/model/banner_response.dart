import 'package:json_annotation/json_annotation.dart';
part 'banner_response.g.dart';

@JsonSerializable()
class BannerListItem{
  int? id;
  String? image;
  String? title;
  String? link;
  String? type;
  String? offer_code;
  var series_id;
  MatchDetails? match_details;


  BannerListItem({this.id, this.image, this.title, this.link, this.type,
      this.offer_code,this.series_id, this.match_details});

  factory BannerListItem.fromJson(Map<String, dynamic> json) => _$BannerListItemFromJson(json);
  Map<String, dynamic> toJson() => _$BannerListItemToJson(this);
}
@JsonSerializable()
class MatchDetails{
  int? id;
  int? series;
  int? match_status_key;
  int? joined_count;
  int? team_count;
  var winning_total;
  int? lineup;
  int? is_leaderboard;
  int? is_amount_show;
  String? name;
  String? short_name;
  String? format;
  String? seriesname;
  String? toss;
  String? team1display;
  String? team2display;
  String? team1name;
  String? team2name;
  String? matchkey;
  String? winnerstatus;
  String? launch_status;
  String? match_status;
  String? sport_key;
  String? final_status;
  String? url;
  String? banner_image;
  String? team1logo;
  String? team2logo;
  String? matchopenstatus;
  String? matchindexing;
  String? start_date;
  String? time_start;
  var highlights;
  String? team1_color;
  String? team2_color;
  var amount;
  String? total_earned;
  String? giveaway_amount;
  int? is_visible_total_earned;
  int? is_giveaway_visible;
  int? reversefantasy;
  int? secondinning;
  int? bowlingfantasy;
  int? battingfantasy;
  int? livefantasy;
  int? is_fixture;
  Locktime? locktime;
  List<Slots>? slotes;


  MatchDetails({
      this.id,
      this.series,
      this.match_status_key,
      this.joined_count,
      this.team_count,
      this.winning_total,
      this.lineup,
      this.is_leaderboard,
      this.is_amount_show,
      this.name,
      this.short_name,
      this.format,
      this.seriesname,
      this.toss,
      this.team1display,
      this.team2display,
      this.team1name,
      this.team2name,
      this.matchkey,
      this.winnerstatus,
      this.launch_status,
      this.match_status,
      this.sport_key,
      this.final_status,
      this.url,
      this.banner_image,
      this.team1logo,
      this.team2logo,
      this.matchopenstatus,
      this.matchindexing,
      this.start_date,
      this.time_start,
      this.highlights,
      this.team1_color,
      this.team2_color,
      this.amount,
      this.total_earned,
      this.giveaway_amount,
      this.is_visible_total_earned,
      this.is_giveaway_visible,
      this.reversefantasy,
      this.secondinning,
      this.bowlingfantasy,
      this.battingfantasy,
      this.livefantasy,
      this.is_fixture,
      this.locktime,
      this.slotes});

  factory MatchDetails.fromJson(Map<String, dynamic> json) => _$MatchDetailsFromJson(json);
  Map<String, dynamic> toJson() => _$MatchDetailsToJson(this);
}
@JsonSerializable()
class SportType{
  String? sport_name;
  String? sport_key;


  SportType(this.sport_name, this.sport_key);

  factory SportType.fromJson(Map<String, dynamic> json) => _$SportTypeFromJson(json);
  Map<String, dynamic> toJson() => _$SportTypeToJson(this);
}
@JsonSerializable()
class Slots{
  int? id;
  String? type;
  double? min_over;
  double? max_over;
  int? inning;


  Slots({this.id, this.type, this.min_over, this.max_over, this.inning});

  factory Slots.fromJson(Map<String, dynamic> json) => _$SlotsFromJson(json);
  Map<String, dynamic> toJson() => _$SlotsToJson(this);
}
@JsonSerializable()
class Locktime{
  String? date;
  String? timezone;
  int? timezone_type;


  Locktime({this.date, this.timezone, this.timezone_type});

  factory Locktime.fromJson(Map<String, dynamic> json) => _$LocktimeFromJson(json);
  Map<String, dynamic> toJson() => _$LocktimeToJson(this);
}

@JsonSerializable()
class BannerListResponse{
  int? status;
  int? is_popup_redirect;
  int? version;
  int? is_visible_affiliate;
  int? is_visible_promote;
  var is_visible_promoter_leaderboard;
  int? is_visible_promoter_requested;
  String? message;
  String? popup_link;
  int? version_code;
  String? version_changes;
  String? app_download_url;
  String? refer_url;
  String? base_url;
  String? api_base_url;
  int? popup_status;
  String? popup_image;
  int? poup_time;
  int? notification;
  String? team;
  String? state;
  String? popup_type;
  String? popup_redirect_type;
  var popup_series_id;
  var popup_offer_code;
  MatchDetails? match_details;
  List<SportType>? visible_sports;
  List<BannerListItem>? result;


  BannerListResponse({
      this.status,
    this.refer_url,
      this.is_popup_redirect,
      this.version,
      this.is_visible_affiliate,
      this.is_visible_promote,
      this.is_visible_promoter_leaderboard,
      this.is_visible_promoter_requested,
      this.message,
      this.popup_link,
      this.version_code,
      this.version_changes,
      this.app_download_url,
      this.base_url,
      this.api_base_url,
      this.popup_status,
      this.popup_image,
      this.poup_time,
      this.notification,
      this.team,
      this.state,
      this.popup_type,
      this.popup_redirect_type,
      this.popup_series_id,
      this.popup_offer_code,
      this.match_details,
      this.visible_sports,
      this.result});

  factory BannerListResponse.fromJson(Map<String, dynamic> json) => _$BannerListResponseFromJson(json);
  Map<String, dynamic> toJson() => _$BannerListResponseToJson(this);

}