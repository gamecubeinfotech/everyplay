import 'package:json_annotation/json_annotation.dart';
part 'withdraw_amount_response.g.dart';

@JsonSerializable()
class WithdrawItem{
  String? msg;
  double? amount;
  double? wining;
  int? status;


  WithdrawItem({this.msg, this.amount, this.wining, this.status});

  factory WithdrawItem.fromJson(Map<String, dynamic> json) => _$WithdrawItemFromJson(json);
  Map<String, dynamic> toJson() => _$WithdrawItemToJson(this);
}

@JsonSerializable()
class WithdrawResponse{
  int? status;
  String? message;
  WithdrawItem? result;

  WithdrawResponse({this.status, this.message, this.result});

  factory WithdrawResponse.fromJson(Map<String, dynamic> json) => _$WithdrawResponseFromJson(json);
  Map<String, dynamic> toJson() => _$WithdrawResponseToJson(this);

}