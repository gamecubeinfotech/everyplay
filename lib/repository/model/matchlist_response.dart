import 'package:json_annotation/json_annotation.dart';
import 'package:EverPlay/repository/model/banner_response.dart';
part 'matchlist_response.g.dart';

@JsonSerializable()
class MatchListResponse{
  int? status;
  int? total_page;
  int? total_live_match;
  String? message;
  List<MatchDetails>? result;
  List<MatchDetails>? users_matches;


  MatchListResponse({
      this.status,
      this.total_page,
      this.total_live_match,
      this.message,
      this.result,
      this.users_matches}); // LoginResponse({required this.status,required this.message,required this.is_mobile, this.meta, required this.data});
  factory MatchListResponse.fromJson(Map<String, dynamic> json) => _$MatchListResponseFromJson(json);
  Map<String, dynamic> toJson() => _$MatchListResponseToJson(this);

}