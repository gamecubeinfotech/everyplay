// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'promoter_total_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PromoterTotalResult _$PromoterTotalResultFromJson(Map<String, dynamic> json) =>
    PromoterTotalResult(
      winning: json['winning'],
      deposit: json['deposit'],
      aff_bal: json['aff_bal'],
      matches: json['matches'],
      team_join: json['team_join'],
    );

Map<String, dynamic> _$PromoterTotalResultToJson(
        PromoterTotalResult instance) =>
    <String, dynamic>{
      'winning': instance.winning,
      'deposit': instance.deposit,
      'aff_bal': instance.aff_bal,
      'matches': instance.matches,
      'team_join': instance.team_join,
    };

PromoterTotalResponse _$PromoterTotalResponseFromJson(
        Map<String, dynamic> json) =>
    PromoterTotalResponse(
      status: json['status'] as int?,
      message: json['message'] as String?,
      result: json['result'] == null
          ? null
          : PromoterTotalResult.fromJson(
              json['result'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$PromoterTotalResponseToJson(
        PromoterTotalResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'result': instance.result,
    };
