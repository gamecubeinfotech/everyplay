
import 'package:json_annotation/json_annotation.dart';
part 'register_request.g.dart';
@JsonSerializable()
class RegisterRequest {
  String? refer_code;
  String? email;
  String? fcmToken;
  String? deviceId;
  String? password;
  String? mobile;
  String? dob;


  RegisterRequest(this.refer_code, this.email, this.fcmToken, this.deviceId,
      this.password, this.mobile, this.dob);

  factory RegisterRequest.fromJson(Map<String, dynamic> json) => _$RegisterRequestFromJson(json);
  Map<String, dynamic> toJson() => _$RegisterRequestToJson(this);
}

