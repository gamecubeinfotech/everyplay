// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GeneralRequest _$GeneralRequestFromJson(Map<String, dynamic> json) =>
    GeneralRequest(
      user_id: json['user_id'] as String?,
      mobile: json['mobile'] as String?,
      type: json['type'] as String?,
      file: json['file'] as String?,
      amount: json['amount'] as String?,
      email: json['email'] as String?,
      email_or_mobile: json['email_or_mobile'] as String?,
      matchkey: json['matchkey'] as String?,
      promo: json['promo'] as String?,
      payment_type: json['payment_type'] as String?,
      ip: json['ip'] as String?,
      challenge_id: json['challenge_id'] as String?,
      sport_key: json['sport_key'] as String?,
      team1_id: json['team1_id'] as String?,
      team2_id: json['team2_id'] as String?,
      series_id: json['series_id'] as String?,
      verification_type: json['verification_type'] as String?,
      start_date: json['start_date'] as String?,
      end_date: json['end_date'] as String?,
      page: json['page'] as String?,
      my_fav_contest: json['my_fav_contest'] as String?,
      fantasy_type: json['fantasy_type'] as String?,
      slotes_id: json['slotes_id'] as String?,
      promoter_leaderboard_id: json['promoter_leaderboard_id'] as String?,
      fcmToken: json['fcmToken'] as String?,
      search_text: json['search_text'] as String?,
      oldpassword: json['oldpassword'] as String?,
      newpassword: json['newpassword'] as String?,
      otp: json['otp'] as String?,
      playerid: json['playerid'] as String?,
    );

Map<String, dynamic> _$GeneralRequestToJson(GeneralRequest instance) =>
    <String, dynamic>{
      'user_id': instance.user_id,
      'mobile': instance.mobile,
      'type': instance.type,
      'file': instance.file,
      'amount': instance.amount,
      'email': instance.email,
      'email_or_mobile': instance.email_or_mobile,
      'matchkey': instance.matchkey,
      'promo': instance.promo,
      'payment_type': instance.payment_type,
      'ip': instance.ip,
      'challenge_id': instance.challenge_id,
      'sport_key': instance.sport_key,
      'team1_id': instance.team1_id,
      'team2_id': instance.team2_id,
      'series_id': instance.series_id,
      'verification_type': instance.verification_type,
      'start_date': instance.start_date,
      'end_date': instance.end_date,
      'page': instance.page,
      'my_fav_contest': instance.my_fav_contest,
      'fantasy_type': instance.fantasy_type,
      'slotes_id': instance.slotes_id,
      'promoter_leaderboard_id': instance.promoter_leaderboard_id,
      'fcmToken': instance.fcmToken,
      'search_text': instance.search_text,
      'oldpassword': instance.oldpassword,
      'newpassword': instance.newpassword,
      'otp': instance.otp,
      'playerid': instance.playerid,
    };
