// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_withdraw_tds.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TdsWithdrawItem _$TdsWithdrawItemFromJson(Map<String, dynamic> json) =>
    TdsWithdrawItem(
      actual_amount: json['actual_amount'],
      amount: json['amount'],
      tds_amount: json['tds_amount'],
    );

Map<String, dynamic> _$TdsWithdrawItemToJson(TdsWithdrawItem instance) =>
    <String, dynamic>{
      'actual_amount': instance.actual_amount,
      'amount': instance.amount,
      'tds_amount': instance.tds_amount,
    };

TdsWithdrawResponse _$TdsWithdrawResponseFromJson(Map<String, dynamic> json) =>
    TdsWithdrawResponse(
      status: json['status'] as int?,
      result: json['result'] == null
          ? null
          : TdsWithdrawItem.fromJson(json['result'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$TdsWithdrawResponseToJson(
        TdsWithdrawResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'result': instance.result,
    };
