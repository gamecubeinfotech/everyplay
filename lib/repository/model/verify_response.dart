import 'package:json_annotation/json_annotation.dart';
part 'verify_response.g.dart';

@JsonSerializable()
class AllVerifyItem{
  int? email_verify;
  int? pan_verify;
  int? bank_verify;
  int? mobile_verify;


  AllVerifyItem({
      this.email_verify, this.pan_verify, this.bank_verify, this.mobile_verify});

  factory AllVerifyItem.fromJson(Map<String, dynamic> json) => _$AllVerifyItemFromJson(json);
  Map<String, dynamic> toJson() => _$AllVerifyItemToJson(this);
}

@JsonSerializable()
class AllVerifyResponse{
  int? status;
  String? message;
  AllVerifyItem? result;

  AllVerifyResponse({this.status, this.message, this.result});

  factory AllVerifyResponse.fromJson(Map<String, dynamic> json) => _$AllVerifyResponseFromJson(json);
  Map<String, dynamic> toJson() => _$AllVerifyResponseToJson(this);

}