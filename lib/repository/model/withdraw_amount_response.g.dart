// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'withdraw_amount_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WithdrawItem _$WithdrawItemFromJson(Map<String, dynamic> json) => WithdrawItem(
      msg: json['msg'] as String?,
      amount: (json['amount'] as num?)?.toDouble(),
      wining: (json['wining'] as num?)?.toDouble(),
      status: json['status'] as int?,
    );

Map<String, dynamic> _$WithdrawItemToJson(WithdrawItem instance) =>
    <String, dynamic>{
      'msg': instance.msg,
      'amount': instance.amount,
      'wining': instance.wining,
      'status': instance.status,
    };

WithdrawResponse _$WithdrawResponseFromJson(Map<String, dynamic> json) =>
    WithdrawResponse(
      status: json['status'] as int?,
      message: json['message'] as String?,
      result: json['result'] == null
          ? null
          : WithdrawItem.fromJson(json['result'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$WithdrawResponseToJson(WithdrawResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'result': instance.result,
    };
