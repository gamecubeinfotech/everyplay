// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'category_contest_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Contest _$ContestFromJson(Map<String, dynamic> json) => Contest(
      entryfee: json['entryfee'],
      champion_player: json['champion_player'] as String?,
      champion_x: json['champion_x'] as String?,
      is_champion: json['is_champion'] as int?,
      is_offer_team: json['is_offer_team'] as int?,
      is_gadget: json['is_gadget'] as int?,
      gadget_image: json['gadget_image'] as String?,
      first_rank_prize: json['first_rank_prize'],
      getjoinedpercentage: json['getjoinedpercentage'],
      dis_price: json['dis_price'] as String?,
      challenge_type: json['challenge_type'] as String?,
      matchkey: json['matchkey'] as String?,
      isselectedid: json['isselectedid'] as String?,
      refercode: json['refercode'] as String?,
      name: json['name'] as String?,
      bonus_percent: json['bonus_percent'] as String?,
      pdf: json['pdf'] as String?,
      announcement: json['announcement'] as String?,
      first_time_free_amount: json['first_time_free_amount'] as int?,
      winning_percentage: json['winning_percentage'] as int?,
      totalwinners: json['totalwinners'],
      joinedusers: json['joinedusers'] as int?,
      is_free: json['is_free'] as int?,
      multi_entry: json['multi_entry'] as int?,
      is_running: json['is_running'] as int?,
      confirmed_challenge: json['confirmed_challenge'] as int?,
      is_bonus: json['is_bonus'] as int?,
      id: json['id'] as int?,
      win_amount: json['win_amount'],
      maximum_user: json['maximum_user'] as int?,
      status: json['status'] as int?,
      max_multi_entry_user: json['max_multi_entry_user'] as int?,
      is_flexible: json['is_flexible'] as int?,
      is_fav_contest: json['is_fav_contest'],
      is_fav_visible: json['is_fav_visible'] as int?,
      real_challenge_id: json['real_challenge_id'] as int?,
      is_first_time_free: json['is_first_time_free'] as int?,
      isjoined: json['isjoined'] as bool?,
      isselected: json['isselected'] as bool?,
      is_giveaway_text: json['is_giveaway_text'] as String?,
      giveaway_color: json['giveaway_color'] as String?,
      is_giveaway_visible_text: json['is_giveaway_visible_text'] as int?,
    );

Map<String, dynamic> _$ContestToJson(Contest instance) => <String, dynamic>{
      'entryfee': instance.entryfee,
      'is_offer_team': instance.is_offer_team,
      'is_gadget': instance.is_gadget,
      'is_champion': instance.is_champion,
      'champion_player': instance.champion_player,
      'champion_x': instance.champion_x,
      'gadget_image': instance.gadget_image,
      'first_rank_prize': instance.first_rank_prize,
      'getjoinedpercentage': instance.getjoinedpercentage,
      'dis_price': instance.dis_price,
      'challenge_type': instance.challenge_type,
      'matchkey': instance.matchkey,
      'isselectedid': instance.isselectedid,
      'refercode': instance.refercode,
      'name': instance.name,
      'bonus_percent': instance.bonus_percent,
      'pdf': instance.pdf,
      'announcement': instance.announcement,
      'first_time_free_amount': instance.first_time_free_amount,
      'winning_percentage': instance.winning_percentage,
      'totalwinners': instance.totalwinners,
      'joinedusers': instance.joinedusers,
      'is_free': instance.is_free,
      'multi_entry': instance.multi_entry,
      'is_running': instance.is_running,
      'confirmed_challenge': instance.confirmed_challenge,
      'is_bonus': instance.is_bonus,
      'id': instance.id,
      'win_amount': instance.win_amount,
      'maximum_user': instance.maximum_user,
      'status': instance.status,
      'max_multi_entry_user': instance.max_multi_entry_user,
      'is_flexible': instance.is_flexible,
      'is_fav_contest': instance.is_fav_contest,
      'is_fav_visible': instance.is_fav_visible,
      'real_challenge_id': instance.real_challenge_id,
      'is_first_time_free': instance.is_first_time_free,
      'isjoined': instance.isjoined,
      'isselected': instance.isselected,
      'is_giveaway_text': instance.is_giveaway_text,
      'giveaway_color': instance.giveaway_color,
      'is_giveaway_visible_text': instance.is_giveaway_visible_text,
    };

CategoriesItem _$CategoriesItemFromJson(Map<String, dynamic> json) =>
    CategoriesItem(
      contest_sub_text: json['contest_sub_text'] as String?,
      contest_type_image: json['contest_type_image'] as String?,
      contest_image_url: json['contest_image_url'] as String?,
      name: json['name'] as String?,
      id: json['id'] as int?,
      total_category_leagues: json['total_category_leagues'] as int?,
      sort_order: json['sort_order'] as int?,
      status: json['status'] as int?,
      is_view_more: json['is_view_more'] as int?,
      leagues: (json['leagues'] as List<dynamic>?)
          ?.map((e) => Contest.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CategoriesItemToJson(CategoriesItem instance) =>
    <String, dynamic>{
      'contest_sub_text': instance.contest_sub_text,
      'contest_type_image': instance.contest_type_image,
      'contest_image_url': instance.contest_image_url,
      'name': instance.name,
      'id': instance.id,
      'total_category_leagues': instance.total_category_leagues,
      'sort_order': instance.sort_order,
      'status': instance.status,
      'is_view_more': instance.is_view_more,
      'leagues': instance.leagues,
    };

CategoryModel _$CategoryModelFromJson(Map<String, dynamic> json) =>
    CategoryModel(
      total_contest: json['total_contest'] as int?,
      user_teams: json['user_teams'] as int?,
      team_id: json['team_id'] as int?,
      joined_leagues: json['joined_leagues'] as int?,
      categories: (json['categories'] as List<dynamic>?)
          ?.map((e) => CategoriesItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$CategoryModelToJson(CategoryModel instance) =>
    <String, dynamic>{
      'total_contest': instance.total_contest,
      'user_teams': instance.user_teams,
      'team_id': instance.team_id,
      'joined_leagues': instance.joined_leagues,
      'categories': instance.categories,
    };

CategoryByContestResponse _$CategoryByContestResponseFromJson(
        Map<String, dynamic> json) =>
    CategoryByContestResponse(
      status: json['status'] as int?,
      message: json['message'] as String?,
      result: json['result'] == null
          ? null
          : CategoryModel.fromJson(json['result'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$CategoryByContestResponseToJson(
        CategoryByContestResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'result': instance.result,
    };
