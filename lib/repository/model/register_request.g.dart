// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RegisterRequest _$RegisterRequestFromJson(Map<String, dynamic> json) =>
    RegisterRequest(
      json['refer_code'] as String?,
      json['email'] as String?,
      json['fcmToken'] as String?,
      json['deviceId'] as String?,
      json['password'] as String?,
      json['mobile'] as String?,
      json['dob'] as String?,
    );

Map<String, dynamic> _$RegisterRequestToJson(RegisterRequest instance) =>
    <String, dynamic>{
      'refer_code': instance.refer_code,
      'email': instance.email,
      'fcmToken': instance.fcmToken,
      'deviceId': instance.deviceId,
      'password': instance.password,
      'mobile': instance.mobile,
      'dob': instance.dob,
    };
