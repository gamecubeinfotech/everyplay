// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_phonepe_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PhonePeResponse _$PhonePeResponseFromJson(Map<String, dynamic> json) =>
    PhonePeResponse(
      status: json['status'],
      message: json['message'] as String?,
      url: json['url'] as String?,
    );

Map<String, dynamic> _$PhonePeResponseToJson(PhonePeResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'url': instance.url,
    };
