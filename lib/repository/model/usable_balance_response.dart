import 'package:json_annotation/json_annotation.dart';
part 'usable_balance_response.g.dart';

@JsonSerializable()
class UsableBalanceItem{
  var usablebalance;
  var usertotalbalance;
  double? marathon;
  int? is_bonus;


  UsableBalanceItem({
      this.usablebalance, this.usertotalbalance, this.marathon, this.is_bonus});

  factory UsableBalanceItem.fromJson(Map<String, dynamic> json) => _$UsableBalanceItemFromJson(json);
  Map<String, dynamic> toJson() => _$UsableBalanceItemToJson(this);
}

@JsonSerializable()
class BalanceResponse{
  int? status;
  String? message;
  List<UsableBalanceItem>? result;

  BalanceResponse({this.status, this.message, this.result});

  factory BalanceResponse.fromJson(Map<String, dynamic> json) => _$BalanceResponseFromJson(json);
  Map<String, dynamic> toJson() => _$BalanceResponseToJson(this);

}