// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verify_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AllVerifyItem _$AllVerifyItemFromJson(Map<String, dynamic> json) =>
    AllVerifyItem(
      email_verify: json['email_verify'] as int?,
      pan_verify: json['pan_verify'] as int?,
      bank_verify: json['bank_verify'] as int?,
      mobile_verify: json['mobile_verify'] as int?,
    );

Map<String, dynamic> _$AllVerifyItemToJson(AllVerifyItem instance) =>
    <String, dynamic>{
      'email_verify': instance.email_verify,
      'pan_verify': instance.pan_verify,
      'bank_verify': instance.bank_verify,
      'mobile_verify': instance.mobile_verify,
    };

AllVerifyResponse _$AllVerifyResponseFromJson(Map<String, dynamic> json) =>
    AllVerifyResponse(
      status: json['status'] as int?,
      message: json['message'] as String?,
      result: json['result'] == null
          ? null
          : AllVerifyItem.fromJson(json['result'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$AllVerifyResponseToJson(AllVerifyResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'result': instance.result,
    };
