// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'usable_balance_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UsableBalanceItem _$UsableBalanceItemFromJson(Map<String, dynamic> json) =>
    UsableBalanceItem(
      usablebalance: json['usablebalance'],
      usertotalbalance: json['usertotalbalance'],
      marathon: (json['marathon'] as num?)?.toDouble(),
      is_bonus: json['is_bonus'] as int?,
    );

Map<String, dynamic> _$UsableBalanceItemToJson(UsableBalanceItem instance) =>
    <String, dynamic>{
      'usablebalance': instance.usablebalance,
      'usertotalbalance': instance.usertotalbalance,
      'marathon': instance.marathon,
      'is_bonus': instance.is_bonus,
    };

BalanceResponse _$BalanceResponseFromJson(Map<String, dynamic> json) =>
    BalanceResponse(
      status: json['status'] as int?,
      message: json['message'] as String?,
      result: (json['result'] as List<dynamic>?)
          ?.map((e) => UsableBalanceItem.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$BalanceResponseToJson(BalanceResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'result': instance.result,
    };
