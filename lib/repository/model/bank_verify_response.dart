import 'package:json_annotation/json_annotation.dart';
part 'bank_verify_response.g.dart';

@JsonSerializable()
class BankVerifyResponseItem{
  String? msg;
  int? status;

  BankVerifyResponseItem(this.msg, this.status);

  factory BankVerifyResponseItem.fromJson(Map<String, dynamic> json) => _$BankVerifyResponseItemFromJson(json);
  Map<String, dynamic> toJson() => _$BankVerifyResponseItemToJson(this);
}

@JsonSerializable()
class BankVerifyResponse{
  int? status;
  String? message;
  BankVerifyResponseItem? result;

  BankVerifyResponse({this.status, this.message, this.result});

  factory BankVerifyResponse.fromJson(Map<String, dynamic> json) => _$BankVerifyResponseFromJson(json);
  Map<String, dynamic> toJson() => _$BankVerifyResponseToJson(this);

}