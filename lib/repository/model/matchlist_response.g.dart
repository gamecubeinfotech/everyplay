// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'matchlist_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MatchListResponse _$MatchListResponseFromJson(Map<String, dynamic> json) =>
    MatchListResponse(
      status: json['status'] as int?,
      total_page: json['total_page'] as int?,
      total_live_match: json['total_live_match'] as int?,
      message: json['message'] as String?,
      result: (json['result'] as List<dynamic>?)
          ?.map((e) => MatchDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
      users_matches: (json['users_matches'] as List<dynamic>?)
          ?.map((e) => MatchDetails.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MatchListResponseToJson(MatchListResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'total_page': instance.total_page,
      'total_live_match': instance.total_live_match,
      'message': instance.message,
      'result': instance.result,
      'users_matches': instance.users_matches,
    };
