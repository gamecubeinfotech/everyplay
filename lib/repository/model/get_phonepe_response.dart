import 'package:json_annotation/json_annotation.dart';
part 'get_phonepe_response.g.dart';

@JsonSerializable()
class PhonePeResponse{
  var status;
  String? message;
  String? url;


  PhonePeResponse({this.status, this.message,  this.url});
  factory PhonePeResponse.fromJson(Map<String, dynamic> json) => _$PhonePeResponseFromJson(json);
  Map<String, dynamic> toJson() => _$PhonePeResponseToJson(this);

}