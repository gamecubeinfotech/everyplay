// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'refresh_score_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LiveFinishedContestData _$LiveFinishedContestDataFromJson(
        Map<String, dynamic> json) =>
    LiveFinishedContestData(
      winingamount: json['winingamount'] as int?,
      champion_x: json['champion_x'] as String?,
      champion_player: json['champion_player'] as String?,
      is_champion: json['is_champion'] as int?,
      gadget_image: json['gadget_image'] as String?,
      is_gadget: json['is_gadget'] as int?,
      is_private: json['is_private'] as int?,
      totalwinners: json['totalwinners'],
      id: json['id'] as int?,
      minimum_user: json['minimum_user'] as int?,
      multi_entry: json['multi_entry'] as int?,
      confirmed: json['confirmed'] as int?,
      teamid: json['teamid'] as int?,
      userrank: json['userrank'] as int?,
      maximum_user: json['maximum_user'] as int?,
      can_invite: json['can_invite'] as int?,
      entryfee: json['entryfee'],
      joinedusers: json['joinedusers'] as int?,
      joinid: json['joinid'] as int?,
      grand: json['grand'] as int?,
      team_number_get: json['team_number_get'] as int?,
      win_amount: json['win_amount'] as int?,
      pdf_created: json['pdf_created'] as int?,
      status: json['status'] as int?,
      confirmed_challenge: json['confirmed_challenge'] as int?,
      is_bonus: json['is_bonus'] as int?,
      max_multi_entry_user: json['max_multi_entry_user'] as int?,
      refercode: json['refercode'] as String?,
      first_rank_prize: json['first_rank_prize'],
      points: json['points'],
      matchstatus: json['matchstatus'] as String?,
      challenge_type: json['challenge_type'] as String?,
      winning_percentage: json['winning_percentage'],
      pdf: json['pdf'] as String?,
      join_with: json['join_with'] as String?,
      name: json['name'] as String?,
      winning_amount: json['winning_amount'],
      getjoinedpercentage: json['getjoinedpercentage'],
      bonus_percent: json['bonus_percent'] as String?,
      challenge_status: json['challenge_status'] as String?,
      challenge_text: json['challenge_text'] as String?,
      is_giveaway_text: json['is_giveaway_text'] as String?,
      giveaway_color: json['giveaway_color'] as String?,
      is_giveaway_visible_text: json['is_giveaway_visible_text'] as int?,
      recycler_item_price_card:
          (json['recycler_item_price_card'] as List<dynamic>?)
              ?.map((e) => PriceCardItem.fromJson(e as Map<String, dynamic>))
              .toList(),
      winners_zone: (json['winners_zone'] as List<dynamic>?)
          ?.map((e) => WinnersZoneItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      is_flexible: json['is_flexible'] as int?,
    );

Map<String, dynamic> _$LiveFinishedContestDataToJson(
        LiveFinishedContestData instance) =>
    <String, dynamic>{
      'winingamount': instance.winingamount,
      'is_champion': instance.is_champion,
      'champion_player': instance.champion_player,
      'champion_x': instance.champion_x,
      'is_gadget': instance.is_gadget,
      'gadget_image': instance.gadget_image,
      'is_private': instance.is_private,
      'totalwinners': instance.totalwinners,
      'id': instance.id,
      'minimum_user': instance.minimum_user,
      'multi_entry': instance.multi_entry,
      'confirmed': instance.confirmed,
      'teamid': instance.teamid,
      'userrank': instance.userrank,
      'maximum_user': instance.maximum_user,
      'can_invite': instance.can_invite,
      'entryfee': instance.entryfee,
      'joinedusers': instance.joinedusers,
      'joinid': instance.joinid,
      'grand': instance.grand,
      'team_number_get': instance.team_number_get,
      'win_amount': instance.win_amount,
      'pdf_created': instance.pdf_created,
      'status': instance.status,
      'confirmed_challenge': instance.confirmed_challenge,
      'is_bonus': instance.is_bonus,
      'max_multi_entry_user': instance.max_multi_entry_user,
      'refercode': instance.refercode,
      'first_rank_prize': instance.first_rank_prize,
      'points': instance.points,
      'matchstatus': instance.matchstatus,
      'challenge_type': instance.challenge_type,
      'winning_percentage': instance.winning_percentage,
      'pdf': instance.pdf,
      'join_with': instance.join_with,
      'name': instance.name,
      'winning_amount': instance.winning_amount,
      'getjoinedpercentage': instance.getjoinedpercentage,
      'bonus_percent': instance.bonus_percent,
      'challenge_status': instance.challenge_status,
      'challenge_text': instance.challenge_text,
      'is_giveaway_text': instance.is_giveaway_text,
      'giveaway_color': instance.giveaway_color,
      'is_giveaway_visible_text': instance.is_giveaway_visible_text,
      'recycler_item_price_card': instance.recycler_item_price_card,
      'winners_zone': instance.winners_zone,
      'is_flexible': instance.is_flexible,
    };

WinnersZoneItem _$WinnersZoneItemFromJson(Map<String, dynamic> json) =>
    WinnersZoneItem(
      rank: json['rank'] as int?,
      is_winningzone: json['is_winningzone'] as int?,
      team_id: json['team_id'] as int?,
      team_number: json['team_number'],
      team_name: json['team_name'] as String?,
      points: json['points'] as String?,
      arrowname: json['arrowname'] as String?,
      amount: json['amount'] as String?,
    );

Map<String, dynamic> _$WinnersZoneItemToJson(WinnersZoneItem instance) =>
    <String, dynamic>{
      'rank': instance.rank,
      'is_winningzone': instance.is_winningzone,
      'team_id': instance.team_id,
      'team_number': instance.team_number,
      'team_name': instance.team_name,
      'points': instance.points,
      'arrowname': instance.arrowname,
      'amount': instance.amount,
    };

PriceCardItem _$PriceCardItemFromJson(Map<String, dynamic> json) =>
    PriceCardItem(
      start_position: json['start_position'] as int?,
      price: json['price'] as int?,
      id: json['id'] as int?,
    );

Map<String, dynamic> _$PriceCardItemToJson(PriceCardItem instance) =>
    <String, dynamic>{
      'start_position': instance.start_position,
      'price': instance.price,
      'id': instance.id,
    };

LiveFinishedScoreItem _$LiveFinishedScoreItemFromJson(
        Map<String, dynamic> json) =>
    LiveFinishedScoreItem(
      Team1: json['Team1'] as String?,
      Team2: json['Team2'] as String?,
      Team1_Totalovers: json['Team1_Totalovers'] as String?,
      Team1_Totalruns: json['Team1_Totalruns'] as String?,
      Team1_Totalwickets: json['Team1_Totalwickets'] as String?,
      Team2_Totalwickets: json['Team2_Totalwickets'] as String?,
      Team2_Totalovers: json['Team2_Totalovers'] as String?,
      Team2_Totalruns: json['Team2_Totalruns'] as String?,
      Winning_Status: json['Winning_Status'] as String?,
    );

Map<String, dynamic> _$LiveFinishedScoreItemToJson(
        LiveFinishedScoreItem instance) =>
    <String, dynamic>{
      'Team1': instance.Team1,
      'Team2': instance.Team2,
      'Team1_Totalovers': instance.Team1_Totalovers,
      'Team1_Totalruns': instance.Team1_Totalruns,
      'Team1_Totalwickets': instance.Team1_Totalwickets,
      'Team2_Totalwickets': instance.Team2_Totalwickets,
      'Team2_Totalovers': instance.Team2_Totalovers,
      'Team2_Totalruns': instance.Team2_Totalruns,
      'Winning_Status': instance.Winning_Status,
    };

RefreshScoreItem _$RefreshScoreItemFromJson(Map<String, dynamic> json) =>
    RefreshScoreItem(
      late_declared_text: json['late_declared_text'] as String?,
      is_late_declared: json['is_late_declared'] as int?,
      user_teams: json['user_teams'] as int?,
      total_profit: json['total_profit'],
      total_winning: json['total_winning'],
      total_investment: json['total_investment'],
      matchruns: (json['matchruns'] as List<dynamic>?)
          ?.map(
              (e) => LiveFinishedScoreItem.fromJson(e as Map<String, dynamic>))
          .toList(),
      contest: (json['contest'] as List<dynamic>?)
          ?.map((e) =>
              LiveFinishedContestData.fromJson(e as Map<String, dynamic>))
          .toList(),
      teams: (json['teams'] as List<dynamic>?)
          ?.map((e) => Team.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$RefreshScoreItemToJson(RefreshScoreItem instance) =>
    <String, dynamic>{
      'late_declared_text': instance.late_declared_text,
      'is_late_declared': instance.is_late_declared,
      'user_teams': instance.user_teams,
      'total_profit': instance.total_profit,
      'total_winning': instance.total_winning,
      'total_investment': instance.total_investment,
      'matchruns': instance.matchruns,
      'contest': instance.contest,
      'teams': instance.teams,
    };

RefreshScoreResponse _$RefreshScoreResponseFromJson(
        Map<String, dynamic> json) =>
    RefreshScoreResponse(
      status: json['status'] as int?,
      message: json['message'] as String?,
      result: json['result'] == null
          ? null
          : RefreshScoreItem.fromJson(json['result'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RefreshScoreResponseToJson(
        RefreshScoreResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'result': instance.result,
    };
