
import 'package:json_annotation/json_annotation.dart';
part 'base_request.g.dart';
@JsonSerializable()

class GeneralRequest {
  late String? user_id;
  late String? mobile;
  late String? type;
  late String? file;
  late String? amount;
  late String? email;
  late String? email_or_mobile;
  late String? matchkey;
  late String? promo;
  late String? payment_type;
  late String? ip;
  late String? challenge_id;
  late String? sport_key;
  late String? team1_id;
  late String? team2_id;
  late String? series_id;
  late String? verification_type;
  late String? start_date;
  late String? end_date;
  late String? page;
  late String? my_fav_contest;
  late String? fantasy_type;
  late String? slotes_id;
  late String? promoter_leaderboard_id;
  late String? fcmToken;
  late String? search_text;
  late String? oldpassword;
  late String? newpassword;
  late String? otp;
  late String? playerid;


  GeneralRequest({
      this.user_id,
      this.mobile,
      this.type,
      this.file,
      this.amount,
      this.email,
      this.email_or_mobile,
      this.matchkey,
      this.promo,
      this.payment_type,
      this.ip,
      this.challenge_id,
      this.sport_key,
      this.team1_id,
      this.team2_id,
      this.series_id,
      this.verification_type,
      this.start_date,
      this.end_date,
      this.page,
      this.my_fav_contest,
      this.fantasy_type,
      this.slotes_id,
      this.promoter_leaderboard_id,
      this.fcmToken,
      this.search_text,
      this.oldpassword,
      this.newpassword,
      this.otp,
      this.playerid,
  });


  Map<String, dynamic> toJson() => _$GeneralRequestToJson(this);
}

