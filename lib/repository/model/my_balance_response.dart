import 'package:json_annotation/json_annotation.dart';
part 'my_balance_response.g.dart';

@JsonSerializable()
class MyBalanceResultItem{
  String? total;
  String? balance;
  String? winning;
  String? totalamount;
  int? expireamount;
  int? total_match_play;
  String? bonus;
  int? total_league_play;
  String? total_winning;
  int? total_contest_win;
  int? email_verify;
  int? bank_verify;
  int? mobile_verify;
  int? pan_verify;
  int? level;


  MyBalanceResultItem({
      this.total,
      this.balance,
      this.winning,
      this.totalamount,
      this.expireamount,
      this.total_match_play,
      this.bonus,
      this.total_league_play,
      this.total_winning,
      this.total_contest_win,
      this.email_verify,
      this.bank_verify,
      this.mobile_verify,
      this.pan_verify,
      this.level});

  factory MyBalanceResultItem.fromJson(Map<String, dynamic> json) => _$MyBalanceResultItemFromJson(json);
  Map<String, dynamic> toJson() => _$MyBalanceResultItemToJson(this);
}

@JsonSerializable()
class MyBalanceResponse{
  int? status;
  String? message;
  List<MyBalanceResultItem>? result;

  MyBalanceResponse({this.status, this.message, this.result});

  factory MyBalanceResponse.fromJson(Map<String, dynamic> json) => _$MyBalanceResponseFromJson(json);
  Map<String, dynamic> toJson() => _$MyBalanceResponseToJson(this);

}