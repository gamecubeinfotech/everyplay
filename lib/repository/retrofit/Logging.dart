import 'dart:developer';
import 'dart:io';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/views/Login.dart';
import 'package:EverPlay/views/LoginNew.dart';

class Logging extends Interceptor {
  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async{
    options.headers['Authorization'] = AppConstants.token;
    options.headers['Devicetype'] = Platform.isIOS?'IOS':Platform.isAndroid?'ANDROID':'WEB';
    options.headers['Versioncode'] = AppConstants.versionCode;
    options.headers['Versioncode'] = '10';
    debugPrint('REQUEST[${options.data}] => PATH: ${options.baseUrl}${options.path}=> HEADERS: ${options.headers}');
    return super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async{
    debugPrint(
      'RESPONSE[${response.data}] => PATH: ${response.requestOptions.path}',
    );
    return super.onResponse(response, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) async{
    if(err.response?.statusCode==400){
      AppPrefrence.clearPrefrence();
      Navigator.pushAndRemoveUntil(
          AppConstants.context,
          MaterialPageRoute(
              builder: (context) => new LoginNew()
          ),
          ModalRoute.withName("/main")
      );
    }
    debugPrint(
      'ERROR[${err.response?.statusCode}] => PATH: ${err.response?.statusMessage}',
    );
    return super.onError(err, handler);
  }
}