import 'dart:async';
import 'dart:math';

import 'package:EverPlay/views/Home.dart';
import 'package:EverPlay/views/More.dart';
import 'package:EverPlay/views/ReferList.dart';
import 'package:EverPlay/views/UserProfile.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'appUtilities/app_colors.dart';
import 'appUtilities/app_constants.dart';
import 'appUtilities/app_images.dart';
import 'appUtilities/app_navigator.dart';
import 'firebase_options.dart';
import 'localStoage/AppPrefrences.dart';
import 'package:package_info/package_info.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  await Firebase.initializeApp(options: DefaultFirebaseOptions.currentPlatform);
  registerNotification();
  _initPackageInfo();
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  var isLoggedIn = (prefs.getBool(AppConstants.SHARED_PREFERENCES_IS_LOGGED_IN) == null) ? false : prefs.getBool(AppConstants.SHARED_PREFERENCES_IS_LOGGED_IN);
  runApp( 
      MaterialApp(
        highContrastDarkTheme: ThemeData(
            brightness: Brightness.light
        ),
        highContrastTheme: ThemeData(
            brightness: Brightness.light
        ),
        theme: ThemeData(
          brightness: Brightness.light,
        ),
        themeMode: ThemeMode.light,
        darkTheme: ThemeData.light(),
        debugShowCheckedModeBanner: false,
        color: primaryColor,
        home: !isLoggedIn!?MyApp():HomePage(), //MyApp():HomePage(),
      ));


}
Future<void> _initPackageInfo() async {
  final PackageInfo info = await PackageInfo.fromPlatform();
  AppConstants.versionCode=info.buildNumber;
}
void registerNotification() async {
  AwesomeNotifications().initialize(
      'resource://drawable/ic_notifcation',
      [
        NotificationChannel(
            icon: 'resource://drawable/ic_notifcation',
            channelKey: 'basic_channel',
            channelName: 'Basic notifications',
            channelDescription: 'Notification channel',
            defaultColor: primaryColor,
            ledColor: Colors.white
        )
      ]
  );
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  // 2. Instantiate Firebase Messaging
  FirebaseMessaging _messaging = FirebaseMessaging.instance;

  // 3. On iOS, this helps to take the user permissions
  NotificationSettings settings = await _messaging.requestPermission(
    alert: true,
    badge: true,
    provisional: false,
    sound: true,
  );

  if (settings.authorizationStatus == AuthorizationStatus.authorized) {
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      showNotification(message);
    });

  } else {
    print('User declined or has not accepted permission');
  }
}
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
  //firebase push notification
  showNotification(message);
}
void showNotification(RemoteMessage message){
  String titleTxt='';
  String messageTxt='';
  String image='';
  if(message.notification!=null){
    titleTxt=message.notification!.title!;
    messageTxt=message.notification!.body!;
  }else{
    if(message.data.length>0){
      titleTxt=message.data['title'];
      messageTxt=message.data['message'];
      image=message.data['image'];
    }
  }
  if(titleTxt.isNotEmpty&&messageTxt.isNotEmpty){
    AwesomeNotifications().createNotification(content: NotificationContent(
      id: new Random().nextInt(101),
      channelKey: 'basic_channel',
      title: titleTxt,
      body: messageTxt,
      bigPicture:image,
    ));
  }
}

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: MySplash(),
//     );
//   }
// }

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MySplashState createState() => _MySplashState();
}

class _MySplashState extends State<MyApp> {
  bool isTimeOut = false;

  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light);
    startTimeCount();
    super.initState();
  }

  startTimeCount() async {
    var _duration = const Duration(seconds: 1000);
    return Timer(_duration, navigateInside);
  }

  void navigateInside() => {
    AppPrefrence.getBoolean(AppConstants.SHARED_PREFERENCES_IS_LOGGED_IN).then((value) => {
      if (value)
        {
          navigateToHomePage(context),
        }
      else
        {
          setState(() {
            isTimeOut = true;
          })
        }
    }),

  };

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.transparent,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/



        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return Scaffold(
      body: new Stack(
        children: [
          new Container(
            width: MediaQuery.of(context).size.width,
            child: Image.asset(AppImages.splashMainURL,
                width: MediaQuery.of(context).size.width, fit: BoxFit.fill),
          ),
          new Align(
            alignment: Alignment.bottomCenter,
            child: new Container(
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  new Container(
                    margin: EdgeInsets.only(top: 20, left: 20, right: 20),
                    height: 40,
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                      onPressed: () => {navigateToRegisterNew(context)},
                      style: ButtonStyle(
                          backgroundColor:
                          MaterialStateProperty.all(Themecolor),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Register',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Row(mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(width: 50 ,child: Divider(color: Colors.white,thickness: 2,height: 1)),
                        SizedBox(width: 5,),
                        Text(
                          'Already A User?',
                          textAlign: TextAlign.end,
                          style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.white,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(width: 5,),
                        Container(width: 50 ,child: Divider(color: Colors.white,thickness: 2,height: 1)),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20, right: 20,bottom: 40),
                    height: 40,
                    width: MediaQuery.of(context).size.width,
                    child: ElevatedButton(
                      onPressed: () => {navigateToLogin(context)},
                      style: ButtonStyle(
                          backgroundColor:
                          MaterialStateProperty.all(Colors.white),
                          shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(20.0)))),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Login',
                          style: TextStyle(
                              color: Themecolor,
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  // new Container(
                  //   width: MediaQuery.of(context).size.width,
                  //   margin: EdgeInsets.fromLTRB(20, 15, 20, 30),
                  //   child: new Row(
                  //     mainAxisSize: MainAxisSize.max,
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     children: [
                  //       new GestureDetector(
                  //         behavior: HitTestBehavior.translucent,
                  //         onTap: () => {navigateToRegisterNew(context)},
                  //         child: new Container(
                  //           child: new Row(
                  //             children: [
                  //               Text(
                  //                 'Invited By A Friend?  ',
                  //                 textAlign: TextAlign.left,
                  //                 style: TextStyle(
                  //                     fontSize: 12.0,
                  //                     color: Colors.white,
                  //                     fontStyle: FontStyle.normal,
                  //                     fontWeight: FontWeight.normal),
                  //               ),
                  //               Text(
                  //                 'Enter Code',
                  //                 textAlign: TextAlign.left,
                  //                 style: TextStyle(
                  //                     fontSize: 12.0,
                  //                     color: LinkTextColor,
                  //                     fontStyle: FontStyle.normal,
                  //                     fontWeight: FontWeight.bold),
                  //               ),
                  //             ],
                  //             crossAxisAlignment: CrossAxisAlignment.start,
                  //           ),
                  //         ),
                  //       ),
                  //     ],
                  //   ),
                  // )
                ],
              ),
            ),
          )
        ],
      ),
    );


  }

}
