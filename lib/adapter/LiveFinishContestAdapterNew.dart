import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/repository/model/category_contest_response.dart';
import 'package:EverPlay/repository/model/refresh_score_response.dart';

class LiveFinishContestAdapterNew extends StatefulWidget {
  GeneralModel model;
  LiveFinishedContestData contest;
   Contest contestNew=new Contest();
  LiveFinishContestAdapterNew(this.model, this.contest);

  @override
  _LiveFinishContestAdapterStateNew createState() =>
      new _LiveFinishContestAdapterStateNew();
}

class _LiveFinishContestAdapterStateNew
    extends State<LiveFinishContestAdapterNew> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      behavior: HitTestBehavior.translucent,
      child: Opacity(
        opacity: widget.contest.challenge_status == 'canceled' ? 0.4 : 1.0,
        child: new Stack(
          alignment: Alignment.bottomCenter,
          children: [
            widget.contest.challenge_status == 'canceled'
                ? new Container(
                    width: MediaQuery.of(context).size.width,
                    height: 30,
                    margin: EdgeInsets.only(left: 20, right: 20, top: 5),
                    child: new Card(
                      margin: EdgeInsets.zero,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(10),
                              bottomRight: Radius.circular(10))),
                      child: new Container(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          widget.contest.challenge_text??'',
                          style: TextStyle(
                              fontFamily: 'roboto',
                              color: primaryColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 12),
                        ),
                      ),
                    ),
                  )
                : new Container(),

            Card(
              clipBehavior: Clip.antiAlias,
              elevation: 5,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)),
              margin: EdgeInsets.only(
                  left: 10,
                  right: 10,
                  top: 8,
                  bottom:
                      widget.contest.challenge_status == 'canceled' ? 28 : 8),
              child: new Container(
                child: new Column(
                  children: [
//                     new Container(
//                       margin: EdgeInsets.only(top: 5),
//                       child: new Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                         children: [
// /*                          new Container(
//                           height: 20,
//                           padding: EdgeInsets.only(left: 5,right: 15),
//                           decoration: BoxDecoration(
//                               image: DecorationImage(
//                                   image: AssetImage(AppImages.winnersBgIcon),
//                                   fit: BoxFit.fill)),
//                           child: new Row(
//                             children: [
//                               new Container(
//                                 margin: EdgeInsets.only(right: 5),
//                                 height: 15,
//                                 width: 15,
//                                 child: new Image.asset(AppImages.winnersIcon,
//                                   fit: BoxFit.fill,color: orangeColor,),
//                               ),
//                               new Container(
//                                 height: 30,
//                                 alignment: Alignment.centerLeft,
//                                 child: Text(
//                                   widget.contest.totalwinners.toString(),
//                                   style: TextStyle(
//                                       color: orangeColor,
//                                       fontWeight: FontWeight.w800,
//                                       fontSize: 13),
//                                 ),
//                               ),
//                               new Container(
//                                 height: 30,
//                                 alignment: Alignment.centerLeft,
//                                 child: Text(
//                                   ' Winners',
//                                   style: TextStyle(
//                                       color: Colors.black,
//                                       fontWeight: FontWeight.w400,
//                                       fontSize: 11),
//                                 ),
//                               )
//                             ],
//                           ),
//
//                         ),*/
//                           new Container(
//                             margin: EdgeInsets.only(right: 10),
//                             child: new Row(
//                               children: [
// /*                                new Row(
//                                 children: [
//                                   new Container(
//                                     margin: EdgeInsets.only(left: 7,right: 5),
//                                     height: 12,
//                                     width: 12,
//                                     child: Image(
//                                       image: AssetImage(
//                                         AppImages.winnerMedalIcon,),color: Colors.grey,),
//                                   ),
//                                   new Text('₹'+widget.contest.first_rank_prize.toString(),
//                                       style: TextStyle(
//                                           fontFamily: 'noway',
//                                           color: Colors.grey,
//                                           fontWeight: FontWeight.w500,
//                                           fontSize: 12)),
//                                 ],
//                               ),*/
//                                 widget.contest.multi_entry == 1
//                                     ? new Row(
//                                         children: [
//                                           new Container(
//                                             margin: EdgeInsets.only(
//                                                 left: 7, right: 2),
//                                             height: 14,
//                                             width: 14,
//                                             alignment: Alignment.center,
//                                             decoration: BoxDecoration(
//                                                 border: Border.all(
//                                                     color: Colors.grey),
//                                                 borderRadius:
//                                                     BorderRadius.circular(2)),
//                                             child: new Text('M',
//                                                 style: TextStyle(
//                                                     fontFamily: 'noway',
//                                                     color: Colors.grey,
//                                                     fontWeight: FontWeight.w500,
//                                                     fontSize: 10)),
//                                           ),
//                                           new Text(
//                                               'Up to ' +
//                                                   widget.contest
//                                                       .max_multi_entry_user
//                                                       .toString(),
//                                               style: TextStyle(
//                                                   fontFamily: 'noway',
//                                                   color: Colors.grey,
//                                                   fontWeight: FontWeight.w500,
//                                                   fontSize: 10)),
//                                         ],
//                                       )
//                                     : new Container(),
//                                 widget.contest.confirmed_challenge == 1
//                                     ? new Container(
//                                         margin:
//                                             EdgeInsets.only(left: 5, right: 0),
//                                         height: 13,
//                                         width: 13,
//                                         child: Image(
//                                           image: AssetImage(
//                                             AppImages.confirmIcon,
//                                           ),
//                                           color: Colors.grey,
//                                         ),
//                                       )
//                                     : new Container(),
//                               ],
//                             ),
//                           ),
//                         ],
//                       ),
//                     ),
                    new Container(
                      margin: EdgeInsets.only(
                          left: 10, right: 10, top: 5, bottom: 5),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Prize Pool',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 11),
                                ),
                                new Container(
                                  height: 22,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    '₹' +
                                        (widget.contest
                                                    .is_giveaway_visible_text ==
                                                0
                                            ? widget.contest.win_amount
                                                .toString()
                                            : widget.contest.is_giveaway_text!),
                                    style: TextStyle(
                                        color: widget.contest
                                                    .is_giveaway_visible_text ==
                                                0
                                            ? Colors.black
                                            : MethodUtils.hexToColor(
                                                widget.contest.giveaway_color!),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'Winners',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 11),
                                ),
                                new Container(
                                  height: 22,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    widget.contest.totalwinners.toString(),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14),
                                  ),
                                ) /*                                new Container(
                                height: 22,
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  widget.contest.maximum_user.toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14),
                                ),
                              )*/
                                ,
                              ],
                            ),
                          ),
                          new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  'Entry Fees',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 11),
                                ),
                                new Container(
                                  height: 22,
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    '₹' + widget.contest.entryfee.toString(),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      height: 1,
                      thickness: 0.5,
                    ),

                    new Container(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 8.0, right: 8.0, top: 10, bottom: 10),
                        child: Row(

                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                          Row(
                            children: [
                              widget.contest.challenge_type != 'percentage'
                                  ? new Row(
                                      children: [
                                        new Container(
                                          // margin: EdgeInsets.only(left: 7,right: 5),
                                          height: 15,
                                          width: 15,
                                          child: Image(
                                            image: AssetImage(
                                              AppImages.winnerMedalIcon,
                                            ),
                                            color: Colors.black,
                                          ),
                                        ),
                                        new Text(
                                            '₹' +
                                                widget.contest.first_rank_prize!
                                                    .toString(),
                                            style: TextStyle(
                                                fontFamily: 'noway',
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 10)),
                                      ],
                                    )
                                  : new Container(),
                              new Row(
                                children: [
                                  new Container(
                                    margin: EdgeInsets.only(left: 7, right: 2),
                                    height: 12,
                                    width: 12,
                                    child: Image(
                                      image: AssetImage(
                                        AppImages.winnersIcon,
                                      ),
                                      color: Colors.black,
                                    ),
                                  ),
                                  new Text(
                                      widget.contest.challenge_type == 'percentage' ? widget.contest.winning_percentage.toString() + '% Win' : widget.contest.totalwinners.toString() + ' Team Win',
                                      style: TextStyle(
                                          fontFamily: 'noway',
                                          color: Colors.black,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 10)),
                                ],
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              (widget.contest.confirmed_challenge ?? 0) == 1
                                  ? new Container(
                                      margin:
                                          EdgeInsets.only(left: 7, right: 2),
                                      height: 14,
                                      width: 14,
                                      alignment: Alignment.center,
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              color: Color(0xFF54c2ed)),
                                          borderRadius:
                                              BorderRadius.circular(2)),
                                      child: new Text('C',
                                          style: TextStyle(
                                              fontFamily: 'noway',
                                              color: Color(0xFF54c2ed),
                                              fontWeight: FontWeight.w500,
                                              fontSize: 10)),
                                    )
                                  : new Container(),
                              (widget.contest.multi_entry ?? 0) == 1
                                  ? new Row(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: 7, right: 2),
                                          height: 14,
                                          width: 14,
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              border: Border.all(
                                                  color: Color(0xFF6b3d95)),
                                              borderRadius:
                                                  BorderRadius.circular(2)),
                                          child: new Text('M',
                                              style: TextStyle(
                                                  fontFamily: 'noway',
                                                  color: Color(0xFF6b3d95),
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 10)),
                                        ),

                                        // new Text('Up to '+widget.contest.max_multi_entry_user!.toString(),
                                        //     style: TextStyle(
                                        //         fontFamily: 'noway',
                                        //         color: Colors.grey,
                                        //         fontWeight: FontWeight.w500,
                                        //         fontSize: 10)),
                                      ],
                                    )
                                  : new Container(),
                              Visibility(
                                // visible:widget.contest.bonus_percent==1?true:false,
                                child: Container(
                                  margin: EdgeInsets.only(left: 5),
                                  //  height: 14,
                                  // padding: EdgeInsets.only(left: 2,right: 2),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      // border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(5),
                                          bottomRight: Radius.circular(5),
                                          topLeft: Radius.circular(2),
                                          bottomLeft: Radius.circular(2))),
                                  child: Padding(
                                    padding: const EdgeInsets.all(3),
                                    child: Row(
                                      children: [
                                        new Text('B',
                                            style: TextStyle(
                                                fontFamily: 'noway',
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 10)),
                                        SizedBox(
                                          width: 5,
                                        ),
                                        Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius: BorderRadius.only(
                                                  topRight: Radius.circular(3),
                                                  bottomRight:
                                                      Radius.circular(3))),
                                          child: Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Text(
                                                widget.contest.bonus_percent!,
                                                style: TextStyle(
                                                    fontFamily: 'noway',
                                                    color: Color(0xFF8e9193),
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 10)),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
/*                          new Container(
                            height: 20,
                            margin: EdgeInsets.only(top: 5),
                            child: new Row(
                              mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                              children: [
                                new Container(
                                  // margin: EdgeInsets.only(left: 10),
                                  child: Text(
                                    widget.contest.challenge_type ==
                                            'percentage'
                                        ? widget.contest.joinedusers!
                                                .toString() +
                                            '/'
                                        : widget.contest.is_flexible == 1
                                            ? widget.contest.joinedusers!
                                                    .toString() +
                                                '/' +
                                                widget.contest.maximum_user!
                                                    .toString()
                                            : widget.contest.maximum_user! -
                                                        widget.contest
                                                            .joinedusers! >
                                                    0
                                                ? NumberFormat.decimalPattern(
                                                            'hi')
                                                        .format(widget.contest
                                                                .maximum_user! -
                                                            widget.contest
                                                                .joinedusers!)
                                                        .toString() +
                                                    ' Sports Left'
                                                : 'Challenge Closed',
                                    style: TextStyle(
                                        fontFamily: 'noway',
                                        color: widget.contest.is_flexible == 1
                                            ? Colors.grey
                                            : primaryColor,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12),
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(right: 10),
                                  child: Text(
                                    widget.contest.is_flexible == 1
                                        ? 'Flexible'
                                        : NumberFormat.decimalPattern('hi')
                                                .format(widget
                                                    .contest.maximum_user!)
                                                .toString() +
                                            ' Sports',
                                    style: TextStyle(
                                        fontFamily: 'noway',
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12),
                                  ),
                                ),
                              ],
                            ),
                          ),*/
                        ]),
                      ),
                    ),
                    widget.contest.winners_zone!.length > 0
                        ? new Container(
                            color: lightYellowColor,
                            padding: EdgeInsets.only(
                                top: 10, bottom: 10, left: 10, right: 10),
                            child: new Container(
                              child: new Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  new Flexible(
                                    child: new Container(
                                      child: new Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          new Row(
                                            children: [
                                              Flexible(
                                                  child: new Text(
                                                      widget
                                                          .contest
                                                          .winners_zone![0]
                                                          .team_name!,
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                        color: Colors.grey,
                                                        fontWeight:
                                                            FontWeight.w500,
                                                        fontSize: 11,
                                                      ))),
                                              new Text(
                                                  '(T' +
                                                      widget
                                                          .contest
                                                          .winners_zone![0]
                                                          .team_number
                                                          .toString() +
                                                      ')',
                                                  style: TextStyle(
                                                      color: Colors.grey,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      fontSize: 11)),
                                            ],
                                          ),
                                          widget.model.isFromLive!&& widget.model.is_winning_zone==1
                                              ? new Container(
                                                  margin:
                                                      EdgeInsets.only(top: 3),
                                                  child: new Text(
                                                    'IN WINNING ZONE',
                                                    style: TextStyle(
                                                        fontFamily: 'noway',
                                                        color: greenColor,
                                                        fontSize: 11,
                                                        fontWeight:
                                                            FontWeight.normal),
                                                  ),
                                                )
                                              : new Container(),
                                               double.parse(widget.contest.winners_zone![0].amount ?? '0') > 0 && !widget.model.isFromLive!
                                              ? new Container(
                                                  margin:
                                                      EdgeInsets.only(top: 3),
                                                  child: new Text(
                                                    'YOU WON: ₹' +
                                                        widget
                                                            .contest
                                                            .winners_zone![0]
                                                            .amount
                                                            .toString(),
                                                    style: TextStyle(
                                                        fontFamily: 'noway',
                                                        color: greenColor,
                                                        fontSize: 11,
                                                        fontWeight:
                                                            FontWeight.normal),
                                                  ),
                                                )
                                              : new Container(),
                                        ],
                                      ),
                                    ),
                                    flex: 1,
                                  ),
                                  new Flexible(
                                    child: new Container(
                                      child: new Text(
                                          widget
                                              .contest.winners_zone![0].points!,
                                          style: TextStyle(
                                              fontFamily: 'noway',
                                              color: Colors.black,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 11)),
                                    ),
                                    flex: 1,
                                  ),
                                  new Flexible(
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        new Text(
                                            '#' +
                                                widget.contest.winners_zone![0]
                                                    .rank
                                                    .toString(),
                                            style: TextStyle(
                                                color: orangeColor,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 11)),
                                        new Container(
                                          alignment: Alignment.center,
                                          child: Icon(
                                            Icons.remove,
                                            size: 15,
                                          ),
                                        )
                                      ],
                                    ),
                                    flex: 1,
                                  ),
                                ],
                              ),
                            ),
                          )
                        : new Container(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      onTap: () => {

        if (widget.contest.challenge_status != 'canceled')
          {

            widget.contestNew.challenge_type=widget.contest.challenge_type,
            widget.contestNew.entryfee=widget.contest.entryfee,
            widget.contestNew.getjoinedpercentage=widget.contest.getjoinedpercentage,
            // widget.contestNew.matchkey=widget.model.matchKey,
            widget.contestNew.first_rank_prize=widget.contest.first_rank_prize,
            widget.contestNew.winning_percentage=widget.contest.winning_percentage,
            widget.contestNew.bonus_percent=widget.contest.bonus_percent,
            widget.contestNew.confirmed_challenge=widget.contest.confirmed_challenge,
            widget.contestNew.is_bonus=widget.contest.is_bonus,
            widget.contestNew.giveaway_color=widget.contest.giveaway_color,
            widget.contestNew.name=widget.contest.name,
            widget.contestNew.win_amount=widget.contest.win_amount,
            widget.contestNew.is_flexible=widget.contest.is_flexible,
            widget.contestNew.joinedusers=widget.contest.joinedusers,
            widget.contestNew.maximum_user=widget.contest.maximum_user,
            widget.contestNew.max_multi_entry_user=widget.contest.max_multi_entry_user,
            widget.contestNew.id=widget.contest.id,
            widget.contestNew.pdf=widget.contest.pdf,
            widget.contestNew.totalwinners=widget.contest.totalwinners,
            // widget.model.headerText="In Progress",

              print(widget.contest.id),
            navigateToUpcomingContestDetails(context, widget.model, widget.contestNew)
          }
      },
    );
  }
}
