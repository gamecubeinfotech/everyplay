import 'package:cached_network_image/cached_network_image.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/date_utils.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/appUtilities/date_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/ScrollingText.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/banner_response.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class UserMyMatchesItemAdapter extends StatefulWidget {

  late MatchDetails matchDetails;

  UserMyMatchesItemAdapter(this.matchDetails);

  @override
  _UserMyMatchesItemAdapterState createState() => new _UserMyMatchesItemAdapterState();
}

class _UserMyMatchesItemAdapterState extends State<UserMyMatchesItemAdapter>{
  late String userId='0';

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
      })
    });
  }


  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      behavior:
      HitTestBehavior.translucent,
      child: new Stack(
        children: [
          new Container(
            child: Card(
              clipBehavior: Clip.antiAlias,
              elevation: 1,
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(
                      10)),
              child: Container(
                padding: EdgeInsets.all(0),
                child: new Container(
                  child: new Column(
                    children: [
                      new Container(
                        height: 20,
                        margin:
                        EdgeInsets.only(
                            top: 5),
                        child: new Row(
                          mainAxisAlignment:
                          MainAxisAlignment
                              .spaceBetween,
                          children: [
                            new Container(
                              width: 160,
                              margin: EdgeInsets
                                  .only(
                                  left:
                                  10,
                                  top: 5),
                              child: Text(
                                widget.matchDetails.name!,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontFamily:
                                    'noway',
                                    color: darkGrayColor,
                                    fontWeight:
                                    FontWeight
                                        .w400,
                                    fontSize:
                                    12),
                              ),
                            ),
                            new Row(
                              children: [
                                widget.matchDetails.lineup==1?new Container(
                                  alignment:
                                  Alignment
                                      .center,
                                  child:
                                  new Row(
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(
                                            left: 5,
                                            right: 5),
                                        height:
                                        10,
                                        width:
                                        10,
                                        child:
                                        Image(image: AssetImage(AppImages.lineupOutImageURL)),
                                      ),
                                      new Container(
                                        margin:
                                        EdgeInsets.only(top: 2),
                                        child: new Text(
                                            'LINEUPS OUT',
                                            style: TextStyle(fontFamily: 'noway', color: greenColor, fontWeight: FontWeight.w600, fontSize: 12)),
                                      ),
                                    ],
                                  ),
                                ):new Container(),
                              ],
                            ),
                          ],
                        ),
                      ),
                      new Divider(),
                      new Container(
                        padding: EdgeInsets.only(bottom: 5),
                        alignment: Alignment.center,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            new Flexible(child: new Container(
                              child: new Column(
                                children: [
                                  new Container(
                                    width: 110,
                                    margin: EdgeInsets.only(bottom: 5,left: 10),
                                    child:  new Text(
                                      widget.matchDetails.team1name!,
                                      textAlign:
                                      TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontFamily: 'noway',
                                          color: darkGrayColor,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12),
                                    ),
                                  ),
                                  new Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      new Container(
                                        child: new Stack(
                                          alignment:
                                          Alignment
                                              .centerLeft,
                                          children: [
                                            new Container(
                                              height:
                                              16,
                                              width:
                                              43,
                                              child:
                                              Image(image: AssetImage(AppImages.leftTeamIcon),fit: BoxFit.fill,color: MethodUtils.hexToColor(widget.matchDetails.team1_color!),),
                                            ),
                                            new Container(
                                              height:
                                              25,
                                              width:
                                              40,
                                              child:
                                              CachedNetworkImage(
                                                imageUrl: widget.matchDetails.team1logo!,
                                                errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(
                                            left:
                                            5),
                                        child:
                                        new Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [

                                            new Container(
                                              margin:
                                              EdgeInsets.only(top: 2),
                                              width: 60,
                                              child:
                                              new Text(
                                                widget.matchDetails.team1display!,
                                                textAlign: TextAlign.left,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(fontFamily: 'noway', color: Colors.black, fontWeight: FontWeight.w600, fontSize: 14),
                                              ),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),flex: 1,),
                            new Flexible(child: new Container(
                              child: new Column(
                                children: [
                                  widget.matchDetails.is_leaderboard==1?new Container(
                                    height: 25,
                                    padding: EdgeInsets.all(3),
                                    margin: EdgeInsets.only(bottom: 5),
                                    child: Image(image: AssetImage(AppImages.matchLeaderboardIcon)),
                                  ):new Container(),
                                  CustomTimer(
                                    from: Duration(seconds: SuperDateFormat.getFormattedDateObj(widget.matchDetails.time_start!)!.difference(MethodUtils.getDateTime()).inSeconds),
                                    to: Duration(seconds: 0),
                                    onBuildAction: CustomTimerAction.auto_start,
                                    onFinish: (){
                                      // widget.onMatchTimeUp(widget.index);
                                    },
                                    builder: (CustomTimerRemainingTime remaining) {
                                      return new GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: Text(
                                          int.parse(remaining.days)>=1?int.parse(remaining.days)>1?"${remaining.days} Days":"${remaining.days} Day":int.parse(remaining.hours)>=1?"${remaining.hours}h :${remaining.minutes}m":int.parse(remaining.seconds)>=1?"${remaining.minutes}m :${remaining.seconds}s":widget.matchDetails.match_status!,
                                          style: TextStyle(
                                              fontFamily: 'noway',
                                              color: Title_Color_1,
                                              decoration: TextDecoration.none,
                                              fontWeight:
                                              FontWeight.w500,
                                              fontSize: 12),
                                        ),
                                        onTap: (){
                                        },
                                      );
                                    },
                                  )
                                ],
                              ),
                            ),flex: 1,),
                            new Flexible(child: new Container(
                              child: new Column(
                                children: [
                                  new Container(
                                    width: 110,
                                    margin: EdgeInsets.only(bottom: 5,right: 10),
                                    alignment: Alignment.centerRight,
                                    child: new Text(
                                      widget.matchDetails.team2name!,
                                      textAlign:
                                      TextAlign.left,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontFamily: 'noway',
                                          color: darkGrayColor,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12),
                                    ),
                                  ),
                                  new Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(
                                            right:
                                            5),
                                        child:
                                        new Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                          children: [
                                            new Container(
                                              margin:
                                              EdgeInsets.only(top: 2),
                                              width: 60,
                                              alignment: Alignment.centerRight,
                                              child:
                                              new Text(
                                                widget.matchDetails.team2display!,
                                                textAlign: TextAlign.left,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(fontFamily: 'noway', color: Colors.black, fontWeight: FontWeight.w600, fontSize: 14),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      new Stack(
                                        alignment:
                                        Alignment
                                            .centerLeft,
                                        children: [
                                          new Container(
                                            height:
                                            16,
                                            width:
                                            43,
                                            child:
                                            Image(image: AssetImage(AppImages.rightTeamIcon),fit: BoxFit.fill,color: MethodUtils.hexToColor(widget.matchDetails.team2_color!),),
                                          ),
                                          new Container(
                                            height:
                                            25,
                                            width:
                                            40,
                                            child:
                                            CachedNetworkImage(
                                              imageUrl: widget.matchDetails.team2logo!,
                                              errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),flex: 1,),
                          ],
                        ),
                      ),
                      new Container(
                        height: 32,
                        color: lightGrayColor,
                        child: new Container(
                          child: new Row(
                            mainAxisAlignment:
                            MainAxisAlignment.spaceBetween,
                            children: [
                              new Row(
                                children: [
                                  new Container(
                                    margin: EdgeInsets.only(left: 10),
                                    child: Text(
                                      widget.matchDetails.team_count.toString()+' Team',
                                      style: TextStyle(
                                          color:
                                          Colors.black,
                                          fontSize: 12,
                                          fontFamily: 'noway',
                                          fontWeight:
                                          FontWeight.w400),
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(left: 10),
                                    child: new Text(
                                      widget.matchDetails.joined_count.toString()+' Contests Joined',
                                      style: TextStyle(
                                          color:
                                          Colors.grey,
                                          fontSize: 12,
                                          fontFamily: 'noway',
                                          fontWeight:
                                          FontWeight.w400),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
      onTap: () => {
        if((widget.matchDetails.match_status)?.toLowerCase()=='upcoming'){
          navigateToUpcomingContests(context,new GeneralModel(bannerImage: widget.matchDetails.banner_image,matchKey: widget.matchDetails.matchkey,teamVs: (widget.matchDetails.team1display!+' VS '+widget.matchDetails.team2display!),firstUrl: widget.matchDetails.team1logo,secondUrl: widget.matchDetails.team2logo,headerText: widget.matchDetails.time_start,sportKey: widget.matchDetails.sport_key,battingFantasy: widget.matchDetails.battingfantasy,bowlingFantasy: widget.matchDetails.bowlingfantasy,liveFantasy: widget.matchDetails.livefantasy,secondInningFantasy: widget.matchDetails.secondinning,reverseFantasy: widget.matchDetails.reversefantasy,fantasySlots: widget.matchDetails.slotes,team1Name: widget.matchDetails.team1name,team2Name: widget.matchDetails.team2name,team1Logo: widget.matchDetails.team1logo,team2Logo: widget.matchDetails.team2logo))
        }else if((widget.matchDetails.match_status)?.toLowerCase()=='in progress'){
          navigateToLiveFinishContestsNew(context,new GeneralModel(bannerImage: widget.matchDetails.banner_image,matchKey: widget.matchDetails.matchkey,teamVs: (widget.matchDetails.team1display!+' VS '+widget.matchDetails.team2display!),firstUrl: widget.matchDetails.team1logo,secondUrl: widget.matchDetails.team2logo,headerText: widget.matchDetails.time_start,sportKey: widget.matchDetails.sport_key,battingFantasy: widget.matchDetails.battingfantasy,bowlingFantasy: widget.matchDetails.bowlingfantasy,liveFantasy: widget.matchDetails.livefantasy,secondInningFantasy: widget.matchDetails.secondinning,reverseFantasy: widget.matchDetails.reversefantasy,fantasySlots: widget.matchDetails.slotes,team1Name: widget.matchDetails.team1name,team2Name: widget.matchDetails.team2name,team1Logo: widget.matchDetails.team1logo,team2Logo: widget.matchDetails.team2logo,isFromLive: true,isFromLiveFinish: true))
        }else{
          navigateToLiveFinishContestsNew(context,new GeneralModel(bannerImage: widget.matchDetails.banner_image,matchKey: widget.matchDetails.matchkey,teamVs: (widget.matchDetails.team1display!+' VS '+widget.matchDetails.team2display!),firstUrl: widget.matchDetails.team1logo,secondUrl: widget.matchDetails.team2logo,headerText: widget.matchDetails.time_start,sportKey: widget.matchDetails.sport_key,battingFantasy: widget.matchDetails.battingfantasy,bowlingFantasy: widget.matchDetails.bowlingfantasy,liveFantasy: widget.matchDetails.livefantasy,secondInningFantasy: widget.matchDetails.secondinning,reverseFantasy: widget.matchDetails.reversefantasy,fantasySlots: widget.matchDetails.slotes,team1Name: widget.matchDetails.team1name,team2Name: widget.matchDetails.team2name,team1Logo: widget.matchDetails.team1logo,team2Logo: widget.matchDetails.team2logo,isFromLive: false,isFromLiveFinish: true))
        }
      },
    );
  }

  void remindMeDialog(){
    AlertDialog alertDialog=AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius:
          BorderRadius.all(Radius.circular(10.0))),
      title: Text("Match alert",
          style: TextStyle(color: Colors.black)),
      content: Text(
          "Remind me when match CON vs STL is about to start (before 25 min)",
          style: TextStyle(color: Colors.grey)),
      actions: [
        continueButton(context),
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alertDialog;
      },
    );
  }
  Widget continueButton(context) {
    return TextButton(
      onPressed: () {
        remindMe();
      },
      child: Text(
        "REMIND ME",
        style: TextStyle(color: Colors.black),
      ),
    );
  }
  void remindMe() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,matchkey: widget.matchDetails.matchkey);
    final client = ApiClient(AppRepository.dio);
    GeneralResponse myBalanceResponse = await client.remindMe(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (myBalanceResponse.status == 1) {
      Navigator.pop(context);
    }
    Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
  }
}