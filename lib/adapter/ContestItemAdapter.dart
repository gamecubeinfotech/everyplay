


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/dataModels/JoinChallengeDataModel.dart';

import '../appUtilities/app_images.dart';
import '../appUtilities/app_navigator.dart';
import '../repository/app_repository.dart';
import '../repository/model/base_request.dart';
import '../repository/model/category_contest_response.dart';
import '../repository/retrofit/api_client.dart';
import 'package:EverPlay/repository/model/get_offer_response.dart';

class ContestItemAdapter extends StatefulWidget {
  Contest contest;
  GeneralModel model;
  Function onJoinContestResult;
  String userId;


  Function getWinnerPriceCard;
  ContestItemAdapter(this.model,this.contest,this.onJoinContestResult,this.userId,this.getWinnerPriceCard);
  @override
  _ContestItemAdapterState createState() => new _ContestItemAdapterState();
}

class _ContestItemAdapterState extends State<ContestItemAdapter>{
  GetOfferResponse? offerResponse;
  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {

    super.dispose();
  }
  bottomSheet(GetOfferResponse Response){
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      enableDrag: true,
      backgroundColor: Colors.transparent,
      shape : RoundedRectangleBorder(
          borderRadius : BorderRadius.only(topLeft: Radius.circular(40),topRight: Radius.circular(40))
      ),
      builder: (context) {
        return DraggableScrollableSheet(

          // expand: true,
            initialChildSize: 0.30,
            minChildSize: 0.15,
            maxChildSize:1,
            builder: (context,controller) {
              return Container(
                //height: MediaQuery.of(context).size.height*0.5,
                decoration: BoxDecoration(
                    color: Colors.white,borderRadius: BorderRadius.only(topLeft: Radius.circular(40),topRight: Radius.circular(40))
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    controller: controller,
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.grey,borderRadius: BorderRadius.circular(10)
                            ),
                            width: 60,
                            height: 8,
                          ),
                        ),
                        SizedBox(height: 10,),
                        //Image.asset(AppImages.offerPopup,scale: 3,),
                        Image.asset(AppImages.addIcon,scale: 3,),
                        SizedBox(height: 10,),
                        Column(
                          children: [
                            Container(
                              color: lightBlack,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Team NO.',style: TextStyle(
                                        color: Colors.white
                                    ),),
                                    Text('Entry Fee',style: TextStyle(
                                        color: Colors.white
                                    ),),
                                    Text('Offer Entry',style: TextStyle(
                                        color: Colors.white
                                    ),),

                                  ],
                                ),
                              ),
                            ),
                            ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: offerResponse!.result!.length,
                                itemBuilder: (context,index){

                                  return Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Text(Response.result![index].team!,style: TextStyle(
                                              color: Colors.black,fontWeight: FontWeight.bold
                                          ),),
                                        ),
                                        Expanded(
                                          child: Center(
                                            child: Text(Response.result![index].entryfee.toString(),style: TextStyle(
                                                color: Colors.black,fontWeight: FontWeight.bold
                                            ),),
                                          ),
                                        ),
                                        Expanded(
                                          child: Center(
                                            child: Container(
                                              margin:EdgeInsets.only(left: 30),
                                              child: Text(Response.result![index].offer.toString(),style: TextStyle(
                                                  color: Colors.black,fontWeight: FontWeight.bold
                                              ),),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                })
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }
        );
      },
    );
  }

  Widget btnEntryText(){
    return new Text(
      widget.contest.is_free==1||widget.contest.is_first_time_free==1?'FREE':widget.contest.isjoined!=null&&widget.contest.isjoined!?widget.contest.multi_entry==1?'JOIN+':'INVITE':'₹'+widget.contest.entryfee.toString(),
      style: TextStyle(fontFamily: 'noway',fontSize: 12,fontWeight: FontWeight.w500,color: Colors.white,),
      textAlign: TextAlign.center,
    );
  }
  void getOffer(String challengeId )async{
    AppLoaderProgress.showLoader(context);
    final client=ApiClient(AppRepository.dio);
    GeneralRequest request=GeneralRequest(user_id: widget.userId,challenge_id: challengeId);
    final response=await client.getOffer(request);
    if(response.status==1){
      AppLoaderProgress.hideLoader(context);
      offerResponse=response;
      bottomSheet(offerResponse!);
    }
    else{
      AppLoaderProgress.hideLoader(context);
      Fluttertoast.showToast(msg: response.message!);
    }
    // AppLoaderProgress.hideLoader(context);
  }
  @override
  Widget build(BuildContext context) {
    print("widget.contest.challenge_type ${widget.contest.first_rank_prize!}");
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      child: Padding(
        padding: const EdgeInsets.only(bottom: 5),
        child: Card(
          clipBehavior: Clip.antiAlias,
          elevation: 0,
          shape: RoundedRectangleBorder(side: BorderSide(color: bordercolor),
              borderRadius: BorderRadius.circular(5)),
          child: Container(
            // padding: EdgeInsets.all(0),
            child: new Column(
              children: [
                widget.contest.is_champion==1?Container(
                  child: Column(
                    children: [
                      SizedBox(height: 10,),
                      Center(child: Text('Score more than me to',style: TextStyle(
                          fontWeight: FontWeight.w500
                      ),)),
                      SizedBox(height: 10,),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0,right: 8),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [

                            Image.network(widget.contest.champion_player!,fit: BoxFit.contain,scale: 3.5,
                              loadingBuilder:(context, Widget child, loadingProgress) {
                                if (loadingProgress == null) return child;
                                return Center(
                                  child: CircularProgressIndicator(

                                  ),
                                );
                              },

                            ),
                            Stack(
                              children: [
                                Container(
                                  height: 50,
                                  width: 150,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Themecolor),
                                      borderRadius: BorderRadius.circular(5)
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 5,right: 5),
                                    child: Center(child:Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(widget.contest.entryfee.toString()),
                                        Text(widget.contest.champion_x!,style: TextStyle(color: Themecolor),),
                                        Text(" = "+widget.contest.win_amount!),
                                      ],
                                    )),
                                  ),
                                ),
                                Positioned(
                                  right: 40,left: 40,bottom:43 , child: Container(
                                  width: 50,
                                  color: Colors.white,
                                  child: Center(
                                    child: Text('WIN',style: TextStyle(
                                        color: Themecolor,fontWeight: FontWeight.bold,fontSize: 12
                                    ),),
                                  ),
                                ),
                                )
                              ],
                            ),
                            new GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              child:  new Container(
                                alignment: Alignment.centerRight,
                                child: new Card(
                                  color: Themecolor,
                                  child: new Container(
                                    height: 25,
                                    width: 60,
                                    alignment: Alignment.center,
                                    child: btnEntryText(),
                                  ),
                                ),
                              ),
                              onTap: (){
                                if((btnEntryText() as Text).data=='INVITE'){
                                  MethodUtils.onShare(context,widget.contest,widget.model);
                                }
                                else{
                                  if(widget.model.teamCount==1){
                                    MethodUtils.checkBalance(new JoinChallengeDataModel(context, 0, int.parse(widget.userId), widget.contest.id!, widget.model.fantasyType!, widget.model.slotId!, 1, widget.contest.is_bonus!, widget.contest.win_amount!, widget.contest.maximum_user!, widget.model.teamId.toString(), widget.contest.entryfee.toString(), widget.model.matchKey!, widget.model.sportKey!, widget.model.joinedSwitchTeamId.toString(), false, 0, 0,widget.onJoinContestResult));
                                  }
                                  else if(widget.model.teamCount!>0){
                                    navigateToMyJoinTeams(context,widget.model,widget.contest,widget.onJoinContestResult);
                                  }
                                  else{
                                    widget.model.onJoinContestResult=widget.onJoinContestResult;
                                    widget.model.contest=widget.contest;
                                    navigateToCreateTeam(context,widget.model);
                                  }
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ):  Column(
                  children: [
                    new Container(
                      margin: EdgeInsets.only(top: 5,bottom: 5),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          new Container(
                            margin: EdgeInsets.only(left:10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(height: 5,),
                                widget.contest.is_offer_team==1?GestureDetector(
                                    onTap: (){
                                      getOffer(widget.contest.id.toString());
                                    },
                                    // child: Image.asset(AppImages.offer,scale: 3,)):  Text(
                                    child: Image.asset(AppImages.addIcon,scale: 3,)):  Row(
                                  children: [
                                    Image(
                                        image: AssetImage(
                                          AppImages.prizepool,),height: 10),
                                    SizedBox(width: 5,),
                                    Text(
                                      'Prize Pool'.toUpperCase(),
                                      style: TextStyle(
                                          fontFamily: 'noway',
                                          color: textGrey,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 12),
                                    ),
                                  ],
                                ),
                                widget.contest.is_gadget==1?Image.network(widget.contest.gadget_image!,fit: BoxFit.contain,scale: 5,):  new Container(
                                  height: 30,
                                  alignment: Alignment.centerLeft,
                                  child: Text((widget.contest.is_giveaway_visible_text==0?"₹ "+widget.contest.win_amount.toString():"₹"+widget.contest.is_giveaway_text!),
                                    style: TextStyle(
                                        fontFamily: 'noway',
                                        color: widget.contest.is_giveaway_visible_text==0?Colors.black:MethodUtils.hexToColor(widget.contest.giveaway_color!),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [

                              Row(
                                children: [
                                  Image(
                                      image: AssetImage(
                                        AppImages.winnerMedalIcon,),height: 11),
                                  SizedBox(width: 5,),
                                  Text(
                                    'First prize'.toUpperCase(),
                                    style: TextStyle(
                                        fontFamily: 'noway',
                                        color: textGrey,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 12),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5,),
                              widget.contest.challenge_type!='percentage'?new Text('₹ '+widget.contest.first_rank_prize!.toString(),
                                  style: TextStyle(
                                      fontFamily: 'noway',
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16)):new Container(),
                              SizedBox(height: 5,),
                            ],
                          ),
                          new Container(
                            margin: EdgeInsets.only(right: 10),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(right: 5,top:5),
                                  child: Row(
                                    children: [
                                      Image(
                                          image: AssetImage(
                                            AppImages.entryfee,),height: 12),
                                      SizedBox(width: 5,),
                                      Text(
                                        'Entry fee'.toUpperCase(),
                                        style: TextStyle(
                                            fontFamily: 'noway',
                                            color: textGrey,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12),
                                      ),
                                    ],
                                  ),
                                ),
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child:  new Container(
                                    alignment: Alignment.centerRight,
                                    child: new Card(
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                                      color: yellowdark,
                                      child: new Container(
                                        height: 25,
                                        width: 60,
                                        alignment: Alignment.center,
                                        child: btnEntryText(),
                                      ),
                                    ),
                                  ),
                                  onTap: (){
                                    if((btnEntryText() as Text).data=='INVITE'){
                                      MethodUtils.onShare(context,widget.contest,widget.model);
                                    }
                                    else{
                                      if(widget.model.teamCount==1){
                                        MethodUtils.checkBalance(new JoinChallengeDataModel(context, 0, int.parse(widget.userId), widget.contest.id!, widget.model.fantasyType!, widget.model.slotId!, 1, widget.contest.is_bonus!, widget.contest.win_amount!, widget.contest.maximum_user!, widget.model.teamId.toString(), widget.contest.entryfee.toString(), widget.model.matchKey!, widget.model.sportKey!, widget.model.joinedSwitchTeamId.toString(), false, 0, 0,widget.onJoinContestResult));
                                      }
                                      else if(widget.model.teamCount!>0){
                                        navigateToMyJoinTeams(context,widget.model,widget.contest,widget.onJoinContestResult);
                                      }
                                      else{
                                        widget.model.onJoinContestResult=widget.onJoinContestResult;
                                        widget.model.contest=widget.contest;
                                        navigateToCreateTeam(context,widget.model);
                                      }
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(left: 8,right: 8),
                      child: new CustomProgressIndicator(widget.contest.challenge_type=='percentage'?0.5:double.parse((widget.contest.joinedusers!/widget.contest.maximum_user!).toString())),
                    ),
                    new Container(
                      // height: 20,
                      margin: EdgeInsets.only(top: 5),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Container(
                            margin: EdgeInsets.only(left: 10),
                            child: Text(
                              widget.contest.challenge_type=='percentage'?widget.contest.joinedusers!.toString()+'/':widget.contest.is_flexible==1?widget.contest.joinedusers!.toString()+'/'+widget.contest.maximum_user!.toString():widget.contest.maximum_user!-widget.contest.joinedusers!>0?NumberFormat.decimalPattern('hi').format(widget.contest.maximum_user!-widget.contest.joinedusers!).toString()+' Spots Left':'Challenge Closed',
                              style: TextStyle(
                                  fontFamily: 'noway',
                                  color: widget.contest.is_flexible==1?Colors.grey:Title_Color_2,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12),
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.only(right: 10),
                            child: Text(
                              widget.contest.is_flexible==1?'Flexible':NumberFormat.decimalPattern('hi').format(widget.contest.maximum_user!).toString()+' Spots',
                              style: TextStyle(
                                  fontFamily: 'noway',
                                  color: Colors.grey,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),

                new Container(
                  color: TextFieldColor,
                  height: 30,
                  margin: EdgeInsets.only(top: widget.contest.is_champion==1?0: 10),
                  child: new Container(
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        new Row(
                          children: [
                            widget.contest.is_champion==1?Row(
                              children: [
                                SizedBox(width: 5,),
                                // Image.asset(AppImages.trophy,scale: 3,),
                                Image.asset(AppImages.addIcon,scale: 3,),
                                SizedBox(width: 5,),
                                Text('Joined Teams: ${widget.contest.joinedusers} ',style: TextStyle(
                                    color: Colors.black,fontWeight: FontWeight.bold,fontSize: 12
                                ),),
                              ],
                            )
                                :Row(
                              children: [

                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new Row(
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(left: 7,right: 2),
                                        height: 14,
                                        width: 18,
                                        child: Image(
                                          image: AssetImage(
                                            AppImages.winnersIcon,),),
                                      ),
                                      new Text(widget.contest.challenge_type=='percentage'?widget.contest.winning_percentage.toString()+'% Win':widget.contest.totalwinners.toString()+' Team Win',
                                          style: TextStyle(
                                              fontFamily: 'noway',
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 10)),

                                    ],
                                  ),
                                  onTap: (){
                                    widget.getWinnerPriceCard(widget.contest.id,widget.contest.win_amount.toString());
                                  },
                                ),


                                widget.contest.is_bonus==1?new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 7,right: 2),
                                      height: 14,
                                      width: 16,
                                      child: Image(
                                        image: AssetImage(
                                          AppImages.bonusIcon,),),
                                    ),
                                    new Text(widget.contest.bonus_percent!,
                                        style: TextStyle(
                                            fontFamily: 'noway',
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 10)),

                                  ],
                                ):new Container()
                              ],
                            )
                          ],
                        ),
                        new Row(
                          children: [
                            // new Container(
                            //   margin: EdgeInsets.only(left: 7,right: 2),
                            //   height: 14,
                            //   padding: EdgeInsets.only(left: 2,right: 2),
                            //   alignment: Alignment.center,
                            //   decoration: BoxDecoration(
                            //       border: Border.all(color: Colors.grey),
                            //       borderRadius: BorderRadius.circular(2)
                            //   ),
                            //   child: new Text('WD',
                            //       style: TextStyle(
                            //           fontFamily: 'noway',
                            //           color: Colors.grey,
                            //           fontWeight: FontWeight.w500,
                            //           fontSize: 10)),
                            // ),

                            widget.contest.confirmed_challenge==1?new Container(
                              margin: EdgeInsets.only(left: 7,right:  widget.contest.multi_entry==1?2:10),
                              height: 14,
                              width: 14,
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(2)
                              ),
                              child: new Text('c',
                                  style: TextStyle(
                                      fontFamily: 'noway',
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 11)),
                            ):new Container(),

                            widget.contest.multi_entry==1?new Row(
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(left: 7,right: 10),
                                  height: 13,
                                  // width: 14,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      border: Border.all(color: Colors.grey),
                                      borderRadius: BorderRadius.circular(2)
                                  ),
                                  child: Row(
                                    children: [
                                      Container(
                                        decoration:BoxDecoration(
                                            border: Border(
                                                right: BorderSide(color: Colors.grey)
                                            )
                                        ),
                                        child: new Text(' M ',
                                            style: TextStyle(
                                                fontFamily: 'noway',
                                                color: Colors.grey,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 10)),
                                      ),
                                      new Text(' '+widget.contest.max_multi_entry_user!.toString()+" ",
                                          style: TextStyle(
                                              fontFamily: 'noway',
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 9)
                                      ),
                                    ],
                                  ),
                                ),



                              ],
                            ):new Container(),
                          ],
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
      onTap: ()=>{
        navigateToUpcomingContestDetails(context,widget.model,widget.contest)
      },
    );
  }

}