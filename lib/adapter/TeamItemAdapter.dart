import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/repository/model/team_response.dart';

import '../dataModels/GeneralModel.dart';

class TeamItemAdapter extends StatefulWidget {

  GeneralModel model;
  Team team;
  bool isForJoinContest;
  int index;
  TeamItemAdapter(this.model,this.team, this.isForJoinContest,this.index);

  @override
  _TeamItemAdapterState createState() => new _TeamItemAdapterState();
}

class _TeamItemAdapterState extends State<TeamItemAdapter> {


  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: new GestureDetector(
        behavior: HitTestBehavior.translucent,
        child: Opacity(opacity: widget.team.is_joined!=1?1:.3,child: new Container(
          child: new Row(
            children: [
              new Expanded(
                  child: Container(
                    margin:EdgeInsets.only(bottom : 10),
                    decoration: BoxDecoration(border: Border.all(color: bordercolor,width: 0.5 ), borderRadius: BorderRadius.circular(12)),
                    child: Column(
                      children: [
                        Card(
                          elevation: 0,
                          clipBehavior: Clip.antiAlias,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12)),

                          child: Container(

                            padding: EdgeInsets.all(0),
                            child: new Column(
                              children: [
                                new Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: AssetImage(widget.model.sportKey==AppConstants.TAG_CRICKET||widget.model.sportKey==AppConstants.TAG_FOOTBALL?AppImages.whiteImage:widget.model.sportKey==AppConstants.TAG_BASEBALL||widget.model.sportKey==AppConstants.TAG_HOCKEY?AppImages.baseballTeamBgIcon:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppImages.handballTeamBgIcon:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppImages.basketballTeamBgIcon:AppImages.kabbadiTeamBgIcon),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  child: new Column(
                                    children: [

                                      new Container(
                                        padding: EdgeInsets.only(top: 5),
                                        height: 43,
                                          // color: Colors.black26,
                                        alignment: Alignment.center,

                                        child: Column(
                                          children: [
                                            new Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                              children: [
                                                new Container(
                                                  margin: !(widget.model.isFromLiveFinish??false)?EdgeInsets.only(left: 10):EdgeInsets.only(left: 10,top: 4,bottom:4),
                                                  alignment: Alignment.centerLeft,
                                                  child: Text(
                                                    'Team ' + widget.team.teamnumber.toString(),
                                                    style: TextStyle(
                                                        fontFamily: 'noway',
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                                !(widget.model.isFromLiveFinish??false)?new Container(
                                                  margin: EdgeInsets.only(right: 7),
                                                  child: new Row(
                                                    children: [
                                                      new GestureDetector(
                                                        behavior: HitTestBehavior.translucent,
                                                        child: Padding(
                                                          padding: const EdgeInsets.all(5),
                                                          child: Row(
                                                            children: [
                                                              Image(
                                                                image: AssetImage(
                                                                  AppImages.teamEditIcon,), height: 12,
                                                                color: LinkTextColor,
                                                              ),
                                                              SizedBox(width: 5,),
                                                              Text(
                                                                'Edit',
                                                                style: TextStyle(
                                                                    fontFamily: 'noway',
                                                                    color: Colors.black,
                                                                    fontWeight: FontWeight.w400,
                                                                    fontSize: 12),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        onTap: (){
                                                          widget.model.selectedList=widget.team.players;
                                                          widget.model.teamId=widget.team.teamid;
                                                          widget.model.teamname='Team '+widget.team.teamnumber.toString();
                                                          widget.model.isFromEditOrClone=true;
                                                          navigateToCreateTeam(context, widget.model);
                                                        },
                                                      ),
                                                      new GestureDetector(
                                                        behavior: HitTestBehavior.translucent,
                                                        child: Padding(
                                                          padding: const EdgeInsets.all(5.0),
                                                          child: Row(
                                                            children: [
                                                              Image(
                                                                image: AssetImage(
                                                                  AppImages.teamViewIcon,),height: 12,
                                                                color: LinkTextColor,),
                                                              SizedBox(width: 5,),
                                                              Text(
                                                                'Preview',
                                                                style: TextStyle(
                                                                    fontFamily: 'noway',
                                                                    color: Colors.black,
                                                                    fontWeight: FontWeight.w400,
                                                                    fontSize: 12),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        onTap: (){
                                                          openPreview();
                                                        },
                                                      ),
                                                      new GestureDetector(
                                                        behavior: HitTestBehavior.translucent,
                                                        child: Padding(
                                                          padding: const EdgeInsets.all(5.0),
                                                          child: Row(
                                                            children: [
                                                              Image(
                                                                image: AssetImage(
                                                                  AppImages.copyIcon,),height: 12,
                                                                color: LinkTextColor,),
                                                              SizedBox(width: 5,),
                                                              Text(
                                                                'Clone',
                                                                style: TextStyle(
                                                                    fontFamily: 'noway',
                                                                    color: Colors.black,
                                                                    fontWeight: FontWeight.w400,
                                                                    fontSize: 12),
                                                              ),
                                                            ],
                                                          ),
                                                        ),
                                                        onTap: (){
                                                          widget.model.selectedList=widget.team.players;
                                                          widget.model.teamId=0;
                                                          widget.model.teamname='Team '+widget.team.teamnumber.toString();
                                                          widget.model.isFromEditOrClone=true;
                                                          navigateToCreateTeam(context, widget.model);
                                                        },
                                                      ),


                                                    ],
                                                  ),
                                                ):new Container(),
                                              ],
                                            ),
                                            Divider(thickness: 0.5,color: bordercolor),
                                          ],
                                        ),
                                      ),

                                      new Container(
                                        // height: 90,
                                        width: MediaQuery
                                            .of(context)
                                            .size
                                            .width,
                                        
                                        margin: EdgeInsets.only(top: 5,right: 5,left: 5),
                                        child: new Row(
                                          mainAxisAlignment: widget.model.isFromLiveFinish==true?MainAxisAlignment.spaceEvenly:MainAxisAlignment.spaceBetween,
                                          children: [
                                            (widget.model.isFromLiveFinish==true)?Container(
                                            
                                              width: 90,
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [
                                                  Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    children: [
                                                      Text('Points',style: TextStyle(
                                                          color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold
                                                      ),),
                                                      SizedBox(height: 5,),
                                                      Text(widget.team.points.toString(),style: TextStyle(
                                                          color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold
                                                      ),),                                    // Text(widget.model)
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ):Text(''),
                                            widget.model.isFromLiveFinish==true?SizedBox(width: 50,):Container(),
                                            new Container(

                                              width: 170,
                                              alignment: Alignment.center,
                                              child: new Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  new Container(
                                                    margin: EdgeInsets.only(left: 10, right: 5),
                                                    child: Column(

                                                      children: [
                                                        new Stack(
                                                          children: [
                                                            new Stack(
                                                              alignment: Alignment.topCenter,
                                                              children: [
                                                                new Container(
                                                                  height: 60,
                                                                  width: 60,
                                                                  child: CachedNetworkImage(
                                                                    imageUrl: widget.team
                                                                        .captainImage(),
                                                                    placeholder: (context,
                                                                        url) => new Image.asset(
                                                                        AppImages
                                                                            .defaultAvatarIcon),
                                                                    errorWidget: (context, url,
                                                                        error) => new Image.asset(
                                                                        AppImages
                                                                            .defaultAvatarIcon),
                                                                    fit: BoxFit.fill,
                                                                  ),
                                                                ),
                                                                new Container(
                                                                  alignment: Alignment
                                                                      .bottomCenter,
                                                                  height: 80,
                                                                  child: new Container(
                                                                    height: 20,
                                                                    width: 70,
                                                                    margin: EdgeInsets.only(
                                                                        top: 2),
                                                                    alignment: Alignment.center,
                                                                    child: new Column(
                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                      children: [
                                                                        new Flexible(child: new Text(
                                                                          widget.team.captainName(),
                                                                          overflow: TextOverflow.ellipsis,
                                                                          style: TextStyle(
                                                                            fontFamily: 'noway',
                                                                            fontSize: 12,
                                                                            fontWeight: FontWeight
                                                                                .w400,
                                                                            color: Colors.black,),
                                                                          textAlign: TextAlign
                                                                              .center,
                                                                        ))
                                                                      ],),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            new Container(
                                                              height: 24,
                                                              width: 24,
                                                              alignment: Alignment.center,
                                                              decoration: BoxDecoration(
                                                                  border: Border.all(
                                                                      color: Themecolor),
                                                                  borderRadius: BorderRadius
                                                                      .circular(12),
                                                                  color: Themecolor
                                                              ),
                                                              child: new Text('C',
                                                                style: TextStyle(
                                                                    fontFamily: 'noway',
                                                                    color: Colors.white,
                                                                    fontWeight: FontWeight.w500,
                                                                    fontSize: 10),
                                                                textAlign: TextAlign.center,),
                                                            ),
                                                          ],
                                                        ),

                                                      ],
                                                    ),
                                                  ),
                                                  new Container(
                                                    margin: EdgeInsets.only(left: 5, right: 0),
                                                    child: Column(
                                                      children: [
                                                        new Stack(
                                                          children: [
                                                            new Stack(
                                                              alignment: Alignment.topCenter,
                                                              children: [
                                                                new Container(
                                                                  height: 60,
                                                                  width: 60,
                                                                  child: CachedNetworkImage(
                                                                    imageUrl: widget.team
                                                                        .vcCaptainImage(),
                                                                    placeholder: (context,
                                                                        url) => new Image.asset(
                                                                        AppImages
                                                                            .defaultAvatarIcon),
                                                                    errorWidget: (context, url,
                                                                        error) => new Image.asset(
                                                                        AppImages
                                                                            .defaultAvatarIcon),
                                                                    fit: BoxFit.fill,
                                                                  ),
                                                                ),
                                                                new Container(
                                                                  alignment: Alignment
                                                                      .bottomCenter,
                                                                  height: 80,
                                                                  child: new Container(
                                                                    height: 20,
                                                                    width: 70,
                                                                    margin: EdgeInsets.only(
                                                                        top: 2),
                                                                    alignment: Alignment.center,
                                                                    child: new Column(
                                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                                      children: [
                                                                        new Flexible(child: new Text(
                                                                          widget.team
                                                                              .vcCaptainName(),
                                                                          overflow: TextOverflow.ellipsis,
                                                                          style: TextStyle(
                                                                            fontFamily: 'noway',
                                                                            fontSize: 12,
                                                                            fontWeight: FontWeight
                                                                                .w400,
                                                                            color: Colors.black,),
                                                                          textAlign: TextAlign
                                                                              .center,
                                                                        ))
                                                                      ],
                                                                    ),
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            new Container(
                                                              height: 24,
                                                              width: 24,
                                                              alignment: Alignment.center,
                                                              decoration: BoxDecoration(
                                                                  border: Border.all(
                                                                      color: Colors.white),
                                                                  borderRadius: BorderRadius
                                                                      .circular(12),
                                                                  color: yellowdark
                                                              ),
                                                              child: new Text('VC',
                                                                style: TextStyle(
                                                                    fontFamily: 'noway',
                                                                    color: Colors.white,
                                                                    fontWeight: FontWeight.w500,
                                                                    fontSize: 10),
                                                                textAlign: TextAlign.center,),
                                                            ),
                                                          ],
                                                        ),

                                                      ],
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            widget.model.isFromLiveFinish==true?Container():  Expanded(
                                              child: Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  Column(
                                                    // crossAxisAlignment: CrossAxisAlignment
                                                    //     .center,
                                                    // mainAxisSize: MainAxisSize.min,
                                                    children: [
                                                      Text(
                                                        widget.team.team1_name!,
                                                        style: TextStyle(
                                                            fontFamily: 'noway',
                                                            color: Colors.black,
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: 14),
                                                      ),
                                                      SizedBox(height: 5,),
                                                      Text(
                                                        "${widget.team.team1_player_count.toString()}",
                                                        style: TextStyle(
                                                            fontFamily: 'noway',
                                                            color: Colors.black,
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: 14),
                                                      ),
                                                    ],
                                                  ),
                                                  Container(padding : EdgeInsets.all(8),decoration: BoxDecoration(shape: BoxShape.circle,color: yellowdark),child : Text('Vs', style: TextStyle(color: Colors.white,fontSize: 14, fontWeight: FontWeight.bold))),
                                                  Column(

                                                    children: [
                                                      Text(
                                                        widget.team.team2_name!,
                                                        style: TextStyle(
                                                            fontFamily: 'noway',
                                                            color: Colors.black,
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: 14),
                                                      ),
                                                      SizedBox(height: 5,),
                                                      Text(
                                                        "${widget.team.team2_player_count.toString()}",
                                                        style: TextStyle(
                                                            fontFamily: 'noway',
                                                            color: Colors.black,
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: 14),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                            // !(widget.model.isFromLiveFinish??false)?new Container(
                                            //   margin: EdgeInsets.only(right: 10,top: 5,bottom: 5),
                                            //   child: new Column(
                                            //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            //     children: [
                                            //       new GestureDetector(
                                            //         behavior: HitTestBehavior.translucent,
                                            //         child: new Container(
                                            //           width: 80,
                                            //           decoration: BoxDecoration(
                                            //               color: Color(0xFFf7f7f7),
                                            //               borderRadius: BorderRadius.circular(3)
                                            //           ),
                                            //
                                            //           child:Padding(
                                            //             padding: const EdgeInsets.only(right: 10,left: 10,top: 5,bottom: 5),
                                            //             child: Row(
                                            //               children: [
                                            //                 Container(
                                            //                   height: 10,
                                            //                   width: 10,
                                            //                   child: Image(
                                            //                     image: AssetImage(
                                            //                       AppImages.teamEditIcon,),
                                            //                     color: Colors.grey,),
                                            //                 ),
                                            //                 SizedBox(width: 5,),
                                            //                 Text('Edit',style: TextStyle(
                                            //                     fontSize: 11,color: Colors.grey
                                            //                 ),)
                                            //               ],
                                            //             ),
                                            //           ),
                                            //         ),
                                            //         onTap: (){
                                            //           widget.model.selectedList=widget.team.players;
                                            //           widget.model.teamId=widget.team.teamid;
                                            //           widget.model.teamName='Team '+widget.team.teamnumber.toString();
                                            //           widget.model.isFromEditOrClone=true;
                                            //           navigateToCreateTeam(context, widget.model);
                                            //         },
                                            //       ),
                                            //         SizedBox(height: 5,),
                                            //
                                            //       new GestureDetector(
                                            //         behavior: HitTestBehavior.translucent,
                                            //         child: Container(
                                            //           width: 80,
                                            //           decoration: BoxDecoration(
                                            //               color: Color(0xFFf7f7f7),
                                            //               borderRadius: BorderRadius.circular(3)
                                            //           ),
                                            //           child: Padding(
                                            //             padding: const EdgeInsets.only(right: 10,left: 10,top: 5,bottom: 5),
                                            //             child: Row(
                                            //               children: [
                                            //                 new Container(
                                            //
                                            //                   height: 10,
                                            //                   width: 10,
                                            //                   child: Image(
                                            //                     image: AssetImage(
                                            //                       AppImages.copyIcon,),
                                            //                     color: Colors.grey,),
                                            //                 ),
                                            //                 SizedBox(width: 5,),
                                            //                 Text('Clone',style: TextStyle(
                                            //                     fontSize: 11,color: Colors.grey
                                            //                 ),)
                                            //               ],
                                            //             ),
                                            //           ),
                                            //         ),
                                            //         onTap: (){
                                            //           widget.model.selectedList=widget.team.players;
                                            //           widget.model.teamId=0;
                                            //           widget.model.teamName='Team '+widget.team.teamnumber.toString();
                                            //           widget.model.isFromEditOrClone=true;
                                            //           navigateToCreateTeam(context, widget.model);
                                            //         },
                                            //       ),
                                            //       SizedBox(height: 5,),
                                            //
                                            //
                                            //
                                            //       new GestureDetector(
                                            //         behavior: HitTestBehavior.translucent,
                                            //         child: Container(
                                            //           width: 80,
                                            //           decoration: BoxDecoration(
                                            //               color: Color(0xFFf7f7f7),
                                            //               borderRadius: BorderRadius.circular(3)
                                            //           ),
                                            //           child: Padding(
                                            //             padding: const EdgeInsets.only(right: 10,left: 10,top: 5,bottom: 5),
                                            //             child: Row(
                                            //               children: [
                                            //                 new Container(
                                            //                   height: 12,
                                            //                   width: 12,
                                            //                   child: Image(
                                            //                     image: AssetImage(
                                            //                       AppImages.teamViewIcon,),
                                            //                     color: Colors.grey,),
                                            //                 ),
                                            //                 SizedBox(width: 5,),
                                            //                 Text('Preview',style: TextStyle(
                                            //                     fontSize: 11,color: Colors.grey
                                            //                 ),)
                                            //               ],
                                            //             ),
                                            //           ),
                                            //         ),
                                            //         onTap: (){
                                            //           openPreview();
                                            //         },
                                            //       ),
                                            //
                                            //     ],
                                            //   ),
                                            // ):new Container(),


                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),

                                
                              ],
                            ),
                          ),
                        ),
                        new Container(

                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          height: 40,
                          child: new Container(
                            decoration: BoxDecoration(color:TextFieldColor,borderRadius: BorderRadius.only(bottomRight: Radius.circular(12),bottomLeft: Radius.circular(12)), ),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                new Container(
                                  // margin: EdgeInsets.only(left: 10),
                                  alignment: Alignment.center,
                                  child: Text(
                                    keeperCount(),
                                    style: TextStyle(
                                        fontFamily: 'noway',
                                        color: Colors.black54,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12),
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(left: 10),
                                  alignment: Alignment.center,
                                  child: Text(
                                    batsmanCount(),
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12),
                                  ),
                                ),
                                if(widget.model.sportKey!=AppConstants.TAG_HANDBALL&&widget.model.sportKey!=AppConstants.TAG_KABADDI)
                                  new Container(
                                    margin: EdgeInsets.only(left: 10),
                                    alignment: Alignment.center,
                                    child: Text(
                                      allRounderCount(),
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12),
                                    ),
                                  ),
                                new Container(
                                  margin: EdgeInsets.only(left: 10),
                                  alignment: Alignment.center,
                                  child: Text(
                                    bolwerCount(),
                                    style: TextStyle(
                                        color: Colors.black54,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12),
                                  ),
                                ),
                                if(widget.model.sportKey==AppConstants.TAG_BASKETBALL)
                                  new Container(
                                    margin: EdgeInsets.only(left: 10),
                                    alignment: Alignment.center,
                                    child: Text(
                                      CCount(),
                                      style: TextStyle(
                                          color: Colors.black54,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12),
                                    ),
                                  ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  )),
              widget.isForJoinContest&&widget.team.is_joined!=1 ? new Container(
                width: 30,
                child: Radio(
                  value: true,
                  groupValue: widget.team.isSelected??false,
                  activeColor: greenColor,
                  onChanged: (value) {
                    if(widget.isForJoinContest){
                      widget.model.teamClickListener!(widget.index);
                    }
                  },
                ),
              ) : new Container(width: widget.team.is_joined==1?30:0,),
            ],
          ),
        ),),
        onTap: (){
          if(widget.isForJoinContest&&widget.team.is_joined!=1){
            widget.model.teamClickListener!(widget.index);
          }
          else{
            openPreview();
          }
        },
      ),
    );
  }

  String keeperCount() {
    int i = 0;
    String name = "WK ";
    for (Player player in widget.team.players!) {
      if (widget.model.sportKey=="CRICKET") {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_KEEP.toLowerCase())
          i++;
        name = "WK ";
      } else if (widget.model.sportKey=="FOOTBALL" || widget.model.sportKey==AppConstants.TAG_HANDBALL || widget.model.sportKey==AppConstants.TAG_HOCKEY) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_GK.toLowerCase())
          i++;
        name = "GK ";
      } else if (widget.model.sportKey==AppConstants.TAG_KABADDI) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_DEF.toLowerCase())
          i++;
        name = "DEF ";
      } else if (widget.model.sportKey==AppConstants.TAG_BASEBALL) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_OF.toLowerCase())
          i++;
        name = "OF ";
      } else {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_PG.toLowerCase())
          i++;
        name = "PG ";
      }
    }
    return name + "(" + i.toString() + ")";
  }

  String batsmanCount() {
    int i = 0;
    String name = "BAT ";
    for (Player player in widget.team.players!) {
      if (widget.model.sportKey=="CRICKET") {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_BAT.toLowerCase())
          i++;
        name = "BAT ";
      } else if (widget.model.sportKey=="FOOTBALL" || widget.model.sportKey==AppConstants.TAG_HANDBALL || widget.model.sportKey==AppConstants.TAG_HOCKEY ) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_DEF.toLowerCase())
          i++;
        name = "DEF ";
      } else if (widget.model.sportKey==AppConstants.TAG_BASEBALL) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_IF.toLowerCase())
          i++;
        name = "IF ";
      } else if (widget.model.sportKey==AppConstants.TAG_KABADDI) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_ALL_R.toLowerCase())
          i++;
        name = "ALL ";
      } else {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_SG.toLowerCase())
          i++;
        name = "SG ";
      }
    }
    return name + "(" + i.toString() + ")";
  }
  String allRounderCount() {
    int i = 0;
    String name = "AR ";
    for (Player player in widget.team.players!) {
      if (widget.model.sportKey=="CRICKET") {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_ALL_R.toLowerCase())
          i++;
        name = "AR ";
      } else if (widget.model.sportKey=="FOOTBALL" || widget.model.sportKey==AppConstants.TAG_HANDBALL || widget.model.sportKey==AppConstants.TAG_HOCKEY || widget.model.sportKey==AppConstants.TAG_KABADDI) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_MID.toLowerCase())
          i++;
        name = "MID ";
      } else if (widget.model.sportKey==AppConstants.TAG_BASEBALL) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_PITCHER.toLowerCase())
          i++;
        name = "P ";
      } else {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_SF.toLowerCase())
          i++;
        name = "SF ";
      }
    }
    return name + "(" + i.toString() + ")";
  }
  String bolwerCount() {
    int i = 0;
    String name = "BOWL ";
    for (Player player in widget.team.players!) {
      if (widget.model.sportKey=="CRICKET") {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_BOL.toLowerCase())
          i++;
        name = "BOWL ";
      } else if (widget.model.sportKey=="FOOTBALL" || widget.model.sportKey==AppConstants.TAG_HANDBALL || widget.model.sportKey==AppConstants.TAG_HOCKEY) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_ST.toLowerCase())
          i++;
        name = "ST ";
      } else if (widget.model.sportKey==AppConstants.TAG_BASEBALL) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_CATCHER.toLowerCase())
          i++;
        name = "C ";
      } else if (widget.model.sportKey==AppConstants.TAG_KABADDI) {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_RD.toLowerCase())
          i++;
        name = "RAIDER ";
      } else {
        if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_PF.toLowerCase())
          i++;
        name = "PF ";
      }
    }
    return name + "(" + i.toString() + ")";
  }
  String CCount() {
    int i = 0;
    for (Player player in widget.team.players!) {
      if (player.role!.toLowerCase()==AppConstants.KEY_PLAYER_ROLE_C.toLowerCase())
        i++;
    }
    return "C " + "(" + i.toString() + ")";
  }
  void openPreview(){
    List<Player> selectedWkList = [];
    List<Player> selectedBatLiSt =[];
    List<Player> selectedArList = [];
    List<Player> selectedBowlList = [];
    List<Player> selectedcList = [];
    for (Player player in widget.team.players!) {
      if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_KEEP:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_OF:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_GK:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppConstants.KEY_PLAYER_ROLE_GK:widget.model.sportKey==AppConstants.TAG_KABADDI?AppConstants.KEY_PLAYER_ROLE_DEF:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_PG:AppConstants.KEY_PLAYER_ROLE_GK).toString().toLowerCase())
        selectedWkList.add(player);
      else if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_BAT:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_IF:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_DEF:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppConstants.KEY_PLAYER_ROLE_DEF:widget.model.sportKey==AppConstants.TAG_KABADDI?AppConstants.KEY_PLAYER_ROLE_ALL_R:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_SG:AppConstants.KEY_PLAYER_ROLE_DEF).toString().toLowerCase())
        selectedBatLiSt.add(player);
      else if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_ALL_R:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_PITCHER:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_MID:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppConstants.KEY_PLAYER_ROLE_ST:widget.model.sportKey==AppConstants.TAG_KABADDI?AppConstants.KEY_PLAYER_ROLE_RD:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_SF:AppConstants.KEY_PLAYER_ROLE_MID).toString().toLowerCase())
        selectedArList.add(player);
      else if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_BOL:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_CATCHER:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_STR:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_PF:AppConstants.KEY_PLAYER_ROLE_ST).toString().toLowerCase())
        selectedBowlList.add(player);
      else if (player.role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_C.toString().toLowerCase())
        selectedcList.add(player);
    }
    widget.model.selectedWkList=selectedWkList;
    widget.model.selectedBatLiSt=selectedBatLiSt;
    widget.model.selectedArList=selectedArList;
    widget.model.selectedBowlList=selectedBowlList;
    widget.model.selectedcList=selectedcList;
    widget.model.isForLeaderBoard=false;
    widget.model.teamname='Team '+widget.team.teamnumber.toString();
    navigateToTeamPreview(context,widget.model, );
  }
}