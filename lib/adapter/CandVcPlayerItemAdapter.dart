import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../appUtilities/app_colors.dart';
import '../appUtilities/app_images.dart';
import '../appUtilities/app_navigator.dart';
import '../repository/model/team_response.dart';


class CandVcPlayerItemAdapter extends StatefulWidget {
  Player player;
  Function captainViceCaptainListener;
  int index;
  bool displayRole;
  String role;
  String? matchKey;
  String? sportKey;
  int? fantasyType;
  int? slotId;
  CandVcPlayerItemAdapter(this.player,this.captainViceCaptainListener,this.index,this.displayRole,this.role,this.matchKey,this.sportKey,this.fantasyType,this.slotId);
  @override
  _CandVcPlayerItemAdapterState createState() => new _CandVcPlayerItemAdapterState();
}

class _CandVcPlayerItemAdapterState extends State<CandVcPlayerItemAdapter> {

  bool isAnnounced=true;
  bool isLastPlayed=false;
  @override
  void initState() {
    super.initState();
    print("team name");
    print( widget.player.team);
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      color: Colors.white,
      child: new Column(
        children: [
          createHeader1(),
          new Container(
            color: TextFieldColor,
            margin: EdgeInsets.only(bottom: 5),
            padding: EdgeInsets.only(left: 5,right: 0,top: 10,bottom: 5),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new Row(
                  children: [
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: new Container(
                        margin: EdgeInsets.only(right: 5),
                        child: new Stack(
                          // alignment: Alignment.bottomCenter,
                          clipBehavior: Clip.none, children:[
                            new Container(

                              alignment: Alignment.center,
                              width: 70,
                              child: new Container(
                                child: new Container(
                                  alignment: Alignment.topCenter,
                                  height: 55,
                                  width: 60,
                                  child: CachedNetworkImage(
                                    imageUrl: widget.player.image!,
                                    placeholder: (context,url)=> new Image.asset(AppImages.defaultAvatarIcon),
                                    errorWidget: (context, url, error) => new Image.asset(AppImages.defaultAvatarIcon),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                              ),
                            ),
                            new Row(
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(left: 5,top: 45),
                                  height: 16,
                                  width: 40,
                                  decoration: BoxDecoration(
                                      color: widget.player.team=='team1'?Colors.white:Colors.black,
                                      borderRadius: BorderRadius.circular(3),border: widget.player.team=='team1'?Border.all(color: primaryColor):Border.all(color: Colors.black)
                                  ),

                                 alignment: Alignment.center,
                                  child: Center(
                                    child: new Text(
                                      widget.player.teamcode!,
                                      overflow: TextOverflow.clip,
                                      style: TextStyle(fontSize: 10,fontWeight: FontWeight.normal,color: widget.player.team=='team1'?primaryColor:Colors.white,),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                                // new Container(
                                //   child: new Card(
                                //     elevation: 0,
                                //     margin: EdgeInsets.all(0),
                                //     color: orangeColor,
                                //     shape : RoundedRectangleBorder(
                                //         borderRadius : BorderRadius.only(bottomRight: Radius.circular(2),topRight: Radius.circular(2))
                                //     ),
                                //     child: new Container(
                                //       height: 14,
                                //       padding: EdgeInsets.only(left: 5,right: 5),
                                //       alignment: Alignment.center,
                                //       child: new Text(
                                //         widget.player.short_role!,
                                //         style: TextStyle(fontSize: 10,fontWeight: FontWeight.normal,color: Colors.white,),
                                //         textAlign: TextAlign.center,
                                //       ),
                                //     ),
                                //   ),
                                // ),
                              ],
                            )
                          ],
                        ),
                      ),
                      onTap: (){
                        navigateToPlayerInfo(context,widget.matchKey,widget.player.id,widget.player.name,widget.player.team,widget.player.image,widget.player.isSelected,widget.index,0,widget.sportKey,widget.fantasyType,widget.slotId,'1',null,widget.role);
                      },
                    ),
                    new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        new Text(
                          widget.player.getShortName(),
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 13,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    )
                  ],
                ),
                new Row(
                  children: [
                    new Container(
                      width: 50,
                      alignment: Alignment.centerLeft,
                      child: Text(
                        widget.player.series_points,
                        style: TextStyle(
                            color: textGrey,
                            fontWeight: FontWeight.w400,
                            fontSize: 12),
                      ),
                    ),
                    new Column(
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            height: 28,
                            width: 28,
                            margin: EdgeInsets.only(bottom: 2),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                border: Border.all(color: widget.player.isCaptain??false?yellowdark:textGrey),
                                borderRadius: BorderRadius.circular(14),
                                color: widget.player.isCaptain??false?yellowdark:Colors.white
                            ),
                            child: new Text('C',
                              style: TextStyle(
                                  color: widget.player.isCaptain??false?Colors.white:Colors.grey,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12),textAlign: TextAlign.center,),
                          ),
                          onTap: (){
                            widget.captainViceCaptainListener(true,widget.index);
                          },
                        ),
                        new Container(
                          width: 50,
                          alignment: Alignment.center,
                          child: Text(
                            widget.player.captain_selected_by!,
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w400,
                                fontSize: 11),
                          ),
                        ),
                      ],
                    ),
                    new Column(
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            height: 28,
                            width: 28,
                            margin: EdgeInsets.only(bottom: 2),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                border: Border.all(color: widget.player.isVcCaptain??false?Themecolor:textGrey),
                                borderRadius: BorderRadius.circular(14),
                                color: widget.player.isVcCaptain??false?Themecolor:Colors.white
                            ),
                            child: new Text('VC',
                              style: TextStyle(
                                  color: widget.player.isVcCaptain??false?Colors.white:Colors.grey,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 12),textAlign: TextAlign.center,),
                          ),
                          onTap: (){
                            widget.captainViceCaptainListener(false,widget.index);
                          },
                        ),
                        new Container(
                          width: 50,
                          alignment: Alignment.center,
                          child: Text(
                            widget.player.vice_captain_selected_by!,
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w400,
                                fontSize: 11),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          createHeader(),
        ],
      ),
    );
  }
  Widget createHeader1(){
    if(widget.index==0) {
      return new Container(
        height: 40,
        decoration: new BoxDecoration(
          // gradient: new LinearGradient(
          //   colors: [
          //     const Color(0xff787878),
          //     const Color(0xffc9c8c6),
          //   ],
          //   begin: const FractionalOffset(0.0, 0.0),
          //   end: const FractionalOffset(1.0, 0.0),
          // ),
            color: Colors.white
        ),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            new Container(
              margin: EdgeInsets.only(left: 10),
              child: Text(
                widget.player.display_role!,
                style: TextStyle(

                    color: Color(0xFF747272),
                    fontWeight: FontWeight.w500,
                    fontSize: 14),
              ),
            ),
          ],
        ),
      );
    }
    return new Container();
  }
  Widget createHeader(){
    if(widget.displayRole) {
      return new Container(
        height: 35,
        decoration: new BoxDecoration(
            //color: Colors.grey.shade300
            color: Color(0xFFf8f9fb)
        ),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            new Container(
              margin: EdgeInsets.only(left: 10),
              child: Text(
                widget.role,
                style: TextStyle(
                    color: Color(0xFF747272),
                    fontWeight: FontWeight.w500,
                    fontSize: 14),
              ),
            ),
          ],
        ),
      );
    }
    return new Container();
  }
}