import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';


import '../appUtilities/app_colors.dart';
import '../appUtilities/app_images.dart';
import '../appUtilities/app_navigator.dart';
import '../appUtilities/method_utils.dart';
import '../dataModels/GeneralModel.dart';
import '../repository/model/refresh_score_response.dart';

class LiveFinishContestAdapter extends StatefulWidget {
  GeneralModel model;
  LiveFinishedContestData contest;
  LiveFinishContestAdapter(this.model,this.contest);
  @override
  _LiveFinishContestAdapterState createState() => new _LiveFinishContestAdapterState();
}

class _LiveFinishContestAdapterState extends State<LiveFinishContestAdapter>{


  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      child: Opacity(opacity: widget.contest.challenge_status=='canceled'?0.4:1.0,child: new Stack(
        alignment: Alignment.bottomCenter,
        children: [

          Card(
            elevation: 0,
            clipBehavior: Clip.antiAlias,
            // elevation: 5,
            shape: RoundedRectangleBorder(
                side: BorderSide(
                    color: bordercolor
                ),
                borderRadius: BorderRadius.circular(8)),
            margin: EdgeInsets.only(left: 10,right: 10,top: 8,bottom: widget.contest.challenge_status=='canceled'?28:8),
            child: Container(
              padding: EdgeInsets.all(0),
              child: new Container(
                child: new Column(
                  children: [
                    // new Container(
                    //   margin: EdgeInsets.only(top: 5),
                    //   child: new Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       // new Container(
                    //       //   height: 20,
                    //       //   padding: EdgeInsets.only(left: 5,right: 15),
                    //       //   decoration: BoxDecoration(
                    //       //       image: DecorationImage(
                    //       //           image: AssetImage(AppImages.winnersBgIcon),
                    //       //           fit: BoxFit.fill)),
                    //       //   child: new Row(
                    //       //     children: [
                    //       //       new Container(
                    //       //         margin: EdgeInsets.only(right: 5),
                    //       //         height: 15,
                    //       //         width: 15,
                    //       //         child: new Image.asset(AppImages.winnersIcon,
                    //       //           fit: BoxFit.fill,color: orangeColor,),
                    //       //       ),
                    //       //       new Container(
                    //       //         height: 30,
                    //       //         alignment: Alignment.centerLeft,
                    //       //         child: Text(
                    //       //           widget.contest.totalwinners.toString(),
                    //       //           style: TextStyle(
                    //       //               color: orangeColor,
                    //       //               fontWeight: FontWeight.w800,
                    //       //               fontSize: 13),
                    //       //         ),
                    //       //       ),
                    //       //       new Container(
                    //       //         height: 30,
                    //       //         alignment: Alignment.centerLeft,
                    //       //         child: Text(
                    //       //           ' Winners',
                    //       //           style: TextStyle(
                    //       //               color: Colors.black,
                    //       //               fontWeight: FontWeight.w400,
                    //       //               fontSize: 11),
                    //       //         ),
                    //       //       )
                    //       //     ],
                    //       //   ),
                    //       //
                    //       // ),
                    //       new Container(
                    //         margin: EdgeInsets.only(right: 10),
                    //         child:new Row(
                    //           children: [
                    //             new Row(
                    //               children: [
                    //                 new Container(
                    //                   margin: EdgeInsets.only(left: 7,right: 5),
                    //                   height: 12,
                    //                   width: 12,
                    //                   child: Image(
                    //                     image: AssetImage(
                    //                       AppImages.winnerMedalIcon,),color: Colors.grey,),
                    //                 ),
                    //                 new Text('₹'+widget.contest.first_rank_prize.toString(),
                    //                     style: TextStyle(
                    //                         fontFamily: 'noway',
                    //                         color: Colors.grey,
                    //                         fontWeight: FontWeight.w500,
                    //                         fontSize: 12)),
                    //               ],
                    //             ),
                    //             widget.contest.multi_entry==1?new Row(
                    //               children: [
                    //                 new Container(
                    //                   margin: EdgeInsets.only(left: 7,right: 2),
                    //                   height: 14,
                    //                   width: 14,
                    //                   alignment: Alignment.center,
                    //                   decoration: BoxDecoration(
                    //                       border: Border.all(color: Colors.grey),
                    //                       borderRadius: BorderRadius.circular(2)
                    //                   ),
                    //                   child: new Text('M',
                    //                       style: TextStyle(
                    //                           fontFamily: 'noway',
                    //                           color: Colors.grey,
                    //                           fontWeight: FontWeight.w500,
                    //                           fontSize: 10)),
                    //                 ),
                    //                 new Text('Up to '+widget.contest.max_multi_entry_user.toString(),
                    //                     style: TextStyle(
                    //                         fontFamily: 'noway',
                    //                         color: Colors.grey,
                    //                         fontWeight: FontWeight.w500,
                    //                         fontSize: 10)),
                    //
                    //               ],
                    //             ):new Container(),
                    //             widget.contest.confirmed_challenge==1?new Container(
                    //               margin: EdgeInsets.only(left: 5,right: 0),
                    //               height: 13,
                    //               width: 13,
                    //               child: Image(
                    //                 image: AssetImage(
                    //                   AppImages.confirmIcon,),color: Colors.grey,),
                    //             ):new Container(),
                    //           ],
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),

                    widget.contest.is_champion==1?Container(
                      margin: EdgeInsets.only(left: 8,right: 14,top:8),
                      child: Column(
                        children: [
                          new Row(
                            mainAxisAlignment:MainAxisAlignment.end ,
                            children: [
                              new Row(
                                children: [
                                  // new Container(
                                  //   margin: EdgeInsets.only(left: 7,right: 2),
                                  //   height: 14,
                                  //   padding: EdgeInsets.only(left: 2,right: 2),
                                  //   alignment: Alignment.center,
                                  //   decoration: BoxDecoration(
                                  //       border: Border.all(color: Colors.grey),
                                  //       borderRadius: BorderRadius.circular(2)
                                  //   ),
                                  //   child: new Text('WD',
                                  //       style: TextStyle(
                                  //           fontFamily: 'noway',
                                  //           color: Colors.grey,
                                  //           fontWeight: FontWeight.w500,
                                  //           fontSize: 10)),
                                  // ),

                                  widget.contest.confirmed_challenge==1?new Container(
                                    margin: EdgeInsets.only(left: 7,right: 2),
                                    height: 14,
                                    width: 14,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey),
                                        borderRadius: BorderRadius.circular(2)
                                    ),
                                    child: new Text('c',
                                        style: TextStyle(
                                            fontFamily: 'noway',
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 11)),
                                  ):new Container(),

                                  widget.contest.multi_entry==1?new Row(
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(left: 7,right: 10),
                                        height: 13,
                                        // width: 14,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            border: Border.all(color: Colors.grey),
                                            borderRadius: BorderRadius.circular(2)
                                        ),
                                        child: Row(
                                          children: [
                                            Container(
                                              decoration:BoxDecoration(
                                                // border: Border(
                                                //     right: BorderSide(color: Colors.grey)
                                                // )
                                              ),
                                              child: new Text(' M ',
                                                  style: TextStyle(
                                                      fontFamily: 'noway',
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 10)),
                                            ),
                                            // new Text(' '+widget.contest.max_multi_entry_user!.toString()+" ",
                                            //     style: TextStyle(
                                            //         fontFamily: 'noway',
                                            //         color: Colors.grey,
                                            //         fontWeight: FontWeight.w500,
                                            //         fontSize: 9)
                                            // ),
                                          ],
                                        ),
                                      ),



                                    ],
                                  ):new Container(),
                                ],
                              ),
                              //SizedBox(width: 5,),
                              Text(
                                'Joined Teams: ',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 13),
                              ),
                              new Container(
                                height: 22,
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  widget.contest.maximum_user.toString(),
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 13),
                                ),
                              ),


                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Image.network(widget.contest.champion_player!,fit: BoxFit.contain,scale: 4,),
                              SizedBox(width: 40,),
                              Expanded(child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  new Container(
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      // mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        widget.contest.is_gadget==1?Image.network(widget.contest.gadget_image!,fit: BoxFit.contain,scale: 5,) : Container(
                                          height: 22,
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            '₹'+(widget.contest.is_giveaway_visible_text==0?widget.contest.win_amount.toString():widget.contest.is_giveaway_text!),
                                            style: TextStyle(
                                                color: widget.contest.is_giveaway_visible_text==0?Colors.black:MethodUtils.hexToColor(widget.contest.giveaway_color!),
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14),
                                          ),
                                        ),
                                        // SizedBox(height: 10,),
                                        Text(
                                          'Prize Pool',
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 11),
                                        ),

                                      ],
                                    ),
                                  ),

                                  new Container(
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        new Container(
                                          height: 22,
                                          alignment: Alignment.centerRight,
                                          child: Text(
                                            '₹'+widget.contest.entryfee.toString(),
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 14),
                                          ),
                                        ),
                                        widget.contest.is_gadget==1? SizedBox(height: 10,):Container(),
                                        Text(
                                          'Entry Fee',
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 11),
                                        ),

                                      ],
                                    ),
                                  ),
                                ],
                              ))
                            ],
                          )
                        ],
                      ),
                    ):new Container(
                      margin: EdgeInsets.only(left: 10,right: 10,top: 5),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [

                          new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              // mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Prize Pool',
                                  style: TextStyle(
                                      color: textGrey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 11),
                                ),
                                widget.contest.is_gadget==1?Image.network(widget.contest.gadget_image!,fit: BoxFit.contain,scale: 5,) : Container(
                                  height: 22,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    '₹'+(widget.contest.is_giveaway_visible_text==0?widget.contest.win_amount.toString():widget.contest.is_giveaway_text!),
                                    style: TextStyle(
                                        color: widget.contest.is_giveaway_visible_text==0?Colors.black:MethodUtils.hexToColor(widget.contest.giveaway_color!),
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14),
                                  ),
                                ),
                                // SizedBox(height: 10,),


                              ],
                            ),
                          ),
                          new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                widget.contest.is_gadget==1? SizedBox(height: 10,):Container(),
                                Text(
                                  'Spots',
                                  style: TextStyle(
                                      color: textGrey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 11),
                                ),
                                new Container(
                                  height: 22,
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    widget.contest.maximum_user.toString(),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14),
                                  ),
                                ),


                              ],
                            ),
                          ),
                          new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                SizedBox(height: 10,),
                                Text(
                                  'Entry Fee  ',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 11),
                                ),
                                widget.contest.is_gadget==1? SizedBox(height: 10,):Container(),
                                new Container(
                                  alignment: Alignment.centerRight,
                                  child: new Container(
                                    margin: EdgeInsets.only(top: 5),
                                    height: 25,
                                    width: 50,
                                    decoration: BoxDecoration( color: yellowdark,borderRadius: BorderRadius.circular(40)),
                                    alignment: Alignment.center,
                                    child: Text('₹'+widget.contest.entryfee.toString(),style: TextStyle(
                                        color: Colors.white,fontSize: 12,fontWeight: FontWeight.bold
                                    ),),
                                  ),
                                ),


                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    new Container(

                      height: 35,
                      decoration: BoxDecoration(
                          color: TextFieldColor,
                          border:Border.all(color: TextFieldBorderColor)
                      ),
                      margin: EdgeInsets.only(top:widget.contest.is_champion==1?0: 10),
                      child: new Container(
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Row(
                              children: [
                                widget.contest.challenge_type!='percentage'?new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 7,right: 5),
                                      height: 21,
                                      width: 21,
                                      child: Image(
                                        image: AssetImage(
                                          AppImages.winnerMedalIcon,),),
                                    ),
                                    new Text(widget.contest.first_rank_prize!.toString(),
                                        style: TextStyle(
                                            fontFamily: 'noway',
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12)),

                                  ],
                                ):new Container(),
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(left: 7,right: 2),
                                        height: 14,
                                        width: 14,
                                        child: Image(
                                          image: AssetImage(
                                            AppImages.winnersIcon,),),
                                      ),
                                      new Text(widget.contest.challenge_type=='percentage'?widget.contest.winning_percentage.toString()+'% Win':widget.contest.totalwinners.toString()+' Team Win',
                                          style: TextStyle(
                                              fontFamily: 'noway',
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12)),

                                    ],
                                  ),
                                  onTap: (){
                                    // widget.getWinnerPriceCard(widget.contest.id,widget.contest.win_amount.toString());
                                  },
                                ),


                                widget.contest.is_bonus==1?new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 7,right: 2),
                                      height: 12,
                                      width: 14,
                                      child: Image(
                                        image: AssetImage(
                                          AppImages.bonusIcon,),),
                                    ),
                                    new Text(widget.contest.bonus_percent!,
                                        style: TextStyle(
                                            fontFamily: 'noway',
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 10)),

                                  ],
                                ):new Container()
                              ],
                            ),
                            Visibility(
                              visible: widget.contest.is_champion==1?false:true,
                              child: new Row(
                                children: [
                                  // new Container(
                                  //   margin: EdgeInsets.only(left: 7,right: 2),
                                  //   height: 14,
                                  //   padding: EdgeInsets.only(left: 2,right: 2),
                                  //   alignment: Alignment.center,
                                  //   decoration: BoxDecoration(
                                  //       border: Border.all(color: Colors.grey),
                                  //       borderRadius: BorderRadius.circular(2)
                                  //   ),
                                  //   child: new Text('WD',
                                  //       style: TextStyle(
                                  //           fontFamily: 'noway',
                                  //           color: Colors.grey,
                                  //           fontWeight: FontWeight.w500,
                                  //           fontSize: 10)),
                                  // ),

                                  widget.contest.confirmed_challenge==1?new Container(
                                    margin: EdgeInsets.only(left: 7,right: 2),
                                    height: 14,
                                    width: 14,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        border: Border.all(color: Colors.grey),
                                        borderRadius: BorderRadius.circular(2)
                                    ),
                                    child: new Text('c',
                                        style: TextStyle(
                                            fontFamily: 'noway',
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 11)),
                                  ):new Container(),

                                  widget.contest.multi_entry==1?new Row(
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(left: 7,right: 10),
                                        height: 13,
                                        // width: 14,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            border: Border.all(color: Colors.grey),
                                            borderRadius: BorderRadius.circular(2)
                                        ),
                                        child: Row(
                                          children: [
                                            Container(
                                              decoration:BoxDecoration(
                                                // border: Border(
                                                //     right: BorderSide(color: Colors.grey)
                                                // )
                                              ),
                                              child: new Text(' M ',
                                                  style: TextStyle(
                                                      fontFamily: 'noway',
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 10)),
                                            ),
                                            // new Text(' '+widget.contest.max_multi_entry_user!.toString()+" ",
                                            //     style: TextStyle(
                                            //         fontFamily: 'noway',
                                            //         color: Colors.grey,
                                            //         fontWeight: FontWeight.w500,
                                            //         fontSize: 9)
                                            // ),
                                          ],
                                        ),
                                      ),



                                    ],
                                  ):new Container(),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    widget.contest.winners_zone!.length>0?new Container(
                      color: Color(0xFFfefaef),
                      padding: EdgeInsets.only(top: 10,bottom: 10,left: 10,right: 10),
                      child: new Container(
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Flexible(child: new Container(
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  new Row(
                                    children: [
                                      Flexible(child: new Text(widget.contest.winners_zone![0].team_name!,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            color: Colors.black,

                                            fontSize: 12,
                                          ))),
                                      new Text('(T'+widget.contest.winners_zone![0].team_number.toString()+')',
                                          style: TextStyle(
                                              color: Colors.black,
                                              // fontWeight: FontWeight.w500,
                                              fontSize: 12)),

                                    ],
                                  ),
                                  widget.contest.winners_zone![0].is_winningzone==1&&widget.model.isFromLive!?new Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: new Text('WINNING ZONE',
                                      style: TextStyle(
                                          fontFamily: 'noway',
                                          color: greenColor,
                                          fontSize: 14,
                                          fontWeight:





                                          FontWeight.w500),
                                    ),
                                  ):new Container(),
                                  double.parse(widget.contest.winners_zone![0].amount??'0')>0&&!widget.model.isFromLive!?new Container(
                                    margin: EdgeInsets.only(top: 3),
                                    child: new Text(
                                      'YOU WON: ₹'+widget.contest.winners_zone![0].amount.toString(),
                                      style: TextStyle(
                                          fontFamily: 'noway',
                                          color: greenColor,
                                          fontSize: 11,
                                          fontWeight:
                                          FontWeight.normal),
                                    ),
                                  ):new Container(),
                                ],
                              ),
                            ),flex: 1,),
                            new Flexible(child: new Container(
                              child: new Text(widget.contest.winners_zone![0].points!,
                                  style: TextStyle(
                                      fontFamily: 'noway',
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 13)),
                            ),flex: 1,),
                            new Flexible(child: new Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                new Text('#'+widget.contest.winners_zone![0].rank.toString(),
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 13)),
                                new Container(
                                  alignment: Alignment.center,
                                  child:Icon(Icons.remove,size: 15,),
                                )

                              ],
                            ),flex: 1,),
                          ],
                        ),
                      ),
                    ):new Container(),
                    widget.contest.challenge_status=='canceled'?new Container(
                      color: lightYellow,
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Canceled due to clash',
                          style: TextStyle(
                              fontFamily: 'roboto',
                              color:primaryColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 12),
                        ),
                      ),
                    ):new Container(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),),
      onTap: ()=>{
        if(widget.contest.challenge_status!='canceled'){
          navigateToLiveFinishContestDetails(
              context, widget.contest, widget.model)
        }
      },
    );
  }
}