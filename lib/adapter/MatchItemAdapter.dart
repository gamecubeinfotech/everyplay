import 'package:cached_network_image/cached_network_image.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


import '../GetXController/HomeMatchesGetController.dart';
import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/app_images.dart';
import '../appUtilities/app_navigator.dart';
import '../appUtilities/date_utils.dart';
import '../appUtilities/method_utils.dart';
import '../customWidgets/ScrollingText.dart';
import '../customWidgets/app_circular_loader.dart';
import '../dataModels/GeneralModel.dart';
import '../localStoage/AppPrefrences.dart';
import '../repository/app_repository.dart';
import '../repository/model/banner_response.dart';
import '../repository/model/base_request.dart';
import '../repository/model/base_response.dart';
import '../repository/retrofit/api_client.dart';

class MatchItemAdapter extends StatefulWidget {
  late MatchDetails matchDetails;
  Function onMatchTimeUp;
  int index;
  CustomTimerController controller;
  HomeMatchesGetController getController;
  MatchItemAdapter(this.matchDetails,this.onMatchTimeUp,this.index,this.controller,this.getController);

  @override
  _MatchItemAdapterState createState() => new _MatchItemAdapterState();
}

class _MatchItemAdapterState extends State<MatchItemAdapter>{
  late String userId='0';

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
      })
    });
  }


  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new GestureDetector(
      behavior:
      HitTestBehavior.translucent,
      child: Opacity(
        opacity: widget.matchDetails.is_fixture==1?0.4:1.0,
        child: Stack(
          children: [
            widget.matchDetails.toss!.isNotEmpty?new Container(
              width: MediaQuery.of(context).size.width,
              height: 30,
              margin: EdgeInsets.only(left: 30,right: 30,top: 5),
              child: new ClipRRect(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10)),
                child: new Container(
                  color: Themecolor,
                  alignment: Alignment.center,
                  child: Text(
                    widget.matchDetails.toss!,
                    style: TextStyle(
                        fontFamily: 'noway',
                        color:Colors.white,
                        fontWeight: FontWeight.w400,
                        fontSize: 12),
                  ),
                ),
              ),
            ):new Container(),
            new Container(
              margin: EdgeInsets.only(top: widget.matchDetails.toss!.isNotEmpty?30:10),
              child: Container(
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),border: Border.all(color: TextFieldBorderColor)),
                child: Container(

                  child: new Container(
                    child: new Column(
                      children: [
                        new Container(
                          height: 25,
                          margin:
                          EdgeInsets.only(
                              top: 2,bottom: 2),
                          child: new Row(
                            mainAxisAlignment:
                            MainAxisAlignment
                                .spaceBetween,
                            children: [
                              Expanded(
                                child: new Container(
                                  width: 160,
                                  margin: EdgeInsets.only(
                                      left:
                                      10,
                                      top: 5),
                                  child: Text(
                                    widget.matchDetails.name!,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                        fontFamily:
                                        'noway',
                                        color: Text_Color,
                                        fontWeight:
                                        FontWeight
                                            .w500,
                                        fontSize:
                                        14),
                                  ),
                                ),
                              ),
                              new Row(
                                children: [
                                  widget.matchDetails.lineup==1?new Container(
                                    alignment: Alignment.center,
                                    margin: EdgeInsets.only(
                                        right:
                                        10,
                                        top: 5),
                                    child:
                                    new Row(
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(
                                              left: 5,
                                              right: 5),
                                          height:
                                          10,
                                          width:
                                          10,
                                          child:
                                          Image(image: AssetImage(AppImages.lineupOutImageURL)),
                                        ),
                                        new Container(
                                          margin:
                                          EdgeInsets.only(top: 2),
                                          child: new Text(
                                              'Lineups Out',
                                              style: TextStyle(fontFamily: 'noway', color: greenColor, fontWeight: FontWeight.w600, fontSize: 10)),
                                        ),
                                        SizedBox(width: 5,),
                                        Image.asset(AppImages.addRemainder,scale: 4,)
                                        // Icon(Icons.noti)
                                      ],
                                    ),
                                  ):new Container(),
                                  Visibility(
                                    visible:widget.matchDetails.lineup==1?false:true,
                                    child: new GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      child: new Container(
                                        margin: EdgeInsets.only(
                                            left:
                                            10,
                                            right:
                                            5),
                                        height: 18,
                                        width: 18,
                                        child:Image.asset(AppImages.addRemainder,scale: 3,color: Title_Color_2,),
                                      ),
                                      onTap: (){
                                        remindMeDialog();
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        // SizedBox(height: 20,),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10,0,10,0),
                          child: new Divider(height: 2,thickness: 1,color: Color(0xffE8E8E8),),
                        ),
                        new Container(
                          padding: EdgeInsets.only(bottom: 5),
                          alignment: Alignment.center,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              new Flexible(child: new Container(
                                child: new Column(
                                  children: [
                                    new Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(top: 5,),
                                          child: new Column(
                                            children: [
                                              new Container(
                                                margin: EdgeInsets.only(left: 10),
                                                padding: EdgeInsets.all(7),
                                                decoration: BoxDecoration(
                                                  // border: Border.all(color: Colors.blueAccent,width: 2),
                                                  borderRadius: BorderRadius.circular(100),
                                                ),
                                                child: Container(
                                                  height: 35,width: 35,
                                                  child: CachedNetworkImage(
                                                    imageUrl: widget.matchDetails.team1logo!,
                                                    placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                                    errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(
                                              left:
                                              5),
                                          child:
                                          new Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                            children: [
                                              new Container(
                                                margin:
                                                EdgeInsets.only(top: 5),
                                                width: 50,
                                                child:
                                                new Text(
                                                  widget.matchDetails.team1display!,
                                                  textAlign: TextAlign.left,
                                                  overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(fontFamily: 'noway', color: Colors.black, fontWeight: FontWeight.w500, fontSize: 14),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    new Container(
                                      alignment: Alignment.centerLeft,
                                      width: 110,
                                      margin: EdgeInsets.only(bottom: 5,left: 10),
                                      child:  new Text(
                                        widget.matchDetails.team1name!,
                                        textAlign:
                                        TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontFamily: AppConstants.textBold,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12),
                                      ),
                                    ),

                                  ],
                                ),
                              ),flex: 1,),
                              new Flexible(child: new Container(
                                child: new Column(
                                  children: [
                                    Container(height: 30, child: Image(image: AssetImage(AppImages.Vs),color: Themecolor,)),
                                    SizedBox(height: 5,),
                                    widget.matchDetails.is_leaderboard==1?new Container(
                                      height: 25,
                                      padding: EdgeInsets.all(3),
                                      margin: EdgeInsets.only(bottom: 5),
                                      child: Image(image: AssetImage(AppImages.matchLeaderboardIcon)),
                                    ):new Container(),
                                    CustomTimer(
                                      from: Duration(seconds: SuperDateFormat.getFormattedDateObj(widget.matchDetails.time_start!)!.difference(MethodUtils.getDateTime()).inSeconds),
                                      to: Duration(seconds: 0),
                                      controller: widget.controller,
                                      onBuildAction: CustomTimerAction.auto_start,

                                      // onFinish: (){
                                      //   widget.onMatchTimeUp(widget.index);
                                      // },
                                      builder: (CustomTimerRemainingTime remaining) {

                                        // if(remaining.minutes=='00'&& remaining.seconds=='00')
                                        // {
                                        //   widget.controller.pause();
                                        //   // widget.onMatchTimeUp(widget.index);
                                        //   widget.getController.TimeFunction(remaining.minutes, remaining.seconds, widget.index);
                                        //
                                        // }

                                        WidgetsBinding.instance?.addPostFrameCallback((_) {
                                          if (remaining.minutes == '00' && remaining.seconds == '00') {
                                            widget.controller.pause();
                                            widget.getController.TimeFunction(remaining.minutes, remaining.seconds, widget.index);
                                          }
                                        });

                                        return new GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          child: Text(
                                            remaining.minutes=='00'&& remaining.seconds=='00'?'00:00' : int.parse(remaining.days)>=1? int.parse(remaining.days)>1?"${remaining.days} Days  "+"Left":" ${remaining.days} Day"+"Left":
                                            int.parse(remaining.hours)>=1?("${remaining.hours}Hrs : ${remaining.minutes}mins" +" Left"):
                                            "${remaining.minutes}Mins : ${remaining.seconds=='-1'?"00":remaining.seconds}Sec" +" Left",
                                            style: TextStyle(
                                                fontFamily: 'noway',
                                                color: Title_Color_1,
                                                decoration: TextDecoration.none,
                                                fontWeight:
                                                FontWeight.w600,
                                                fontSize: 12),
                                          ),
                                          onTap: (){
                                          },
                                        );
                                      },
                                    )
                                  ],
                                ),
                              ),flex: 1,),
                              new Flexible(child: new Container(
                                child: new Column(
                                  children: [
                                    new Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(right: 5,top: 5),
                                          child:
                                          new Column(mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              new Container(
                                                child:
                                                new Text(
                                                  widget.matchDetails.team2display!,
                                                  overflow: TextOverflow.ellipsis,
                                                  textAlign: TextAlign.left,
                                                  style: TextStyle(fontFamily: 'noway', color: Colors.black, fontWeight: FontWeight.w500, fontSize: 14),
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(right: 10,top: 5),
                                          padding: EdgeInsets.all(7),
                                          decoration: BoxDecoration(
                                            // border: Border.all(color: Colors.blueAccent,width: 2),
                                            borderRadius: BorderRadius.circular(100),
                                          ),
                                          child:
                                          Container(height: 35,width: 35,
                                            child: CachedNetworkImage(
                                              imageUrl: widget.matchDetails.team2logo!,
                                              placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                              errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    new Container(
                                      width: 100,
                                      margin: EdgeInsets.only(bottom: 5,right: 10),
                                      alignment: Alignment.centerRight,
                                      child: new Text(
                                        widget.matchDetails.team2name!,
                                        textAlign:
                                        TextAlign.left,
                                        overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                            fontFamily: AppConstants.textBold,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w600,
                                            fontSize: 12),
                                      ),
                                    ),

                                  ],
                                ),
                              ),flex: 1,),
                            ],
                          ),
                        ),

                        new Container(
                          height: 30,
                          decoration: BoxDecoration(
                            color: TextFieldColor,
                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(10),bottomRight: Radius.circular(10)) ,

                          ),
                          child: new Container(
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Expanded(
                                  child: new Row(
                                    children: [
                                      widget.matchDetails.is_amount_show==1?new Row(
                                        children: [
                                          Padding(
                                            padding: const EdgeInsets.only(left: 8,right: 8),
                                            child: new Container(
                                              width: 40,
                                              height: 20,
                                              decoration: BoxDecoration(
                                                  color: Color(0xFF1a219d47),
                                                  border: Border.all(color: Color(0xFF80219d47)),borderRadius: BorderRadius.circular(2)
                                              ),
                                              child: Center(
                                                child: Text('MEGA',style: TextStyle(
                                                    color: greenColor,fontWeight: FontWeight.bold,fontSize: 12
                                                ),),
                                              ),
                                            ),
                                          ),
                                          new Text(
                                            widget.matchDetails.amount!,
                                            style: TextStyle(
                                                color:greenColor,
                                                fontSize: 12,
                                                fontWeight:
                                                FontWeight.normal),
                                          ),
                                        ],
                                      ):new Container(),
                                      widget.matchDetails.highlights!=null&&widget.matchDetails.highlights!.isNotEmpty?Expanded(child: new Container(
                                        alignment: Alignment.centerLeft,
                                        child: new Row(
                                          children: [
                                            new Container(
                                              margin: EdgeInsets.only(left: 30),
                                              alignment: Alignment.centerLeft,
                                              height: 30,
                                              child: Image(
                                                  height: 15,
                                                  width: 15,
                                                  color: Colors.green,
                                                  image: AssetImage(
                                                      AppImages.megaphoneIcon)),
                                            ),
                                            SizedBox(width: 10,),
                                            Expanded(child: new Container(
                                              child: new Row(
                                                children: [
                                                  new Expanded(child: new ScrollingText(
                                                    text: widget.matchDetails.highlights!,
                                                    textStyle: TextStyle(
                                                      fontFamily: AppConstants.textSemiBold,
                                                      color: Colors.green,
                                                      fontWeight: FontWeight.normal,
                                                      fontSize: 11,),
                                                  ))
                                                ],
                                              ),
                                            )),
                                          ],
                                        ),
                                      )):new Container(),
                                    ],
                                  ),
                                ),
                                widget.matchDetails.is_giveaway_visible==1?new Container(
                                  padding: EdgeInsets
                                      .only(
                                      left:
                                      5,
                                      right:
                                      10),
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                          image: AssetImage(AppImages
                                              .giveAwayURL),
                                          fit: BoxFit
                                              .fill)),
                                  child:
                                  new Row(
                                    children: [
                                      new Container(
                                        padding:
                                        EdgeInsets.all(7),
                                        child: Image.asset(
                                            AppImages.giveAwayIconURL),
                                      ),
                                      new Text(
                                        '₹'+widget.matchDetails.giveaway_amount!,
                                        style: TextStyle(
                                            color: Colors
                                                .white,
                                            fontSize:
                                            12,
                                            fontWeight:
                                            FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ):new Container()
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
      onTap: () => {
        if(widget.matchDetails.is_fixture==1){
          MethodUtils.showOrange(context, "Contests for this match will open soon. Stay tuned!")
        }else{
          navigateToUpcomingContests(context,new GeneralModel(bannerImage: widget.matchDetails.banner_image,matchKey: widget.matchDetails.matchkey,teamVs: (widget.matchDetails.team1display!+' VS '+widget.matchDetails.team2display!),firstUrl: widget.matchDetails.team1logo,secondUrl: widget.matchDetails.team2logo,headerText: widget.matchDetails.time_start,sportKey: widget.matchDetails.sport_key,battingFantasy: widget.matchDetails.battingfantasy,bowlingFantasy: widget.matchDetails.bowlingfantasy,liveFantasy: widget.matchDetails.livefantasy,secondInningFantasy: widget.matchDetails.secondinning,reverseFantasy: widget.matchDetails.reversefantasy,fantasySlots: widget.matchDetails.slotes,team1Name: widget.matchDetails.team1name,team2Name: widget.matchDetails.team2name,team1Logo: widget.matchDetails.team1logo,team2Logo: widget.matchDetails.team2logo))
        }
      },
    );
  }

  void remindMeDialog(){
    AlertDialog alertDialog=AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius:
          BorderRadius.all(Radius.circular(10.0))),
      title: Text("Match alert",
          style: TextStyle(color: Colors.black)),
      content: Text(
          "Remind me when match "+widget.matchDetails.team1display!+" vs "+widget.matchDetails.team2display!+" is about to start (before 25 min)",
          style: TextStyle(color: Colors.grey)),
      actions: [
        continueButton(context),
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alertDialog;
      },
    );
  }
  Widget continueButton(context) {
    return TextButton(
      onPressed: () {
        remindMe();
      },
      child: Text(
        "REMIND ME",
        style: TextStyle(color: Colors.black),
      ),
    )
    ;
  }
  void remindMe() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,matchkey: widget.matchDetails.matchkey);
    final client = ApiClient(AppRepository.dio);
    GeneralResponse myBalanceResponse = await client.remindMe(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (myBalanceResponse.status == 1) {
      Navigator.pop(context);
    }
    Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
  }
}