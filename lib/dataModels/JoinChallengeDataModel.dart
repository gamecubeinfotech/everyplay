import 'package:flutter/cupertino.dart';

class JoinChallengeDataModel {
  BuildContext context;
  int newChallengeId;
  int userId;
  int ChallengeId;
  int selectedFantasyType;
  int selectedSlotId;
  int totalSelected;
  int isBonus;
  int winningAmount;
  int maxUser;
  String teamId;
  String entryFee;
  String matchKey;
  String sport_key;
  String isJoinId;
  bool isSwitchTeam;
  double availableB;
  double usableB;
  Function onJoinContestResult;

  JoinChallengeDataModel(
      this.context,
      this.newChallengeId,
      this.userId,
      this.ChallengeId,
      this.selectedFantasyType,
      this.selectedSlotId,
      this.totalSelected,
      this.isBonus,
      this.winningAmount,
      this.maxUser,
      this.teamId,
      this.entryFee,
      this.matchKey,
      this.sport_key,
      this.isJoinId,
      this.isSwitchTeam,
      this.availableB,
      this.usableB,
      this.onJoinContestResult);
}