class LocationCustom {
  String name;
  String district;
  String region;
  String state;

  LocationCustom(this.name, this.district, this.region, this.state);

  factory LocationCustom.fromJson(Map<String, dynamic> json) {
    return LocationCustom(
        json['Name'], json['District'], json['Region'], json['State']);
  }
}