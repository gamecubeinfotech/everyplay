import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'app_colors.dart';


class AppImages {
  static String get splashImageURL => "assets/images/splash_bitmap.png";
  static String get splashMainURL => "assets/images/splash_bitmap_main.png";
  static String get TicketUrl => "assets/images/ticket.png";
  static String get addRound => "assets/images/add_round.png";
  static String get HomeBg => "assets/images/HomeBgGameList.png";
  static String get WinnerMedal => "assets/images/winner_medal.png";
  static String get logoImageURL => "assets/images/vision_home_ic.png";

  static String get backImageURL => "assets/images/back.png";
  static String get Vs => "assets/images/vs.png";
  static String get WinnerDeclear => "assets/images/winnerDeclear.png";
  static String get all_contests => "assets/images/all_contests.png";
  static String get inLauncherURL => "assets/images/ic_launcher.png";

  static String get fbImageURL => "assets/images/facebook.png";
  static String get prizepool => "assets/images/Prize_Pool.png";
  static String get googleImageURL => "assets/images/google.png";
  static String get entryfee => "assets/images/Entry_Fee.png";
  static String get appleImageURL => "assets/images/apple.webp";
  static String get star => "assets/images/star_ic.webp";

  static String get notificationImageURL =>
      "assets/images/notification_bell.png";



  static String get bellImageURL => "assets/images/bell.png";
  static String get superBgLogo => "assets/images/everplay_logo.png";


  static String get walletImageURL => "assets/images/wallet.png";

  static String get lineupOutImageURL => "assets/images/lineup_out.png";

  static String get logoPlaceholderURL => "assets/images/logo_placeholder.png";

  static String get giveAwayURL => "assets/images/giveaway_bg.png";

  static String get giveAwayIconURL => "assets/images/giveaway_icon.png";

  static String get moreProfileIcon => "assets/images/ic_user_avt.png";
  static String get desposited => "assets/images/depositMoney.png";
  static String get caseBonus => "assets/images/bonus.png";
  static String get recentTrx => "assets/images/recentTrx.png";
  static String get referEarn => "assets/images/referEarn.png";

  static String get moreForwardIcon => "assets/images/ic_forward.png";
  static String get moreLegalityIcon => "assets/images/ic_leaglity.png";

  static String get moreReferIcon => "assets/images/ic_more_refer_earn.png";
  static String get UploadDocVerifyIcon => "assets/images/upload_placeholder.png";

  static String get moreVerifiedIcon =>
      "assets/images/verified_protection.png";

  static String get moreLeaderboardIcon =>
      "assets/images/ic_more_fantasy_point.png"; //assets/images/promoter_leaderboard_icon.png

  static String get moreFantasyPointIcon =>
      "assets/images/ic_more_fantasy_point.png";

  static String get moreReferListIcon =>
      "assets/images/ic_more_refer_list.png";
  static String get moreResponsiblePlayIcon =>
      "assets/images/ic_responsible_play.png";

  static String get depositbalanceUrl => "assets/images/deposit_balance.png";

  static String get trophycupsilhouetteUrl =>
      "assets/images/trophy_cup_silhouette.png";

  static String get cashbonusUrl => 'assets/images/cash_bonus.png';

  static String get investmentanalyzerUrl =>
      'assets/images/investment_analyzer_icon.png';

  static String get morePrivacyIcon => "assets/images/ic_privacy_policy.png";

  static String get moreDocsIcon => "assets/images/docs.png";

  static String get moreInformationIcon => "assets/images/information.png";

  static String get moreFindIcon => "assets/images/find.png";

  static String get moreCallIcon => "assets/images/call.png";

  static String get moreAffiliateMarketIcon =>
      "assets/images/affiliate_marketing.webp";

  static String get morePromoteIcon => "assets/images/promote_app_ic.webp";

  static String get moreLogoutIcon => "assets/images/logout.png";

  static String get levelIcon => "assets/images/lavel_bg.webp";

  static String get defaultAvatarIcon => "assets/images/ic_default_avatar.png";
  static String get sabpaisa => "assets/images/sabPaisaicon.png";
  static String get phonePe => "assets/images/phone_pe.png";

  static String get levelArrowIcon => "assets/images/level_arroww.webp";

  static String get completedIcon => "assets/images/completed.png";

  static String get matchPlayedIcon => "assets/images/ic_match_played.webp";

  static String get contestPlayedIcon => "assets/images/ic_contest_played.webp";

  static String get contestWinIcon => "assets/images/ic_contest_win.webp";

  static String get totalWinningIcon => "assets/images/ic_total_wining.webp";

  static String get cashContestIcon => "assets/images/cash_contests_ic.webp";

  static String get rupeeIcon => "assets/images/rupee_ic.webp";

  static String get informationIcon => "assets/images/information_ic.webp";

  static String get notificationIcon => "assets/images/notification_icon.png";

  static String get referScreenIcon => "assets/images/refer_screen_bg.png";

  static String get referHeaderIcon => "assets/images/ic_refer_header.png";

  static String get copyIcon => "assets/images/ic_document.png";

  static String get referReferIcon => "assets/images/ic_refer_refer.png";

  static String get rupeeReferIcon => "assets/images/ic_rupee_refer.png";

  static String get bonusReferIcon => "assets/images/ic_bonus_refer.png";

  static String get lifetimeReferIcon => "assets/images/ic_refer_lifetime.png";

  static String get verificationEmailIcon =>
      "assets/images/ic_verification_email.png";

  static String get verifiedEmailIcon => "assets/images/ic_email_verified.png";
  static String get verifiedMobileIcon => "assets/images/ic_mobile_verified.png";
  static String get verifiedPanIcon => "assets/images/ic_pan_verified.png";
  static String get verifiedBankIcon => "assets/images/ic_bank_verified.png";

  static String get teamEditIcon => "assets/images/ic_team_edit.png";

  static String get verificationPanIcon => "assets/images/ic_verification_pan.webp";

  static String get verificationBankIcon =>
      "assets/images/ic_verification_bank.webp";

  static String get contactUsBgIcon => "assets/images/contact_us.png";

  static String get emailContactIcon => "assets/images/ic_email_contact.webp";

  static String get instaIcon => "assets/images/insta_circle.webp";

  static String get facebookIcon => "assets/images/facebook_circle.webp";

  static String get twitterIcon => "assets/images/twitter_circle.webp";

  static String get createContestIcon => "assets/images/create_contest.webp";

  static String get contestIcon => "assets/images/contest.webp";
  static String get contestIcon_placeholder => "assets/images/contest_icon.png";

  static String get myContestIcon => "assets/images/mycontest.webp";

  static String get createTeamIcon => "assets/images/create_team_icon.webp";

  static String get filterIcon => "assets/images/filter.webp";

  static String get contestsIcon => "assets/images/ic_contest.webp";

  static String get winnerMedalIcon => "assets/images/winner_medal.png";

  static String get bonusIcon => "assets/images/bonusIcon.png";

  static String get confirmIcon => "assets/images/confirm.webp";

  static String get winnersIcon => "assets/images/winners.webp";
  static String get winning => "assets/images/winning_icon.png";

  static String get cricketTeamBgIcon => "assets/images/cricket_team_bg.webp";
  static String get whiteImage => "assets/images/whiteImage.png";

  static String get basketballTeamBgIcon =>
      "assets/images/basketball_team_bg.webp";

  static String get handballTeamBgIcon => "assets/images/handball_team_bg.webp";

  static String get baseballTeamBgIcon =>
      "assets/images/baseball_hockey_team_bg.webp";

  static String get kabbadiTeamBgIcon => "assets/images/kabaddi_team_bg.webp";

  static String get teamViewIcon => "assets/images/ic_team_view.png";

  static String get starFillIcon => "assets/images/star_fill.png";

  static String get starIcon => "assets/images/star.png";

  static String get downloadIcon => "assets/images/download.webp";

  static String get helpIcon => "assets/images/ic_help.webp";

  static String get deleteIcon => "assets/images/rubbish_bin.png";

  static String get megaphoneIcon => "assets/images/megaphone.png";

  static String get infoIcon => "assets/images/info.webp";

  static String get addPlayerIcon => "assets/images/ic_add_player.webp";

  static String get removePlayerIcon => "assets/images/ic_remove_player.webp";

  static String get previewBgIcon => "assets/images/groundteam.png";

  static String get telegramIcon => "assets/images/ic_telegram.webp";

  static String get youtubeIcon => "assets/images/ic_youtube.webp";

  static String get footballPreviewBgIcon =>
      "assets/images/football_preview_bg.webp";

  static String get basketballPreviewBgIcon =>
      "assets/images/basketball_bg.webp";

  static String get baseballPreviewBgIcon =>
      "assets/images/ic_baseball_ground.webp";

  static String get hockeyPreviewBgIcon =>
      "assets/images/hockey_preview_bg.webp";

  static String get handballPreviewBgIcon =>
      "assets/images/handball_preview_bg.webp";

  static String get kabbaddiPreviewBgIcon =>
      "assets/images/kabaddi_preview_bg.webp";

  static String get previewLogoIcon => "assets/images/preview_logo.png";

  static String get chatIcon => "assets/images/chat_header_ic.png";

  static String get contactChatIcon => "assets/images/contact_chat.webp";

  static String get profileMenuIcon => "assets/images/profile_menu.webp";

  static String get inviteFriendIcon => "assets/images/invite_friend.png";

  static String get podiumIcon => "assets/images/podium.webp";

  static String get verifyIcon => "assets/images/verify.webp";

  static String get drawerCallIcon => "assets/images/drawer_call.webp";

  static String get moreIcon => "assets/images/more.webp";

  static String get turnOffIcon => "assets/images/turn_off.webp";

  static String get userAvatarIcon => "assets/images/user_avatar.webp";


  static String get bgMyMatchesIcon => "assets/images/bg_my_matches.webp";

  static String get winnersBgIcon => "assets/images/winners.png";
  static String get popupAvatar => "assets/images/popup_place.png";

  static String get greenCircleTickIcon =>
      "assets/images/green_circle_tick.webp";

  static String get paytmIcon => "assets/images/paytm.png";

  static String get upiIcon => "assets/images/upi.png";

  static String get walletIcon => "assets/images/add_wallet.png";
  static String get walletSymbol => "assets/images/wallet_symbol.png";

  static String get bankIcon => "assets/images/bank.png";

  static String get debitIcon => "assets/images/debit.png";

  static String get creditIcon => "assets/images/credit.webp";

  static String get firstWinIcon => "assets/images/first_win.webp";

  static String get winnerBgIcon => "assets/images/winner_bg.webp";

  static String get statsWinnerBgIcon => "assets/images/stats_winner_bg.webp";

  static String get promoteAppBgIcon => "assets/images/promote_app_bg.webp";

  static String get addIcon => "assets/images/add_channel.png";

  static String get compareIcon => "assets/images/ic_compare.webp";

  static String get verifiedIcon => "assets/images/verified.png";
  static String get UpiVerifiedIcon => "assets/images/upi_verify_account.webp";

  static String get notificationDotIcon =>
      "assets/images/notification_dot.webp";

  static String get cricPlayerIcon => "assets/images/cricket_player_iv.png";

  static String get footPlayerIcon => "assets/images/football_player_iv.webp";

  static String get baskPlayerIcon => "assets/images/basket_player_iv.webp";

  static String get basePlayerIcon => "assets/images/baseball_player_iv.webp";

  static String get handPlayerIcon => "assets/images/handball_player_iv.webp";

  static String get hockeyPlayerIcon => "assets/images/hocky_player_iv.webp";

  static String get kabPlayerIcon => "assets/images/kabaddi_artwork_iv.webp";

  static String get matchLeaderboardIcon =>
      "assets/images/match_leaderboard.webp";

  static String get popupPlaceholderIcon =>
      "assets/images/ic_banner_placeholder.webp";

  static String get noContestJoinedIcon => "assets/images/ic_no_joined_contest.png";

  static String get homeActiveIcon => "assets/images/ic_home_active.png";

  static String get homeInActiveIcon => "assets/images/ic_home.png";

  static String get matchActiveIcon => "assets/images/trophy_active.webp";

  static String get matchInActiveIcon => "assets/images/trophy.png";

  static String get accountActiveIcon => "assets/images/account_active_ic.webp";

  static String get accountInActiveIcon => "assets/images/ic_account.png";

  static String get moreActiveIcon => "assets/images/more_active_ic.webp";

  static String get moreInActiveIcon => "assets/images/ic_more.png";

  static String get cricActiveIcon => "assets/images/selected_cricket_ball.webp";

  static String get cricInActiveIcon => "assets/images/cricket_ball.webp";

  static String get liveActiveIcon => "assets/images/live_fantasy_active.png";

  static String get liveInActiveIcon => "assets/images/live_fantasy.webp";

  static String get footActiveIcon => "assets/images/football_active.webp";

  static String get footInActiveIcon => "assets/images/football.webp";

  static String get baskActiveIcon => "assets/images/basketball_active.webp";

  static String get baskInActiveIcon => "assets/images/basketball.webp";

  static String get baseActiveIcon => "assets/images/baseball_ball_active.webp";

  static String get baseInActiveIcon => "assets/images/baseball_ball.webp";

  static String get handActiveIcon => "assets/images/handball_active.webp";

  static String get handInActiveIcon => "assets/images/handball.webp";

  static String get hockeyActiveIcon => "assets/images/hocky_active.webp";

  static String get hockeyInActiveIcon => "assets/images/hocky.webp";

  static String get kabActiveIcon => "assets/images/kabaddi_active.webp";

  static String get kabInActiveIcon => "assets/images/kabaddi.webp";

  static String get leftTeamIcon => "assets/images/left_team.png";

  static String get rightTeamIcon => "assets/images/right_team.png";

  static String get leaderboardIcon => "assets/images/ic_leaderboard.webp";

  static String get downSort => "assets/images/ic_down_sort.png";

  static String get upSort => "assets/images/ic_up_sort.png";

  static String get checkTeamIcon => "assets/images/check_mark.webp";

  static String get uncheckTeamIcon => "assets/images/uncheck.webp";

  static String get topPlayerIcon => "assets/images/top_player.webp";

  static String get circleTickIcon => "assets/images/player_in_your_team.webp";

  static String get starTickIcon => "assets/images/star.webp";

  static String get greyCircleTickIcon => "assets/images/player_not_your_team.webp";

  static String get switchTeamIcon => "assets/images/ic_switch_team.webp";

  static String get comparePointsBgIcon =>
      "assets/images/compare_points_bg.webp";

  static String get otpVerifyImage => "assets/images/otpverifyImageHeader.webp";

  static String get main_bg => "assets/images/main_bg.png";

  static String get fb_image => "assets/images/facebook_image.png";

  static String get addRemainder => "assets/images/add_reminder.webp";

  static String get gmail_image => "assets/images/google_image.png";

  static String get winning_image => "assets/images/ic_your_winning.webp";

  static String get safeSecure => "assets/images/ic_secure_safe.png";
  static String get closeIcon => "assets/images/popup_close_icon.webp";
  static String get changePassword => "assets/images/change_password.png";
  static String get forgotPassword => "assets/images/forgotpassword.png";
  static String get teamName => "assets/images/teamName.png";
  static String get otpVerify => "assets/images/otpVerify.png";
  static String get phoneVerify => "assets/images/phoneOtp.png";
  static String get whatApp => "assets/images/whatsApp.png";
  static String get linkDin => "assets/images/Linkdin.png";
  static String get instagram => "assets/images/Instagram.png";
  static String get mailIcon => "assets/images/mailIcon.png";
}

Widget getMyIcon(BuildContext context, IconData icons) => Icon(
      icons,
      size: 35,
      // color: Theme.of(context).accentColor,
    );

Widget getMyImageAsset(BuildContext context, String _url, {double size = 35}) =>
    Image.asset(
      _url,
      height: size,
      width: size,
      // color: Theme.of(context).accentColor,
    );

Widget myVerticalDividerGray() => Container(
      width: 0.5,
      height: 70,
//      color: Colors.transparent,
      color: Colors.grey.shade300,
    );

Widget myHorizontalDividerGray() => Container(
      margin: EdgeInsets.only(left: 12, right: 12),
      height: 0.5,
//      color: Colors.transparent,
      color: Colors.grey.shade300,
    );
