import 'dart:io';

// import 'package:Repco/NavigatorPanel/ScanBarcode.dart';
// import 'package:Repco/NavigatorPanel/WelcomPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/repository/model/category_contest_response.dart';
import 'package:EverPlay/repository/model/create_private_contest_request.dart';
import 'package:EverPlay/repository/model/earn_contests_response.dart';
import 'package:EverPlay/repository/model/player_points_response.dart';
import 'package:EverPlay/repository/model/refresh_score_response.dart';
import 'package:EverPlay/views/AddBalance.dart';
import 'package:EverPlay/views/AffiliateMatches.dart';
import 'package:EverPlay/views/AffiliateProgram.dart';
import 'package:EverPlay/views/AllContests.dart';
import 'package:EverPlay/views/BreakupPlayerPoints.dart';
import 'package:EverPlay/views/ChangePassword.dart';
import 'package:EverPlay/views/ChooseCandVc.dart';
import 'package:EverPlay/views/CompareTeam.dart';
import 'package:EverPlay/views/ContactUs.dart';
import 'package:EverPlay/views/CreateTeam.dart';
import 'package:EverPlay/views/EarnContests.dart';
import 'package:EverPlay/views/ForgotPassword.dart';
import 'package:EverPlay/views/Home.dart';
import 'package:EverPlay/views/HomeLeaderboard.dart';
import 'package:EverPlay/views/InviteContestCode.dart';
import 'package:EverPlay/views/LiveFinishContestDetails.dart';
import 'package:EverPlay/views/LiveFinishedContests.dart';
import 'package:EverPlay/views/Login.dart';
import 'package:EverPlay/views/LoginNew.dart';
import 'package:EverPlay/views/MyJoinTeams.dart';
import 'package:EverPlay/views/OtpVerify.dart';
import 'package:EverPlay/views/PaymentOptions.dart';
import 'package:EverPlay/views/PlayerInfo.dart';
import 'package:EverPlay/views/PrivateContest.dart';
import 'package:EverPlay/views/PromoteApp.dart';
import 'package:EverPlay/views/PromoteAppDetails.dart';
import 'package:EverPlay/views/PromoterLeaderboard.dart';
import 'package:EverPlay/views/PromoterUserStats.dart';
import 'package:EverPlay/views/RecentTransactions.dart';
import 'package:EverPlay/views/ReferAndEarn.dart';
import 'package:EverPlay/views/ReferList.dart';
import 'package:EverPlay/views/Register.dart';
import 'package:EverPlay/views/SetTeamName.dart';
import 'package:EverPlay/views/TeamEarnings.dart';
import 'package:EverPlay/views/TeamPreview.dart';
import 'package:EverPlay/views/UpcomingContestDetails.dart';
import 'package:EverPlay/views/UpcomingContests.dart';
import 'package:EverPlay/views/UpiVerifyAccountWithdrawal.dart';
import 'package:EverPlay/views/UserNotifications.dart';
import 'package:EverPlay/views/UserProfile.dart';
import 'package:EverPlay/views/UserStats.dart';
import 'package:EverPlay/views/VerifyAccount.dart';
import 'package:EverPlay/views/VerifyEmailOrMobile.dart';
import 'package:EverPlay/views/EverplayWebView.dart';
import 'package:EverPlay/views/Wallet.dart';
import 'package:EverPlay/views/WinningBreakupManager.dart';
import 'package:EverPlay/views/WithdrawCash.dart';

import '../appUtilities/app_routes.dart';
import '../dataModels/GeneralModel.dart';
import '../views/LiveFinishContestDetailsNew.dart';
import '../views/LiveFinishedContestsNew.dart';
import '../views/PaymentView.dart';
import '../views/RegisterNew.dart';

//=============================================================================

Future<dynamic> openScreenAsLeftToRight(BuildContext context, Widget targetWidget) async {
  return await openScreenAsPlatformWiseRoute(context, targetWidget, isFullScreen: true);
}

void navigateToVerifyEmailOrMobile(BuildContext context,String type,Function emailViewRefresh) =>
    openScreenAsLeftToRight(context, VerifyEmailOrMobile(type,emailViewRefresh));

void navigateToCompareTeam(BuildContext context,String matchKey,String sportKey,String team1Id,String team2Id,String fantasyType,String slotId,String challengeId) =>
    openScreenAsLeftToRight(context, CompareTeam(matchKey,sportKey,team1Id,team2Id,fantasyType,slotId,challengeId));

void navigateToWithdrawCash(BuildContext context,{String? type}) =>
    openScreenAsLeftToRight(context, WithdrawCash(type:type));

void navigateToHomePage(BuildContext context) =>
    openScreenAsLeftToRight(context, HomePage());

void navigateToVisionWebView(BuildContext context,var title,var url) =>
    openScreenAsLeftToRight(context, EverplayWebView(title,url));

void navigateToPromoteAppDetails(BuildContext context) =>
    openScreenAsLeftToRight(context, PromoteAppDetails());

void navigateToPromoteApp(BuildContext context) =>
    openScreenAsLeftToRight(context, PromoteApp());

void navigateToTeamEarnings(BuildContext context,EarnContestItem earnContestItem,String shortName) =>
    openScreenAsLeftToRight(context, TeamEarnings(earnContestItem,shortName));

void navigateToEarnContests(BuildContext context,String matchKey,String shortName) =>
    openScreenAsLeftToRight(context, EarnContests(matchKey,shortName));

void navigateToAffiliateMatches(BuildContext context,String start_date,String end_date) =>
    openScreenAsLeftToRight(context, AffiliateMatches(start_date,end_date));

void navigateToAffiliateProgram(BuildContext context) =>
    openScreenAsLeftToRight(context, AffiliateProgram());

void navigateToPromoterUserStats(BuildContext context,String seriesId,String userId) =>
    openScreenAsLeftToRight(context, PromoterUserStats(seriesId,userId));

void navigateToPromoterLeaderboard(BuildContext context) =>
    openScreenAsLeftToRight(context, PromoterLeaderboard());

void navigateToUserStats(BuildContext context,String seriesId,String userId) =>
    openScreenAsLeftToRight(context, UserStats(seriesId,userId));

void navigateToAddBalance(BuildContext context,String promo_code, ) =>
    openScreenAsLeftToRight(context, AddBalance(promo_code));

Future<dynamic> navigateToPhonePe(BuildContext context,String title, String url ) async
{
  return await openScreenAsLeftToRight(context, PaymentView(title, url));
}

void navigateToPaymentOptions(BuildContext context,String amount,int promoId) =>
    openScreenAsLeftToRight(context, PaymentOptions(amount,promoId));

void navigateToHomeLeaderboard(BuildContext context,{int? seriesId}) =>
    openScreenAsLeftToRight(context, HomeLeaderboard(seriesId: seriesId));

void navigateToRecentTransactions(BuildContext context) =>
    openScreenAsLeftToRight(context, RecentTransactions());

void navigateToBreakupPlayerPoints(BuildContext context,MultiSportsPlayerPointItem breakupPoint) =>
    openScreenAsLeftToRight(context, BreakupPlayerPoints(breakupPoint));
//
void navigateToLiveFinishContestDetails(BuildContext context,LiveFinishedContestData contest,GeneralModel model) =>
    openScreenAsLeftToRight(context, LiveFinishContestDetails(contest,model));

void navigateToLiveFinishContestDetailsNew(BuildContext context,LiveFinishedContestData contest,GeneralModel model) =>
    openScreenAsLeftToRight(context, LiveFinishContestDetailsNew(contest,model));

void navigateToLiveFinishContests(BuildContext context,GeneralModel model) =>
    openScreenAsLeftToRight(context, LiveFinishContests(model));

void navigateToLiveFinishContestsNew(BuildContext context,GeneralModel model) =>
    openScreenAsLeftToRight(context, LiveFinishedContestsNew(model));

void navigateToInviteContestCode(BuildContext context,GeneralModel model) =>
    openScreenAsLeftToRight(context, InviteContestCode(model));

void navigateToMyJoinTeams(BuildContext context,GeneralModel model,Contest contest,Function onJoinContestResult) =>
    openScreenAsLeftToRight(context, MyJoinTeams(model,contest,onJoinContestResult));

void navigateToWinningBreakupManager(BuildContext context,GeneralModel model,CreatePrivateContestRequest request,Function onTeamCreated) =>
    openScreenAsLeftToRight(context, WinningBreakupManager(request,model,onTeamCreated));

void navigateToPrivateContest(BuildContext context,GeneralModel model,Function onTeamCreated) =>
    openScreenAsLeftToRight(context, PrivateContest(model,onTeamCreated));

void navigateToTeamPreview(BuildContext context,GeneralModel model) =>
    openScreenAsLeftToRight(context, TeamPreview(model));

void navigateToChooseCandVc(BuildContext context,GeneralModel model,{Contest? contest}) =>
    openScreenAsLeftToRight(context, ChooseCandVc(model));

void navigateToPlayerInfo(BuildContext context,String? matchKey,int? playerId,String? playerName,String? team,String? playerImage,bool? isSelected,int index,int? type,String? sportKey,int? fantasyType,int? slotId,String isFrom,Function? onPlayerClick,String shorRole) =>
    openScreenAsLeftToRight(context, PlayerInfo(matchKey,playerId,playerName,team,playerImage,isSelected,index,type,sportKey,fantasyType,slotId,isFrom,onPlayerClick,shorRole));

void navigateToAllContests(BuildContext context,GeneralModel model) =>
    openScreenAsLeftToRight(context, AllContests(model));

void navigateToCreateTeam(BuildContext context,GeneralModel model,{Contest? contest}) =>
    openScreenAsLeftToRight(context, CreateTeam(model,contest: contest,));

void navigateToUpcomingContestDetails(BuildContext context,GeneralModel model,Contest contest) =>
    openScreenAsLeftToRight(context, UpcomingContestDetails(model,contest));

void navigateToUpcomingContests(BuildContext context,GeneralModel model) =>
    openScreenAsLeftToRight(context, UpcomingContests(model));

void navigateToReferList(BuildContext context) =>
    openScreenAsLeftToRight(context, ReferList());

void navigateToContactUs(BuildContext context) =>
    openScreenAsLeftToRight(context, ContactUs());

Future<dynamic> navigateToVerifyAccount(BuildContext context) async {
  return await openScreenAsLeftToRight(context, VerifyAccount());
}

void navigateToUserNotifications(BuildContext context) =>
    openScreenAsLeftToRight(context, UserNotifications());

void navigateToReferAndEarn(BuildContext context) =>
    openScreenAsLeftToRight(context, ReferAndEarn());

void navigateToUserProfile(BuildContext context) =>
    openScreenAsLeftToRight(context, UserProfile());

void navigateToWallet(BuildContext context) =>
    openScreenAsLeftToRight(context, Wallet());

void navigateToSetTeamName(BuildContext context) =>
    openScreenAsLeftToRight(context, SetTeamName());


void navigateToChangePassword(BuildContext context) =>
    openScreenAsLeftToRight(context, ChangePassword());

void navigateToOtpVerify(BuildContext context) =>
    openScreenAsLeftToRight(context, OtpVerify());

void navigateToRegister(BuildContext context) =>
    openScreenAsLeftToRight(context, Register());


void navigateToRegisterNew(BuildContext context) =>
    openScreenAsLeftToRight(context, RegisterNew());

void navigateToForgotPassword(BuildContext context) =>
    openScreenAsLeftToRight(context, ForgotPassword());

void navigateToLogin(BuildContext context) =>
    openScreenAsLeftToRight(context, LoginNew());

void navigateToUPIVerify(BuildContext context) =>
    openScreenAsLeftToRight(context, UpiVerifyAccountWithdrawal());

//
// //=============================================================================
// void navigateToDeliveryListPage(BuildContext context) {
//   Navigator.of(context).push(
//       CupertinoPageRoute<Null>(builder: (BuildContext context) {
//         return new DeliveryListPage();
//       }));
// }
//
// //=============================================================================
// void navigateToDeliveryDetailsPage(BuildContext context) {
//   openScreenAsBottomToTop(context, DeliveryDetailsPage());
// }
//=============================================================================

openScreenAsBottomToTop(BuildContext context, Widget targetWidget,
        {bool isFullScreen = false}) async =>
    await Navigator.of(context)
        .push(AppRoute.createRoute(targetWidget, isFullScreen: isFullScreen));

//Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => targetWidget));

//=============================================================================

Future<dynamic> openScreenAsPlatformWiseRoute(BuildContext context, Widget targetWidget, {bool isFullScreen = false}) async {
  return Platform.isIOS
      ? await Navigator.of(context).push(CupertinoPageRoute(
      builder: (BuildContext context) => targetWidget,
      fullscreenDialog: isFullScreen))
      : await Navigator.of(context).push(MaterialPageRoute(
      builder: (BuildContext context) => targetWidget,
      fullscreenDialog: isFullScreen));
}
