import 'dart:math';

import 'package:flutter/material.dart';

const MaterialColor materialPrimaryColor = const MaterialColor(
  0xff7a0d11,
  const <int, Color>{
    50: const Color(0xff7a0d11),
    100: const Color(0xff7a0d11),
    200: const Color(0xff7a0d11),
    300: const Color(0xff7a0d11),
    400: const Color(0xff7a0d11),
    500: const Color(0xff7a0d11),
    600: const Color(0xff7a0d11),
    700: const Color(0xff7a0d11),
    800: const Color(0xff7a0d11),
    900: const Color(0xff7a0d11),
  },
);

const MaterialColor materialPrimaryColor2 = const MaterialColor(
  0xffffffff,
  const <int, Color>{
    50: const Color(0xffffffff),
    100: const Color(0xffffffff),
    200: const Color(0xffffffff),
    300: const Color(0xffffffff),
    400: const Color(0xffffffff),
    500: const Color(0xffffffff),
    600: const Color(0xffffffff),
    700: const Color(0xffffffff),
    800: const Color(0xffffffff),
    900: const Color(0xffffffff),
  },
);
const MaterialColor gradientPrimaryColor = const MaterialColor(
  0xffffffff,
  const <int, Color>{
    50: const Color(0xff190304),
    100: const Color(0xff630f12),
  },
);

Color get primaryColor => Color(0xffAA100E);
Color get primaryDarkColor => Color(0xffcb1409);
Color get darkPrimaryColor => Color(0xff7a0d11);
Color get accentColor => Color(0xff000000);
Color get darkgray => Color(0xff8e9193);
Color get bgColor => Color(0xffeeeeee);
Color get editbgcolor => Color(0xffdbdbdb);
Color get bgColorDark => Color(0xffededed);
Color get textFieldBgColor => Color(0xfff3f2f2);
Color get textFieldBorderColoer => Color(0xffe1e1e1);
Color get greenColor => Color(0xff219d47);
Color get lightGrayColor => Color(0xfff5f5f5);
Color get lightGrayColor2 => Color(0xfff7f7f7);
Color get lightGrayColor3 => Color(0xffdbdfea);
Color get lightGrayColor4 => Color(0xffA1FFFFFF);
Color get lightGrayColor5 => Color(0xffd9d9d9);
Color get darkGrayColor => Color(0xff797979);
Color get mediumGrayColor => Color(0xffD8D8D8);
Color get yellowColor => Color(0xffffd350);
Color get lightGreenColor => Color(0xfff2f8f4);
Color get lightGreenColor2 => Color(0xff92af9b);
Color get lightYellowColor => Color(0xfffefaef);
Color get lightSkyColor => Color(0xffedf6ff);
Color get lightBlack => Color(0xff343434);
Color get transBlack => Color(0xffCC0D141A);
Color get lightYellow => Color(0xffffffed);
Color get lightYellowColor2 => Color(0xfffffda6);
Color get darkBlue => Color(0xff08114d);
Color get orangeColor => Color(0xffff6f00);
Color get darkOrangeColor => Color(0xffE14C14);
Color get otherOrangeColor => Color(0xffDF7308);
Color get teamSelectedNew => Color(0xfff8f8f8);
Color get pointPlayerBg => Color(0xffffe8db);
Color get teamBg => Color(0xfffffaf4);
Color get red => Color(0xffe6292b);

Color get textBlueDark => Color(0xff262970);

Color get categoryIconCyan => Color(0xff1bbef9);

Color get categoryTitleGrayDark => Color(0xff707070);

Color get categoryCardBg => Color(0xffe6ecf2);

Color get lightDividerColor => Colors.grey.shade200;

Color get lightchatBgColor => Colors.grey.shade50;

MaterialColor get randomColor =>
    Colors.primaries[Random().nextInt(Colors.primaries.length)];


Color get Themecolor => Color(0xffAA100E);
Color get LinkTextColor => Color(0xff000000); //AA100E
Color get Title_Color_1 => Color(0xff283F4C); //#283F4C
Color get yellowdark => Color(0xffFFAB3B);
Color get textGrey => Color(0xff848484);
Color get lightgrey => Color(0xffFAFAFA);

Color get Title_Color_2 => Color(0xffAA100E);
Color get Text_Color => Color(0xff8f8c8c);
Color get TextFieldColor => Color(0xfff1f4fc);
Color get TextFieldTextColor => Color(0xff283f4c);
Color get whiteColor => Colors.white;
Color get TextFieldBorderColor => Color(0xffd9d9d9);
Color get bordercolor => Color(0xffB8B8B8);
Color get backgroundColor => Color(0xffF8F6F4);