//import 'package:device_info/device_info.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class DeviceInfo {
  static useMobileLayout(BuildContext context) =>
      MediaQuery.of(context).size.shortestSide < 600;

  static isDeviceIOS() => defaultTargetPlatform == TargetPlatform.iOS;

  static getDeviceIMEI() async => "123456789";

  static getDeviceType() => "";

  static getDeviceName() => "";

/* static getDeviceName() async {
    String receivedDeviceName;
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

    if (isDeviceIOS()) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      receivedDeviceName = iosInfo.utsname.machine;
    } else {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      receivedDeviceName = "${androidInfo.brand} : ${androidInfo.device} ";
    }
    print('[ Running on Device : ${receivedDeviceName} ]');
    return receivedDeviceName;
  }*/
}
