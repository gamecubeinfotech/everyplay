import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/dataModels/JoinChallengeDataModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/category_contest_response.dart';
import 'package:EverPlay/repository/model/contest_request.dart';
import 'package:EverPlay/repository/model/join_contest_request.dart';
import 'package:EverPlay/repository/model/join_contest_response.dart';
import 'package:EverPlay/repository/model/my_balance_response.dart';
import 'package:EverPlay/repository/model/score_card_response.dart';
import 'package:EverPlay/repository/model/usable_balance_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

import 'app_colors.dart';
import 'app_constants.dart';
import 'app_navigator.dart';

class MethodUtils {
  static String currentTimeStamp({bool includeNano = false}) {
    DateTime now = DateTime.now();

    if (includeNano) {
      return "${now.year}${now.month}${now.day}${now.hour}${now.minute}${now.second}${now.millisecondsSinceEpoch}${now.microsecondsSinceEpoch}";
    } else {
      return "${now.year}${now.month}${now.day}${now.hour}${now.minute}${now.second}";
    }
  }

  static String getCurrentDateTime({bool isYMD = true}) {
    return getCurrentDate(isYMD: isYMD) + " " + getCurrentTime();
  }

  static String getCurrentDate({bool isYMD = true, DateTime? now }) {
    if (now == null) {
      now = DateTime.now();
    }
    return isYMD
        ? "${now.year}-${now.month > 9 ? now.month : "0${now.month}"}-${now.day > 9 ? now.day : "0${now.day}"}"
        : "${now.day > 9 ? now.day : "0${now.day}"}-${now.month > 9 ? now.month : "0${now.month}"}-${now.year}";
  }

  static String getCurrentTime({DateTime? now }) {
    if (now == null) {
      now = DateTime.now();
    }

    return "${now.hour > 9 ? now.hour : "0${now.hour}"}:${now.minute > 9 ? now.minute : "0${now.minute}"}:${now.second > 9 ? now.second : "0${now.second}"}";
  }
  static DateTime getDateTime() {
    return DateTime.now();
  }

  static String getCurrTimestamp({bool includeMillisecond = true}) {
    DateTime now = DateTime.now();
    return includeMillisecond
        ? "${now.day}${now.month}${now.year}${now.hour}${now.minute}${now.second}${now.millisecond}"
        : "${now.day}${now.month}${now.year}${now.hour}${now.minute}${now.second}";
  }

  static void hideKeyboard(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  static void showSnackBar(BuildContext ctx, msgToDisplay) {
    print(msgToDisplay);

    var snackBar = SnackBar(
      content: Text(msgToDisplay),
    );

    ScaffoldMessenger.of(ctx).showSnackBar(snackBar);
  }
  static void showSuccess(BuildContext ctx, msgToDisplay) {
    Flushbar(message: msgToDisplay,flushbarPosition: FlushbarPosition.TOP,isDismissible: true,backgroundColor: Themecolor,duration: Duration(seconds: 2),animationDuration: Duration(milliseconds: 500),).show(ctx);
  }
  static void showError(BuildContext ctx, msgToDisplay) {
    Flushbar(message: msgToDisplay,flushbarPosition: FlushbarPosition.TOP,isDismissible: true,backgroundColor: primaryColor,duration: Duration(seconds: 2),animationDuration: Duration(milliseconds: 500),).show(ctx);
  }
  static void showOrange(BuildContext ctx, msgToDisplay) {
    Flushbar(message: msgToDisplay,flushbarPosition: FlushbarPosition.TOP,isDismissible: true,backgroundColor: orangeColor,duration: Duration(seconds: 2),animationDuration: Duration(milliseconds: 500),).show(ctx);
  }

  static void showSnackBarGK(
      GlobalKey<ScaffoldState> globalScaffoldKey, msgToDisplay) {
    print(msgToDisplay);

    var snackBar = SnackBar(
      content: Text(msgToDisplay),
    );

    // globalScaffoldKey.currentState!.showSnackBar(snackBar);
  }

  static String getGreetingText() {
    String wText = "";
    DateTime c = DateTime.now();
    int timeOfDay = c.hour;

    if (timeOfDay >= 0 && timeOfDay < 12) {
      wText = "Good Morning";
    } else if (timeOfDay >= 12 && timeOfDay < 16) {
      wText = "Good Afternoon";
    } else if (timeOfDay >= 16 && timeOfDay < 21) {
      wText = "Good Evening";
    } else if (timeOfDay >= 21 && timeOfDay < 24) {
      wText = "Good Night";
    }
    return wText;
  }

  static Future<bool> isInternetPresentOld() async {
    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        //  print('connected');
        return true;
      } else {
        return false;
      }
    } on SocketException catch (_) {
      //print('not connected');
      return false;
    }
  }

  static Future<bool> isInternetPresent() async {
    try {
      var connectivityResult = await (Connectivity().checkConnectivity());
      if (connectivityResult == ConnectivityResult.mobile) {
        print("Connected to Mobile Network");

        return true;
      } else if (connectivityResult == ConnectivityResult.wifi) {
        print("Connected to WiFi");
        return true;
      } else {
        print("Unable to connect. Please Check Internet Connection");
        return false;
      }
    } on SocketException catch (_) {
      //print('not connected');
      return false;
    }
  }
  static Color hexToColor(String code) {
    return new Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  static void checkBalance(JoinChallengeDataModel model) async{
    AppLoaderProgress.showLoader(model.context);
    JoinContestRequest contestRequest = new JoinContestRequest(user_id: model.userId.toString(),teamid: model.teamId,challengeid: model.newChallengeId==0?model.ChallengeId.toString():model.newChallengeId.toString(),fantasy_type: model.selectedFantasyType.toString(),slotes_id: model.selectedSlotId.toString());
    final client = ApiClient(AppRepository.dio);
    BalanceResponse response = await client.getUsableBalance(contestRequest);
    AppLoaderProgress.hideLoader(model.context);
    if(response.status==1 && response.result!.length>0){
        model.availableB=double.parse(response.result![0].usertotalbalance!);
        model.usableB=double.parse(response.result![0].usablebalance!);
        if(model.newChallengeId==0){
          showDialog(model);
        }
        else{

        }
    }
  }
  static void showDialog(JoinChallengeDataModel model) {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: false,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: Duration(milliseconds: 300),
      context: model.context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.center,
          child: Wrap(
            children:[
              Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                ),
                child: new Column(
                  children: [
                    new Container(
                      height: 50,
                      width: MediaQuery.of(model.context).size.width,
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              alignment: Alignment.center,
                              child: new Text('Confirmation',
                                style: TextStyle(
                                    fontFamily: AppConstants.textBold,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,decoration: TextDecoration.none),),
                            ),
                            onTap: () => {},
                          ),
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              height: 40,
                              alignment: Alignment.center,
                              child: new Container(
                                child: Icon(
                                  Icons.clear,
                                  color: Colors.black,
                                ),
                              ),
                            ),
                            onTap: () => {
                              Navigator.pop(model.context)
                            },
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      child: new Row(
                        children: [
                          new Container(
                            alignment: Alignment.center,
                            child: new Text('Utilized Balance + Winnings = ',
                              style: TextStyle(
                                  fontFamily: AppConstants.textSemiBold,
                                  color: textGrey,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,decoration: TextDecoration.none),),
                          ),
                          new Container(
                            alignment: Alignment.center,
                            child: new Text('₹'+(NumberFormat('#.##').format(model.availableB)),
                              style: TextStyle(
                                  fontFamily: AppConstants.textSemiBold,
                                  color: textGrey,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14,decoration: TextDecoration.none),),
                          )
                        ],
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: 30),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Container(
                            alignment: Alignment.center,
                            child: new Text('Entry Fees',
                              style: TextStyle(
                                  fontFamily: AppConstants.textRegular,
                                  color: textGrey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,decoration: TextDecoration.none),),
                          ),
                          new Container(
                            alignment: Alignment.center,
                            child: new Text('₹'+(NumberFormat('#.##').format(double.parse(model.entryFee)*model.totalSelected)),
                              style: TextStyle(
                                  fontFamily: AppConstants.textRegular,
                                  color: textGrey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,decoration: TextDecoration.none),),
                          )
                        ],
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: 10,bottom: 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Container(
                            alignment: Alignment.center,
                            child: new Text('Usable Cash Bonus',
                              style: TextStyle(
                                  fontFamily: AppConstants.textRegular,
                                  color: textGrey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,decoration: TextDecoration.none),),
                          ),
                          new Container(
                            alignment: Alignment.center,
                            child: new Text('₹'+(NumberFormat('#.##').format(model.usableB)),
                              style: TextStyle(
                                  fontFamily: AppConstants.textRegular,
                                  color: textGrey,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 14,decoration: TextDecoration.none),),
                          )
                        ],
                      ),
                    ),
                    new Divider(thickness: .5,),
                    new Container(
                      color:TextFieldColor,
                      padding: EdgeInsets.all(10),
                      margin: EdgeInsets.only(bottom: 5),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Container(
                            alignment: Alignment.center,
                            child: new Text('To Pay',
                              style: TextStyle(
                                  // fontFamily: AppConstants.textRegular,
                                  color: primaryColor,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14,decoration: TextDecoration.none),),
                          ),
                          new Container(
                            alignment: Alignment.center,
                            child: new Text('₹'+(NumberFormat('#.##').format((double.parse(model.entryFee)*model.totalSelected)-model.usableB)),
                              style: TextStyle(

                                  color: primaryColor,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 14,decoration: TextDecoration.none),),
                          )
                        ],
                      ),
                    ),
                    new Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 5),
                      child: new Html(data: "By joining this contest, you accept <font color='#c61d24'> EverPlay </font> <a href='" + AppConstants.terms_url + "' style='text-decoration: none;'><font color='#000000'>Terms &amp; Conditions </font> </a> and <a href='" + AppConstants.privacy_url + "' style='text-decoration: none;'><font color='#000000'>Privacy Policy </font></a> Confirm that you are not a resident of  Andhra Pradesh, Assam, Nagaland, Orissa, Sikkim or Telangana.",
                        onLinkTap: (link,_,__){
                          if(link!.contains("terms")) {
                            navigateToVisionWebView(model.context, 'Terms & Condition',link);
                          }else{
                            navigateToVisionWebView(model.context, 'Privacy Policy',link);
                          }
                        },
                        style: {
                          'html': Style(fontFamily: 'roboto',fontWeight: FontWeight.w400,fontSize: FontSize.small,textAlign: TextAlign.center,color: Colors.grey,textDecoration: TextDecoration.none)
                        },
                      ),
                    ),
                    new Container(
                        width:
                        MediaQuery.of(model.context).size.width,
                        height: 45,
                        margin: EdgeInsets.only(top: 10,right: 20, left : 20),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Themecolor,
                            elevation: 0.5,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40),
                            ),
                          ),
                          onPressed: () async {
                            Navigator.pop(model.context);

                            double requiredBalance = double.parse(model.entryFee) * model.totalSelected - (model.usableB + model.availableB);
                            bool addBalance = false;

                            if (model.isBonus == 1) {
                              if (model.usableB + model.availableB < double.parse(model.entryFee) * model.totalSelected) {
                                addBalance = true;
                              }
                            } else {
                              if (model.availableB < double.parse(model.entryFee) * model.totalSelected) {
                                addBalance = true;
                              }
                            }

                            if (addBalance) {
                              navigateToAddBalance(model.context, '');
                            } else {
                              joinChallenge(model);
                            }
                          },
                          child: Text(
                            'Join This Contest',
                            style: TextStyle(fontSize: 14, color: Colors.white),
                          ),
                        )
                    ),
                  ],
                ),
              )
            ] ,
          ),
        );
      },
    );
  }

  static void joinChallenge(JoinChallengeDataModel model) async{
    AppLoaderProgress.showLoader(model.context);
    JoinContestRequest contestRequest = new JoinContestRequest(matchkey:model.matchKey,sport_key:model.sport_key,user_id: model.userId.toString(),teamid: model.teamId,challengeid: model.newChallengeId==0?model.ChallengeId.toString():model.newChallengeId.toString(),fantasy_type: model.selectedFantasyType.toString(),slotes_id: model.selectedSlotId.toString());
    final client = ApiClient(AppRepository.dio);
    JoinContestResponse response = await client.joinChallenge(contestRequest);
    AppLoaderProgress.hideLoader(model.context);
    if(response.status==1 && response.result!.length>0){
      if(response.result![0].status!){
        getUserBalance(model.matchKey,model.userId.toString(), response.result![0].isjoined!, response.result![0].refercode!, model.context,model.onJoinContestResult);
      }
    }
    else if(response.status==0){
      model.newChallengeId=response.new_challenge_id!;
      checkBalance(model);
    }
    else{
      Fluttertoast.showToast(msg: response.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
      model.onJoinContestResult(0,'');
    }

  }
  static void getUserBalance(String matchKey,String userId,int isJoined, String referCode,BuildContext context,Function onJoinContestResult) async {
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,fcmToken: '',matchkey: matchKey);
    final client = ApiClient(AppRepository.dio);
    MyBalanceResponse myBalanceResponse = await client.getUserBalance(loginRequest);
    if (myBalanceResponse.status == 1&&myBalanceResponse.result!.length>0) {
      MyBalanceResultItem myBalanceResultItem = myBalanceResponse.result![0];
      AppPrefrence.putString(AppConstants.KEY_USER_BALANCE, myBalanceResultItem.balance.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_WINING_AMOUNT, myBalanceResultItem.winning.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_BONUS_BALANCE, myBalanceResultItem.bonus.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_TOTAL_BALANCE, myBalanceResultItem.total.toString());
      onJoinContestResult(isJoined,referCode);
      Fluttertoast.showToast(msg: 'You have Successfully join this contest', toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
      return;
    }
    Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
  }
  static void onShare(BuildContext context,Contest contest,GeneralModel model) async {

    String shareBody =
        "Hi, Inviting you to join " + AppConstants.APP_NAME + " and play fantasy sports to win cash daily.\n" +
            "Use Contest code - " + contest.refercode! +
            "\nTo join this Exclusive invite-only contest on " + AppConstants.APP_NAME + " for the " + model.teamVs! + " match, Download App from~ " + AppConstants.apk_url;
    final RenderBox box = context.findRenderObject() as RenderBox;
    await Share.share(shareBody, sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }
  static void showWinningPopup(BuildContext context,List<WinnerScoreCardItem> breakupList,String winAmount){
    showModalBottomSheet(
        context: context,
        shape : RoundedRectangleBorder(
            borderRadius : BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10))
        ),
        builder: (builder){
          return Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Container(padding:EdgeInsets.all(10),child: new Text('Winners Rank Calculation',style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.w500),),),
              new Container(
                padding:EdgeInsets.only(bottom: 10),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    new Text('Total Winnings ',style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.w400),),
                    new Text("₹"+winAmount,style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold),),
                  ],
                ),
              ),
              new Container(
                width: MediaQuery.of(context).size.width,
                color: primaryColor,
                height: 40,
                alignment: Alignment.center,
                padding:EdgeInsets.all(5),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    new Text('Winning Position',style: TextStyle(color: Colors.white,fontSize: 14,fontWeight: FontWeight.w400),),
                    new Text("Winning Amount",style: TextStyle(color: Colors.white,fontSize: 14,fontWeight: FontWeight.w400),),
                  ],
                ),
              ),
              Expanded(child: new Container(
                child: new ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: breakupList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return new Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        height: 40,
                        alignment: Alignment.center,
                        padding:EdgeInsets.all(5),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Text(breakupList[index].start_position!,style: TextStyle(color: Colors.grey,fontSize: 14,fontWeight: FontWeight.w500),),
                            new Text("₹"+breakupList[index].price.toString(),style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                          ],
                        ),
                      );
                    }
                ),
              )),
              new Container(
                color: bgColor,
                alignment: Alignment.center,
                padding: EdgeInsets.all(5),
                child: new Text("Note: The actual price money may be different than the prize money mentioned above if there is a tie for any of the winning position. As per goverment regulations, a tax of 30% will be deducted if an individual wins more than ₹ 10,000.",textAlign: TextAlign.center,style: TextStyle(color: Colors.black,fontSize: 10,fontWeight: FontWeight.w400),),
              )
            ],
          );
        }
    );
  }
}
