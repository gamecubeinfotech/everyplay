import 'package:flutter/material.dart';

class AppConstants {

  static const String base_api_url ='https://everplayfantasy.com/';
  // static const String base_api_url ='https://everplay.gamecubeinfo.com/';

  static String web_pages_url = base_api_url+"app/";
  static String terms_url = web_pages_url + "terms-and-conditions";
  static String fantasy_point_url = web_pages_url + "fantasy-point-system";
  static String privacy_url = web_pages_url + "privacy-policy";
  static String about_us_url = web_pages_url + "about-us";
  static String responsible_play = web_pages_url + "responsible-play";
  static String how_to_play_url = web_pages_url + "how-to-play";
  static String legality_url = web_pages_url + "legalities-app";
  static String apk_url = "";
  static String apk_refer_url = "";
  static String fb_url = "https://www.facebook.com/profile.php?id=61554477293614&mibextid=zLoPMf";
  static String rupee ="₹";
  static String invite_bonus = "200";
  static String signup_bonus = "25";
  static String token = "";
  static String cashfree_notify_url = "https://admin.EverPlay.in/admin/cashfree-notify";
  //static String cashfree_notify_url = "https://fantasy.rgisports.com/repos/EverPlay/EverPlay_admin/admin/cashfree-notify";
  static String twitter_url = "https://twitter.com/EverPlaySPL";
  static String instagram_url = "https://www.instagram.com/everplay_fantasy/";
  static String telegram_url = "https://t.me/EverPlayOfficial";
  static String linkdin_url = "https://www.linkedin.com/company/everplay-fantasy/";
  static String whatsapp_url = "https://whatsapp.com/channel/0029VaG0OXr1nozAxzVI2S1K";
  static String cash_free_app_id = "1916626555d506aa26c5cb4aba266191";
  static String paytm_mid = "VISION15147168410696";
  static bool isFilter = false;
  static String versionCode = '0';
  static late BuildContext context;


//  =================== MESSAGES ============================

  static String get getNoInternetMsg => "Internet not available.";

  static String get getTechnicalErrorMsg => "Technical Error";

  static String get notPermittedMsg =>
      "You can't login using these credentials";

  static String get successMsg => "Success";

  static String get somethingWentWrongMsg => "Something Went Wrong";

  static String get invalidCredentialsMsg => "Invalid Credentials";

  static String get technicalErrorMsg => "TECHNICAL ERROR";

//  ==================================== URLS AND OTHER =====================

  static String get textRegular => 'sans-serif';

  static String get textBold => 'MontserratBold';

  static String get textSemiBold => 'sans-serif-medium';

  static String get localNotifStorage => '_localNotifStorage';

  static String get localCouriorStorage => '_localCouriorStorageJSON_ARY';

//  ======================== REPLACEMENTS =======================

  static String get replaceString4 => 'amp;';

  static var channelTypes =['Select Chanel Type','Facebook','YouTube','Twitter','Instagram','Telegram','Other'];

  static var items = ['Select State',
    'Andaman & Nicobar Islands',
    'Arunachal Pradesh',
    'Bihar',
    'Chandigarh',
    'Chhattisgarh',
    'Dadra & Nagar Haveli',
    'Daman & Diu',
    'Delhi',
    'Goa',
    'Gujarat',
    'Haryana',
    'Himachal Pradesh',
    'Jammu & Kashmir',
    'Jharkhand',
    'Karnataka',
    'Kerala',
    'Lakshadweep',
    'Madhya Pradesh',
    'Maharashtra',
    'Manipur',
    'Meghalaya',
    'Mizoram',
    'Pondicherry',
    'Punjab',
    'Rajasthan',
    'Tamil Nadu',
    'Tripura',
    'Uttaranchal',
    'Uttar Pradesh',
    'West Bengal'];

  static final String SHARED_PREFERENCES_IS_LOGGED_IN = "is_logged_in";
  static final String SHARED_PREFERENCE_USER_ID = "user_id";
  static final String SHARED_PREFERENCE_USER_TOKEN = "user_token";
  static final String SHARED_PREFERENCE_USER_NAME = "user_name";
  static final String SHARED_PREFERENCE_USER_EMAIL = "user_email";
  static final String SHARED_PREFERENCE_USER_MOBILE = "user_mobile";
  static final String SHARED_PREFERENCE_USER_DOB = "user_dob";

  static final String SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS = "user_mobile_verify_status";
  static final String SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS = "user_email_verify_status";
  static final String SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS = "user_pan_verify_status";
  static final String SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS = "user_bank_verify_status";
  static final String LOGIN_REGISTER_TYPE = "login_register_type";
  static final String FROM = "from";

  static final String SHARED_PREFERENCE_USER_FCM_TOKEN = "user_fcm_token";
  static final String SHARED_PREFERENCE_USER_REFER_CODE = "user_refer_code";
  static final String SPORT_KEY = "sport_key";
  static final String MATCH_STATUS = "match_status";
  static final String KEY_BANNER_IMAGE = "banner_image";
  static final String IS_VISIBLE_AFFILIATE = "is_visible_affiliate";
  static final String IS_VISIBLE_PROMOTE = "is_visible_promote";
  static final String IS_VISIBLE_PROMOTER_LEADERBOARD = "is_visible_promoter_leaderboard";
  static final String IS_VISIBLE_PROMOTER_REQUEST = "is_visible_promoter_requested";
  static final String SportTabSlected = "SportTabSlected";

  static final String SHARED_PREFERENCE_USER_TEAM_NAME = "user_team_name";
  static final String SHARED_PREFERENCE_USER_STATE_NAME = "user_state_name";
  static final String SHARED_PREFERENCE_USER_PIC = "user_pic_url";
  static final String SHARED_PREFERENCES_ACCESS_TOKEN = "access_token";
  static final String SHARED_PREFERENCES_CLIENT_ACCESS_TOKEN = "client_access_token";
  static final String SHARED_SPORTS_LIST = "SHARED_SPORTS_LIST";
  static final String SERVER_DATE_TIME = "SERVER_DATE_TIME";

  static final String FILTER_CONDITION = "filter_condition";

  static final String IS_MULTI_SPORTS_ENABLE = "IS_MULTI_SPORTS_ENABLE";
  static final String MULTI_SPORTS = "MULTI_SPORTS";
  static final String AUTHTOKEN = "AUTHTOKEN";
  static final String VERIFIED = "Verified";
  static final String UNVERIFIED = "Unverified";
  static final String PAYTM = "paytm";
  static final String NO_TEAM_CREATED = "You have not created any team yet !!";
  static final String SOMETHING_WENT_WRONG = "Oops! Something went Wrong";
  static final int OTP_SEND_TIME = 60 * 1000;
  static final int SPLASH_TIMEOUT = 1500;
  static final String APP_NAME = "EverPlay";
  static final String RAZORPAY = "razorPay";
  static final int MIN_BANK_WITHDRAW_AMOUNT = 200;
  static final int MIN_PAYTM_WITHDRAW_AMOUNT = 200;
  static final int MIN_BANK_INSTANT_WITHDRAW_AMOUNT = 200;

  static final int MAX_PAYTM_WITHDRAW_AMOUNT = 10000;
  static final int MAX_BANK_WITHDRAW_AMOUNT = 200000;
  static final int MAX_BANK_INSTANT_WITHDRAW_AMOUNT = 50000;
  static final double DEFAULT_PERCENT = 1.15;


  static final String API_VERSION = "1.1";
  static final String ACCEPT_HEADER = "application/vnd.md.api.v" + API_VERSION + "+json";
  static final String KEY_WINING_PERCENT = "key_winning_percent";


  static final int KEY_LIVE_MATCH = 1;
  static final int KEY_UPCOMING_MATCH = 2;
  static final int KEY_FINISHED_MATCH = 3;

  //PAN STATUS
  static final int KEY_PAN_VERIFIED = 1;
  static final int KEY_PAN_UNVERIFIED = 0;
  static final int KEY_PAN_NOT_REQUESTED = -1;
  static final int KEY_PAN_REJECTED = 2;

  //BANK STATUS
  static final int KEY_BANK_VERIFIED = 1;
  static final int KEY_BANK_UNVERIFIED = 0;
  static final int KEY_BANK_NOT_REQUESTED = -1;
  static final int KEY_BANK_REJECTED = 2;

  //EMAIL STATUS
  static final int KEY_EMAIL_VERIFIED = 1;
  static final int KEY_EMAIL_UNVERIFIED = 0;

  //MOBILE_STATUS
  static final int KEY_MOBILE_VERIFIED = 1;
  static final int KEY_MOBILE_UNVERIFIED = 0;


  //Player Role
  static final String KEY_PLAYER_ROLE_BAT = "batsman";
  static final String KEY_PLAYER_ROLE_ALL_R = "allrounder";
  static final String KEY_PLAYER_ROLE_BOL = "bowler";
  static final String KEY_PLAYER_ROLE_KEEP = "keeper";

  //That use for ic_switch_team data
  static final String KEY_MATCH_KEY = "key_match_key";
  static final String KEY_WINING_AMOUNT = "key_wining_amount";
  static final String KEY_TEAM_VS = "key_team_vs";
  static final String KEY_TEAM_FIRST_URL = "key_team_first_url";
  static final String KEY_TEAM_SECOND_URL = "key_team_second_url";
  static final String KEY_TEAM_ID = "key_team_id";
  static final String KEY_TEAM_ID2 = "key_team_id2";
  static final String KEY_CONTEST_KEY = "key_contest_key";
  static final String KEY_IS_FOR_JOIN_CONTEST = "key_is_for_join_contest";
  static final String KEY_CONTEST_DATA = "key_contest_data";
  static final String KEY_USER_BALANCE = "key_user_balance";
  static final String KEY_USER_BONUS_BALANCE = "key_user_bonus_balance";
  static final String KEY_USER_WINING_AMOUNT = "key_user_wining_amount";
  static final String KEY_USER_TOTAL_BALANCE = "key_user_total_balance";
  static final String CONTEST_ID = "CONTEST_ID";

  //key for fantsy type
  static final String KEY_IS_BATTING_FANTASY = "KEY_IS_BATTING_FANTASY";
  static final String KEY_IS_BOWLING_FANTASY = "KEY_IS_BOWLING_FANTASY";
  static final String KEY_IS_LIVE_FANTASY = "KEY_IS_LIVE_FANTASY";
  static final String KEY_IS_SECOND_INNING_FANTASY = "KEY_IS_SECOND_INNING_FANTASY";
  static final String KEY_IS_REVERSE_FANTASY = "KEY_IS_REVERSE_FANTASY";

  static final int FULL_MATCH_FANTASY_TYPE = 0;
  static final int LIVE_FANTASY_TYPE = 1;
  static final int BATTING_FANTASY_TYPE = 2;
  static final int BOWLING_FANTASY_TYPE = 3;
  static final int SECOND_INNINGS_FANTASY_TYPE = 4;
  static final int REVERSE_FANTASY = 5;

  static final String KEY_FANTASY_TYPE = "KEY_FANTASY_TYPE";
  static final String KEY_FANTASY_SLOT_ID = "KEY_FANTASY_SLOT_ID";

  static final String KEY_FANTASY_SLOTS = "KEY_FANTASY_SLOTS";
  static final String KEY_PROMTER_LEADERBOARD_ID = "key_promoter_leaderboard_id";


  static final String KEY_STATUS_HEADER_TEXT = "key_status_header_text";
  static final String KEY_STATUS_IS_TIMER_HEADER = "key_status_is_timer_text";
  static final String KEY_IS_FOR_SWITCH_TEAM = "key_is_for_switch_team";

  static final String KEY_TEAM_COUNT = "key_team_count";


  static final String KEY_STATUS_IS_FOR_CONTEST_DETAILS = "key_is_for_contest_details";
  static String SKIP_CREATETEAM_INSTRUCTION = "SKIP_CREATETEAM_INSTRUCTION";
  static String SKIP_CREATECVC_INSTRUCTION = "SKIP_CREATECVC_INSTRUCTION";


  static final String KEY_TEAM_LIST_WK = "key_team_list_wk";
  static final String KEY_TEAM_LIST_BAT = "key_team_list_bat";
  static final String KEY_TEAM_LIST_AR = "key_team_list_ar";
  static final String KEY_TEAM_LIST_BOWL = "key_team_list_bowl";
  static final String KEY_TEAM_LIST_C = "KEY_TEAM_LIST_C";
  static final String KEY_TEAM_NAME = "key_team_name";
  static final String KEY_TEAM1_FULL_NAME = "key_team1_full_name";
  static final String KEY_TEAM2_FULL_NAME = "key_team2_full_name";
  static final String KEY_TEAM1_SCORE = "key_team1_score";
  static final String KEY_TEAM2_SCORE = "key_team2_score";
  static final String KEY_TEXT = "key_text";

  static final String KEY_ALL_CONTEST = "key_all_contest";
  static final String ERROR_MSG = "We are facing problem...We will be right back!";

  //TAGS
  static final String TAG_CRICKET = "CRICKET";
  static final String White_Image = "WhiteImage";
  static final String TAG_LIVE = "LIVE";
  static final String TAG_FOOTBALL = "FOOTBALL";
  static final String TAG_BASKETBALL = "BASKETBALL";
  static final String TAG_BASEBALL = "BASEBALL";
  static final String TAG_HANDBALL = "HANDBALL";
  static final String TAG_HOCKEY = "HOCKEY";
  static final String TAG_KABADDI = "KABADDI";

  //CRICKET TAG
  static final String WK = "WK";
  static final String BAT = "BAT";
  static final String AR = "AR";
  static final String BOWL = "BOWL";
  //Player Role for Football
  static final String KEY_PLAYER_ROLE_DEF = "Defender";
  static final String KEY_PLAYER_ROLE_ST = "Forward";
  static final String KEY_PLAYER_ROLE_GK = "Goalkeeper";
  static final String KEY_PLAYER_ROLE_MID = "Midfielder";
  static final String KEY_PLAYER_ROLE_STR = "Striker";
  static final String KEY_PLAYER_ROLE_RD = "raider";
  //FOOTBALL TAG
  static final String GK = "GK";
  static final String DEF = "DEF";
  static final String MID = "MID";
  static final String ST = "ST";
  static final String ALL = "ALL";
  static final String RAIDER = "RAIDER";
  //Player Role for BasketBall(NBA)
  static final String KEY_PLAYER_ROLE_PG = "Point guard";//1-4
  static final String KEY_PLAYER_ROLE_SG = "Shooting guard";//1-4
  static final String KEY_PLAYER_ROLE_SF = "Small forward";//1-4
  static final String KEY_PLAYER_ROLE_PF = "Power forward"; //1-4
  static final String KEY_PLAYER_ROLE_C = "Center"; //1-4

  static final String PG = "PG";
  static final String SG = "SG";
  static final String SF = "SF";
  static final String PF = "PF";
  static final String C = "C";


  //Player Role for BaseBall
  static final String KEY_PLAYER_ROLE_OF = "Outfielder";
  static final String KEY_PLAYER_ROLE_IF = "Infielder";
  static final String KEY_PLAYER_ROLE_PITCHER = "Pitcher";
  static final String KEY_PLAYER_ROLE_CATCHER = "Catcher";
  //BASEBALL TAG
  static final String OF = "OF";
  static final String IF = "IF";
  static final String PIT = "P";
  static final String CAT = "C";


  // Playing Status
  static final String PLAYING = "playing";
  static final String NOT_PLAYING = "not_playing";
  static final String BOTH = "both";

  //Home Pop Up Banner
  static final String PREVIOUS_DATE = "previous_date";
  static final String IS_NIGHT_BANNER_SHOWN = "is_night_banner_shown";
  static final String IS_MORNING_BANNER_SHOWN = "is_morning_banner_shown";
  static final String IS_AFTERNOON_BANNER_SHOWN = "is_afternoon_banner_shown";
  static final String IS_EVENING_BANNER_SHOWN = "is_evening_banner_shown";
  static final String NIGHT_START_TIME = "00:00:00";
  static final String NIGHT_END_TIME = "05:59:59";
  static final String MORNING_START_TIME = "06:00:00";
  static final String MORNING_END_TIME = "11:59:59";
  static final String AFTERNOON_START_TIME = "12:00:00";
  static final String AFTERNOON_END_TIME = "17:59:59";
  static final String EVENING_START_TIME = "18:00:00";
  static final String EVENING_END_TIME = "23:59:59";
}
