import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/ShimmerContestItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/repository/model/category_contest_response.dart';

class MyJoinedContests extends StatefulWidget {
  List<Contest> joinedContestList;
  bool contestLoading;
  GeneralModel model;
  Function onJoinContestResult;
  Function getWinnerPriceCard;
  String userId;
  MyJoinedContests(this.model,this.joinedContestList,this.contestLoading,this.onJoinContestResult,this.userId,this.getWinnerPriceCard);
  @override
  _MyJoinedContestsState createState() => new _MyJoinedContestsState();
}

class _MyJoinedContestsState extends State<MyJoinedContests>{


  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: EdgeInsets.all(8),
      child: !widget.contestLoading?new ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemCount: widget.joinedContestList.length,
          itemBuilder: (BuildContext context, int index) {
            return new ContestItemAdapter(widget.model,widget.joinedContestList[index],widget.onJoinContestResult,widget.userId,widget.getWinnerPriceCard);
          }
      ):new ListView.builder(
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemCount: 7,
          itemBuilder: (BuildContext context, int index) {
            return new ShimmerContestItemAdapter();
          }
      ),
    );
  }
}