import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:webview_flutter/webview_flutter.dart';

class EverplayWebView extends StatefulWidget {
  var title,url;
  EverplayWebView(this.title,this.url);
  @override
  _WebViewState createState() => _WebViewState();
}

class _WebViewState extends State<EverplayWebView> {

  final Completer<WebViewController> _controller =
  Completer<WebViewController>();

  var _stackToView = 1;

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(55), // Set this height
            child: Container(
              // padding: EdgeInsets.only(top: 40),
              color: Colors.black,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  new GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () => {Navigator.pop(context)},
                    child: new Container(
                      padding: EdgeInsets.all(15),
                      alignment: Alignment.centerLeft,
                      child: new Container(
                        width: 16,
                        height: 16,
                        child: Image(
                          image: AssetImage(AppImages.backImageURL),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                  ),
                  new Container(
                    child: new Text(widget.title,
                        style: TextStyle(
                            fontFamily: AppConstants.textBold,
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 18)),
                  ),
                ],
              ),
            ),
          ),
          body: IndexedStack(
            index: _stackToView,
            children: [
              Column(
                children: <Widget>[
                  Expanded(
                      child: WebView(
                        initialUrl: widget.url,
                        javascriptMode: widget.title=='Telegram'?JavascriptMode.disabled:JavascriptMode.unrestricted,
                        onPageFinished: _handleLoad,
                        onWebViewCreated: (WebViewController webViewController) {
                          _controller.complete(webViewController);
                        },
                      )),
                ],
              ),
              Container(
                  child: Center(child: CircularProgressIndicator(),)
              ),
            ],
          )),
    );
  }
}