import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/repository/model/player_points_response.dart';


class BreakupPlayerPoints extends StatefulWidget {
  MultiSportsPlayerPointItem breakupPoint;
  BreakupPlayerPoints(this.breakupPoint);
  @override
  _BreakupPlayerPointsState createState() => new _BreakupPlayerPointsState();
}

class _BreakupPlayerPointsState extends State<BreakupPlayerPoints>{

  TextEditingController codeController = TextEditingController();
  bool _enable=false;

  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: darkPrimaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: primaryColor,
            // padding: EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.white,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Points Breakup',
                      style: TextStyle(
                        fontFamily: AppConstants.textBold,
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 15,)),
                ),
              ],
            ),
          ),
        ),
        body: new Stack(
          alignment: Alignment.bottomCenter,
          children: [
            new Container(
              height: MediaQuery.of(context).size.height,
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10,),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.black,
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: new Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          new Container(
                            decoration: BoxDecoration(
                              //shape: BoxShape.circle,
                              // color: Colors.grey,
                            ),
                            alignment: Alignment. bottomCenter,
                            width: 80,
                            height: 70,
                            margin: EdgeInsets.all(10),
                            child:  Align(
                              alignment: Alignment.bottomCenter,
                              child: CachedNetworkImage(
                                imageUrl: widget.breakupPoint.image!,
                                placeholder: (context,url)=> new Image.asset(AppImages.userAvatarIcon),
                                errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon),
                              ),
                            ),
                          ),
                          new Column(
                            //  crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [

                              // new Container(
                              //   margin: EdgeInsets.only(bottom: 4),
                              //   child: new Text( widget.breakupPoint.short_name!=null&&widget.breakupPoint.role!=null?widget.breakupPoint.short_name!+'-'+widget.breakupPoint.role!:'',
                              //       style: TextStyle(
                              //           color: Colors.grey,
                              //           fontWeight: FontWeight.w400,
                              //           fontSize: 11)),
                              // ),
                              new Row(
                                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  new Container(
                                    // margin: EdgeInsets.only(right: 40),
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(bottom: 4),
                                          child: new Text('SELECTED BY',
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12)),
                                        ),

                                        new Container(
                                          child: new Text(widget.breakupPoint.selected_by!+"%",
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 17)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: 10,),
                                  Container(
                                    height: 40,color: Colors.white,width: 1,
                                  ),
                                  SizedBox(width: 10,),
                                  new Container(
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(bottom: 4),
                                          child: new Text('CREDITS',
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12)),
                                        ),
                                        new Container(
                                          child: new Text(widget.breakupPoint.credit.toString()+"%",
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 17)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: 10,),
                                  Container(
                                    height: 40,color: Colors.white,width: 1,
                                  ),
                                  SizedBox(width: 10,),
                                  new Container(
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(bottom: 4),
                                          child: new Text('POINTS',
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12)),
                                        ),
                                        new Container(
                                          child: new Text(widget.breakupPoint.points.toString(),
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 17)),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              // new Container(
                              //   margin: EdgeInsets.only(top: 6,bottom: 5),
                              //   child: new Row(
                              //     children: [
                              //       new Container(
                              //         margin: EdgeInsets.only(right: 4),
                              //         child: new Text(widget.breakupPoint.isSelected==1?'In your team':'Not in your team',
                              //             style: TextStyle(
                              //                 color: Colors.grey,
                              //                 fontWeight: FontWeight.w400,
                              //                 fontSize: 12)),
                              //       ),
                              //       new Container(
                              //         width: 16,
                              //         height: 16,
                              //         child: Image(
                              //           image: AssetImage(widget.breakupPoint.isSelected==1?AppImages.greenCircleTickIcon:AppImages.greyCircleTickIcon),
                              //           fit: BoxFit.fill,),
                              //       ),
                              //
                              //     ],
                              //   ),
                              // )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(bottom: 2,left: 11),
                    child: new Text(widget.breakupPoint.player_name!,
                        style: TextStyle(
                            fontFamily: AppConstants.textBold,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 16)),
                  ),
                  SizedBox(height: 8,),
                  new Divider(height: 1,thickness: 1,),
                  new Container(
                    height: 30,
                    color: lightGrayColor,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        new Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Text(
                            'Event',
                            style: TextStyle(
                                color: Colors.grey,
                                fontWeight: FontWeight.w400,
                                fontSize: 12),
                          ),
                        ),
                        new Row(
                          children: [
                            new Container(
                              width: 100,
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Actual',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12),
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.only(right: 10,left: 0),
                              width: 50,
                              alignment: Alignment.center,
                              child: Text(
                                'Points',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  new Divider(height: 1,thickness: 1,),
                  new Expanded(
                    child: new ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: widget.breakupPoint.breakup_points!.length,
                        itemBuilder: (BuildContext context, int index) {
                          return new Column(
                            children: [
                              new Container(
                                padding: EdgeInsets.only(left: 10,right: 0,top: 10,bottom: 10),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    new Container(
                                      child:  new Text(
                                        widget.breakupPoint.breakup_points![index].event_name!,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 13,
                                            fontWeight: widget.breakupPoint.breakup_points![index].event_name=='Total points'?FontWeight.bold: FontWeight.w400
                                        ),
                                      ),
                                    ),
                                    new Row(
                                      children: [
                                        new Container(
                                          width: 100,
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            widget.breakupPoint.breakup_points![index].actual!.toString(),
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: widget.breakupPoint.breakup_points![index].event_name=='Total points'?FontWeight.bold: FontWeight.w400,
                                                fontSize: 13),
                                          ),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(right: 10,left: 0),
                                          width: 50,
                                          alignment: Alignment.center,
                                          child: Text(
                                            widget.breakupPoint.breakup_points![index].actual_points!.toString(),
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: widget.breakupPoint.breakup_points![index].event_name=='Total points'?FontWeight.bold: FontWeight.w400,
                                                fontSize: 13),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              new Divider(height: 1,thickness: 1,),
                            ],
                          );
                        }
                    ),
                  ),
                ],
              ),
            ),
            new Container(
              height: 40,
              alignment: Alignment.center,
              color: Colors.black,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new Container(
                        width: 16,
                        height: 16,
                        child: Image(
                          image: AssetImage(AppImages.greenCircleTickIcon),
                          fit: BoxFit.fill,),
                      ),
                      new Container(
                        margin: EdgeInsets.only(left: 10),
                        child: new Text('Your Players',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 12)),
                      ),
                    ],
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      new Container(
                        width: 16,
                        height: 16,
                        // decoration: BoxDecoration(
                        //   shape: BoxShape.circle,color: Colors.pink,
                        // ),
                        child: Image(
                          image: AssetImage(AppImages.star),
                          fit: BoxFit.contain,

                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only(left: 10),
                        child: new Text('Top Players',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 12)),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}