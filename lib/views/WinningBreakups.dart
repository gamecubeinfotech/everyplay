import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/repository/model/score_card_response.dart';
import 'package:EverPlay/views/WinningBreakupManager.dart';

class WinningBreakups extends StatefulWidget {
  List<WinnerScoreCardItem> breakupList;

  WinningBreakups(this.breakupList);

  @override
  _WinningBreakupsState createState() => new _WinningBreakupsState();
}

class _WinningBreakupsState extends State<WinningBreakups> {


  @override
  void initState() {
    super.initState();

  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      color: Colors.white,
        child: new Column(
            children: [
              new Divider(height: 1,thickness: 1,),
              new Container(
                height: 40,
                color: Colors.white,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    new Container(
                      margin: EdgeInsets.only(left: 10),
                      child: Text(
                        'RANK',
                        style: TextStyle(
                            color: textGrey,
                            fontWeight: FontWeight.w500,
                            fontSize: 12),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(right: 10),
                      child: Text(
                        'PRIZE',
                        style: TextStyle(
                            color: textGrey,
                            fontWeight: FontWeight.w500,
                            fontSize: 12),
                      ),
                    ),
                  ],
                ),
              ),
              new Divider(height: 1,thickness: 1,),
              Expanded(
                child:  ListView.builder(padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount: widget.breakupList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return  Container(
                        color: TextFieldColor,
                        margin: EdgeInsets.only(bottom: 5),
                        height: 40,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                widget.breakupList[index].start_position!,
                                style: TextStyle(
                                    color: TextFieldTextColor,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14),
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.only(right: 10),
                              child: Text(
                                '₹'+widget.breakupList[index].price!.toString(),
                                style: TextStyle(
                                    color: TextFieldTextColor,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14),
                              ),
                            ),
                          ],
                        ),
                      );
                    }
                ),
              ),
              // new Container(
              //   padding: EdgeInsets.all(5),
              //   child: Text(
              //     "Note : The actual prize money may be different then the prize money mentioned above if there is a tie for any of the winning positions. Check FAQ\'s for further details. As per government regulations, a tax of 30% will be deducted if an individual wins more than Rs. 10,000.",
              //     style: TextStyle(
              //         color: Colors.grey,
              //         fontWeight: FontWeight.w400,
              //         fontSize: 12),
              //   ),
              // ),
            ],
        )
    );
  }

}