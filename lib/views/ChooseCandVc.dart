import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:EverPlay/adapter/CandVcPlayerItemAdapter.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/date_utils.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';

import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/dataModels/JoinChallengeDataModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/create_team_request.dart';
import 'package:EverPlay/repository/model/create_team_response.dart';
import 'package:EverPlay/repository/model/team_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';


class ChooseCandVc extends StatefulWidget {
  GeneralModel model;
  ChooseCandVc(this.model);
  @override
  _ChooseCandVcState createState() => new _ChooseCandVcState();
}

class _ChooseCandVcState extends State<ChooseCandVc> {
  late int batC, wkC, alC, bwC;
  String displayRole='';
  String selectedCaptainName = "";
  String selectedVcCaptainName = "";
  String userId='0';
  @override
  void initState() {
    super.initState();
    captainSelected();
    vcCaptainSelected();
    createHeaders();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
      })
    });
  }

  @override
  void dispose() {

    super.dispose();
  }
  final CustomTimerController controller = CustomTimerController();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Themecolor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
          body: new WillPopScope(
              child: new Stack(
                children: [
                  new Scaffold(
                    backgroundColor: Themecolor,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(55), // Set this height
                      child: Container(
                        color: Colors.black,
                        // padding: EdgeInsets.only(top: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Row(
                              children: [
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: () => {_onWillPop()},
                                  child: new Container(
                                    padding: EdgeInsets.fromLTRB(15, 15, 25, 15),
                                    alignment: Alignment.centerLeft,
                                    child: new Container(
                                      width: 16,
                                      height: 16,
                                      child: Image(
                                        image: AssetImage(AppImages.backImageURL),
                                        fit: BoxFit.fill,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    new Text(widget.model.teamVs.toString(),
                                        style: TextStyle(
                                            fontFamily: AppConstants.textBold,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14)),
                                    new Container(
                                      // margin: EdgeInsets.only(top: 20),
                                      child: CustomTimer(
                                        controller: controller,
                                        from: Duration(milliseconds: SuperDateFormat.getFormattedDateObj(widget.model.headerText!)!.millisecondsSinceEpoch-MethodUtils.getDateTime().millisecondsSinceEpoch),
                                        to: Duration(milliseconds: 0),
                                        onBuildAction: CustomTimerAction.auto_start,
                                        builder: (CustomTimerRemainingTime remaining) {
                                          if(remaining.hours=='00'&&remaining.minutes=='00'&& remaining.seconds=='00')
                                          {
                                            controller.pause();
                                            // // widget.onMatchTimeUp(widget.index);
                                            // widget.getController.TimeFunction(remaining.minutes, remaining.seconds, widget.index);

                                          }
                                          return new GestureDetector(
                                            behavior: HitTestBehavior.translucent,
                                            child: Text(
                                              int.parse(remaining.days)>=1?int.parse(remaining.days)>1?"${remaining.days} Days Left":" ${remaining.days} Day Left":int.parse(remaining.hours)>=1?"${remaining.hours}h : ${remaining.minutes}m Left":"${remaining.minutes}m : ${remaining.seconds}s Left",
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  decoration: TextDecoration.none,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 12),
                                            ),
                                            onTap: (){
                                            },
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    body: new Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        new Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          color: Themecolor,
                          child: new Column(
                            children: [
                              // new Container(
                              //   padding: EdgeInsets.all(0),
                              //   margin: EdgeInsets.only(
                              //       left: 10, top: 15, bottom: 13, right: 10),
                              //   child: new Row(
                              //     mainAxisAlignment:
                              //     MainAxisAlignment.spaceEvenly,
                              //     children: [
                              //       new Container(
                              //           child: new Column(
                              //               crossAxisAlignment:
                              //               CrossAxisAlignment.start,
                              //               children: [
                              //                 new Row(
                              //                   mainAxisAlignment:
                              //                   MainAxisAlignment.start,
                              //                   children: [
                              //                     new Container(
                              //                       margin: EdgeInsets.only(right: 10),
                              //                       child: new Column(
                              //                         crossAxisAlignment:
                              //                         CrossAxisAlignment.start,
                              //                         children: [
                              //                           new Container(
                              //                             margin:
                              //                             EdgeInsets.only(top: 2),
                              //                             child: new Text(
                              //                               widget.model.teamVs!.split(' VS ')[0],
                              //                               textAlign: TextAlign.left,
                              //                               style: TextStyle(
                              //                                   fontFamily: 'noway',
                              //                                   color: Colors.white,
                              //                                   fontWeight:
                              //                                   FontWeight.w600,
                              //                                   fontSize: 14),
                              //                             ),
                              //                           )
                              //                         ],
                              //                       ),
                              //                     ),
                              //                     new Column(
                              //                       children: [
                              //                         new Container(
                              //                           height: 40,
                              //                           width: 40,
                              //                           margin: EdgeInsets.only(
                              //                               bottom: 5),
                              //                           child: CachedNetworkImage(
                              //                             imageUrl: widget.model.team1Logo!,
                              //                             placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                              //                             errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                              //                             fit: BoxFit.fill,
                              //                           ),
                              //                         ),
                              //                       ],
                              //                     ),
                              //                   ],
                              //                 ),
                              //               ])),
                              //       new Container(
                              //         margin: EdgeInsets.only(top: 2),
                              //         child: new Column(
                              //           children: [
                              //             new Container(
                              //               child: new Text(
                              //                 'VS',
                              //                 textAlign: TextAlign.left,
                              //                 style: TextStyle(
                              //                     color: Colors.white,
                              //                     fontWeight: FontWeight.w500,
                              //                     fontSize: 16),
                              //               ),
                              //             )
                              //           ],
                              //         ),
                              //       ),
                              //       new Container(
                              //           child: new Column(
                              //             crossAxisAlignment: CrossAxisAlignment.end,
                              //             children: [
                              //               new Row(
                              //                 children: [
                              //
                              //
                              //                   new Column(
                              //                     children: [
                              //                       new Container(
                              //                         height: 40,
                              //                         width: 40,
                              //                         margin: EdgeInsets.only(
                              //                           bottom: 5,
                              //                         ),
                              //                         child: CachedNetworkImage(
                              //                           imageUrl: widget.model.team2Logo!,
                              //                           placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                              //                           errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                              //                           fit: BoxFit.fill,
                              //                         ),
                              //                       ),
                              //                     ],
                              //                   ),
                              //                   new Container(
                              //                     margin: EdgeInsets.only(left: 10),
                              //                     child: new Column(
                              //                       crossAxisAlignment:
                              //                       CrossAxisAlignment.start,
                              //                       children: [
                              //                         new Container(
                              //                           margin:
                              //                           EdgeInsets.only(top: 2),
                              //                           child: new Text(
                              //                             widget.model.teamVs!.split(' VS ')[1],
                              //                             textAlign: TextAlign.left,
                              //                             style: TextStyle(
                              //                                 fontFamily: 'noway',
                              //                                 color: Colors.white,
                              //                                 fontWeight:
                              //                                 FontWeight.w600,
                              //                                 fontSize: 14),
                              //                           ),
                              //                         )
                              //                       ],
                              //                     ),
                              //                   ),
                              //                 ],
                              //               ),
                              //
                              //             ],
                              //           )),
                              //     ],
                              //   ),
                              // ),
                              SizedBox(height: 10,),
                              new Text("CHOOSE YOUR CAPTAIN AND \nVICE CAPTAIN",textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: AppConstants.textBold,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 17)),

                              new Container(
                                margin: EdgeInsets.only(top: 20,bottom: 20),
                                child: new Row(
                                  children: [
                                    new Flexible(
                                      child: new Container(
                                          width: MediaQuery.of(context).size.width,
                                          height: 40,
                                          margin: EdgeInsets.only(left: 10, right: 5),
                                          child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              primary: Colors.white,
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10),
                                              ),
                                            ),
                                            onPressed: () {
                                              // navigateToCreateTeam(context);
                                            },
                                            child: Row(
                                              children: [
                                                Container(
                                                  height: 24,
                                                  width: 24,
                                                  margin: EdgeInsets.only(right: 5),
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    border: Border.all(color: Colors.white),
                                                    borderRadius: BorderRadius.circular(12),
                                                    color: Themecolor,
                                                  ),
                                                  child: Text(
                                                    'C',
                                                    style: TextStyle(
                                                      fontFamily: 'noway',
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 12,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                                Text(
                                                  'Get 2x points',
                                                  style: TextStyle(fontSize: 14, color: Themecolor),
                                                )
                                              ],
                                            ),
                                          )
                                      ),
                                      flex: 1,
                                    ),
                                    new Flexible(
                                      child: new Container(
                                          width: MediaQuery.of(context).size.width,
                                          height: 40,
                                          margin: EdgeInsets.only(left: 5, right: 10),
                                          child: ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                              primary: yellowdark,
                                              elevation: 0.5,
                                              shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10),
                                              ),
                                            ),
                                            onPressed: () {
                                              // navigateToCreateTeam(context);
                                            },
                                            child: Row(
                                              children: [
                                                Container(
                                                  height: 24,
                                                  width: 24,
                                                  margin: EdgeInsets.only(right: 5),
                                                  alignment: Alignment.center,
                                                  decoration: BoxDecoration(
                                                    border: Border.all(color: Colors.white),
                                                    borderRadius: BorderRadius.circular(12),
                                                    color: Colors.white,
                                                  ),
                                                  child: Text(
                                                    'VC',
                                                    style: TextStyle(
                                                      fontFamily: 'noway',
                                                      color: yellowdark,
                                                      fontWeight: FontWeight.w500,
                                                      fontSize: 12,
                                                    ),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                ),
                                                Text(
                                                  'Get 1.5x points',
                                                  style: TextStyle(fontSize: 14, color: Colors.white),
                                                )
                                              ],
                                            ),
                                          )
                                      ),
                                      flex: 1,
                                    ),
                                  ],
                                ),
                              ),
                              new Divider(height: 1,thickness: 1,),

                              // new Container(
                              //   height: 35,
                              //   color: lightGrayColor,
                              //   margin: EdgeInsets.only(top: 15),
                              //   child: new Row(
                              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              //     children: [
                              //       new Container(
                              //         margin: EdgeInsets.only(left: 10),
                              //         child: Text(
                              //           'Players',
                              //           style: TextStyle(
                              //               color: Colors.grey,
                              //               fontWeight: FontWeight.w400,
                              //               fontSize: 12),
                              //         ),
                              //       ),
                              //       new Row(
                              //         children: [
                              //           new Container(
                              //             width: 70,
                              //             margin: EdgeInsets.only(right:10,left: 0),
                              //             alignment: Alignment.centerLeft,
                              //             child: Text(
                              //               'Points',
                              //               style: TextStyle(
                              //                   color: Colors.grey,
                              //                   fontWeight: FontWeight.w400,
                              //                   fontSize: 12),
                              //             ),
                              //           ),
                              //           new Container(
                              //             margin: EdgeInsets.only(right: 10,left: 0),
                              //             // width: 40,
                              //             alignment: Alignment.center,
                              //             child: Text(
                              //               'Selected %',
                              //               style: TextStyle(
                              //                   color: Colors.grey,
                              //                   fontWeight: FontWeight.w400,
                              //                   fontSize: 12),
                              //             ),
                              //           ),
                              //
                              //         ],
                              //       ),
                              //     ],
                              //   ),
                              // ),
                              new Expanded(
                                child: new Container(
                                  color: Colors.white,
                                  child: new ListView.builder(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.vertical,
                                      itemCount: widget.model.selectedList!.length,
                                      padding: EdgeInsets.only(bottom: 70),
                                      itemBuilder: (BuildContext context, int index) {
                                        bool isdisplayRole=isDisplayRole(index);
                                        return CandVcPlayerItemAdapter(widget.model.selectedList![index],captainViceCaptainListener,index,isdisplayRole,displayRole,widget.model.matchKey,widget.model.sportKey,widget.model.fantasyType,widget.model.slotId);
                                      }
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.only(bottom: 20),
                          child: new Row(
                            children: [
                              new Flexible(
                                child: new Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 40,
                                    margin: EdgeInsets.only(left: 20, right: 10),
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        primary: whiteColor,
                                        elevation: 0.5,
                                        side: BorderSide(width: 1.0,color:Themecolor),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(40),
                                        ),
                                      ),
                                      onPressed: () {
                                        List<Player> selectedWkList = [];
                                        List<Player> selectedBatList = [];
                                        List<Player> selectedArList = [];
                                        List<Player> selectedBowlList = [];
                                        List<Player> selectedCList = [];

                                        for (Player player in widget.model.selectedList!) {
                                          if (player.isSelected ?? false) {
                                            if (player.role.toString().toLowerCase() == (widget.model.sportKey == AppConstants.TAG_CRICKET ? AppConstants.KEY_PLAYER_ROLE_KEEP : widget.model.sportKey == AppConstants.TAG_BASEBALL ? AppConstants.KEY_PLAYER_ROLE_OF : widget.model.sportKey == AppConstants.TAG_HOCKEY ? AppConstants.KEY_PLAYER_ROLE_GK : widget.model.sportKey == AppConstants.TAG_HANDBALL ? AppConstants.KEY_PLAYER_ROLE_GK : widget.model.sportKey == AppConstants.TAG_KABADDI ? AppConstants.KEY_PLAYER_ROLE_DEF : widget.model.sportKey == AppConstants.TAG_BASKETBALL ? AppConstants.KEY_PLAYER_ROLE_PG : AppConstants.KEY_PLAYER_ROLE_GK).toString().toLowerCase()) {
                                              selectedWkList.add(player);
                                            }
                                            if (player.role.toString().toLowerCase() == (widget.model.sportKey == AppConstants.TAG_CRICKET ? AppConstants.KEY_PLAYER_ROLE_BAT : widget.model.sportKey == AppConstants.TAG_BASEBALL ? AppConstants.KEY_PLAYER_ROLE_IF : widget.model.sportKey == AppConstants.TAG_HOCKEY ? AppConstants.KEY_PLAYER_ROLE_DEF : widget.model.sportKey == AppConstants.TAG_HANDBALL ? AppConstants.KEY_PLAYER_ROLE_DEF : widget.model.sportKey == AppConstants.TAG_KABADDI ? AppConstants.KEY_PLAYER_ROLE_ALL_R : widget.model.sportKey == AppConstants.TAG_BASKETBALL ? AppConstants.KEY_PLAYER_ROLE_SG : AppConstants.KEY_PLAYER_ROLE_DEF).toString().toLowerCase()) {
                                              selectedBatList.add(player);
                                            }
                                            if (player.role.toString().toLowerCase() == (widget.model.sportKey == AppConstants.TAG_CRICKET ? AppConstants.KEY_PLAYER_ROLE_ALL_R : widget.model.sportKey == AppConstants.TAG_BASEBALL ? AppConstants.KEY_PLAYER_ROLE_PITCHER : widget.model.sportKey == AppConstants.TAG_HOCKEY ? AppConstants.KEY_PLAYER_ROLE_MID : widget.model.sportKey == AppConstants.TAG_HANDBALL ? AppConstants.KEY_PLAYER_ROLE_ST : widget.model.sportKey == AppConstants.TAG_KABADDI ? AppConstants.KEY_PLAYER_ROLE_RD : widget.model.sportKey == AppConstants.TAG_BASKETBALL ? AppConstants.KEY_PLAYER_ROLE_SF : AppConstants.KEY_PLAYER_ROLE_MID).toString().toLowerCase()) {
                                              selectedArList.add(player);
                                            }
                                            if (player.role.toString().toLowerCase() == (widget.model.sportKey == AppConstants.TAG_CRICKET ? AppConstants.KEY_PLAYER_ROLE_BOL : widget.model.sportKey == AppConstants.TAG_BASEBALL ? AppConstants.KEY_PLAYER_ROLE_CATCHER : widget.model.sportKey == AppConstants.TAG_HOCKEY ? AppConstants.KEY_PLAYER_ROLE_STR : widget.model.sportKey == AppConstants.TAG_BASKETBALL ? AppConstants.KEY_PLAYER_ROLE_PF : AppConstants.KEY_PLAYER_ROLE_ST).toString().toLowerCase()) {
                                              selectedBowlList.add(player);
                                            }
                                            if (player.role.toString().toLowerCase() == AppConstants.KEY_PLAYER_ROLE_C.toString().toLowerCase()) {
                                              selectedCList.add(player);
                                            }
                                          }
                                        }
                                        widget.model.selectedWkList = selectedWkList;
                                        widget.model.selectedBatLiSt = selectedBatList;
                                        widget.model.selectedArList = selectedArList;
                                        widget.model.selectedBowlList = selectedBowlList;
                                        widget.model.selectedcList = selectedCList;
                                        navigateToTeamPreview(context, widget.model);
                                      },
                                      child: Text(
                                        'Team Preview',
                                        style: TextStyle(fontSize: 14, color: Themecolor),
                                      ),
                                    )
                                ),
                                flex: 1,
                              ),

                              new Flexible(
                                child: new Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 40,
                                    margin: EdgeInsets.only(left: 10, right: 20),
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        primary: selectedCaptainName.isNotEmpty && selectedVcCaptainName.isNotEmpty
                                            ? Themecolor
                                            : Themecolor,
                                        elevation: 0.5,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(40),
                                        ),
                                      ),

                                      onPressed: () {
                                        if (selectedCaptainName == "" || selectedVcCaptainName == "") {
                                          MethodUtils.showError(context, "Please select Captain & V. Captain");
                                          return;
                                        }
                                        createTeam();
                                      },
                                      child: Text(
                                        'Save Team',
                                        style: TextStyle(fontSize: 14, color: Colors.white),
                                      ),
                                    )
                                ),
                                flex: 1,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              onWillPop: _onWillPop)),
    );
  }

  Future<bool> _onWillPop() async {
    for (int i = 0; i < widget.model.selectedList!.length; i++) {
      widget.model.selectedList![i].isCaptain=false;
      widget.model.selectedList![i].isVcCaptain=false;
    }
    Navigator.pop(context);
    return Future.value(false);
  }

  void createTeam() async {
    AppLoaderProgress.showLoader(context);
    StringBuffer sb = new StringBuffer();
    String captain = "";
    String vcCaptain = "";

    for (Player player in widget.model.selectedList!) {
      if (!(player.isHeader??false))
        sb.write(player.id.toString());
      sb.write(",");

      if (player.isCaptain??false)
        captain = player.id.toString();

      if (player.isVcCaptain??false)
        vcCaptain = player.id.toString();
    }
    CreateTeamRequest loginRequest = new CreateTeamRequest(userid: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.slotId.toString(),teamid: widget.model.teamId??0,captain: captain,vicecaptain: vcCaptain,players: sb.toString());
    final client = ApiClient(AppRepository.dio);
    CreateTeamResponse response = await client.createTeam(loginRequest);
    AppLoaderProgress.hideLoader(context);

    if (response.status == 1) {
      if(widget.model.contest!=null&&widget.model.contest!.id!>0&&response.result!=null&&(widget.model.contest!.multi_entry==0?response.result!.teamcount==1?true:false:true)){
        MethodUtils.checkBalance(new JoinChallengeDataModel(context, 0, int.parse(userId), widget.model.contest!.id!, widget.model.fantasyType!, widget.model.slotId!, 1, widget.model.contest!.is_bonus!, widget.model.contest!.win_amount!, widget.model.contest!.maximum_user!, response.result!.teamid.toString(), widget.model.contest!.entryfee.toString(), widget.model.matchKey.toString(), widget.model.sportKey.toString(), widget.model.joinedSwitchTeamId.toString(), false, 0, 0,onJoinContestResult));
      }
      else{
        if(widget.model.onTeamCreated!=null) {
          widget.model.onTeamCreated!();
        }
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      }
    }
    Fluttertoast.showToast(msg: response.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
  }

  void captainViceCaptainListener(bool isCaptain,int index){
    if(isCaptain){
      for (int i = 0; i < widget.model.selectedList!.length; i++) {
        widget.model.selectedList![i].isCaptain=false;
      }

      if (widget.model.selectedList![index].isVcCaptain??false) {
        // isVice = false;
        selectedVcCaptainName = "";
        widget.model.selectedList![index].isVcCaptain=false;
      }

      widget.model.selectedList![index].isCaptain=true;

      selectedCaptainName = widget.model.selectedList![index].name!;
    }else{
      for (int i = 0; i < widget.model.selectedList!.length; i++) {
        widget.model.selectedList![i].isVcCaptain=false;
      }
      if (widget.model.selectedList![index].isCaptain??false) {
        // isCap = false;
        selectedCaptainName = "";
        widget.model.selectedList![index].isCaptain=false;
      }

      widget.model.selectedList![index].isVcCaptain=true;
      selectedVcCaptainName = widget.model.selectedList![index].name!;
    }
    setState(() {});
  }
  void createHeaders() {
    batC = 0;
    wkC = 0;
    alC = 0;
    bwC = 0;
    for (int i = 0; i < widget.model.selectedList!.length; i++) {
      if (widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_KEEP.toString().toLowerCase() || widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_GK.toString().toLowerCase() ||
          widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_PG.toString().toLowerCase() ||
          widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_OF.toString().toLowerCase()) {
        batC = wkC += 1;
      } else if (widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_BAT.toString().toLowerCase() || widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_DEF.toString().toLowerCase() ||
          widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_SG.toString().toLowerCase() ||
          widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_IF.toString().toLowerCase()) {
        alC = batC += 1;
      } else if (widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_ALL_R.toString().toLowerCase() || widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_MID.toString().toLowerCase() ||
          widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_SF.toString().toLowerCase() ||
          widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_PITCHER.toString().toLowerCase()) {
        bwC = alC += 1;
      } else if (widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_PF.toString().toLowerCase()) {
        bwC += 1;
      } else if (widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_BOL.toString().toLowerCase() || widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_ST.toString().toLowerCase() ||
          widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_C.toString().toLowerCase() ||
          widget.model.selectedList![i].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_CATCHER.toString().toLowerCase()) {
        break;
      }
    }
  }

  bool isDisplayRole(int index){
    if (index + 1 < widget.model.selectedList!.length) {
      displayRole=widget.model.selectedList![index+1].display_role!;
      if (index + 1 == wkC) {
        return true;
      } else if (index + 1 == batC) {
        return true;
      } else if (index + 1  == alC) {
        return true;
      } else if (index + 1 == bwC && widget.model.selectedList![index].role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_PF.toString().toLowerCase()) {
        return true;
      } else {
        return false;
      }
    }
    return false;
  }
  void captainSelected() {
    for (var player in widget.model.selectedList!) {
      if(player.isCaptain??false) {
        selectedCaptainName=player.name!;
        break;
      }

    }
  }
  void vcCaptainSelected() {
    for (var player in widget.model.selectedList!) {
      if(player.isVcCaptain??false) {
        selectedVcCaptainName=player.name!;
        break;
      }
    }
  }
  void onJoinContestResult(int isJoined,String referCode){
    Navigator.of(context).pop();
    Navigator.of(context).pop();
    if(isJoined==1){
      widget.model.onJoinContestResult!(isJoined,referCode);
      if(widget.model.onTeamCreated!=null){
        widget.model.onTeamCreated!();
      }
    }
  }
}
