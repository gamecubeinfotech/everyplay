import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/user_details_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  UserDetailValue userDetailValue = new UserDetailValue();
  TextEditingController nameController = TextEditingController();
  TextEditingController teamNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController changeController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController pincodeController = TextEditingController();
  String val='';
  var groupValue;
  var _dropDownValue;
  late String userId = '0';

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) => {
              setState(() {
                userId = value;
                getFullUserDetails();
              })
            });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: primaryColor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.light) /* set Status bar icon color in iOS. */
        );
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            // padding: EdgeInsets.only(top: 40),
            color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 15,
                      height: 15,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Update Profile',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 16)),
                ),
              ],
            ),
          ),
        ),
        body: new SingleChildScrollView(
          child: new Stack(
            children: [
              new Container(
                padding: EdgeInsets.all(10),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    new Container(
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'Name',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Text_Color,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.all(10),
                      height: 45,
                      child: TextField(
                        controller: nameController,
                        decoration: InputDecoration(
                          counterText: "",
                          fillColor: Colors.white,
                          filled: true,
                          hintStyle: TextStyle(
                            fontSize: 12,
                            color: Colors.grey,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.bold,
                          ),
                          hintText: 'Enter Your Name',
                          enabledBorder:  OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            // width: 0.0 produces a thin "hairline" border
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                        ),
                      ),
                    ),
                    /* new Container(
                      padding: EdgeInsets.only(left: 10,top: 4),
                      child: Text(
                        'Team Name',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.all(10),
                      height: 45,
                      child: TextField(
                        inputFormatters: <TextInputFormatter>[
                          FilteringTextInputFormatter.allow(RegExp("[0-9a-zA-Z]")),
                        ],
                        readOnly: userDetailValue.teamfreeze==1,
                        controller: teamNameController,
                        decoration: InputDecoration(
                          counterText: "",
                          fillColor: Colors.white,
                          filled: true,
                          hintStyle: TextStyle(
                            fontSize: 12,
                            color: Colors.grey,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.bold,
                          ),
                          hintText: 'Team Name',
                          enabledBorder: const OutlineInputBorder(
                            // width: 0.0 produces a thin "hairline" border
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                        ),
                      ),
                    ),*/
                    new Container(
                      padding: EdgeInsets.only(left: 10,top: 4),
                      child: Text(
                        'Email',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Text_Color,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.all(10),
                      height: 45,
                      child: TextField(
                        readOnly: userDetailValue.emailfreeze==1,
                        controller: emailController,
                        decoration: InputDecoration(
                          counterText: "",
                          fillColor: Colors.white,
                          filled: true,
                          hintStyle: TextStyle(
                            fontSize: 12,
                            color: Colors.grey,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.bold,
                          ),
                          hintText: 'Enter Your Email Address',
                          enabledBorder:  OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            // width: 0.0 produces a thin "hairline" border
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                        ),
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 10,top: 4),
                      child: Text(
                        'Mobile',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Text_Color,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.all(10),
                      height: 45,
                      child: TextField(
                        readOnly: userDetailValue.mobilefreeze==1,
                        controller: mobileController,
                        keyboardType: TextInputType.number,
                        maxLength: 10,
                        decoration: InputDecoration(
                          counterText: "",
                          fillColor: Colors.white,
                          filled: true,
                          prefixIcon: SizedBox(
                            child: Center(
                              widthFactor: 0.0,
                              child: Text('+91',
                                  style: TextStyle(
                                    fontSize: 15,
                                    color: Text_Color,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold,
                                  )),
                            ),
                          ),
                          hintStyle: TextStyle(
                            fontSize: 12,
                            color: Text_Color,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.bold,
                          ),
                          hintText: 'Mobile',
                          enabledBorder:  OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            // width: 0.0 produces a thin "hairline" border
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                        ),
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 10,top: 4),
                      child: Text(
                        'Change Password',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Text_Color,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.all(10),
                      child: new Stack(
                        alignment: Alignment.centerRight,
                        children: [
                          new Container(
                            height: 45,
                            child: TextField(
                              readOnly: true,
                              controller: changeController,
                              decoration: InputDecoration(
                                counterText: "",
                                fillColor: Colors.white,
                                filled: true,
                                hintStyle: TextStyle(
                                  fontSize: 12,
                                  color: Text_Color,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.bold,
                                ),
                                hintText: 'Password',
                                enabledBorder:  OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  // width: 0.0 produces a thin "hairline" border
                                  borderSide: const BorderSide(
                                      color: Colors.grey, width: 0.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                      color: Colors.grey, width: 0.0),
                                ),
                              ),
                            ),
                          ),
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child:  new Container(
                              padding: EdgeInsets.only(right: 10),
                              child: Text(
                                'Change Password',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 12,
                                  color: Themecolor,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                            onTap: (){
                              navigateToChangePassword(context);
                            },
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 10,top: 4),
                      child: Text(
                        'Gender',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Text_Color,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Radio(
                          value: 'male',
                          groupValue: val,
                          activeColor: Colors.green,
                          onChanged: (value) {
                            setState(() {
                              val = 'male';
                            });
                          },
                        ),
                        Text(
                          'Male',
                          style: new TextStyle(
                              fontSize: 14.0,
                              color: Text_Color,
                              fontWeight: FontWeight.w500),
                        ),
                        Radio(
                          value: 'female',
                          activeColor: Colors.green,
                          groupValue: val,
                          onChanged: (value) {
                            setState(() {
                              val = 'female';
                            });
                          },
                        ),
                        Text(
                          'Female',
                          style: new TextStyle(
                              fontSize: 14.0,
                              color: Text_Color,
                              fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 10,top: 4),
                      child: Text(
                        'Date of Birth',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Text_Color,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new IgnorePointer(
                          child: new Container(
                            margin: EdgeInsets.all(10),
                            height: 45,
                            child: TextField(
                              readOnly: true,
                              controller: dobController,
                              decoration: InputDecoration(
                                counterText: "",
                                fillColor: TextFieldColor,
                                filled: true,
                                hintStyle: TextStyle(
                                  fontSize: 12,
                                  color: Text_Color,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.bold,
                                ),
                                hintText: 'Date of Birth',
                                enabledBorder:  OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  // width: 0.0 produces a thin "hairline" border
                                  borderSide:  BorderSide(
                                      color: TextFieldColor, width: 0.0),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: BorderSide(
                                      color: TextFieldColor, width: 0.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                        onTap: () async {
                          if (userDetailValue.dobfreeze != 1) {
                            FocusScope.of(context).requestFocus(new FocusNode());
                            await showDatePicker(
                                context: context,
                                initialDate:DateTime(DateTime.now().year-18,DateTime.now().month,DateTime.now().day),
                                firstDate:DateTime(1900),
                                lastDate: DateTime(DateTime.now().year-18,DateTime.now().month,DateTime.now().day)).then((value) => {
                              dobController.text = DateFormat("dd/MM/yyyy").format(value!)
                            });
                          }
                        }),
                    new Container(
                      padding: EdgeInsets.only(left: 10,top: 4),
                      child: Text(
                        'City',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Text_Color,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.all(10),
                      height: 45,
                      child: TextField(
                        controller: cityController,
                        decoration: InputDecoration(
                          counterText: "",
                          fillColor: Colors.white,
                          filled: true,
                          hintStyle: TextStyle(
                            fontSize: 12,
                            color: Text_Color,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.bold,
                          ),
                          hintText: 'City',
                          enabledBorder:  OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            // width: 0.0 produces a thin "hairline" border
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                        ),
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.only(left: 10,top: 4),
                      child: Text(
                        'Pin Code',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Text_Color,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.all(10),
                      height: 45,
                      child: TextField(
                        controller: pincodeController,
                        decoration: InputDecoration(
                          counterText: "",
                          fillColor: Colors.white,
                          filled: true,
                          hintStyle: TextStyle(
                            fontSize: 12,
                            color: Text_Color,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.bold,
                          ),
                          hintText: 'Pin Code',
                          enabledBorder:  OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            // width: 0.0 produces a thin "hairline" border
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                            const BorderSide(color: Colors.grey, width: 0.0),
                          ),
                        ),
                      ),
                    ),

                    new Container(
                      padding: EdgeInsets.only(left: 10,top: 4),
                      child: Text(
                        'State',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Text_Color,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),

                    new Container(
                      margin: EdgeInsets.all(10),
                      padding: EdgeInsets.all(0),
                      child: new DropdownButtonFormField(
                        isDense: true,
                        hint: _dropDownValue == null
                            ? Text('Select State',
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal,
                            ))
                            : Text(
                          _dropDownValue,
                          style: TextStyle(
                            fontSize: 14,
                            color: Text_Color,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                          ),
                        ),
                        isExpanded: true,
                        iconSize: 0.0,
                        style: TextStyle(color: Colors.black, fontSize: 14),
                        items: AppConstants.items.map(
                              (val) {
                            return DropdownMenuItem<String>(
                              value: val,
                              child: Text(val),
                            );
                          },
                        ).toList(),
                        onChanged:userDetailValue.statefreeze!=1? (val) {
                          setState(
                                () {
                              _dropDownValue = val;
                            },
                          );
                        }:null,
                        decoration: InputDecoration(
                          counterText: "",
                          fillColor: Colors.white,
                          filled: true,
                          hintStyle: TextStyle(
                            fontSize: 14,
                            color: Colors.grey,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.bold,
                          ),
                          enabledBorder:  OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            // width: 0.0 produces a thin "hairline" border
                            borderSide:
                            BorderSide(color: bordercolor, width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(

                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(width: 1, color: bordercolor),
                          ),
                          disabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide:
                            BorderSide(width: 1, color: Colors.orange),
                          ),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                              BorderSide(width: 1, color: Colors.red)),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide:
                              BorderSide(width: 1, color: Colors.red)),
                        ),
                      ),
                    ),
                    new Container(
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        child: ElevatedButton(
                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))) ,
                          child: Text(
                            'Update Profile',
                            style: TextStyle(fontSize: 14,color: Colors.white),
                          ),
                          onPressed: () {
                            updateUserProfile();
                          },
                        )),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void getFullUserDetails() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    GetUserFullDetailsResponse userFullDetailsResponse =
        await client.getFullUserDetails(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (userFullDetailsResponse.status == 1) {
      userDetailValue = userFullDetailsResponse.result!.value!;
      nameController.text = userDetailValue.username!;
      emailController.text = userDetailValue.email!;
      mobileController.text = userDetailValue.mobile!.toString();
      teamNameController.text = userDetailValue.team!;
      dobController.text = userDetailValue.dob!;
      cityController.text = userDetailValue.city!;
      pincodeController.text = userDetailValue.pincode!;
      val=userDetailValue.gender!;
      for (int i = 0; i < AppConstants.items.length; i++) {
        if (AppConstants.items[i] == userDetailValue.state)
          _dropDownValue = AppConstants.items[i];
      }
    }
    setState(() {});
  }

  void updateUserProfile() async {
    if(val==''){
      MethodUtils.showError(context, "Please select your gender.");
      return;
    }else if(nameController.text.isEmpty){
      MethodUtils.showError(context, "Please enter valid name.");
      return;
    }else if(teamNameController.text.length<4||teamNameController.text.length>15){
      MethodUtils.showError(context, "Team name should be 3 to 15 characters.");
      return;
    }else if(!emailController.text.contains("@")||!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailController.text)){
      MethodUtils.showError(context, "Please enter valid email address.");
      return;
    }else if(mobileController.text.length<10){
      MethodUtils.showError(context, "Please enter valid mobile number.");
      return;
    }else if(_dropDownValue==null || _dropDownValue=='Select State'){
      MethodUtils.showError(context, "Please select your state.");
      return;
    }
    FocusScope.of(context).unfocus();
    AppLoaderProgress.showLoader(context);
    UserDetailValue loginRequest = new UserDetailValue(user_id: userId,username: nameController.text.toString(),dob: dobController.text.toString(),gender: val,city: cityController.text.toString(),state: _dropDownValue,pincode: pincodeController.text.toString(),team: teamNameController.text.toString(),email: emailController.text.toString(),mobile: int.parse(mobileController.text.toString()));
    final client = ApiClient(AppRepository.dio);
    GeneralResponse userFullDetailsResponse = await client.updateUserProfile(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (userFullDetailsResponse.status == 1) {
      MethodUtils.showSuccess(context, "Profile Updated Successfully.");
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_NAME, nameController.text.toString());
      return;
    }
    MethodUtils.showError(context, userFullDetailsResponse.message);
  }
}
