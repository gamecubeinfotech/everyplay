import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/teamname_update_request.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:http/http.dart' as http;
import '../appUtilities/app_colors.dart';
import '../repository/model/base_request.dart';
import '../repository/model/refer_code_response.dart';

class SetTeamName extends StatefulWidget {
  @override
  _SetTeamNameState createState() => _SetTeamNameState();
}

class _SetTeamNameState extends State<SetTeamName> {
  var _dropDownValue;
  bool _isMobileLogin=true;
  bool isCodeVisible = false;
  String teamName = '';
  String state = '';
  String userId = '0';
  TextEditingController nameController = TextEditingController();
  TextEditingController codeController = TextEditingController();

  @override
  void initState() {
    super.initState();/* set Status bar icon color in iOS. */

    AppPrefrence.getString(AppConstants.LOGIN_REGISTER_TYPE).then((value) =>
    {
      setState(() {
        if(value=='manual_register'){
          isCodeVisible=false;
        }else{
          isCodeVisible=true;
        }
      })
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME).then((value) =>
    {
      setState(() {
        teamName = value;
        nameController.text=teamName;
      })
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_STATE_NAME).then((value) =>
    {
      setState(() {
        state = value;
      })
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID).then((value) =>
    {
      setState(() {
        userId = value;
        getPublicIP().then((value) => {getRefercode(value)});
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.light) /* set Status bar icon color in iOS. */
    );

    return SafeArea(
      child: new Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(55), // Set this height
            child: Container(
              //height: 500,
              color: Colors.white,
              // padding: EdgeInsets.only(top: 40),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  new GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    // onTap: () => {Navigator.pop(context)},
                    child: new Container(
                      padding: EdgeInsets.all(15),
                      alignment: Alignment.centerLeft,
                      child: new Container(
                        width: 16,
                        height: 16,
                        child: Image(
                          image: AssetImage(AppImages.backImageURL),
                          fit: BoxFit.fill,
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                  new Container(
                    child: new Text(teamName.isEmpty?'Set Team Name':'Update State',
                        style: TextStyle(
                            fontFamily: AppConstants.textBold,
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                            fontSize: 18)),
                  ),
                ],
              ),
            ),
          ),
        backgroundColor: Colors.white,
        body: new WillPopScope(
          child: new SingleChildScrollView(
          child: new Stack(
            children: [
              new Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                     /* new Container(
                        child: new Column(
                          children: [
                            Text(
                              teamName.isEmpty?'Set Team Name':'Update State',
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                fontSize: 20,
                                color: _isMobileLogin?Colors.black:Colors.grey,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                          crossAxisAlignment:
                          CrossAxisAlignment.center,
                        ),
                      ),*/
                      Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Image.asset(AppImages.teamName,scale: 2,height: 200),
                            Container(
                              child: new Text('Team Name',
                                  style: TextStyle(
                                      color: Title_Color_1,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 24)),margin: EdgeInsets.only(left: 16),),
                            Padding(
                              padding: const EdgeInsets.only(left: 16.0,top: 8),
                              child: new Text('Set a name for your Ever Play Fantasy Cricket team.',
                                  style: TextStyle(
                                      fontFamily: AppConstants.textBold,
                                      color: Text_Color,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 12)),
                            ),
                          ],
                        ),
                      ),

                      Container(
                        margin: EdgeInsets.only(top: 24,bottom: 8,left: 8,right: 8),
                        padding: const EdgeInsets.all(8.0),
                        child: new Container(
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Container(
                                margin: EdgeInsets.fromLTRB(10, 20, 0, 0),
                                child: Text(
                                  'Set Team Name',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              teamName.isEmpty?new Container(
                                height: 70,
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                padding: EdgeInsets.all(10),
                                child: TextField(
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(RegExp("[0-9a-zA-Z]")),
                                  ],
                                  controller: nameController,
                                  decoration: InputDecoration(
                                    isDense: true,
                                    counterText: "",
                                    fillColor: Colors.white,
                                    filled: true,
                                    hintStyle: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    hintText: 'Team Name',
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1,
                                          style: BorderStyle.none,
                                          color: Color(0xFFdbdbdb)
                                      ),
                                      borderRadius:
                                      BorderRadius.circular(10.0),

                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide(width: 1,color: Color(0xFFdbdbdb)),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide(width: 1,color: Colors.orange),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb)),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        borderSide: BorderSide(width: 1,color: Colors.red)
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        borderSide: BorderSide(width: 1,color: Colors.red)
                                    ),
                                  ),

                                ),
                              ):new Container(),
                              teamName.isEmpty?new Container(
                                margin: EdgeInsets.fromLTRB(10,6,10,10),
                                child: new Text(
                                  "Team name can be alphanumeric, must not contain any special characters (expect for underscpre) 3 to 9 characters long",
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 12,

                                    color: Themecolor,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ):

                              !isCodeVisible? new Container(
                                margin: EdgeInsets.fromLTRB(10, 20, 0, 0),
                                child: Text(
                                  'Refer Code',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ):new Container(),
                             new Container(),
                              !isCodeVisible?new Container(
                                margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                padding: EdgeInsets.all(10),
                                child: TextField(
                                  controller: codeController,
                                  decoration: InputDecoration(
                                    counterText: "",
                                    fillColor: textFieldBgColor,
                                    filled: true,
                                    hintStyle: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    hintText: 'Refer Code',
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1,
                                          style: BorderStyle.none,
                                          color: Colors.grey
                                      ),
                                      borderRadius:
                                      BorderRadius.circular(5.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(4)),
                                      borderSide: BorderSide(width: 1,color: Colors.grey),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(4)),
                                      borderSide: BorderSide(width: 1,color: Colors.orange),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(4)),
                                      borderSide: BorderSide(width: 1,color: Colors.grey),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(4)),
                                        borderSide: BorderSide(width: 1,color: Colors.red)
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(4)),
                                        borderSide: BorderSide(width: 1,color: Colors.red)
                                    ),
                                  ),

                                ),
                              ):

                              new Container(
                              ),

                              new Container(
                                margin: EdgeInsets.fromLTRB(10, 20, 0, 0),
                                child: Text(
                                  'Select state',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              new Container(

                                decoration: BoxDecoration(color: backgroundColor,borderRadius: BorderRadius.circular(10)),
                                height: 50,
                                margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                                child: new DropdownButtonFormField(
                                  hint: _dropDownValue == null
                                      ? Text('Select State',style: TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                  ))
                                      : Text(
                                    _dropDownValue,
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                  isExpanded: true,
                                  iconSize: 20.0,iconDisabledColor: Colors.grey,iconEnabledColor: Colors.black,

                                  style: TextStyle(color: Colors.black),
                                  items: AppConstants.items.map(
                                        (val) {
                                      return DropdownMenuItem<String>(
                                        value: val,
                                        child: Text(val),
                                      );
                                    },
                                  ).toList(),
                                  onChanged: (val) {
                                    setState(
                                          () {
                                        _dropDownValue = val;
                                      },
                                    );
                                  },
                                  decoration: InputDecoration(
                                    isDense: true,
                                    counterText: "",
                                    fillColor: backgroundColor,
                                    filled: true,
                                    hintStyle: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          width: 1,
                                          style: BorderStyle.none,
                                          color: Color(0xFFdbdbdb)
                                      ),
                                      borderRadius:
                                      BorderRadius.circular(10.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb)),
                                    ),
                                    disabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide(width: 1,color: Colors.orange),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(Radius.circular(10)),
                                      borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb)),
                                    ),
                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        borderSide: BorderSide(width: 1,color: Colors.red)
                                    ),
                                    focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.all(Radius.circular(10)),
                                        borderSide: BorderSide(width: 1,color: Colors.red)
                                    ),
                                  ),

                                ),
                              ),
                              new Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 50,
                                  margin: EdgeInsets.fromLTRB(10, 40, 10, 10),
                                  child: ElevatedButton(
                                    onPressed: () {
                                      updateTeamName();
                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: primaryColor,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(30),
                                      ),
                                    ),
                                    child: Text(
                                      teamName.isEmpty ? 'Set Team Name' : 'Submit',
                                      style: TextStyle(fontSize: 15),
                                    ),
                                  )
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ), onWillPop:_onWillPop,),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    return Future.value(false);
  }

  void updateTeamName() async {

    if(nameController.text.isEmpty){
      MethodUtils.showError(context, 'Please enter team name.');
      return;
    }
    else if(nameController.text.length<=3||nameController.text.length>=15){
      MethodUtils.showError(context, 'Team name should be 3 to 15 characters.');
      return;
    }
    else if(_dropDownValue==null){
      MethodUtils.showError(context, 'Please select your state.');
      return;
    }

    AppLoaderProgress.showLoader(context);
    TeamNameUpdateRequest loginRequest = new TeamNameUpdateRequest(user_id: userId,state: _dropDownValue,teamname: nameController.text.toString(),user_refer_code: codeController.text.toString(),is_state_update: teamName.isNotEmpty&&state.isEmpty?true:false);
    final client = ApiClient(AppRepository.dio);
    GeneralResponse myBalanceResponse = await client.updateTeamName(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (myBalanceResponse.status == 1) {
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME, teamName);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_STATE_NAME, state);
      Navigator.pop(context);
    }
    Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
  }

  void getRefercode(ip) async {
    setState(() {

    });

    GeneralRequest loginRequest = new GeneralRequest(
        user_id: userId,ip: ip);
    final client = ApiClient(AppRepository.dio);
    ReferCodeResponse  referCodeResponse = await client.getRefercode(loginRequest);
    if (referCodeResponse.status == 1) {
      codeController.text=referCodeResponse.result!.refer_code.toString();
      setState(() {
      });
      // referCodeResponse




    }
    // Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
    });
  }

  Future<String> getPublicIP() async {
    try {
      const url = 'https://api.ipify.org';
      var response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        // The response body is the IP in plain text, so just
        // return it as-is.
        return response.body;
      } else {
        // The request failed with a non-200 code
        // The ipify.org API has a lot of guaranteed uptime
        // promises, so this shouldn't ever actually happen.
        print(response.statusCode);
        print(response.body);
        return '';
      }
    } catch (e) {
      // Request failed due to an error, most likely because
      // the phone isn't connected to the internet.
      print(e);
      return '';

    }
  }

}
