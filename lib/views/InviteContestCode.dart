import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/dataModels/JoinChallengeDataModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/category_contest_response.dart';
import 'package:EverPlay/repository/model/join_contest_by_code_request.dart';
import 'package:EverPlay/repository/model/join_contest_by_code_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class InviteContestCode extends StatefulWidget {
  GeneralModel model;
  InviteContestCode(this.model);
  @override
  _InviteContestCodeState createState() => new _InviteContestCodeState();
}

class _InviteContestCodeState extends State<InviteContestCode>{

  TextEditingController codeController = TextEditingController();
  bool _enable=false;
  String userId='0';

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
      })
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.light) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: WillPopScope(
        child: new Scaffold(
          backgroundColor: Colors.white,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(55),
            // Set this height
            child: Container(
             color: Colors.black,
              // padding: EdgeInsets.only(top: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  new GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () => {Navigator.pop(context)},
                    child: new Container(
                      padding: EdgeInsets.all(15),
                      alignment: Alignment.centerLeft,
                      child: new Container(
                        width: 16,
                        height: 16,
                        child: Image(
                          image: AssetImage(AppImages.backImageURL),
                          fit: BoxFit.fill,color: Colors.white,),
                      ),
                    ),
                  ),
                  new Container(
                    child: new Text('Invite Contest Code',
                        style: TextStyle(
                            fontFamily: AppConstants.textBold,
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 16)),
                  ),
                ],
              ),
            ),
          ),
          body: new Stack(
            children: [
              new Container(
                height: MediaQuery.of(context).size.height,
                child: new SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.only(top: 20,left: 10,right: 10),
                    child: new Container(
                      decoration: BoxDecoration(color: Colors.white,border: Border.all(color: TextFieldBorderColor, ),borderRadius: BorderRadius.circular(10)),
                      child: new Column(
                        children: [
                          new Container(
                            padding: EdgeInsets.only(top: 24),
                            alignment: Alignment.center,
                            child: Text(
                              'CONTEST CODE',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 20,
                                color: Colors.black,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w800,
                              ),
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.only(top: 10,bottom: 20),
                            alignment: Alignment.center,
                            child: Text(
                              'Share Contest code With Your friends.',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 13,
                                color: Colors.grey,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.all(12),
                            child: new Stack(
                              alignment: Alignment.centerRight,
                              children: [
                                new Container(
                                  height: 45,
                                  child: TextField(
                                    controller: codeController,
                                    decoration: InputDecoration(
                                      counterText: "",
                                      fillColor: TextFieldColor,
                                      filled: true,
                                      hintStyle: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                      ),
                                      hintText: 'Enter Contest Code',
                                      enabledBorder:  OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderSide:
                                         BorderSide(color: TextFieldColor, width: 0.0),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(color: Color(0xFF05464545), width: 0.0),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                              width:
                              MediaQuery.of(context).size.width*0.5,
                              height: 45,
                              margin: EdgeInsets.all(20),
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: Themecolor,
                                  elevation: 0.5,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                ),
                                onPressed: () {
                                  if (codeController.text.isEmpty)
                                    MethodUtils.showError(context, "Please enter contest code.");
                                  else {
                                    joinContestByCode();
                                  }
                                },
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                                  child: Text(
                                    'Join This Contest',
                                    style: TextStyle(fontSize: 14, color: Colors.white),
                                  ),
                                ),
                              )
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        onWillPop: (){
          SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
              statusBarColor: primaryColor,
              /* set Status bar color in Android devices. */

              statusBarIconBrightness: Brightness.light,
              /* set Status bar icons color in Android devices.*/

              statusBarBrightness:
              Brightness.dark) /* set Status bar icon color in iOS. */
          );
          return Future.value(false);
        },
      ),
    );
  }

  void joinContestByCode() async {
    AppLoaderProgress.showLoader(context);
    JoinContestByCodeRequest contestRequest = new JoinContestByCodeRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.slotId.toString(),getcode: codeController.text.toString());
    final client = ApiClient(AppRepository.dio);
    JoinContestByCodeResponse response = await client.joinByContestCode(contestRequest);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
    if (response.status == 1) {
        if(response.result![0].message =='Challenge opened'){
          Contest contest=response.result![0].contest!;
          if(widget.model.teamCount==1){
            print("team count if ${widget.model.teamCount}");
            MethodUtils.checkBalance(new JoinChallengeDataModel(context, 0, int.parse(userId), contest.id!,
                widget.model.fantasyType!, widget.model.slotId!, 1, contest.is_bonus!, contest.win_amount!,
                contest.maximum_user!, widget.model.teamId.toString(), contest.entryfee.toString(),
                contest.matchkey!, widget.model.sportKey!, widget.model.joinedSwitchTeamId.toString(), false, 0, 0,onJoinContestResult));

          }

          else if(widget.model.teamCount!>0){
            print("team count else if ${widget.model.teamCount}");
            navigateToMyJoinTeams(context,widget.model,contest,onJoinContestResult);
          }

          else{

            widget.model.onJoinContestResult=onJoinContestResult;
            widget.model.contest=contest;
            widget.model.teamId=0;
            navigateToCreateTeam(context,widget.model);

          }

        }

        else if(response.result![0].message =='Already used'){
          MethodUtils.showError(context, "Invite code already used.");
        }
        else if(response.result![0].message =='Challenge closed'){
          MethodUtils.showError(context, "Sorry, this League is full! Please join another League.");
        }
        else if(response.result![0].message =='invalid code'){
          MethodUtils.showError(context, "Invalid code.");
        }
        else{
          MethodUtils.showError(context, response.result![0].message);

        }
    }
    else{
      MethodUtils.showError(context, response.message);
    }
  }
  void onJoinContestResult(int isJoined,String referCode){
    if(isJoined==1){
      Navigator.pop(context);
      widget.model.onJoinContestResult!(isJoined,referCode);
    }
  }
}
