import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/date_utils.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/model/create_private_contest_request.dart';

class PrivateContest extends StatefulWidget {
  GeneralModel model;
  Function onTeamCreated;
   PrivateContest(this.model,this.onTeamCreated);
  @override
  _PrivateContestState createState() => new _PrivateContestState();
}

class _PrivateContestState extends State<PrivateContest>{

  TextEditingController nameController = TextEditingController();
  TextEditingController amountController = TextEditingController();
  TextEditingController sizeController = TextEditingController();
  bool _enable=false;
  String entryFee='0.0';
  String userId='0';

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.light) /* set Status bar icon color in iOS. */
    );
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
      })
    });
    amountController.addListener(() {
      setState(() {
        if (amountController.text.isNotEmpty && sizeController.text.isNotEmpty) {
          double total = (double.parse(amountController.text.toString()) * AppConstants.DEFAULT_PERCENT) / double.parse(sizeController.text.toString());
          entryFee = NumberFormat("#.#").format(total);
        } else {
          entryFee = '0.00';
        }
      });
    });
    sizeController.addListener(() {
      setState(() {
        if (amountController.text.isNotEmpty && sizeController.text.isNotEmpty) {
          double total = (double.parse(amountController.text.toString()) * AppConstants.DEFAULT_PERCENT) / double.parse(sizeController.text.toString());
          entryFee = NumberFormat("#.#").format(total);
        } else {
          entryFee = '0.0';
        }
      });
    });
  }


  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.white,
      /*  appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            padding: EdgeInsets.only(top: 28),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.black,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Create Contest',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),



              ],
            ),
          ),
        ),*/

        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: Colors.black,
            // padding: EdgeInsets.only(top: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new Row(
                  children: [
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: ()=>{
                        Navigator.pop(context)
                      },
                      child: new Container(
                        padding: EdgeInsets.fromLTRB(15,15,25,15),
                        alignment: Alignment.centerLeft,
                        child: new Container(
                          width: 16,
                          height: 16,
                          child: Image(
                              image: AssetImage(AppImages.backImageURL),
                              fit: BoxFit.fill),
                        ),
                      ),
                    ),
                    new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          width: 150,
                          child: new Text( widget.model.teamVs!,
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16)),
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 5),
                          child: CustomTimer(
                            from: Duration(milliseconds: SuperDateFormat.getFormattedDateObj(widget.model.headerText!)!.millisecondsSinceEpoch-MethodUtils.getDateTime().millisecondsSinceEpoch),
                            to: Duration(milliseconds: 0),
                            onBuildAction: CustomTimerAction.auto_start,
                            builder: (CustomTimerRemainingTime remaining) {

                              if(remaining.hours=='00'&&remaining.minutes=='00'&& remaining.seconds=='00') {
                                navigateToHomePage(context);
                              }

                              return new GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                child: Text(
                                  int.parse(remaining.days)>=1?int.parse(remaining.days)>1?"${remaining.days} Days Left" :"${remaining.days} Day Left" :int.parse(remaining.hours)>=1?" ${remaining.hours}Hrs : ${remaining.minutes}Min Left":"${remaining.minutes}Min :${remaining.seconds}Sec Left" ,
                                  style: TextStyle(
                                      fontFamily: 'noway',
                                      color: Colors.white,
                                      decoration: TextDecoration.none,
                                      fontWeight:
                                      FontWeight.w500,
                                      fontSize: 12),
                                ),
                                onTap: (){
                                },
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                new Container(
                  width: 150,
                  alignment: Alignment.center,
                  child: new Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    // mainAxisSize: MainAxisSize.max,
                    children: [
                      // new GestureDetector(
                      //   behavior: HitTestBehavior.translucent,
                      //   child: new Container(
                      //     height: 20,
                      //     width: 20,
                      //     child: Image(
                      //       image: AssetImage(
                      //           AppImages.createContestIcon),
                      //     ),
                      //   ),
                      //   onTap: ()=>{
                      //     _modalBottomSheetMenu()
                      //   },
                      // ),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          child: new Row(
                            children: [
                              //   new Text('₹'+balance,
                              //       style: TextStyle(
                              //           fontFamily: AppConstants.textBold,
                              //           color: Colors.white,
                              //           fontWeight: FontWeight.normal,
                              //           fontSize: 15)),
                              new Container(
                                margin: EdgeInsets.only(right: 10),
                                height: 20,
                                width: 20,
                                child: Image(
                                    image: AssetImage(
                                        AppImages.walletImageURL)),
                              )
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToWallet(context)
                        },
                      ),

                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        body: new SingleChildScrollView(
          child: new Container(
            color: Colors.white,
            child: new Column(
              children: [
               // new MatchHeader(widget.model.teamVs!,widget.model.headerText!,widget.model.isFromLive??false,widget.model.isFromLiveFinish??false),
                new Container(
                  color: Colors.white,
                  child: new Column(
                    children: [
                      new Container(
                        padding: EdgeInsets.all(10),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Container(
                              alignment: Alignment.center,
                              child: new Text('Entry Per Team',
                                style: TextStyle(
                                    // fontFamily: AppConstants.textSemiBold,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 14,decoration: TextDecoration.none),),
                            ),
                            new Container(
                              alignment: Alignment.center,
                              child: new Text('₹ '+entryFee,
                                style: TextStyle(
                                    fontFamily: AppConstants.textSemiBold,
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18,decoration: TextDecoration.none),),
                            )
                          ],
                        ),
                      ),
                      // new Divider(height: 2,),
                      new Container(
                        decoration: BoxDecoration(color:TextFieldColor,border: Border(top: BorderSide(width: 1,color: TextFieldBorderColor),bottom: BorderSide(width: 1,color: TextFieldBorderColor))),

                        padding: EdgeInsets.all(10),
                        alignment: Alignment.centerLeft,
                        child: new Text('Entry is calculated based on total prize amount & contest size',
                          style: TextStyle(
                              color: darkgray,
                              fontWeight: FontWeight.w500,
                              fontSize: 12,decoration: TextDecoration.none),),
                      ),
                    ],
                  ),
                ),
               Container(
                 color: Colors.white,
                 child: Center(
                   child: Padding(
                     padding: const EdgeInsets.all(8.0),
                     child: Column(
                       crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         new Container(
                           padding: EdgeInsets.only(left: 10,top: 20),
                           alignment: Alignment.centerLeft,
                           child: Text(
                             'Give you contest a name',
                             textAlign: TextAlign.start,
                             style: TextStyle(
                               fontSize: 14,
                               color: darkgray,
                               fontFamily: AppConstants.textRegular,
                               fontStyle: FontStyle.normal,
                               fontWeight: FontWeight.w400,
                             ),
                           ),
                         ),
                         new Container(
                           margin: EdgeInsets.all(10),
                           height: 45,
                           child: TextField(
                             controller: nameController,
                             decoration: InputDecoration(
                               counterText: "",
                               fillColor: Colors.white,
                               filled: true,
                               hintStyle: TextStyle(
                                 fontSize: 12,
                                 color: Colors.grey,
                                 fontStyle: FontStyle.normal,
                                 fontWeight: FontWeight.bold,
                               ),
                               hintText: '',
                               enabledBorder: const OutlineInputBorder(
                                 // width: 0.0 produces a thin "hairline" border
                                 borderSide:
                                 const BorderSide(color: Colors.grey, width: 0.0),
                               ),
                               focusedBorder: OutlineInputBorder(
                                 borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                               ),
                             ),
                           ),
                         ),
                         new Container(
                           padding: EdgeInsets.only(left: 10,top: 10,right: 10),
                           child: new Row(
                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                             children: [
                               new Container(
                                 alignment: Alignment.centerLeft,
                                 child: Text(
                                   'Total winning amount (Rs.)',
                                   textAlign: TextAlign.start,
                                   style: TextStyle(
                                     fontSize: 14,
                                     color: Colors.grey,
                                     fontFamily: AppConstants.textRegular,
                                     fontStyle: FontStyle.normal,
                                     fontWeight: FontWeight.w400,
                                   ),
                                 ),
                               ),

                             ],
                           ),
                         ),
                         new Container(
                           margin: EdgeInsets.only(left: 10,right: 10,top: 10),
                           height: 45,
                           child: TextField(
                             maxLength: 5,
                             controller: amountController,
                             keyboardType: TextInputType.number,
                             decoration: InputDecoration(
                               counterText: "",
                               fillColor: TextFieldColor,
                               filled: true,
                               hintStyle: TextStyle(
                                 fontSize: 12,
                                 color: Colors.grey,
                                 fontStyle: FontStyle.normal,
                                 fontWeight: FontWeight.bold,
                               ),
                               hintText: '',
                               enabledBorder:  OutlineInputBorder(
                                 // width: 0.0 produces a thin "hairline" border
                                 borderSide:
                                  BorderSide(color: TextFieldColor, width: 0.0),
                               ),
                               focusedBorder: OutlineInputBorder(
                                 borderSide:  BorderSide(color: TextFieldColor, width: 0.0),
                               ),
                             ),
                           ),
                         ),
                         Container(
                           margin: EdgeInsets.only(left: 10,right: 10,top: 3),
                           child: Row(
                             children: [
                               Image.asset('assets/images/notice.png',height: 20,width: 20,),
                               SizedBox(width: 10,),
                               Text(
                                 'Minimum 20 Rs. and Maximum 25,000 Rs.',
                                 textAlign: TextAlign.start,
                                 style: TextStyle(
                                   fontSize: 10,
                                   color: Themecolor,
                                   fontFamily: AppConstants.textRegular,
                                   fontStyle: FontStyle.normal,
                                   fontWeight: FontWeight.w400,
                                 ),
                               ),
                             ],
                           ),
                         ),
                         new Container(
                           padding: EdgeInsets.only(left: 10,top: 10,right: 10),
                           child: new Row(
                             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                             children: [
                               new Container(
                                 alignment: Alignment.centerLeft,
                                 child: Text(
                                   'Contest size',
                                   textAlign: TextAlign.start,
                                   style: TextStyle(
                                     fontSize: 14,
                                     color: Colors.grey,
                                     fontFamily: AppConstants.textRegular,
                                     fontStyle: FontStyle.normal,
                                     fontWeight: FontWeight.w400,
                                   ),
                                 ),
                               ),
                             ],
                           ),
                         ),

                         new Container(
                           margin: EdgeInsets.only(left: 10,right: 10,top: 10),
                           height: 45,
                           child: TextField(
                             controller: sizeController,
                             keyboardType: TextInputType.number,
                             decoration: InputDecoration(
                               counterText: "",
                               fillColor: TextFieldColor,
                               filled: true,
                               hintStyle: TextStyle(
                                 fontSize: 12,
                                 color: Colors.grey,
                                 fontStyle: FontStyle.normal,
                                 fontWeight: FontWeight.bold,
                               ),
                               hintText: '',
                               enabledBorder:  OutlineInputBorder(
                                 // width: 0.0 produces a thin "hairline" border
                                 borderSide:
                                  BorderSide(color: TextFieldColor, width: 0.0),
                               ),
                               focusedBorder: OutlineInputBorder(
                                 borderSide:  BorderSide(color: TextFieldColor, width: 0.0),
                               ),
                             ),
                           ),
                         ),
                         Container(
                           margin: EdgeInsets.only(left: 10,right: 10,top: 3),
                           child: Row(
                             children: [
                               Image.asset('assets/images/notice.png',height: 20,width: 20,),
                               SizedBox(width: 10,),
                               Text(
                                 'Minimum 2 and Maximum 50.',
                                 textAlign: TextAlign.start,
                                 style: TextStyle(
                                   fontSize: 10,
                                   color: Themecolor,
                                   fontFamily: AppConstants.textRegular,
                                   fontStyle: FontStyle.normal,
                                   fontWeight: FontWeight.w400,
                                 ),
                               ),
                             ],
                           ),
                         ),
                         SizedBox(height: 20,),
                         new Container(
                           child: new Row(
                             children: [
                               Switch(
                                 value: _enable,
                                 activeColor: yellowdark,
                                 onChanged: (bool val){
                                   setState(() {
                                     _enable = val;
                                   });
                                 },
                               ),
                               new Container(
                                 alignment: Alignment.centerLeft,
                                 child: Text(
                                   'Allow friends to join with multiple league.',
                                   textAlign: TextAlign.start,
                                   style: TextStyle(
                                     fontSize: 14,
                                     color: Title_Color_1,
                                     fontFamily: AppConstants.textRegular,
                                     fontStyle: FontStyle.normal,
                                     fontWeight: FontWeight.w400,
                                   ),
                                 ),
                               ),
                             ],
                           ),
                         ),
                         new Container(
                             width:
                             MediaQuery.of(context).size.width,
                             height: 45,
                             margin: EdgeInsets.only(right: 30,left: 30,top: 10,bottom: 24),
                             child: ElevatedButton(
                               style: ElevatedButton.styleFrom(
                                 primary: primaryColor,
                                 elevation: 0.5,
                                 shape: RoundedRectangleBorder(
                                   borderRadius: BorderRadius.circular(40),
                                 ),
                               ),
                               onPressed: () {
                                 if (nameController.text.isEmpty) {
                                   MethodUtils.showError(context, "Please Enter Challenge Name");
                                   return;
                                 } else if (amountController.text.isEmpty) {
                                   MethodUtils.showError(context, "Please Enter Winning Amount");
                                   return;
                                 } else if (int.parse(amountController.text.toString()) > 10000) {
                                   MethodUtils.showError(context, "Please enter a valid amount");
                                   return;
                                 } else if (sizeController.text.isEmpty) {
                                   MethodUtils.showError(context, "Please Enter Challenge Size");
                                   return;
                                 } else if (int.parse(sizeController.text.toString()) < 2) {
                                   MethodUtils.showError(context, "You need to choose minimum 2 Challenge sizes");
                                   return;
                                 } else if (int.parse(sizeController.text.toString()) > 100) {
                                   MethodUtils.showError(context, "You can choose up to 100 Challenge sizes");
                                   return;
                                 }
                                 navigateToWinningBreakupManager(
                                   context,
                                   widget.model,
                                   CreatePrivateContestRequest(
                                     name: nameController.text.toString(),
                                     user_id: userId,
                                     matchkey: widget.model.matchKey,
                                     win_amount: amountController.text.toString(),
                                     maximum_user: sizeController.text.toString(),
                                     entryfee: entryFee,
                                     multi_entry: _enable ? '1' : '0',
                                     sport_key: widget.model.sportKey,
                                     is_public: '0',
                                     fantasy_type: widget.model.fantasyType.toString(),
                                     slotes_id: widget.model.slotId.toString(),
                                   ),
                                   widget.onTeamCreated,
                                 );
                               },
                               child: Padding(
                                 padding: EdgeInsets.symmetric(vertical: 10),
                                 child: Text(
                                   'Choose Winning Breakup',
                                   style: TextStyle(fontSize: 14, color: Colors.white),
                                 ),
                               ),
                             )
                         )
                       ],
                     ),
                   ),
                 ),
               )
              ],
            ),
          ),
        ),
      ),
    );
  }
}