import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/offer_list_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

import '../dataModels/Location.dart';
import '../repository/model/verify_response.dart';

class AddBalance extends StatefulWidget {
  String promo_code;
  AddBalance(this.promo_code);
  @override
  _AddBalanceState createState() => new _AddBalanceState();
}

class _AddBalanceState extends State<AddBalance> {
  List<OfferListDataModel> offersList = [];
  TextEditingController amountController = TextEditingController();
  TextEditingController codeController = TextEditingController();
  String balance = '0';
  String userId = '0';
  String winningAmount = '0';
  String bonusAmount = '0';
  String promoAmount = '0';
  int promoId = 0;
  double gstAmount = 0.0;
  double discount = 0.0;
  // double totalPay = 0.0;
  String totalPay = "0.0";
  String addCash = "0.0";
  List<String>? stateList = [
    "assam",
    "odisha",
    "andhra pradesh",
    "sikkim",
    "telangana",
    "nagaland",
  ];

  // void calculateGST() {
  //   double amount = double.tryParse(amountController.text) ?? 0.0;
  //   double gstPercentage = 0.28; // 28% GST
  //   double calculatedGST = amount * gstPercentage;
  //
  //   setState(() {
  //     gstAmount = calculatedGST;
  //   });
  // }

  void calculateGST() {
    double amount = double.tryParse(amountController.text) ?? 0.0;
    double gstPercentage = 28/128; // 28% GST
    double calculatedGST = amount * gstPercentage;
    setState(() {
      gstAmount = double.parse(calculatedGST.toStringAsFixed(2));
    });
  }

  void calculateDiscount() {
    double amount = double.tryParse(gstAmount.toString()) ?? 0.0;
    double gstPercentage = 1; // 28% GST
    double calculatedDiscount = amount * gstPercentage;
    setState(() {
      discount = calculatedDiscount;
    });
  }



  void totalAddCash() {
    double amount = double.tryParse(amountController.text.toString()) ?? 0.0;

      double calculatedTotalPay = (amount - gstAmount) ;
      setState(() {
        addCash = calculatedTotalPay.toStringAsFixed(2);
      });
  }

  TextEditingController stateOrCountryController = TextEditingController();
  var state = '';
  var country = '';

  String? latitude;
  String? longitude;

  AllVerifyItem? allVerifyItem;
  List<LocationCustom> locations = [];

  @override
  void initState() {
    super.initState();
    getCurrentLocation();
    amountController.addListener(() {
      calculateGST();
      calculateDiscount();
      // totalPayCash();
      totalAddCash();
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) => {
              setState(() {
                userId = value;
                allVerify();
              })
            });
  }

  void initialCall() {
    AppPrefrence.getString(AppConstants.KEY_USER_BALANCE).then((value) => {
          setState(() {
            balance = value;
          })
        });
    AppPrefrence.getString(AppConstants.KEY_USER_WINING_AMOUNT)
        .then((value) => {
              setState(() {
                winningAmount = value;
              })
            });
    AppPrefrence.getString(AppConstants.KEY_USER_BONUS_BALANCE)
        .then((value) => {
              setState(() {
                bonusAmount = value;
              })
            });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) => {
              setState(() {
                userId = value;
                addCaseOfferList();
              })
            });
    if (widget.promo_code.isNotEmpty) {
      setState(() {
        codeController.text = widget.promo_code;
      });
      applyPromoCode();
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: primaryColor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.light) /* set Status bar icon color in iOS. */
        );

    return SafeArea(
      child: new Scaffold(
        backgroundColor: whiteColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: Colors.black,
            // padding: EdgeInsets.only(top: 40),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,
                        color: whiteColor,
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Add Cash',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: whiteColor,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Stack(
          alignment: Alignment.bottomCenter,
          children: [
            new Container(
              height: MediaQuery.of(context).size.height,
              child: new SingleChildScrollView(
                padding: EdgeInsets.only(bottom: 60),
                child: new Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    new Container(
                      padding: EdgeInsets.all(20),
                      decoration: BoxDecoration(
                        color: Themecolor,
                      ),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          new Column(
                            children: [
                              new Container(
                                margin: EdgeInsets.only(bottom: 4),
                                alignment: Alignment.center,
                                child: Text(
                                  'Deposit',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: 'roboto',
                                    fontSize: 12,
                                    color: whiteColor,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              new Container(
                                alignment: Alignment.center,
                                child: Text(
                                  '₹' + balance,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: 'roboto',
                                    fontSize: 16,
                                    color: whiteColor,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          new Container(
                            width: 1,
                            height: 50,
                            alignment: Alignment.center,
                            color: whiteColor,
                          ),
                          new Column(
                            children: [
                              new Container(
                                margin: EdgeInsets.only(bottom: 4),
                                alignment: Alignment.center,
                                child: Text(
                                  'Winning',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: 'roboto',
                                    fontSize: 12,
                                    color: whiteColor,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              new Container(
                                alignment: Alignment.center,
                                child: Text(
                                  '₹' + winningAmount,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: 'roboto',
                                    fontSize: 16,
                                    color: whiteColor,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          new Container(
                            width: 1,
                            height: 50,
                            alignment: Alignment.center,
                            color: lightGrayColor3,
                          ),
                          new Column(
                            children: [
                              new Container(
                                margin: EdgeInsets.only(bottom: 4),
                                alignment: Alignment.center,
                                child: Text(
                                  'Fancash',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: 'roboto',
                                    fontSize: 12,
                                    color: whiteColor,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              new Container(
                                alignment: Alignment.center,
                                child: Text(
                                  '₹' + bonusAmount,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontFamily: 'roboto',
                                    fontSize: 16,
                                    color: whiteColor,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                    new Divider(
                      height: 1,
                      thickness: 1,
                    ),
                    new Container(
                      margin: EdgeInsets.all(10),
                      child: new Column(
                        children: [
                          new Container(
                            padding: EdgeInsets.only(bottom: 10, top: 5),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Add Cash To Your Wallet',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontFamily: 'roboto',
                                fontSize: 14,
                                color: Text_Color,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          new Container(
                            child: new Stack(
                              alignment: Alignment.centerRight,
                              children: [
                                new Container(
                                  height: 45,
                                  child: TextField(
                                    controller: amountController,
                                    keyboardType: TextInputType.number,
                                    maxLength: 5,
                                    inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                                    decoration: InputDecoration(
                                      counterText: "",
                                      fillColor: TextFieldColor,
                                      filled: true,
                                      hintStyle: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                      ),
                                      hintText: 'Enter Amount',
                                      enabledBorder: OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderSide: BorderSide(
                                            color: TextFieldColor, width: 0.0),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: TextFieldColor, width: 0.0),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.only(top: 20, bottom: 10),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Choose your amount',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontFamily: 'roboto',
                                fontSize: 14,
                                color: Text_Color,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          new Container(
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new Container(
                                    padding:
                                        EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: TextFieldBorderColor,
                                            width: 1),
                                        borderRadius: BorderRadius.circular(5),
                                        color: TextFieldColor),
                                    child: new Text('+ ₹100',
                                        style: TextStyle(
                                            fontFamily: 'roboto',
                                            color: TextFieldTextColor,
                                            fontWeight: FontWeight.w800,
                                            fontSize: 14)),
                                  ),
                                  onTap: () {

                                    if (amountController.text.isEmpty) {
                                      amountController.text = '100';
                                    } else {
                                      amountController.text = (int.parse(
                                                  amountController.text
                                                      .toString()) +
                                              100)
                                          .toString();
                                    }

                                  },
                                ),
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new Container(
                                    padding:
                                        EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: TextFieldBorderColor,
                                            width: 1),
                                        borderRadius: BorderRadius.circular(5),
                                        color: TextFieldColor),
                                    child: new Text('+ ₹300',
                                        style: TextStyle(
                                            fontFamily: 'roboto',
                                            color: TextFieldTextColor,
                                            fontWeight: FontWeight.w800,
                                            fontSize: 14)),
                                  ),
                                  onTap: () {
                                    if (amountController.text.isEmpty) {
                                      amountController.text = '300';
                                    } else {
                                      amountController.text = (int.parse(
                                                  amountController.text
                                                      .toString()) +
                                              300)
                                          .toString();
                                    }
                                  },
                                ),
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new Container(
                                    padding:
                                        EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        border: Border.all(
                                            color: TextFieldBorderColor,
                                            width: 1),
                                        borderRadius: BorderRadius.circular(5),
                                        color: TextFieldColor),
                                    child: new Text('+ ₹500',
                                        style: TextStyle(
                                            fontFamily: 'roboto',
                                            color: TextFieldTextColor,
                                            fontWeight: FontWeight.w800,
                                            fontSize: 16)),
                                  ),
                                  onTap: () {
                                    if (amountController.text.isEmpty) {
                                      amountController.text = '500';
                                    } else {
                                      amountController.text = (int.parse(
                                                  amountController.text
                                                      .toString()) +
                                              500)
                                          .toString();
                                    }
                                  },
                                ),
                              ],
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 15),

                            decoration: BoxDecoration(
                                border:
                                    Border.all(color: bordercolor, width: 0.5),
                                borderRadius: BorderRadius.circular(5)),
                            child: Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.fromLTRB(20, 15, 20, 5),
                                  child: Column(
                                    children: [
                                      Row(
                                        children: [
                                          Text("Add Cash :",
                                              style: TextStyle(
                                                  fontFamily: 'roboto',
                                                  color: Color(0xff283F4C),
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12)),
                                          Spacer(),
                                          Text(
                                            "₹ ${addCash.isEmpty ? '0.00' : addCash}",
                                            style: TextStyle(
                                              fontFamily: 'roboto',
                                              color: textGrey,
                                              fontWeight: FontWeight.w800,
                                              fontSize: 12,
                                            ),
                                          )
                                        ],
                                      ),
                                      SizedBox(height: 10,),
                                      Row(
                                        children: [
                                          Text("GST (28%) :",
                                              style: TextStyle(
                                                  fontFamily: 'roboto',
                                                  color: Color(0xff283F4C),
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12)),
                                          Spacer(),
                                          Text("₹ ${gstAmount.toStringAsFixed(2)}",
                                              style: TextStyle(
                                                  fontFamily: 'roboto',
                                                  color: textGrey,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 12))
                                        ],
                                      ),
                                      SizedBox(height: 10,),
                                      Row(
                                        children: [
                                          Text("To Pay :",
                                              style: TextStyle(
                                                  fontFamily: 'roboto',
                                                  color: Color(0xff283F4C),
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12)),
                                          Spacer(),
                                          // Text("₹ ${totalPay.toStringAsFixed(2)}",
                                          Text("₹ ${amountController.text.toString().isEmpty ? '0.00' : amountController.text.toString()}",
                                              style: TextStyle(
                                                  fontFamily: 'roboto',
                                                  color: textGrey,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 12))
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Divider(thickness: 1,color: TextFieldBorderColor,),
                                Container(
                                  padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                                  child: Row(
                                    children: [
                                      Text("Discount(50%) :",
                                          style: TextStyle(
                                              fontFamily: 'roboto',
                                              color: Color(0xff283F4C),
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14)),
                                      Spacer(),
                                      Text("₹ ${discount.toStringAsFixed(2)}",
                                          style: TextStyle(
                                              fontFamily: 'roboto',
                                              color: textGrey,
                                              fontWeight: FontWeight.w800,
                                              fontSize: 14))
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.only(top: 10, bottom: 10),
                            alignment: Alignment.centerLeft,
                            child: Text(
                              'Do you have a promotion code ?',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontFamily: 'roboto',
                                fontSize: 14,
                                color: Text_Color,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                          ),
                          new Container(
                            child: new Row(
                              textDirection: TextDirection.rtl,
                              children: [
                                Container(
                                    height: 45,
                                    margin: EdgeInsets.only(left: 10),
                                    child: ElevatedButton(
                                      style: ButtonStyle(
                                          backgroundColor:
                                              MaterialStateProperty.all(
                                                  yellowColor),
                                          elevation:
                                              MaterialStateProperty.all(.5),
                                          shape: MaterialStateProperty.all(
                                              RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)))),
                                      child: Text(
                                        'Apply',
                                        style: TextStyle(
                                            fontSize: 14, color: Colors.white),
                                      ),
                                      onPressed: () {
                                        if (codeController.text.isEmpty) {
                                          MethodUtils.showError(context,
                                              'Please enter promo code.');
                                        } else {
                                          applyPromoCode();
                                        }
                                      },
                                    )),
                                Expanded(
                                    child: new Container(
                                  height: 45,
                                  child: TextField(
                                    controller: codeController,
                                    decoration: InputDecoration(
                                      counterText: "",
                                      fillColor: Colors.white,
                                      filled: true,
                                      hintStyle: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                      ),
                                      hintText: 'Enter Promo Code',
                                      enabledBorder: OutlineInputBorder(
                                        // width: 0.0 produces a thin "hairline" border
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(
                                            color: bordercolor, width: 0.0),
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(10),
                                        borderSide: BorderSide(
                                            color: bordercolor, width: 0.0),
                                      ),
                                    ),
                                  ),
                                )),
                              ],
                            ),
                          ),
                          promoId > 0
                              ? new Container(
                                  padding: EdgeInsets.only(top: 5, bottom: 5),
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    'Promo Code Applied.',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontFamily: 'roboto',
                                      fontSize: 14,
                                      color: greenColor,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                )
                              : new Container(),
                        ],
                      ),
                    ),
                    // new Divider(height: 1,thickness: 1,),
                    // new Container(
                    //   height: 30,
                    //   color: lightGrayColor4,
                    //   child: new Row(
                    //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       new Container(
                    //         margin: EdgeInsets.only(left: 10),
                    //         child: Text(
                    //           'Available Offers',
                    //           style: TextStyle(
                    //               color: Colors.black,
                    //               fontWeight: FontWeight.w400,
                    //               fontSize: 14),
                    //         ),
                    //       ),
                    //     ],
                    //   ),
                    // ),
                    // new Divider(height: 1,thickness: 1,),
                    // new ListView.builder(
                    //     physics: NeverScrollableScrollPhysics(),
                    //     shrinkWrap: true,
                    //     scrollDirection: Axis.vertical,
                    //     itemCount: offersList.length,
                    //     itemBuilder: (BuildContext context, int index) {
                    //       return new Card(
                    //         margin: EdgeInsets.only(bottom: 5),
                    //         elevation: 0,
                    //         child: new Container(
                    //           padding: EdgeInsets.all(10),
                    //           child: new Column(
                    //             crossAxisAlignment: CrossAxisAlignment.start,
                    //             children: [
                    //               new Container(
                    //                 child:  new Text(
                    //                   offersList[index].title!,
                    //                   style: TextStyle(
                    //                       fontFamily: 'roboto',
                    //                       color: Colors.black,
                    //                       fontSize: 14,
                    //                       fontWeight: FontWeight.w500),
                    //                 ),
                    //               ),
                    //               new Container(
                    //                 margin: EdgeInsets.only(top: 5,bottom: 10),
                    //                 child:  new Text(
                    //                   offersList[index].description!,
                    //                   style: TextStyle(
                    //                       fontFamily: 'roboto',
                    //                       color: Colors.black,
                    //                       fontSize: 12,
                    //                       fontWeight: FontWeight.w400),
                    //                 ),
                    //               ),
                    //               new Divider(height: 1,thickness: 1,),
                    //               new Row(
                    //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //                 crossAxisAlignment: CrossAxisAlignment.center,
                    //                 children: [
                    //                   DottedBorder(
                    //                     dashPattern: [6, 3],
                    //                     strokeWidth: 2,
                    //                     padding: EdgeInsets.all(0),
                    //                     color: lightGreenColor2,
                    //                     child: Container(
                    //                       height: 35,
                    //                       color: lightGreenColor,
                    //                       child:new Container(
                    //                         padding: EdgeInsets.all(5),
                    //                         alignment: Alignment.center,
                    //                         child: new Text(
                    //                           offersList[index].code!,
                    //                           style: TextStyle(
                    //                             fontFamily: 'roboto',
                    //                             color: Colors.black,
                    //                             fontWeight: FontWeight.w500,
                    //                             fontSize: 14,
                    //                           ),
                    //                           textAlign: TextAlign.center,
                    //                         ),
                    //                       ),
                    //                     ),
                    //                   ),
                    //                   new GestureDetector(
                    //                     behavior: HitTestBehavior.translucent,
                    //                     child: new Container(
                    //                       height: 50,
                    //                       width: 70,
                    //                       alignment: Alignment.center,
                    //                       child:  new Text(
                    //                         'Apply',
                    //                         style: TextStyle(
                    //                             fontFamily: 'roboto',
                    //                             color: greenColor,
                    //                             fontSize: 14,
                    //                             fontWeight: FontWeight.w500),
                    //                       ),
                    //                     ),
                    //                     onTap: (){
                    //                       codeController.text=offersList[index].code!;
                    //                       applyPromoCode();
                    //                     },
                    //                   ),
                    //                 ],
                    //               )
                    //             ],
                    //           ),
                    //         ),
                    //       );
                    //     }
                    // ),
                  ],
                ),
              ),
            ),
            new Container(
                width: 200,
                height: 45,
                margin: EdgeInsets.all(10),
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Themecolor),
                      elevation: MaterialStateProperty.all(.5),
                      shape: MaterialStateProperty.all(RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40)))),
                  child: Text(
                    'Add Cash',
                    style: TextStyle(fontSize: 14, color: Colors.white),
                  ),
                  onPressed: () {
                    if (amountController.text.isEmpty) {
                      MethodUtils.showError(context, 'Please enter amount');
                    } else {
                      FocusScope.of(context).unfocus();
                      // navigateToPaymentOptions(
                      //     context, totalPay.toStringAsFixed(2), promoId);
                      navigateToPaymentOptions(
                          context, amountController.text.toString(), promoId);
                    }
                  },
                ))
          ],
        ),
      ),
    );
  }

  void addCaseOfferList() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    OfferListResponse transactionsResponse =
        await client.addCaseOfferList(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (transactionsResponse.status == 1) {
      offersList = transactionsResponse.result!.data!;
    } else {
      Fluttertoast.showToast(
          msg: transactionsResponse.message!,
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
    }
    setState(() {});
  }

  void applyPromoCode() async {
    FocusScope.of(context).unfocus();
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(
        user_id: userId, promo: codeController.text.toString());
    final client = ApiClient(AppRepository.dio);
    GeneralResponse transactionsResponse =
        await client.applyPromoCode(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (transactionsResponse.status == 1) {
      promoAmount = (transactionsResponse.amount ?? '').toString();
      promoId = transactionsResponse.promo_id!;
      MethodUtils.showSuccess(context, 'Promo Code Applied.');
    } else {
      promoId = 0;
      promoAmount = '0';
      Fluttertoast.showToast(
          msg: transactionsResponse.message!,
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
    }
    setState(() {});
  }

  void getCurrentLocation() async {
    PermissionStatus status = await Permission.location.request();

    if (status == PermissionStatus.granted) {
      AppLoaderProgress.showLoader(context);
      Position position = await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high);
      var lat = position.latitude;
      var long = position.longitude;

      latitude = "$lat";
      longitude = "$long";

      GetAddressFromLatLong(position);

      /* else{
        AppLoaderProgress.hideLoader(context);
        notSatisfyingConditionDialog();
      }*/

      /*setState(() {
        print("AASASASAS" + lat.toString() + long.toString());
      });*/

      return AppLoaderProgress.hideLoader(context);
    } else if (status == PermissionStatus.denied) {
      Navigator.of(context).pop(true);
      /*Map<Permission, PermissionStatus> map =
          await [Permission.location].request();
      if (await Permission.location.serviceStatus.isEnabled) {
        print("ASDASDASD" + " ENABLED_LOCATION");
      } else {

      }*/

      return;
    } else if (status == PermissionStatus.permanentlyDenied) {
      showDialog(
          barrierDismissible: false,
          context: context,
          builder: (context) {
            return WillPopScope(
                child: Dialog(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(7)),
                  elevation: 16,
                  child: Container(
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        SizedBox(height: 30),
                        Center(
                            child: Text(
                          'Permission',
                          style: TextStyle(
                              color: Colors.red,
                              fontSize: 16,
                              fontFamily: 'roboto',
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w800),
                        )),
                        SizedBox(height: 20),
                        Center(
                            child: Padding(
                          padding: EdgeInsets.only(left: 20, right: 20),
                          child: Text(
                              'You have permanently disable location permission, Please allow manually',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 13,
                                  fontFamily: 'roboto',
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w800)),
                        )),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            new Container(
                              margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              child: Container(
                                  width: 110,
                                  height: 35,
                                  margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  child: ElevatedButton(
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                Themecolor),
                                        elevation:
                                            MaterialStateProperty.all(.5),
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)))),
                                    child: Text(
                                      'Allow'.toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    onPressed: () {
                                      openAppSettings();
                                      Navigator.pop(context);
                                      Navigator.of(context).pop();
                                    },
                                  )),

                              // DefaultButton(
                              //   margin: EdgeInsets.zero,
                              //   width: 110,
                              //   height: 35,
                              //   // textColor: Colors.white,
                              //   color: greenColor,
                              //   borderRadius: 5,
                              //   child: Text(
                              //     'Allow'.toUpperCase(),
                              //     style: TextStyle(
                              //         fontSize: 12,
                              //         fontWeight: FontWeight.w500),
                              //   ),
                              //   // shape: RoundedRectangleBorder(
                              //   //     borderRadius:
                              //   //     BorderRadius.circular(5)),
                              //   onPressed: () {
                              //     openAppSettings();
                              //     Navigator.pop(context);
                              //     Navigator.of(context).pop();
                              //   },
                              // )
                            ),
                            new Container(
                              margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              child: Container(
                                  width: 110,
                                  height: 35,
                                  margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                  child: ElevatedButton(
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                Themecolor),
                                        elevation:
                                            MaterialStateProperty.all(.5),
                                        shape: MaterialStateProperty.all(
                                            RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5)))),
                                    child: Text(
                                      'Cancel'.toUpperCase(),
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                      Navigator.of(context).pop();
                                    },
                                  )),

                              // DefaultButton(
                              //   width: 110,
                              //   height: 35,
                              //   margin: EdgeInsets.zero,
                              //   borderRadius: 5,
                              //   // textColor: Colors.white,
                              //   color: primaryColor,
                              //   child: Text(
                              //     'Cancel'.toUpperCase(),
                              //     style: TextStyle(
                              //         fontSize: 12,
                              //         fontWeight: FontWeight.w500),
                              //   ),
                              //   // shape: RoundedRectangleBorder(
                              //   //     borderRadius:
                              //   //     BorderRadius.circular(5)),
                              //   onPressed: () {
                              //     Navigator.pop(context);
                              //     Navigator.of(context).pop();
                              //   },
                              // )
                            ),
                            SizedBox(height: 30)
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                onWillPop: () async => false);
          });
    }
  }

  Future<void> GetAddressFromLatLong(Position position) async {
    List<Placemark> placemarks =
        await placemarkFromCoordinates(position.latitude, position.longitude);
    print(placemarks);
    Placemark place = placemarks[0];
    state = place!.administrativeArea!;
    country = place!.isoCountryCode!;
    String postalCode = place!.postalCode!;

    stateOrCountryController.text =
        ' ${place.locality}, $state, ${place.country}';

    if (country == "") {
      _getLocations(place!.postalCode!);
    } else {
      if (stateList!.contains(state.toLowerCase())) {
        notSatisfyingConditionDialog(
            "You cannot proceed with add cash, Because you are playing from one of our restricted states");
      } else if (country != "IN") {
        notSatisfyingConditionDialog(
            "Sorry, Our app is restricted in your Country");
      } else {
        // AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        //     .then((value) => {
        //           setState(() {
        //             userId = value;
        //             allVerify();
        //           })
        //         });
      }
    }

    //_getLocations(place!.postalCode!);

    //print("COUNTRYNAME_IS NOT IN "+postalCode.substring(0,2));

    // stateOrCountryController.text =
    //     '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode},${state} ${place.country}';
  }

  _getLocations(pincode) {
    final JsonDecoder _decoder = const JsonDecoder();
    http
        .get(Uri.parse("http://www.postalpincode.in/api/pincode/$pincode"))
        .then((http.Response response) {
      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400) {
        //throw Exception("Error while fetching data");
        Navigator.of(context).pop();
      }

      var json = _decoder.convert(res);
      var tmp = json['PostOffice'] as List;
      locations = tmp
          .map<LocationCustom>((json) => LocationCustom.fromJson(json))
          .toList();
      //print("MALINGA_COUNTRY"+locations[0].state+" "+locations[0].region);
      AppLoaderProgress.hideLoader(context);

      if (locations[0].state == "Jammu & Kashmir") {
        AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
            .then((value) => {
                  setState(() {
                    userId = value;
                    allVerify();
                  })
                });
      } else {
        if (stateList!.contains(state.toLowerCase())) {
          notSatisfyingConditionDialog(
              "You cannot proceed with add cash, Because you are playing from one of our restricted states");
        } else if (country != "IN") {
          notSatisfyingConditionDialog(
              "Sorry, Our app is restricted in your Country");
        } else {
          AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
              .then((value) => {
                    setState(() {
                      userId = value;
                      allVerify();
                    })
                  });
        }
      }
    });
  }

  void allVerify() async {
    // isLoading=false;
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    AllVerifyResponse referBonusListResponse =
        await client.allVerify(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (referBonusListResponse.status == 1) {
      allVerifyItem = referBonusListResponse.result!;
      if (isVerified()) {
        setState(() {
          initialCall();
        });
      } else {
        Navigator.of(context).pop();
        navigateToVerifyAccount(context);
      }

      // _tabCount=getCount();
      // isLoading=false;
    }
  }

  bool isVerified() {
    return allVerifyItem!.mobile_verify == 1 &&
        allVerifyItem!.email_verify == 1;
  }

  void notSatisfyingConditionDialog(String message) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          double width = MediaQuery.of(context).size.width;
          double height = MediaQuery.of(context).size.height;

          return WillPopScope(
              child: Dialog(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0)),
                elevation: 0.0,
                child: Container(
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      SizedBox(height: 30),
                      Center(
                          child: Text(
                        'Sorry!',
                        style: TextStyle(
                            color: Colors.red,
                            fontSize: 16,
                            fontFamily: 'roboto',
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w800),
                      )),
                      SizedBox(height: 20),
                      Center(
                          child: Padding(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Text(message,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 13,
                                fontFamily: 'roboto',
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.w800)),
                      )),
                      Center(
                          child: Padding(
                        padding: EdgeInsets.only(left: 20, right: 20, top: 15),
                        child: Center(
                            child: Text(
                                'For more information, You can write us at',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 13,
                                    fontFamily: 'roboto',
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w800))),
                      )),
                      Center(
                          child: Padding(
                        padding: EdgeInsets.only(left: 20, right: 20, top: 15),
                        child: Center(
                            child: Text('support@everplayfantasy.com',
                                style: TextStyle(
                                    color: Colors.red,
                                    fontSize: 15,
                                    fontFamily: 'roboto',
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w900))),
                      )),
                      Container(
                        margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                        child: Container(
                            width: 130,
                            height: 35,
                            margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor:
                                      MaterialStateProperty.all(Themecolor),
                                  elevation: MaterialStateProperty.all(.5),
                                  shape: MaterialStateProperty.all(
                                      RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)))),
                              child: Text(
                                'CONTINUE'.toUpperCase(),
                                style: TextStyle(
                                    fontSize: 12, fontWeight: FontWeight.w500),
                              ),
                              onPressed: () {
                                try {
                                  AppLoaderProgress.hideLoader(context);
                                  Navigator.pop(context);
                                  Navigator.of(context).pop();
                                } on Exception catch (_) {}
                              },
                            )),

                        // Container(
                        //   margin: EdgeInsets.zero,
                        //   width: 130,
                        //   height: 35,
                        //   // textColor: Colors.white,
                        //   color: greenColor,
                        //   borderRadius: 5,
                        //   child: Text(
                        //     'CONTINUE'.toUpperCase(),
                        //     style: TextStyle(
                        //         fontSize: 12, fontWeight: FontWeight.w500),
                        //   ),
                        //   // shape: RoundedRectangleBorder(
                        //   //     borderRadius:
                        //   //     BorderRadius.circular(5)),
                        //   onPressed: () {
                        //     try{
                        //       AppLoaderProgress.hideLoader(context);
                        //       Navigator.pop(context);
                        //       Navigator.of(context).pop();
                        //     }
                        //     on Exception catch (_){
                        //
                        //     }
                        //   },
                        // )
                      )
                    ],
                  ),
                ),
              ),
              onWillPop: () async => false);
        });
  }
}
