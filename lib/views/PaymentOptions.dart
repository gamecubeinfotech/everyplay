import 'dart:convert';

import 'package:built_value/json_object.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:paytm_allinonesdk/paytm_allinonesdk.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/my_balance_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:EverPlay/repository/retrofit/apis.dart';

import '../repository/model/get_phonepe_response.dart';
import 'PaymentView.dart';
import 'UserProfile.dart';

class PaymentOptions extends StatefulWidget {
  String amount;
  int promoId;

  PaymentOptions(this.amount, this.promoId);

  @override
  _PaymentOptionsState createState() => new _PaymentOptionsState();
}

class _PaymentOptionsState extends State<PaymentOptions> {
  TextEditingController codeController = TextEditingController();
  String userId = '0';
  String name = '';
  String email = '';
  String mobile = '';

  static const platform = MethodChannel('com.everplay/sabpaisa');
  @override
  void initState() {

    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) => {
              setState(() {
                userId = value;
              })
            });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_NAME)
        .then((value) => {
              setState(() {
                name = value;
              })
            });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_MOBILE)
        .then((value) => {
              setState(() {
                mobile = value;
              })
            });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_EMAIL)
        .then((value) => {
              setState(() {
                email = value;
              })
            });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override

  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: primaryColor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.light) /* set Status bar icon color in iOS. */
        );



    return SafeArea(
      child: Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: Colors.black,
            // padding: EdgeInsets.only(top: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,
                        color: whiteColor,
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Payment Options',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: whiteColor,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Stack(
          children: [
            new Container(
              color: whiteColor,
              height: MediaQuery.of(context).size.height,
              child: new SingleChildScrollView(
                child: new Container(
                  child: new Column(
                    children: [
                      SizedBox(height: 10,),
                      Container(
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(color: Themecolor),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Image.asset(AppImages.walletSymbol,scale: 4,),
                                SizedBox(width: 10,),
                                new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    new Container(
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        'Amount to be added',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: whiteColor,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.centerLeft,
                                      child: Padding(
                                        padding: const EdgeInsets.only(top:4.0),
                                        child: Text(
                                          '₹' + widget.amount,
                                          textAlign: TextAlign.start,
                                          style: TextStyle(
                                            fontSize: 17,
                                            color: whiteColor,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w700,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            new Container(
                              alignment: Alignment.centerRight,
                              height: 70,
                              width: 90,
                              child: Image(
                                image: AssetImage(AppImages.safeSecure),
                              ),
                            )
                          ],
                        ),
                      ),
                      SizedBox(height: 10,),
                      // new GestureDetector(
                      //   behavior: HitTestBehavior.translucent,
                      //   child: new Container(
                      //     margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      //     decoration: BoxDecoration(color: whiteColor, borderRadius: BorderRadius.circular(10),border: Border.all(color: bordercolor)),
                      //     child: new Container(
                      //       padding: EdgeInsets.only(
                      //           left: 10, right: 10, top: 13, bottom: 13),
                      //       child: new Row(
                      //         children: [
                      //           new Container(
                      //             margin: EdgeInsets.only(right: 33),
                      //             height: 40,
                      //             width: 40,
                      //             child: new Image.asset(AppImages.debitIcon,
                      //                 fit: BoxFit.fill),
                      //           ),
                      //           new Column(
                      //             crossAxisAlignment: CrossAxisAlignment.start,
                      //             children: [
                      //               new Text(
                      //                 'Debit Card',
                      //                 style: TextStyle(
                      //                     color: TextFieldTextColor,
                      //                     fontSize: 14,
                      //                     fontWeight: FontWeight.w500),
                      //               ),
                      //             ],
                      //           )
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      //   onTap: () {
                      //     // getCfToken('dc');
                      //   },
                      // ),
                      // new GestureDetector(
                      //   behavior: HitTestBehavior.translucent,
                      //   child: new Container(
                      //     margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      //     decoration: BoxDecoration(color: TextFieldColor, borderRadius: BorderRadius.circular(10),),
                      //     child: new Container(
                      //       padding: EdgeInsets.only(
                      //           left: 10, right: 10, top: 15, bottom: 15),
                      //       child: new Row(
                      //         children: [
                      //           new Container(
                      //             margin: EdgeInsets.only(right: 20),
                      //             height: 35,
                      //             width: 35,
                      //             child: new Image.asset(AppImages.upiIcon,
                      //                 fit: BoxFit.fill),
                      //           ),
                      //           new Column(
                      //             crossAxisAlignment: CrossAxisAlignment.start,
                      //             children: [
                      //               new Text(
                      //                 'UPI',
                      //                 style: TextStyle(
                      //                     color: TextFieldTextColor,
                      //                     fontSize: 14,
                      //                     fontWeight: FontWeight.w500),
                      //               ),
                      //             ],
                      //           )
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      //   onTap: () {
                      //     // getCfToken("upi");
                      //   },
                      // ),
                      // new GestureDetector(
                      //   behavior: HitTestBehavior.translucent,
                      //   child: new Container(
                      //     margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      //     decoration: BoxDecoration(color: whiteColor, borderRadius: BorderRadius.circular(10),border: Border.all(color: bordercolor)),
                      //     child: new Container(
                      //       padding: EdgeInsets.only(
                      //           left: 10, right: 10, top: 10, bottom: 10),
                      //       child: new Row(
                      //         children: [
                      //           new Container(
                      //             margin: EdgeInsets.only(right: 33),
                      //             height: 40,
                      //             width: 40,
                      //             child: new Image.asset(AppImages.debitIcon,
                      //               fit: BoxFit.fill,),
                      //           ),
                      //           new Column(
                      //             crossAxisAlignment: CrossAxisAlignment.start,
                      //             children: [
                      //               new Text(
                      //                 'Credit Card',
                      //                 style: TextStyle(
                      //                     color: TextFieldTextColor,
                      //                     fontSize: 14,
                      //                     fontWeight: FontWeight.w500),
                      //               ),
                      //             ],
                      //           )
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      //   onTap: () {
                      //     // getCfToken('cc');
                      //   },
                      // ),
                      // new GestureDetector(
                      //   behavior: HitTestBehavior.translucent,
                      //   child: new Container(
                      //     margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      //     decoration: BoxDecoration(color: TextFieldColor, borderRadius: BorderRadius.circular(10),),
                      //     child: new Container(
                      //       padding: EdgeInsets.only(
                      //           left: 10, right: 10, top: 10, bottom: 10),
                      //       child: new Row(
                      //         children: [
                      //           new Container(
                      //             margin: EdgeInsets.only(right: 33),
                      //             height: 40,
                      //             width: 40,
                      //             child: new Image.asset(AppImages.bankIcon,
                      //                 fit: BoxFit.fill),
                      //           ),
                      //           new Column(
                      //             crossAxisAlignment: CrossAxisAlignment.start,
                      //             children: [
                      //               new Text(
                      //                 'Net Banking',
                      //                 style: TextStyle(
                      //                     color: TextFieldTextColor,
                      //                     fontSize: 14,
                      //                     fontWeight: FontWeight.w500),
                      //               ),
                      //             ],
                      //           )
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      //   onTap: () {
                      //     // getCfToken("nb");
                      //   },
                      // ),
                      // new GestureDetector(
                      //   behavior: HitTestBehavior.translucent,
                      //   child: new Container(
                      //     margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                      //     decoration: BoxDecoration(color: whiteColor, borderRadius: BorderRadius.circular(10),border: Border.all(color: bordercolor)),
                      //     child: new Container(
                      //       padding: EdgeInsets.only(
                      //           left: 10, right: 10, top: 10, bottom: 10),
                      //       child: new Row(
                      //         children: [
                      //           new Container(
                      //             margin: EdgeInsets.only(right: 33),
                      //             height: 40,
                      //             width: 40,
                      //             child: new Image.asset(AppImages.walletIcon,
                      //                 fit: BoxFit.fill),
                      //           ),
                      //           new Column(
                      //             crossAxisAlignment: CrossAxisAlignment.start,
                      //             children: [
                      //               new Text(
                      //                 'Wallet',
                      //                 style: TextStyle(
                      //                     color: TextFieldTextColor,
                      //                     fontSize: 14,
                      //                     fontWeight: FontWeight.w500),
                      //               ),
                      //             ],
                      //           )
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      //   onTap: () {
                      //     // getCfToken("wallet");
                      //   },
                      // ),
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child:  Container(
                          margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          decoration: BoxDecoration(color: whiteColor, borderRadius: BorderRadius.circular(10),border: Border.all(color: bordercolor)),
                          child: new Container(
                            padding: EdgeInsets.only(
                                left: 10, right: 10, top: 10, bottom: 10),
                            child: new Row(
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(right: 20),
                                  padding: EdgeInsets.all(5),
                                  height: 40,
                                  width: 40,
                                  child: new Image.asset(AppImages.sabpaisa,
                                      fit: BoxFit.fill),
                                ),
                                new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    new Text(
                                      'SabPaisa',
                                      style: TextStyle(
                                          color: TextFieldTextColor,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () async{
                          name =await AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_NAME);
                          if(name.isNotEmpty && email.isNotEmpty && mobile.isNotEmpty){
                            getSabPaisaChecksum();
                          }else{
                           await Fluttertoast.showToast(msg: 'Please Update Your Profile');
                           navigateToUserProfile(context);
                           // Navigator.push(context, MaterialPageRoute(builder: (context) =>  UserProfile(),));
                          }

                        },
                      ),
                      GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child:  Container(
                          margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                          decoration: BoxDecoration(color: whiteColor, borderRadius: BorderRadius.circular(10),border: Border.all(color: bordercolor)),
                          child: new Container(
                            padding: EdgeInsets.only(
                                left: 10, right: 10, top: 10, bottom: 10),
                            child: new Row(
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(right: 20),
                                  padding: EdgeInsets.all(5),
                                  height: 40,
                                  width: 40,
                                  child: new Image.asset(AppImages.phonePe,
                                      fit: BoxFit.fill),
                                ),
                                new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    new Text(
                                      'PhonePe',
                                      style: TextStyle(
                                          color: TextFieldTextColor,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () async{
                          name =await AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_NAME);
                          if(name.isNotEmpty && email.isNotEmpty && mobile.isNotEmpty){
                            getPhonePePayment();
                          }else{
                            await Fluttertoast.showToast(msg: 'Please Update Your Profile');
                            navigateToUserProfile(context);
                            // Navigator.push(context, MaterialPageRoute(builder: (context) =>  UserProfile(),));
                          }




                        },
                      ),
                      // SizedBox(height: 4,),
                      // Row(
                      //
                      //   children: [
                      //     SizedBox(width: 20,),
                      //     Icon(Icons.info_outlined,color: Colors.black,size: 14,),
                      //     SizedBox(width: 4,),
                      //     Text("Use SabPaisa for Card payment",
                      //       style: TextStyle(
                      //           color: Colors.black,
                      //           fontSize: 12,
                      //           fontWeight: FontWeight.normal),),
                      //     SizedBox(width: 4,)
                      //   ],
                      // ),
                      // SizedBox(height: 14,),

                      /*  new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Card(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 1),
                          color: lightGrayColor2,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0),
                          ),
                          child: new Container(
                            padding: EdgeInsets.only(
                                left: 10, right: 10, top: 15, bottom: 15),
                            child: new Row(
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(right: 20),
                                  padding: EdgeInsets.all(5),
                                  height: 25,
                                  width: 48,
                                  child: new Image.asset(AppImages.paytmIcon,
                                      fit: BoxFit.fill),
                                ),
                                new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    new Text(
                                      'Paytm',
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        onTap: () {
                          getPaytmChecksum();
                        },
                      ),*/
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void getSabPaisaChecksum() async {
    AppLoaderProgress.showLoader(context);
    Map<String, dynamic> inputParams = {
      "user_id": userId,
      "amount": widget.amount,
    };
    Dio dio = new Dio();
    dio.options.headers["Content-Type"] = "application/x-www-form-urlencoded";
    var response = await dio
        .post(
      AppRepository.dio.options.baseUrl + Apis.getSabPaisaChecksum,
      data: inputParams,
    )
        .catchError((e) => debugPrint(e.response.toString()));
    AppLoaderProgress.hideLoader(context);
    var jsonObject = json.decode(response.toString())['result'];

    String lastName = "";
    String firstName= "";
    if(name.split("\\w+").length>1){

      lastName = name.substring(name.lastIndexOf(" ")+1);
      firstName = name.substring(0, name.lastIndexOf(' '));
    }
    else{
      firstName = name;
      lastName = name;
    }


    await platform.invokeMethod('callSabPaisaSdk',[firstName,lastName,email,mobile,widget.amount,jsonObject['transaction_id'],jsonObject['ClientCode'],jsonObject['AesApiIv'],jsonObject['AesApiKey'],jsonObject['TransUserName'],jsonObject['TransUserPassword']]).then((value) {
      if(value[0].toString() == 'SUCCESS'){
        getUserBalance();
      }else{
        Fluttertoast.showToast(
            msg: value[0].toString(),
            toastLength: Toast.LENGTH_SHORT,
            timeInSecForIosWeb: 1);
      }
    });
  }

  void getPhonePePayment() async {
    // isLoading=false;
    AppLoaderProgress.showLoader(context);
    GeneralRequest phonePeRequest = new GeneralRequest(user_id: userId, mobile: mobile,amount: widget.amount);
    final client = ApiClient(AppRepository.dio);
    PhonePeResponse phonePePaymentResponse = await client.getPhonePePayment(phonePeRequest);
    AppLoaderProgress.hideLoader(context);
    if (phonePePaymentResponse.status == 1) {
     var phonePeResponse = phonePePaymentResponse.url!;
     var status = await navigateToPhonePe(context,"PhonePe", phonePeResponse);
      if(status!=null){
        if(status=='SUCCESS'){
          getUserBalance();
        }
      }
    }else{
      Fluttertoast.showToast(msg: phonePePaymentResponse.message!);
    }
  }


  void getPaytmChecksum() async {
    String orderId =
        userId + 'pic6' + DateTime.now().millisecondsSinceEpoch.toString();
    AppLoaderProgress.showLoader(context);
    Map<String, dynamic> inputParams = {
      "MID": AppConstants.paytm_mid,
      "ORDER_ID": orderId,
      "CUST_ID": 'pic6' + userId,
      "INDUSTRY_TYPE_ID": 'Retail105',
      "CHANNEL_ID": 'WAP',
      "TXN_AMOUNT": widget.amount,
      "WEBSITE": 'DEFAULT',
      "EMAIL": email,
      "MOBILE_NO": mobile,
      "CALLBACK_URL":
      'https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=' + orderId,
    };
    Dio dio = new Dio();
    dio.options.headers["Content-Type"] = "application/x-www-form-urlencoded";
    var response = await dio
        .post(
      AppRepository.dio.options.baseUrl + Apis.getPaytmChecksum,
      data: inputParams,
    )
        .catchError((e) => debugPrint(e.response.toString()));
    AppLoaderProgress.hideLoader(context);
    var jsonObject = json.decode(response.toString());
    doPaytmPayment(inputParams, jsonObject['token']);
  }

  void doPaytmPayment(Map<String, dynamic> inputParams, String txnToken) {
    var response = AllInOneSdk.startTransaction(
        inputParams['MID'],
        inputParams['ORDER_ID'],
        inputParams['ORDER_ID'],
        txnToken,
        inputParams['CALLBACK_URL'],
        false,
        true);
    response.then((value) {
      print(value);
      setState(() {
        getUserBalance();
      });
    }).catchError((onError) {
      if (onError is PlatformException) {
        setState(() {
          Fluttertoast.showToast(
              msg: onError.message! + " \n  " + onError.details.toString(),
              toastLength: Toast.LENGTH_SHORT,
              timeInSecForIosWeb: 1);
        });
      } else {
        setState(() {
          Fluttertoast.showToast(
              msg: onError.toString(),
              toastLength: Toast.LENGTH_SHORT,
              timeInSecForIosWeb: 1);
        });
      }
    });
  }

  void getCfToken(String mode) async {
    AppLoaderProgress.showLoader(context);
    Map<String, dynamic> inputParams = {
      "orderId": userId + DateTime.now().millisecondsSinceEpoch.toString(),
      "orderAmount": widget.amount,
      "customerName": name,
      "orderNote": '',
      "orderCurrency": 'INR',
      "appId": AppConstants.cash_free_app_id,
      "customerPhone": mobile,
      "customerEmail": email,
      "stage": 'PROD',
      "notifyUrl": AppConstants.cashfree_notify_url
    };
    if (mode != 'upi') {
      inputParams.putIfAbsent('paymentModes', () => mode);
    }
    debugPrint(inputParams.toString());
    Dio dio = new Dio();
    dio.options.headers["Content-Type"] = "application/x-www-form-urlencoded";
    var response = await dio
        .post(
      AppRepository.dio.options.baseUrl + Apis.getCfToken,
      data: inputParams,
    )
        .catchError((e) => debugPrint(e.response.toString()));
    AppLoaderProgress.hideLoader(context);
    var jsonObject = json.decode(response.toString());
    if (jsonObject['status'] == 'OK') {
      doCashfreePayment(inputParams, jsonObject['token'], mode);
      // Fluttertoast.showToast(msg: jsonObject['token'], toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
      setState(() {});
    }
  }

  void doCashfreePayment(
      Map<String, dynamic> params, String token, String paymentMode) {

  }

  void doPayment(Map<String, dynamic> params) {

  }


  /* void doCashfreePayment(
      Map<String, dynamic> params, String token, String paymentMode) {
    params.putIfAbsent('tokenData', () => token);
    if (paymentMode == "upi") {
      CashfreePGSDK.getUPIApps().then((value) => {
            if (value!.length > 0)
              {
                CashfreePGSDK.doUPIPayment(params)
                    .then((value) => value?.forEach((key, value) {
                          debugPrint("$key : $value");
                          if (key == 'txStatus' && value == 'SUCCESS') {
                            getUserBalance();
                          } else {
                            // Fluttertoast.showToast(
                            //     msg: key == 'txStatus' ? value : 'FAILED',
                            //     toastLength: Toast.LENGTH_SHORT,
                            //     timeInSecForIosWeb: 1);
                          }
                        }))
              }
          });
    } else {
      CashfreePGSDK.doPayment(params)
          .then((value) => value?.forEach((key, value) {
                debugPrint("$key : $value");
                if (key == 'txStatus' && value == 'SUCCESS') {
                  getUserBalance();
                } else {
                  // Fluttertoast.showToast(
                  //     msg: key == 'txStatus' ? value : 'FAILED',
                  //     toastLength: Toast.LENGTH_SHORT,
                  //     timeInSecForIosWeb: 1);
                }
              }));
    }
  }*/

  void getUserBalance() async {
    GeneralRequest loginRequest =
    new GeneralRequest(user_id: userId, fcmToken: '');
    final client = ApiClient(AppRepository.dio);
    MyBalanceResponse myBalanceResponse =
    await client.getUserBalance(loginRequest);
    if (myBalanceResponse.status == 1) {
      MyBalanceResultItem myBalanceResultItem = myBalanceResponse.result![0];
      AppPrefrence.putString(AppConstants.KEY_USER_BALANCE,
          myBalanceResultItem.balance.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_WINING_AMOUNT,
          myBalanceResultItem.winning.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_BONUS_BALANCE,
          myBalanceResultItem.bonus.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_TOTAL_BALANCE,
          myBalanceResultItem.total.toString());
      Fluttertoast.showToast(
          msg: 'Payment add successfully',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      Navigator.pop(context);
      Navigator.pop(context);
    }
    setState(() {});
  }
}
