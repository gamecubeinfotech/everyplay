import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:EverPlay/views/AddBalance.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:EverPlay/views/Home.dart';
//import 'package:need_resume/need_resume.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomToolTip.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/my_balance_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:EverPlay/repository/retrofit/apis.dart';

import '../repository/model/account_play_response.dart';
import '../repository/model/transactions_response.dart';


class AccountNNew extends StatefulWidget {
  String userId;
  String? username;
  String? teamName;
  AccountNNew(this.userId);

  @override
  _AccountNNewState createState() => _AccountNNewState();
}

class _AccountNNewState extends State<AccountNNew> with SingleTickerProviderStateMixin {

  PlayDataResponse? playDataResponse;
 //MyBalanceResponse myBalanceResultItem = new MyBalanceResponse();
  var playerHLoader = true;
  var myBalanceLoader = true;
  var transactionLoader = true;
  List<TransactionItem> transactionList = [];

  late String userId='0';
  late String userName='';
  String? upiId;

  bool isAccountLoading = true;
  String userPic = '';
  ImagePicker _imagePicker=new ImagePicker();
  GlobalKey btnKey = GlobalKey();
  GlobalKey btnKey1 = GlobalKey();
  GlobalKey btnKey2 = GlobalKey();
 MyBalanceResultItem myBalanceResultItem=new MyBalanceResultItem(total: '0',winning: '0',balance: '0',totalamount: '0',bonus: '0');

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getLevelData();
        getUserBalance();
      })
    });

    AppPrefrence.getString(AppConstants.KEY_TEAM_NAME)
        .then((value) =>
    {
      setState(() {
        widget.teamName = value;
        print(widget.teamName);

      })
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_NAME)
        .then((value) =>
    {
      setState(() {
        widget.username = value;
        print(widget.username);
      })
    });


    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_PIC).then((value) =>
    {
      setState(() {
        userPic = value;
      })
    });
  }

  void getLevelData() async {
    AppLoaderProgress.showLoader(context);

    GeneralRequest loginRequest = new GeneralRequest(user_id: widget.userId);
    final client = ApiClient(AppRepository.dio);
    playDataResponse = await client.getPlayData(loginRequest);
    print(PlayDataResponse);
    AppLoaderProgress.hideLoader(context);
    setState(() {
      playerHLoader = false;
    });
  }

  @override
  void onResume() {
    getUserBalance();
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: darkPrimaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return Scaffold(
      backgroundColor: whiteColor,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black,
        centerTitle: false,
        title: Text("Account",style: TextStyle(
            fontSize: 18
        ),),

      ),
      body: playDataResponse==null?Center(child: CircularProgressIndicator(color: Themecolor,)):SingleChildScrollView(
        child: Column(
          children:<Widget> [
            SizedBox(height: 10,),
            Container(
              padding: EdgeInsets.fromLTRB(5,5,5,5),
              decoration: BoxDecoration(color:Themecolor),
              child: Padding(
                padding: const EdgeInsets.only(left: 8,right: 8,bottom: 8,top: 16),
                child: Column(children: [
                  Row(
                      children: [
                        Row(
                          children: [
                            Image.asset(AppImages.walletSymbol,scale: 2,),
                            SizedBox(width: 10,),
                            Column(
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(top: 7),
                                  child: new Text(
                                    '₹'+myBalanceResultItem.total.toString(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                                new Container(padding: EdgeInsets.symmetric(vertical: 5),
                                  child: new Text(
                                    'Total Balance',
                                    style: TextStyle(
                                        color: whiteColor,
                                        fontSize: 13,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(top: 5),
                                  child: Text('Winning + Deposit + Bonus',style: TextStyle(
                                      color: TextFieldBorderColor,
                                      fontSize: 10,
                                      fontWeight: FontWeight.w500)),
                                ),
                              ],crossAxisAlignment: CrossAxisAlignment.start,
                            ),
                          ],
                        ),
                        Spacer(),
                        Container(height:68,child: VerticalDivider(thickness: 2,color: Colors.white,)),
                        Column(
                          children: [
                            ElevatedButton(
                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(yellowdark),elevation: MaterialStateProperty.all(.5) ,shape: MaterialStateProperty.all(CircleBorder())) ,
                              child: Icon(Icons.add,color: whiteColor),
                              onPressed: () {
                                // push(context, MaterialPageRoute(builder: (context) => AddBalance('')));
                                navigateToAddBalance(context,'');
                              },
                            ),
                            SizedBox(height: 5,),
                            Text(
                              'Add Cash',
                              style: TextStyle(
                                  color: whiteColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ]
                  ),
                  SizedBox(height: 20,),
                ],),
              ),
            ),
            SizedBox(height: 10,),
            new Container(
              width: MediaQuery.of(context).size.width,
              child: new Container(
                margin: EdgeInsets.fromLTRB(15, 10, 15, 10),
                decoration: BoxDecoration(border: Border.all(color: TextFieldBorderColor),borderRadius: BorderRadius.all(Radius.circular(10))),
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                            top: 80,),
                          child: Padding(
                            padding: const EdgeInsets.only(
                                left: 5.0,
                                right: 5.0,
                                top: 5),
                            child: Row(
                              children: [
                                if(!isVerified())
                                Expanded(
                                    flex: 3,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        'Verify your account to activate withdraw money service',
                                        maxLines: 2,
                                        overflow: TextOverflow
                                            .ellipsis,
                                        style: TextStyle(
                                            color:
                                            Themecolor,
                                            fontSize: 12,fontWeight:FontWeight.w500),
                                        textAlign:
                                        TextAlign.start,
                                      ),
                                    )),
                                Expanded(
                                  flex: 2,
                                  child: GestureDetector(
                                    onTap: () {
                                      if(isVerified()){
                                        navigateToWithdrawCash(context);
                                      }
                                      else {
                                        navigateToVerifyAccount(context).then((value){
                                          getUserBalance();
                                        });
                                      }

                                      // if(isVerified()){
                                      //   showDialog(context: context, builder:(context){
                                      //     return Dialog(
                                      //       child: Container(
                                      //         decoration:BoxDecoration(
                                      //             borderRadius: BorderRadius.circular(5)
                                      //         ),
                                      //         child: Wrap(
                                      //           children: [
                                      //             Column(
                                      //               children: [
                                      //                 Container(
                                      //                   height: 50,
                                      //                   decoration:BoxDecoration(
                                      //                       color: yellowdark,
                                      //                       borderRadius: BorderRadius.only(topLeft: Radius.circular(3),topRight: Radius.circular(3))
                                      //                   ),
                                      //                   child:Padding(
                                      //                     padding: const EdgeInsets.all(8.0),
                                      //                     child: Row(
                                      //                       mainAxisAlignment: MainAxisAlignment.start,
                                      //                       crossAxisAlignment: CrossAxisAlignment.center,
                                      //                       children: [
                                      //                         Text('Winning Balance',style: TextStyle(
                                      //                             color: Colors.white,fontWeight: FontWeight.bold,fontSize: 14
                                      //                         ),),
                                      //                         SizedBox(width: 10,),
                                      //                         Text('₹' +
                                      //                             myBalanceResultItem
                                      //                                 .winning
                                      //                                 .toString(),
                                      //                           style: TextStyle(
                                      //                               color: Colors.white,fontWeight: FontWeight.bold,fontSize: 14
                                      //                           ),
                                      //                         ),
                                      //                         Spacer(),
                                      //                         GestureDetector(
                                      //                             onTap: (){
                                      //                               Navigator.pop(context);
                                      //                             },
                                      //                             child: Icon(Icons.clear,color: Colors.white,))
                                      //                       ],
                                      //                     ),
                                      //                   ),
                                      //                 ),
                                      //                 SizedBox(height: 10,),
                                      //                 Container(
                                      //                   child: Padding(
                                      //                     padding: const EdgeInsets.all(8.0),
                                      //                     child: Column(
                                      //                       crossAxisAlignment: CrossAxisAlignment.center,
                                      //                       mainAxisAlignment: MainAxisAlignment.center,
                                      //                       children: [
                                      //                         GestureDetector(
                                      //                           onTap:(){
                                      //                             navigateToWithdrawCash(context);
                                      //                           },
                                      //                           child: Container(
                                      //                             width:MediaQuery.of(context).size.width*0.6,
                                      //                             height: 40,
                                      //                             decoration: BoxDecoration(
                                      //                                 color: Colors.white,
                                      //                                 border: Border.all(color: Themecolor),
                                      //                                 borderRadius: BorderRadius.circular(5)
                                      //
                                      //                             ),
                                      //                             child: Center(
                                      //                               child: Text('Instant Bank Withdraw',style: TextStyle(
                                      //                                   color: Themecolor
                                      //                               ),),
                                      //                             ),
                                      //                           ),
                                      //                         ),
                                      //                         SizedBox(height: 10,),
                                      //                         GestureDetector(
                                      //                           onTap: (){
                                      //                             // upiId==null?navigateToUpiVerification(context) :navigateToUpiWithdraw(context);
                                      //                             navigateToWithdrawCash(context);
                                      //                           },
                                      //                           child: Container(
                                      //                             width:MediaQuery.of(context).size.width*0.6,
                                      //                             height: 40,
                                      //                             decoration: BoxDecoration(
                                      //                               color: Colors.white,
                                      //                               borderRadius: BorderRadius.circular(5),
                                      //                               border: Border.all(color: Themecolor),
                                      //                             ),
                                      //                             child: Center(
                                      //                               child: Text('Instant UPI Withdraw',style: TextStyle(
                                      //                                   color: Themecolor
                                      //                               ),),
                                      //                             ),
                                      //                           ),
                                      //                         ),
                                      //                         SizedBox(height: 30,),
                                      //                       ],
                                      //                     ),
                                      //                   ),
                                      //                 )
                                      //               ],
                                      //             ),
                                      //           ],
                                      //         ),
                                      //       ),
                                      //     );
                                      //   }
                                      //   );
                                      //   // navigateToWithdrawCash(context);
                                      // }
                                      // else {
                                      //   navigateToVerifyAccount(context).then((value){
                                      //
                                      //     getUserBalance();
                                      //   });
                                      // }
                                    },
                                    child: Container(
                                      margin:
                                      EdgeInsets.only(
                                          left: 0),
                                      height: 40,
                                      decoration: BoxDecoration(
                                          borderRadius:
                                          BorderRadius
                                              .all(Radius
                                              .circular(
                                              40)),
                                          color:
                                          isVerified()?Themecolor:yellowdark),
                                      alignment: Alignment.center,
                                      child: Padding(
                                        padding:
                                        const EdgeInsets
                                            .symmetric(vertical: 8.0,horizontal: 15),
                                        child: Text(
                                          isVerified()?"Withdraw":'Verify Account',
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Colors.white),
                                          textAlign:
                                          TextAlign
                                              .center,
                                        ),
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        Container(
                                          // margin: EdgeInsets.only(left: 5),
                                            height: 40,
                                            width: 40,
                                            child: new Image(
                                                image: AssetImage(AppImages.winning))),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        Padding(
                                          padding:
                                          const EdgeInsets
                                              .all(8.0),
                                          child: Container(
                                            margin:
                                            EdgeInsets.only(right: myBalanceResultItem.winning=="0"?70:20),
                                            child: new Column(
                                              children: [
                                                Text(
                                                  'Your Winnings',
                                                  style: TextStyle(
                                                      color: Colors
                                                          .black,
                                                      fontSize: 18),
                                                  textAlign:
                                                  TextAlign
                                                      .start,
                                                ),
                                                Text(
                                                  'The Money You Won',
                                                  style: TextStyle(
                                                      color: Colors
                                                          .grey,
                                                      fontSize: 12),
                                                  textAlign:
                                                  TextAlign
                                                      .start,
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Center(
                                      child: Text(
                                        '₹' +
                                            myBalanceResultItem
                                                .winning
                                                .toString(),
                                        style: TextStyle(
                                            fontFamily:
                                            AppConstants
                                                .textBold,
                                            color:Themecolor,
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Divider(color: lightGrayColor5),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Divider(color: lightGrayColor5),
                    Divider(color: lightGrayColor5),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                        child: new Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            new Container(
                              child: new Row(
                                children: [
                                  Image(image: AssetImage(AppImages.desposited),height: 50,),
                                  Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: new Text(
                                      'DEPOSITED',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight:
                                          FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(left: 93),
                                    child: new Text(
                                      '₹'+myBalanceResultItem.balance.toString(),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                          fontWeight:
                                          FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            new GestureDetector(behavior: HitTestBehavior.translucent,
                              child: new Container(
                                key: btnKey,
                                height: 20,
                                width: 20,
                                child: new Image.asset(
                                  AppImages.informationIcon,
                                ),
                              ),
                              onTap: (){
                                CustomToolTip popup = CustomToolTip(context,
                                  text: "Money deposited by you that you can use to join any contest but can't withdraw",
                                  textStyle: TextStyle(color: Colors.white,fontSize: 12),
                                  height: 65,
                                  backgroundColor: Colors.black,
                                  padding: EdgeInsets.all(10.0),
                                  borderRadius: BorderRadius.circular(5.0),
                                  shadowColor: Colors.transparent,
                                );
                                popup.show(
                                  widgetKey: btnKey,
                                );
                              },),
                          ],
                        ),
                      ),
                    ),
                    Divider(color: lightGrayColor5),
                    Divider(color: lightGrayColor5),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8,0,8,8),
                      child: Container(
                        child: new Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            new Container(
                              child: new Row(
                                children: [
                                  Image(image: AssetImage(AppImages.caseBonus),height: 50),
                                  new Container(
                                    padding: EdgeInsets.only(left: 15),
                                    child: new Text(
                                      'BONUS',
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight:
                                          FontWeight.w500),
                                    ),
                                  ),
                                  SizedBox(width: 35,),
                                  new Container(
                                    padding: EdgeInsets.only(left: 80),
                                    child: new Text(
                                      '₹'+myBalanceResultItem.bonus.toString(),
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                          fontWeight:
                                          FontWeight.w500),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            new GestureDetector(behavior: HitTestBehavior.translucent,
                              child: new Container(
                                key: btnKey2,
                                height: 20,
                                width: 20,
                                child: new Image.asset(
                                  AppImages.informationIcon,
                                ),
                              ),
                              onTap: (){
                                CustomToolTip popup = CustomToolTip(context,
                                  text: "Money that you can use to join any public contests",
                                  textStyle: TextStyle(color: Colors.white,fontSize: 12),
                                  height: 50,
                                  backgroundColor: Colors.black,
                                  padding: EdgeInsets.all(10.0),
                                  borderRadius: BorderRadius.circular(5.0),
                                  shadowColor: Colors.transparent,
                                );
                                popup.show(
                                  widgetKey: btnKey2,
                                );
                              },),
                          ],
                        ),
                      ),
                    ),

                    // new Divider(color: lightGrayColor5),
                    // new Container(
                    //   child: new Row(
                    //     mainAxisAlignment:
                    //     MainAxisAlignment.spaceBetween,
                    //     children: [
                    //       new Container(
                    //         child: new Column(
                    //           crossAxisAlignment:
                    //           CrossAxisAlignment.start,
                    //           children: [
                    //             new Container(
                    //               child: new Text(
                    //                 'REFERRAL EARNING',
                    //                 style: TextStyle(
                    //                     color: Colors.black,
                    //                     fontSize: 14,
                    //                     fontWeight:
                    //                     FontWeight.w500),
                    //               ),
                    //             ),
                    //             new Container(
                    //               margin: EdgeInsets.only(top: 7),
                    //               child: new Text('₹'+myBalanceResultItem.referral_earning.toString(),
                    //                 style: TextStyle(
                    //                     color: Colors.black,
                    //                     fontSize: 16,
                    //                     fontWeight:
                    //                     FontWeight.w500),
                    //               ),
                    //             ),
                    //           ],
                    //         ),
                    //       ),
                    //       new GestureDetector(behavior: HitTestBehavior.translucent,
                    //         child: new Container(
                    //           key: btnKey3,
                    //           height: 20,
                    //           width: 20,
                    //           child: new Image.asset(
                    //             AppImages.informationIcon,
                    //           ),
                    //         ),
                    //         onTap: (){
                    //           CustomToolTip popup = CustomToolTip(context,
                    //             text: "Money that you can use to join any public contests",
                    //             textStyle: TextStyle(color: Colors.white,fontSize: 12),
                    //             height: 50,
                    //             backgroundColor: Colors.black,
                    //             padding: EdgeInsets.all(10.0),
                    //             borderRadius: BorderRadius.circular(5.0),
                    //             shadowColor: Colors.transparent,
                    //           );
                    //           popup.show(
                    //             widgetKey: btnKey3,
                    //           );
                    //         },),
                    //     ],
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
            // Container(
            //   width: MediaQuery.of(context).size.width,
            //   child: new Card(
            //     shape: RoundedRectangleBorder(
            //       borderRadius: BorderRadius.circular(10.0),
            //     ),
            //     child: Padding(
            //       padding: const EdgeInsets.all(8.0),
            //       child: Column(
            //         children: [
            //           SizedBox(height: 10,),
            //           new Divider(color: lightGrayColor5),
            //           new Container(
            //             child: new Row(
            //               mainAxisAlignment:
            //               MainAxisAlignment.spaceBetween,
            //               children: [
            //                 new Container(
            //                   child: new Column(
            //                     crossAxisAlignment:
            //                     CrossAxisAlignment.start,
            //                     children: [
            //                       new Container(
            //                         child: new Text(
            //                           'DEPOSITED',
            //                           style: TextStyle(
            //                               color: darkgray,
            //                               fontSize: 14,
            //                               fontWeight:
            //                               FontWeight.w500),
            //                         ),
            //                       ),
            //                       new Container(
            //                         margin: EdgeInsets.only(top: 7),
            //                         child: new Text(
            //                           '₹'+myBalanceResultItem.balance.toString(),
            //                           style: TextStyle(
            //                               color: Colors.black,
            //                               fontSize: 16,
            //                               fontWeight:
            //                               FontWeight.w500),
            //                         ),
            //                       ),
            //                     ],
            //                   ),
            //                 ),
            //                 new GestureDetector(behavior: HitTestBehavior.translucent,
            //                   child: new Container(
            //                     key: btnKey,
            //                     height: 20,
            //                     width: 20,
            //                     child: new Image.asset(
            //                       AppImages.informationIcon,
            //                     ),
            //                   ),
            //                   onTap: (){
            //                     CustomToolTip popup = CustomToolTip(context,
            //                       text: "Money deposited by you that you can use to join any contest but can't withdraw",
            //                       textStyle: TextStyle(color: Colors.white,fontSize: 12),
            //                       height: 65,
            //                       backgroundColor: Colors.black,
            //                       padding: EdgeInsets.all(10.0),
            //                       borderRadius: BorderRadius.circular(5.0),
            //                       shadowColor: Colors.transparent,
            //                     );
            //                     popup.show(
            //                       widgetKey: btnKey,
            //                     );
            //                   },),
            //               ],
            //             ),
            //           ),
            //           new Divider(color: lightGrayColor5),
            //           new Container(
            //             child: new Row(
            //               mainAxisAlignment:
            //               MainAxisAlignment.spaceBetween,
            //               children: [
            //                 new Container(
            //                   child: new Column(
            //                     crossAxisAlignment:
            //                     CrossAxisAlignment.start,
            //                     children: [
            //                       new Container(
            //                         child: new Text(
            //                           'CASH BONUS',
            //                           style: TextStyle(
            //                               color: darkgray,
            //                               fontSize: 14,
            //                               fontWeight:
            //                               FontWeight.w500),
            //                         ),
            //                       ),
            //                       new Container(
            //                         margin: EdgeInsets.only(top: 7),
            //                         child: new Text(
            //                           '₹'+myBalanceResultItem.bonus.toString(),
            //                           style: TextStyle(
            //                               color: Colors.black,
            //                               fontSize: 16,
            //                               fontWeight:
            //                               FontWeight.w500),
            //                         ),
            //                       ),
            //                     ],
            //                   ),
            //                 ),
            //                 new GestureDetector(behavior: HitTestBehavior.translucent,
            //                   child: new Container(
            //                     key: btnKey2,
            //                     height: 20,
            //                     width: 20,
            //                     child: new Image.asset(
            //                       AppImages.informationIcon,
            //                     ),
            //                   ),
            //                   onTap: (){
            //                     CustomToolTip popup = CustomToolTip(context,
            //                       text: "Money that you can use to join any public contests",
            //                       textStyle: TextStyle(color: Colors.white,fontSize: 12),
            //                       height: 50,
            //                       backgroundColor: Colors.black,
            //                       padding: EdgeInsets.all(10.0),
            //                       borderRadius: BorderRadius.circular(5.0),
            //                       shadowColor: Colors.transparent,
            //                     );
            //                     popup.show(
            //                       widgetKey: btnKey2,
            //                     );
            //                   },),
            //               ],
            //             ),
            //           ),
            //
            //           // new Divider(color: lightGrayColor5),
            //           // new Container(
            //           //   child: new Row(
            //           //     mainAxisAlignment:
            //           //     MainAxisAlignment.spaceBetween,
            //           //     children: [
            //           //       new Container(
            //           //         child: new Column(
            //           //           crossAxisAlignment:
            //           //           CrossAxisAlignment.start,
            //           //           children: [
            //           //             new Container(
            //           //               child: new Text(
            //           //                 'REFERRAL EARNING',
            //           //                 style: TextStyle(
            //           //                     color: Colors.black,
            //           //                     fontSize: 14,
            //           //                     fontWeight:
            //           //                     FontWeight.w500),
            //           //               ),
            //           //             ),
            //           //             new Container(
            //           //               margin: EdgeInsets.only(top: 7),
            //           //               child: new Text('₹'+myBalanceResultItem.referral_earning.toString(),
            //           //                 style: TextStyle(
            //           //                     color: Colors.black,
            //           //                     fontSize: 16,
            //           //                     fontWeight:
            //           //                     FontWeight.w500),
            //           //               ),
            //           //             ),
            //           //           ],
            //           //         ),
            //           //       ),
            //           //       new GestureDetector(behavior: HitTestBehavior.translucent,
            //           //         child: new Container(
            //           //           key: btnKey3,
            //           //           height: 20,
            //           //           width: 20,
            //           //           child: new Image.asset(
            //           //             AppImages.informationIcon,
            //           //           ),
            //           //         ),
            //           //         onTap: (){
            //           //           CustomToolTip popup = CustomToolTip(context,
            //           //             text: "Money that you can use to join any public contests",
            //           //             textStyle: TextStyle(color: Colors.white,fontSize: 12),
            //           //             height: 50,
            //           //             backgroundColor: Colors.black,
            //           //             padding: EdgeInsets.all(10.0),
            //           //             borderRadius: BorderRadius.circular(5.0),
            //           //             shadowColor: Colors.transparent,
            //           //           );
            //           //           popup.show(
            //           //             widgetKey: btnKey3,
            //           //           );
            //           //         },),
            //           //     ],
            //           //   ),
            //           // ),
            //         ],
            //       ),
            //     ),
            //   ),
            // ),
            SizedBox(height: 20,),
            new GestureDetector (
              behavior: HitTestBehavior.translucent,
              child: new Container(
                child: new Container(
                  margin: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),color: TextFieldColor),
                  child: new Container(
                    padding: EdgeInsets.all(5),
                    child: new Container(
                      height: 50,
                      child: new Row(
                        mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                        children: [
                          Image(image: AssetImage(AppImages.recentTrx),height: 50),
                          new Container(
                            padding: EdgeInsets.all(5),
                            child: new Row(
                              children: [
                                new Text(
                                  'My Recent Transactions',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.only(right: 15),
                            height: 10,
                            width: 10,
                            child:
                            Image.asset(AppImages.moreForwardIcon),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              onTap: (){
                navigateToRecentTransactions(context);
              },
            ),
            new GestureDetector(
              behavior: HitTestBehavior.translucent,
              child: new Container(
                child: new Container(margin: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),color: TextFieldColor),
                  child: new Container(
                    padding: EdgeInsets.all(5),
                    child: new Container(
                      height: 50,
                      child: new Row(
                        mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                        children: [
                          Image(image: AssetImage(AppImages.referEarn),height: 50),
                          new Container(
                            padding: EdgeInsets.all(5),
                            child: new Row(
                              children: [
                                new Text(
                                  'Refer & Earn',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.only(right: 15),
                            height: 10,
                            width: 10,
                            child:
                            Image.asset(AppImages.moreForwardIcon),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              onTap: (){
                navigateToReferAndEarn(context);
              },
            ),
            // new Container(
            //   alignment: Alignment.centerLeft,
            //   margin: EdgeInsets.only(left: 6,top: 20),
            //   child: new Text(
            //     'Playing History',
            //     style: TextStyle(
            //         color: Colors.black,
            //         fontSize: 16,
            //         fontWeight: FontWeight.w500),
            //   ),
            // ),
            // new Container(
            //   margin: EdgeInsets.only(top: 10),
            //   child: new Row(
            //     children: [
            //       new Flexible(child:
            //       Container(
            //           margin: EdgeInsets.fromLTRB(10,10,5,10),
            //           color: TextFieldColor,
            //           height: 80,
            //           alignment: Alignment.center,
            //           padding: EdgeInsets.all(10),
            //           child:new Row(
            //             children: [
            //               new Container(
            //                 height: 35,
            //                 width: 35,
            //                 padding: EdgeInsets.all(5),
            //                 child: new Image.asset(
            //                   AppImages.matchPlayedIcon,
            //                   fit: BoxFit.fill,
            //                 ),
            //               ),
            //               new Container(
            //                 margin: EdgeInsets.only(left: 15,top: 10),
            //                 child: new Column(
            //                   crossAxisAlignment:
            //                   CrossAxisAlignment.start,
            //                   children: [
            //                     new Text(
            //                       playDataResponse!.result!.total_match_play.toString(),
            //                       style: TextStyle(
            //                           color: Colors.black,
            //                           fontSize: 16,
            //                           fontWeight: FontWeight.w500),
            //                     ),
            //                     new Container(
            //                       margin: EdgeInsets.only(top: 5),
            //                       child: new Text(
            //                         'Match Played',
            //                         style: TextStyle(
            //                             color: darkGrayColor,
            //                             fontSize: 12,
            //                             fontWeight:
            //                             FontWeight.w400),
            //                       ),
            //                     ),
            //                   ],
            //                 ),
            //               )
            //             ],
            //           )
            //       ),flex: 1,),
            //       new Flexible(child:
            //       Container(
            //           margin: EdgeInsets.fromLTRB(10,10,5,10),
            //           color: TextFieldColor,
            //           height: 80,
            //           alignment: Alignment.center,
            //           padding: EdgeInsets.all(10),
            //           child:new Row(
            //             children: [
            //               new Container(
            //                 height: 35,
            //                 width: 35,
            //                 padding: EdgeInsets.all(5),
            //                 child: new Image.asset(
            //                   AppImages.contestPlayedIcon,
            //                   fit: BoxFit.fill,
            //                 ),
            //               ),
            //               new Container(
            //                 margin: EdgeInsets.only(left: 15,top: 10),
            //                 child: new Column(
            //                   crossAxisAlignment:
            //                   CrossAxisAlignment.start,
            //                   children: [
            //                     new Text(
            //                       playDataResponse!.result!.total_league_play.toString(),
            //                       style: TextStyle(
            //                           color: Colors.black,
            //                           fontSize: 16,
            //                           fontWeight: FontWeight.w500),
            //                     ),
            //                     new Container(
            //                       margin: EdgeInsets.only(top: 5),
            //                       child: new Text(
            //                         'Contest Played',
            //                         style: TextStyle(
            //                             color: darkGrayColor,
            //                             fontSize: 12,
            //                             fontWeight:
            //                             FontWeight.w400),
            //                       ),
            //                     ),
            //                   ],
            //                 ),
            //               )
            //             ],
            //           )
            //       ),flex: 1,),
            //     ],
            //   ),
            // ),
            // new Container(
            //   margin: EdgeInsets.only(top: 10,bottom: 20),
            //   child: new Row(
            //     children: [
            //       new Flexible(child: new Container(
            //           color: TextFieldColor,
            //           margin: EdgeInsets.fromLTRB(10,5,5,10),
            //           height: 80,
            //           alignment: Alignment.center,
            //           padding: EdgeInsets.all(10),
            //           child:new Row(
            //             children: [
            //               new Container(
            //                 height: 35,
            //                 width: 35,
            //                 padding: EdgeInsets.all(5),
            //                 child: new Image.asset(
            //                   AppImages.contestWinIcon,
            //                   fit: BoxFit.fill,
            //                 ),
            //               ),
            //               new Container(
            //                 margin: EdgeInsets.only(left: 15,top: 10),
            //                 child: new Column(
            //                   crossAxisAlignment:
            //                   CrossAxisAlignment.start,
            //                   children: [
            //                     new Text(
            //                       playDataResponse!.result!.total_contest_win.toString(),
            //                       style: TextStyle(
            //                           color: Colors.black,
            //                           fontSize: 16,
            //                           fontWeight: FontWeight.w500),
            //                     ),
            //                     new Container(
            //                       margin: EdgeInsets.only(top: 5),
            //                       child: new Text(
            //                         'Contest Win',
            //                         style: TextStyle(
            //                             color: darkGrayColor,
            //                             fontSize: 12,
            //                             fontWeight:
            //                             FontWeight.w400),
            //                       ),
            //                     ),
            //                   ],
            //                 ),
            //               )
            //             ],
            //           )
            //       ),flex: 1,),
            //       new Flexible(child: new Container(
            //           margin: EdgeInsets.fromLTRB(10,5,5,10),
            //           color: TextFieldColor,
            //           height: 80,
            //           alignment: Alignment.center,
            //           padding: EdgeInsets.all(10),
            //           child:new Row(
            //             children: [
            //               new Container(
            //                 height: 35,
            //                 width: 35,
            //                 padding: EdgeInsets.all(5),
            //                 child: new Image.asset(
            //                   AppImages.totalWinningIcon,
            //                   fit: BoxFit.fill,
            //                 ),
            //               ),
            //               new Container(
            //                 margin: EdgeInsets.only(left: 15,top: 10),
            //                 child: new Column(
            //                   crossAxisAlignment:
            //                   CrossAxisAlignment.start,
            //                   children: [
            //
            //                     new Text(
            //                 '₹'+ (myBalanceResultItem.total_winning ?? "0").toString(),
            //                       style: TextStyle(
            //                           color: Colors.black,
            //                           fontSize: 16,
            //                           fontWeight: FontWeight.w500),
            //                     ),
            //                     new Container(
            //                       margin: EdgeInsets.only(top: 5),
            //                       child: new Text(
            //                         'Total Winning',
            //                         style: TextStyle(
            //                             color: darkGrayColor,
            //                             fontSize: 12,
            //                             fontWeight:
            //                             FontWeight.w400),
            //                       ),
            //                     ),
            //                   ],
            //                 ),
            //               )
            //             ],
            //           )
            //       ),flex: 1,),
            //     ],
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
  void getUserBalance() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,fcmToken: '');
    final client = ApiClient(AppRepository.dio);
    MyBalanceResponse myBalanceResponse = await client.getUserBalance(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (myBalanceResponse.status == 1) {
      myBalanceResultItem = myBalanceResponse.result![0];
      //upiId=myBalanceResultItem.upi_id;
      AppPrefrence.putString(AppConstants.KEY_USER_BALANCE, myBalanceResultItem.balance.toString());
      // AppPrefrence.putString(AppConstants.UpiId,myBalanceResultItem.upi_id);
      AppPrefrence.putString(AppConstants.KEY_USER_WINING_AMOUNT, myBalanceResultItem.winning.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_BONUS_BALANCE, myBalanceResultItem.bonus.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_TOTAL_BALANCE, myBalanceResultItem.total.toString());
      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, myBalanceResultItem.bank_verify);
      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, myBalanceResultItem.pan_verify);
      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, myBalanceResultItem.mobile_verify);
      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, myBalanceResultItem.email_verify);


    }
    // Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
    });
  }
  bool isVerified(){
    return myBalanceResultItem.mobile_verify==1
        && myBalanceResultItem.email_verify==1
        && myBalanceResultItem.pan_verify==1
        && myBalanceResultItem.bank_verify==1;
  }
  Future<void> showOptionsDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Select Profile Picture",style: TextStyle(color: Colors.black,fontSize: 16),),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  GestureDetector(
                    child: Text("Take Photo",style: TextStyle(color: Colors.black,fontSize: 16),),
                    onTap: () {
                      Navigator.pop(context);
                      _getImage(context,ImageSource.camera);
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  GestureDetector(
                    child: Text("Choose from Gallery",style: TextStyle(color: Colors.black,fontSize: 16),),
                    onTap: () {
                      Navigator.pop(context);
                      _getImage(context,ImageSource.gallery);
                    },
                  ),Padding(padding: EdgeInsets.all(10)),
                  GestureDetector(
                    child: Text("Remove Photo",style: TextStyle(color: Colors.black,fontSize: 16),),
                    onTap: () {
                      Navigator.pop(context);
                      removeProfilePhoto();
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  GestureDetector(
                    child: Text("Cancel",style: TextStyle(color: Colors.black,fontSize: 16),),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
  void _getImage(BuildContext context, ImageSource source) {
    _imagePicker.pickImage(source: source, imageQuality: 70).then((value) => {
      sendFile(File(value!.path))
    });
  }
  void sendFile(File file) async {
    AppLoaderProgress.showLoader(context);
    FormData formData = new FormData.fromMap({
      "user_id": userId,
      'file':await MultipartFile.fromFile(file.path, filename: file.path.split('/').last,)
    });
    Dio dio=new Dio();
    dio.options.headers["Content-Type"] = "multipart/form-data";
    var response = await dio.post(AppRepository.dio.options.baseUrl+Apis.uploadProfileImage, data: formData,).catchError((e) => debugPrint(e.response.toString()));
    AppLoaderProgress.hideLoader(context);
    var jsonObject=json.decode(response.toString());
    if(jsonObject['status']==1){
      userPic=jsonObject['result'][0]['image'];
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_PIC, userPic);
      MethodUtils.showSuccess(context, 'Profile Photo Uploaded Successfully.');
      setState(() {});
    }else{
      MethodUtils.showError(context, 'Error in image uploading.');
    }
  }
  void removeProfilePhoto() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    GeneralResponse response = await client.removeProfilePhoto(loginRequest);
    if (response.status == 1) {
      userPic='';
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_PIC, '');
      setState(() {
        AppLoaderProgress.hideLoader(context);
      });
      MethodUtils.showSuccess(context, 'Profile Photo Removed Successfully.');
    }
  }

}