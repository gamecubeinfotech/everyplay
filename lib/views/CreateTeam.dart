import 'package:cached_network_image/cached_network_image.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/date_utils.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/ScrollingText.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/dataModels/SelectedPlayer.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/contest_request.dart';
import 'package:EverPlay/repository/model/player_list_response.dart';
import 'package:EverPlay/repository/model/team_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:EverPlay/views/CreateTeamPager.dart';

import '../repository/model/category_contest_response.dart';

class CreateTeam extends StatefulWidget {
  GeneralModel model;
  Contest? contest;
  CreateTeam(this.model,{this.contest});
  @override
  _CreateTeamState createState() => new _CreateTeamState();
}

class _CreateTeamState extends State<CreateTeam> with SingleTickerProviderStateMixin{
  int _currentMatchIndex = 0;
  int localTeamplayerCount = 0;
  final timeController=CustomTimerController();
  int visitorTeamPlayerCount = 0;
  List<Widget> tabs = <Widget>[];
  List<Widget> tabsView = <Widget>[];
  bool clearTeamDialog = false;
  bool rulesDialog = false;
  bool exeedCredit = false;
  bool fantasyAlertShown = false;
  int isPointSorted = 0, isCreditSorted = 0, isPLayerSorted = 0;
  String userId='0';
  String filterType='All';
  static final int WK = 1;
  static final int BAT = 2;
  static final int AR = 3;
  static final int BOWLER = 4;
  static final int C = 5;
  late TabController _tabController;
  List<Player> team1wkList = [];
  List<Player> team2wkList = [];
  List<Player> wkListTemp = [];
  List<Player> wkList = [];
  List<Player> team1bolList = [];
  List<Player> team2bolList = [];
  List<Player> bolListTemp = [];
  List<Player> bolList = [];
  List<Player> team1batList = [];
  List<Player> team2batList = [];
  List<Player> batListTemp = [];
  List<Player> batList = [];
  List<Player> team1arList = [];
  List<Player> team2arList = [];
  List<Player> arListTemp = [];
  List<Player> arList = [];
  List<Player> team1cList = [];
  List<Player> team2cList = [];
  List<Player> cListTemp = [];
  List<Player> cList = [];
  List<Player> allPlayerList = [];
  List<Player> selectedList = [];
  late Limit limit=Limit(total_credits: 0,maxplayers: 0);
  int islineUp=0;
  SelectedPlayer selectedPlayer=new SelectedPlayer();

  @override
  void initState() {
    super.initState();
    if(widget.model.isFromEditOrClone??false){
      selectedList=widget.model.selectedList??[];
      widget.model.isFromEditOrClone=false;
    }
    _tabController = new TabController( length: widget.model.sportKey==AppConstants.TAG_HANDBALL||widget.model.sportKey==AppConstants.TAG_KABADDI?3:widget.model.sportKey==AppConstants.TAG_BASKETBALL?5:4, vsync: this);
    _tabController.animation!..addListener(() {
      setState(() {
        _currentMatchIndex = (_tabController.animation!.value).round();

      });

    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getPlayerList();
      })
    });
    if (widget.model.fantasyType == AppConstants.BATTING_FANTASY_TYPE) {
      _tabController.index=1;
    } else if (widget.model.fantasyType == AppConstants.BOWLING_FANTASY_TYPE) {
      _tabController.index=3;
    }

  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
            statusBarColor:primaryColor,

             statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.dark) /* set Status bar icon color in iOS. */
        );
    return SafeArea(
      child: new Scaffold(
          body: new WillPopScope(
              child: new Stack(
                children: [
                  new Scaffold(
                    backgroundColor: bgColorDark,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(55), // Set this height
                      child: Container(
                        color: Color(0xFF1a1a1a),
                        // padding: EdgeInsets.only(top: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Row(
                              children: [
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: () => {_onWillPop()},
                                  child: new Container(
                                    padding: EdgeInsets.fromLTRB(15, 15, 25, 15),
                                    alignment: Alignment.centerLeft,
                                    child: new Container(
                                      width: 16,
                                      height: 16,
                                      child: Image(
                                        image: AssetImage(AppImages.backImageURL),
                                        fit: BoxFit.fill,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    SizedBox(
                                      //width:50,
                                      child: new Text(widget.model.teamVs.toString(),
                                          style: TextStyle(
                                              fontFamily: AppConstants.textBold,
                                              color: Colors.white,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16)),
                                    ),
                                    new Container(
                                     // margin: EdgeInsets.only(left: 10),
                                      child: CustomTimer(
                                        controller: timeController,
                                        from: Duration(milliseconds: SuperDateFormat.getFormattedDateObj(widget.model.headerText!)!.millisecondsSinceEpoch-MethodUtils.getDateTime().millisecondsSinceEpoch),
                                        to: Duration(milliseconds: 0),
                                        onBuildAction: CustomTimerAction.auto_start,
                                        builder: (CustomTimerRemainingTime remaining) {

                                          if(remaining.hours=='00'&&remaining.minutes=='00'&& remaining.seconds=='00') {
                                            navigateToHomePage(context);
                                          }

                                          return new GestureDetector(
                                            behavior: HitTestBehavior.translucent,
                                            child: Text(
                                              int.parse(remaining.days)>=1?int.parse(remaining.days)>1?"${remaining.days} Days "+"Left":"${remaining.days} Day "+"Left":int.parse(remaining.hours)>=1?"${remaining.hours}Hrs : ${remaining.minutes}Mins ": " ${remaining.minutes} Mins :${remaining.seconds}Sec " +"Left",
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  decoration: TextDecoration.none,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 11),
                                            ),
                                            onTap: (){
                                            },
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    body: new Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        new Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          color: Themecolor,
                          child: new Column(
                            children: [
                              SizedBox(height: 10 ,),
                              Center(
                                child: Text('YOU CAN SELECT 5 FROM EACH TEAM.',style: TextStyle(
                                    color: Colors.white,fontSize: 14,
                                    fontWeight: FontWeight.w500
                                ),),
                              ),
                              new Container(
                                padding: EdgeInsets.all(0),
                                margin: EdgeInsets.only(
                                    left: 10, top: 15, bottom: 13, right: 10),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(top: 2),
                                      child: new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          new Container(
                                            child: new Text(
                                              'Players',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 12),
                                            ),
                                          ),
                                          SizedBox(height: 10,),
                                          new Container(
                                            child: new Text(
                                              (selectedPlayer.selectedPlayer).toString()+ '/${(widget.model.fantasyType==0||widget.model.fantasyType==5) ? "8" :'5'}',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 16),
                                            ),
                                          ),


                                        ],
                                      ),
                                    ),
                                    new Container(child: new Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        children: [
                                          new Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment.start,
                                            children: [


                                              new Column(
                                                children: [
                                                  new Container(
                                                    height: 40,
                                                    width: 40,
                                                    margin: EdgeInsets.only(
                                                        bottom: 5),
                                                    child: CachedNetworkImage(
                                                      imageUrl: widget.model.team1Logo!,
                                                      placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                                      errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                                      fit: BoxFit.fill,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              new Container(
                                                margin: EdgeInsets.only(left: 10),
                                                child: new Column(
                                                  // crossAxisAlignment: CrossAxisAlignment.start,
                                                  children: [
                                                    new Container(
                                                      // width: 50,
                                                      margin:
                                                      EdgeInsets.only(top: 2),
                                                      child: new Text(
                                                        widget.model.teamVs!.split(' VS ')[0],
                                                        textAlign: TextAlign.left,
                                                        style: TextStyle(
                                                            fontFamily: 'noway',
                                                            color: Colors.white,
                                                            fontWeight:
                                                            FontWeight.w600,
                                                            fontSize: 14),
                                                      ),
                                                    ),
                                                    new Container(
                                                      // height: 24,
                                                      // width: 24,
                                                      // margin: EdgeInsets.only(left: 5),
                                                      alignment: Alignment.center,
                                                      // decoration: BoxDecoration(
                                                      //     border: Border.all(
                                                      //         color:
                                                      //         Colors.transparent),
                                                      //     borderRadius:
                                                      //     BorderRadius.circular(12),
                                                      //     color: Colors.white12),
                                                      child: new Text(
                                                        localTeamplayerCount.toString(),
                                                        style: TextStyle(
                                                            fontFamily: 'noway',
                                                            color: Colors.white,
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: 14),
                                                        textAlign: TextAlign.center,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                            ],
                                          ),
                                        ])),
                                    Container(
                                      decoration: BoxDecoration(
                                          // color: Colors.black,
                                          shape: BoxShape.circle,
                                          border: Border.all(color: Colors.white)

                                      ),
                                      height: 25,
                                      width: 30,
                                      child: Center(
                                        child: new Text(
                                          "VS",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontFamily: 'noway',
                                              color: Colors.white,
                                              // fontWeight: FontWeight.bold,
                                              fontSize: 10),
                                        ),
                                      ),
                                    ),
                                    // new Container(
                                    //   margin: EdgeInsets.only(top: 2),
                                    //   child: new Column(
                                    //     children: [
                                    //       new Container(
                                    //         child: new Text(
                                    //           'Credits Left',
                                    //           textAlign: TextAlign.left,
                                    //           style: TextStyle(
                                    //               color: Colors.grey,
                                    //               fontWeight: FontWeight.w400,
                                    //               fontSize: 12),
                                    //         ),
                                    //       ),
                                    //       new Container(
                                    //         child: new Text(
                                    //           (limit.total_credits!-selectedPlayer.total_credit).toString(),
                                    //           textAlign: TextAlign.left,
                                    //           style: TextStyle(
                                    //               color: Colors.white,
                                    //               fontWeight: FontWeight.w500,
                                    //               fontSize: 16),
                                    //         ),
                                    //       )
                                    //     ],
                                    //   ),
                                    // ),
                                    new Container(
                                        child: new Column(
                                          crossAxisAlignment: CrossAxisAlignment.end,
                                          children: [
                                            new Row(
                                              children: [
                                                // new Container(
                                                //   height: 24,
                                                //   width: 24,
                                                //   margin: EdgeInsets.only(right: 5),
                                                //   alignment: Alignment.center,
                                                //   decoration: BoxDecoration(
                                                //       border: Border.all(
                                                //           color: Colors.transparent),
                                                //       borderRadius:
                                                //       BorderRadius.circular(12),
                                                //       color: Colors.white12),
                                                //   child: new Text(
                                                //     visitorTeamPlayerCount.toString(),
                                                //     style: TextStyle(
                                                //         fontFamily: 'noway',
                                                //         color: Colors.white,
                                                //         fontWeight: FontWeight.w500,
                                                //         fontSize: 14),
                                                //     textAlign: TextAlign.center,
                                                //   ),
                                                // ),
                                                new Container(
                                                  margin: EdgeInsets.only(right: 10),
                                                  child: new Column(
                                                    // crossAxisAlignment:
                                                    // CrossAxisAlignment.start,
                                                    children: [
                                                      new Container(
                                                        // width: 60,
                                                        alignment: Alignment.centerRight,
                                                        margin:
                                                        EdgeInsets.only(top: 2),
                                                        child: new Text(" "+widget.model.teamVs!.split(' VS ')[1]+"",
                                                          textAlign: TextAlign.left,
                                                          style: TextStyle(
                                                              fontFamily: 'noway',
                                                              color: Colors.white,
                                                              fontWeight:
                                                              FontWeight.w600,
                                                              fontSize: 14),
                                                        ),
                                                      ),
                                                      new Container(
                                                        // height: 24,
                                                        // width: 24,
                                                        // margin: EdgeInsets.only(left: 5),
                                                        alignment: Alignment.center,
                                                        // decoration: BoxDecoration(
                                                        //     border: Border.all(
                                                        //         color:
                                                        //         Colors.transparent),
                                                        //     borderRadius:
                                                        //     BorderRadius.circular(12),
                                                        //     color: Colors.white12),
                                                        child: new Text(
                                                          "${visitorTeamPlayerCount.toString()}",
                                                          style: TextStyle(
                                                              fontFamily: 'noway',
                                                              color: Colors.white,
                                                              fontWeight: FontWeight.w500,
                                                              fontSize: 14),
                                                          textAlign: TextAlign.center,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                new Column(
                                                  children: [
                                                    new Container(
                                                      height: 40,
                                                      width: 40,
                                                      margin: EdgeInsets.only(
                                                        bottom: 5,
                                                      ),
                                                      child: CachedNetworkImage(
                                                        imageUrl: widget.model.team2Logo!,
                                                        placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                                        errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                                        fit: BoxFit.fill,
                                                      ),
                                                    ),
                                                  ],
                                                ),


                                              ],
                                            ),
                                            // new Text(
                                            //   widget.model.team2Name!,
                                            //   textAlign: TextAlign.left,
                                            //   style: TextStyle(
                                            //       fontFamily: 'noway',
                                            //       color: Colors.grey,
                                            //       fontWeight: FontWeight.w400,
                                            //       fontSize: 12),
                                            // ),
                                          ],
                                        )),
                                    new Container(
                                      margin: EdgeInsets.only(top: 2),
                                      child: new Column(
                                        crossAxisAlignment: CrossAxisAlignment.end,
                                        children: [
                                          new Container(
                                            child: new Text(
                                              'Credits Left',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 12),
                                            ),
                                          ),
                                          SizedBox(height: 10,),
                                          new Container(
                                            child: new Text(
                                              (limit.total_credits!-selectedPlayer.total_credit).toString(),
                                              textAlign: TextAlign.left,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 16),
                                            ),
                                          ),


                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),



                              new Container(
                                padding: EdgeInsets.only(top: 7, bottom: 7),
                               // color: Colors.black38,
                                width: MediaQuery.of(context).size.width,

                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    new Container(
                                        height: 25,
                                        child: new ListView.builder(
                                            shrinkWrap: true,
                                            scrollDirection: Axis.horizontal,
                                            itemCount: limit.maxplayers,
                                            itemBuilder:
                                                (BuildContext context, int index) {
                                              return new Container(
                                                height: 40,
                                                width: 40,

                                                // margin: EdgeInsets.only(right: 1),
                                                alignment: Alignment.center,
                                                decoration: BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    border: Border.all(
                                                        color: index<selectedPlayer.selectedPlayer?yellowdark:Colors.white),
                                                    color: index<selectedPlayer.selectedPlayer?yellowdark:Colors.transparent),
                                                child: Container(
                                                  child: Center(
                                                    child: Text('${index+1}',style: TextStyle(
                                                        color: index<selectedPlayer.selectedPlayer?Colors.white:Colors.white,fontSize: 10,fontWeight: FontWeight.bold
                                                    ),),
                                                  ),
                                                ),
                                              );
                                            })
                                    ),
                                    SizedBox(width: 5,),
                                    new GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      child: new Container(
                                        height: 24,
                                        padding: EdgeInsets.only(left: 5, right: 5),
                                        margin: EdgeInsets.only(right: 5),
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            border: Border.all(color: Colors.white),
                                            // borderRadius: BorderRadius.circular(3),
                                            color: Colors.transparent),
                                        child: Center(
                                          child: new Container(
                                            height: 11,
                                            width: 11,
                                            // margin: EdgeInsets.only(right: 5),
                                            child: Image(
                                              image: AssetImage(AppImages.deleteIcon),
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                      onTap: () {
                                        if(selectedPlayer.selectedPlayer>0){
                                          setState(() {
                                            clearTeamDialog = true;
                                          });
                                        }else{
                                          MethodUtils.showError(context, 'No player selected to clear.');
                                        }
                                      },
                                    ),

                                  ],
                                ),
                              ),
                              islineUp==1?new Container(
                                decoration: BoxDecoration(
                                    color: TextFieldColor,

                                  border:Border(
                                      bottom: BorderSide(color: Colors.grey.shade300)                                      ),
                                ),
                                height: 35,

                                child: new Row(
                                  children: [
                                    new Container(
                                      height: 30,
                                      width: 20,

                                      margin:
                                      EdgeInsets.only(bottom: 5, left: 10),
                                      child: Image(
                                          image: AssetImage(
                                              AppImages.megaphoneIcon)),
                                    ),
                                    SizedBox(width : 5),
                                    new Container(
                                      width:
                                      MediaQuery.of(context).size.width - 40,
                                      child: new ScrollingText(
                                        text:
                                        'Lineup features is for your convenience. please do due research before creating team.',
                                        textStyle: TextStyle(
                                            fontFamily: 'noway',
                                            color: Title_Color_1,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 12),
                                      ),
                                    ),
                                  ],
                                ),
                              ):new Container(),


                              new Expanded(
                                  child: new Container(
                                    color: Colors.white,
                                height: MediaQuery.of(context).size.height,
                                child: new DefaultTabController(
                                  length: widget.model.sportKey==AppConstants.TAG_HANDBALL||widget.model.sportKey==AppConstants.TAG_KABADDI?3:widget.model.sportKey==AppConstants.TAG_BASKETBALL?5:4,
                                  child: Scaffold(
                                    backgroundColor: Colors.white,
                                    appBar: PreferredSize(
                                      preferredSize: Size.fromHeight(40),
                                      child: new Container(
                                        color: Colors.white,
                                        child: TabBar(
                                          controller: _tabController,
                                          labelPadding: EdgeInsets.all(0),
                                          onTap: (int index) {
                                            setState(() {
                                              _currentMatchIndex = index;
                                            });
                                          },
                                          indicatorWeight: 2,
                                          indicatorSize:
                                              TabBarIndicatorSize.label,
                                          indicatorColor: primaryColor,
                                          // indicator: ShapeDecoration(
                                          //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10))),
                                          //     color: Colors.black
                                          // ),
                                          isScrollable: false,
                                          labelColor: Colors.black,
                                          unselectedLabelColor: Colors.grey,
                                          tabs: getTabs(),
                                        ),
                                      ),
                                    ),
                                    body: new Container(
                                      child: new Column(
                                        children: [
                                          new Divider(
                                            height: .5,
                                            thickness: .5,
                                          ),
                                          new Container(
                                            height: 40,
                                            width:
                                                MediaQuery.of(context).size.width,
                                            padding: EdgeInsets.only(
                                                left: 10, right: 10),
                                            color: Colors.white,
                                            child: new Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                              children: [
                                                new GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.translucent,
                                                  child: new Container(
                                                    child: new Row(

                                                      children: [
                                                        new Text(
                                                            _currentMatchIndex==0?'Pick '+selectedPlayer.wk_min_count.toString()+'-'+selectedPlayer.wk_max_count.toString()+(widget.model.sportKey==AppConstants.TAG_CRICKET?' Wicket-Keeper':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Outfielders':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Goal-Keeper':widget.model.sportKey==AppConstants.TAG_HANDBALL?' Goal-Keeper':widget.model.sportKey==AppConstants.TAG_KABADDI?' Defender':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Point-Guard':' Goal-Keeper'):
                                                            _currentMatchIndex==1?'Pick '+selectedPlayer.bat_mincount.toString()+'-'+selectedPlayer.bat_maxcount.toString()+(widget.model.sportKey==AppConstants.TAG_CRICKET?' Batter':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Infielders':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Defender':widget.model.sportKey==AppConstants.TAG_HANDBALL?' Defender':widget.model.sportKey==AppConstants.TAG_KABADDI?' All-Rounder':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Shooting-Guard':' Defender'):
                                                            _currentMatchIndex==2?'Pick '+selectedPlayer.ar_mincount.toString()+'-'+selectedPlayer.ar_maxcount.toString()+(widget.model.sportKey==AppConstants.TAG_CRICKET?' All-Rounders':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Pitcher':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Midfielder':widget.model.sportKey==AppConstants.TAG_HANDBALL?' State-Forward':widget.model.sportKey==AppConstants.TAG_KABADDI?' Raider':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Small-Forward':' Midfielder'):
                                                            _currentMatchIndex==3?'Pick '+selectedPlayer.bowl_mincount.toString()+'-'+selectedPlayer.bowl_maxcount.toString()+(widget.model.sportKey==AppConstants.TAG_CRICKET?' Bowlers':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Catcher':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Striker':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Power-Forward':' State-Forward'):
                                                            'Pick '+selectedPlayer.c_min_count.toString()+'-'+selectedPlayer.c_max_count.toString()+' Center',
                                                            style: TextStyle(
                                                                fontFamily:
                                                                    AppConstants
                                                                        .textBold,
                                                                color:
                                                                    Color(0xFF8e9193),
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w500,
                                                                fontSize: 12)),
                                                        new Container(
                                                          margin: EdgeInsets.only(
                                                              left: 5),
                                                          height: 13,
                                                          width: 13,
                                                          child: Image(
                                                            image: AssetImage(
                                                              AppImages.infoIcon,
                                                            ),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  onTap: (){
                                                    setState(() {
                                                      rulesDialog=true;
                                                    });
                                                  },
                                                ),
                                                new GestureDetector(
                                                  behavior:
                                                      HitTestBehavior.translucent,
                                                  child: new Container(
                                                    child: new Row(
                                                      children: [
                                                        new Container(
                                                          margin: EdgeInsets.only(
                                                              right: 5),
                                                          height: 12,
                                                          width: 12,
                                                          child: Image(
                                                            image: AssetImage(
                                                              AppImages
                                                                  .filterIcon,
                                                            ),
                                                            color: Colors.black,
                                                          ),
                                                        ),
                                                        // new Text(
                                                        //     'Filter by Teams',
                                                        //     style: TextStyle(
                                                        //         fontFamily:
                                                        //             AppConstants
                                                        //                 .textBold,
                                                        //         color: Colors
                                                        //             .black87,
                                                        //         fontWeight:
                                                        //             FontWeight
                                                        //                 .normal,
                                                        //         fontSize: 11)),
                                                      ],
                                                    ),
                                                  ),
                                                  onTap: () =>
                                                      {_modalBottomSheetMenu()},
                                                ),
                                              ],
                                            ),
                                          ),
                                          new Divider(
                                            height: .5,
                                            thickness: .5,
                                          ),

                                          new Container(
                                            color:lightgrey,
                                            height: 40,
                                            child: new Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                              children: [
                                                new Flexible(
                                                  child: new Container(
                                                    color:lightgrey,
                                                       margin: EdgeInsets.only(right: 15),
                                                      child: new GestureDetector(
                                                        behavior: HitTestBehavior.translucent,
                                                        child: new Row(
                                                          // mainAxisAlignment: MainAxisAlignment.end,
                                                          children: [
                                                            new Container(
                                                              margin: EdgeInsets.only(left: 10),
                                                              alignment:
                                                              Alignment.centerRight,
                                                              child: Text(
                                                                'Selected BY'.toUpperCase(),
                                                                style: TextStyle(
                                                                    color: Colors.black,
                                                                    fontWeight:
                                                                    FontWeight.w500,
                                                                    fontSize: 12),
                                                              ),
                                                            ),
                                                            SizedBox(width: 5,),
                                                            isPLayerSorted!=0?new Container(
                                                              height: 13,
                                                              width: 5,
                                                              child: Image(
                                                                image: AssetImage(isPLayerSorted==1?AppImages.downSort:AppImages.upSort,),
                                                                  color: primaryColor,
                                                              ),
                                                            ):new Container(),
                                                          ],
                                                        ),
                                                        onTap: (){
                                                          setState(() {
                                                            if(isPLayerSorted==2||isPLayerSorted==0){
                                                              isPLayerSorted=1;
                                                              wkList.sort((a,b) => a.getSelectedBy().compareTo(b.getSelectedBy()));
                                                              batList.sort((a,b) => a.getSelectedBy().compareTo(b.getSelectedBy()));
                                                              arList.sort((a,b) => a.getSelectedBy().compareTo(b.getSelectedBy()));
                                                              bolList.sort((a,b) => a.getSelectedBy().compareTo(b.getSelectedBy()));
                                                            }else{
                                                              isPLayerSorted=2;
                                                              wkList.sort((a,b) => b.getSelectedBy().compareTo(a.getSelectedBy()));
                                                              batList.sort((a,b) => b.getSelectedBy().compareTo(a.getSelectedBy()));
                                                              arList.sort((a,b) => b.getSelectedBy().compareTo(a.getSelectedBy()));
                                                              bolList.sort((a,b) => b.getSelectedBy().compareTo(a.getSelectedBy()));
                                                            }
                                                            isPointSorted=0;
                                                            isCreditSorted=0;
                                                          });
                                                        },
                                                      ),
                                                  ),
                                                  flex: 1,
                                                ),
                                                new Flexible(
                                                  child: new Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: [
                                                      new GestureDetector(
                                                        behavior: HitTestBehavior.translucent,
                                                        child: new Row(
                                                          children: [
                                                            new Container(
                                                              color: lightgrey,
                                                              margin: EdgeInsets.only(right: 5,),
                                                              alignment:
                                                              Alignment.centerLeft,
                                                              child: Text(
                                                                'Points'.toUpperCase(),
                                                                style: TextStyle(
                                                                    color: Colors.black,
                                                                    fontWeight:
                                                                    FontWeight.w500,
                                                                    fontSize: 12),
                                                              ),
                                                            ),
                                                            isPointSorted!=0?new Container(
                                                              height: 13,
                                                              width: 5,
                                                              child: Image(
                                                                image: AssetImage(isPointSorted==1?AppImages.downSort:AppImages.upSort,
                                                                ),
                                                                color: primaryColor,
                                                              ),
                                                            ):new Container(),
                                                          ],
                                                        ),
                                                        onTap: (){
                                                          setState(() {
                                                            if(isPointSorted==2||isPointSorted==0){
                                                              isPointSorted=1;
                                                              wkList.sort((a,b) => a.getPoints().compareTo(b.getPoints()));
                                                              batList.sort((a,b) => a.getPoints().compareTo(b.getPoints()));
                                                              arList.sort((a,b) => a.getPoints().compareTo(b.getPoints()));
                                                              bolList.sort((a,b) => a.getPoints().compareTo(b.getPoints()));
                                                            }else{
                                                              isPointSorted=2;
                                                              wkList.sort((a,b) => b.getPoints().compareTo(a.getPoints()));
                                                              batList.sort((a,b) => b.getPoints().compareTo(a.getPoints()));
                                                              arList.sort((a,b) => b.getPoints().compareTo(a.getPoints()));
                                                              bolList.sort((a,b) => b.getPoints().compareTo(a.getPoints()));
                                                            }
                                                            isPLayerSorted=0;
                                                            isCreditSorted=0;
                                                          });
                                                        },
                                                      ),
                                                      new Container(
                                                          child: new GestureDetector(
                                                            behavior: HitTestBehavior.translucent,
                                                            child: new Row(
                                                              children: [
                                                                new Container(
                                                                  color: lightgrey,
                                                                  margin: EdgeInsets.only(),
                                                                  alignment:
                                                                  Alignment.center,
                                                                  child: Text(
                                                                    'Credits'.toUpperCase(),
                                                                    style: TextStyle(
                                                                        color: Colors.black,
                                                                        fontWeight:
                                                                        FontWeight.w500,
                                                                        fontSize: 12),
                                                                  ),
                                                                ),
                                                                SizedBox(width: 5,),
                                                                isCreditSorted!=0?new Container(
                                                                  height: 13,
                                                                  width: 5,
                                                                  child: Image(
                                                                    image: AssetImage(isCreditSorted==1?AppImages.downSort:AppImages.upSort,
                                                                    ),
                                                                    color: primaryColor,
                                                                  ),
                                                                ):new Container(),
                                                              ],
                                                            ),
                                                            onTap: (){
                                                              setState(() {
                                                                if(isCreditSorted==2||isCreditSorted==0){
                                                                  isCreditSorted=1;
                                                                  wkList.sort((a,b) => a.getCredits().compareTo(b.getCredits()));
                                                                  batList.sort((a,b) => a.getCredits().compareTo(b.getCredits()));
                                                                  arList.sort((a,b) => a.getCredits().compareTo(b.getCredits()));
                                                                  bolList.sort((a,b) => a.getCredits().compareTo(b.getCredits()));
                                                                }else{
                                                                  isCreditSorted=2;
                                                                  wkList.sort((a,b) => b.getCredits().compareTo(a.getCredits()));
                                                                  batList.sort((a,b) => b.getCredits().compareTo(a.getCredits()));
                                                                  arList.sort((a,b) => b.getCredits().compareTo(a.getCredits()));
                                                                  bolList.sort((a,b) => b.getCredits().compareTo(a.getCredits()));
                                                                }
                                                                isPLayerSorted=0;
                                                                isPointSorted=0;
                                                              });
                                                            },
                                                          ),
                                                      ),
                                                    ],
                                                  ),
                                                  flex: 1,
                                                ),
                                              ],
                                            ),
                                          ),

                                          // new Divider(
                                          //   height: .5,
                                          //   thickness: .5,
                                          // ),

                                          new Expanded(
                                            child: new TabBarView(
                                              controller: _tabController,
                                              children: [
                                                new CreateTeamPager(wkList,onPlayerClick,1,selectedPlayer,limit,widget.model.fantasyType!,widget.model.slotId!,exeedCredit,widget.model.matchKey,widget.model.sportKey),
                                                new CreateTeamPager(batList,onPlayerClick,2,selectedPlayer,limit,widget.model.fantasyType!,widget.model.slotId!,exeedCredit,widget.model.matchKey,widget.model.sportKey),
                                                new CreateTeamPager(arList,onPlayerClick,3,selectedPlayer,limit,widget.model.fantasyType!,widget.model.slotId!,exeedCredit,widget.model.matchKey,widget.model.sportKey),
                                                if(widget.model.sportKey!=AppConstants.TAG_HANDBALL&&widget.model.sportKey!=AppConstants.TAG_KABADDI)
                                                    new CreateTeamPager(bolList,onPlayerClick,4,selectedPlayer,limit,widget.model.fantasyType!,widget.model.slotId!,exeedCredit,widget.model.matchKey,widget.model.sportKey),
                                                if(widget.model.sportKey==AppConstants.TAG_BASKETBALL)
                                                    new CreateTeamPager(cList,onPlayerClick,5,selectedPlayer,limit,widget.model.fantasyType!,widget.model.slotId!,exeedCredit,widget.model.matchKey,widget.model.sportKey)

                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ))
                            ],
                          ),
                        ),

                        new Container(
                          margin: EdgeInsets.only(bottom: 20),
                          child: new Row(
                            children: [
                              new Flexible(
                                child: new Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 40,
                                    margin: EdgeInsets.only(left: 20, right: 10),
                                    child: ElevatedButton(

                                      style: ElevatedButton.styleFrom(
                                        primary: whiteColor,
                                        elevation: 0.5,
                                        side: BorderSide(width: 1.0,color:Themecolor),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(40),
                                        ),
                                      ),
                                      onPressed: () {
                                        List<Player> selectedWkList = [];
                                        List<Player> selectedBatList = [];
                                        List<Player> selectedArList = [];
                                        List<Player> selectedBowlList = [];
                                        List<Player> selectedCList = [];

                                        for (Player player in wkListTemp) {
                                          if (player.isSelected ?? false) {
                                            selectedWkList.add(player);
                                          }
                                        }

                                        for (Player player in batListTemp) {
                                          if (player.isSelected ?? false) {
                                            selectedBatList.add(player);
                                          }
                                        }

                                        for (Player player in arListTemp) {
                                          if (player.isSelected ?? false) {
                                            selectedArList.add(player);
                                          }
                                        }

                                        for (Player player in bolListTemp) {
                                          if (player.isSelected ?? false) {
                                            selectedBowlList.add(player);
                                          }
                                        }

                                        for (Player player in cListTemp) {
                                          if (player.isSelected ?? false) {
                                            selectedCList.add(player);
                                          }
                                        }

                                        widget.model.selectedWkList = selectedWkList;
                                        widget.model.selectedBatLiSt = selectedBatList;
                                        widget.model.selectedArList = selectedArList;
                                        widget.model.selectedBowlList = selectedBowlList;
                                        widget.model.selectedcList = selectedCList;
                                        widget.model.isEditable = false;

                                        navigateToTeamPreview(context, widget.model);
                                      },
                                      child: Text(
                                        'Team Preview',
                                        style: TextStyle(fontSize: 14, color: Themecolor),
                                      ),
                                    )
                                ),
                                flex: 1,
                              ),
                              new Flexible(
                                child: new Container(

                                    width: MediaQuery.of(context).size.width,
                                    height: 40,
                                    margin: EdgeInsets.only(left: 10, right: 20),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(3),
                                     // border: Border.all(color: Color(0xFF707070))
                                    ),
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        primary: selectedPlayer.selectedPlayer == selectedPlayer.total_player_count
                                            ? Themecolor
                                            : Themecolor,
                                        elevation: 0.5,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(40),
                                        ),
                                      ),
                                      onPressed: () {
                                        if (selectedPlayer.selectedPlayer == selectedPlayer.total_player_count) {
                                          List<Player> selectedList = [];

                                          for (Player player in wkListTemp) {
                                            if (player.isSelected ?? false) selectedList.add(player);
                                          }

                                          for (Player player in batListTemp) {
                                            if (player.isSelected ?? false) selectedList.add(player);
                                          }

                                          for (Player player in arListTemp) {
                                            if (player.isSelected ?? false) selectedList.add(player);
                                          }

                                          for (Player player in bolListTemp) {
                                            if (player.isSelected ?? false) selectedList.add(player);
                                          }

                                          for (Player player in cListTemp) {
                                            if (player.isSelected ?? false) selectedList.add(player);
                                          }

                                          widget.model.selectedList = selectedList;
                                          widget.model.localTeamCount = selectedPlayer.localTeamplayerCount;
                                          widget.model.visitorTeamCount = selectedPlayer.visitorTeamPlayerCount;

                                          navigateToChooseCandVc(context, widget.model, contest: widget.contest);
                                        } else {
                                          showTeamValidation(
                                              "Please select " + selectedPlayer.total_player_count.toString() + " players.");
                                        }
                                      },
                                      child: Text(
                                        'Continue',
                                        style: TextStyle(
                                          fontSize: 14,
                                          color: selectedPlayer.selectedPlayer == selectedPlayer.total_player_count
                                              ? Colors.white
                                              : Colors.white,
                                        ),
                                      ),
                                    )
                                ),
                                flex: 1,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),

                  clearTeamDialog
                      ? new Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.black54,
                          child: AlertDialog(
                            actionsPadding: EdgeInsets.all(20),titlePadding: EdgeInsets.only(top : 20 , left : 20,right : 20),
                            contentPadding: EdgeInsets.only(top : 20 , left : 20,right : 20),
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            title: Text("Confirmation".toUpperCase(),
                                style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold)),
                            content: Text(
                                "Are you sure you want to clear selected team?",
                                style: TextStyle(height: 1.5,color: Colors.grey,fontWeight: (FontWeight.w400))),
                            actions: [
                              cancelButton(context),
                              continueButton(context),
                            ],
                          ),
                        )
                      : new Container(),

                  rulesDialog
                      ? new Container(
                          height: MediaQuery.of(context).size.height,
                          width: MediaQuery.of(context).size.width,
                          color: Colors.black54,
                          child: AlertDialog(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10.0))),
                            title: Text("Selection Rules!",
                                style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold)),
                            content: new Container(
                              // decoration:BoxDecoration(
                              //   border: Border.all(color: Colors.grey.shade400)
                              // ),
                              height: 160,

                              child: new Column(
                                children: [
                                  Text(
                                      widget.model.sportKey==AppConstants.TAG_CRICKET?"Now you can select upto 4 Wicket-Keepers, 5 Batter, 5 All-Rounders & 5 Bowlers.":
                                      widget.model.sportKey==AppConstants.TAG_BASEBALL?'Now you can select upto 5 Outfielders, 5 Infielders, 1 Pitcher & 1 Catcher.':
                                      widget.model.sportKey==AppConstants.TAG_HOCKEY?'Now you can select upto 1 Goal-Keeper, 5 Defender, 5 Mid-Fielder & 3 Forward.':
                                      widget.model.sportKey==AppConstants.TAG_HANDBALL?'Now you can select upto 1 Goal-Keeper, 4 Defender & 4 Forward.':
                                      widget.model.sportKey==AppConstants.TAG_KABADDI?'Now you can select upto 4 Defender, 2 All-Rounders & 3 Raiders.':
                                      widget.model.sportKey==AppConstants.TAG_BASKETBALL?'Now you can select upto 4 Point Guard, 4 Shooting Guard, 4 Small Forward, 4 Power Forward & 4 Center.':
                                      'Now you can select upto 1 Goal-Keeper, 5 Defender, 5 Mid-Fielder & 3 Forward.' ,
                                      style: TextStyle(color: Colors.grey,fontSize: 13)),
                                  SizedBox(height: 5,),
                                  new Container(
                                    decoration:BoxDecoration(
                                        border: Border(
                                          top: BorderSide(color: Colors.grey.shade300),right:  BorderSide(color: Colors.grey.shade300),
                                          left:  BorderSide(color: Colors.grey.shade300),
                                        )
                                    ),
                                    height: 40,
                                    child: new Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              '',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'WK':widget.model.sportKey==AppConstants.TAG_BASEBALL?'OF':widget.model.sportKey==AppConstants.TAG_HOCKEY?'GK':widget.model.sportKey==AppConstants.TAG_HANDBALL?'GK':widget.model.sportKey==AppConstants.TAG_KABADDI?'DEF':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'PG':'GK',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'BAT':widget.model.sportKey==AppConstants.TAG_BASEBALL?'IF':widget.model.sportKey==AppConstants.TAG_HOCKEY?'DEF':widget.model.sportKey==AppConstants.TAG_HANDBALL?'DEF':widget.model.sportKey==AppConstants.TAG_KABADDI?'ALL':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'SG':'DEF',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'AR':widget.model.sportKey==AppConstants.TAG_BASEBALL?'P':widget.model.sportKey==AppConstants.TAG_HOCKEY?'MID':widget.model.sportKey==AppConstants.TAG_HANDBALL?'FWD':widget.model.sportKey==AppConstants.TAG_KABADDI?'RAIDER':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'SF':'MID',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        widget.model.sportKey!=AppConstants.TAG_HANDBALL&&widget.model.sportKey!=AppConstants.TAG_KABADDI?new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'BOWL':widget.model.sportKey==AppConstants.TAG_BASEBALL?'C':widget.model.sportKey==AppConstants.TAG_HOCKEY?'ST':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'PF':'ST',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ):new Flexible(child: new Container(),flex: 0,),
                                        widget.model.sportKey==AppConstants.TAG_BASKETBALL?new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              'C',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ):new Flexible(child: new Container(),flex: 0,),
                                      ],
                                    ),
                                  ),
                                  // new Divider(
                                  //   height: 2,
                                  //   thickness: 1,
                                  // ),
                                  new Container(
                                    decoration:BoxDecoration(
                                        border: Border.all(color: Colors.grey.shade300)
                                    ),
                                    height: 40,
                                    child: new Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              'MIN',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'1':widget.model.sportKey==AppConstants.TAG_BASEBALL?'2':widget.model.sportKey==AppConstants.TAG_HOCKEY?'1':widget.model.sportKey==AppConstants.TAG_HANDBALL?'1':widget.model.sportKey==AppConstants.TAG_KABADDI?'2':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'1':'1',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'1':widget.model.sportKey==AppConstants.TAG_BASEBALL?'2':widget.model.sportKey==AppConstants.TAG_HOCKEY?'3':widget.model.sportKey==AppConstants.TAG_HANDBALL?'2':widget.model.sportKey==AppConstants.TAG_KABADDI?'1':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'1':'3',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'1':widget.model.sportKey==AppConstants.TAG_BASEBALL?'1':widget.model.sportKey==AppConstants.TAG_HOCKEY?'3':widget.model.sportKey==AppConstants.TAG_HANDBALL?'2':widget.model.sportKey==AppConstants.TAG_KABADDI?'1':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'1':'3',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        widget.model.sportKey!=AppConstants.TAG_HANDBALL&&widget.model.sportKey!=AppConstants.TAG_KABADDI?new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'1':widget.model.sportKey==AppConstants.TAG_BASEBALL?'1':widget.model.sportKey==AppConstants.TAG_HOCKEY?'1':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'1':'1',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ):new Flexible(child: new Container(),flex: 0,),
                                        widget.model.sportKey==AppConstants.TAG_BASKETBALL?new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              '1',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ):new Flexible(child: new Container(),flex: 0,),
                                      ],
                                    ),
                                  ),
                                  // new Divider(
                                  //   height: 2,
                                  //   thickness: 1,
                                  // ),
                                  new Container(
                                    decoration:BoxDecoration(
                                        border: Border(
                                          bottom: BorderSide(color: Colors.grey.shade300),right:  BorderSide(color: Colors.grey.shade300),
                                          left:  BorderSide(color: Colors.grey.shade300),
                                        )
                                    ),
                                    height: 40,
                                    child: new Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              'MAX',
                                              style: TextStyle(
                                                  color: Colors.black,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'4':widget.model.sportKey==AppConstants.TAG_BASEBALL?'5':widget.model.sportKey==AppConstants.TAG_HOCKEY?'1':widget.model.sportKey==AppConstants.TAG_HANDBALL?'1':widget.model.sportKey==AppConstants.TAG_KABADDI?'4':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'4':'1',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'5':widget.model.sportKey==AppConstants.TAG_BASEBALL?'5':widget.model.sportKey==AppConstants.TAG_HOCKEY?'5':widget.model.sportKey==AppConstants.TAG_HANDBALL?'4':widget.model.sportKey==AppConstants.TAG_KABADDI?'2':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'4':'5',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'5':widget.model.sportKey==AppConstants.TAG_BASEBALL?'1':widget.model.sportKey==AppConstants.TAG_HOCKEY?'5':widget.model.sportKey==AppConstants.TAG_HANDBALL?'4':widget.model.sportKey==AppConstants.TAG_KABADDI?'3':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'4':'5',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ),
                                        widget.model.sportKey!=AppConstants.TAG_HANDBALL&&widget.model.sportKey!=AppConstants.TAG_KABADDI?new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              widget.model.sportKey==AppConstants.TAG_CRICKET?'5':widget.model.sportKey==AppConstants.TAG_BASEBALL?'1':widget.model.sportKey==AppConstants.TAG_HOCKEY?'3':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'4':'3',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ):new Flexible(child: new Container(),flex: 0,),
                                        widget.model.sportKey==AppConstants.TAG_BASKETBALL?new Flexible(
                                          child: new Container(
                                            alignment:
                                            Alignment.center,
                                            child: Text(
                                              '4',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight:
                                                  FontWeight.w400,
                                                  fontSize: 13),
                                            ),
                                          ),
                                          flex: 1,
                                        ):new Flexible(child: new Container(),flex: 0,),
                                      ],
                                    ),
                                  ),
                                ],
                              ),

                            ),
                            actions: [
                              gotItButton(context)
                            ],
                              actionsAlignment: MainAxisAlignment.start,
                          ),
                        )
                      : new Container(),
                ],
              ),
              onWillPop: _onWillPop)),
    );
  }

  Widget cancelButton(context) {
    return ElevatedButton(
      onPressed: () {
        setState(() {
          clearTeamDialog = false;
        });
      },
      style: ElevatedButton.styleFrom(
        primary: Colors.white,
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Color(0xFF8e9193),
            width: 1,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(40),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Text(
          "CANCEL",
          style: TextStyle(color: Colors.grey),
        ),
      ),
    );


    //   OutlinedButton(
    //   onPressed: () {
    //     setState(() {
    //       clearTeamDialog = false;
    //     });
    //   },
    //   style: OutlinedButton.styleFrom(
    //     side: BorderSide(
    //       color: Color(0xFF8e9193),
    //       width: 1,
    //     ),
    //     shape: RoundedRectangleBorder(
    //       borderRadius: BorderRadius.circular(40),
    //     ),
    //   ),
    //   child: Text(
    //     "CANCEL",
    //     style: TextStyle(color: Colors.grey),
    //   ),
    // );

  }

  Widget continueButton(context) {
    return ElevatedButton(
      onPressed: () {
        for (int i = 0; i < allPlayerList.length; i++) {
          allPlayerList[i].isSelected = false;
        }
        createTeamData();
        setState(() {
          clearTeamDialog = false;
        });
      },
      style: ElevatedButton.styleFrom(
        primary: Themecolor,
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: Themecolor,
            width: 1,
            style: BorderStyle.solid,
          ),
          borderRadius: BorderRadius.circular(40),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Text(
          "CLEAR TEAM",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget gotItButton(context) {
    return Container(
      margin: EdgeInsets.only(left: 19),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          primary: Themecolor,
          elevation: 0.5,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40),
          ),
        ),
        onPressed: () {
          setState(() {
            rulesDialog = false;
          });
        },
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            "GOT IT",
            style: TextStyle(
              color: Colors.white,
              fontSize: 14,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: primaryColor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.dark) /* set Status bar icon color in iOS. */
        );
    widget.model.contest=null;
    Navigator.pop(context);
    return Future.value(false);
  }

  List<Widget> getTabs() {
    tabs.clear();
    tabs.add(getWKTab());
    tabs.add(getBATTab());
    tabs.add(getARTab());
    if(widget.model.sportKey!=AppConstants.TAG_HANDBALL&&widget.model.sportKey!=AppConstants.TAG_KABADDI){
      tabs.add(getBOWLTab());
    }
    if(widget.model.sportKey==AppConstants.TAG_BASKETBALL){
      tabs.add(getCTab());
    }
    return tabs;
  }
  // List<Widget> getTabsView() {
  //   tabsView.clear();
  //   tabsView.add(new CreateTeamPager(wkList,onPlayerClick,1,selectedPlayer,limit,widget.model.fantasyType!,exeedCredit));
  //   tabsView.add(new CreateTeamPager(batList,onPlayerClick,2,selectedPlayer,limit,widget.model.fantasyType!,exeedCredit));
  //   tabsView.add(new CreateTeamPager(arList,onPlayerClick,3,selectedPlayer,limit,widget.model.fantasyType!,exeedCredit));
  //   if(widget.model.sportKey!=AppConstants.TAG_HANDBALL&&widget.model.sportKey!=AppConstants.TAG_KABADDI){
  //     tabsView.add(new CreateTeamPager(bolList,onPlayerClick,4,selectedPlayer,limit,widget.model.fantasyType!,exeedCredit));
  //   }
  //   if(widget.model.sportKey==AppConstants.TAG_BASKETBALL){
  //     tabsView.add(new CreateTeamPager(cList,onPlayerClick,5,selectedPlayer,limit,widget.model.fantasyType!,exeedCredit));
  //   }
  //   return tabsView;
  // }

  Widget getWKTab() {
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Text(
          selectedPlayer.wk_selected==0?widget.model.sportKey==AppConstants.TAG_CRICKET?'WK':widget.model.sportKey==AppConstants.TAG_BASEBALL?'OF':widget.model.sportKey==AppConstants.TAG_HOCKEY?'GK':widget.model.sportKey==AppConstants.TAG_HANDBALL?'GK':widget.model.sportKey==AppConstants.TAG_KABADDI?'DEF':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'PG':'GK':(widget.model.sportKey==AppConstants.TAG_CRICKET?'WK(':widget.model.sportKey==AppConstants.TAG_BASEBALL?'OF(':widget.model.sportKey==AppConstants.TAG_HOCKEY?'GK(':widget.model.sportKey==AppConstants.TAG_HANDBALL?'GK(':widget.model.sportKey==AppConstants.TAG_KABADDI?'DEF(':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'PG(':'GK(')+selectedPlayer.wk_selected.toString()+')',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 0 ? primaryColor : Colors.grey,
              fontWeight: FontWeight.w500,
              fontSize: 14),
        ),
      ),
    );
  }

  Widget getBATTab() {
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Text(
          selectedPlayer.bat_selected==0?widget.model.sportKey==AppConstants.TAG_CRICKET?'BAT':widget.model.sportKey==AppConstants.TAG_BASEBALL?'IF':widget.model.sportKey==AppConstants.TAG_HOCKEY?'DEF':widget.model.sportKey==AppConstants.TAG_HANDBALL?'DEF':widget.model.sportKey==AppConstants.TAG_KABADDI?'ALL':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'SG':'DEF':(widget.model.sportKey==AppConstants.TAG_CRICKET?'BAT(':widget.model.sportKey==AppConstants.TAG_BASEBALL?'IF(':widget.model.sportKey==AppConstants.TAG_HOCKEY?'DEF(':widget.model.sportKey==AppConstants.TAG_HANDBALL?'DEF(':widget.model.sportKey==AppConstants.TAG_KABADDI?'ALL(':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'SG(':'DEF(')+selectedPlayer.bat_selected.toString()+')',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 1 ?primaryColor : Colors.grey,
              fontWeight: FontWeight.w500,
              fontSize: 14),
        ),
      ),
    );
  }

  Widget getARTab() {
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Text(
          selectedPlayer.ar_selected==0?widget.model.sportKey==AppConstants.TAG_CRICKET?'AR':widget.model.sportKey==AppConstants.TAG_BASEBALL?'P':widget.model.sportKey==AppConstants.TAG_HOCKEY?'MID':widget.model.sportKey==AppConstants.TAG_HANDBALL?'FWD':widget.model.sportKey==AppConstants.TAG_KABADDI?'RAIDERS':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'SF':'MID':(widget.model.sportKey==AppConstants.TAG_CRICKET?'AR(':widget.model.sportKey==AppConstants.TAG_BASEBALL?'P(':widget.model.sportKey==AppConstants.TAG_HOCKEY?'MID(':widget.model.sportKey==AppConstants.TAG_HANDBALL?'FWD(':widget.model.sportKey==AppConstants.TAG_KABADDI?'RAIDERS(':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'SF(':'MID(')+selectedPlayer.ar_selected.toString()+')',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 2 ? primaryColor : Colors.grey,
              fontWeight: FontWeight.w500,
              fontSize: 14),
        ),
      ),
    );
  }

  Widget getBOWLTab() {
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Text(
          selectedPlayer.bowl_selected==0?widget.model.sportKey==AppConstants.TAG_CRICKET?'BOWL':widget.model.sportKey==AppConstants.TAG_BASEBALL?'C':widget.model.sportKey==AppConstants.TAG_HOCKEY?'ST':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'PF':'ST':(widget.model.sportKey==AppConstants.TAG_CRICKET?'BOWL(':widget.model.sportKey==AppConstants.TAG_BASEBALL?'C(':widget.model.sportKey==AppConstants.TAG_HOCKEY?'ST(':widget.model.sportKey==AppConstants.TAG_BASKETBALL?'PF(':'ST(')+selectedPlayer.bowl_selected.toString()+')',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 3 ? primaryColor : Colors.grey,
              fontWeight: FontWeight.w500,
              fontSize: 14),
        ),
      ),
    );
  }
  Widget getCTab() {
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Text(
          selectedPlayer.c_selected==0?'C':'C('+selectedPlayer.c_selected.toString()+')',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 3 ? primaryColor : Colors.grey,
              fontWeight: FontWeight.w500,
              fontSize: 14),
        ),
      ),
    );
  }
  void refresh(){
    setState(() {});
  }
  void _modalBottomSheetMenu() {
    showModalBottomSheet(
        context: context,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(0), topRight: Radius.circular(0))),
        builder: (builder) {
          return StatefulBuilder(builder: (context,setState)=>Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(left: 10, right: 10),
                color: mediumGrayColor,
                child: new Row(
                  children: [
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: new Container(
                        height: 40,
                        alignment: Alignment.center,
                        child: new Container(
                          child: Icon(
                            Icons.clear,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      onTap: () => {
                        Navigator.pop(context)
                      },
                    ),
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: new Container(
                        width: MediaQuery.of(context).size.width - 50,
                        alignment: Alignment.center,
                        child: new Text('Filter by Teams',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 16)),
                      ),
                      onTap: () => {},
                    ),
                  ],
                ),
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: new Container(
                  padding: EdgeInsets.only(left: 10,right: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Container(
                        alignment: Alignment.center,
                        child: new Text(widget.model.teamVs!.split(' VS ')[0],
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 14)),
                      ),
                      Radio(
                        value: 'Team1',
                        groupValue:filterType,
                        activeColor: primaryColor,
                        onChanged: (value){
                          team1Click(context);
                        },
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  team1Click(context);
                },
              ),
              new Divider(
                height: 2,
                thickness: 1,
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: new Container(
                  padding: EdgeInsets.only(left: 10,right: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Container(
                        alignment: Alignment.center,
                        child: new Text(widget.model.teamVs!.split(' VS ')[1],
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 14)),
                      ),
                      Radio(
                        value: 'Team2',
                        groupValue: filterType,
                        activeColor: primaryColor,
                        onChanged: (value){
                          team2Click(context);
                        },
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  team2Click(context);

                },
              ),
              new Divider(
                height: 2,
                thickness: 1,
              ),
              GestureDetector(
                behavior: HitTestBehavior.translucent,
                  child: new Container(
                    padding: EdgeInsets.only(left: 10,right: 10),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        new Container(
                          alignment: Alignment.center,
                          child: new Text('ALL',
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14)),
                        ),
                        Radio(
                          value: 'All',
                          groupValue: filterType,
                          activeColor: primaryColor,
                          onChanged: (value){
                            allClick(context);
                          },
                        ),
                      ],
                    ),
                  ),
                onTap: (){
                  allClick(context);
                },
              ),
            ],
          ));
        });
  }
  void team1Click(BuildContext context){
    setState(() {
      filterType='Team1';
      wkList = team1wkList;
      batList = team1batList;
      arList = team1arList;
      bolList = team1bolList;
      cList = team1cList;
      Navigator.pop(context);
      refresh();
    });
  }
  void team2Click(BuildContext context){
    setState(() {
      filterType='Team2';
      wkList = team2wkList;
      batList = team2batList;
      arList = team2arList;
      bolList = team2bolList;
      cList = team2cList;
      Navigator.pop(context);
      refresh();
    });
  }
  void allClick(BuildContext context){
    setState(() {
      filterType='All';
      wkList = wkListTemp;
      batList = batListTemp;
      arList = arListTemp;
      bolList = bolListTemp;
      cList = cListTemp;
      Navigator.pop(context);
      refresh();
    });
  }
  void onPlayerClick(bool isSelect, int position, int type){
    if (!fantasyAlertShown && isSelect && ((type == BOWLER && widget.model.fantasyType == AppConstants.BATTING_FANTASY_TYPE) || ((type == BAT || type == WK) && widget.model.fantasyType == AppConstants.BOWLING_FANTASY_TYPE))) {
      String text='';
      if (type == BOWLER && widget.model.fantasyType == AppConstants.BATTING_FANTASY_TYPE) {
        text="This is batting fantasy! You want to choose a bowler?";
      } else if (type == BAT || type == WK && widget.model.fantasyType == AppConstants.BOWLING_FANTASY_TYPE) {
        if (type == BAT) {
          text="This is bowling fantasy! You want to choose a batter?";
        } else if (type == WK) {
          text="This is bowling fantasy! You want to choose a wicket-keeper?";
        }
      }
      showDialog(barrierDismissible: false,context: context, builder: (BuildContext context){
        return new Dialog(
          backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(5.0))),
          child: new Container(
            height: 150,
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                new Container(
                  alignment: Alignment.center,
                  child: new Text('Alert',
                      style: TextStyle(
                          fontFamily: 'roboto',
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 16)),
                ),
                new Container(
                  alignment: Alignment.center,
                  child: new Text(text,
                      style: TextStyle(
                          fontFamily: 'roboto',
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          height: 1.5),
                      textAlign: TextAlign.center),
                ),
                new Container(
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                          elevation: 0.5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            fantasyAlertShown = false;
                            Navigator.pop(context);
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "NO",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(width: 10),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                          elevation: 0.5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5),
                          ),
                        ),
                        onPressed: () {
                          setState(() {
                            fantasyAlertShown = true;
                            Navigator.pop(context);
                            continuePlayerSelectionProcess(isSelect, position, type);
                          });
                        },
                        child: Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            "YES",
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 14,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      });
    }else{
      continuePlayerSelectionProcess(isSelect, position, type);
    }
  }

  void continuePlayerSelectionProcess(bool isSelect, int position, int type) {


    if (type == WK) {
      double player_credit;

      if (isSelect) {
        if (selectedPlayer.selectedPlayer >= selectedPlayer.total_player_count) {
          showTeamValidation("You can choose maximum " + selectedPlayer.total_player_count.toString() + " players.");

          return;
        }
        if (selectedPlayer.wk_selected > selectedPlayer.wk_max_count) {
          showTeamValidation("You can select only " + selectedPlayer.wk_max_count.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' Wicket-Keeper':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Outfielders':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Goal-Keeper':widget.model.sportKey==AppConstants.TAG_HANDBALL?' Goal-Keeper':widget.model.sportKey==AppConstants.TAG_KABADDI?' Defender':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Point-Guard':' Goal-Keeper')+'.');
          return;
        }

        if (wkList[position].team=="team1") {
          if (selectedPlayer.localTeamplayerCount >= limit.team_max_player!) {
            showTeamValidation("You can select only " + limit.team_max_player!.toString() + " from each team.");
            return;
          }
        } else {
          if (selectedPlayer.visitorTeamPlayerCount >= limit.team_max_player!) {
            showTeamValidation("You can select only " + limit.team_max_player!.toString() + " from each team.");
            return;
          }
        }

        if (selectedPlayer.wk_selected < selectedPlayer.wk_max_count) {
          if (selectedPlayer.selectedPlayer < selectedPlayer.total_player_count) {
            if (selectedPlayer.wk_selected < selectedPlayer.wk_min_count || selectedPlayer.extra_player > 0) {

              int extra = selectedPlayer.extra_player;
              if (selectedPlayer.wk_selected >= selectedPlayer.wk_min_count) {
                extra = selectedPlayer.extra_player - 1;
              }

              player_credit = double.parse(wkList[position].credit);


              double total_credit = selectedPlayer.total_credit + player_credit;
              if (total_credit > limit.total_credits!) {
                exeedCredit = true;
                showTeamValidation("Not enough credits to select this player.");
                return;
              }
              int localTeamplayerCount = selectedPlayer.localTeamplayerCount;
              int visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount;

              if (wkList[position].team=="team1")
                localTeamplayerCount = selectedPlayer.localTeamplayerCount + 1;
              else
                visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount + 1;

              wkList[position].isSelected=isSelect;

              updateTeamData(
                  extra,
                  selectedPlayer.wk_selected + 1,
                  selectedPlayer.bat_selected,
                  selectedPlayer.ar_selected,
                  selectedPlayer.bowl_selected,
                  selectedPlayer.c_selected,
                  selectedPlayer.selectedPlayer + 1,
                  localTeamplayerCount,
                  visitorTeamPlayerCount,
                  total_credit
              );

            } else {
              minimumPlayerWarning();
            }
          }
        }
      } else {

        if (selectedPlayer.wk_selected > 0) {
          showTeamValidation("Pick " + selectedPlayer.wk_min_count.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' Wicket-Keeper':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Outfielders':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Goal-Keeper':widget.model.sportKey==AppConstants.TAG_HANDBALL?' Goal-Keeper':widget.model.sportKey==AppConstants.TAG_KABADDI?' Defender':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Point-Guard':' Goal-Keeper')+'.');
          player_credit = double.parse(wkList[position].credit);

          double total_credit = selectedPlayer.total_credit - player_credit;

          int extra = selectedPlayer.extra_player;
          if (selectedPlayer.wk_selected > selectedPlayer.wk_min_count) {
            extra = selectedPlayer.extra_player + 1;
          }
          int localTeamplayerCount = selectedPlayer.localTeamplayerCount;
          int visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount;

          if (wkList[position].team=="team1")
            localTeamplayerCount = selectedPlayer.localTeamplayerCount - 1;
          else
            visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount - 1;

          wkList[position].isSelected=isSelect;

          updateTeamData(
              extra,
              selectedPlayer.wk_selected - 1,
              selectedPlayer.bat_selected,
              selectedPlayer.ar_selected,
              selectedPlayer.bowl_selected,
              selectedPlayer.c_selected,
              selectedPlayer.selectedPlayer - 1,
              localTeamplayerCount,
              visitorTeamPlayerCount,
              total_credit
          );
        }
      }
    } else if (type == AR) {
      double player_credit;
      if (isSelect) {
        if (selectedPlayer.selectedPlayer >= selectedPlayer.total_player_count) {
          showTeamValidation("You can choose maximum " + selectedPlayer.total_player_count.toString() + " players.");

          return;
        }

        if (selectedPlayer.ar_selected > selectedPlayer.ar_maxcount) {
          showTeamValidation("You can select only " + selectedPlayer.ar_maxcount.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' All-Rounders':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Infielders':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Midfielder':widget.model.sportKey==AppConstants.TAG_HANDBALL?' State-Forward':widget.model.sportKey==AppConstants.TAG_KABADDI?' Raider':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Small-Forward':' Midfielder')+'.');
          return;
        }

        if (arList[position].team=="team1") {
          if (selectedPlayer.localTeamplayerCount >= limit.team_max_player!) {
            showTeamValidation("You can select only " +limit.team_max_player!.toString() + " from each team.");
            return;
          }
        } else {
          if (selectedPlayer.visitorTeamPlayerCount >= limit.team_max_player!) {
            showTeamValidation("You can select only " + limit.team_max_player!.toString() + " from each team.");
            return;
          }
        }


        if (selectedPlayer.ar_selected < selectedPlayer.ar_maxcount) {
          if (selectedPlayer.selectedPlayer < selectedPlayer.total_player_count) {
            if (selectedPlayer.ar_selected < selectedPlayer.ar_mincount || selectedPlayer.extra_player > 0) {

              int extra = selectedPlayer.extra_player;
              if (selectedPlayer.ar_selected >= selectedPlayer.ar_mincount) {
                extra = selectedPlayer.extra_player - 1;
              }

              player_credit = double.parse(arList[position].credit);

              double total_credit = selectedPlayer.total_credit + player_credit;
              if (total_credit > limit.total_credits!) {
                exeedCredit = true;
                showTeamValidation("Not enough credits to select this player.");
                return;
              }
              int localTeamplayerCount = selectedPlayer.localTeamplayerCount;
              int visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount;

              if (arList[position].team=="team1")
                localTeamplayerCount = selectedPlayer.localTeamplayerCount + 1;
              else
                visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount + 1;

              arList[position].isSelected=isSelect;
              updateTeamData(
                  extra,
                  selectedPlayer.wk_selected,
                  selectedPlayer.bat_selected,
                  selectedPlayer.ar_selected + 1,
                  selectedPlayer.bowl_selected,
                  selectedPlayer.c_selected,
                  selectedPlayer.selectedPlayer + 1,
                  localTeamplayerCount,
                  visitorTeamPlayerCount,
                  total_credit
              );
            } else {
              minimumPlayerWarning();
            }
          }
        }
      } else {
        if (selectedPlayer.ar_selected > 0) {
          showTeamValidation("Pick " + selectedPlayer.ar_mincount.toString() + "-" + selectedPlayer.ar_maxcount.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' All-Rounders':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Pitcher':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Midfielder':widget.model.sportKey==AppConstants.TAG_HANDBALL?' State-Forward':widget.model.sportKey==AppConstants.TAG_KABADDI?' Raider':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Small-Forward':' Midfielder')+'.');
          player_credit = double.parse(arList[position].credit);

          double total_credit = selectedPlayer.total_credit - player_credit;


          int extra = selectedPlayer.extra_player;
          if (selectedPlayer.ar_selected > selectedPlayer.ar_mincount) {
            extra = selectedPlayer.extra_player + 1;
          }
          int localTeamplayerCount = selectedPlayer.localTeamplayerCount;
          int visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount;

          if (arList[position].team=="team1")
            localTeamplayerCount = selectedPlayer.localTeamplayerCount - 1;
          else
            visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount - 1;

          arList[position].isSelected=isSelect;

          updateTeamData(
              extra,
              selectedPlayer.wk_selected,
              selectedPlayer.bat_selected,
              selectedPlayer.ar_selected - 1,
              selectedPlayer.bowl_selected,
              selectedPlayer.c_selected,
              selectedPlayer.selectedPlayer - 1,
              localTeamplayerCount,
              visitorTeamPlayerCount,
              total_credit
          );
        }
      }
    } else if (type == BAT) {
      double player_credit;


      if (isSelect) {

        if (selectedPlayer.selectedPlayer >= selectedPlayer.total_player_count) {
          showTeamValidation("You can choose maximum " + selectedPlayer.total_player_count.toString() + " players");
          return;
        }

        if (selectedPlayer.bat_selected >= selectedPlayer.bat_maxcount) {
          showTeamValidation("You can select only " + selectedPlayer.bat_maxcount.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' Batter':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Pitcher':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Defender':widget.model.sportKey==AppConstants.TAG_HANDBALL?' Defender':widget.model.sportKey==AppConstants.TAG_KABADDI?' All-Rounder':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Shooting-Guard':' Defender')+'.');
          return;
        }

        if (batList[position].team=="team1") {
          if (selectedPlayer.localTeamplayerCount >= limit.team_max_player!) {
            showTeamValidation("You can select only " + limit.team_max_player!.toString() + " from each team.");
            return;
          }
        } else {
          if (selectedPlayer.visitorTeamPlayerCount >= limit.team_max_player!) {
            showTeamValidation("You can select only " + limit.team_max_player!.toString() + " from each team.");
            return;
          }
        }


        if (selectedPlayer.bat_selected < selectedPlayer.bat_maxcount) {
          if (selectedPlayer.selectedPlayer < selectedPlayer.total_player_count) {
            if (selectedPlayer.bat_selected < selectedPlayer.bat_mincount || selectedPlayer.extra_player > 0) {

              int extra = selectedPlayer.extra_player;
              if (selectedPlayer.bat_selected >= selectedPlayer.bat_mincount) {
                extra = selectedPlayer.extra_player - 1;
              }

              player_credit = double.parse(batList[position].credit);

              double total_credit = selectedPlayer.total_credit + player_credit;
              if (total_credit > limit.total_credits!) {
                exeedCredit = true;
                showTeamValidation("Not enough credits to select this player.");

                return;
              }
              int localTeamplayerCount = selectedPlayer.localTeamplayerCount;
              int visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount;

              if (batList[position].team=="team1")
                localTeamplayerCount = selectedPlayer.localTeamplayerCount + 1;
              else
                visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount + 1;

              batList[position].isSelected=isSelect;

              updateTeamData(
                  extra,
                  selectedPlayer.wk_selected,
                  selectedPlayer.bat_selected + 1,
                  selectedPlayer.ar_selected,
                  selectedPlayer.bowl_selected,
                  selectedPlayer.c_selected,
                  selectedPlayer.selectedPlayer + 1,
                  localTeamplayerCount,
                  visitorTeamPlayerCount,
                  total_credit
              );
            } else {
              minimumPlayerWarning();
            }
          }
        }
      } else {
        if (selectedPlayer.bat_selected > 0) {
          showTeamValidation("Pick " + selectedPlayer.bat_mincount.toString() + "-" + selectedPlayer.bat_maxcount.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' Batter':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Infielders':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Defender':widget.model.sportKey==AppConstants.TAG_HANDBALL?' Defender':widget.model.sportKey==AppConstants.TAG_KABADDI?' All-Rounder':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Shooting-Guard':' Defender')+'.');
          player_credit =double.parse(batList[position].credit);

          double total_credit = selectedPlayer.total_credit - player_credit;

          int extra = selectedPlayer.extra_player;
          if (selectedPlayer.bat_selected > selectedPlayer.bat_mincount) {
            extra = selectedPlayer.extra_player + 1;
          }
          int localTeamplayerCount = selectedPlayer.localTeamplayerCount;
          int visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount;

          if (batList[position].team=="team1")
            localTeamplayerCount = selectedPlayer.localTeamplayerCount - 1;
          else
            visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount - 1;

          batList[position].isSelected=isSelect;
          updateTeamData(
              extra,
              selectedPlayer.wk_selected,
              selectedPlayer.bat_selected - 1,
              selectedPlayer.ar_selected,
              selectedPlayer.bowl_selected,
              selectedPlayer.c_selected,
              selectedPlayer.selectedPlayer - 1,
              localTeamplayerCount,
              visitorTeamPlayerCount,
              total_credit
          );
        }
      }
    } else if (type == BOWLER) {
      double player_credit = 0.0;

      if (isSelect) {

        if (selectedPlayer.selectedPlayer >= selectedPlayer.total_player_count) {
          showTeamValidation("You can choose maximum " + selectedPlayer.total_player_count.toString() + " players.");

          return;
        }

        if (selectedPlayer.bowl_selected >= selectedPlayer.bowl_maxcount) {
          showTeamValidation("You can select only " + selectedPlayer.bowl_maxcount.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' Bowlers':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Catcher':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Striker':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Power-Forward':' State-Forward')+'.');
          return;
        }

        if (bolList[position].team=="team1") {
          if (selectedPlayer.localTeamplayerCount >= limit.team_max_player!) {
            showTeamValidation("You can select only " + limit.team_max_player.toString() + " from each team.");
            return;
          }
        } else {
          if (selectedPlayer.visitorTeamPlayerCount >= limit.team_max_player!) {
            showTeamValidation("You can select only " + limit.team_max_player.toString() + " from each team.");
            return;
          }
        }


        if (selectedPlayer.bowl_selected < selectedPlayer.bowl_maxcount) {
          if (selectedPlayer.selectedPlayer < selectedPlayer.total_player_count) {
            if (selectedPlayer.bowl_selected < selectedPlayer.bowl_mincount || selectedPlayer.extra_player > 0) {

              int extra = selectedPlayer.extra_player;
              if (selectedPlayer.bowl_selected >= selectedPlayer.bowl_mincount) {
                extra = selectedPlayer.extra_player - 1;
              }

              player_credit = double.parse(bolList[position].credit);

              double total_credit = selectedPlayer.total_credit + player_credit;
              if (total_credit > limit.total_credits!) {
                exeedCredit = true;
                showTeamValidation("Not enough credits to select this player.");

                return;
              }
              int localTeamplayerCount = selectedPlayer.localTeamplayerCount;
              int visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount;

              if (bolList[position].team=="team1")
                localTeamplayerCount = selectedPlayer.localTeamplayerCount + 1;
              else
                visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount + 1;

              bolList[position].isSelected=isSelect;

              updateTeamData(
                  extra,
                  selectedPlayer.wk_selected,
                  selectedPlayer.bat_selected,
                  selectedPlayer.ar_selected,
                  selectedPlayer.bowl_selected + 1,
                  selectedPlayer.c_selected,
                  selectedPlayer.selectedPlayer + 1,
                  localTeamplayerCount,
                  visitorTeamPlayerCount,
                  total_credit
              );
            } else {
              minimumPlayerWarning();
            }
          }
        }
      } else {

        if (selectedPlayer.bowl_selected > 0) {
          showTeamValidation("Pick " + selectedPlayer.bowl_mincount.toString() + "-" + selectedPlayer.bowl_maxcount.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' Bowlers':widget.model.sportKey==AppConstants.TAG_BASEBALL?' Catcher':widget.model.sportKey==AppConstants.TAG_HOCKEY?' Striker':widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Power-Forward':' State-Forward')+'.');
          player_credit = double.parse(bolList[position].credit);

          double total_credit = selectedPlayer.total_credit- player_credit;


          int extra = selectedPlayer.extra_player;
          if (selectedPlayer.bowl_selected > selectedPlayer.bowl_mincount) {
            extra = selectedPlayer.extra_player + 1;
          }
          int localTeamplayerCount = selectedPlayer.localTeamplayerCount;
          int visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount;

          if (bolList[position].team=="team1")
            localTeamplayerCount = selectedPlayer.localTeamplayerCount - 1;
          else
            visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount - 1;

          bolList[position].isSelected=isSelect;
          updateTeamData(
              extra,
              selectedPlayer.wk_selected,
              selectedPlayer.bat_selected,
              selectedPlayer.ar_selected,
              selectedPlayer.bowl_selected - 1,
              selectedPlayer.c_selected,
              selectedPlayer.selectedPlayer- 1,
              localTeamplayerCount,
              visitorTeamPlayerCount,
              total_credit
          );
        }
      }
    } else if (type == C) {
      double player_credit = 0.0;

      if (isSelect) {

        if (selectedPlayer.selectedPlayer >= selectedPlayer.total_player_count) {
          showTeamValidation("You can choose maximum " + selectedPlayer.total_player_count.toString() + " players.");
          return;
        }

        if (selectedPlayer.c_selected >= 4) {
          showTeamValidation("You can select only 4 Centre");
          return;
        }


        if (cList[position].team=="team1") {
          if (selectedPlayer.localTeamplayerCount >= limit.team_max_player!) {
            showTeamValidation("You can select only " + limit.team_max_player.toString() + " from each team.");
            return;
          }
        } else {
          if (selectedPlayer.visitorTeamPlayerCount >= limit.team_max_player!) {
            showTeamValidation("You can select only " + limit.team_max_player.toString() + " from each team.");
            return;
          }
        }


        if (selectedPlayer.c_selected < selectedPlayer.c_max_count) {
          if (selectedPlayer.selectedPlayer < selectedPlayer.total_player_count) {
            if (selectedPlayer.c_selected < selectedPlayer.c_max_count || selectedPlayer.extra_player > 0) {

              int extra = selectedPlayer.extra_player;
              if (selectedPlayer.c_selected >= selectedPlayer.c_min_count) {
                extra = selectedPlayer.extra_player - 1;
              }

              player_credit = double.parse(cList[position].credit);

              double total_credit = selectedPlayer.total_credit + player_credit;
              if (total_credit > limit.total_credits!) {
                exeedCredit = true;
                showTeamValidation("Not enough credits to select this player.");
                return;
              }
              int localTeamplayerCount = selectedPlayer.localTeamplayerCount;
              int visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount;

              if (cList[position].team=="team1")
                localTeamplayerCount = selectedPlayer.localTeamplayerCount + 1;
              else
                visitorTeamPlayerCount = selectedPlayer.localTeamplayerCount + 1;

              cList[position].isSelected=isSelect;

              updateTeamData(
                  extra,
                  selectedPlayer.wk_selected,
                  selectedPlayer.bat_selected,
                  selectedPlayer.ar_selected,
                  selectedPlayer.bowl_selected,
                  selectedPlayer.c_selected + 1,
                  selectedPlayer.selectedPlayer + 1,
                  localTeamplayerCount,
                  visitorTeamPlayerCount,
                  total_credit
              );
            } else {
              minimumPlayerWarning();
            }
          }
        }
      } else {

        if (selectedPlayer.c_selected > 0) {
          //     showTeamValidation("Pick 1-3 Forward");
          player_credit = double.parse(cList[position].credit);

          double total_credit = selectedPlayer.total_credit - player_credit;


          int extra = selectedPlayer.extra_player;
          if (selectedPlayer.c_selected > selectedPlayer.c_min_count) {
            extra = selectedPlayer.extra_player + 1;
          }
          int localTeamplayerCount = selectedPlayer.localTeamplayerCount;
          int visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount;

          if (cList[position].team=="team1")
            localTeamplayerCount = selectedPlayer.localTeamplayerCount - 1;
          else
            visitorTeamPlayerCount = selectedPlayer.visitorTeamPlayerCount - 1;

          cList[position].isSelected=isSelect;
          updateTeamData(
              extra,
              selectedPlayer.wk_selected,
              selectedPlayer.bat_selected,
              selectedPlayer.ar_selected,
              selectedPlayer.bowl_selected,
              selectedPlayer.c_selected - 1,
              selectedPlayer.selectedPlayer - 1,
              localTeamplayerCount,
              visitorTeamPlayerCount,
              total_credit
          );
        }
      }
    }
  }

  void createTeamData() {

    if (widget.model.battingFantasy == AppConstants.BATTING_FANTASY_TYPE || widget.model.battingFantasy == AppConstants.BOWLING_FANTASY_TYPE || (widget.model.battingFantasy == AppConstants.LIVE_FANTASY_TYPE && limit.total_credits == 45)) {
      selectedPlayer.extra_player=4;
      selectedPlayer.wk_min_count=1;
      selectedPlayer.wk_max_count=5;
      selectedPlayer.wk_selected=0;
      selectedPlayer.bat_mincount=1;
      selectedPlayer.bat_maxcount=5;
      selectedPlayer.bat_selected=0;
      selectedPlayer.bowl_mincount=1;
      selectedPlayer.bowl_maxcount=5;
      selectedPlayer.bowl_selected=0;
      selectedPlayer.ar_mincount=1;
      selectedPlayer.ar_maxcount=5;
      selectedPlayer.ar_selected=0;
      selectedPlayer.selectedPlayer=0;
      selectedPlayer.total_player_count=5;
      if (widget.model.fantasyType == AppConstants.LIVE_FANTASY_TYPE) {
        selectedPlayer.localTeamMaxplayerCount=5;
        selectedPlayer.visitorTeamMaxplayerCount=5;
      } else {
        selectedPlayer.localTeamMaxplayerCount=3;
        selectedPlayer.visitorTeamMaxplayerCount=3;
      }
      selectedPlayer.localTeamplayerCount=0;
      selectedPlayer.visitorTeamPlayerCount=0;
      selectedPlayer.total_credit=0.0;
    } else {
      selectedPlayer.extra_player=4;
      selectedPlayer.wk_min_count=widget.model.sportKey==AppConstants.TAG_CRICKET?1:widget.model.sportKey==AppConstants.TAG_BASEBALL?2:widget.model.sportKey==AppConstants.TAG_HOCKEY?1:widget.model.sportKey==AppConstants.TAG_HANDBALL?1:widget.model.sportKey==AppConstants.TAG_KABADDI?2:widget.model.sportKey==AppConstants.TAG_BASKETBALL?1:1;
      selectedPlayer.wk_max_count=widget.model.sportKey==AppConstants.TAG_CRICKET?4:widget.model.sportKey==AppConstants.TAG_BASEBALL?5:widget.model.sportKey==AppConstants.TAG_HOCKEY?1:widget.model.sportKey==AppConstants.TAG_HANDBALL?1:widget.model.sportKey==AppConstants.TAG_KABADDI?4:widget.model.sportKey==AppConstants.TAG_BASKETBALL?4:1;
      selectedPlayer.wk_selected=0;
      selectedPlayer.bat_mincount=widget.model.sportKey==AppConstants.TAG_CRICKET?1:widget.model.sportKey==AppConstants.TAG_BASEBALL?2:widget.model.sportKey==AppConstants.TAG_HOCKEY?3:widget.model.sportKey==AppConstants.TAG_HANDBALL?2:widget.model.sportKey==AppConstants.TAG_KABADDI?1:widget.model.sportKey==AppConstants.TAG_BASKETBALL?1:3;
      selectedPlayer.bat_maxcount=widget.model.sportKey==AppConstants.TAG_CRICKET?5:widget.model.sportKey==AppConstants.TAG_BASEBALL?5:widget.model.sportKey==AppConstants.TAG_HOCKEY?5:widget.model.sportKey==AppConstants.TAG_HANDBALL?4:widget.model.sportKey==AppConstants.TAG_KABADDI?2:widget.model.sportKey==AppConstants.TAG_BASKETBALL?4:5;
      selectedPlayer.bat_selected=0;
      selectedPlayer.bowl_mincount=widget.model.sportKey==AppConstants.TAG_CRICKET?1:widget.model.sportKey==AppConstants.TAG_BASEBALL?1:widget.model.sportKey==AppConstants.TAG_HOCKEY?1:widget.model.sportKey==AppConstants.TAG_BASKETBALL?1:1;
      selectedPlayer.bowl_maxcount=widget.model.sportKey==AppConstants.TAG_CRICKET?5:widget.model.sportKey==AppConstants.TAG_BASEBALL?1:widget.model.sportKey==AppConstants.TAG_HOCKEY?3:widget.model.sportKey==AppConstants.TAG_BASKETBALL?4:3;
      selectedPlayer.bowl_selected=0;
      selectedPlayer.ar_mincount=widget.model.sportKey==AppConstants.TAG_CRICKET?1:widget.model.sportKey==AppConstants.TAG_BASEBALL?1:widget.model.sportKey==AppConstants.TAG_HOCKEY?3:widget.model.sportKey==AppConstants.TAG_HANDBALL?2:widget.model.sportKey==AppConstants.TAG_KABADDI?1:widget.model.sportKey==AppConstants.TAG_BASKETBALL?1:3;
      selectedPlayer.ar_maxcount=widget.model.sportKey==AppConstants.TAG_CRICKET?5:widget.model.sportKey==AppConstants.TAG_BASEBALL?1:widget.model.sportKey==AppConstants.TAG_HOCKEY?5:widget.model.sportKey==AppConstants.TAG_HANDBALL?4:widget.model.sportKey==AppConstants.TAG_KABADDI?3:widget.model.sportKey==AppConstants.TAG_BASKETBALL?4:5;
      selectedPlayer.ar_selected=0;
      selectedPlayer.c_min_count=1;
      selectedPlayer.c_max_count=4;
      selectedPlayer.c_selected=0;
      selectedPlayer.selectedPlayer=0;
      selectedPlayer.total_player_count=limit.maxplayers!;
      selectedPlayer.localTeamMaxplayerCount=6;
      selectedPlayer.visitorTeamMaxplayerCount=6;
      selectedPlayer.localTeamplayerCount=0;
      selectedPlayer.visitorTeamPlayerCount=0;
      selectedPlayer.total_credit=0.0;
      localTeamplayerCount=0;
      visitorTeamPlayerCount=0;
    }
  }
  void updateTeamData(
      int extra_player,
      int wk_selected,
      int bat_selected,
      int ar_selected,
      int bowl_selected,
      int c_selected,
      int selectPlayer,
      int localTeamplayerCount,
      int visitorTeamPlayerCount,
      double total_credit) {
    exeedCredit = false;
    selectedPlayer.extra_player=extra_player;
    selectedPlayer.wk_selected=wk_selected;
    selectedPlayer.bat_selected=bat_selected;
    selectedPlayer.ar_selected=ar_selected;
    selectedPlayer.bowl_selected=bowl_selected;
    selectedPlayer.c_selected=c_selected;
    selectedPlayer.selectedPlayer=selectPlayer;
    selectedPlayer.localTeamplayerCount=localTeamplayerCount;
    selectedPlayer.visitorTeamPlayerCount=visitorTeamPlayerCount;
    selectedPlayer.total_credit=total_credit;
    this.localTeamplayerCount=localTeamplayerCount;
    this.visitorTeamPlayerCount=visitorTeamPlayerCount;
    setState(() {});

  }
  void minimumPlayerWarning() {
    if (selectedPlayer.c_selected < selectedPlayer.c_min_count && widget.model.sportKey==AppConstants.TAG_BASKETBALL) {
      showTeamValidation("You must select at least " + selectedPlayer.c_min_count.toString() + ' Center.');
    }else if (selectedPlayer.bowl_selected < selectedPlayer.bowl_mincount) {
      showTeamValidation("You must select at least " + selectedPlayer.bowl_mincount.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' Bowlers':widget.model.sportKey==AppConstants.TAG_BASEBALL? ' Catcher':
      widget.model.sportKey==AppConstants.TAG_HOCKEY?' Striker':
      widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Power-Forward':
      ' State-Forward')+'.');
    } else if (selectedPlayer.bat_selected < selectedPlayer.bat_mincount) {
      showTeamValidation("You must select at least " + selectedPlayer.bat_mincount.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' Batter':widget.model.sportKey==AppConstants.TAG_BASEBALL? ' Pitcher':
      widget.model.sportKey==AppConstants.TAG_HOCKEY?' Defender':
      widget.model.sportKey==AppConstants.TAG_HANDBALL?' Defender':
      widget.model.sportKey==AppConstants.TAG_KABADDI?' All-Rounder':
      widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Shooting-Guard':
      ' Defender')+'.');
    } else if (selectedPlayer.ar_selected < selectedPlayer.ar_mincount) {
      showTeamValidation("You must select at least " + selectedPlayer.ar_mincount.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' All-Rounders':widget.model.sportKey==AppConstants.TAG_BASEBALL? ' Infielders':
      widget.model.sportKey==AppConstants.TAG_HOCKEY?' Midfielder':
      widget.model.sportKey==AppConstants.TAG_HANDBALL?' State-Forward':
      widget.model.sportKey==AppConstants.TAG_KABADDI?' Raider':
      widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Small-Forward':
      ' Midfielder')+'.');
    } else if (selectedPlayer.wk_selected< selectedPlayer.wk_min_count) {
      showTeamValidation("You must select at least " + selectedPlayer.wk_min_count.toString() + (widget.model.sportKey==AppConstants.TAG_CRICKET?' Wicket-Keeper':widget.model.sportKey==AppConstants.TAG_BASEBALL? ' Outfielders':
      widget.model.sportKey==AppConstants.TAG_HOCKEY?' Goal-Keeper':
      widget.model.sportKey==AppConstants.TAG_HANDBALL?' Goal-Keeper':
      widget.model.sportKey==AppConstants.TAG_KABADDI?' Defender':
      widget.model.sportKey==AppConstants.TAG_BASKETBALL?' Point-Guard':
      ' Goal-Keeper')+'.');
    }
  }


  void showTeamValidation(String mesg) {
    MethodUtils.showError(context, mesg);
  }



  void getPlayerList() async {
    AppLoaderProgress.showLoader(context);
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.fantasySlots.toString());
    final client = ApiClient(AppRepository.dio);
    PlayerListResponse response = await client.getPlayerList(contestRequest);

    if (response.status == 1) {
      allPlayerList = response.result!;
      limit=response.limit!;
      islineUp=response.is_visible_lineup??0;
      createTeamData();
      for (Player player in allPlayerList) {
        if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_KEEP:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_OF:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_GK:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppConstants.KEY_PLAYER_ROLE_GK:widget.model.sportKey==AppConstants.TAG_KABADDI?AppConstants.KEY_PLAYER_ROLE_DEF:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_PG:AppConstants.KEY_PLAYER_ROLE_GK).toString().toLowerCase()) {
          wkList.add(player);
          wkListTemp.add(player);
          if (player.team=="team1") {
            team1wkList.add(player);
          } else {
            team2wkList.add(player);
          }
        } else if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_BAT:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_IF:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_DEF:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppConstants.KEY_PLAYER_ROLE_DEF:widget.model.sportKey==AppConstants.TAG_KABADDI?AppConstants.KEY_PLAYER_ROLE_ALL_R:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_SG:AppConstants.KEY_PLAYER_ROLE_DEF).toString().toLowerCase()) {
          batList.add(player);
          batListTemp.add(player);
          if (player.team=="team1") {
            team1batList.add(player);
          } else {
            team2batList.add(player);
          }
        }else if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_ALL_R:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_PITCHER:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_MID:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppConstants.KEY_PLAYER_ROLE_ST:widget.model.sportKey==AppConstants.TAG_KABADDI?AppConstants.KEY_PLAYER_ROLE_RD:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_SF:AppConstants.KEY_PLAYER_ROLE_MID).toString().toLowerCase()) {
          arList.add(player);
          arListTemp.add(player);
          if (player.team=="team1") {
            team1arList.add(player);
          } else {
            team2arList.add(player);
          }
        }else if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_BOL:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_CATCHER:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_STR:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_PF:AppConstants.KEY_PLAYER_ROLE_ST).toString().toLowerCase()) {
          bolList.add(player);
          bolListTemp.add(player);
          if (player.team=="team1") {
            team1bolList.add(player);
          } else {
            team2bolList.add(player);
          }
        }else if (player.role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_C.toString().toLowerCase()) {
          cList.add(player);
          cListTemp.add(player);
          if (player.team=="team1") {
            team1cList.add(player);
          } else {
            team2cList.add(player);
          }
        }
      }

    }
    if (selectedList.length > 0) {
      for (int i = 0; i < allPlayerList.length; i++) {
        for (Player player in selectedList) {
          if (player.id == allPlayerList[i].id) {
            allPlayerList[i].isSelected=true;
            if (player.captain == 1)
              allPlayerList[i].isCaptain=true;
            if (player.vicecaptain == 1)
              allPlayerList[i].isVcCaptain=true;
          }
        }
      }
      setSelectedCountForEditCloneOrUpload(selectedPlayer.total_player_count, 0);
    }
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
  void setSelectedCountForEditCloneOrUpload(int totalPlayerSelected, int extraPlayer) {
    int countWK = 0;
    int countBAT = 0;
    int countBALL = 0;
    int countALL = 0;
    int countC = 0;
    int totalCount = 0;
    int team1Count = 0;
    int team2Count = 0;
    double usedCredit = 0;

    for (Player player in allPlayerList) {
      if (player.isSelected??false) {
        if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_KEEP:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_OF:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_GK:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppConstants.KEY_PLAYER_ROLE_GK:widget.model.sportKey==AppConstants.TAG_KABADDI?AppConstants.KEY_PLAYER_ROLE_DEF:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_PG:AppConstants.KEY_PLAYER_ROLE_GK).toString().toLowerCase()) {
          countWK++;
        }
        if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_BAT:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_IF:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_DEF:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppConstants.KEY_PLAYER_ROLE_DEF:widget.model.sportKey==AppConstants.TAG_KABADDI?AppConstants.KEY_PLAYER_ROLE_ALL_R:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_SG:AppConstants.KEY_PLAYER_ROLE_DEF).toString().toLowerCase()) {
          countBAT++;
        }
        if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_ALL_R:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_PITCHER:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_MID:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppConstants.KEY_PLAYER_ROLE_ST:widget.model.sportKey==AppConstants.TAG_KABADDI?AppConstants.KEY_PLAYER_ROLE_RD:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_SF:AppConstants.KEY_PLAYER_ROLE_MID).toString().toLowerCase()) {
          countALL++;
        }
        if (player.role.toString().toLowerCase()==(widget.model.sportKey==AppConstants.TAG_CRICKET?AppConstants.KEY_PLAYER_ROLE_BOL:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppConstants.KEY_PLAYER_ROLE_CATCHER:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppConstants.KEY_PLAYER_ROLE_STR:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppConstants.KEY_PLAYER_ROLE_PF:AppConstants.KEY_PLAYER_ROLE_ST).toString().toLowerCase()) {
          countBALL++;
        }
        if (player.role.toString().toLowerCase()==AppConstants.KEY_PLAYER_ROLE_C.toString().toLowerCase()) {
          countC++;
        }

        if (player.team=="team1") {
          team1Count++;
        }

        if (player.team=="team2") {
          team2Count++;
        }

        totalCount++;
        usedCredit += double.parse(player.credit);
      }
    }


    selectedPlayer.wk_selected=countWK;
    selectedPlayer.bat_selected=countBAT;
    selectedPlayer.ar_selected=countALL;
    selectedPlayer.bowl_selected=countBALL;
    selectedPlayer.c_selected=countC;
    selectedPlayer.selectedPlayer=totalPlayerSelected;
    selectedPlayer.localTeamplayerCount=team1Count;
    selectedPlayer.visitorTeamPlayerCount=team2Count;
    selectedPlayer.total_credit=usedCredit;

    updateTeamData(
        extraPlayer,
        selectedPlayer.wk_selected,
        selectedPlayer.bat_selected,
        selectedPlayer.ar_selected,
        selectedPlayer.bowl_selected,
        selectedPlayer.c_selected,
        selectedPlayer.selectedPlayer,
        selectedPlayer.localTeamplayerCount,
        selectedPlayer.visitorTeamPlayerCount,
        selectedPlayer.total_credit);
  }



}
