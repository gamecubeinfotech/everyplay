import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/leaderboard_details_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class PromoterUserStats extends StatefulWidget {
  String seriesId;
  String userId;
  PromoterUserStats(this.seriesId,this.userId);
  @override
  _PromoterUserStatsState createState() => new _PromoterUserStatsState();
}

class _PromoterUserStatsState extends State<PromoterUserStats>{

  TextEditingController codeController = TextEditingController();
  List<LeaderboardMatchData> matchList=[];
  LeaderboardDetailsResponse details=new LeaderboardDetailsResponse();
  bool loading=false;
  String userId='0';

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getLeaderboardDetails();
      })
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: primaryColor,
            // padding: EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.white,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('User Stats',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Stack(
          children: [
            !loading?new Container(
              height: MediaQuery.of(context).size.height,
              child: new NestedScrollView(
                headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[
                    SliverToBoxAdapter(
                      child: new Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(AppImages.statsWinnerBgIcon),
                                fit: BoxFit.fill)),
                        width: MediaQuery.of(context).size.width,
                        child: new Column(
                          children: [
                            new Container(
                              height: 96,
                              width: 96,
                              margin: EdgeInsets.only(top: 5),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.white,width: 3),
                                  borderRadius: BorderRadius.circular(48),
                                  color: Colors.white
                              ),
                              child: ClipOval(
                                child: CachedNetworkImage(
                                    height: 96,
                                    width: 96,
                                    imageUrl: details.image??'',
                                    placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                    errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                    fit: BoxFit.fill
                                ),
                              ),
                            ),
                            new Container(
                              padding: EdgeInsets.only(top: 10),
                              alignment: Alignment.center,
                              child: Text(
                                details.name!,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 18,
                                  fontFamily: 'roboto',
                                  color: Colors.white,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            new Container(
                              padding: EdgeInsets.only(top: 20,left: 10),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                details.series_name!,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 16,
                                  fontFamily: 'roboto',
                                  color: Colors.white,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.only(top: 10,bottom: 10),
                              child: new Card(
                                color: Colors.black54,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: new Container(
                                  padding: EdgeInsets.all(8),
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          new Container(
                                            margin: EdgeInsets.only(bottom: 4),
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              'Investment',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontFamily: 'roboto',
                                                fontSize: 14,
                                                color: Colors.white,
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          new Container(
                                            alignment: Alignment.center,
                                            child: Text(
                                              details.points.toString(),
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontFamily: 'roboto',
                                                fontSize: 14,
                                                color: Colors.white,
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      new Container(
                                        width: 1,
                                        height: 50,
                                        alignment: Alignment.center,
                                        color: darkPrimaryColor,
                                      ),
                                      new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          new Container(
                                            margin: EdgeInsets.only(bottom: 4),
                                            alignment: Alignment.center,
                                            child: Text(
                                              'Rank',
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontFamily: 'roboto',
                                                fontSize: 14,
                                                color: Colors.white,
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          new Container(
                                            alignment: Alignment.center,
                                            child: Text(
                                              '#'+details.rank.toString(),
                                              textAlign: TextAlign.start,
                                              style: TextStyle(
                                                fontFamily: 'roboto',
                                                fontSize: 14,
                                                color: Colors.white,
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  ];
                },
                body: new Container(
                  color: Colors.white,
                  child: new Column(
                    children: [
                      new Container(
                        padding: EdgeInsets.only(top: 10,left: 10,bottom: 10),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Matchwise Fantasy Stats',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      new Divider(height: 1,thickness: 1,),
                      new Container(
                        height: 30,
                        color: lightGrayColor,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                'Match ('+matchList.length.toString()+')',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12),
                              ),
                            ),

                            new Container(
                              alignment: Alignment.centerLeft,
                              margin: EdgeInsets.only(right: 10),
                              child: Text(
                                'Investment',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12),
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Divider(height: 1,thickness: 1,),
                      new Expanded(
                        child: new ListView.builder(
                            shrinkWrap: true,
                            scrollDirection: Axis.vertical,
                            itemCount: matchList.length,
                            itemBuilder: (BuildContext context, int index) {
                              return new GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                child: new Column(
                                  children: [
                                    new Container(
                                      padding: EdgeInsets.only(left: 10,right: 0,top: 10,bottom: 10),
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          new Container(
                                            child:  new Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                new Text(
                                                  matchList[index].match_name!,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 14,
                                                      fontWeight: FontWeight.w500),
                                                ),
                                                new Container(
                                                  margin: EdgeInsets.only(top: 5),
                                                  child: new Text(
                                                    matchList[index].match_date!,
                                                    style: TextStyle(
                                                        color: Colors.grey,
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.w500),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.only(right: 10,left: 0),
                                            alignment: Alignment.center,
                                            child: Text(
                                              "₹ "+matchList[index].points.toString(),
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 14),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    new Divider(height: 1,thickness: 1,),
                                  ],
                                ),
                                onTap: (){
                                  // navigateToTeamPreview(context);
                                },
                              );
                            }
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ):new Container(),
          ],
        ),
      ),
    );
  }
  void getLeaderboardDetails() async {
    setState(() {
      loading=true;
    });
    AppLoaderProgress.showLoader(context);
    GeneralRequest request = new GeneralRequest(user_id: widget.userId,series_id: widget.seriesId);
    final client = ApiClient(AppRepository.dio);
    LeaderboardDetailsResponse response = await client.getPromoterMatchLeaderboard(request);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
    if (response.success??false) {
      details=response;
      matchList=response.match_list!;
    }
    setState(() {
      loading=false;
    });
  }
}