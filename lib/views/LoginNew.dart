import 'dart:convert';
import 'dart:ffi' as ffi;
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/login_request.dart';
import 'package:EverPlay/repository/model/data.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:http/http.dart' as http;

class LoginNew extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginNew> {
  bool _passwordVisible = false;
  TextEditingController mobileController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  String fcmToken = '';
  int version = 0;
  bool checkedValue = false;

  @override
  void initState() {
    super.initState();

    FirebaseMessaging _messaging = FirebaseMessaging.instance;
    _messaging.getToken().then((token) {
      fcmToken = token!;
    });
    deviceVersion();
  }

  void deviceVersion() async {
    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      setState(() {
        version = 0;
        // version = int.parse(iosInfo.systemVersion.split('.')[0]);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        statusBarIconBrightness: Brightness.light,
    /* set Status bar icons color in Android devices.*/
        statusBarBrightness: Brightness.light)
    );

    return Container(
      // decoration: BoxDecoration(
      //   image: DecorationImage(
      //     image: AssetImage(AppImages.main_bg),
      //     fit: BoxFit.fill,
      //   ),
      // ),
      color: whiteColor,
      child: SafeArea(
        child: new Scaffold(
          //  backgroundColor: Colors.transparent,
            backgroundColor: whiteColor,
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16),
                      child: Row(
                        children: [
                          new Text('Welcome Back!',
                              style: TextStyle(
                                  fontFamily: 'roboto',
                                  color: Title_Color_1,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20)),
                        ],
                      ),
                    ),margin: EdgeInsets.only(left: 16),),

                  Padding(
                    padding: const EdgeInsets.only(left: 16.0,top: 8),
                    child: new Text('Log in to your existant account of EverPlay Fantasy Cricket.',
                        style: TextStyle(
                            fontFamily: AppConstants.textBold,
                            color: darkgray,
                            fontWeight: FontWeight.w500,
                            fontSize: 14)),
                  ),


                  Container(
                    padding: EdgeInsets.fromLTRB(10,20,10,10,),
                    width: MediaQuery.of(context).size.width,
                    child: Container(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            //TODO to edit employee details
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.fromLTRB(14,14,14,7),
                                child: Text("Email/Mobile Number",
                                    style: TextStyle(
                                        fontFamily: AppConstants.textBold,
                                        color: Text_Color,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 12)),
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            width: MediaQuery.of(context).size.width,

                            //padding: new EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              color: Colors.white,
                              border: Border.all(color: editbgcolor),
                            ),
                            child: Padding(
                              padding:
                              const EdgeInsets.only(left: 16.0, right: 8),
                              child: TextField(
                                controller: mobileController,
                                style: TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: 'Enter Mobile No. or Email Address',
                                  hintStyle: TextStyle(color: Colors.grey),
                                ),
                                keyboardType: TextInputType.emailAddress,
                                obscureText: false,
                              ),
                            ),
                          ),
                          new Container(
                            padding: EdgeInsets.fromLTRB(14,14,14,7),
                            child: Text(
                              'Enter Password',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 12,
                                color: Text_Color,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            width: MediaQuery.of(context).size.width,

                            //padding: new EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              borderRadius:
                              BorderRadius.all(Radius.circular(10)),
                              color: TextFieldColor,
                            ),
                            padding: const EdgeInsets.only(left: 16.0, right: 8,top: 5),
                            child: TextField(
                              obscureText: !_passwordVisible,
                              style: TextStyle(color: Colors.black),
                              controller: passwordController,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Password',
                                hintStyle: TextStyle(color: Colors.grey),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                    // Based on passwordVisible state choose the icon
                                      _passwordVisible
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: _passwordVisible? yellowColor : Colors.grey
                                  ),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _passwordVisible = !_passwordVisible;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),
                          SizedBox(height: 10,),
                          GestureDetector(
                            onTap: (){
                              navigateToForgotPassword(context);
                            },
                            child: Container(
                              margin: EdgeInsets.fromLTRB(10, 0, 16, 0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text("Forgot Password?",style: TextStyle(
                                    color: Themecolor,
                                    fontSize: 13,
                                    /*decoration: TextDecoration.underline*/
                                  ),)
                                ],
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 16,top: 5),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                new Container(
                                  width: 20,
                                  child: Checkbox(
                                    side:BorderSide(color: darkgray),
                                    value: checkedValue,
                                    activeColor: Themecolor,
                                    onChanged: (value) {
                                      setState(() {
                                        checkedValue = value!;
                                      });
                                    },
                                  ),
                                ),
                                Expanded(child: new Container(
                                  padding: EdgeInsets.only(left: 10),
                                  child: Text(
                                    'I certify that i am above 18 Years',
                                    style: TextStyle(fontWeight: FontWeight.normal,fontSize: 15,color: Title_Color_1),
                                  ),
                                )),
                              ],
                            ),
                          ),
                          /* Container(
                          width: MediaQuery.of(context).size.width,
                          padding: const EdgeInsets.only(left:16,right: 16,top: 20),
                          //height: MediaQuery.of(context).size.height*0.,
                          child: ElevatedButton(
                            disabledColor: Colors.red,
                            disabledTextColor: Colors.black,
                            onPressed: null,
                            child: Text('Send'),
                          ),
                        ),*/

                          new Container(
                              width: MediaQuery.of(context).size.width,
                              height: 45,
                              margin: EdgeInsets.fromLTRB(10, 32, 10, 16),
                              child: ElevatedButton(
                                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),elevation: MaterialStateProperty.all(.5) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)))),
                                child: Text(
                                  'Sign In',
                                  style: TextStyle(fontSize: 15,color: Colors.white),
                                ),

                                onPressed: () {
                                  FocusScope.of(context).unfocus();
                                  if (isValidMobile(
                                      mobileController.text.toString())) {
                                    userLogin("mobile");
                                  } else if (isValidMail(
                                      mobileController.text.toString())) {
                                    userLogin("email");
                                  }  else {
                                    Fluttertoast.showToast(
                                        msg:
                                        'Please enter a valid Mobile Number / Email Id.',
                                        toastLength: Toast.LENGTH_SHORT,
                                        timeInSecForIosWeb: 1);
                                  }
                                },
                              )),

                        ],
                      ),
                    ),
                  ),

                  new Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.only(
                        left: 13, right: 13, top: 13),
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        new Flexible(
                          child: new Container(
                            height: .1,
                            child: DottedBorder(
                              color: yellowdark,
                              strokeWidth: .5,
                              borderType: BorderType.RRect,
                              child: Container(height: .1,),
                            ),
                          ),
                          flex: 1,
                        ),
                        new Flexible(
                          child: new Text(
                            'Or Log in with',
                            style: TextStyle(
                                color: Colors.grey, fontSize: 14),
                          ),
                          flex: 1,
                        ),
                        new Flexible(
                          child: new Container(
                            height: .1,
                            child: DottedBorder(
                              color: yellowdark,
                              strokeWidth: .5,
                              borderType: BorderType.RRect,
                              child: Container(height: .1,),
                            ),
                          ),
                          flex: 1,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 20),
                    child: Column(
                      children: [
                        /* Expanded(
                          flex: 1,
                          child: GestureDetector(
                              child: Container(
                                height: 45,
                                margin: EdgeInsets.all(12),
                                // margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(4)),
                                    color: const Color(0xff3b5999)),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  children: [
                                    Image.asset(AppImages.fb_image,
                                        width: 24, height: 24),
                                    Text(
                                      'Facebook',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white),
                                    )
                                  ],
                                ),
                              ),
                              onTap: () {
                                initiateFacebookLogin();
                              }),
                        ),*/
                        GestureDetector(
                            child: Container(
                              height: 45,
                              margin: EdgeInsets.all(12),
                              //margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                              //padding: const EdgeInsets.only(top: 10,bottom: 10),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(25)),
                                  color: const Color(0xffffffff),
                                  border: Border.all(color: Themecolor)),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.center,
                                children: [
                                  Image.asset(AppImages.gmail_image,
                                      width: 24, height: 24),
                                  SizedBox(width: 10,),
                                  Text(
                                    '  Login with Google',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(color: Themecolor,fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ),
                            onTap: (){
                              if (!checkedValue){
                                Fluttertoast.showToast(
                                    msg:
                                    'Please accept EverPlay T&Cs.',
                                    toastLength: Toast.LENGTH_SHORT,
                                    timeInSecForIosWeb: 1);
                                return;
                              }else {signup(context);}

                            }
                        ),
                        version >= 13
                            ? new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: Container(
                            height: 45,
                            margin: EdgeInsets.all(12),
                            //margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            //padding: const EdgeInsets.only(top: 10,bottom: 10),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(25)),
                                color: const Color(0xffffffff),
                                border: Border.all(color: Themecolor)),
                            child: Row(
                              mainAxisAlignment:
                              MainAxisAlignment.center,
                              children: [
                                Image.asset(AppImages.appleImageURL,
                                    width: 24, height: 24),
                                SizedBox(width: 10,),
                                Text(
                                  '  Login with Apple',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(color: Themecolor,fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          onTap: () async {
                            final credential =
                            await SignInWithApple
                                .getAppleIDCredential(
                              scopes: [
                                AppleIDAuthorizationScopes
                                    .email,
                                AppleIDAuthorizationScopes
                                    .fullName,
                              ],
                              webAuthenticationOptions:
                              WebAuthenticationOptions(
                                // TODO: Set the `clientId` and `redirectUri` arguments to the values you entered in the Apple Developer portal during the setup
                                clientId: 'com.everplay',
                                redirectUri: Uri.parse(
                                  'https://flutter-sign-in-with-apple-example.glitch.me/callbacks/sign_in_with_apple',
                                ),
                              ),
                              // TODO: Remove these if you have no need for them
                              nonce: 'example-nonce',
                              state: 'example-state',
                            );
                            userLoginSocial(new LoginRequest(
                                name:
                                credential.givenName ?? '',
                                email: credential.email ?? '',
                                image: '',
                                authorizationCode: credential
                                    .authorizationCode ??
                                    '',
                                idToken:
                                credential.identityToken ??
                                    '',
                                deviceId: '',
                                social_id:
                                credential.userIdentifier,
                                socialLoginType: 'apple',
                                fcmToken: fcmToken));
                          },
                        )
                            : new Container(),
                      ],
                    ),
                  ),

                  new Container(
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        new Container(
                          child: Text("Don't have an account? ",
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: Title_Color_1,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 14)),
                        ),
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () => {
                            Navigator.pop(context),
                            navigateToRegisterNew(context),
                          },
                          child: new Container(
                            child: Text("Register Now",
                                style: TextStyle(
                                    fontFamily: AppConstants.textBold,
                                    color: Themecolor,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14)),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(55), // Set this height
              child: Container(
                //height: 500,
                color: Colors.transparent,
                // padding: EdgeInsets.only(top: 28),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                      new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => {Navigator.pop(context)},
                      child: new Container(
                        padding: EdgeInsets.all(15),
                        alignment: Alignment.centerLeft,
                        child: new Container(
                          width: 16,
                          height: 16,
                          child: Image(
                            image: AssetImage(AppImages.backImageURL),
                            fit: BoxFit.fill,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    new Container(
                      padding: EdgeInsets.all(15),
                      child: new Text('Log in',
                          style: TextStyle(
                              fontFamily: AppConstants.textBold,
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 18)),
                    ),
                  ],
                ),
              ),
            )),
      ),
    );
  }

  void userLogin(type) async {
    if (passwordController.text.isEmpty) {
      Fluttertoast.showToast(
          msg: 'Please enter Password.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }else if (!checkedValue){
      Fluttertoast.showToast(
          msg:
          'Please accept EverPlay T&Cs.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }

    AppLoaderProgress.showLoader(context);
    LoginRequest loginRequest = new LoginRequest(
        email: mobileController.text.toString(),
        password: passwordController.text.toString(),
        deviceId: '',
        fcmToken: fcmToken,
        type: type);

    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse = await client.userLogin(loginRequest);
    AppLoaderProgress.hideLoader(context);
    // print("my login data is");
    // print(loginResponse.result!.custom_user_token!);
    if (loginResponse.status == 1) {
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE,
          loginResponse.result!.mobile.toString());
/*
      if(loginResponse.isMobile==1){
        AppPrefrence.putString(AppConstants.FROM, "login");
        AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID, loginResponse.result!.user_id.toString());
        navigateToOtpVerify(context);
        return;
      }
*/
      AppPrefrence.putString(AppConstants.FROM, "login");
      AppPrefrence.putString(AppConstants.LOGIN_REGISTER_TYPE, "manual_login");
      AppPrefrence.putBoolean(
          AppConstants.SHARED_PREFERENCES_IS_LOGGED_IN, true);

      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID, loginResponse.result!.user_id.toString());
      print("my user id is");
      print(loginResponse.result!.user_id.toString());

      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_NAME,
          loginResponse.result!.username);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_EMAIL,
          loginResponse.result!.email);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TOKEN,
          loginResponse.result!.custom_user_token);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_REFER_CODE,
          loginResponse.result!.refercode);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME,
          loginResponse.result!.team);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_STATE_NAME,
          loginResponse.result!.state);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_PIC,
          loginResponse.result!.user_profile_image);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS,
          loginResponse.result!.bank_verify);
      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS,
          loginResponse.result!.pan_verify);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS,
          loginResponse.result!.mobile_verify);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS,
          loginResponse.result!.email_verify);
      AppPrefrence.putString(
          AppConstants.AUTHTOKEN, loginResponse.result!.custom_user_token);
      // navigateToOtpVerify(context);
      navigateToHomePage(context);
    }

    Fluttertoast.showToast(
        msg: loginResponse.message!,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1);
  }

  bool isValidMail(String email) {
    return RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(email);
  }

  bool isValidMobile(String mobile) {
    return RegExp(r'(^(?:[+0]9)?[0-9]{10}$)').hasMatch(mobile);
  }

  void userLoginSocial(LoginRequest loginRequest) async {
    AppLoaderProgress.showLoader(context);
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse = await client.userLoginSocial(loginRequest);
    AppPrefrence.putString(AppConstants.LOGIN_REGISTER_TYPE, "social_login");
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID,
        loginResponse.result!.user_id.toString());
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE,
        loginResponse.result!.mobile.toString());

    /* if(loginResponse.isMobile==1){
      AppPrefrence.putString(AppConstants.FROM, "login");
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID, loginResponse.result!.user_id.toString());
      navigateToOtpVerify(context);
    }else*/
    AppLoaderProgress.hideLoader(context);
    if(loginResponse.result!.mobile_verify==1){
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_NAME,
        loginResponse.result!.username);
    AppPrefrence.putString(
        AppConstants.SHARED_PREFERENCE_USER_EMAIL, loginResponse.result!.email);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TOKEN,
        loginResponse.result!.custom_user_token);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_REFER_CODE,
        loginResponse.result!.refercode);
    AppPrefrence.putBoolean(
        AppConstants.SHARED_PREFERENCES_IS_LOGGED_IN, true);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME,
        loginResponse.result!.team);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_STATE_NAME,
        loginResponse.result!.state??'');
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_PIC,
        loginResponse.result!.user_profile_image);
    AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS,
        loginResponse.result!.bank_verify);
    AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS,
        loginResponse.result!.pan_verify);
    AppPrefrence.putInt(
        AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS,
        loginResponse.result!.mobile_verify);
    AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS,
        loginResponse.result!.email_verify);
    AppPrefrence.putString(
        AppConstants.AUTHTOKEN, loginResponse.result!.custom_user_token);
    AppConstants.token = loginResponse.result!.custom_user_token!;
    AppPrefrence.putString(AppConstants.FROM, "login");
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID,
        loginResponse.result!.user_id.toString());

    navigateToHomePage(context);
    // navigateToOtpVerify(context);
    }
    else{
      navigateToVerifyEmailOrMobile(context, 'mobile', emailViewRefresh);
    }
  }

  Future<void> signup(BuildContext context) async {
    final FirebaseAuth auth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
      await googleSignInAccount.authentication;
      final AuthCredential authCredential = GoogleAuthProvider.credential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);
      // Getting users credential
      UserCredential result = await auth.signInWithCredential(authCredential);

      if (result != null) {
        User user = result.user!;
        userLoginSocial(new LoginRequest(name: user.displayName,email: user.email,image: user.photoURL??'',idToken: user.refreshToken,deviceId: '',social_id: user.uid,socialLoginType: 'gmail',fcmToken: fcmToken));
        googleSignIn.signOut();
      }  // if result not null we simply call the MaterialpageRoute,
      // for go to the HomePage screen
    }
  }

  void emailViewRefresh(String email, int type) {}

  Future<void> initiateFacebookLogin() async {
    final LoginResult result = await FacebookAuth.instance.login(permissions: ["public_profile", "email", "user_friends"]);
    switch (result.status) {
      case  LoginStatus.failed:
        print("Error");

        break;
      case LoginStatus.cancelled:
        print("CancelledByUser");

        break;
      case LoginStatus.success:
        var graphResponse = await http.get(Uri.parse('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token='+result.accessToken!.token));
        var profile = json.decode(graphResponse.body);
        userLoginSocial(new LoginRequest(name: profile['name'] ,email: profile['email'],image: profile['picture']??'',idToken: '',deviceId: '',social_id: profile['id'],socialLoginType: 'facebook',fcmToken: fcmToken));
        break;
    }
  }

}
