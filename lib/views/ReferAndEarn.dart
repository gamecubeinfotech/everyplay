import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/widgets.dart';
import 'package:package_info/package_info.dart';
import 'package:share/share.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';

class ReferAndEarn extends StatefulWidget {
  @override
  _ReferAndEarnState createState() => _ReferAndEarnState();
}

class _ReferAndEarnState extends State<ReferAndEarn> {
  TextEditingController nameController = TextEditingController();
  TextEditingController teamNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController changeController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController pincodeController = TextEditingController();
  var val;
  var groupValue;
  var _dropDownValue;
  late String referCode = '0';
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_REFER_CODE)
        .then((value) => {
              setState(() {
                referCode = value;
              })
            });
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: primaryColor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.dark) /* set Status bar icon color in iOS. */
        );
    return SafeArea(
      child: Scaffold(
        backgroundColor: whiteColor,
        body: new SingleChildScrollView(
          child: new Stack(
            children: [
              new Container(
                child: new Column(
                  children: [
                    Container(color: Colors.black,
                      height: 55,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () => {Navigator.pop(context)},
                            child: new Container(
                              padding: EdgeInsets.all(15),
                              alignment: Alignment.centerLeft,
                              child: new Container(
                                width: 16,
                                height: 16,
                                child: Image(
                                  image: AssetImage(AppImages.backImageURL),
                                  fit: BoxFit.fill,
                                ),
                              ),
                            ),
                          ),
                          new Container(
                            child: new Text('Refer & Earn',
                                style: TextStyle(
                                    fontFamily: AppConstants.textBold,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18)),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      // height: 370,
                      alignment: Alignment.topCenter,
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          new Container(
                            margin: EdgeInsets.only(top: 16,left: 32,right: 32,bottom: 16),
                            child: Image(
                              image: AssetImage(AppImages.referHeaderIcon),
                              fit: BoxFit.fill,
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.only(top: 10, left: 32, right: 32,bottom: 32),
                            alignment: Alignment.center,

                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(children: <TextSpan>[
                                TextSpan(
                                    text: 'Refer to  your friend and Get '.toUpperCase(),
                                    style: TextStyle(
                                        fontFamily: AppConstants.textSemiBold,
                                        color: TextFieldTextColor,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 15,
                                        height: 1.5)),
                                TextSpan(
                                    text: '₹ 100 '.toUpperCase(),
                                    style: TextStyle(
                                        fontFamily: AppConstants.textSemiBold,
                                        color: TextFieldTextColor,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 15,
                                        height: 1.5)),
                                TextSpan(
                                    text: 'and '.toUpperCase(),
                                    style: TextStyle(
                                        fontFamily: AppConstants.textSemiBold,
                                        color: TextFieldTextColor,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14,
                                        height: 1.5)),
                                TextSpan(
                                    text: ' 20% lifetime commission '.toUpperCase(),
                                    style: TextStyle(
                                      fontFamily: AppConstants.textSemiBold,
                                        color: TextFieldTextColor,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14,
                                        height: 1.5)),
                                TextSpan(
                                    text: 'on refer'.toUpperCase(),
                                    style: TextStyle(
                                        fontFamily: AppConstants.textSemiBold,
                                        color: TextFieldTextColor,
                                        fontWeight: FontWeight.w700,
                                        fontSize: 14,
                                        height: 1.5)),
                              ]),
                            ),
                          ),

                        ],
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(
                          top: 10, bottom: 20, left: 36, right: 36),
                      alignment: Alignment.center,
                      child: new Text(
                        'Share this code with your friends and after they Signup and Deposit the money, both of you will get ₹100 and your friend will get ₹300',
                        style: TextStyle(
                            fontFamily: AppConstants.textBold,
                            color: Text_Color,
                            fontWeight: FontWeight.w500,
                            fontSize: 13,
                            height: 1.5),
                        textAlign: TextAlign.center,
                      ),
                    ),

                    DottedBorder(
                      dashPattern: [4, 4],
                      strokeWidth: 5,
                      padding: EdgeInsets.all(0),
                      color: yellowdark,
                      child: Container(
                        // height: 45,
                        width: 250,
                        color: yellowdark,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Container(
                              margin: EdgeInsets.all(15),
                              alignment: Alignment.center,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Your referral code:',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 8,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                  new Text(
                                    referCode,
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 17,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              height: 30,
                              width: 1,color: Colors.white,
                            ),
                            new GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Icon(Icons.copy,color: Colors.white,size: 20,),
                                  Text(
                                    'Copy code',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 10,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              onTap: (){
                                Clipboard.setData(new ClipboardData(text: referCode)).then((_){
                                  MethodUtils.showSuccess(context, "Invite code copied.");
                                });
                              },
                            ),
                            SizedBox(width: 5,),
                          ],
                        ),
                      ),
                    ),

                    SizedBox(height: 20,),
                    new Container(
                      margin: EdgeInsets.only(
                          top: 16, bottom: 15, left: 15, right: 15),
                      alignment: Alignment.center,
                      child: new Text(
                        'How does it work?',
                        style: TextStyle(
                          color: TextFieldTextColor,
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Divider(height: 2, thickness: 1,color: Color(0xffEAEBF6),),
                    new Container(
                      margin: EdgeInsets.only(left: 35,top: 8, bottom: 8),
                      child: new Row(
                        children: [
                          new Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 40,
                            width: 40,
                            child: new Image.asset(AppImages.referReferIcon,
                                fit: BoxFit.fill),
                          ),
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Text(
                                'Invite your Friends',
                                style: TextStyle(
                                    color: TextFieldTextColor,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w700),
                              ),
                              new Container(
                                margin: EdgeInsets.only(top: 5),
                                child: new Text(
                                  'Just share your referral code',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Divider(height: 2, thickness: 1,color: Color(0xffEAEBF6),),
                    SizedBox(height: 10,),
                    Divider(height: 2, thickness: 1,color: Color(0xffEAEBF6),),
                    new Container(
                      margin: EdgeInsets.only(left: 35, top: 8, bottom: 8),
                      child: new Row(
                        children: [
                          new Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 40,
                            width: 40,
                            child: new Image.asset(AppImages.rupeeReferIcon,
                                fit: BoxFit.fill),
                          ),
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Text(
                                'Signup & deposit',
                                style: TextStyle(
                                    color: TextFieldTextColor,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w700),
                              ),
                              new Container(
                                margin: EdgeInsets.only(top: 5),
                                child: new Text(
                                  'Refer your friends for signup and deposit',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Divider(height: 2, thickness: 1,color: Color(0xffEAEBF6),),
                    SizedBox(height: 10,),
                    Divider(height: 2, thickness: 1,color: Color(0xffEAEBF6),),
                    new Container(
                      margin: EdgeInsets.only(left: 35, top: 8, bottom: 8),
                      child: new Row(
                        children: [
                          new Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 40,
                            width: 40,
                            child: new Image.asset(AppImages.bonusReferIcon,
                                fit: BoxFit.fill),
                          ),
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Text(
                                'Both Earn ₹100',
                                style: TextStyle(
                                    color: TextFieldTextColor,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w700),
                              ),
                              new Container(
                                margin: EdgeInsets.only(top: 5),
                                child: new Text(
                                  'Both of you will get ₹100',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Divider(height: 2, thickness: 1,color: Color(0xffEAEBF6),),
                    SizedBox(height: 10,),
                    Divider(height: 2, thickness: 1,color: Color(0xffEAEBF6),),
                    new Container(
                      margin: EdgeInsets.only(left: 35, top: 8, bottom: 8),
                      child: new Row(
                        children: [
                          new Container(
                            margin: EdgeInsets.only(right: 20),
                            height: 40,
                            width: 40,
                            child: new Image.asset(AppImages.lifetimeReferIcon,
                                fit: BoxFit.fill),
                          ),
                          new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Text(
                                'You get 20% lifetime',
                                style: TextStyle(
                                    color: TextFieldTextColor,
                                    fontSize: 15,
                                    fontWeight: FontWeight.w700),
                              ),
                              new Container(
                                margin: EdgeInsets.only(top: 5),
                                child: new Text(
                                  'You get 20% lifetime commission on refer',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                    Divider(height: 2, thickness: 1,color: Color(0xffEAEBF6),),
                    new Container(
                        width: MediaQuery.of(context).size.width,
                        height: 50,
                        margin: EdgeInsets.fromLTRB(20, 30, 20, 20),
                        child: ElevatedButton(
                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))),
                          child: Text(
                            'Refer To A Friend',
                            style: TextStyle(fontSize: 15, color: Colors.white),
                          ),
                          onPressed: () {
                            _onShare(context);
                          },
                        )),
                    Divider(height: 2, thickness: 1,color: Color(0xffEAEBF6),),
                  ],
                ),
              )
            ],
          ),
        ),

      ),
    );
  }

  _onShare(BuildContext context) async {
    String shareBody = "Here's " +
        AppConstants.rupee +
        " " +
        AppConstants.invite_bonus +
        " to play fantasy cricket with me On " +
        _packageInfo.appName +
        " Click " +
        AppConstants.apk_refer_url +
        " to download " +
        _packageInfo.appName +
        " app & use My code " +

        " To Register";
    final RenderBox box = context.findRenderObject() as RenderBox;
    await Share.share(shareBody,
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }
}
