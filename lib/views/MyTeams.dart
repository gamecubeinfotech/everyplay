import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/repository/model/team_response.dart';

class MyTeams extends StatefulWidget {

  List<Team> teamsList;
  GeneralModel model;
  MyTeams(this.model,this.teamsList);

  @override
  _MyTeamsState createState() => new _MyTeamsState();
}

class _MyTeamsState extends State<MyTeams>{


  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
      padding: EdgeInsets.all(8),
      height: MediaQuery.of(context).size.height,
      child: new Stack(
        alignment: Alignment.bottomCenter,
        children: [
          new Container(
            height: MediaQuery.of(context).size.height,
            padding: EdgeInsets.only(bottom: 52),
            child: new ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: widget.teamsList.length,
                itemBuilder: (BuildContext context, int index) {
                  return new TeamItemAdapter(widget.model,widget.teamsList[index],false,index);
                }
            ),
          ),

          new Container(
            width: 180,
              height: 50,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Themecolor,
                  elevation: 0.5,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(40),
                  ),
                ),
                onPressed: () {
                  widget.model.teamId = 0;
                  navigateToCreateTeam(context, widget.model);
                },
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                  child: Text(
                    'Create Team (' + (widget.teamsList.length + 1).toString() + ')',
                    style: TextStyle(fontSize: 14, color: Colors.white),
                  ),
                ),
              )
          ),
        ],
      ),
    );
  }
  void selectTeam(){

  }
}