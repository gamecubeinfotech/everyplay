import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/dataModels/SelectedPlayer.dart';
import 'package:EverPlay/repository/model/player_list_response.dart';
import 'package:EverPlay/repository/model/team_response.dart';


class CreateTeamPager extends StatefulWidget {
  List<Player> list;
  Function onPlayerClick;
  int type;
  SelectedPlayer selectedPlayer;
  Limit limit;
  int fantasyType;
  int slotId;
  bool exeedCredit;
  String? matchKey;
  String? sportKey;
  CreateTeamPager(this.list,this.onPlayerClick,this.type,this.selectedPlayer,this.limit,this.fantasyType,this.slotId,this.exeedCredit,this.matchKey,this.sportKey);

  @override
  _CreateTeamPagerState createState() => new _CreateTeamPagerState();
}

class _CreateTeamPagerState extends State<CreateTeamPager> {

  bool isAnnounced=true;
  bool isLastPlayed=false;
  double alpha=1.0;

  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return new Container(
        color: Colors.white,
        child: new Column(
          children: [
            new Expanded(
              child: new ListView.builder(
                  padding: EdgeInsets.only(bottom: 70),
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: widget.list.length,
                  itemBuilder: (BuildContext context, int index) {
                    bool isSelected=widget.list[index].isSelected??false;
                    if (widget.list[index].team=="team1") {
                      if (widget.selectedPlayer.localTeamplayerCount == widget.limit.team_max_player) {
                        if (!isSelected)
                          alpha=0.3;
                        else
                          alpha=1.0;
                      } else {
                        checkList(index);
                      }

                    }
                    else if (widget.list[index].team=="team2") {

                      if (widget.selectedPlayer.visitorTeamPlayerCount == widget.limit.team_max_player) {
                        if (!isSelected)
                          alpha=0.3;
                        else
                          alpha=1.0;
                      } else {
                        checkList(index);
                      }
                    }
                    return new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: new Opacity(opacity: alpha,child: new Container(
                        margin: EdgeInsets.only(bottom: 5),
                        color: widget.list[index].isSelected??false?backgroundColor:TextFieldColor,
                        child: new Column(
                          children: [
                            new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Row(
                                  children: [
                                    new GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      child: new Container(
                                        margin: EdgeInsets.only(right: 5,top: 10),
                                        child: new Container(
                                          alignment: Alignment.bottomRight,
                                          width: 70,
                                          child: new Container(
                                            margin: EdgeInsets.only(bottom: 15),
                                            child: new Container(
                                              alignment: Alignment.bottomRight,
                                              child: CachedNetworkImage(
                                                width: 50,
                                                height: 50,
                                                imageUrl: widget.list[index].image!,
                                                placeholder: (context,url)=> new Image.asset(AppImages.defaultAvatarIcon),
                                                errorWidget: (context, url, error) => new Image.asset(AppImages.defaultAvatarIcon),
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      onTap: (){
                                        navigateToPlayerInfo(context,widget.matchKey,widget.list[index].id,widget.list[index].name,widget.list[index].team,widget.list[index].image,widget.list[index].isSelected,index,widget.type,widget.sportKey,widget.fantasyType,widget.slotId,'0',widget.onPlayerClick,widget.list[index].short_role!);
                                      },
                                    ),
                                    new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        new Text(
                                          widget.list[index].getShortName(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 13,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        widget.list[index].is_playing_show==1?new Row(
                                          children: [
                                            new Container(
                                              child: new Container(
                                                height: 6,
                                                width: 6,
                                                margin: EdgeInsets.only(top: 3,right: 3),
                                                alignment: Alignment.center,
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: widget.list[index].is_playing==1?yellowdark:Colors.red),
                                                    borderRadius:
                                                    BorderRadius.circular(3),
                                                    color: widget.list[index].is_playing==1?yellowdark:Colors.red),
                                                child: new Container(),
                                              ),
                                            ),
                                            widget.list[index].is_playing==1?new Container(
                                              margin: EdgeInsets.only(top: 3),
                                              child: new Text(
                                                'Announced',
                                                style: TextStyle(
                                                    color: greenColor,
                                                    fontSize: 11,
                                                    fontWeight: FontWeight.w400),
                                              ),
                                            ):new Container(),
                                          ],
                                        ):new Container(),
                                        widget.list[index].last_match==1?new Row(
                                          children: [
                                            new Container(
                                              child: new Container(
                                                height: 6,
                                                width: 6,
                                                margin: EdgeInsets.only(right: 3),
                                                alignment: Alignment.center,
                                                decoration: BoxDecoration(
                                                    border: Border.all(
                                                        color: darkBlue),
                                                    borderRadius:
                                                    BorderRadius.circular(3),
                                                    color: darkBlue),
                                                child: new Container(),
                                              ),
                                            ),
                                            new Container(
                                              margin: EdgeInsets.only(top: 2),
                                              child: new Text(
                                                widget.list[index].last_match_text!,
                                                style: TextStyle(
                                                    color: darkBlue,
                                                    fontSize: 11,
                                                    fontWeight: FontWeight.w400),
                                              ),
                                            ),
                                          ],
                                        ):new Container(),
                                        SizedBox(height: 10,),
                                        new Card(
                                          elevation: 0,
                                          margin: EdgeInsets.all(0),
                                          color: widget.list[index].team=='team1'?Colors.black:Colors.white,
                                          child: new Container(
                                            decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(5),
                                                border: widget.list[index].team=='team1'?Border.all(color: Colors.black):Border.all(color: primaryColor)
                                            ),
                                            height: 17,
                                            padding: EdgeInsets.only(left: 5,right: 5),
                                            // alignment: Alignment.center,
                                            child: Center(
                                              child: new Text(
                                                widget.list[index].teamcode!,
                                                style: TextStyle(fontSize: 10,fontWeight: FontWeight.normal,color: widget.list[index].team=='team1'?Colors.white:primaryColor,),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                new Row(
                                  children: [
                                    new Container(
                                      width: 50,
                                      alignment: Alignment.centerLeft,
                                      child: Text(
                                        widget.list[index].series_points,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 14),
                                      ),
                                    ),

                                    new Row(
                                      children: [
                                        new Container(
                                           margin: EdgeInsets.only(left: 10),
                                          width: 50,
                                          alignment: Alignment.center,
                                          child: Text(
                                            double.parse(widget.list[index].credit).toString(),
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 14),
                                          ),
                                        ),
                                        new Container(
                                           margin: EdgeInsets.only(left: 5,right: 10),
                                          child: new Container(
                                            child: new Container(
                                              alignment: Alignment.bottomRight,
                                              height: 20,
                                              width: 20,
                                              child: new Image.asset(widget.list[index].isSelected??false?AppImages.removePlayerIcon:AppImages.addPlayerIcon,color: widget.list[index].isSelected??false?Themecolor:yellowdark,
                                                  fit: BoxFit.fill),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),),
                      onTap: (){
                        if (widget.fantasyType == AppConstants.BOWLING_FANTASY_TYPE && widget.type == 1)
                          return;
                        bool isSelected=widget.list[index].isSelected??false;
                        widget.onPlayerClick(!isSelected,index,widget.type);
                      },
                    );
                  }
              ),
            )
          ],
        ),
    );
  }
  void checkList(int position) {
    bool isSelected=widget.list[position].isSelected??false;
    if (widget.type == 1) {
      if (widget.fantasyType == AppConstants.BOWLING_FANTASY_TYPE) {
          alpha=0.3;
      } else if (widget.selectedPlayer.wk_selected == widget.selectedPlayer.wk_max_count) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else if (widget.selectedPlayer.wk_selected >= widget.selectedPlayer.wk_min_count && widget.selectedPlayer.extra_player == 0) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else if (widget.exeedCredit) {
        if (!isSelected)
          alpha=1.0;
        else if (widget.limit.total_credits! - widget.selectedPlayer.total_credit >= widget.selectedPlayer.total_credit + double.parse(widget.list[position].credit))
          alpha=1.0;
        else
          alpha=0.3;
      } else if (double.parse(widget.list[position].credit)> (widget.limit.total_credits! - widget.selectedPlayer.total_credit)) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else {
          alpha=1.0;
      }
    }
    else if (widget.type == 3) {
      if (widget.selectedPlayer.ar_selected == widget.selectedPlayer.ar_maxcount) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else if (widget.selectedPlayer.ar_selected >= widget.selectedPlayer.ar_mincount && widget.selectedPlayer.extra_player == 0) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else if (widget.exeedCredit) {
        if (!isSelected)
          alpha=1.0;
        else if (widget.limit.total_credits! - widget.selectedPlayer.total_credit >= widget.selectedPlayer.total_credit + double.parse(widget.list[position].credit))
          alpha=1.0;
        else
          alpha=0.3;
      } else if (double.parse(widget.list[position].credit) > (widget.limit.total_credits! - widget.selectedPlayer.total_credit)) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else {
          alpha=1.0;
      }
    }
    else if (widget.type == 2) {
      if (widget.selectedPlayer.bat_selected == widget.selectedPlayer.bat_maxcount) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else if (widget.selectedPlayer.bat_selected >= widget.selectedPlayer.bat_mincount && widget.selectedPlayer.extra_player == 0) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else if (widget.exeedCredit) {
        if (!isSelected)
          alpha=1.0;
        else if (widget.limit.total_credits! - widget.selectedPlayer.total_credit >= widget.selectedPlayer.total_credit + double.parse(widget.list[position].credit))
          alpha=1.0;
        else
          alpha=0.3;
      } else if (double.parse(widget.list[position].credit) > (widget.limit.total_credits! - widget.selectedPlayer.total_credit)) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else {
          alpha=1.0;
      }
    }
    else if (widget.type == 4) {
      if (widget.selectedPlayer.bowl_selected == widget.selectedPlayer.bowl_maxcount) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else if (widget.selectedPlayer.bowl_selected >= widget.selectedPlayer.bowl_mincount && widget.selectedPlayer.extra_player == 0) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else if (widget.exeedCredit) {
        if (!isSelected)
          alpha=1.0;
        else if (widget.limit.total_credits! - widget.selectedPlayer.total_credit >= widget.selectedPlayer.total_credit + double.parse(widget.list[position].credit))
          alpha=1.0;
        else
          alpha=0.3;
      } else if (double.parse(widget.list[position].credit) > (widget.limit.total_credits! - widget.selectedPlayer.total_credit)) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else {
          alpha=1.0;
      }
    }else if (widget.type == 5) {
      if (widget.selectedPlayer.c_selected == widget.selectedPlayer.c_max_count) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else if (widget.selectedPlayer.c_selected >= widget.selectedPlayer.c_min_count && widget.selectedPlayer.extra_player == 0) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      } else if (widget.exeedCredit) {
        if (isSelected)
          alpha=1.0;
        else if (100 - widget.selectedPlayer.total_credit >= widget.selectedPlayer.total_credit + double.parse(widget.list[position].credit))
          alpha=1.0;
        else
          alpha=0.3;
      } else if ( double.parse(widget.list[position].credit) > (100 - widget.selectedPlayer.total_credit)) {
        if (!isSelected)
          alpha=0.3;
        else
          alpha=1.0;
      }else {
        alpha=1.0;
      }
    }
  }
}