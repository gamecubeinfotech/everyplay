
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/player_info_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class PlayerInfo extends StatefulWidget {
  String? matchKey,playerName,team,playerImage,sportKey;
  int? playerId,type,fantasyType,slotId;
  int index;
  bool? isSelected;
  String isFrom;
  String shorRole;
  Function? onPlayerClick;
  PlayerInfo(this.matchKey,this.playerId,this.playerName,this.team,this.playerImage,this.isSelected,this.index,this.type,this.sportKey,this.fantasyType,this.slotId,this.isFrom,this.onPlayerClick,this.shorRole);
  @override
  _PlayerInfoState createState() => new _PlayerInfoState();
}

class _PlayerInfoState extends State<PlayerInfo>{
  late String userId='0';
  PlayerInfoResult? playerInfoResult;
  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getPLayerInfo();
      })
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Themecolor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
          body: new WillPopScope(
              child: new Stack(
                children: [
                  new Scaffold(
                    backgroundColor: Themecolor,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(55), // Set this height
                      child: Container(
                        color: Colors.black,
                        padding: EdgeInsets.only(top: 15,bottom: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 8),
                              child: new Row(
                                children: [
                                  new GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    onTap: () => {_onWillPop()},
                                    child: CircleAvatar(

                                        radius: 10,
                                        backgroundColor: Colors.white,
                                        child: Icon(Icons.clear,color: Colors.black,size: 16,)),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(left: 15),
                                    child: new Text("Player Info",
                                        style: TextStyle(
                                            fontFamily: AppConstants.textBold,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14)),
                                  ),
                                  //SizedBox(width: 10,),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    body: (playerInfoResult)!=null?new Container(
                      alignment: Alignment.topCenter,
                      color: Themecolor,
                      child: new Column(
                        children: [

                          new Container(
                            margin: EdgeInsets.only(left: 10,right: 15,top: 20),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [


                                new Container(
                                  child: CachedNetworkImage(
                                    width: 60,
                                    height: 60,
                                    imageUrl: widget.playerImage!,
                                    placeholder: (context,url)=> new Image.asset(AppImages.defaultAvatarIcon),
                                    errorWidget: (context, url, error) => new Image.asset(AppImages.defaultAvatarIcon),
                                    fit: BoxFit.fill,
                                  ),
                                ),
                               /* new Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: new Text(widget.playerName!,
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18)),
                                ),*/

                                new Container(
                                  margin: EdgeInsets.only(left: 20,right: 15),
                                  alignment: Alignment.center,
                                  child: IntrinsicHeight(
                                    child: new Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        new Container(
                                          child: new Column(
                                            children: [
                                              new Container(
                                                child: new Text("CREDITS",
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12)),
                                              ),
                                              new Container(
                                                margin: EdgeInsets.only(top: 10),
                                                child: new Text(playerInfoResult!.playercredit.toString(),
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.w800,
                                                        fontSize: 20)),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(left: 8.0,right: 8),
                                          child: VerticalDivider(
                                            thickness: 1,
                                            width: 20,
                                            color: Colors.white,
                                          ),
                                        ),
                                        new Container(
                                          child: new Column(
                                            children: [
                                              new Container(
                                                child: new Text("TOTAL POINTS",
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12)),
                                              ),
                                              new Container(
                                                margin: EdgeInsets.only(top: 10),
                                                child: new Text(playerInfoResult!.playerpoints.toString(),
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.w800,
                                                        fontSize: 20)),
                                              ),


                                            ],
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),
                                ),


                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 16.0),
                            child: new Divider(height: 1,thickness: 1,color: Colors.white,),
                          ),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(left: 12),
                                    child: new Text(widget.playerName!+" (${widget.shorRole})",
                                        style: TextStyle(
                                            // fontFamily: AppConstants.textBold,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 18)),
                                  ),


                                  Container(
                                    margin: EdgeInsets.only(left: 14,top: 4,bottom: 10),
                                    child: new Text(playerInfoResult!.battingstyle??"-",
                                          style: TextStyle(
                                              // fontFamily: AppConstants.textBold,
                                              color: Colors.white,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14)),
                                  ),



                                ],
                              ),
                              widget.isFrom=='0'?new Container(
                                margin: EdgeInsets.only(right: 12,bottom: 14),
                                  height: 35,
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      padding: EdgeInsets.only(left: 20, right: 20),
                                      primary: widget.isSelected ?? false ? Colors.white : yellowdark,
                                      elevation: 0.5,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40),
                                      ),
                                    ),
                                    onPressed: () {
                                      if (widget.fantasyType == AppConstants.BOWLING_FANTASY_TYPE && widget.type == 1) {
                                        return;
                                      }
                                      bool isSelected = widget.isSelected ?? false;
                                      Navigator.of(context).pop();
                                      widget.onPlayerClick!(!isSelected, widget.index, widget.type);
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(vertical: 10),
                                      child: Text(
                                        widget.isSelected ?? false ? 'Remove' : 'Add in Team',
                                        style: TextStyle(
                                            fontSize: 14,
                                            color: widget.isSelected ?? false ? Colors.black : Colors.white),
                                      ),
                                    ),
                                  )
                              ):Container(),
                            ],
                          ),

                      /*    new Container(
                            margin: EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 10),
                            child: new Card(
                              child: new Container(
                                // padding: EdgeInsets.all(10),
                                child: IntrinsicHeight(
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      new Flexible(child: new Container(
                                        margin: EdgeInsets.only(left: 5),
                                        alignment: Alignment.topLeft,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: new Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              new Container(
                                                child: new Text("Bats",
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.grey,
                                                        fontWeight: FontWeight.w400,
                                                        fontSize: 14)),
                                              ),
                                              new Container(
                                                margin: EdgeInsets.only(top: 10),
                                                child: new Text(playerInfoResult!.battingstyle??"",
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 14)),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),flex: 1,),
                                      VerticalDivider(
                                        thickness: .5,
                                        width: 20,

                                        color: Colors.grey,
                                      ),
                                      new Flexible(child: new Container(
                                        alignment: Alignment.topLeft,
                                        margin: EdgeInsets.only(left: 5),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: new Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              new Container(
                                                child: new Text("Bowl",
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.grey,
                                                        fontWeight: FontWeight.w400,
                                                        fontSize: 14)),
                                              ),
                                              new Container(
                                                margin: EdgeInsets.only(top: 10),
                                                child: new Text(playerInfoResult!.bowlingstyle??"",
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 14)),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),flex: 1,),

                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),*/
                          new Expanded(child: new Container(
                            height: MediaQuery.of(context).size.height,
                            color: Colors.white,
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(top: 15,bottom: 15,left: 10),
                                  child: new Text("Match wise Fantasy Stats",
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14)),
                                ),
                                // new Divider(height: 1,thickness: 1,),
                                new Container(
                                  height: 30,
                                  color: TextFieldColor,
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          'Match',
                                          style: TextStyle(
                                              color: textGrey,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12),
                                        ),
                                      ),
                                      new Row(
                                        children: [
                                          new Container(
                                            width: 50,
                                            alignment: Alignment.centerLeft,
                                            child: Text(
                                              'Points',
                                              style: TextStyle(
                                                  color: textGrey,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 12),
                                            ),
                                          ),
                                          new Container(
                                            margin: EdgeInsets.only(right: 10,left: 0),
                                            width: 100,
                                            alignment: Alignment.center,
                                            child: Text(
                                              'Selected By',
                                              style: TextStyle(
                                                  color: textGrey,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 12),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                // new Divider(height: 1,thickness: 1,),
                                new Expanded(
                                  child: new ListView.builder(
                                      shrinkWrap: true,
                                      scrollDirection: Axis.vertical,
                                      itemCount: playerInfoResult!.matches==null?0:playerInfoResult!.matches!.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return new Column(
                                          children: [
                                            new Container(
                                              padding: EdgeInsets.only(left: 10,right: 0,top: 10,bottom: 10),
                                              child: new Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  new Row(
                                                    children: [
                                                      new Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          new Text(
                                                            playerInfoResult!.matches![index].short_name!,
                                                            style: TextStyle(
                                                                color: Colors.black,
                                                                fontSize: 14,
                                                                fontWeight: FontWeight.w500),
                                                          ),
                                                          new Container(
                                                            margin: EdgeInsets.only(top: 2),
                                                            child: new Text(
                                                              playerInfoResult!.matches![index].matchdate!,
                                                              style: TextStyle(
                                                                  color: Colors.grey,
                                                                  fontSize: 12,
                                                                  fontWeight: FontWeight.w400),
                                                            ),
                                                          ),
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                  new Row(
                                                    children: [
                                                      new Container(
                                                        width: 50,
                                                        alignment: Alignment.centerLeft,
                                                        child: Text(
                                                          playerInfoResult!.matches![index].total_points.toString(),
                                                          style: TextStyle(
                                                              color: Colors.grey,
                                                              fontWeight: FontWeight.w400,
                                                              fontSize: 12),
                                                        ),
                                                      ),
                                                      new Container(
                                                        margin: EdgeInsets.only(right: 10,left: 0),
                                                        width: 100,
                                                        alignment: Alignment.center,
                                                        child: Text(
                                                          playerInfoResult!.matches![index].selectper!,
                                                          style: TextStyle(
                                                              color: Colors.grey,
                                                              fontWeight: FontWeight.w400,
                                                              fontSize: 12),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            new Divider(height: 1,thickness: 1,),
                                          ],
                                        );
                                      }
                                  ),
                                ),
                              ],
                            ),
                          ))
                        ],
                      ),
                    ):Container(),
                  ),
                ],
              ),
              onWillPop: _onWillPop)),
    );
  }
  void getPLayerInfo() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest generalRequest = new GeneralRequest(user_id: userId,matchkey: widget.matchKey,playerid:widget.playerId.toString(),sport_key: widget.sportKey,fantasy_type: widget.fantasyType.toString(),slotes_id: widget.slotId.toString());
    final client = ApiClient(AppRepository.dio);
    PLayerInfoResponse pLayerInfoResponse = await client.getPlayerInfo(generalRequest);
    if (pLayerInfoResponse.status == 1) {
      AppLoaderProgress.hideLoader(context);
      playerInfoResult = pLayerInfoResponse.result!;
    }
    setState(() {

    });
  }

  Future<bool> _onWillPop() async {
    Navigator.pop(context);
    return Future.value(false);
  }
}
