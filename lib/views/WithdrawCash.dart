import 'package:EverPlay/repository/model/get_withdraw_tds.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:intl/intl.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/bank_details_response.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/withdraw_amount_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class WithdrawCash extends StatefulWidget {
  String type = 'bank';
  WithdrawCash({type});

  @override
  _WithdrawCashState createState() => new _WithdrawCashState();
}

class _WithdrawCashState extends State<WithdrawCash> {
  TextEditingController amountController = TextEditingController();
  TextEditingController creditAmountController = TextEditingController();
  String winningAmount = '0';
  String mobile = '0';
  String userId = '0';
  String val = 'bank';
  String gatewayCharges = 'Gateway charges 1.99% + 18%';
  BankDetailItem bankDetailItem = new BankDetailItem();
  @override
  void initState() {
    super.initState();
    val = widget.type;
    AppPrefrence.getString(AppConstants.KEY_USER_WINING_AMOUNT)
        .then((value) => {
              setState(() {
                winningAmount = value;
              })
            });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_MOBILE)
        .then((value) => {
              setState(() {
                mobile = value;
              })
            });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) => {
              setState(() {
                userId = value;
                getBankDetail();
              })
            });
    amountController.addListener(() {

      setState(() {
        if (amountController.text.length > 0) {
          double amount = double.parse(amountController.text.toString());
          double gatewayCharge = (amount * 1.99) / 100;
          double gstCharge = (gatewayCharge * 18) / 100;
          double deductionCharge = gatewayCharge + gstCharge;
          gatewayCharges = "Gateway charges 1.99% + 18% = " +
              (new NumberFormat("##.##").format(deductionCharge));
          creditAmountController.text =
              new NumberFormat("##.##").format(amount - deductionCharge);
        } else {
          creditAmountController.text = '';
          gatewayCharges = "Gateway charges 1.99% + 18%";
        }
      });
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: Themecolor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.light) /* set Status bar icon color in iOS. */
        );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: Colors.black,
            // padding: EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Withdraw',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Stack(
          children: [
            new Container(
              height: MediaQuery.of(context).size.height,
              child: new SingleChildScrollView(
                child: new Container(
                  child: new Column(
                    children: [
                      new Container(
                        padding:
                            EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                        color: Themecolor,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Container(
                              alignment: Alignment.center,
                              child: Text(
                                'YOUR TOTAL WINNINGS',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                            ),
                            new Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 15, vertical: 5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  border: Border.all(
                                    color: Colors.white,
                                    width: 1,
                                  )),
                              alignment: Alignment.center,
                              child: Text(
                                '₹' + winningAmount,
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.white,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.normal,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.all(15),
                        decoration: BoxDecoration(
                            color: backgroundColor,
                            borderRadius: BorderRadius.circular(10)),
                        child: new Container(
                          padding: EdgeInsets.all(10),
                          child: new Row(
                            children: [
                              new Container(
                                margin: EdgeInsets.only(right: 10),
                                padding: EdgeInsets.all(5),
                                height: val == 'paytm_instant' ? 30 : 50,
                                width: 50,
                                child: new Image.asset(
                                    val == 'paytm_instant'
                                        ? AppImages.paytmIcon
                                        : AppImages.bankIcon,
                                    fit: BoxFit.fill),
                              ),
                              new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  new Text(
                                    val == 'paytm_instant'
                                        ? 'PAYTM WALLET'
                                        : bankDetailItem.bankname == null
                                            ? ''
                                            : bankDetailItem.bankname!,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w600),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: new Text(
                                      val == 'paytm_instant'
                                          ? mobile
                                          : bankDetailItem.accno == null
                                              ? ''
                                              : "A/C " + bankDetailItem.accno!,
                                      style: TextStyle(
                                          color: Themecolor,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only(left: 15, right: 15),
                        child: new Column(
                          children: [
                            new Container(
                              padding: EdgeInsets.only(bottom: 10),
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Add Withdraw Amount',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontFamily: 'roboto',
                                  fontSize: 14,
                                  color: textGrey,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            new Container(
                              child: new Stack(
                                alignment: Alignment.centerRight,
                                children: [
                                  new Container(
                                    height: 45,
                                    child: TextField(
                                      controller: amountController,
                                      keyboardType: TextInputType.number,
                                      maxLength: 6,
                                      decoration: InputDecoration(
                                        counterText: "",
                                        fillColor: Colors.white,
                                        filled: true,
                                        prefixIcon: new Container(
                                          child: Center(
                                            widthFactor: 0.0,
                                            child: Text('₹',
                                                style: TextStyle(
                                                  fontSize: 15,
                                                  color: textGrey,
                                                  fontStyle: FontStyle.normal,
                                                  fontWeight: FontWeight.bold,
                                                )),
                                          ),
                                        ),
                                        hintStyle: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                        ),
                                        hintText: 'Enter Amount',
                                        enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          // width: 0.0 produces a thin "hairline" border
                                          borderSide: BorderSide(
                                              color: bordercolor, width: 0.0),
                                        ),
                                        focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          borderSide: BorderSide(
                                              color: bordercolor, width: 0.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      val == 'paytm_instant'
                          ? new Container(
                              margin:
                                  EdgeInsets.only(left: 15, right: 15, top: 20),
                              child: new Column(
                                children: [
                                  new Container(
                                    padding: EdgeInsets.only(bottom: 10),
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      'Amount to be credited',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: 'roboto',
                                        fontSize: 14,
                                        color: Colors.black,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    child: new Stack(
                                      alignment: Alignment.centerRight,
                                      children: [
                                        new Container(
                                          height: 45,
                                          child: TextField(
                                            controller: creditAmountController,
                                            keyboardType: TextInputType.number,
                                            maxLength: 6,
                                            decoration: InputDecoration(
                                              counterText: "",
                                              fillColor: Colors.white,
                                              filled: true,
                                              prefixIcon: new Container(
                                                child: Center(
                                                  widthFactor: 0.0,
                                                  child: Text('₹',
                                                      style: TextStyle(
                                                        fontSize: 15,
                                                        color: Colors.black,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      )),
                                                ),
                                              ),
                                              hintStyle: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey,
                                                fontStyle: FontStyle.normal,
                                                fontWeight: FontWeight.normal,
                                              ),
                                              hintText: 'Enter Amount',
                                              enabledBorder:
                                                  const OutlineInputBorder(
                                                // width: 0.0 produces a thin "hairline" border
                                                borderSide: const BorderSide(
                                                    color: Colors.grey,
                                                    width: 0.0),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderSide: const BorderSide(
                                                    color: Colors.grey,
                                                    width: 0.0),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    alignment: Alignment.centerLeft,
                                    padding: EdgeInsets.only(top: 3),
                                    child: Text(
                                      gatewayCharges,
                                      style: new TextStyle(
                                          fontFamily: AppConstants.textRegular,
                                          fontSize: 12.0,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Container(),
                      val == 'paytm_instant'
                          ? new Container(
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(
                                  left: 15, top: 5, bottom: 5, right: 15),
                              child: Text(
                                "Paytm Instant min:- " +
                                    AppConstants.MIN_PAYTM_WITHDRAW_AMOUNT
                                        .toString() +
                                    " max:- " +
                                    AppConstants.MAX_PAYTM_WITHDRAW_AMOUNT
                                        .toString(),
                                style: new TextStyle(
                                    fontFamily: AppConstants.textRegular,
                                    fontSize: 13.0,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w400),
                              ),
                            )
                          : val == 'bank_instant'
                              ? new Container(
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(
                                      left: 15, top: 5, bottom: 5, right: 15),
                                  child: Text(
                                    "Bank Instant min:- " +
                                        AppConstants
                                            .MIN_BANK_INSTANT_WITHDRAW_AMOUNT
                                            .toString() +
                                        " , Max:- " +
                                        AppConstants
                                            .MAX_BANK_INSTANT_WITHDRAW_AMOUNT
                                            .toString(),
                                    style: new TextStyle(
                                        fontFamily: AppConstants.textRegular,
                                        fontSize: 13.0,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w400),
                                  ),
                                )
                              : new Container(
                                  alignment: Alignment.centerLeft,
                                  padding: EdgeInsets.only(
                                      left: 15, top: 10, bottom: 5, right: 15),
                                  child: Row(
                                    children: [
                                      Image.asset(
                                        'assets/images/notice.png',
                                        height: 20,
                                        width: 20,
                                      ),
                                      SizedBox(
                                        width: 10,
                                      ),
                                      Text(
                                        "Min ₹" +
                                            AppConstants
                                                .MIN_BANK_WITHDRAW_AMOUNT
                                                .toString() +
                                            " & max ₹" +
                                            AppConstants
                                                .MAX_BANK_WITHDRAW_AMOUNT
                                                .toString() +
                                            " allowed per day",
                                        style: new TextStyle(
                                            fontFamily:
                                                AppConstants.textRegular,
                                            fontSize: 13.0,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ],
                                  ),
                                ),

                      // new Container(
                      //   alignment: Alignment.centerLeft,
                      //   padding: EdgeInsets.only(left: 15,bottom: 5,right: 15),
                      //   child: Text(
                      //     val=='paytm_instant'?'After full verification we started max 10k withdrawal in paytm instant':val=='bank_instant'?'After full verification we started max 50k withdrawal in bank instant':'After full verification we started max 200k withdrawal in bank',
                      //     style: new TextStyle(
                      //         fontFamily: AppConstants.textRegular,
                      //         fontSize: 14.0,
                      //         height: 1.5,
                      //         color: Colors.grey,
                      //         fontWeight: FontWeight.w400),
                      //   ),
                      // ),

                      // Column(
                      //   crossAxisAlignment: CrossAxisAlignment.start,
                      //   children: <Widget>[
                      //     new GestureDetector(
                      //       behavior: HitTestBehavior.translucent,
                      //       child: new Container(
                      //         margin: EdgeInsets.only(top: 10,left: 5),
                      //         child: new Row(
                      //           children: [
                      //             new Container(
                      //               height: 20,
                      //               width: 35,
                      //               child: Radio(
                      //                 value: 'paytm_instant',
                      //                 groupValue: val,
                      //                 activeColor: Colors.black,
                      //                 onChanged: (value) {
                      //                   setState(() {
                      //                     val = value.toString();
                      //                   });
                      //                 },
                      //               ),
                      //             ),
                      //             Text(
                      //               'Paytm Wallet',
                      //               style: new TextStyle(
                      //                   fontSize: 14.0,
                      //                   color: Colors.black,
                      //                   fontWeight: FontWeight.w400),
                      //             ),
                      //           ],
                      //         ),
                      //       ),
                      //       onTap: (){
                      //         setState(() {
                      //           val='paytm_instant';
                      //         });
                      //       },
                      //     ),
                      //     val=='paytm_instant'?new Container(
                      //       alignment: Alignment.center,
                      //       margin: EdgeInsets.only(left: 30),
                      //       child: new Html(data: "Only 1 withdrawal / transaction allowed per day. Minimum Rs. " + AppConstants.MIN_PAYTM_WITHDRAW_AMOUNT.toString()
                      //           + " & maximum Rs." + AppConstants.MAX_PAYTM_WITHDRAW_AMOUNT.toString() + " can be withdraw per day, Gateway charges 1.99% + GST applicable and will be"
                      //           + " deducted from withdrawal amount. If transaction failed for any reason / no PayTM wallet for registered mobile number / "
                      //           + "technical issue from PayTm, withdrawal amount will be credited back to users mirchi11 wallet within 7 to 10 working days, For "
                      //           + "instant withdrawal, your registered mobile number should have PayTm wallet account to withdraw instantly. mirchi11 has solely rights to"
                      //           + " withdraw this feature anytime.",
                      //         style: {
                      //           'html': Style(textAlign: TextAlign.start,color: Colors.grey,textDecoration: TextDecoration.none,
                      //               fontWeight: FontWeight.normal,lineHeight:LineHeight(1.5) )
                      //         },
                      //       ),
                      //     ):Container(),
                      //     new GestureDetector(
                      //       behavior: HitTestBehavior.translucent,
                      //       child: new Container(
                      //         margin: EdgeInsets.only(top: 15,left: 5),
                      //         child: new Row(
                      //           children: [
                      //             new Container(
                      //               height: 20,
                      //               width: 35,
                      //               child: Radio(
                      //                 value: 'bank',
                      //                 activeColor: Colors.black,
                      //                 groupValue: val,
                      //                 onChanged: (value) {
                      //                   setState(() {
                      //                     val = value.toString();
                      //                   });
                      //                 },
                      //               ),
                      //             ),
                      //             Text(
                      //               'Bank',
                      //               style: new TextStyle(
                      //                   fontSize: 14.0,
                      //                   color: Colors.black,
                      //                   fontWeight: FontWeight.w400),
                      //             ),
                      //           ],
                      //         ),
                      //       ),
                      //       onTap: (){
                      //         setState(() {
                      //           val='bank';
                      //         });
                      //       },
                      //     ),
                      //     new GestureDetector(
                      //       behavior: HitTestBehavior.translucent,
                      //       child: new Container(
                      //         margin: EdgeInsets.only(top: 15,left: 5),
                      //         child: new Row(
                      //           children: [
                      //             new Container(
                      //               height: 20,
                      //               width: 35,
                      //               child: Radio(
                      //                 value: 'bank_instant',
                      //                 activeColor: Colors.black,
                      //                 groupValue: val,
                      //                 onChanged: (value) {
                      //                   setState(() {
                      //                     val = value.toString();
                      //                   });
                      //                 },
                      //               ),
                      //             ),
                      //             Text(
                      //               'Instant Withdrawal (Bank)',
                      //               style: new TextStyle(
                      //                   fontSize: 14.0,
                      //                   color: Colors.black,
                      //                   fontWeight: FontWeight.w400),
                      //             ),
                      //           ],
                      //         ),
                      //       ),
                      //       onTap: (){
                      //         setState(() {
                      //           val='bank_instant';
                      //         });
                      //       },
                      //     ),
                      //     val=='bank_instant'?new Container(
                      //       alignment: Alignment.center,
                      //       margin: EdgeInsets.only(left: 30),
                      //       child: new Html(data: "<b>Important Note</b>" + " (Imps)<br><br>" +
                      //           "&#8226;" + " Maximum withdrawal of Rs. 50,000 is allowed per day.<br>" +
                      //           "&#8226;" + " For withdrawal of any amount flat Rs. 10 per transaction charge will be levied<br>" +
                      //           "&#8226;" + " To use this feature User must get his KYC verified on mirchi11.<br>" +
                      //           "&#8226;" + " Only one transaction can be made in 24 hrs.<br>",
                      //         style: {
                      //           'html': Style(textAlign: TextAlign.start,color: Colors.grey,textDecoration: TextDecoration.none,
                      //               fontWeight: FontWeight.normal,lineHeight:LineHeight(1.5) )
                      //         },
                      //       ),
                      //     ):Container(),
                      //   ],
                      // ),
                      new Container(
                          width: MediaQuery.of(context).size.width,
                          height: 45,
                          margin: EdgeInsets.fromLTRB(40, 25, 40, 15),
                          child: ElevatedButton(
                            onPressed: () {
                              if (val == "paytm_instant") {
                                if (amountController.text.isEmpty ||
                                    int.parse(amountController.text) <
                                        AppConstants.MIN_PAYTM_WITHDRAW_AMOUNT)
                                  MethodUtils.showError(
                                      context,
                                      "Please enter minimum ₹ " +
                                          AppConstants.MIN_PAYTM_WITHDRAW_AMOUNT
                                              .toString() +
                                          " balance");
                                else if (int.parse(amountController.text) >
                                    AppConstants.MAX_PAYTM_WITHDRAW_AMOUNT)
                                  MethodUtils.showError(
                                      context,
                                      "You can withdrawal up to " +
                                          AppConstants.MAX_PAYTM_WITHDRAW_AMOUNT
                                              .toString() +
                                          " from paytm instantly");
                                else if (double.parse(amountController.text) >
                                    double.parse(winningAmount))
                                  MethodUtils.showError(
                                      context, "Insufficient Fund");
                                else {
                                  withdrawUserBalance();
                                }
                              } else if (val == "bank_instant") {
                                if (amountController.text.isEmpty ||
                                    int.parse(amountController.text) <
                                        AppConstants
                                            .MIN_BANK_INSTANT_WITHDRAW_AMOUNT)
                                  MethodUtils.showError(
                                      context,
                                      "Please enter minimum ₹ " +
                                          AppConstants
                                              .MIN_BANK_INSTANT_WITHDRAW_AMOUNT
                                              .toString() +
                                          " balance");
                                else if (int.parse(amountController.text) >
                                    AppConstants
                                        .MAX_BANK_INSTANT_WITHDRAW_AMOUNT)
                                  MethodUtils.showError(
                                      context,
                                      "You can withdrawal up to " +
                                          AppConstants
                                              .MAX_BANK_INSTANT_WITHDRAW_AMOUNT
                                              .toString() +
                                          " from bank instantly");
                                else if (double.parse(amountController.text) >
                                    double.parse(winningAmount))
                                  MethodUtils.showError(
                                      context, "Insufficient Fund");
                                else {
                                  getTdsDetail();
                                  // withdrawUserBalance();
                                }
                              } else {
                                if (amountController.text.isEmpty ||
                                    int.parse(amountController.text) <
                                        AppConstants.MIN_BANK_WITHDRAW_AMOUNT)
                                  MethodUtils.showError(
                                      context,
                                      "Please enter minimum ₹ " +
                                          AppConstants.MIN_BANK_WITHDRAW_AMOUNT
                                              .toString() +
                                          " balance");
                                else if (int.parse(amountController.text) >
                                    AppConstants.MAX_BANK_WITHDRAW_AMOUNT)
                                  MethodUtils.showError(
                                      context,
                                      "You can withdrawal up to " +
                                          AppConstants.MAX_BANK_WITHDRAW_AMOUNT
                                              .toString() +
                                          " from bank");
                                else if (double.parse(amountController.text) >
                                    double.parse(winningAmount))
                                  MethodUtils.showError(
                                      context, "Insufficient Fund");
                                else {
                                  getTdsDetail();
                                  // withdrawUserBalance();
                                }
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              primary: Themecolor,
                              elevation: 0.5,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(40),
                              ),
                            ),
                            child: Text(
                              'Withdraw Now',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                              ),
                            ),
                          ))
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }


  void getTdsDetail() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,amount: amountController.text.toString(),payment_type: 'bank');
    final client = ApiClient(AppRepository.dio);
    TdsWithdrawResponse withdrawResponse =
    await client.getWithdrawTds(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (withdrawResponse.status == 1) {
      TdsWithdrawItem tdsWithdrawItem= withdrawResponse.result!;
      withdrawNow(tdsWithdrawItem);
    }
  }


  void getBankDetail() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    BankDetailResponse referBonusListResponse =
        await client.getBankDetails(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (referBonusListResponse.status == 1) {
      bankDetailItem = referBonusListResponse.result![0];
      setState(() {});

    }
  }

  void withdrawUserBalance() async {
    FocusScope.of(context).unfocus();
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(
        user_id: userId,
        amount: amountController.text.toString(),
        payment_type: val);
    final client = ApiClient(AppRepository.dio);
    WithdrawResponse withdrawResponse =
        await client.withdrawUserBalance(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (withdrawResponse.status == 1) {
      if (withdrawResponse.result!.status == 1) {
        MethodUtils.showSuccess(context, withdrawResponse.result!.msg);
        AppPrefrence.putString(AppConstants.KEY_USER_WINING_AMOUNT,
            withdrawResponse.result!.wining.toString());
        AppPrefrence.putString(AppConstants.KEY_USER_BALANCE,
            withdrawResponse.result!.amount.toString());
      } else {
        MethodUtils.showError(context, withdrawResponse.result!.msg);
      }
      setState(() {});
    } else {
      MethodUtils.showError(context, withdrawResponse.message);
    }
  }


  String calculateTDS() {
    double amount = double.tryParse(amountController.text) ?? 0.0;
    double gstPercentage = 0.30; // 28% GST
    double calculatedGST = amount * gstPercentage;
    return calculatedGST.toStringAsFixed(2).replaceAll('.00', '');
  }

  String afterTDSAmount() {
    double amount = double.tryParse(amountController.text) ?? 0.0;
    return (amount- (0.3*amount)).toStringAsFixed(2).replaceAll('.00', '');
  }


  withdrawNow(TdsWithdrawItem withdrawItem) {
    return showModalBottomSheet(
        context: context,
        useSafeArea: true,
        backgroundColor: backgroundColor,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(25.0),
          ),
        ),
        builder: (context) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 10),
              Text(
                "₹ ${withdrawItem.amount}",
                style: new TextStyle(
                    fontSize: 25.0,
                    color: Themecolor,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(height: 5),
              Text(
                "Withdrawal ( After TDS )",
                style: new TextStyle(
                    fontSize: 22.0,
                    color: TextFieldTextColor,
                    fontWeight: FontWeight.w500),
              ),
              SizedBox(height: 10),
              Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(25)),
                child: Column(
                  children: [
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Text(
                                "Withdrawal Amount :",
                                style: new TextStyle(
                                    fontFamily: AppConstants.textRegular,
                                    fontSize: 16.0,
                                    color: textGrey,
                                    fontWeight: FontWeight.w800),
                              ),
                              Spacer(),
                              Text(
                                "₹ ${withdrawItem.actual_amount}",
                                style: new TextStyle(
                                    fontFamily: AppConstants.textRegular,
                                    fontSize: 16.0,
                                    color: textGrey,
                                    fontWeight: FontWeight.w800),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                          Row(
                            children: [
                              Text(
                                "TDS Applicable :",
                                style: new TextStyle(
                                    fontFamily: AppConstants.textRegular,
                                    fontSize: 16.0,
                                    color: textGrey,
                                    fontWeight: FontWeight.w800),
                              ),
                              Spacer(),
                              Text(
                                "₹ ${withdrawItem.tds_amount}",
                                style: new TextStyle(
                                    fontFamily: AppConstants.textRegular,
                                    fontSize: 16.0,
                                    color: textGrey,
                                    fontWeight: FontWeight.w800),
                              ),
                            ],
                          ),
                          SizedBox(height: 5),
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      height: 5,
                      color: TextFieldBorderColor,
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                      color: backgroundColor,
                      child: Row(
                        children: [
                          Text(
                            "Withdrawal ( After TDS ) :",
                            style: new TextStyle(

                                fontSize: 16.0,
                                color: Themecolor,
                                fontWeight: FontWeight.w500),
                          ),
                          Spacer(),
                          Text(
                            "₹ ${withdrawItem.amount}",
                            style: new TextStyle(

                                fontSize: 16.0,
                                color: Themecolor,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      height: 5,
                      color: TextFieldBorderColor,
                    ),
                    // Container(
                    //   padding:
                    //       EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    //   child: RichText(
                    //     text: TextSpan(
                    //       text: 'Government Rules: ',
                    //       style: TextStyle(
                    //         color: Colors.black,
                    //         fontWeight: FontWeight.bold,
                    //         fontSize: 13,
                    //       ),
                    //       children: [
                    //         TextSpan(
                    //           text:
                    //               'Lorem ipsum dolor sit amet cons ectetur. Gravida placerat justo ut orci consequatfa cilisi arcu. At tempor at nulla luctus iaculis.',
                    //           style: TextStyle(
                    //             fontWeight: FontWeight.normal,
                    //             color: textGrey,
                    //             fontSize: 12,
                    //           ),
                    //         ),
                    //       ],
                    //     ),
                    //   ),
                    // ),
                    SizedBox(height: 30),
                    Row(
                      children: [
                        Expanded(
                          child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 45,
                              margin: EdgeInsets.fromLTRB(10, 0, 10, 15),
                              child: ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                style: ElevatedButton.styleFrom(
                                  primary: Colors.white,
                                  elevation: 0.5,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(40),
                                      side: BorderSide(color: textGrey,width: .5)
                                  ),
                                ),
                                child: Text(
                                  'Cancel',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: textGrey,
                                  ),
                                ),
                              )),
                        ),
                        Expanded(
                          child: Container(
                              width: MediaQuery.of(context).size.width,
                              height: 45,
                              margin: EdgeInsets.fromLTRB(10, 0, 10, 15),
                              child: ElevatedButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                  withdrawUserBalance();
                                },

                                style: ElevatedButton.styleFrom(
                                  primary: Themecolor,
                                  elevation: 0.5,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(40),
                                  ),
                                ),
                                child: Text(
                                  'Confirm',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                  ),
                                ),
                              )),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
            ],
          );
        });
  }
}
