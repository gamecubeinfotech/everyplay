import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/earn_contests_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class EarnContests extends StatefulWidget {
  String matchKey;
  String shortName;

  EarnContests(this.matchKey,this.shortName);

  @override
  _EarnContestsState createState() => new _EarnContestsState();
}

class _EarnContestsState extends State<EarnContests>{

  TextEditingController codeController = TextEditingController();
  bool _tabVisible=false;
  int _tabCount = 2;
  List<Widget> tabs=<Widget>[];
  int _currentMatchIndex = 0;
  late String userId='0';
  EarnContestResult earnContestResult=new EarnContestResult();
  bool isLoading=false;
  @override
  void initState() {
    super.initState();

    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getAffiliateEarnData('regular');
      })
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(_tabVisible?102:55), // Set this height
          child: Container(
            color: primaryColor,
            // padding: EdgeInsets.only(top: 28),
            child: new Column(
              children: [
                new Container(
                  height: 55,
                  alignment: Alignment.center,
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        onTap: () => {Navigator.pop(context)},
                        child: new Container(
                          padding: EdgeInsets.all(15),
                          alignment: Alignment.centerLeft,
                          child: new Container(
                            width: 16,
                            height: 16,
                            child: Image(
                              image: AssetImage(AppImages.backImageURL),
                              fit: BoxFit.fill,color: Colors.white,),
                          ),
                        ),
                      ),
                      new Container(
                        child: new Text(widget.shortName,
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 18)),
                      ),
                    ],
                  ),
                ),
                _tabVisible?new Container(
                  height: 50,
                  alignment: Alignment.bottomCenter,
                  child: new Container(
                    color: bgColor,
                    child: new DefaultTabController(
                        length: _tabCount,
                        child: new Container(
                          color: bgColor,
                          child: TabBar(
                            labelPadding: EdgeInsets.all(0),
                            onTap: (int index) {
                              setState(() {
                                _currentMatchIndex = index;
                              });
                            },
                            indicatorSize: TabBarIndicatorSize.label,
                            indicator: ShapeDecoration(
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10))),
                                color: primaryColor
                            ),
                            isScrollable: _tabCount==1?true:false,
                            tabs: getTabs(),
                          ),
                        )),
                  ),
                ):new Container()
              ],
            ),
          ),
        ),
        body: new Stack(
          alignment: Alignment.bottomCenter,
          children: [
            new Container(
              margin: EdgeInsets.all(8),
              height: MediaQuery.of(context).size.height,
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: earnContestResult.contests==null?0:earnContestResult.contests!.length,
                  padding: EdgeInsets.only(bottom: 80),
                  itemBuilder: (BuildContext context, int index) {
                    return new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        elevation: 1,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          child: new Container(
                            child: new Column(
                              children: [
                                new Container(
                                  padding: EdgeInsets.all(10),
                                  child: new Column(
                                    children: [
                                      new Container(
                                        child: new Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: [
                                            new Flexible(child: new Column(
                                              children: [
                                                new Container(
                                                  child: Text(
                                                    '₹'+earnContestResult.contests![index].win_amount!.toString(),
                                                    style: TextStyle(
                                                        fontFamily: 'roboto',
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 20),
                                                  ),
                                                ),
                                                new Container(
                                                  margin: EdgeInsets.only( top: 10),
                                                  child: Text(
                                                    'Pool Prize',
                                                    style: TextStyle(
                                                        fontFamily: 'roboto',
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.normal,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                              ],
                                            ),flex: 1,),
                                            new Flexible(child: new Column(
                                              children: [
                                                new Container(
                                                  child: Text(
                                                    earnContestResult.contests![index].maximum_user!.toString(),
                                                    style: TextStyle(
                                                        fontFamily: 'roboto',
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 20),
                                                  ),
                                                ),
                                                new Container(
                                                  margin: EdgeInsets.only( top: 10),
                                                  child: Text(
                                                    'Slot',
                                                    style: TextStyle(
                                                        fontFamily: 'roboto',
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.normal,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                              ],
                                            ),flex: 1,),
                                            new Flexible(child: new Column(
                                              children: [
                                                new Container(
                                                  child: Text(
                                                    '₹'+earnContestResult.contests![index].entryfee.toString(),
                                                    style: TextStyle(
                                                        fontFamily: 'roboto',
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 20),
                                                  ),
                                                ),
                                                new Container(
                                                  margin: EdgeInsets.only( top: 10),
                                                  child: Text(
                                                    'Entry Fee',
                                                    style: TextStyle(
                                                        fontFamily: 'roboto',
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.normal,
                                                        fontSize: 14),
                                                  ),
                                                ),
                                              ],
                                            ),flex: 1,),
                                          ],
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(top: 20),
                                        child: new Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: [
                                            new Container(
                                              child: Text(
                                                earnContestResult.contests![index].joined.toString()+' Joined',
                                                style: TextStyle(
                                                    fontFamily: 'roboto',
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14),
                                              ),
                                            ),
                                            new Container(
                                              child: Text(
                                                'Used Bonus ₹'+earnContestResult.contests![index].bonus_used.toString(),
                                                style: TextStyle(
                                                    fontFamily: 'roboto',
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.w500,
                                                    fontSize: 14),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                new Divider(thickness: 1,height: 2,),
                                new Container(
                                  padding: EdgeInsets.only(left: 10,right: 10,bottom: 10,top: 5),
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      new Container(
                                        child: Text('Earn ₹'+earnContestResult.contests![index].earned.toString(), style: TextStyle(
                                            fontFamily: 'roboto',
                                            color: greenColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 14),
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(top: 5),
                                        alignment: Alignment.center,
                                        child: new Row(
                                          children: [
                                            new Container(
                                              child: new Text('View Teams', style: TextStyle( color: primaryColor, fontWeight: FontWeight.w500, fontSize: 14)),
                                            ),
                                            new Container(
                                              margin: EdgeInsets.only(left: 5, right: 5),
                                              height: 10,
                                              width: 10,
                                              child: Image(image: AssetImage(AppImages.moreForwardIcon),color: primaryColor,),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      onTap: (){
                          navigateToTeamEarnings(context,earnContestResult.contests![index],widget.shortName);
                      },
                    );
                  }),
            ),
            new Container(
              width: MediaQuery.of(context).size.width,
              height: 80,
              alignment: Alignment.center,
              color: primaryColor,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new Flexible(child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      new Container(
                        child: Text(
                          'Joined',
                          style: TextStyle(
                              fontFamily: 'roboto',
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only( top: 10),
                        child: Text(
                          earnContestResult.total==null?'0':earnContestResult.total!.joined!,
                          style: TextStyle(
                              fontFamily: 'roboto',
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),flex: 1,),
                  new Flexible(child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      new Container(
                        child: Text(
                          'Total Contests',
                          style: TextStyle(
                              fontFamily: 'roboto',
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only( top: 10),
                        child: Text(
                          earnContestResult.total==null?'0':earnContestResult.total!.total_contest!,
                          style: TextStyle(
                              fontFamily: 'roboto',
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),flex: 1,),
                  new Flexible(child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      new Container(
                        child: Text(
                          'Earn',
                          style: TextStyle(
                              fontFamily: 'roboto',
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                              fontSize: 16),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only( top: 10),
                        child: Text(
                          earnContestResult.total==null?'₹0':'₹'+earnContestResult.total!.earned!,
                          style: TextStyle(
                              fontFamily: 'roboto',
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 16),
                        ),
                      ),
                    ],
                  ),flex: 1,),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  List<Widget> getTabs(){
    tabs.clear();
    tabs.add(getRegularTab());
    tabs.add(getCommissionTab());
    return tabs;
  }
  Widget getRegularTab(){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Text(
          'REGULAR OFFER',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 0
                  ? primaryColor
                  : Colors.grey,
              fontWeight: FontWeight.w600,
              fontSize: 14),
        ),
      ),
    );
  }

  Widget getCommissionTab(){
    return new Container(
      height: 45,
      alignment: Alignment.center,
      padding: EdgeInsets.all(0),
      margin: EdgeInsets.all(0),
      decoration: BoxDecoration(
        color:Colors.white,
      ),
      child: Text(
        '100% COMMISSION',
        style: TextStyle(
            fontFamily: AppConstants.textBold,
            color: _currentMatchIndex == 1
                ? primaryColor
                : Colors.grey,
            fontWeight: FontWeight.w600,
            fontSize: 14),
      ),
    );
  }
  void getAffiliateEarnData(type) async {
    setState(() {
      isLoading=true;
    });
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,matchkey:widget.matchKey,type:type);
    final client = ApiClient(AppRepository.dio);
    EarnContestResponse matchListResponse = await client.getAffiliateEarnData(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (matchListResponse.status == 1) {
      earnContestResult=matchListResponse.result!;
      setState(() {
        isLoading=false;
      });
    }
  }
}