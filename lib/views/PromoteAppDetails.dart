import 'dart:io';

import 'package:device_apps/device_apps.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/dataModels/AddChannelModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/promote_app_request.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:EverPlay/views/PromoteApp.dart';

class PromoteAppDetails extends StatefulWidget {
  @override
  _PromoteAppDetailsState createState() => _PromoteAppDetailsState();
}

class _PromoteAppDetailsState extends State<PromoteAppDetails> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController cityController = TextEditingController();
  TextEditingController pincodeController = TextEditingController();
  TextEditingController channelNameController = TextEditingController();
  TextEditingController channelUrlController = TextEditingController();
  List<AddChannelModel> channelsList=<AddChannelModel>[];
  List<ChannelDetailModel> channelDetailsList=[];
  var val;
  var groupValue;
  var _dropDownValue;
  String userId='0';

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.light) /* set Status bar icon color in iOS. */
    );
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: new Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            // padding: EdgeInsets.only(top: 28),
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Promote App',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Stack(
          alignment: Alignment.bottomCenter,
          children: [
            new Container(
              height: MediaQuery.of(context).size.height,
              child: new SingleChildScrollView(
                padding: EdgeInsets.only(bottom: 60),
                child: new Container(
                  padding: EdgeInsets.all(10),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      new Container(
                        padding: EdgeInsets.only(left: 10,bottom: 15),
                        child: Text(
                          'Enter your basic details',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'Full Name',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10),
                        height: 45,
                        child: TextField(
                          controller: nameController,
                          decoration: InputDecoration(
                            counterText: "",
                            fillColor: Colors.white,
                            filled: true,
                            hintStyle: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                            ),
                            hintText: 'Full Name',
                            enabledBorder: const OutlineInputBorder(
                              // width: 0.0 produces a thin "hairline" border
                              borderSide:
                              const BorderSide(color: Colors.grey, width: 0.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                            ),
                          ),
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'Email',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10),
                        height: 45,
                        child: TextField(
                          controller: emailController,
                          decoration: InputDecoration(
                            counterText: "",
                            fillColor: Colors.white,
                            filled: true,
                            hintStyle: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                            ),
                            hintText: 'Email',
                            enabledBorder: const OutlineInputBorder(
                              // width: 0.0 produces a thin "hairline" border
                              borderSide:
                              const BorderSide(color: Colors.grey, width: 0.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                            ),
                          ),
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'Mobile',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10),
                        height: 45,
                        child: TextField(
                          controller: mobileController,
                          keyboardType: TextInputType.number,
                          maxLength: 10,
                          decoration: InputDecoration(
                            counterText: "",
                            fillColor: Colors.white,
                            filled: true,
                            prefixIcon: SizedBox(
                              child: Center(
                                widthFactor: 0.0,
                                child: Text('+91',
                                    style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.bold,
                                    )),
                              ),
                            ),
                            hintStyle: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                            ),
                            hintText: 'Mobile',
                            enabledBorder: const OutlineInputBorder(
                              // width: 0.0 produces a thin "hairline" border
                              borderSide:
                              const BorderSide(color: Colors.grey, width: 0.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                            ),
                          ),
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'City',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10),
                        height: 45,
                        child: TextField(
                          controller: cityController,
                          decoration: InputDecoration(
                            counterText: "",
                            fillColor: Colors.white,
                            filled: true,
                            hintStyle: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                            ),
                            hintText: 'City',
                            enabledBorder: const OutlineInputBorder(
                              // width: 0.0 produces a thin "hairline" border
                              borderSide:
                              const BorderSide(color: Colors.grey, width: 0.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                            ),
                          ),
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10),
                        child: Text(
                          'State',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.all(0),
                        child: new DropdownButtonFormField(
                          isDense: true,
                          hint: _dropDownValue == null
                              ? Text('Select State',style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.normal,
                          ))
                              : Text(
                            _dropDownValue,
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          isExpanded: true,
                          iconSize: 0.0,

                          style: TextStyle(color: Colors.black,fontSize: 14),
                          items: AppConstants.items.map(
                                (val) {
                              return DropdownMenuItem<String>(
                                value: val,
                                child: Text(val),
                              );
                            },
                          ).toList(),
                          onChanged: (val) {
                            setState(
                                  () {
                                _dropDownValue = val;
                              },
                            );
                          },
                          decoration: InputDecoration(
                            counterText: "",
                            fillColor: Colors.white,
                            filled: true,
                            hintStyle: TextStyle(
                              fontSize: 14,
                              color: Colors.grey,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                            ),
                            enabledBorder: const OutlineInputBorder(
                              // width: 0.0 produces a thin "hairline" border
                              borderSide:
                              const BorderSide(color: Colors.grey, width: 0.0),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(4)),
                              borderSide: BorderSide(width: 1,color: Colors.grey),
                            ),
                            disabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(4)),
                              borderSide: BorderSide(width: 1,color: Colors.orange),
                            ),
                            errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide(width: 1,color: Colors.red)
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.all(Radius.circular(4)),
                                borderSide: BorderSide(width: 1,color: Colors.red)
                            ),
                          ),

                        ),
                      ),
                      new Container(
                        child: new Column(
                          children: List.generate(
                            channelsList.length,
                                (index) => channelsList[index].widget,
                          ),
                        ),
                      ),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          margin: EdgeInsets.only(left: 10,right: 10,top: 10),
                          child: new DottedBorder(
                            dashPattern: [6, 3],
                            strokeWidth: .7,
                            padding: EdgeInsets.all(0),
                            color: Colors.black,
                            child: Container(
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  new Container(
                                    width: 10,
                                    height: 10,
                                    child: Image(
                                      image: AssetImage(AppImages.addIcon),
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(
                                        top: 10, bottom: 10, left: 5),
                                    alignment: Alignment.center,
                                    child: new Text(
                                      'Add Channel Details',
                                      style: TextStyle(
                                        fontFamily: 'roboto',
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 16,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        onTap: (){
                          setState(() {
                            AddChannelModel channel=new AddChannelModel();
                            channel.widget=ChannelDetailsCard(channelsList.length,channelNameController,channelUrlController,removeChannel);
                            channel.nameController=channelNameController;
                            channel.urlController=channelUrlController;
                            channelsList.add(channel);
                          });
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            new Container(
                width: MediaQuery.of(context).size.width,
                height: 50,
                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    primary: greenColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  onPressed: () {
                    if (nameController.text.isEmpty) {
                      MethodUtils.showError(context, "Please enter valid name");
                    } else if (emailController.text.isEmpty ||
                        !emailController.text.contains("@") ||
                        !RegExp(
                            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                            .hasMatch(emailController.text)) {
                      MethodUtils.showError(context, "Please enter valid email address");
                    } else if (mobileController.text.isEmpty) {
                      MethodUtils.showError(context, "Please enter valid mobile number");
                    } else if (cityController.text.isEmpty) {
                      MethodUtils.showError(context, "Please enter city");
                    } else if (_dropDownValue == null || _dropDownValue.isEmpty) {
                      MethodUtils.showError(context, "Please select your state");
                    } else {
                      updatePromoteAppDetails();
                    }
                  },
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      'Submit',
                      style: TextStyle(fontSize: 14),
                    ),
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }

  void removeChannel(index){
    setState(() {
      channelsList.removeAt(index);
      for (var i = index; i < channelsList.length; i++) {
          channelsList[i].widget.index=channelsList[i].widget.index-1;
      }
    });
  }
  void updatePromoteAppDetails() async {

    AppLoaderProgress.showLoader(context);
    FocusScope.of(context).unfocus();
    for (var i = 0; i < channelsList.length; i++) {
      channelDetailsList.add(new ChannelDetailModel(name: channelsList[i].nameController.text.toString(),url: channelsList[i].urlController.text.toString(),type: channelsList[i].widget._dropDownValue));
    }
    PromoteAppRequest loginRequest = new PromoteAppRequest(user_id: userId,name: nameController.text.toString(),email: emailController.text.toString(),mobile: mobileController.text.toString(),city: cityController.text.toString(),state: _dropDownValue,channel_details: channelDetailsList);
    final client = ApiClient(AppRepository.dio);
    GeneralResponse matchListResponse = await client.updatePromoteAppDetails(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (matchListResponse.status == 1) {
      AppPrefrence.putInt(AppConstants.IS_VISIBLE_PROMOTER_REQUEST, 1);
      if(Platform.isAndroid){
        bool isInstalled = await DeviceApps.isAppInstalled('org.telegram.messenger');
        if(isInstalled){
          _launchURL(AppConstants.telegram_url);
        }else{
          navigateToVisionWebView(context, 'Telegram', AppConstants.telegram_url);
        }
      }else{
        navigateToVisionWebView(context, 'Telegram', AppConstants.telegram_url);
      }
      Navigator.of(context).pop();
      MethodUtils.showSuccess(context, 'Updated Successfully.');
    }
  }
  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class ChannelDetailsCard extends StatefulWidget{
  late int index;
  late TextEditingController channelNameController;
  late TextEditingController channelUrlController;
  Function removeChannel;
  ChannelDetailsCard(this.index,this.channelNameController,this.channelUrlController,this.removeChannel);
  var _dropDownValue;
  @override
  _ChannelDetailsCardState createState() => new _ChannelDetailsCardState();
}

class _ChannelDetailsCardState extends State<ChannelDetailsCard>{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return channelDetails();
  }
  Widget channelDetails(){
    return new Container(
      margin: EdgeInsets.only(left: 5,right: 5),
      child: new Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: new Container(
          padding: EdgeInsets.only(left: 10,right: 10,bottom: 10),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              new Container(
                width: MediaQuery.of(context).size.width,
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: new Container(
                        alignment: Alignment.center,
                        child: new Text('Channel Details',
                          style: TextStyle(
                              fontFamily: AppConstants.textSemiBold,
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 16,decoration: TextDecoration.none),),
                      ),
                      onTap: () => {},
                    ),
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: new Container(
                        height: 40,
                        alignment: Alignment.center,
                        child: new Container(
                          child: Icon(
                            Icons.clear,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      onTap: () => {
                          widget.removeChannel(widget.index)
                      },
                    ),
                  ],
                ),
              ),
              new Container(
                padding: EdgeInsets.only(bottom: 10,top: 10),
                child: Text(
                  'Select Channel Type',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              new Container(
                padding: EdgeInsets.all(0),
                height: 56,
                child: new DropdownButtonFormField(
                  itemHeight: 56,
                  isDense: true,
                  hint: widget._dropDownValue == null
                      ? Text('Select Channel Type',style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                  ))
                      : Text(
                    widget._dropDownValue,
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.black,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  isExpanded: true,
                  iconSize: 20.0,

                  style: TextStyle(color: Colors.black,fontSize: 14),
                  items: AppConstants.channelTypes.map(
                        (val) {
                      return DropdownMenuItem<String>(
                        value: val,
                        child: Text(val),
                      );
                    },
                  ).toList(),
                  onChanged: (val) {
                    setState(
                          () {
                            widget._dropDownValue = val;
                      },
                    );
                  },
                  decoration: InputDecoration(
                    counterText: "",
                    fillColor: Colors.white,
                    filled: true,
                    hintStyle: TextStyle(
                      fontSize: 14,
                      color: Colors.grey,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.bold,
                    ),
                    enabledBorder: const OutlineInputBorder(
                      // width: 0.0 produces a thin "hairline" border
                      borderSide:
                      const BorderSide(color: Colors.grey, width: 0.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      borderSide: BorderSide(width: 1,color: Colors.grey),
                    ),
                    disabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4)),
                      borderSide: BorderSide(width: 1,color: Colors.orange),
                    ),
                    errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(width: 1,color: Colors.red)
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4)),
                        borderSide: BorderSide(width: 1,color: Colors.red)
                    ),
                  ),

                ),
              ),
              new Container(
                padding: EdgeInsets.only(bottom: 10,top: 10),
                child: Text(
                  'Channel Name',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              new Container(
                height: 45,
                child: TextField(
                  controller: widget.channelNameController,
                  decoration: InputDecoration(
                    counterText: "",
                    fillColor: Colors.white,
                    filled: true,
                    hintStyle: TextStyle(
                      fontSize: 12,
                      color: Colors.grey,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.bold,
                    ),
                    hintText: 'Channel Name',
                    enabledBorder: const OutlineInputBorder(
                      // width: 0.0 produces a thin "hairline" border
                      borderSide:
                      const BorderSide(color: Colors.grey, width: 0.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                    ),
                  ),
                ),
              ),
              new Container(
                padding: EdgeInsets.only(bottom: 10,top: 10),
                child: Text(
                  'Channel URL',
                  textAlign: TextAlign.start,
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.black,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              new Container(
                height: 45,
                child: TextField(
                  controller: widget.channelUrlController,
                  decoration: InputDecoration(
                    counterText: "",
                    fillColor: Colors.white,
                    filled: true,
                    hintStyle: TextStyle(
                      fontSize: 12,
                      color: Colors.grey,
                      fontStyle: FontStyle.normal,
                      fontWeight: FontWeight.bold,
                    ),
                    hintText: 'Channel URL',
                    enabledBorder: const OutlineInputBorder(
                      // width: 0.0 produces a thin "hairline" border
                      borderSide:
                      const BorderSide(color: Colors.grey, width: 0.0),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.grey, width: 0.0),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}
