import 'dart:async';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/contest_details_response.dart';
import 'package:EverPlay/repository/model/contest_request.dart';
import 'package:EverPlay/repository/model/player_points_response.dart';
import 'package:EverPlay/repository/model/refresh_score_response.dart';
import 'package:EverPlay/repository/model/score_card_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:EverPlay/views/More.dart';
import 'package:EverPlay/views/MyJoinedContests.dart';
import 'package:EverPlay/views/MyMatches.dart';
import 'package:EverPlay/views/MyTeams.dart';
import 'package:EverPlay/views/PlayerPoints.dart';
import 'package:EverPlay/views/UserNotifications.dart';

import 'Account.dart';
import 'Leaderboard.dart';
import 'WinningBreakups.dart';

class LiveFinishContestDetailsNew extends StatefulWidget {
  LiveFinishedContestData contest;
  GeneralModel model;
  LiveFinishContestDetailsNew(this.contest,this.model);
  @override
  _LiveFinishContestDetailsState createState() => _LiveFinishContestDetailsState();
}

class _LiveFinishContestDetailsState extends State<LiveFinishContestDetailsNew> {
  var title='Home';
  bool back_dialog = false;
  int _currentMatchIndex = 1;
  List<Widget> tabs=<Widget>[];
  String userId='0';
  List<WinnerScoreCardItem> breakupList=[];
  List<JoinedContestTeam> leaderboardList=[];
  List<MultiSportsPlayerPointItem> pointsList = [];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getLeaderboardList();
      })
    });
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.black,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    // TODO: implement build
    return SafeArea(
      child: new Scaffold(
          body: new WillPopScope(
              child: new Stack(
                children: [
                  new Scaffold(
                    backgroundColor: bgColorDark,
                  appBar: PreferredSize(
                    preferredSize: Size.fromHeight(55), // Set this height
                    child: Container(
                      color: Colors.black,
                      // padding: EdgeInsets.only(top: 40),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Row(
                            children: [
                              new GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: ()=>{
                                  Navigator.pop(context)
                                },
                                child: new Container(
                                  padding: EdgeInsets.fromLTRB(15,15,25,15),
                                  alignment: Alignment.centerLeft,
                                  child: new Container(
                                    width: 16,
                                    height: 16,
                                    child: Image(
                                      image: AssetImage(AppImages.backImageURL),
                                      fit: BoxFit.fill,
                                      color: Colors.white,),
                                  ),
                                ),
                              ),
                              new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  new Text("Contests",
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16)),
                                ],
                              ),
                            ],
                          ),
                          new Container(
                            width: 150,
                            margin: EdgeInsets.only(top: 0, left: 10),
                            alignment: Alignment.center,
                            child: new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.end,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                // new GestureDetector(
                                //   behavior: HitTestBehavior.translucent,
                                //   child: new Container(
                                //     height: 20,
                                //     width: 20,
                                //     child: Image(
                                //       image: AssetImage(
                                //           widget.contest.is_fav_contest==1?AppImages.starFillIcon:AppImages.starIcon),
                                //     ),
                                //   ),
                                //   onTap: ()=>{
                                //     favouriteContest()
                                //   },
                                // ),
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new Container(
                                    child: new Row(
                                      children: [
                                        // new Text('₹'+balance,
                                        //     style: TextStyle(
                                        //         fontFamily: AppConstants.textBold,
                                        //         color: primaryColor,
                                        //         fontWeight: FontWeight.normal,
                                        //         fontSize: 15)),
                                        new Container(
                                          margin: EdgeInsets.only(right: 10),
                                          height: 20,
                                          width: 20,
                                          child: Image(
                                            image: AssetImage(
                                                AppImages.walletImageURL),color: primaryColor,),
                                        )
                                      ],
                                    ),
                                  ),
                                  onTap: ()=>{
                                    navigateToWallet(context)
                                  },
                                ),

                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                    body: new Stack(
                      children: [
                        new Container(
                          height: 40,
                          margin: EdgeInsets.all(10),
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black12),
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white
                          ),
                          child: new Container(
                            height: 20,
                            alignment: Alignment.center,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Container(
                                  height: 20,
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          widget.model.teamVs.toString().split(' VS ')[0],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          'Vs',
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          widget.model.teamVs.toString().split(' VS ')[1],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(right: 10),
                                  child: GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    child: Text(
                                      'In Progress',
                                      style: TextStyle(
                                          color: Title_Color_1,
                                          decoration: TextDecoration.none,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 12),
                                    ),
                                    onTap: () {},
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        new Container(
                          height: MediaQuery.of(context).size.height,
                          color: Colors.white,
                          child: new Column(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                            new Container(
                            height: 225,
                            decoration: new BoxDecoration(
                              gradient: new LinearGradient(
                                colors: [
                                  const Color(0xff1a1a1a),
                                  const Color(0xff1a1a1a),
                                ],
                                begin: const FractionalOffset(0.0, 0.0),
                                end: const FractionalOffset(0.0, 1.0),
                              ),
                            ),
                            child: new Container(
                              margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
                              child: new Column(
                                children: [
                                  new Container(
                                    child: Row(
                                      children: [
                                        new GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          onTap: () => {Navigator.pop(context)},
                                          child: new Container(
                                            padding: EdgeInsets.all(15),
                                            alignment: Alignment.centerLeft,
                                            child: new Container(
                                              width: 16,
                                              height: 16,
                                              child: Image(
                                                  image:
                                                  AssetImage(AppImages.backImageURL),
                                                  fit: BoxFit.fill),
                                            ),
                                          ),
                                        ),
                                        new Container(
                                          child: new Text(widget.model.teamVs!,
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 18)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    padding:
                                    EdgeInsets.only(left: 10, right: 10, top: 15),
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        new Flexible(child: new Container(
                                          child: new Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                height:
                                                40,
                                                width:
                                                40,
                                                alignment: Alignment.topRight,
                                                child: CachedNetworkImage(

                                                  imageUrl: widget.model.team1Logo!,
                                                  placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                                  errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                                ),
                                              ),
                                              SizedBox(height: 5,),
                                              new Text(widget.model.team1Name!,
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 12)),
                                              new Container(
                                                margin: EdgeInsets.only(top:5),
                                                child: new Text(widget.model.team1Score!,
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 16)),
                                              ),
                                            ],
                                          ),
                                        ),flex: 1,),
                                        new Flexible(child: new Container(
                                          child: new Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              new Container(
                                                height: 20,
                                                width: 20,
                                                alignment: Alignment.center,
                                                decoration: BoxDecoration(
                                                  image: DecorationImage(image: AssetImage(widget.model.isFromLive!?'assets/images/completeLive.png':'assets/images/liveimage.png')),
                                                ),
                                              ),
                                              SizedBox(height: 10,),
                                              new Container(
                                                alignment: Alignment.center,
                                                child: new Text(widget.model.isFromLive!?'Live':'Completed',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 14)),
                                              ),
                                            ],
                                          ),
                                        ),flex: 1,),
                                        new Flexible(child: new Container(
                                          child: new Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            children: [
                                              Container(
                                                height:
                                                40,
                                                width:
                                                40,
                                                alignment: Alignment.topRight,
                                                child: CachedNetworkImage(

                                                  imageUrl: widget.model.team2Logo!,
                                                  placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                                  errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                                ),
                                              ),
                                              SizedBox(height: 5,),
                                              new Text(widget.model.team2Name!,
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight: FontWeight.w400,
                                                      fontSize: 12)),
                                              new Container(
                                                margin: EdgeInsets.only(top:5),
                                                child: new Text(widget.model.team2Score!,
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 16)),
                                              ),
                                            ],
                                          ),
                                        ),flex: 1,),
                                      ],
                                    ),
                                  ),
                                  widget.model.winningText!.isNotEmpty?new Container(
                                    alignment: Alignment.center,
                                    width: MediaQuery.of(context).size.width,
                                    margin: EdgeInsets.only(top: 20, bottom: 0),
                                    child: new Text(widget.model.winningText! ,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 12)),
                                  ):new Container(),
                                ],
                              ),
                            ),
                          ),
                              new Expanded(
                                child:new DefaultTabController(
                                  length: 3,
                                  initialIndex: 1,
                                  child: new Container(
                                    child: Column(
                                      children: [
                                        new Container(
                                          color: Colors.white,
                                          child: TabBar(
                                            labelPadding: EdgeInsets.all(0),
                                            onTap: (int index) {
                                              setState(() {
                                                _currentMatchIndex = index;
                                                if(_currentMatchIndex==0){
                                                  getWinnerPriceCard();
                                                }else if(_currentMatchIndex==1){
                                                  getWinnerPriceCard();
                                                }else{
                                                  getPlayerPoints();
                                                }
                                              });
                                            },
                                            indicatorWeight: 2,
                                            indicatorSize: TabBarIndicatorSize.label,
                                            indicatorColor: Colors.black,
                                            // indicator: ShapeDecoration(
                                            //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10))),
                                            //     color: Colors.black
                                            // ),
                                            isScrollable:false,
                                            tabs: getTabs(),
                                          ),
                                        ),
                                        Expanded(child: Container(
                                          child: _currentMatchIndex==0?new WinningBreakups(breakupList):_currentMatchIndex==1?new Leaderboard(leaderboardList,userId,false,widget.contest.pdf!,widget.model):new SingleChildScrollView(
                                            child: new Container(
                                              child: new Column(
                                                children: [
                                                  new PlayerPoints(pointsList,widget.model)
                                                ],
                                              ),
                                            ),
                                          ),
                                        ))
                                      ],
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              onWillPop: _onWillPop)),
    );
  }




  Future<bool> _onWillPop() async {
    setState(() {
      Navigator.pop(context);
    });
    return Future.value(false);
  }
  List<Widget> getTabs(){
    tabs.clear();
    tabs.add(getWinningTab());
    tabs.add(getLeaderboardTab());
    tabs.add(getPlayerStatsTab());
    return tabs;
  }
  Widget getWinningTab(){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Text(
          'Winning Breakup',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 0
                  ? Colors.black
                  : Colors.grey,
              fontWeight: FontWeight.w400,
              fontSize: 14),
        ),
      ),
    );
  }
  Widget getLeaderboardTab(){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Text(
          'Leaderboard',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 1
                  ? Colors.black
                  : Colors.grey,
              fontWeight: FontWeight.w400,
              fontSize: 14),
        ),
      ),
    );
  }
  Widget getPlayerStatsTab(){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Text(
          'Player Stats',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 2
                  ? Colors.black
                  : Colors.grey,
              fontWeight: FontWeight.w400,
              fontSize: 14),
        ),
      ),
    );
  }
  void getWinnerPriceCard() async {
    AppLoaderProgress.showLoader(context);
    ContestRequest request = new ContestRequest(user_id: userId,challenge_id: widget.contest.id.toString(),matchkey: widget.model.matchKey,);
    final client = ApiClient(AppRepository.dio);
    ScoreCardResponse response = await client.getWinnersPriceCard(request);
    if (response.status == 1) {
      breakupList=response.result!;
    }
    // Fluttertoast.showToast(msg: response.message, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
  void getLeaderboardList() async {
    AppLoaderProgress.showLoader(context);
    ContestRequest request = new ContestRequest(user_id: userId,challenge_id: widget.contest.id.toString(),matchkey: widget.model.matchKey,sport_key: widget.model.sportKey);
    final client = ApiClient(AppRepository.dio);
    ContestDetailResponse response = await client.getLeaderboardList(request);
    if (response.status == 1) {
      leaderboardList=response.result!.contest!;
    }
    // Fluttertoast.showToast(msg: response.message, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
  void getPlayerPoints() async {
    AppLoaderProgress.showLoader(context);
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.slotId.toString());
    final client = ApiClient(AppRepository.dio);
    PlayerPointsResponse response = await client.getPlayerPoints(contestRequest);
    if (response.status == 1) {
      pointsList=response.result!;
    }
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
}
