import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/dataModels/JoinChallengeDataModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/category_contest_response.dart';
import 'package:EverPlay/repository/model/compare_team_response.dart';
import 'package:EverPlay/repository/model/join_contest_by_code_request.dart';
import 'package:EverPlay/repository/model/join_contest_by_code_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class CompareTeam extends StatefulWidget {
  String matchKey,sportKey,team1Id,team2Id,fantasyType,slotId,challengeId;
  CompareTeam(this.matchKey,this.sportKey,this.team1Id,this.team2Id,this.fantasyType,this.slotId,this.challengeId);
  @override
  _InviteContestCodeState createState() => new _InviteContestCodeState();
}

class _InviteContestCodeState extends State<CompareTeam>{

  TextEditingController codeController = TextEditingController();
  bool loading=false;
  String userId='0';
  late CompareData compareData;

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        compareTeamData();
      })
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            // padding: EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.black,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Compare Team',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: !loading?new Stack(
          children: [
            new Container(
              height: MediaQuery.of(context).size.height,
              child: new Column(
                children: [
                  new Container(
                    margin: EdgeInsets.only(bottom: 20),
                    width: MediaQuery.of(context).size.width,
                    child: new Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        new Expanded(child: new Container(
                          padding: EdgeInsets.only(left: 10),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                            new Container(
                              child: new ClipRRect(
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                                child: new Container(
                                  height: 60,
                                  width: 60,
                                  child: CachedNetworkImage(
                                      imageUrl: compareData.team1_image!,
                                      placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                      errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                      fit: BoxFit.fill
                                  ),
                                ),
                              ),
                            ),
                            new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                new Text(
                                  compareData.team1_name!,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                  style: TextStyle(
                                      fontFamily: AppConstants.textRegular,
                                      color: Colors.black,
                                      fontSize: 14,
                                      fontWeight: FontWeight.w400),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(top: 5),
                                  child: new Text(
                                    compareData.team1_rank!,
                                    style: TextStyle(
                                        fontFamily: AppConstants.textRegular,
                                        color: Colors.grey,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
                              ],
                            )
                          ],),
                        ),flex: 3,),
                        new Expanded(child: new Container(
                          child: new Column(children: [
                            new Container(
                              margin: EdgeInsets.only(bottom: 5),
                              child: new Text(
                                'Total Points',
                                style: TextStyle(
                                    fontFamily: AppConstants.textRegular,
                                    color: Colors.grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                            new Container(
                              height: 30,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(AppImages.comparePointsBgIcon),
                                      fit: BoxFit.fill)),
                              child: new Row(
                                children: [
                                  Expanded(child: new Text(
                                    compareData.team1_points.toString().replaceAll(RegExp(r'([.]*0)(?!.*\d)'), '').replaceAll(RegExp(r'([.]*0)(?!.*\d)'), ''),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: AppConstants.textRegular,
                                        color: Colors.white,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500),
                                  )),
                                  Expanded(child: new Text(
                                    compareData.team2_points.toString().replaceAll(RegExp(r'([.]*0)(?!.*\d)'), '').replaceAll(RegExp(r'([.]*0)(?!.*\d)'), ''),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontFamily: AppConstants.textRegular,
                                        color: Colors.white,
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500),
                                  ))
                                ],
                              ),
                            )
                          ],),
                        ),flex: 2,),
                        new Expanded(child: new Container(
                          padding: EdgeInsets.only(right: 10),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              new Container(
                                child: new ClipRRect(
                                  borderRadius: BorderRadius.all(Radius.circular(30)),
                                  child: new Container(
                                    height: 60,
                                    width: 60,
                                    child: CachedNetworkImage(
                                        imageUrl: compareData.team2_image!,
                                        placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                        errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                        fit: BoxFit.fill
                                    ),
                                  ),
                                ),
                              ),
                              new Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  new Text(
                                    compareData.team2_name!,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        fontFamily: AppConstants.textRegular,
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: new Text(
                                      compareData.team2_rank!,
                                      style: TextStyle(
                                          fontFamily: AppConstants.textRegular,
                                          color: Colors.grey,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                ],
                              )
                            ],),
                        ),flex: 3,)
                      ],
                    ),
                  ),
                  new Divider(
                    height: 1,
                    thickness: 1,
                  ),
                  new Container(
                    height: 40,
                    color: lightGrayColor,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        new Container(
                          alignment: Alignment.center,
                          child: new Text(
                            compareData.diff_text!,
                            style: TextStyle(
                                fontFamily: AppConstants.textSemiBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 17,
                                height: 1.5),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        new Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(left: 5),
                          child: new Text(
                            compareData.diff_points.toString(),
                            style: TextStyle(
                                fontFamily: AppConstants.textSemiBold,
                                color: primaryColor,
                                fontWeight: FontWeight.w500,
                                fontSize: 17,
                                height: 1.5),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Divider(
                    height: 1,
                    thickness: 1,
                  ),
                  Expanded(child: new SingleChildScrollView(
                      padding: EdgeInsets.only(bottom: 20),
                      child: new Column(
                    children: List.generate(
                      compareData.player_list!.length,
                          (index) => new Column(
                        children: [
                          new Container(
                            margin: EdgeInsets.only(bottom: 10,top: 10),
                            child: new Column(
                              children: [
                                new Container(
                                  alignment: Alignment.center,
                                  child: new Text(
                                    index==0?'Captain and Vice Captain':index==1?'Different Players':'Common Players',
                                    style: TextStyle(
                                        fontFamily: AppConstants.textSemiBold,
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 14,
                                        height: 1.5),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    new Container(
                                      alignment: Alignment.center,
                                      child: new Text(
                                        compareData.player_list![index].diff_text!,
                                        style: TextStyle(
                                            fontFamily: AppConstants.textSemiBold,
                                            color: Colors.grey,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 11,
                                            height: 1.5),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.center,
                                      margin: EdgeInsets.only(left: 5),
                                      child: new Text(
                                        compareData.player_list![index].diff_points.toString(),
                                        style: TextStyle(
                                            fontFamily: AppConstants.textSemiBold,
                                            color: primaryColor,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 11,
                                            height: 1.5),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                          compareData.player_list![index].data!.length>0?new Column(
                            children: List.generate(compareData.player_list![index].data!.length, (position) => new Column(
                              children: [
                                new Container(
                                  height: 50,
                                  alignment: Alignment.center,
                                  child: new Row(
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      new Expanded(child: new Container(
                                        child: new Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            new Container(
                                              child: new Stack(
                                                alignment: Alignment.topLeft,
                                                children: [
                                                  new Container(
                                                    margin: EdgeInsets.only(top: 5),
                                                    child: CachedNetworkImage(
                                                        height: 45,
                                                        width: 45,
                                                        imageUrl: compareData.player_list![index].data![position].player_data==null?'':compareData.player_list![index].data![position].player_data![0].image??'',
                                                        placeholder: (context,url)=>new Image.asset(AppImages.defaultAvatarIcon,),
                                                        errorWidget: (context, url, error) => new Image.asset(AppImages.defaultAvatarIcon,),
                                                        fit: BoxFit.fill
                                                    ),
                                                  ),
                                                  index==0?new Container(
                                                    height: 20,
                                                    width: 20,
                                                    margin: EdgeInsets.only(bottom: 2),
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration(
                                                        border: Border.all(color: position==0?primaryColor:greenColor),
                                                        borderRadius: BorderRadius.circular(14),
                                                        color: position==0?primaryColor:greenColor
                                                    ),
                                                    child: new Text(position==0?'C':'VC',
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 10),textAlign: TextAlign.center,),
                                                  ):new Container()
                                                ],
                                              ),
                                            ),
                                            new Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                new Text(
                                                  compareData.player_list![index].data![position].player_data==null?'':compareData.player_list![index].data![position].player_data![0].getShortName(),
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      fontFamily: AppConstants.textRegular,
                                                      color: Colors.black,
                                                      fontSize: 11,
                                                      fontWeight: FontWeight.w500),
                                                ),
                                                new Container(
                                                  margin: EdgeInsets.only(top: 5),
                                                  child: new Text(
                                                    compareData.player_list![index].data![position].player_data==null?'':compareData.player_list![index].data![position].player_data![0].role??'',
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textRegular,
                                                        color: Colors.grey,
                                                        fontSize: 11,
                                                        fontWeight: FontWeight.w400),
                                                  ),
                                                ),
                                              ],
                                            )
                                          ],),
                                      ),flex: 2,),
                                      new Expanded(child: new Container(
                                        alignment: Alignment.center,
                                        child: new Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            new Container(
                                              height: 30,
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: AssetImage(AppImages.comparePointsBgIcon),
                                                      fit: BoxFit.fill)),
                                              child: new Row(
                                                children: [
                                                  Expanded(child: new Text(
                                                    compareData.player_list![index].data![position].player_data==null?'':(compareData.player_list![index].data![position].player_data![0].playerpoints??'0').toString().replaceAll(RegExp(r'([.]*0)(?!.*\d)'), '').replaceAll(RegExp(r'([.]*0)(?!.*\d)'), ''),
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textRegular,
                                                        color: Colors.white,
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.w500),
                                                  )),
                                                  Expanded(child: new Text(
                                                    compareData.player_list![index].data![position].player_data==null?'':(compareData.player_list![index].data![position].player_data![1].playerpoints??'0').toString().replaceAll(RegExp(r'([.]*0)(?!.*\d)'), '').replaceAll(RegExp(r'([.]*0)(?!.*\d)'), ''),
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textRegular,
                                                        color: Colors.white,
                                                        fontSize: 12,
                                                        fontWeight: FontWeight.w500),
                                                  ))
                                                ],
                                              ),
                                            )
                                          ],),
                                      ),flex: 1,),
                                      new Expanded(child: new Container(
                                        child: new Row(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            new Column(
                                              crossAxisAlignment: CrossAxisAlignment.end,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                new Text(
                                                  compareData.player_list![index].data![position].player_data==null?'':compareData.player_list![index].data![position].player_data![1].getShortName(),
                                                  overflow: TextOverflow.ellipsis,
                                                  maxLines: 1,
                                                  style: TextStyle(
                                                      fontFamily: AppConstants.textRegular,
                                                      color: Colors.black,
                                                      fontSize: 11,
                                                      fontWeight: FontWeight.w500),
                                                ),
                                                new Container(
                                                  margin: EdgeInsets.only(top: 5),
                                                  child: new Text(
                                                    compareData.player_list![index].data![position].player_data==null?'':compareData.player_list![index].data![position].player_data![1].role??'',
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textRegular,
                                                        color: Colors.grey,
                                                        fontSize: 11,
                                                        fontWeight: FontWeight.w400),
                                                  ),
                                                ),
                                              ],
                                            ),
                                            new Container(
                                              child: new Stack(
                                                alignment: Alignment.topRight,
                                                children: [
                                                  new Container(
                                                    height: 45,
                                                    width: 45,
                                                    margin: EdgeInsets.only(top: 5),
                                                    child: CachedNetworkImage(
                                                        imageUrl: compareData.player_list![index].data![position].player_data==null?'':compareData.player_list![index].data![position].player_data![1].image??'',
                                                        placeholder: (context,url)=>new Image.asset(AppImages.defaultAvatarIcon,),
                                                        errorWidget: (context, url, error) => new Image.asset(AppImages.defaultAvatarIcon,),
                                                        fit: BoxFit.fill
                                                    ),
                                                  ),
                                                  index==0?new Container(
                                                    height: 20,
                                                    width: 20,
                                                    margin: EdgeInsets.only(bottom: 2),
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration(
                                                        border: Border.all(color: position==0?primaryColor:greenColor),
                                                        borderRadius: BorderRadius.circular(14),
                                                        color: position==0?primaryColor:greenColor
                                                    ),
                                                    child: new Text(position==0?'C':'VC',
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 10),textAlign: TextAlign.center,),
                                                  ):new Container()
                                                ],
                                              ),
                                            )

                                          ],),
                                      ),flex: 2,)
                                    ],
                                  ),
                                ),
                                new Divider(
                                  height: 1,
                                  thickness: 1,
                                  color: MethodUtils.hexToColor('#ac8736'),
                                ),
                              ],
                            )),
                          ):new Container()
                        ],
                      ),
                    ),
                  )))
                ],
              ),
            ),
          ],
        ):new Container(),
      ),
    );
  }
  void compareTeamData() async {
    setState(() {loading=true;});
    AppLoaderProgress.showLoader(context);
    GeneralRequest contestRequest = new GeneralRequest(user_id: userId,matchkey: widget.matchKey,sport_key: widget.sportKey,fantasy_type: widget.fantasyType,slotes_id: widget.slotId,team1_id: widget.team1Id,team2_id: widget.team2Id);
    final client = ApiClient(AppRepository.dio);
    CompareTeamResponse response = await client.getCompareTeamData(contestRequest);
    if (response.status == 1) {
        compareData=response.result!;
    }else{
        MethodUtils.showError(context, response.message??'Something went wrong!');
    }
    setState(() {
      loading=false;
      AppLoaderProgress.hideLoader(context);
    });
  }
}
