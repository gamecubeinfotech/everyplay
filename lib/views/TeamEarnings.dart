import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/earn_contests_response.dart';
import 'package:EverPlay/repository/model/promoter_teams_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class TeamEarnings extends StatefulWidget {
  EarnContestItem earnContestItem;
  String shortName;

  TeamEarnings(this.earnContestItem,this.shortName);

  @override
  _TeamEarningsState createState() => new _TeamEarningsState();
}

class _TeamEarningsState extends State<TeamEarnings>{

  TextEditingController codeController = TextEditingController();
  List<PromoterTeamsData> teamList=[];
  List<Widget> tabs=<Widget>[];
  bool isLoading=false;
  late String userId='0';

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        promoterTeams();
      })
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: primaryColor,
            // padding: EdgeInsets.only(top: 28),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.white,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text(widget.shortName,
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Stack(
          children: [
            new Column(
              children: [
                new Container(
                  color: Colors.white,
                  padding: EdgeInsets.all(10),
                  child: new Column(
                    children: [
                      new Container(
                        child: new Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            new Flexible(child: new Column(
                              children: [
                                new Container(
                                  child: Text(
                                    '₹'+widget.earnContestItem.win_amount!.toString(),
                                    style: TextStyle(
                                        fontFamily: 'roboto',
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20),
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only( top: 10),
                                  child: Text(
                                    'Pool Prize',
                                    style: TextStyle(
                                        fontFamily: 'roboto',
                                        color: Colors.black,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14),
                                  ),
                                ),
                              ],
                            ),flex: 1,),
                            new Flexible(child: new Column(
                              children: [
                                new Container(
                                  child: Text(
                                    widget.earnContestItem.maximum_user!.toString(),
                                    style: TextStyle(
                                        fontFamily: 'roboto',
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20),
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only( top: 10),
                                  child: Text(
                                    'Slot',
                                    style: TextStyle(
                                        fontFamily: 'roboto',
                                        color: Colors.black,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14),
                                  ),
                                ),
                              ],
                            ),flex: 1,),
                            new Flexible(child: new Column(
                              children: [
                                new Container(
                                  child: Text(
                                    '₹'+widget.earnContestItem.entryfee.toString(),
                                    style: TextStyle(
                                        fontFamily: 'roboto',
                                        color: Colors.black,
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20),
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only( top: 10),
                                  child: Text(
                                    'Entry Fee',
                                    style: TextStyle(
                                        fontFamily: 'roboto',
                                        color: Colors.black,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 14),
                                  ),
                                ),
                              ],
                            ),flex: 1,),
                          ],
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only(top: 20),
                        child: new Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            new Container(
                              child: Text(
                                widget.earnContestItem.joined.toString()+' Joined',
                                style: TextStyle(
                                    fontFamily: 'roboto',
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12),
                              ),
                            ),
                            new Container(
                              child: Text(
                                'Used Bonus ₹'+widget.earnContestItem.bonus_used.toString(),
                                style: TextStyle(
                                    fontFamily: 'roboto',
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12),
                              ),
                            ),
                            new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                new Text(
                                  'Earn ₹'+widget.earnContestItem.earned.toString(),
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                !isLoading?Expanded(child: new Container(
                  margin: EdgeInsets.all(8),
                  height: MediaQuery.of(context).size.height,
                  child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: teamList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return new Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: new Container(
                            padding: EdgeInsets.only(left: 20,right: 10,top: 10,bottom: 10),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(right: 10),
                                      child: new ClipRRect(
                                        borderRadius: BorderRadius.all(Radius.circular(20)),
                                        child: new Container(
                                          height: 40,
                                          width: 40,
                                          child: CachedNetworkImage(
                                              imageUrl: teamList[index].image==null?'':teamList[index].image!,
                                              placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                              errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                              fit: BoxFit.fill
                                          ),
                                        ),
                                      ),
                                    ),
                                    new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        new Text(
                                          teamList[index].team!,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                                new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    new Text(
                                      'Earn ₹'+teamList[index].amount!,
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                )):new Container(),
              ],
            )
          ],
        ),
      ),
    );
  }
  void promoterTeams() async {
    setState(() {
      isLoading=true;
    });
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,challenge_id:widget.earnContestItem.challenge_id);
    final client = ApiClient(AppRepository.dio);
    PromoterTeamsResponse matchListResponse = await client.promoterTeams(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (matchListResponse.status == 1) {
      teamList=matchListResponse.result!;
      setState(() {
        isLoading=false;
      });
    }
  }
}