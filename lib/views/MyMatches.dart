import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:EverPlay/adapter/ShimmerMatchItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/appUtilities/date_utils.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/repository/model/finish_matchlist_response.dart';
import 'package:EverPlay/repository/model/matchlist_response.dart';
import 'package:EverPlay/views/NoMatchesListFound.dart';
import 'package:EverPlay/GetXController/UpcomingMyMatchesGetController.dart';

class MyMatches extends StatefulWidget {
  Function? pullRefresh;
  Function? getMyMatchFinishList;
  MatchListResponse? matchListResponse;
  FinishMatchListResponse? finishMatchListResponse;
  bool? isMyMatchLoading;
  MyMatches({this.pullRefresh,this.matchListResponse,this.finishMatchListResponse,this.isMyMatchLoading,this.getMyMatchFinishList});

  @override
  _MyMatchesState createState() => _MyMatchesState();
}

class _MyMatchesState extends State<MyMatches> {
  int _currentSportIndex = 0;
  // final upcomingMyMatchesGetController=Get.put(UpcomingMyMatchesGetController());
  int currentMatchIndex = 0;
  bool back_dialog = false;
  final List<CustomTimerController> _controller =[];
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: darkPrimaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    // TODO: implement build
    return SafeArea(
      child: Scaffold(
          appBar:  PreferredSize(
            preferredSize: Size.fromHeight(55), // Set this height
            child: Container(
              width: MediaQuery.of(context).size.width,
              color: Colors.black,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 10),
                    child: Text('My Contest',style: TextStyle(
                        color: Colors.white,fontSize: 17
                    ),),
                  ),

                  new Container(
                    // padding: EdgeInsets.only(top: 40),
                    alignment: Alignment.center,
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.end,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        // new GestureDetector(
                        //   behavior: HitTestBehavior.translucent,
                        //   child: new Container(
                        //     margin: EdgeInsets.only(right: 5),
                        //     height: 20,
                        //     width: 20,
                        //     child: Image(
                        //       image: AssetImage(AppImages.chatIcon),
                        //     ),
                        //   ),
                        //   onTap: openChat,
                        // ),
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            margin:
                            EdgeInsets.only(right: 5, left: 5),
                            child: new Row(
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(left: 5),
                                  height: 20,
                                  width: 20,
                                  child: Image(
                                      image: AssetImage(AppImages
                                          .walletImageURL)),
                                )
                              ],
                            ),
                          ),
                          onTap: () => {navigateToWallet(context)},
                        ),
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            margin: EdgeInsets.only(right: 10, left: 5),
                            height: 20,
                            width: 20,
                            child: Image(
                              image: AssetImage(
                                // widget.notification==0?AppImages.notificationImageURL:AppImages.notificationDotIcon),
                                  AppImages.notificationImageURL),
                            ),
                          ),
                          onTap: () =>
                          {navigateToUserNotifications(context)},
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
          body: new Stack(
            children: [
              DefaultTabController(
                length: 3,
                child: Scaffold(
                  backgroundColor: whiteColor,
                  appBar: PreferredSize(
                    preferredSize: Size.fromHeight(45),
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        new Container(
                          margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
                          padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
                          decoration: BoxDecoration(border: Border(bottom: BorderSide(width: 1,color: TextFieldBorderColor))),
                          height: 40,
                          child: new DefaultTabController(
                              length: 3,
                              child: new Container(
                                child: TabBar(
                                  indicatorColor: Themecolor,
                                  labelPadding: EdgeInsets.all(0),
                                  onTap: (int index) {
                                    setState(() {
                                      currentMatchIndex = index;
                                      currentMatchIndex>1?widget.getMyMatchFinishList!(currentMatchIndex):widget.pullRefresh!(currentMatchIndex);
                                    });
                                  },
                                  indicatorWeight: 3,
                                  isScrollable: false,
                                  tabs: [
                                    Text(
                                      'Upcoming',
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: currentMatchIndex == 0
                                              ? Themecolor
                                              : Text_Color,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14),
                                    ),
                                    Text(
                                      'Live',
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: currentMatchIndex == 1
                                              ? Themecolor
                                              : Text_Color,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14),
                                    ),
                                    Text(
                                      'Completed',
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: currentMatchIndex == 2
                                              ? Themecolor
                                              : Text_Color,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 14),
                                    )
                                  ],
                                ),
                              )),
                        )
                      ],
                    ),
                  ),
                  body: new RefreshIndicator(child: new SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    child: new Column(
                      children: [
                        !widget.isMyMatchLoading!?widget.matchListResponse!.result!.length>0?new Container(
                          margin: EdgeInsets.all(8),
                          child: GetBuilder<UpcomingMyMatchesGetController>(
                              builder: (controller) {
                                return ListView.builder(
                                    physics: NeverScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    scrollDirection: Axis.vertical,
                                    itemCount: widget.matchListResponse!.result!.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      controller.MinuteTestTimingVariable.add("");
                                      controller.SecTestTimingVariable.add("");
                                      _controller.add(CustomTimerController());
                                      return Visibility(
                                        visible: "${ controller.MinuteTestTimingVariable[index]}:""${ controller.SecTestTimingVariable[index]}"=="00:-1" ?false: true,
                                        child: new GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          child: Container(
                                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(12),border: Border.all(color:bordercolor,width: 0.5)),
                                            margin: EdgeInsets.only(bottom: 10),
                                            child: new Container(
                                              child: new Column(
                                                children: [

                                                  new Container(
                                                    height: 20,
                                                    margin: EdgeInsets.only(top: 5),
                                                    child: new Row(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        new Container(
                                                          margin: EdgeInsets.only(
                                                              left: 10, top: 5),
                                                          child: Text(
                                                            widget.matchListResponse!.result![index].name!,
                                                            style: TextStyle(
                                                                // fontFamily: 'noway',
                                                                color: textGrey,
                                                                fontWeight: FontWeight.w500,
                                                                fontSize: 14),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  Container( padding: EdgeInsets.symmetric(horizontal: 10),child: Divider(color: bordercolor)),
                                                  SizedBox(height: 5,),
                                                  new Container(
                                                    padding: EdgeInsets.only(bottom: 5),
                                                    alignment: Alignment.center,
                                                    child: new Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      children: [
                                                        new Flexible(child: new Container(

                                                          child: new Column(
                                                            children: [

                                                              new Row(
                                                                mainAxisAlignment: MainAxisAlignment.start,
                                                                children: [
                                                                  new Container(
                                                                    padding: EdgeInsets.only(left: 10),
                                                                    child: new Container(
                                                                      height:
                                                                      35,
                                                                      width:
                                                                      35,
                                                                      alignment: Alignment.topRight,
                                                                      child: CachedNetworkImage(

                                                                        imageUrl: widget.matchListResponse!.result![index].team1logo!,
                                                                        placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                                                        errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                  SizedBox(width: 10,),
                                                                  new Container(
                                                                    child:
                                                                    new Column(
                                                                      crossAxisAlignment:
                                                                      CrossAxisAlignment.start,
                                                                      children: [

                                                                        new Container(
                                                                          margin:
                                                                          EdgeInsets.only(top: 2),
                                                                          width: 50,
                                                                          child:
                                                                          new Text(
                                                                            widget.matchListResponse!.result![index].team1display!,
                                                                            textAlign: TextAlign.left,
                                                                            overflow: TextOverflow.ellipsis,
                                                                            style: TextStyle(fontFamily: 'noway', color: Colors.black, fontWeight: FontWeight.w600, fontSize: 14),
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                  )
                                                                ],
                                                              ),
                                                               SizedBox(height: 10,),
                                                               Container(
                                                                width: 110,
                                                                margin: EdgeInsets.only(bottom: 5,left: 10),
                                                                child:  new Text(
                                                                  widget.matchListResponse!.result![index].team1name!,
                                                                  textAlign:
                                                                  TextAlign.left,
                                                                  overflow: TextOverflow.ellipsis,
                                                                  style: TextStyle(
                                                                      fontFamily: 'noway',
                                                                      color: LinkTextColor,
                                                                      fontWeight: FontWeight.w400,
                                                                      fontSize: 14),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),flex: 1,),
                                                        new Flexible(child: new Container(
                                                          child: new Column(
                                                            children: [
                                                              // Container(height: 35, child: Image(image: AssetImage(AppImages.Vs),color: Themecolor,)),
                                                              Container(height: 35, child: Image(image: AssetImage(widget.matchListResponse!.result![index].match_status=='Winner Declared'?AppImages.WinnerDeclear:AppImages.Vs),)),
                                                              SizedBox(height: 5,),
                                                              currentMatchIndex==0?CustomTimer(
                                                                from: Duration(milliseconds: SuperDateFormat.getFormattedDateObj(widget.matchListResponse!.result![index].time_start!)!.millisecondsSinceEpoch-MethodUtils.getDateTime().millisecondsSinceEpoch),
                                                                to: Duration(milliseconds: 0),
                                                                onBuildAction: CustomTimerAction.auto_start,
                                                                controller: _controller[index],
                                                                builder: (CustomTimerRemainingTime remaining) {
                                                                  if(remaining.minutes=='00'&& remaining.seconds=='-1')
                                                                  {
                                                                    _controller[index].pause();
                                                                    controller.TimeFunction(remaining.minutes, remaining.seconds, index);

                                                                  }
                                                                  return new GestureDetector(
                                                                    behavior: HitTestBehavior.translucent,
                                                                    child: Text(
                                                                      int.parse(remaining.days)>=1? int.parse(remaining.days)>1?"${remaining.days} Days ":" ${remaining.days} Day":
                                                                      int.parse(remaining.hours)>=1?("${remaining.hours}H : ${remaining.minutes}M"):
                                                                      "${remaining.minutes}M : ${remaining.seconds=='-1'?"00":remaining.seconds}S",
                                                                      style: TextStyle(
                                                                          fontFamily: 'noway',
                                                                          color: Title_Color_1,
                                                                          decoration: TextDecoration.none,
                                                                          fontWeight:
                                                                          FontWeight.w500,
                                                                          fontSize: 12),
                                                                    ),
                                                                    onTap: (){
                                                                    },
                                                                  );
                                                                },
                                                              ):Text(
                                                                widget.matchListResponse!.result![index].match_status!,
                                                                style: TextStyle(
                                                                    fontFamily: 'noway',
                                                                    color:  widget.matchListResponse!.result![index].match_status=='Winner Declared'?Title_Color_1:Themecolor,
                                                                    decoration: TextDecoration.none,
                                                                    fontWeight:
                                                                    FontWeight.w500,
                                                                    fontSize: 12),
                                                              ),
                                                            ],
                                                          ),
                                                        ),flex: 1,),
                                                        new Flexible(child: new Container(
                                                          child: new Column(
                                                            children: [

                                                              new Row(
                                                                mainAxisAlignment: MainAxisAlignment.end,
                                                                children: [
                                                                  new Container(
                                                                    margin: EdgeInsets.only(
                                                                        right:
                                                                        5),
                                                                    child:
                                                                    new Column(
                                                                      crossAxisAlignment:
                                                                      CrossAxisAlignment.end,
                                                                      children: [
                                                                        new Container(
                                                                          margin:
                                                                          EdgeInsets.only(top: 2,right: 5),
                                                                          width: 50,
                                                                          alignment: Alignment.centerRight,
                                                                          child:
                                                                          new Text(
                                                                            widget.matchListResponse!.result![index].team2display!,
                                                                            overflow: TextOverflow.ellipsis,
                                                                            textAlign: TextAlign.left,
                                                                            style: TextStyle(fontFamily: 'noway', color: Colors.black, fontWeight: FontWeight.w600, fontSize: 14),
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                  ),
                                                                  new Container(
                                                                    height:
                                                                    35,
                                                                    width:
                                                                    35,
                                                                    // padding: EdgeInsets.only(right: 2),
                                                                    alignment: Alignment.topLeft,
                                                                    child:
                                                                    CachedNetworkImage(
                                                                      imageUrl: widget.matchListResponse!.result![index].team2logo!,
                                                                      placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                                                      errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                                                    ),
                                                                  ),
                                                                  SizedBox(width: 10,),
                                                                ],
                                                              ),
                                                              SizedBox(height: 10,),
                                                              new Container(
                                                                width: 110,
                                                                margin: EdgeInsets.only(bottom: 5,right: 10),
                                                                alignment: Alignment.centerRight,
                                                                child: new Text(
                                                                  widget.matchListResponse!.result![index].team2name!,
                                                                  textAlign:
                                                                  TextAlign.left,
                                                                  overflow: TextOverflow.ellipsis,
                                                                  style: TextStyle(
                                                                      fontFamily: 'noway',
                                                                      color: LinkTextColor,
                                                                      fontWeight: FontWeight.w400,
                                                                      fontSize: 14),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ),flex: 1,),
                                                      ],
                                                    ),
                                                  ),
                                                  SizedBox(height: 5,),
                                                  new Divider(height: 1,),

                                                  new Container(
                                                    decoration: BoxDecoration(
                                                      color: TextFieldColor,
                                                      borderRadius: BorderRadius.only(bottomLeft: Radius.circular(12),bottomRight: Radius.circular(12)) ,
                                                    ),
                                                    height: 30,
                                                    child: new Row(
                                                      mainAxisAlignment:
                                                      MainAxisAlignment.spaceBetween,
                                                      children: [
                                                        new Row(
                                                          children: [
                                                            new Container(
                                                              margin: EdgeInsets.only(left: 10),
                                                              child: Text(
                                                                widget.matchListResponse!.result![index].team_count!.toString()+' Team',
                                                                style: TextStyle(
                                                                    color:
                                                                    Themecolor,
                                                                    fontSize: 12,
                                                                    fontFamily: 'noway',
                                                                    fontWeight:
                                                                    FontWeight.w400),
                                                              ),
                                                            ),
                                                            new Container(
                                                              margin: EdgeInsets.only(left: 10),
                                                              child: new Text(
                                                                widget.matchListResponse!.result![index].joined_count!.toString()+' Contests Joined',
                                                                style: TextStyle(
                                                                    color:
                                                                    textGrey,
                                                                    fontSize: 12,
                                                                    fontFamily: 'noway',
                                                                    fontWeight:
                                                                    FontWeight.w400),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        widget.matchListResponse!.result![index].is_visible_total_earned==1?new Container(
                                                          margin: EdgeInsets.only(right: 10),
                                                          child: new Text(
                                                            'Total Affiliation: ₹'+widget.matchListResponse!.result![index].total_earned!.toString(),
                                                            style: TextStyle(
                                                                color: greenColor,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                FontWeight.bold),
                                                          ),
                                                        ):new Container(),
                                                        currentMatchIndex==2?new Container(
                                                          margin: EdgeInsets.only(right: 10),
                                                          child: new Text(
                                                            'YOU WON: ₹'+widget.matchListResponse!.result![index].winning_total!.toString(),
                                                            style: TextStyle(
                                                                fontFamily: 'noway',
                                                                color: Themecolor,
                                                                fontSize: 12,
                                                                fontWeight:
                                                                FontWeight.w500),
                                                          ),
                                                        ):Icon(Icons.keyboard_arrow_right,color: textGrey,),
                                                        // currentMatchIndex==2?Container():
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                          onTap: (){
                                            if(currentMatchIndex==0) {
                                              navigateToUpcomingContests(context,new GeneralModel(bannerImage: widget.matchListResponse!.result![index].banner_image,matchKey: widget.matchListResponse!.result![index].matchkey,teamVs: (widget.matchListResponse!.result![index].team1display!+' VS '+widget.matchListResponse!.result![index].team2display!),firstUrl: widget.matchListResponse!.result![index].team1logo,secondUrl: widget.matchListResponse!.result![index].team2logo,headerText: widget.matchListResponse!.result![index].time_start,sportKey: widget.matchListResponse!.result![index].sport_key,battingFantasy: widget.matchListResponse!.result![index].battingfantasy,bowlingFantasy: widget.matchListResponse!.result![index].bowlingfantasy,liveFantasy: widget.matchListResponse!.result![index].livefantasy,secondInningFantasy: widget.matchListResponse!.result![index].secondinning,reverseFantasy: widget.matchListResponse!.result![index].reversefantasy,fantasySlots: widget.matchListResponse!.result![index].slotes,team1Name: widget.matchListResponse!.result![index].team1name,team2Name: widget.matchListResponse!.result![index].team2name,team1Logo: widget.matchListResponse!.result![index].team1logo,team2Logo: widget.matchListResponse!.result![index].team2logo));
                                            }else{
                                              navigateToLiveFinishContests(context,new GeneralModel(bannerImage: widget.matchListResponse!.result![index].banner_image,matchKey: widget.matchListResponse!.result![index].matchkey,teamVs: (widget.matchListResponse!.result![index].team1display!+' VS '+widget.matchListResponse!.result![index].team2display!),firstUrl: widget.matchListResponse!.result![index].team1logo,secondUrl: widget.matchListResponse!.result![index].team2logo,headerText: widget.matchListResponse!.result![index].time_start,sportKey: widget.matchListResponse!.result![index].sport_key,battingFantasy: widget.matchListResponse!.result![index].battingfantasy,bowlingFantasy: widget.matchListResponse!.result![index].bowlingfantasy,liveFantasy: widget.matchListResponse!.result![index].livefantasy,secondInningFantasy: widget.matchListResponse!.result![index].secondinning,reverseFantasy: widget.matchListResponse!.result![index].reversefantasy,fantasySlots: widget.matchListResponse!.result![index].slotes,team1Name: widget.matchListResponse!.result![index].team1name,team2Name: widget.matchListResponse!.result![index].team2name,team1Logo: widget.matchListResponse!.result![index].team1logo,team2Logo: widget.matchListResponse!.result![index].team2logo,isFromLive: currentMatchIndex==1,isFromLiveFinish: true));
                                            }
                                          },
                                        ),
                                      );
                                    });
                              }
                          ),
                        ):NoMatchesListFound():new Container(
                          margin: EdgeInsets.all(8),
                          child: ListView.builder(
                              physics:
                              NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              scrollDirection: Axis.vertical,
                              itemCount: 7,
                              itemBuilder: (BuildContext context,
                                  int index) {
                                return new ShimmerMatchItemAdapter();
                              }),
                        )
                      ],
                    ),
                  ), onRefresh: _pullRefresh),
                ),
              ),
            ],
          )),
    );
  }
  Future<void> _pullRefresh() async {
    currentMatchIndex>1?widget.getMyMatchFinishList!(currentMatchIndex):widget.pullRefresh!(currentMatchIndex);
  }
}
