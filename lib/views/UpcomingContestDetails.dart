import 'dart:async';
import 'dart:ui';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:share/share.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter/services.dart';

import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/app_images.dart';
import '../appUtilities/app_navigator.dart';
import '../appUtilities/date_utils.dart';
import '../appUtilities/method_utils.dart';
import '../customWidgets/CustomProgressIndicator.dart';
import '../customWidgets/app_circular_loader.dart';
import '../dataModels/GeneralModel.dart';
import '../dataModels/JoinChallengeDataModel.dart';
import '../localStoage/AppPrefrences.dart';
import '../repository/app_repository.dart';
import '../repository/model/base_request.dart';
import '../repository/model/base_response.dart';
import '../repository/model/category_contest_response.dart';
import '../repository/model/contest_details_response.dart';
import '../repository/model/contest_request.dart';
import '../repository/model/get_offer_response.dart';
import '../repository/model/score_card_response.dart';
import '../repository/retrofit/api_client.dart';
import 'Leaderboard.dart';
import 'WinningBreakups.dart';

class UpcomingContestDetails extends StatefulWidget {
  GeneralModel model;
  Contest contest;
  UpcomingContestDetails(this.model,this.contest);
  @override
  _UpcomingContestDetailsState createState() => _UpcomingContestDetailsState();
}

class _UpcomingContestDetailsState extends State<UpcomingContestDetails> {
  int _currentSportIndex = 0;
  int _currentIndex = 0;
  var title='Home';
  bool back_dialog = false;
  int _currentMatchIndex = 0;
  String balance='0';
  GetOfferResponse? offerResponse;
  String userId='0';
  List<Widget> tabs=<Widget>[];
  List<WinnerScoreCardItem> breakupList=[];
  List<JoinedContestTeam> leaderboardList=[];
  final CustomTimerController controller = CustomTimerController();
  void getOffer(String challengeId )async{
    AppLoaderProgress.showLoader(context);
    final client=ApiClient(AppRepository.dio);
    GeneralRequest request=GeneralRequest(user_id: userId,challenge_id: challengeId.toString());
    final response=await client.getOffer(request);
    if(response.status==1){
      AppLoaderProgress.hideLoader(context);
      offerResponse=response;
      bottomSheet(offerResponse!);
    }
    else{
      AppLoaderProgress.hideLoader(context);
      Fluttertoast.showToast(msg: response.message!);
    }
    // AppLoaderProgress.hideLoader(context);
  }
  bottomSheet(GetOfferResponse Response){
    showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      enableDrag: true,
      backgroundColor: Colors.transparent,
      shape : RoundedRectangleBorder(
          borderRadius : BorderRadius.only(topLeft: Radius.circular(40),topRight: Radius.circular(40))
      ),
      builder: (context) {
        return DraggableScrollableSheet(

          // expand: true,
            initialChildSize: 0.30,
            minChildSize: 0.15,
            maxChildSize:1,
            builder: (context,controller) {
              return Container(
                //height: MediaQuery.of(context).size.height*0.5,
                decoration: BoxDecoration(
                    color: Colors.white,borderRadius: BorderRadius.only(topLeft: Radius.circular(40),topRight: Radius.circular(40))
                ),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: SingleChildScrollView(
                    controller: controller,
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: (){
                            Navigator.pop(context);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.grey,borderRadius: BorderRadius.circular(10)
                            ),
                            width: 60,
                            height: 8,
                          ),
                        ),
                        SizedBox(height: 10,),
                        Image.asset(AppImages.addIcon,scale: 3,),
                        SizedBox(height: 10,),
                        Column(
                          children: [
                            Container(
                              color: lightBlack,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Team NO.',style: TextStyle(
                                        color: Colors.white
                                    ),),
                                    Text('Entry Fee',style: TextStyle(
                                        color: Colors.white
                                    ),),
                                    Text('Offer Entry',style: TextStyle(
                                        color: Colors.white
                                    ),),

                                  ],
                                ),
                              ),
                            ),
                            ListView.builder(
                                physics: NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemCount: offerResponse!.result!.length,
                                itemBuilder: (context,index){

                                  return Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Expanded(
                                          child: Text(Response.result![index].team!,style: TextStyle(
                                              color: Colors.black,fontWeight: FontWeight.bold
                                          ),),
                                        ),
                                        Expanded(
                                          child: Center(
                                            child: Text(Response.result![index].entryfee.toString(),style: TextStyle(
                                                color: Colors.black,fontWeight: FontWeight.bold
                                            ),),
                                          ),
                                        ),
                                        Expanded(
                                          child: Center(
                                            child: Container(
                                              margin:EdgeInsets.only(left: 30),
                                              child: Text(Response.result![index].offer.toString(),style: TextStyle(
                                                  color: Colors.black,fontWeight: FontWeight.bold
                                              ),),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  );
                                })
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }
        );
      },
    );
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppPrefrence.getString(AppConstants.KEY_USER_BALANCE)
        .then((value) =>
    {
      setState(() {
        balance = value;
      })
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getWinnerPriceCard();
      })
    });
    widget.contest.isjoined=widget.contest.isjoined??false;
    widget.contest.pdf=widget.contest.pdf??'';
    widget.model.onSwitchTeam=onSwitchTeam;
    widget.model.onSwitchTeamResult=onSwitchTeamResult;
  }
  Widget btnJoinText(){
    return Text(
      widget.contest.isjoined!?widget.contest.multi_entry==1?'JOIN+':'INVITE CONTEST':'Join Contest Now',
      style: TextStyle(fontSize: 15,color: Colors.white),
    );
  }
  Widget btnEntryText(){
    return new Text(
      widget.contest.is_free==1||widget.contest.is_first_time_free==1?'FREE':widget.contest.isjoined!=null&&widget.contest.isjoined!?widget.contest.multi_entry==1?'JOIN+':'INVITE':'₹'+widget.contest.entryfee.toString(),
      style: TextStyle(fontFamily: 'noway',fontSize: 14,fontWeight: FontWeight.w500,color: Colors.white,),
      textAlign: TextAlign.center,
    );
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    // TODO: implement build
    return SafeArea(
      child: new Scaffold(
          body: new WillPopScope(
              child: new Stack(
                children: [
                  new Scaffold(
                    backgroundColor: bgColorDark,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(55), // Set this height
                      child: Container(
                        color: Colors.black,
                        // padding: EdgeInsets.only(top: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Row(
                              children: [
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: () => {Navigator.pop(context)},
                                  child: new Container(
                                    padding: EdgeInsets.fromLTRB(15, 15, 25, 15),
                                    alignment: Alignment.centerLeft,
                                    child: new Container(
                                      width: 16,
                                      height: 16,
                                      child: Image(
                                          image:
                                          AssetImage(AppImages.backImageURL),
                                          fit: BoxFit.fill),
                                    ),
                                  ),
                                ),
                                new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      width: 150,
                                      child: new Text(widget.model.teamVs!,
                                          style: TextStyle(
                                              fontFamily: AppConstants.textBold,
                                              color: Colors.white,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16)),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.only(top: 5),
                                      child: CustomTimer(
                                        controller: controller,
                                        from: Duration(
                                            milliseconds:
                                            SuperDateFormat.getFormattedDateObj(
                                                widget.model
                                                    .headerText!)!
                                                .millisecondsSinceEpoch -
                                                MethodUtils.getDateTime()
                                                    .millisecondsSinceEpoch),
                                        to: Duration(milliseconds: 0),
                                        onBuildAction:
                                        CustomTimerAction.auto_start,
                                        builder:
                                            (CustomTimerRemainingTime remaining) {
                                          if(remaining.hours=='00'&&remaining.minutes=='00'&& remaining.seconds=='00') {
                                            navigateToHomePage(context);
                                          }
                                          /*    if(remaining.hours=='00'&&remaining.minutes=='00'&& remaining.seconds=='00')
                                          {
                                            controller.pause();
                                            // // widget.onMatchTimeUp(widget.index);
                                            // widget.getController.TimeFunction(remaining.minutes, remaining.seconds, widget.index);

                                          }*/
                                          return new GestureDetector(
                                            behavior: HitTestBehavior.translucent,
                                            child: Text(
                                              int.parse(remaining.days) >= 1
                                                  ? int.parse(remaining.days) > 1
                                                  ? "${remaining.days} Days "
                                                  : " ${remaining.days} Day left"
                                                  : int.parse(remaining.hours) >=
                                                  1
                                                  ? "${remaining.hours}Hrs : ${remaining.minutes}Mins left"
                                                  : "${remaining.minutes}Mins : ${remaining.seconds}Sec left",
                                              style: TextStyle(
                                                  fontFamily: 'noway',
                                                  color: Colors.white,
                                                  decoration: TextDecoration.none,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 12),
                                            ),
                                            onTap: () {},
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            new Container(
                              width: 150,
                              alignment: Alignment.center,
                              child: new Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  // new GestureDetector(
                                  //   behavior: HitTestBehavior.translucent,
                                  //   child: new Container(
                                  //     height: 20,
                                  //     width: 20,
                                  //     child: Image(
                                  //       image: AssetImage(
                                  //           AppImages.createContestIcon),
                                  //     ),
                                  //   ),
                                  //   onTap: ()=>{
                                  //     _modalBottomSheetMenu()
                                  //   },
                                  // ),

                                  new GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    child: new Container(
                                      child: new Row(
                                        children: [
                                          // new Text('₹'+balance,
                                          //     style: TextStyle(
                                          //         fontFamily: AppConstants.textBold,
                                          //         color: Colors.white,
                                          //         fontWeight: FontWeight.normal,
                                          //         fontSize: 15)),
                                          new Container(
                                            margin: EdgeInsets.only(
                                                left: 5, right: 10),
                                            height: 20,
                                            width: 20,
                                            child: Image(
                                                image: AssetImage(
                                                    AppImages.walletImageURL)),
                                          )
                                        ],
                                      ),
                                    ),
                                    onTap: () => {navigateToWallet(context)},
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    body: new Stack( alignment: Alignment.bottomCenter,
                      children: [
                        new Container(
                          height: MediaQuery.of(context).size.height,
                          color: Colors.white,
                          child: new Column(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              // new MatchHeader(widget.model.teamVs!,widget.model.headerText!),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: new Card(
                                  clipBehavior: Clip.antiAlias,
                                  elevation: 0,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),side: BorderSide(color: Colors.grey.shade300)),
                                  child: Container(
                                    child: Column(
                                      children: [
                                        widget.contest.is_champion==1?Container(
                                          child: Column(
                                            children: [
                                              SizedBox(height: 10,),
                                              Center(child: Text('Score more than me to',style: TextStyle(
                                                  fontWeight: FontWeight.bold
                                              ),)),
                                              SizedBox(height: 10,),
                                              Padding(
                                                padding: const EdgeInsets.only(left: 8.0,right: 8),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [

                                                    Image.network(widget.contest.champion_player!,fit: BoxFit.contain,scale: 3.5,
                                                      loadingBuilder:(context, Widget child, loadingProgress) {
                                                        if (loadingProgress == null) return child;
                                                        return Center(
                                                          child: CircularProgressIndicator(

                                                          ),
                                                        );
                                                      },

                                                    ),
                                                    Stack(
                                                      children: [
                                                        Container(
                                                          height: 50,
                                                          width: 150,
                                                          decoration: BoxDecoration(
                                                              border: Border.all(color: Themecolor),
                                                              borderRadius: BorderRadius.circular(5)
                                                          ),
                                                          child: Padding(
                                                            padding: const EdgeInsets.only(left: 5,right: 5),
                                                            child: Center(child:Row(
                                                              mainAxisAlignment: MainAxisAlignment.center,
                                                              children: [
                                                                Text(widget.contest.entryfee.toString()),
                                                                Text(widget.contest.champion_x!,style: TextStyle(color: Themecolor),),
                                                                Text(" = "+widget.contest.win_amount!),
                                                              ],
                                                            )),
                                                          ),
                                                        ),
                                                        Positioned(
                                                          right: 40,left: 40,bottom:43 , child: Container(
                                                          width: 50,
                                                          color: Colors.white,
                                                          child: Center(
                                                            child: Text('WIN',style: TextStyle(
                                                                color: Themecolor,fontWeight: FontWeight.bold,fontSize: 12
                                                            ),),
                                                          ),
                                                        ),
                                                        )
                                                      ],
                                                    ),
                                                    new GestureDetector(
                                                      behavior: HitTestBehavior.translucent,
                                                      child: new Container(
                                                        alignment: Alignment.centerRight,
                                                        child: new Card(
                                                          color: greenColor,
                                                          child: new Container(
                                                            height: 30,
                                                            width: 65,
                                                            alignment: Alignment.center,
                                                            child: btnEntryText(),
                                                          ),
                                                        ),
                                                      ),
                                                      onTap: (){
                                                        if((btnEntryText() as Text).data=='INVITE'){
                                                          MethodUtils.onShare(context,widget.contest,widget.model);
                                                        }else{
                                                          if(widget.model.teamCount==1){
                                                            MethodUtils.checkBalance(new JoinChallengeDataModel(context, 0, int.parse(userId), widget.contest.id!, widget.model.fantasyType!, widget.model.slotId!, 1, widget.contest.is_bonus!, widget.contest.win_amount!, widget.contest.maximum_user!, widget.model.teamId.toString(), widget.contest.entryfee.toString(), widget.model.matchKey!, widget.model.sportKey!, widget.model.joinedSwitchTeamId.toString(), false, 0, 0,onJoinContestResult));
                                                          }else if(widget.model.teamCount!>0){
                                                            navigateToMyJoinTeams(context,widget.model,widget.contest,onJoinContestResult);
                                                          }else{
                                                            widget.model.onJoinContestResult=onJoinContestResult;
                                                            widget.model.contest=widget.contest;
                                                            widget.model.onTeamCreated=onTeamCreated;
                                                            navigateToCreateTeam(context,widget.model);
                                                          }
                                                        }
                                                      },
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ): Padding(
                                          padding: const EdgeInsets.all(5),
                                          child: new Column(
                                            children: [
                                              new Container(
                                                child: new Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,

                                                  children: [
                                                    new Container(
                                                      margin: EdgeInsets.only(left: 10),
                                                      child: new Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        mainAxisAlignment: MainAxisAlignment.start,
                                                        children: [
                                                          widget.contest.is_offer_team==1?GestureDetector(
                                                              onTap:(){
                                                                getOffer(widget.contest.id.toString());
                                                              },
                                                              child: Image.asset(AppImages.addIcon,scale: 3,)):Row(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisAlignment: MainAxisAlignment.start,
                                                            children: [
                                                              Image(
                                                                  image: AssetImage(
                                                                    AppImages.prizepool,),height: 10),
                                                              SizedBox(width: 5,),
                                                              Text(
                                                                'Prize Pool',
                                                                style: TextStyle(
                                                                    fontFamily: 'noway',
                                                                    color: Colors.grey,
                                                                    fontWeight: FontWeight.w500,
                                                                    fontSize: 12),
                                                              ),
                                                            ],
                                                          ),
                                                          widget.contest.is_gadget==1?Image.network(widget.contest.gadget_image!,fit: BoxFit.contain,scale: 5,):new Container(
                                                            height: 30,
                                                            alignment: Alignment.centerLeft,
                                                            child: Text(
                                                              (widget.contest.is_giveaway_visible_text==0?'₹'+widget.contest.win_amount.toString():'₹'+widget.contest.is_giveaway_text!),
                                                              style: TextStyle(
                                                                  fontFamily: 'noway',
                                                                  color: widget.contest.is_giveaway_visible_text==0?Colors.black:MethodUtils.hexToColor(widget.contest.giveaway_color!),
                                                                  fontWeight: FontWeight.w500,
                                                                  fontSize: 18),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                    Column(children: [
                                                      Row(
                                                        children: [
                                                          Image(
                                                              image: AssetImage(
                                                                AppImages.winnerMedalIcon,),height: 11),
                                                          SizedBox(width: 5,),
                                                          Text(
                                                            'First prize'.toUpperCase(),
                                                            style: TextStyle(
                                                                fontFamily: 'noway',
                                                                color: Colors.grey,
                                                                fontWeight: FontWeight.w500,
                                                                fontSize: 12),
                                                          ),

                                                        ],
                                                      ),
                                                      SizedBox(height: 5,),
                                                      widget.contest.challenge_type!='percentage'?new Row(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          new Text('₹'+widget.contest.first_rank_prize!.toString(),
                                                              style: TextStyle(
                                                                  fontFamily: 'noway',
                                                                  color: Colors.black,
                                                                  fontWeight: FontWeight.w500,
                                                                  fontSize: 16)),

                                                        ],
                                                      ):new Container(),SizedBox(height: 10,),],),
                                                    new Container(
                                                      margin: EdgeInsets.only(right: 10),
                                                      child: new Column(
                                                        crossAxisAlignment: CrossAxisAlignment.end,
                                                        children: [
                                                          new Container(
                                                            margin: EdgeInsets.only(right: 5,top: 5),
                                                            child: Row(
                                                              children: [
                                                                Image(
                                                                    image: AssetImage(
                                                                      AppImages.entryfee,),height: 12),
                                                                SizedBox(width: 5,),
                                                                Text(
                                                                  'Entry Fee'.toUpperCase(),
                                                                  style: TextStyle(
                                                                      fontFamily: 'noway',
                                                                      color: Colors.grey,
                                                                      fontWeight: FontWeight.w500,
                                                                      fontSize: 12),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                          // SizedBox(height: 5,),
                                                          new GestureDetector(
                                                            behavior: HitTestBehavior.translucent,
                                                            child: new Container(
                                                              alignment: Alignment.centerRight,
                                                              child: new Card(
                                                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
                                                                color: yellowdark,
                                                                child: new Container(
                                                                  height: 30,
                                                                  width: 65,
                                                                  alignment: Alignment.center,
                                                                  child: btnEntryText(),
                                                                ),
                                                              ),
                                                            ),
                                                            onTap: (){
                                                              if((btnEntryText() as Text).data=='INVITE'){
                                                                MethodUtils.onShare(context,widget.contest,widget.model);
                                                              }else{
                                                                if(widget.model.teamCount==1){
                                                                  MethodUtils.checkBalance(new JoinChallengeDataModel(context, 0, int.parse(userId), widget.contest.id!, widget.model.fantasyType!, widget.model.slotId!, 1, widget.contest.is_bonus!, widget.contest.win_amount!, widget.contest.maximum_user!, widget.model.teamId.toString(), widget.contest.entryfee.toString(), widget.model.matchKey!, widget.model.sportKey!, widget.model.joinedSwitchTeamId.toString(), false, 0, 0,onJoinContestResult));
                                                                }else if(widget.model.teamCount!>0){
                                                                  navigateToMyJoinTeams(context,widget.model,widget.contest,onJoinContestResult);
                                                                }else{
                                                                  widget.model.onJoinContestResult=onJoinContestResult;
                                                                  widget.model.contest=widget.contest;
                                                                  widget.model.onTeamCreated=onTeamCreated;
                                                                  navigateToCreateTeam(context,widget.model);
                                                                }
                                                              }
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),

                                              new Container(
                                                margin: EdgeInsets.only(left: 8,right: 8,),
                                                child: new CustomProgressIndicator(widget.contest.challenge_type=='percentage'?0.5:double.parse((widget.contest.joinedusers!/widget.contest.maximum_user!).toString())),
                                              ),
                                              new Container(
                                                height: 20,
                                                margin: EdgeInsets.only(top: 5),
                                                child:widget.contest.maximum_user!-widget.contest.joinedusers!>0?new Row(
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [

                                                    new Container(
                                                      margin: EdgeInsets.only(left: 10),
                                                      // child: Text(widget.contest.maximum_user!.toString()+" Spot Left",
                                                      // child: Text(widget.contest.joinedusers!.toString()+" Spot Left",
                                                      child: Text(widget.contest.challenge_type=='percentage'?widget.contest.joinedusers!.toString()+'/':widget.contest.is_flexible==1?widget.contest.joinedusers!.toString()+'/'+widget.contest.maximum_user!.toString():widget.contest.maximum_user!-widget.contest.joinedusers!>0?NumberFormat.decimalPattern('hi').format(widget.contest.maximum_user!-widget.contest.joinedusers!).toString()+' Spots Left':'Challenge Closed',
                                                        style: TextStyle(
                                                            fontFamily: 'noway',
                                                            color: Title_Color_2,
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: 12),
                                                      ),
                                                    ),
                                                    new Container(
                                                      margin: EdgeInsets.only(left: 10,right: 10),

                                                      child: Text(widget.contest.maximum_user!.toString()+" Spot",
                                                        style: TextStyle(
                                                            fontFamily: 'noway',
                                                            color: Colors.grey,
                                                            fontWeight: FontWeight.w500,
                                                            fontSize: 12),
                                                      ),
                                                    ),
                                                    // new Row(
                                                    //   children: [
                                                    //     // new Container(
                                                    //     //   margin: EdgeInsets.only(left: 7,right: 2),
                                                    //     //   height: 14,
                                                    //     //   padding: EdgeInsets.only(left: 2,right: 2),
                                                    //     //   alignment: Alignment.center,
                                                    //     //   decoration: BoxDecoration(
                                                    //     //       border: Border.all(color: Colors.grey),
                                                    //     //       borderRadius: BorderRadius.circular(2)
                                                    //     //   ),
                                                    //     //   child: new Text('WD',
                                                    //     //       style: TextStyle(
                                                    //     //           fontFamily: 'noway',
                                                    //     //           color: Colors.grey,
                                                    //     //           fontWeight: FontWeight.w500,
                                                    //     //           fontSize: 10)),
                                                    //     // ),
                                                    //     widget.contest.confirmed_challenge==1?new Container(
                                                    //       margin: EdgeInsets.only(left: 5,right: 10),
                                                    //       height: 13,
                                                    //       width: 13,
                                                    //       child: Image(
                                                    //         image: AssetImage(
                                                    //           AppImages.confirmIcon,),color: Colors.grey,),
                                                    //     ):new Container(),
                                                    //   ],
                                                    // ),
                                                  ],
                                                ):new Container(
                                                  margin: EdgeInsets.only(left: 10),
                                                  child: Text('Challenge Closed',
                                                    style: TextStyle(
                                                        fontFamily: 'noway',
                                                        color: Themecolor,
                                                        fontWeight: FontWeight.w500,
                                                        fontSize: 12),
                                                  ),
                                                ),

                                              ),

                                            ],
                                          ),
                                        ),

                                        new Container(

                                          color:TextFieldColor,
                                          height: 35,
                                          margin: EdgeInsets.only(top:widget.contest.is_champion==1?0: 10),
                                          child: new Container(
                                            child: new Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                widget.contest.is_champion==1?Row(
                                                  children: [
                                                    SizedBox(width: 5,),
                                                    Image.asset(AppImages.addIcon,scale: 3,),
                                                    SizedBox(width: 5,),
                                                    Text('Joined Teams: ${widget.contest.joinedusers} ',style: TextStyle(
                                                        color: Colors.black,fontWeight: FontWeight.bold,fontSize: 12
                                                    ),),
                                                  ],
                                                ): new Row(
                                                  children: [

                                                    new GestureDetector(
                                                      behavior: HitTestBehavior.translucent,
                                                      child: new Row(
                                                        children: [
                                                          new Container(
                                                            margin: EdgeInsets.only(left: 10,right: 2),
                                                            height: 16,
                                                            width: 20,
                                                            child: Image(
                                                              image: AssetImage(
                                                                AppImages.winnersIcon,),),
                                                          ),
                                                          new Text(widget.contest.challenge_type=='percentage'?widget.contest.winning_percentage.toString()+'% Win':widget.contest.totalwinners.toString()+' Team Win',
                                                              style: TextStyle(
                                                                  fontFamily: 'noway',
                                                                  color: Colors.grey,
                                                                  fontWeight: FontWeight.w500,
                                                                  fontSize: 10)),

                                                        ],
                                                      ),
                                                      onTap: (){
                                                        // widget.getWinnerPriceCard(widget.contest.id,widget.contest.win_amount.toString());
                                                      },
                                                    ),


                                                    widget.contest.is_bonus==1?new Row(
                                                      children: [
                                                        new Container(
                                                          margin: EdgeInsets.only(left: 7,right: 2),
                                                          height: 12,
                                                          width: 14,
                                                          child: Image(
                                                            image: AssetImage(
                                                              AppImages.bonusIcon,),),
                                                        ),
                                                        new Text(widget.contest.bonus_percent!,
                                                            style: TextStyle(
                                                                fontFamily: 'noway',
                                                                color: Colors.grey,
                                                                fontWeight: FontWeight.w500,
                                                                fontSize: 10)),

                                                      ],
                                                    ):new Container()
                                                  ],
                                                ),
                                                new Row(
                                                  children: [
                                                    // new Container(
                                                    //   margin: EdgeInsets.only(left: 7,right: 2),
                                                    //   height: 14,
                                                    //   padding: EdgeInsets.only(left: 2,right: 2),
                                                    //   alignment: Alignment.center,
                                                    //   decoration: BoxDecoration(
                                                    //       border: Border.all(color: Colors.grey),
                                                    //       borderRadius: BorderRadius.circular(2)
                                                    //   ),
                                                    //   child: new Text('WD',
                                                    //       style: TextStyle(
                                                    //           fontFamily: 'noway',
                                                    //           color: Colors.grey,
                                                    //           fontWeight: FontWeight.w500,
                                                    //           fontSize: 10)),
                                                    // ),

                                                    widget.contest.confirmed_challenge==1?new Container(
                                                      margin: EdgeInsets.only(left: 7,right: 10),
                                                      height: 14,
                                                      width: 14,
                                                      alignment: Alignment.center,
                                                      decoration: BoxDecoration(
                                                          border: Border.all(color: Colors.grey),
                                                          borderRadius: BorderRadius.circular(2)
                                                      ),
                                                      child: new Text('c',
                                                          style: TextStyle(
                                                              fontFamily: 'noway',
                                                              color: Colors.grey,
                                                              fontWeight: FontWeight.w500,
                                                              fontSize: 11)),
                                                    ):new Container(),

                                                    widget.contest.multi_entry==1?new Row(
                                                      children: [
                                                        new Container(
                                                          margin: EdgeInsets.only(left: 7,right: 7),
                                                          height: 13,
                                                          // width: 14,
                                                          alignment: Alignment.center,
                                                          decoration: BoxDecoration(
                                                              border: Border.all(color: Colors.grey),
                                                              borderRadius: BorderRadius.circular(2)
                                                          ),
                                                          child: Row(
                                                            children: [
                                                              Container(

                                                                decoration:BoxDecoration(
                                                                    border: Border(
                                                                        right: BorderSide(color: Colors.grey)
                                                                    )
                                                                ),
                                                                child: new Text(' M ',
                                                                    style: TextStyle(
                                                                        fontFamily: 'noway',
                                                                        color: Colors.grey,
                                                                        fontWeight: FontWeight.w500,
                                                                        fontSize: 10)),
                                                              ),
                                                              new Text(' '+widget.contest.max_multi_entry_user!.toString()+" ",
                                                                  style: TextStyle(
                                                                      fontFamily: 'noway',
                                                                      color: Colors.grey,
                                                                      fontWeight: FontWeight.w500,
                                                                      fontSize: 9)
                                                              ),
                                                            ],
                                                          ),
                                                        ),



                                                      ],
                                                    ):new Container(),
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),

                              new Expanded(
                                child:new DefaultTabController(
                                  length: 2,
                                  child: Scaffold(
                                    // backgroundColor: bgColorDark,
                                    // backgroundColor: Colors.green,
                                    appBar: PreferredSize(
                                      preferredSize: Size.fromHeight(40),
                                      child: new Container(
                                        margin: EdgeInsets.symmetric(horizontal: 20),
                                        color: Colors.white,
                                        child: TabBar(
                                          labelPadding: EdgeInsets.all(0),
                                          onTap: (int index) {
                                            setState(() {
                                              _currentMatchIndex = index;
                                              if(_currentMatchIndex==0){
                                                getWinnerPriceCard();
                                              }else{
                                                getLeaderboardList();
                                              }
                                            });
                                          },
                                          indicatorWeight: 2,
                                          indicatorSize: TabBarIndicatorSize.label,
                                          indicatorColor: Themecolor,
                                          // indicator: ShapeDecoration(
                                          //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10))),
                                          //     color: Colors.black
                                          // ),
                                          isScrollable:false,
                                          tabs: getTabs(),
                                        ),
                                      ),
                                    ),
                                    body: _currentMatchIndex==0?new WinningBreakups(breakupList):new Leaderboard(leaderboardList,userId,true,widget.contest.pdf!,widget.model),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        widget.contest.maximum_user!-widget.contest.joinedusers!>0?new Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60,
                            color: Colors.white,
                            padding: EdgeInsets.symmetric(horizontal: 40,vertical: 10),

                            child: ElevatedButton(
                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),elevation: MaterialStateProperty.all(0) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)))),
                              child: btnJoinText(),
                              onPressed: () {
                                if((btnJoinText() as Text).data=='Join Contest Now' ||(btnJoinText() as Text).data=='JOIN+'){
                                  if(widget.model.teamCount==1){
                                    MethodUtils.checkBalance(new JoinChallengeDataModel(context, 0, int.parse(userId), widget.contest.id!, widget.model.fantasyType!, widget.model.slotId!, 1, widget.contest.is_bonus!, widget.contest.win_amount!, widget.contest.maximum_user!, widget.model.teamId.toString(), widget.contest.entryfee.toString(), widget.model.matchKey!, widget.model.sportKey!, widget.model.joinedSwitchTeamId.toString(), false, 0, 0,onJoinContestResult));
                                  }else if(widget.model.teamCount!>0){
                                    navigateToMyJoinTeams(context,widget.model,widget.contest,onJoinContestResult);
                                  }else{
                                    widget.model.onJoinContestResult=onJoinContestResult;
                                    widget.model.contest=widget.contest;
                                    navigateToCreateTeam(context,widget.model);
                                  }
                                }else{
                                  MethodUtils.onShare(context,widget.contest,widget.model);
                                }

                              },
                            )):new Container(),
                      ],
                    ),
                  ),
                ],
              ),
              onWillPop: _onWillPop)),
    );
  }




  Future<bool> _onWillPop() async {
    setState(() {
      Navigator.pop(context);
    });
    return Future.value(false);
  }
  List<Widget> getTabs(){
    tabs.clear();
    tabs.add(getWinningTab());
    tabs.add(getLeaderboardTab());
    return tabs;
  }
  Widget getWinningTab(){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Text(
          'Winning Breakup',
          style: TextStyle(
              // fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 0
                  ? primaryColor
                  : Colors.grey,
              fontWeight: FontWeight.w500,
              fontSize: 14),
        ),
      ),
    );
  }
  Widget getLeaderboardTab(){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Text(
          'Leaderboard',
          style: TextStyle(
              // fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 1
                  ? primaryColor
                  : Colors.grey,
              fontWeight: FontWeight.w500,
              fontSize: 14),
        ),
      ),
    );
  }
  void favouriteContest() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest request = new GeneralRequest(user_id: userId,challenge_id: widget.contest.real_challenge_id.toString(),my_fav_contest: (widget.contest.is_fav_contest==1?0:1).toString(),);
    final client = ApiClient(AppRepository.dio);
    GeneralResponse response = await client.getFavouriteContest(request);
    if (response.status == 1) {
      widget.contest.is_fav_contest=widget.contest.is_fav_contest==1?0:1;
    }
    Fluttertoast.showToast(msg: response.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
  void getWinnerPriceCard() async {
    AppLoaderProgress.showLoader(context);
    ContestRequest request = new ContestRequest(user_id: userId,challenge_id: widget.contest.id.toString(),matchkey: widget.model.matchKey,);
    final client = ApiClient(AppRepository.dio);
    ScoreCardResponse response = await client.getWinnersPriceCard(request);
    if (response.status == 1) {
      breakupList=response.result!;
    }
    // Fluttertoast.showToast(msg: response.message, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
  void getLeaderboardList() async {
    AppLoaderProgress.showLoader(context);
    ContestRequest request = new ContestRequest(user_id: userId,challenge_id: widget.contest.id.toString(),matchkey: widget.model.matchKey,sport_key: widget.model.sportKey);
    final client = ApiClient(AppRepository.dio);
    ContestDetailResponse response = await client.getLeaderboardList(request);
    if (response.status == 1) {
      leaderboardList=response.result!.contest!;
    }
    // Fluttertoast.showToast(msg: response.message, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
  void onJoinContestResult(int isJoined,String referCode){
    widget.contest.isjoined=true;
    widget.contest.refercode=referCode;
    widget.contest.joinedusers=widget.contest.joinedusers!+1;
    setState(() {});
  }
  void onTeamCreated(){
    widget.model.teamCount=widget.model.teamCount!+1;
    setState(() {});
  }
  void onSwitchTeam(int id){
    widget.model.isSwitchTeam=true;
    widget.model.joinedSwitchTeamId=id;
    navigateToMyJoinTeams(context,widget.model,widget.contest,onJoinContestResult);
  }
  void onSwitchTeamResult(){
    getLeaderboardList();
  }
  void getWinnerPriceCardSheet(int id,String winAmount) async {
    AppLoaderProgress.showLoader(context);
    ContestRequest request = new ContestRequest(user_id: userId,challenge_id: id.toString(),matchkey: widget.model.matchKey,);
    final client = ApiClient(AppRepository.dio);
    ScoreCardResponse response = await client.getWinnersPriceCard(request);
    if (response.status == 1) {
      breakupList=response.result!;
      MethodUtils.showWinningPopup(context,breakupList,winAmount);
    }
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
}
