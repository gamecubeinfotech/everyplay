 import 'dart:async';
import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/ShimmerContestItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/appUtilities/date_utils.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/CustomWidget.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/banner_response.dart';
import 'package:EverPlay/repository/model/category_contest_response.dart';
import 'package:EverPlay/repository/model/contest_request.dart';
import 'package:EverPlay/repository/model/contest_response.dart';
import 'package:EverPlay/repository/model/score_card_response.dart';
import 'package:EverPlay/repository/model/team_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:EverPlay/views/More.dart';
import 'package:EverPlay/views/MyJoinedContests.dart';
import 'package:EverPlay/views/MyMatches.dart';
import 'package:EverPlay/views/MyTeams.dart';
import 'package:EverPlay/views/NoMatchesListFound.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'Account.dart';
import 'NoTeamsFound.dart';

class UpcomingContests extends StatefulWidget {
  GeneralModel model;
  UpcomingContests(this.model);
  @override
  _UpcomingContestsState createState() => _UpcomingContestsState();
}

class _UpcomingContestsState extends State<UpcomingContests> {
  int _currentSportIndex = 0;
  int _currentIndex = 0;
  bool isExtend=true;
  var title='Home';
  String balance='0';
  String userId='0';
  String inningText='';
  bool back_dialog = false;
  int _currentMatchIndex = 0;
  int selectedFantasyType= 0;
  int selectedSlotId= 0;
  int teamCount= 0;
  int joinedContestCount= 0;
  int totalContest= 0;
  int teamId= 0;
  bool contestLoading=false;
  List<int> fantasyTypes=[];
  List<Widget> tabs=<Widget>[];
  List<Widget> inningTabs=<Widget>[];
  List<CategoriesItem> categoriesList=[];
  List<Contest> joinedContestList=[];
  List<Team> teamsList=[];
  var _stackToView = 0;
  static bool showBatFantasySheet = false,
      showBowlFantasySheet = false,
      showReverseFantasySheet = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppPrefrence.getString(AppConstants.KEY_USER_BALANCE)
        .then((value) =>
    {
      setState(() {
        balance = value;
      })
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getContestByCategory();
      })
    });
    startTimeCount();
    // if(widget.model.bannerImage!=null&&widget.model.bannerImage!.isNotEmpty){
    //   _showPopImage(widget.model.bannerImage);
    // }
    widget.model.fantasyType=0;
    widget.model.slotId=0;
    if(widget.model.liveFantasy==1){
      selectedFantasyType=1;
      widget.model.fantasyType=selectedFantasyType;
      selectedSlotId=widget.model.fantasySlots![0].id!;
      widget.model.slotId=selectedSlotId;
      inningText="'Live Fantasy - " + widget.model.fantasySlots![0].inning.toString() + "st  Inning " + widget.model.fantasySlots![0].min_over.toString() + "-" + widget.model.fantasySlots![0].max_over.toString() + " Overs' Contest";
    }
  }
  startTimeCount() async {
    var _duration = const Duration(seconds: 2);
    return Timer(_duration, navigateInside);
  }
  void navigateInside() => {
    setState(() {
      isExtend = false;
    })
  };
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    // TODO: implement build
    return SafeArea(
      child: new Scaffold(
          body: new WillPopScope(
              child: new Stack(
                children: [
                  new Scaffold(
                    backgroundColor: whiteColor,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(55), // Set this height
                      child: Container(
                        color: Colors.black,
                        // padding: EdgeInsets.only(top: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Row(
                              children: [
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: ()=>{
                                    Navigator.pop(context)
                                  },
                                  child: new Container(
                                    padding: EdgeInsets.fromLTRB(15,15,25,15),
                                    alignment: Alignment.centerLeft,
                                    child: new Container(
                                      width: 16,
                                      height: 16,
                                      child: Image(
                                          image: AssetImage(AppImages.backImageURL),
                                          fit: BoxFit.fill),
                                    ),
                                  ),
                                ),
                                new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                      width: 150,
                                      child: new Text( widget.model.teamVs!,
                                          style: TextStyle(
                                              fontFamily: AppConstants.textBold,
                                              color: Colors.white,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 16)),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.only(top: 5),
                                      child: CustomTimer(
                                        from: Duration(milliseconds: SuperDateFormat.getFormattedDateObj(widget.model.headerText!)!.millisecondsSinceEpoch-MethodUtils.getDateTime().millisecondsSinceEpoch),
                                        to: Duration(milliseconds: 0),
                                        onBuildAction: CustomTimerAction.auto_start,
                                        builder: (CustomTimerRemainingTime remaining) {

                                          if(remaining.hours=='00'&&remaining.minutes=='00'&& remaining.seconds=='00') {
                                            navigateToHomePage(context);
                                          }

                                          return new GestureDetector(
                                            behavior: HitTestBehavior.translucent,
                                            child: Text(
                                              int.parse(remaining.days)>=1?int.parse(remaining.days)>1?"${remaining.days} Days":"${remaining.days} Day":int.parse(remaining.hours)>=1?" ${remaining.hours}Hrs : ${remaining.minutes}Min":"${remaining.minutes}Min :${remaining.seconds}Sec " +"Left",
                                              style: TextStyle(
                                                  fontFamily: 'noway',
                                                  color: Colors.white,
                                                  decoration: TextDecoration.none,
                                                  fontWeight:
                                                  FontWeight.w500,
                                                  fontSize: 12),
                                            ),
                                            onTap: (){
                                            },
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            new Container(
                              width: 150,
                              alignment: Alignment.center,
                              child: new Row(
                                // crossAxisAlignment: CrossAxisAlignment.center,
                                 mainAxisAlignment: MainAxisAlignment.end,
                                // mainAxisSize: MainAxisSize.max,
                                children: [
                                  // new GestureDetector(
                                  //   behavior: HitTestBehavior.translucent,
                                  //   child: new Container(
                                  //     height: 20,
                                  //     width: 20,
                                  //     child: Image(
                                  //       image: AssetImage(
                                  //           AppImages.createContestIcon),
                                  //     ),
                                  //   ),
                                  //   onTap: ()=>{
                                  //     _modalBottomSheetMenu()
                                  //   },
                                  // ),
                                  new GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    child: new Container(
                                      child: new Row(
                                         children: [
                                        //   new Text('₹'+balance,
                                        //       style: TextStyle(
                                        //           fontFamily: AppConstants.textBold,
                                        //           color: Colors.white,
                                        //           fontWeight: FontWeight.normal,
                                        //           fontSize: 15)),
                                          new Container(
                                             margin: EdgeInsets.only(right: 10),
                                            height: 20,
                                            width: 20,
                                            child: Image(
                                                image: AssetImage(
                                                    AppImages.walletImageURL)),
                                          )
                                        ],
                                      ),
                                    ),
                                    onTap: ()=>{
                                      navigateToWallet(context)
                                    },
                                  ),

                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    body: new Stack(
                      children: [
                        DefaultTabController(
                          length: getFantasyTabCount(),
                          child: Scaffold(
                            // backgroundColor: Colors.white,
                            backgroundColor: whiteColor,

                            // appBar: PreferredSize(
                            //   preferredSize: Size.fromHeight(widget.model.sportKey==AppConstants.TAG_CRICKET&&widget.model.liveFantasy==0?55:0),
                            //   child: new Container(
                            //     color: Colors.white,
                            //     child: TabBar(
                            //       labelPadding: EdgeInsets.all(0),
                            //       onTap: (int index) {
                            //         setState(() {
                            //           _currentMatchIndex = index;
                            //           selectedFantasyType=fantasyTypes[_currentMatchIndex];
                            //           widget.model.fantasyType=selectedFantasyType;
                            //           if(_currentIndex==0){
                            //             getContestByCategory();
                            //           }else if(_currentIndex==1){
                            //             getJoinedContests();
                            //           }else{
                            //             getMyTeams();
                            //           }
                            //           if(selectedFantasyType==AppConstants.BATTING_FANTASY_TYPE&&!showBatFantasySheet){
                            //             showBatFantasySheet=true;
                            //             _bottomSheet();
                            //           }else if(selectedFantasyType==AppConstants.BOWLING_FANTASY_TYPE&&!showBowlFantasySheet){
                            //             showBowlFantasySheet=true;
                            //             _bottomSheet();
                            //           }else if(selectedFantasyType==AppConstants.REVERSE_FANTASY&&!showReverseFantasySheet){
                            //             showReverseFantasySheet=true;
                            //             _bottomSheet();
                            //           }
                            //
                            //         });
                            //       },
                            //       indicatorWeight: 2,
                            //       indicatorSize: TabBarIndicatorSize.label,
                            //       indicatorColor: primaryColor,
                            //       // indicator: ShapeDecoration(
                            //       //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10))),
                            //       //     color: Colors.black
                            //       // ),
                            //       isScrollable:false,
                            //       tabs: getTabs(),
                            //     ),
                            //   ),
                            // ),
                            body: new Column(
                              children: [

                                // Container(
                                //  // margin:EdgeInsets.only(left: 8,right: 8) ,
                                //    height: 45,
                                //
                                //
                                //   child: Row(
                                //     children: [
                                //
                                //       Expanded(
                                //         child: GestureDetector(
                                //           onTap: () {
                                //             onTabTapped(0);
                                //           },
                                //           child: Container(
                                //
                                //             decoration: BoxDecoration(
                                //               // borderRadius: BorderRadius.only(topLeft: Radius.circular(3),bottomLeft: Radius.circular(3)),
                                //                 color:
                                //                 Colors.white,
                                //                 border: Border(
                                //
                                //                     bottom: BorderSide(
                                //                         width: 2,
                                //                         color: _currentIndex == 0
                                //                             ? primaryColor
                                //                             : Colors.grey.shade400))),
                                //             child: Center(
                                //               child: Text(
                                //                 'Contests',
                                //                 style: TextStyle(
                                //                   color: _currentIndex == 0
                                //                       ? primaryColor
                                //                       : Colors.grey,
                                //                 ),
                                //               ),
                                //             ),
                                //           ),
                                //         ),
                                //       ),
                                //       Expanded(
                                //         child: GestureDetector(
                                //           onTap: () {
                                //             onTabTapped(1);
                                //           },
                                //           child: Container(
                                //             decoration: BoxDecoration(
                                //                 color: Colors.white,
                                //                 border: Border(
                                //
                                //                     bottom: BorderSide(
                                //                         width: 2,
                                //                         color: _currentIndex == 1
                                //                             ? primaryColor
                                //                             : Colors.grey.shade400))),
                                //             child: Center(
                                //               child: Text(
                                //                 'My Contests(' +
                                //                     joinedContestCount
                                //                         .toString() +
                                //                     ')',
                                //                 style: TextStyle(
                                //                     color: _currentIndex == 1
                                //                         ? primaryColor
                                //                         : Colors.grey,
                                //                     fontSize: 13),
                                //               ),
                                //             ),
                                //           ),
                                //         ),
                                //       ),
                                //       Expanded(
                                //         child: GestureDetector(
                                //           onTap: () {
                                //             onTabTapped(2);
                                //           },
                                //           child: Container(
                                //             decoration: BoxDecoration(
                                //               color: Colors.white,
                                //
                                //               border: Border(
                                //                   bottom: BorderSide(
                                //                     width: 2,
                                //                       color: _currentIndex == 2
                                //                           ? primaryColor
                                //                           : Colors.grey.shade400)),
                                //               // borderRadius: BorderRadius.only(topRight: Radius.circular(3),bottomRight: Radius.circular(3)),
                                //             ),
                                //             child: Center(
                                //               child: Text(
                                //                 'My Teams(' +
                                //                     teamCount.toString() +
                                //                     ')',
                                //                 style: TextStyle(
                                //                   color: _currentIndex == 2
                                //                       ? primaryColor
                                //                       : Colors.grey,
                                //                 ),
                                //               ),
                                //             ),
                                //           ),
                                //         ),
                                //       ),
                                //     ],
                                //   ),
                                // ),
                                // widget.model.liveFantasy==1?DefaultTabController(length: widget.model.fantasySlots!.length, child: new Container(
                                //   color: Colors.white,
                                //   child: TabBar(
                                //     labelPadding: EdgeInsets.all(0),
                                //     onTap: (int index) {
                                //       setState(() {
                                //         selectedSlotId=widget.model.fantasySlots![index].id??0;
                                //         inningText="'Live Fantasy - " + widget.model.fantasySlots![index].inning.toString() + "st  Inning " + widget.model.fantasySlots![index].min_over.toString() + "-" + widget.model.fantasySlots![index].max_over.toString() + " Overs' Contest";
                                //         if(_currentIndex==0){
                                //           getContestByCategory();
                                //         }else if(_currentIndex==1){
                                //           getJoinedContests();
                                //         }else{
                                //           getMyTeams();
                                //         }
                                //
                                //       });
                                //     },
                                //     indicatorWeight: 2,
                                //     indicatorSize: TabBarIndicatorSize.label,
                                //     indicatorColor: primaryColor,
                                //     labelColor: primaryColor,
                                //     unselectedLabelColor: Colors.grey,
                                //     // indicator: ShapeDecoration(
                                //     //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10))),
                                //     //     color: Colors.black
                                //     // ),
                                //     isScrollable:false,
                                //     tabs: getSlotTabs(),
                                //   ),
                                // )):Container(),
                                //
                                // widget.model.liveFantasy==1?Container(
                                //   width: MediaQuery.of(context).size.width,
                                //   color: Colors.white,
                                //   alignment: Alignment.center,
                                //   padding: EdgeInsets.all(5),
                                //   child: Text(
                                //     inningText,
                                //     style: TextStyle(
                                //         fontFamily: AppConstants.textBold,
                                //         color:Colors.grey,
                                //         fontWeight: FontWeight.w400,
                                //         fontSize: 11),
                                //   ),
                                // ):Container(),

                                Container(
                                  color:Colors.white,
                                  child: DefaultTabController(

                                    length: 3,
                                    child: TabBar(

                                      onTap: (int index) {
                                        setState(() {
                                          _currentIndex=index;
                                          if (_currentIndex == 0) {
                                            getContestByCategory();
                                          } else if (_currentIndex == 1) {
                                            getJoinedContests();
                                          } else {
                                            getMyTeams();
                                          }
                                        });
                                      }
                                      ,

                                       // labelPadding: EdgeInsets.all(10),
                                        indicatorWeight: 2,
                                        indicatorSize: TabBarIndicatorSize.label,
                                        indicatorColor: primaryColor,
                                        labelColor: primaryColor,
                                        unselectedLabelColor: Colors.grey,
                                        isScrollable: false,



                                        tabs: [
                                          Container(
                                            width: MediaQuery.of(context).size.width,
                                            height: 40,
                                            alignment: Alignment.center,
                                            child: Text('Contest',style: TextStyle(
                                                fontSize: 13,fontFamily: 'noway',fontWeight: FontWeight.w500,
                                            ),),
                                          ),
                                          Container(
                                            width: MediaQuery.of(context).size.width,
                                            height: 40,
                                          alignment: Alignment.center,
                                            child: Text( 'My Contests(' +
                                                joinedContestCount
                                                    .toString() +
                                                ')',style: TextStyle(
                                                fontSize: 13,fontFamily: 'noway',fontWeight: FontWeight.w500,),)
                                          ),
                                          Container(
                                            width: MediaQuery.of(context).size.width,
                                            height: 40,
                                            alignment: Alignment.center,
                                            child: Text( 'My Teams(' +
                                                teamCount.toString() +
                                                ')',style: TextStyle(
                                                fontSize: 13,fontFamily: 'noway',fontWeight: FontWeight.w500,)),
                                          ),
                                        ]),
                                  ),
                                ),
                                new Divider(height: 1,thickness: 1,),
                                Expanded(
                                    child:_currentIndex==0?RefreshIndicator(child: new SingleChildScrollView(child: new Column(
                                      children: [
                                        new Container(
                                          color:Colors.white,
                                          padding: EdgeInsets.all(10),
                                          child: Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                children: [
                                                  GestureDetector(
                                                    onTap: (){
                                                      navigateToPrivateContest(context, widget.model, onTeamCreated);


                                                    },
                                                    child: Container(
                                                      decoration:BoxDecoration(
                                                          color:Color(0xFFffffff),
                                                          border: Border.all(color:Title_Color_2 ),

                                                          // border: Border.all(color:Color(0xFFe1e1e1) ),
                                                          borderRadius: BorderRadius.circular(50)
                                                      ),
                                                      // width: MediaQuery.of(context).size.width*0.4,
                                                      child: Padding(
                                                        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 15),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: [
                                                            Text('Create a Contest',style: TextStyle(
                                                                color: Title_Color_2,fontSize: 12
                                                            ),),
                                                            // Icon(Icons.add,size: 15,color: Color(0xFF8e9193),)
                                                            // Image.asset(AppImages.addRound,scale: 4,color: whiteColor,)
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap:(){
                                                      widget.model.onJoinContestResult=onJoinContestResult;
                                                      navigateToInviteContestCode(context, widget.model);
                                                    },
                                                    child: Container(
                                                      decoration:BoxDecoration(
                                                          color:Title_Color_2,
                                                          borderRadius: BorderRadius.circular(40)
                                                      ),
                                                      // width: MediaQuery.of(context).size.width*0.4,
                                                      child: Padding(
                                                        padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 15),
                                                        child: Row(
                                                          mainAxisAlignment: MainAxisAlignment.center,
                                                          children: [
                                                            Text('Enter Contest Code',style: TextStyle(
                                                                color: whiteColor,fontSize: 12
                                                            ),),
                                                            // Image.asset(AppImages.TicketUrl,scale: 4,color: Title_Color_2,)
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ),

                                                ],
                                              ),
                                              SizedBox(height: 20,),
                                              new Row(
                                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                children: [
                                                  // new Row(
                                                  //   children: [
                                                  //     Text(
                                                  //       'Sort by',
                                                  //       style: TextStyle(fontSize: 12,fontWeight: FontWeight.w400,color: Colors.grey),
                                                  //     ),
                                                  //     new GestureDetector(
                                                  //       behavior: HitTestBehavior.translucent,
                                                  //       child: new Container(
                                                  //         margin: EdgeInsets.only(left: 5),
                                                  //         alignment: Alignment.center,
                                                  //         child: new Card(
                                                  //           child: new Container(
                                                  //             height: 30,
                                                  //             width: 65,
                                                  //             alignment: Alignment.center,
                                                  //             child: new Text(
                                                  //               'Entry',
                                                  //               style: TextStyle(fontSize: 12,fontWeight: FontWeight.w400,color: Colors.grey,),
                                                  //               textAlign: TextAlign.center,
                                                  //             ),
                                                  //           ),
                                                  //         ),
                                                  //       ),
                                                  //       onTap: (){
                                                  //         openAllContests(111, 'entry_fee');
                                                  //       },
                                                  //     ),
                                                  //     new GestureDetector(
                                                  //       behavior: HitTestBehavior.translucent,
                                                  //       child: new Container(
                                                  //         alignment: Alignment.center,
                                                  //         child: new Card(
                                                  //           child: new Container(
                                                  //             height: 30,
                                                  //             width: 90,
                                                  //             alignment: Alignment.center,
                                                  //             child: new Text(
                                                  //               'Contest Size',
                                                  //               style: TextStyle(fontSize: 12,fontWeight: FontWeight.w400,color: Colors.grey,),
                                                  //               textAlign: TextAlign.center,
                                                  //             ),
                                                  //           ),
                                                  //         ),
                                                  //       ),
                                                  //       onTap: (){
                                                  //         openAllContests(111, 'contest_size');
                                                  //       },
                                                  //     ),
                                                  //   ],
                                                  // ),
                                                  GestureDetector(
                                                    onTap:(){
                                                      openAllContests(111,'');
                                                    },
                                                    child: Row(
                                                      children: [
                                                        Image(image: AssetImage(AppImages.all_contests,),color: yellowdark,width: 25,height: 20,),
                                                        SizedBox(width: 10,),
                                                        Text('View All Contests ',style: TextStyle(
                                                          fontWeight: FontWeight.bold,fontSize: 15,
                                                          color: TextFieldTextColor,
                                                        ),),
                                                        Text('(${totalContest.toString()})',style: TextStyle(
                                                          fontWeight: FontWeight.bold,fontSize: 15,
                                                          color: Title_Color_2,
                                                        ),),
                                                      ],
                                                    ),
                                                  ),
                                                  new GestureDetector(
                                                    behavior: HitTestBehavior.translucent,
                                                    child: new Container(
                                                      child: new Row(
                                                        children: [
                                                          new Container(
                                                            margin: EdgeInsets.only(left: 5),
                                                            height: 13,
                                                            width: 13,
                                                            child: Image(
                                                              image: AssetImage(
                                                                AppImages.filterIcon,),color: Colors.grey,),
                                                          ),
                                                          new Text('All Filters',
                                                              style: TextStyle(
                                                                  fontFamily: AppConstants.textBold,
                                                                  color: Colors.grey,
                                                                  fontWeight: FontWeight.normal,
                                                                  fontSize: 12)),

                                                        ],
                                                      ),
                                                    ),
                                                    onTap: (){
                                                      AppConstants.isFilter=true;
                                                      openAllContests(111, 'all_filters');
                                                    },
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),

                                        new Divider(height: 2,thickness: 1,),

                                        !contestLoading?new Container(
                                          color: whiteColor,
                                          margin: EdgeInsets.only(top: 10, bottom: 10),
                                          child: new Column(
                                            children: List.generate(
                                              categoriesList.length,
                                                  (index) => new Column(
                                                children: [
                                                  new Container(
                                                    margin: EdgeInsets.only(bottom:10),
                                                    color: Color(0xffF1F4FC),
                                                    padding: EdgeInsets.only(left: 5,right: 10,top: 10,bottom: 10),
                                                    child: new Row(
                                                      children: [
                                                        new Container(
                                                          margin: EdgeInsets.only(right: 10),
                                                          height: 30,
                                                          width: 30,
                                                          child:  CachedNetworkImage(
                                                            imageUrl: categoriesList[index].contest_image_url!,
                                                            placeholder: (context,url)=> new Image.asset(AppImages.contestIcon_placeholder),
                                                            errorWidget: (context, url, error) => new Image.asset(AppImages.contestIcon_placeholder),
                                                            fit: BoxFit.fill,
                                                          ),
                                                        ),

                                                        new Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: [
                                                            new Text(
                                                              categoriesList[index].name!,
                                                              style: TextStyle(
                                                                  color: Title_Color_2,
                                                                  fontSize: 14,
                                                                  fontWeight: FontWeight.bold),
                                                            ),
                                                            new Container(
                                                              margin: EdgeInsets.only(top: 5),
                                                              child: new Text(
                                                                categoriesList[index].contest_sub_text!,
                                                                style: TextStyle(
                                                                    color: Color(0xFF8e9193),
                                                                    fontSize: 12,
                                                                    fontWeight: FontWeight.w400),
                                                              ),
                                                            ),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  ),


                                                  new Container(
                                                    padding: EdgeInsets.only(right: 10, left: 10),
                                                    child: new ListView.builder(
                                                        physics: NeverScrollableScrollPhysics(),
                                                        shrinkWrap: true,
                                                        scrollDirection: Axis.vertical,
                                                        itemCount: categoriesList[index].leagues!.length>3?3:categoriesList[index].leagues!.length,
                                                        itemBuilder: (BuildContext context, int position) {
                                                          return new ContestItemAdapter(widget.model,categoriesList[index].leagues![position],onJoinContestResult,userId,getWinnerPriceCard);
                                                        }
                                                    ),
                                                  ),

                                                  categoriesList[index].is_view_more==1?new GestureDetector(
                                                    behavior: HitTestBehavior.translucent,
                                                    child: new Container(
                                                      height: 35,
                                                      child: new Row(
                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                        children: [
                                                          new Container(
                                                            padding:EdgeInsets.all(5),
                                                            child: new Row(
                                                              children: [
                                                                new Text('View More',style: TextStyle(color: textGrey,fontSize: 14,fontWeight: FontWeight.w400),),
                                                              ],
                                                            ),
                                                          ),
                                                          new Container(
                                                            margin: EdgeInsets.only(right: 15),
                                                            height: 10,
                                                            width: 10,
                                                            child: Image.asset(AppImages.moreForwardIcon),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    onTap: ()=>{
                                                      openAllContests(categoriesList[index].id!, '')
                                                    },
                                                  ):new Container(),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ): new Container(
                                          margin: EdgeInsets.all(8),
                                          child: new Column(
                                            children: List.generate(
                                              7,
                                                  (index) => new Column(
                                                children: [
                                                  new Container(
                                                    padding: EdgeInsets.only(left: 5,right: 10,top: 10,bottom: 10),
                                                    child: new Row(
                                                      children: [
                                                        new Container(
                                                          margin: EdgeInsets.only(right: 10),
                                                          height: 30,
                                                          width: 30,
                                                          child:  CustomWidget.circular(height: 30,width: 30,),
                                                        ),
                                                        new Column(
                                                          crossAxisAlignment: CrossAxisAlignment.start,
                                                          children: [
                                                            CustomWidget.rectangular(height: 15,width: 50,),
                                                            CustomWidget.rectangular(height: 15,width: 100,),
                                                          ],
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  new Container(
                                                    child: new ListView.builder(
                                                        physics: NeverScrollableScrollPhysics(),
                                                        shrinkWrap: true,
                                                        scrollDirection: Axis.vertical,
                                                        itemCount: 2,
                                                        itemBuilder: (BuildContext context, int index) {
                                                          return new ShimmerContestItemAdapter();
                                                        }
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),


                                        new GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          child: new Container(
                                            margin: EdgeInsets.only(left: 5,bottom: 50),
                                            alignment: Alignment.center,
                                            child: new Card(
                                              elevation: .5,
                                              child: new Container(
                                                height: 30,
                                                width: 150,
                                                alignment: Alignment.center,
                                                child: new Text(
                                                  'View All '+totalContest.toString()+' Contests',
                                                  style: TextStyle(fontSize: 12,fontWeight: FontWeight.w500,color: Colors.black,),
                                                  textAlign: TextAlign.center,
                                                ),
                                              ),
                                            ),
                                          ),
                                          onTap: ()=>{
                                            openAllContests(111,'')
                                          },
                                        ),
                                        SizedBox(height: 20,),
                                      ],
                                    ),), onRefresh: _pullRefresh):_currentIndex==1?new RefreshIndicator(child: joinedContestList.length>0||contestLoading?new MyJoinedContests(widget.model,joinedContestList,contestLoading,onJoinContestResult,userId,getWinnerPriceCard):new Container(
                                      child: new Column(
                                        children: [
                                          NoMatchesListFound(sportKey: widget.model.sportKey!,),
                                          new Container(
                                              width: 150,
                                              height: 40,
                                              margin: EdgeInsets.only(top: 30),
                                              child: ElevatedButton(
                                                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),elevation: MaterialStateProperty.all(.5) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)))) ,
                                                child: Text(
                                                  'Join A Contest',
                                                  style: TextStyle(
                                                      fontSize: 14, color: Colors.white),
                                                ),
                                                onPressed: () {
                                                  onTabTapped(0);
                                                },
                                              )),
                                        ],
                                      ),
                                    ), onRefresh: _pullRefresh2): new RefreshIndicator(child: teamsList.length>0?new MyTeams(widget.model,teamsList):new Container(
                                      child: new Column(
                                        children: [
                                          NoTeamsFound(widget.model.sportKey!),
                                          new Container(
                                              width: 150,
                                              height: 40,
                                              margin: EdgeInsets.only(top: 30,left: 10),
                                              child: ElevatedButton(
                                                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),elevation: MaterialStateProperty.all(.5) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)))),
                                                child: Text(
                                                  'Create A Team',
                                                  style: TextStyle(
                                                      fontSize: 14, color: Colors.white),
                                                ),
                                                onPressed: () {
                                                  widget.model.onTeamCreated=onTeamCreated;
                                                  widget.model.teamId=teamId=0;
                                                  navigateToCreateTeam(context,widget.model);
                                                },
                                              )),
                                        ],
                                      ),
                                    ), onRefresh: _pullRefresh3)
                                ),
                              ],
                            ),
                          ),
                        ),

                        back_dialog ? AlertDialog(
                          title: Text("Exit"),
                          content: Text("Do you want to Exit?"),
                          actions: [
                            cancelButton(context),
                            continueButton(context),
                          ],
                        ) : new Container(),
                        _currentIndex==0&&teamCount<20?new Container(
                            alignment: Alignment.bottomCenter,
                            padding: EdgeInsets.only(bottom: 10,right: 10),
                            child: GestureDetector(
                              onTap: () {
                                widget.model.onTeamCreated=onTeamCreated;
                                widget.model.teamId=teamId=0;
                                navigateToCreateTeam(context,widget.model);
                              },
                              child: new Container(
                                height: 50,
                                width: 180,
                                 margin: EdgeInsets.only(left: 10),
                                 decoration: BoxDecoration(
                                   color: Themecolor,
                                   borderRadius: BorderRadius.circular(40)
                                 ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Center(
                                    child: Text('Create Team',style: TextStyle(
                                      color: Colors.white,fontWeight: FontWeight.bold
                                    ),),
                                  ),
                                ),
                              ),
                            )
                        ):new Container(),
                      ],
                    ),
                 /*   bottomNavigationBar: BottomNavigationBar(
                      landscapeLayout: BottomNavigationBarLandscapeLayout.centered,
                      backgroundColor: Colors.white,
                      onTap: onTabTapped,
                      // new
                      currentIndex: _currentIndex,
                      //
                      selectedItemColor: primaryColor,
                      unselectedItemColor: Colors.grey,
                      selectedFontSize: 12,
                      unselectedFontSize: 12,

                      // new
                      type: BottomNavigationBarType.fixed,
                      items: [
                        new BottomNavigationBarItem(
                          icon: new Container(
                            height: 25,
                            width: 25,
                            margin: EdgeInsets.only(bottom: 4),
                            child: Image(
                              image: AssetImage(AppImages.contestIcon),
                              color: _currentIndex==0?primaryColor:Colors.grey,
                            ),
                          ),
                          label: 'Contests',
                        ),
                        new BottomNavigationBarItem(
                          icon: new Container(
                            height: 25,
                            width: 25,
                            margin: EdgeInsets.only(bottom: 4),
                            child: Image(
                              image: AssetImage(AppImages.myContestIcon),
                              color: _currentIndex==1?primaryColor:Colors.grey,
                            ),
                          ),
                          label:joinedContestCount==0?"My Contests": 'My Contests('+joinedContestCount.toString()+')',
                        ),
                        new BottomNavigationBarItem(
                          icon: new Container(
                            height: 25,
                            width: 25,
                            child: Image(
                              image: AssetImage(AppImages.createTeamIcon),
                              color: _currentIndex==2?primaryColor:Colors.grey,
                            ),
                          ),
                          label:teamCount==0?"My Teams": 'My Teams('+teamCount.toString()+')',
                        ),
                      ],
                    ),*/
                  ),
                ],
              ),
              onWillPop: _onWillPop)),
    );
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
      if(_currentIndex==0){
        getContestByCategory();
      }else if(_currentIndex==1){
        getJoinedContests();
      }else{
        getMyTeams();
      }
    });
  }

  Widget cancelButton(context) {
    return TextButton(
      child: Text("NO"),
      onPressed: () {
        setState(() {
          back_dialog = false;
        });
      },
    );
  }

  Widget continueButton(context) {
    return TextButton(
      child: Text("YES"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
  }

  Future<bool> _onWillPop() async {
    setState(() {
      Navigator.pop(context);
    });
    return Future.value(false);
  }
  List<Widget> getSlotTabs(){
    inningTabs.clear();
    for(int i=0; i<widget.model.fantasySlots!.length; i++){
      inningTabs.add(getInningsTab(widget.model.fantasySlots![i]));
    }
    return inningTabs;
  }
  List<Widget> getTabs(){
    tabs.clear();
    fantasyTypes.clear();
    tabs.add(getFullMatchTab());
    fantasyTypes.add(0);
    if(widget.model.battingFantasy==1){
      tabs.add(getBattingTab());
      fantasyTypes.add(2);
    }
    if(widget.model.bowlingFantasy==1){
      tabs.add(getBowlingTab());
      fantasyTypes.add(3);
    }
    if(widget.model.reverseFantasy==1){
      tabs.add(getReverseTab());
      fantasyTypes.add(5);
    }
    return tabs;
  }
  int getFantasyTabCount(){
    int count=1;
    if(widget.model.battingFantasy==1){
      count+=1;
    }
    if(widget.model.bowlingFantasy==1){
      count+=1;
    }
    if(widget.model.reverseFantasy==1){
      count+=1;
    }
    return count;
  }
  Widget getFullMatchTab(){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Text(
          'Full Match',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 0
                  ? primaryColor
                  : Colors.grey,
              fontWeight: FontWeight.w400,
              fontSize: 14),
        ),
      ),
    );
  }
  Widget getInningsTab(Slots slots){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 2),
              child: Text(
                slots.inning.toString()+'st inning',
                style: TextStyle(
                    fontFamily: AppConstants.textBold,
                    fontWeight: FontWeight.w400,
                    fontSize: 12),
              ),
            ),
            Text(
              slots.min_over.toString()+'-'+slots.max_over.toString()+' Overs',
              style: TextStyle(
                  fontFamily: AppConstants.textBold,
                  fontWeight: FontWeight.w400,
                  fontSize: 12),
            )
          ],
        ),
      ),
    );
  }
  Widget getBattingTab(){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Text(
          'Batting',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 1
                  ? primaryColor
                  : Colors.grey,
              fontWeight: FontWeight.w400,
              fontSize: 14),
        ),
      ),
    );
  }
  Widget getBowlingTab(){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Text(
          'Bowling',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 2
                  ? primaryColor
                  : Colors.grey,
              fontWeight: FontWeight.w400,
              fontSize: 14),
        ),
      ),
    );
  }
  Widget getReverseTab(){
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(
          left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        margin: EdgeInsets.zero,
        decoration: BoxDecoration(
          color:Colors.white,
        ),
        child: Text(
          'Reverse',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 3
                  ? primaryColor
                  : Colors.grey,
              fontWeight: FontWeight.w400,
              fontSize: 14),
        ),
      ),
    );
  }
  void _modalBottomSheetMenu(){
    showModalBottomSheet(
        context: context,
        shape : RoundedRectangleBorder(
            borderRadius : BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10))
        ),
        builder: (builder){
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.add_circle_outline),
                title: Text('Create a Contest'),
                onTap: () {
                    Navigator.pop(context);
                    navigateToPrivateContest(context,widget.model,onTeamCreated);
                },
              ),
              new Divider(height:2,thickness: 1,),
              ListTile(
                leading: Icon(Icons.confirmation_num_outlined),
                title: Text('Enter Contest Code'),
                onTap: () {
                  Navigator.pop(context);
                  widget.model.onJoinContestResult=onJoinContestResult;
                  navigateToInviteContestCode(context,widget.model);
                },
              ),
            ],
          );
        }
    );
  }
  void _showPopImage(String? popup_image){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return new Container(
            alignment: Alignment.center,
            height: 450,
            child: new Stack(
              alignment: Alignment.topRight,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                      margin: EdgeInsets.only(left: 10,right: 10,top: 20),
                      child: new Card(
                        child: CachedNetworkImage(
                          fit: BoxFit.fill,
                          height: 450,
                          width: 300,
                          imageUrl: popup_image!,
                          placeholder: (context, url) => new Image.asset(AppImages.popupPlaceholderIcon,fit: BoxFit.fill,),
                          errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon),
                        ),
                      )
                  ),
                ),
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  child: new Container(
                    height: 40,
                    width: 40,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white,width: 2),
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.black
                    ),
                    child: Icon(
                      Icons.clear,
                      color: Colors.white,
                    ),
                  ),
                  onTap: (){
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          );
        }
    );
  }
  Future<void> _pullRefresh() async {
    getContestByCategory();
  }
  Future<void> _pullRefresh2() async {
    getJoinedContests();
  }
  Future<void> _pullRefresh3() async {
    getMyTeams();
  }
  void getContestByCategory() async {
    setState(() {
      contestLoading=true;
    });
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: selectedFantasyType.toString(),slotes_id: selectedSlotId.toString());
    final client = ApiClient(AppRepository.dio);
    CategoryByContestResponse response = await client.getContestByCategory(contestRequest);
    print("getContestByCategory $response");
    if (response.status == 1) {
        categoriesList=response.result!.categories!;
        totalContest=response.result!.total_contest!;
        widget.model.teamCount=teamCount=response.result!.user_teams!;
        joinedContestCount=response.result!.joined_leagues!;
        widget.model.teamId=teamId=response.result!.team_id!;
    }
    setState(() {
      contestLoading=false;
    });
  }
  void getJoinedContests() async {
    setState(() {
      contestLoading=true;
    });
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: selectedFantasyType.toString(),slotes_id: selectedSlotId.toString());
    final client = ApiClient(AppRepository.dio);
    ContestResponse response = await client.getJoinedContests(contestRequest);
    if (response.status == 1) {
      joinedContestList=response.result!.contest!;
      widget.model.teamCount=teamCount=response.result!.user_teams!;
      joinedContestCount=response.result!.joined_leagues!;
    }
    setState(() {
      contestLoading=false;
    });
  }
  void getMyTeams() async {
    AppLoaderProgress.showLoader(context);
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: selectedFantasyType.toString(),slotes_id: selectedSlotId.toString());
    final client = ApiClient(AppRepository.dio);
    MyTeamResponse response = await client.getMyTeams(contestRequest);
    print("GetMyTeams $response");
    if (response.status == 1) {
      teamsList=response.result!.teams!;
      widget.model.teamCount=teamCount=response.result!.user_teams!;
      joinedContestCount=response.result!.joined_leagues!;
    }
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
  void openAllContests(int catId,String filterType){
    GeneralModel model=widget.model;
    model.teamCount=teamCount;
    model.teamId=teamId;
    model.categoryId=catId;
    model.filterType=filterType;
    model.fantasyType=selectedFantasyType;
    model.slotId=selectedSlotId;
    navigateToAllContests(context,model);
  }
  void onJoinContestResult(int isJoined,String referCode){
    if(_currentIndex==0){
      if(isJoined==1){
        getContestByCategory();
      }
    }else if(_currentIndex==1){
      getJoinedContests();
    }

  }
  void onTeamCreated(){
    if(_currentIndex==0){
      getContestByCategory();
    }else if(_currentIndex==2){
      getMyTeams();
    }
  }
  void _bottomSheet() {
    final Completer<WebViewController> _controller =Completer<WebViewController>();
    _stackToView=1;
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: Colors.transparent,
        builder: (builder) {
          return StatefulBuilder(builder: (context,setState)=>DraggableScrollableSheet(
            initialChildSize: 0.5, // half screen on load
            maxChildSize: 1,       // full screen on scroll
            minChildSize: 0.5,
            builder: (BuildContext context, ScrollController scrollController) {
              return new Column(children: [
                new Container(
                  height: 40,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.only(left: 10, right: 10),
                  margin: EdgeInsets.only(top: 50),
                  decoration: new BoxDecoration(
                    color: primaryColor,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20), topRight: Radius.circular(20)),
                  ),
                  child: new Stack(
                    alignment: Alignment.centerRight,
                    children: [
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          child: new Text('Close',
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16)),
                        ),
                        onTap: (){
                          Navigator.pop(context);
                        },
                      ),
                      new Container(
                        alignment: Alignment.center,
                        width: MediaQuery.of(context).size.width,
                        child: new Text(selectedFantasyType==AppConstants.BATTING_FANTASY_TYPE?'Batting Fantasy':selectedFantasyType==AppConstants.BOWLING_FANTASY_TYPE?'Bowling Fantasy':'Reverse Fantasy',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 16)),
                      )
                    ],
                  ),
                ),
                Expanded(child: SingleChildScrollView(
                  controller: scrollController,
                  child: new Stack(
                    children: [
                      new Container(
                        height: MediaQuery.of(context).size.height,
                        child:  WebView(
                          initialUrl: selectedFantasyType==AppConstants.BATTING_FANTASY_TYPE?'http://EverPlay.in/app/batting-fantasy':selectedFantasyType==AppConstants.BOWLING_FANTASY_TYPE?'http://EverPlay.in/app/bowling-fantasy':'https://EverPlay.in/app/reverse-fantasy',
                          javascriptMode: JavascriptMode.unrestricted,
                          onPageFinished:(url){
                            setState(() {
                              _stackToView = 0;
                            });
                          } ,
                          onWebViewCreated: (WebViewController webViewController) {
                            _controller.complete(webViewController);
                          },
                        ),
                      ),
                      _stackToView==1?new AppLoaderProgress():new Container()
                    ],
                  ),
                ))
              ],);
            },
          ));
        });
  }
  void getWinnerPriceCard(int id,String winAmount) async {
    AppLoaderProgress.showLoader(context);
    ContestRequest request = new ContestRequest(user_id: userId,challenge_id: id.toString(),matchkey: widget.model.matchKey,);
    final client = ApiClient(AppRepository.dio);
    ScoreCardResponse response = await client.getWinnersPriceCard(request);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
    if (response.status == 1) {
      MethodUtils.showWinningPopup(context,response.result!,winAmount);
    }
  }
}
