import 'dart:io';

import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:otp_autofill/otp_autofill.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/dataModels/OtpWidgetModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/data.dart';
import 'package:EverPlay/repository/model/otp_request.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:flutter/foundation.dart';

class OtpVerify extends StatefulWidget {
  @override
  _OtpVerifyState createState() => _OtpVerifyState();
}

class _OtpVerifyState extends State<OtpVerify> {
  List<OtpWidgetModel> mobileOtpFields = <OtpWidgetModel>[];
  List<OtpWidgetModel> emailOtpFields = <OtpWidgetModel>[];
  String otpFrom = 'register';
  late String mobile = '';
  late String email = '';
  late String userId = '0';
  bool isMobileVerified = false;
  bool isEmailVerified = false;
  String mOtp = '';
  CustomTimerController _mController = new CustomTimerController();
  CustomTimerController _eController = new CustomTimerController();
  late OTPInteractor _otpInteractor;

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.FROM).then((value) => {
          setState(() {
            otpFrom = value;
          })
        });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_MOBILE)
        .then((value) => {
              setState(() {
                mobile = value;
              })
            });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_EMAIL)
        .then((value) => {
              setState(() {
                email = value;
              })
            });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) => {
              setState(() {
                userId = value;
              })
            });

    for (int i = 0; i < 6; i++) {
      late TextEditingController otpController = new TextEditingController();
      OtpWidgetModel model = new OtpWidgetModel();
      model.widget = _otpTextField(context, true, otpController);
      model.otpController = otpController;
      mobileOtpFields.add(model);
    }
    for (int i = 0; i < 6; i++) {
      late TextEditingController otpController = new TextEditingController();
      OtpWidgetModel model = new OtpWidgetModel();
      model.widget = _otpTextField(context, false, otpController);
      model.otpController = otpController;
      emailOtpFields.add(model);
    }
    otpListener();
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
    //     statusBarColor: primaryColor,
    //     /* set Status bar color in Android devices. */
    //
    //     statusBarIconBrightness: Brightness.light,
    //     /* set Status bar icons color in Android devices.*/
    //
    //     statusBarBrightness:
    //     Brightness.dark) /* set Status bar icon color in iOS. */
    // );
    return SafeArea(
      child: Scaffold(
        backgroundColor: whiteColor,
        // appBar: PreferredSize(
        //   preferredSize: Size.fromHeight(60),
        //   child: Column(
        //     children: [
        //       Container(
        //         padding: EdgeInsets.only(top: 30),
        //         color:whiteColor,
        //         child: Row(children: [
        //           GestureDetector(
        //             child: Padding(
        //               padding: const EdgeInsets.only(
        //                   top: 16.0, bottom: 16.0, left: 16.0),
        //               child: new Image(
        //                 image: AssetImage(AppImages.backImageURL),
        //                 height: 16,
        //                 width: 16,
        //               ),
        //             ),
        //             onTap: () {
        //               Navigator.pop(context);
        //             },
        //           ),
        //           Padding(
        //             padding: const EdgeInsets.all(16.0),
        //             child: Text(
        //               "OTP Verification",
        //               style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black, fontSize: 18),
        //             ),
        //           )
        //         ]),
        //       ),
        //     ],
        //   ),
        // ),
        body: SafeArea(
          child: new SingleChildScrollView(
            child: new Container(
              width: MediaQuery.of(context).size.width*1,
              child: Column(
                children: [
                  Container(
                    //height: 500,
                    color: Colors.transparent,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () => {Navigator.pop(context)},
                          child: new Container(
                            padding: EdgeInsets.all(15),
                            alignment: Alignment.centerLeft,
                            child: new Container(
                              width: 16,
                              height: 16,
                              child: Image(
                                image: AssetImage(AppImages.backImageURL),
                                fit: BoxFit.fill,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                        new Container(
                          child: new Text('OTP Verify',
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18)),
                        ),

                      ],
                    ),
                  ),
                  Image.asset(AppImages.otpVerify,scale: 2,height: 200),
                  Text('Verify Mobile Number', style: TextStyle(
                    fontSize: 24,
                    color: TextFieldTextColor,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.bold,
                  ),),
                  new Container(
                    margin: EdgeInsets.only(
                        left: 10, right: 10, bottom: 10, top: 10),
                    child: new Container(
                      padding: EdgeInsets.all(5),
                      child: new Column(
                        children: [
                          !isMobileVerified
                              ? new Column(
                            children: [
                              new Container(
                                padding: EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new GestureDetector(
                                      behavior:
                                      HitTestBehavior.translucent,
                                      child: new Container(
                                        child: new Column(
                                          children: [
                                            Center(
                                              child: new Container(
                                                margin: EdgeInsets.only(
                                                    top: 5),
                                                child: new Text(
                                                  otpFrom != 'register'
                                                      ? "We have sent an OTP on your number"
                                                      : "We have sent an OTP on your number and email",
                                                  textAlign:
                                                  TextAlign.center,
                                                  style: TextStyle(
                                                    fontSize: 14,
                                                    color: Colors.grey,
                                                    fontStyle:
                                                    FontStyle.normal,
                                                    fontWeight:
                                                    FontWeight.normal,
                                                  ),
                                                ),
                                              ),
                                            ),
                                            new Container(
                                              margin: EdgeInsets.only(
                                                  top: 15),
                                              child: new Text(
                                                "+91" + mobile,
                                                textAlign: TextAlign.end,
                                                style: TextStyle(
                                                  fontSize: 14,
                                                  color: Themecolor,
                                                  fontStyle:
                                                  FontStyle.normal,
                                                  fontWeight:
                                                  FontWeight.bold,
                                                  decoration:
                                                  TextDecoration
                                                      .underline,
                                                ),
                                              ),
                                            ),
                                          ],
                                          crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                        ),
                                      ),
                                      onTap: () => {},
                                    ),
                                  ],
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(top: 20),
                                child: new Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin:
                                      EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      padding: EdgeInsets.all(5),
                                      child: Wrap(
                                          alignment: WrapAlignment.start,
                                          spacing: 4,
                                          direction: Axis.horizontal,
                                          runSpacing: 10,
                                          children: List.generate(
                                            mobileOtpFields.length,
                                                (index) =>
                                            mobileOtpFields[index]
                                                .widget,
                                          )),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.fromLTRB(
                                          10, 15, 10, 10),
                                      width: MediaQuery.of(context)
                                          .size
                                          .width,
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          new Container(
                                            margin: EdgeInsets.only(
                                                right: 20),
                                            child: Text(
                                                "Didn't received the OTP?",
                                                style: TextStyle(
                                                    fontFamily:
                                                    AppConstants
                                                        .textBold,
                                                    color: LinkTextColor,
                                                    fontWeight:
                                                    FontWeight.normal,
                                                    fontSize: 14)),
                                          ),
                                          new Container(
                                            child: CustomTimer(
                                              from: Duration(seconds: 60),
                                              to: Duration(seconds: 0),
                                              controller: _mController,
                                              onBuildAction:
                                              CustomTimerAction
                                                  .auto_start,
                                              builder:
                                                  (CustomTimerRemainingTime
                                              remaining) {
                                                return new GestureDetector(
                                                  behavior:
                                                  HitTestBehavior
                                                      .translucent,
                                                  child: Text(
                                                    remaining.seconds ==
                                                        '00'
                                                        ? "Resend OTP ?"
                                                        : "${remaining.minutes}:${remaining.seconds}",
                                                    style: TextStyle(
                                                        fontFamily:
                                                        AppConstants
                                                            .textBold,
                                                        color:
                                                        Themecolor,
                                                        decoration: remaining
                                                            .seconds ==
                                                            '00'
                                                            ? TextDecoration
                                                            .underline
                                                            : TextDecoration
                                                            .none,
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        fontSize: 14),
                                                  ),
                                                  onTap: () {
                                                    if (remaining
                                                        .seconds ==
                                                        '00') {
                                                      if (otpFrom ==
                                                          'register') {
                                                        resendOTP(
                                                            'mobile');
                                                      } else {
                                                        resendForgotOTP();
                                                      }
                                                    }
                                                  },
                                                );
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    new Container(
                                        width: MediaQuery.of(context)
                                            .size
                                            .width,
                                        height: 50,
                                        margin: EdgeInsets.fromLTRB(
                                            10, 30, 10, 10),
                                        child: ElevatedButton(
                                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),elevation: MaterialStateProperty.all(.5) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))),


                                          child: Text(
                                            'CONTINUE',
                                            style:
                                            TextStyle(fontSize: 15, color: Colors.white),
                                          ),
                                          onPressed: () {
                                            verifyMobileOtp();
                                          },
                                        ))
                                  ],
                                ),
                              ),
                            ],
                          )
                              : new Container(
                            margin: EdgeInsets.only(top: 20, bottom: 20),
                            child: new Row(
                              children: [
                                new Container(
                                  padding: EdgeInsets.all(15),
                                  alignment: Alignment.centerLeft,
                                  child: new Container(
                                    width: 50,
                                    height: 50,
                                    child: Image(
                                        image: AssetImage(
                                            AppImages.verifiedIcon),
                                        fit: BoxFit.fill),
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: new Text(
                                    "Verified mobile number",
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Themecolor,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          otpFrom != 'register'
                              ? !isEmailVerified
                              ? new Column(
                            crossAxisAlignment:
                            CrossAxisAlignment.center,
                            children: [
                              new Container(
                                margin: EdgeInsets.only(
                                    top: 30, left: 10),
                                child: new Text(
                                  "Enter email OTP here",
                                  textAlign: TextAlign.end,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(top: 10),
                                child: new Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      margin: EdgeInsets.fromLTRB(
                                          0, 0, 0, 0),
                                      padding: EdgeInsets.all(10),
                                      child: Wrap(
                                          alignment:
                                          WrapAlignment.start,
                                          spacing: 4,
                                          direction: Axis.horizontal,
                                          runSpacing: 10,
                                          children: List.generate(
                                            emailOtpFields.length,
                                                (index) =>
                                            emailOtpFields[index]
                                                .widget,
                                          )),
                                    ),
                                    new Container(
                                        width: MediaQuery.of(context)
                                            .size
                                            .width,
                                        height: 50,
                                        margin: EdgeInsets.fromLTRB(
                                            10, 30, 10, 10),
                                        child: ElevatedButton(
                                          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))),
                                          child: Text(
                                            'CONTINUE',
                                            style: TextStyle(color: Colors.white,
                                                fontSize: 15),
                                          ),
                                          onPressed: () {
                                            mOtp = '';
                                            for (int i = 0;
                                            i < 6;
                                            i++) {
                                              if (emailOtpFields[i]
                                                  .otpController
                                                  .text
                                                  .isEmpty) {
                                                Fluttertoast.showToast(
                                                    msg:
                                                    'Please enter your six digit otp',
                                                    toastLength: Toast
                                                        .LENGTH_SHORT,
                                                    timeInSecForIosWeb:
                                                    1);
                                                return;
                                              } else {
                                                mOtp = mOtp +
                                                    emailOtpFields[i]
                                                        .otpController
                                                        .text
                                                        .toString();
                                              }
                                            }
                                            otpVerify('email');
                                          },
                                        )),
                                    new Container(
                                      margin: EdgeInsets.fromLTRB(
                                          10, 0, 10, 10),
                                      width: MediaQuery.of(context)
                                          .size
                                          .width,
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          new Container(
                                            margin: EdgeInsets.only(
                                                bottom: 10, top: 10),
                                            child: Text(
                                                "Didn't received the OTP?",
                                                style: TextStyle(
                                                    fontFamily:
                                                    AppConstants
                                                        .textBold,
                                                    color:
                                                    LinkTextColor,
                                                    fontWeight:
                                                    FontWeight
                                                        .normal,
                                                    fontSize: 14)),
                                          ),
                                          SizedBox(width: 10),
                                          new Container(
                                            child: CustomTimer(
                                              controller:
                                              _eController,
                                              from: Duration(
                                                  seconds: 60),
                                              to: Duration(
                                                  seconds: 0),
                                              onBuildAction:
                                              CustomTimerAction
                                                  .auto_start,
                                              builder:
                                                  (CustomTimerRemainingTime
                                              remaining) {
                                                return new GestureDetector(
                                                  behavior:
                                                  HitTestBehavior
                                                      .translucent,
                                                  child: Text(
                                                    remaining.seconds ==
                                                        '00'
                                                        ? "Resend OTP ?"
                                                        : "${remaining.minutes}:${remaining.seconds}",
                                                    style: TextStyle(
                                                        fontFamily:
                                                        AppConstants
                                                            .textBold,
                                                        color:
                                                        Themecolor,
                                                        decoration: remaining.seconds ==
                                                            '00'
                                                            ? TextDecoration
                                                            .underline
                                                            : TextDecoration
                                                            .none,
                                                        fontWeight:
                                                        FontWeight
                                                            .bold,
                                                        fontSize: 14),
                                                  ),
                                                  onTap: () {
                                                    if (remaining
                                                        .seconds ==
                                                        '00') {
                                                      resendOTP(
                                                          'email');
                                                    }
                                                  },
                                                );
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          )
                              : new Container(
                            margin:
                            EdgeInsets.only(top: 20, bottom: 20),
                            child: new Row(
                              children: [
                                new Container(
                                  padding: EdgeInsets.all(15),
                                  alignment: Alignment.centerLeft,
                                  child: new Container(
                                    width: 50,
                                    height: 50,
                                    child: Image(
                                        image: AssetImage(
                                            AppImages.verifiedIcon),
                                        fit: BoxFit.fill),
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(left: 10),
                                  child: new Text(
                                    "Verified email",
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Themecolor,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )
                              : new Container()
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _otpTextField(BuildContext context, bool autoFocus,
      TextEditingController otpController) {
    return Container(
      padding: EdgeInsets.all(0),
      height: 40,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        shape: BoxShape.rectangle,
      ),
      child: AspectRatio(
        aspectRatio: 1,
        child: TextField(
          controller: otpController,
          autofocus: autoFocus,
          decoration: InputDecoration(
            border: InputBorder.none,
            contentPadding: EdgeInsets.only(bottom: 10.0),
            counterText: "",
          ),
          textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
          style: TextStyle(),
          maxLines: 1,
          maxLength: 6,
          onChanged: (value) {
            if (value.length == 6) {
              for (int i = 0; i < 6; i++) {
                mobileOtpFields[i].otpController.text =
                    value.substring(i, i + 1);
                FocusScope.of(context).unfocus();
              }
            } else {
              if (value.length == 1) {
                FocusScope.of(context).nextFocus();
              } else {
                FocusScope.of(context).previousFocus();
              }
            }
          },
        ),
      ),
    );
  }

  void validateMobileOtp() async {
    AppLoaderProgress.showLoader(context);
    OtpRequest loginRequest = new OtpRequest(mOtp, mobile, userId, '', '', '');
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse = await client.validateMobileOtp(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      AppPrefrence.putString(AppConstants.LOGIN_REGISTER_TYPE, "manual_login");
      AppPrefrence.putBoolean(
          AppConstants.SHARED_PREFERENCES_IS_LOGGED_IN, true);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID,
          loginResponse.result!.user_id.toString());
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_NAME,
          loginResponse.result!.username);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE,
          loginResponse.result!.mobile.toString());
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_EMAIL,
          loginResponse.result!.email);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TOKEN,
          loginResponse.result!.custom_user_token);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_REFER_CODE,
          loginResponse.result!.refercode);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME,
          loginResponse.result!.team);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_STATE_NAME,
          loginResponse.result!.state);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_PIC,
          loginResponse.result!.user_profile_image);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS,
          loginResponse.result!.bank_verify);
      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS,
          loginResponse.result!.pan_verify);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS,
          loginResponse.result!.mobile_verify);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS,
          loginResponse.result!.email_verify);
      AppPrefrence.putString(
          AppConstants.AUTHTOKEN, loginResponse.result!.custom_user_token);
      navigateToHomePage(context);
    }
    else {
      Fluttertoast.showToast(
          msg: loginResponse.message!,
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
    }
  }

  void otpVerify(type) async {
    AppLoaderProgress.showLoader(context);
    OtpRequest loginRequest =
        new OtpRequest(mOtp, mobile, userId, type, email, '');
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse =
        await client.otpRegisterVerifyNew(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      setState(() {
        if (loginResponse.result!.mobile_verify == 1) {
          isMobileVerified = true;
        }
        if (loginResponse.result!.email_verify == 1) {
          isEmailVerified = true;
        }
      });

      AppPrefrence.putString(AppConstants.LOGIN_REGISTER_TYPE, "manual_login");
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID,
          loginResponse.result!.user_id.toString());
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_NAME,
          loginResponse.result!.username);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE,
          loginResponse.result!.mobile.toString());
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_EMAIL,
          loginResponse.result!.email);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TOKEN,
          loginResponse.result!.custom_user_token);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_REFER_CODE,
          loginResponse.result!.refercode);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME,
          loginResponse.result!.team);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_STATE_NAME,
          loginResponse.result!.state);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_PIC,
          loginResponse.result!.user_profile_image);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS,
          loginResponse.result!.bank_verify);
      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS,
          loginResponse.result!.pan_verify);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS,
          loginResponse.result!.mobile_verify);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS,
          loginResponse.result!.email_verify);
      AppPrefrence.putString(
          AppConstants.AUTHTOKEN, loginResponse.result!.custom_user_token);


      if (isMobileVerified) {
        AppPrefrence.putBoolean(
            AppConstants.SHARED_PREFERENCES_IS_LOGGED_IN, true);
        navigateToHomePage(context);
      }

    /*  if (isMobileVerified && isEmailVerified) {
        AppPrefrence.putBoolean(
            AppConstants.SHARED_PREFERENCES_IS_LOGGED_IN, true);
        navigateToHomePage(context);
      }*/
    } else {
      Fluttertoast.showToast(
          msg: loginResponse.message!,
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
    }
  }

  void verifyForgotMobileOtp() async {
    AppLoaderProgress.showLoader(context);
    OtpRequest loginRequest =
        new OtpRequest(mOtp, mobile, userId, '', email, '');
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse =
        await client.verifyForgotMobileOtp(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      AppPrefrence.putString(AppConstants.FROM, "otp");
      navigateToChangePassword(context);
      Navigator.pop(context);
    }
    Fluttertoast.showToast(
        msg: loginResponse.message!,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1);
  }

  void resendOTP(type) async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(
        mobile: mobile,
        email: email,
        user_id: userId,
        type: '1',
        verification_type: type,
        fcmToken: '');
    final client = ApiClient(AppRepository.dio);
    GeneralResponse loginResponse = await client.sendOTPNew(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      otpListener();
      if (type == 'mobile') {
        _mController.reset();
        _mController.start();
      } else {
        _eController.reset();
        _eController.start();
      }
    }
    Fluttertoast.showToast(
        msg: loginResponse.message!,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1);
  }

  void resendForgotOTP() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest =
        new GeneralRequest(user_id: userId, fcmToken: '');
    final client = ApiClient(AppRepository.dio);
    GeneralResponse loginResponse = await client.sendForgotOTP(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      otpListener();
      _mController.reset();
      _mController.start();
    }
    Fluttertoast.showToast(
        msg: loginResponse.message!,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1);
  }

  void otpListener() {
    _otpInteractor = OTPInteractor();
    if (Platform.isAndroid) {
      _otpInteractor
          .getAppSignature()
          //ignore: avoid_print
          .then((value) => print('signature - $value'));
    }
    OTPTextEditController(
      codeLength: 6,
      //ignore: avoid_print
      onCodeReceive: (code) => {
        for (int i = 0; i < 6; i++)
          {
            mobileOtpFields[i].otpController.text = code.substring(i, i + 1),
            FocusScope.of(context).unfocus()
          },
        verifyMobileOtp()
      },
      otpInteractor: _otpInteractor,
    )..startListenUserConsent(
        (code) {
          final exp = RegExp(r'(\d{6})');
          return exp.stringMatch(code ?? '') ?? '';
        },
      );
  }

  void verifyMobileOtp() {
    mOtp = '';
    for (int i = 0; i < 6; i++) {
      if (mobileOtpFields[i].otpController.text.isEmpty) {
        Fluttertoast.showToast(
            msg: 'Please enter your six digit otp',
            toastLength: Toast.LENGTH_SHORT,
            timeInSecForIosWeb: 1);
        return;
      } else {
        mOtp = mOtp + mobileOtpFields[i].otpController.text.toString();
      }
    }
    if (otpFrom == 'register') {
      otpVerify('mobile');
    } else if (otpFrom == 'login') {
      validateMobileOtp();
    } else {
      verifyForgotMobileOtp();
    }
  }
}
