import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:retrofit/http.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/dataModels/JoinChallengeDataModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/category_contest_response.dart';
import 'package:EverPlay/repository/model/contest_request.dart';
import 'package:EverPlay/repository/model/team_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class MyJoinTeams extends StatefulWidget {
  GeneralModel model;
  Contest contest;
  Function onJoinContestResult;
  MyJoinTeams(this.model,this.contest,this.onJoinContestResult);

  @override
  _MyJoinTeamsState createState() => new _MyJoinTeamsState();
}

class _MyJoinTeamsState extends State<MyJoinTeams>{
  String userId='0';
  bool allSelect=false;
  bool showSelectAll=false;
  List<Team> teamsList=[];
  int joinedCount = 0;
  int teamCount= 0;
  int teamId= 0;
  int selectedTeamCount = 0;
  int joinedContestCount= 0;
  bool isSwitchTeam=false;
  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getMyTeams();
      })
    });
    isSwitchTeam=widget.model.isSwitchTeam??false;
  }


  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: new WillPopScope(child: new Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            // padding: EdgeInsets.only(top: 40),
            color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.white,),
                    ),
                  ),
                ),
                new Container(
                  child:  (widget.contest.multi_entry==1 && !isSwitchTeam) || showSelectAll? Text('Select Your Team',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)):Text('My Teams',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18))
                ),
              ],
            ),
          ),
        ),
        body: new Container(
          height: MediaQuery.of(context).size.height,
          child: new Stack(
            alignment: Alignment.bottomCenter,
            children: [
              new Column(
                children: [
                  (widget.contest.multi_entry==1 && !isSwitchTeam) || showSelectAll?new Container(
                    width: MediaQuery.of(context).size.width,
                    color: Colors.white,
                    child: new Column(
                      children: [
                        SizedBox(height: 10,),
                        new Container(
                          padding: EdgeInsets.only(left: 10),
                          // alignment: Alignment.centerLeft,
                          child: Center(
                            child: new Text("You can enter upto " + widget.contest.max_multi_entry_user.toString() + " teams in this contest",
                                style: TextStyle(
                                    fontFamily: 'robot',
                                    color: Colors.black,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14)),
                          ),
                        ),
                        Divider(
                          thickness: 0,
                          color: Colors.grey,
                        ),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              new Container(
                                child: new Text('Select All ('+getTotalSelectedWithJoined().toString()+')',
                                    style: TextStyle(
                                        fontFamily: 'robot',
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14)),
                              ),
                              new GestureDetector(
                                behavior: HitTestBehavior.translucent,
                                onTap: (){
                                  setState(() {
                                    if(allSelect){
                                      allSelect=false;
                                    }
                                    else{
                                      allSelect=true;
                                    }
                                    selectAll();
                                  });
                                },
                                child: new Container(
                                  padding: EdgeInsets.all(8),
                                  alignment: Alignment.centerLeft,
                                  child: new Container(
                                    child: Image(
                                      width: 22,
                                      height: 22,
                                      image: AssetImage(allSelect?AppImages.checkTeamIcon:AppImages.uncheckTeamIcon),
                                      fit: BoxFit.fill,),
                                  ),
                                ),
                              ),

                            ],
                          ),
                        ), //Row
                      ],
                    ),
                  ):new Container(),
                  new Expanded(child: new Container(
                    height: MediaQuery.of(context).size.height,
                    padding: EdgeInsets.only(bottom: (widget.contest.multi_entry==1 && !isSwitchTeam)?80:65,top: 8,left: 8,right: 8),
                    child: new ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: teamsList.length,
                        itemBuilder: (BuildContext context, int index) {
                          return new TeamItemAdapter(widget.model,teamsList[index],true,index);
                        }
                    ),
                  ))
                ],
              ),
              // widget.contest.multi_entry==1 && !isSwitchTeam?new Container(
              //   height: 80,
              //   padding: EdgeInsets.all(10),
              //   color: Colors.white,
              //   child: new Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       new Container(
              //         margin: EdgeInsets.only(left: 10,top: 5),
              //         child: new Column(
              //           crossAxisAlignment: CrossAxisAlignment.start,
              //           children: [
              //             new Container(
              //               child: new Text('No. of Teams',
              //                   style: TextStyle(
              //                       fontFamily: 'robot',
              //                       color: Colors.grey,
              //                       fontWeight: FontWeight.w400,
              //                       fontSize: 12)),
              //             ),
              //             new Container(
              //               child: new Text(selectedTeamCount.toString(),
              //                   style: TextStyle(
              //                       fontFamily: 'robot',
              //                       color: Colors.black,
              //                       fontWeight: FontWeight.w400,
              //                       fontSize: 18)),
              //             ),
              //           ],
              //         ),
              //       ),
              //       new Container(
              //         child: new Row(
              //           children: [
              //             new Container(
              //                 width:100,
              //                 height: 45,
              //                 margin: EdgeInsets.only(left: 5),
              //                 child: RaisedButton(
              //                   textColor: Colors.white,
              //                   elevation: .5,
              //                   color: primaryColor,
              //                   child: Text(
              //                     'JOIN',
              //                     style: TextStyle(fontSize: 14,color: Colors.white),
              //                   ),
              //                   shape: RoundedRectangleBorder(
              //                       borderRadius:
              //                       BorderRadius.circular(3)),
              //                   onPressed: () {
              //                     joinContest();
              //                   },
              //                 )
              //             ),
              //             new Container(
              //                 margin: EdgeInsets.only(left: 10),
              //                 child: new GestureDetector(
              //                   behavior: HitTestBehavior.translucent,
              //                   child: new Container(
              //                     child: new ClipRRect(
              //                       borderRadius: BorderRadius.all(Radius.circular(28)),
              //                       child: new Container(
              //                         color: primaryColor,
              //                         height: 56,
              //                         width: 56,
              //                         padding: EdgeInsets.all(10),
              //                         child: Image(
              //                           image: AssetImage(AppImages.createTeamIcon),color: Colors.white,),
              //                       ),
              //                     ),
              //                   ),
              //                   onTap: (){
              //                     widget.model.onTeamCreated=onTeamCreated;
              //                     widget.model.teamId=0;
              //                     navigateToCreateTeam(context,widget.model);
              //                   },
              //                 )
              //             )
              //           ],
              //         ),
              //       )
              //     ],
              //   ),
              // ):
              new Container(
                padding: EdgeInsets.all(13),
                child: new Row(
                  children: [
                    new Flexible(child: new Container(
                        width:
                        MediaQuery.of(context).size.width,
                        height: 45,
                        margin: EdgeInsets.only(right: 5),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: Themecolor,
                            elevation: 0.5,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40),
                            ),
                          ),
                          onPressed: () {
                            widget.model.onTeamCreated = onTeamCreated;
                            widget.model.teamId = 0;
                            navigateToCreateTeam(context, widget.model);
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                            child: Text(
                              'Create Team (' + (teamsList.length + 1).toString() + ')',
                              style: TextStyle(fontSize: 12, color: Colors.white),
                            ),
                          ),
                        )
                    ),flex: 1,),
                    new Flexible(child: new Container(
                        width:
                        MediaQuery.of(context).size.width,
                        height: 45,
                        margin: EdgeInsets.only(left: 5),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            primary: yellowdark,
                            elevation: 0.5,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(40),
                            ),
                          ),
                          onPressed: () {
                            joinContest();
                          },
                          child: Padding(
                            padding: EdgeInsets.symmetric(vertical: 8, horizontal: 5),
                            child: Text(
                              'Join Contest',
                              style: TextStyle(fontSize: 12, color: Colors.white),
                            ),
                          ),
                        )
                    ),flex: 1,),
                  ],
                ),
              )
            ],
          ),
        ),
      ), onWillPop: _onWillPop),
    );
  }

  Future<bool> _onWillPop() async {
    setState(() {
      widget.model.isSwitchTeam=false;
      Navigator.pop(context);
    });
    return Future.value(false);
  }
  void getMyTeams() async {
    AppLoaderProgress.showLoader(context);
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.slotId.toString(),challenge_id: widget.contest.id.toString());
    final client = ApiClient(AppRepository.dio);
    MyTeamResponse response = await client.getMyTeams(contestRequest);
    if (response.status == 1) {
      teamsList=response.result!.teams!;
      teamCount=response.result!.user_teams!;
      joinedContestCount=response.result!.joined_leagues!;

      if (widget.contest.multi_entry == 1 && !isSwitchTeam) {
        for (int i = 0; i < teamsList.length; i++) {
          if (teamsList[i].is_joined == 1) {
            joinedCount++;
          }
        }

        int remainingCount = teamCount - joinedCount;
        showSelectAll=true;
        if (remainingCount <= (widget.contest.max_multi_entry_user! - joinedCount)) {
            allSelect = false;
        } else {
            showSelectAll=false;
        }
      }else{
        showSelectAll=false;
      }
    }
    widget.model.teamClickListener=teamClickListener;
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
  void switchTeam() async {
    AppLoaderProgress.showLoader(context);
    ContestRequest contestRequest = new ContestRequest(userid: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,teamid: getMultiTeamId().toString(),joinid: widget.model.joinedSwitchTeamId.toString(),challenge_id: widget.contest.id.toString());
    final client = ApiClient(AppRepository.dio);
    GeneralResponse response = await client.switchTeam(contestRequest);
    Navigator.pop(context);
    AppLoaderProgress.hideLoader(context);
    Fluttertoast.showToast(msg: response.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    if(widget.model.onSwitchTeamResult!=null){
      widget.model.onSwitchTeamResult!();
    }
  }

  void selectAll() {
    selectedTeamCount = 0;
    for (int i = 0; i < teamsList.length; i++) {
      teamsList[i].isSelected=false;
      if (teamsList[i].is_joined != 1) {
        if (allSelect) {
          teamsList[i].isSelected=true;
          selectedTeamCount++;
        }
      }
    }
    setState(() {});
  }
  int getTotalSelectedWithJoined() {
    int selectedTeamCount = 0;
    for (int i = 0; i < teamsList.length; i++) {
      if (teamsList[i].isSelected??false || teamsList[i].is_joined == 1) {
        selectedTeamCount++;
      }
    }
    return selectedTeamCount;
  }
  int getTotalSelected() {
    int selectedTeamCount = 0;
    for (int i = 0; i < teamsList.length; i++) {
      if (teamsList[i].isSelected??false) {
        selectedTeamCount++;
      }
    }
    return selectedTeamCount;
  }
  String getMultiTeamId() {
    StringBuffer stringBuilder = new StringBuffer();
    if (teamsList.length == 1 && teamsList[0].is_joined != 1 && widget.contest.multi_entry == 0 && !isSwitchTeam) {
      stringBuilder.write(teamsList[0].teamid);
    } else if (isSwitchTeam || widget.contest.multi_entry == 0) {
      if (teamId != 0) {
        stringBuilder.write(teamId);
      }
    } else if (widget.contest.multi_entry == 1) {
      for (int i = 0; i < teamsList.length; i++) {
        if (teamsList[i].isSelected??false) {
          stringBuilder.write(teamsList[i].teamid);
          stringBuilder.write(",");
        }
      }
    }

    return stringBuilder.toString().trim();
  }
  void teamClickListener(int position){
    if (teamsList[position].is_joined != 1) {
      if (isSwitchTeam || widget.contest.multi_entry == 0) {

        for (int i = 0; i < teamsList.length; i++) {
          teamsList[i].isSelected=false;
        }
        teamId = teamsList[position].teamid!;
        teamsList[position].isSelected=true;
        selectedTeamCount = 1;
        // ((MyTeamsActivity) context).mBinding.btnJoinContest.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
      } else {

        if (teamsList[position].isSelected??false) {
          teamsList[position].isSelected=false;
          selectedTeamCount--;
          // ((MyTeamsActivity) context).mBinding.btnJoinContest.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        } else if (!(teamsList[position].isSelected??false) && widget.contest.max_multi_entry_user != selectedTeamCount + joinedCount) {
          teamsList[position].isSelected=true;
          selectedTeamCount++;
          // ((MyTeamsActivity) context).mBinding.btnJoinContest.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        } else
          MethodUtils.showError(context, "You can only enter with upto " + widget.contest.max_multi_entry_user.toString() + " teams");
      }
      setState(() {});
    }
  }
  void joinContest(){
    if (getMultiTeamId().length != 0) {
      if (isSwitchTeam) {
          switchTeam();
      }
      else {
        MethodUtils.checkBalance(new JoinChallengeDataModel(context, 0, int.parse(userId), widget.contest.id!, widget.model.fantasyType!, widget.model.slotId!, getTotalSelected(), widget.contest.is_bonus!, widget.contest.win_amount!, widget.contest.maximum_user!, getMultiTeamId(), widget.contest.entryfee.toString(), widget.model.matchKey!, widget.model.sportKey!, widget.model.joinedSwitchTeamId.toString(), isSwitchTeam, 0, 0,onJoinContestResult));
      }
    }
    else {
      MethodUtils.showError(context, "Please select at least one team to join contest");
    }
  }

  void onJoinContestResult(int isJoined,String referCode){
        if(isJoined==1){
          widget.onJoinContestResult(isJoined,referCode);
          Navigator.of(context).pop();
        }

  }
  void onTeamCreated(){
    getMyTeams();
  }
}