import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';

class PromoteApp extends StatefulWidget {
  @override
  _PromoteAppState createState() => new _PromoteAppState();
}

class _PromoteAppState extends State<PromoteApp>{

  TextEditingController codeController = TextEditingController();
  int isVisiblePromoterRequest=0;

  @override
  void initState() {
    super.initState();
    AppPrefrence.getInt(AppConstants.IS_VISIBLE_PROMOTER_REQUEST,0)
        .then((value) =>
    {
      setState(() {
        isVisiblePromoterRequest = value;
        print("my visibility is$isVisiblePromoterRequest");
      })
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.light) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            // padding: EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.black,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Promote App',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Stack(
          children: [
            isVisiblePromoterRequest==0?new Container(
              height: MediaQuery.of(context).size.height,
              child: new SingleChildScrollView(
                child: new Container(
                  child: new Column(
                    children: [
                      new Container(
                        height: 150,
                        child: new Image.asset(AppImages.promoteAppBgIcon,
                            fit: BoxFit.fill),
                      ),
                      new Container(
                        margin: EdgeInsets.only(top: 40),
                        child: new Text('Do you want to collaborate with us?',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 16)),
                      ),
                      new Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(top: 10),
                        child: new Html(data: "Are you a <font color='#c61d24'>Youtuber</font> or Having a <font color='#219ef2'>Telegram</font> channel or want to promote <b>EverPlay?</b>",
                          style: {
                            'html': Style(textAlign: TextAlign.center,color: Colors.black,textDecoration: TextDecoration.none,
                                fontWeight: FontWeight.normal,lineHeight:LineHeight(1.5) )
                          },
                        ),
                      ),
                      new Container(
                          width:
                          MediaQuery.of(context).size.width,
                          height: 45,
                          margin: EdgeInsets.only(left: 40,right: 40,top: 30),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: greenColor,
                              elevation: 0.5,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(3),
                              ),
                            ),
                            onPressed: () {
                              Navigator.pop(context);
                              navigateToPromoteAppDetails(context);
                            },
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                'Connect Now',
                                style: TextStyle(fontSize: 14, color: Colors.white),
                              ),
                            ),
                          )
                      ),
                    ],
                  ),
                ),
              ),
            ):new Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(top: 50),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new Container(
                    height: 100,
                    width: 100,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        border: Border.all(color: greenColor),
                        borderRadius: BorderRadius.circular(50),
                        color: greenColor
                    ),
                    child: Icon(Icons.check,color: Colors.white,size: 50,),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 30),
                    alignment: Alignment.center,
                    child:  Text(
                      'Your request has been submitted \n successfully!',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          height: 1.5,
                          fontFamily: AppConstants.textSemiBold,
                          color: Colors.black,
                          fontWeight: FontWeight.w400,
                          fontSize: 18),
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 20,left: 15,right: 15),
                    alignment: Alignment.center,
                    child:  Text(
                      'Thank you for sharing details. Our team will connect with you in next 24 hours.',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          height: 1.5,
                          fontFamily: AppConstants.textRegular,
                          color: Colors.grey,
                          fontWeight: FontWeight.w400,
                          fontSize: 14),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}