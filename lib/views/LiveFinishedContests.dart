import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/LiveFinishContestAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/contest_request.dart';
import 'package:EverPlay/repository/model/player_points_response.dart';
import 'package:EverPlay/repository/model/refresh_score_response.dart';
import 'package:EverPlay/repository/model/team_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

import 'PlayerPoints.dart';

class LiveFinishContests extends StatefulWidget {
  GeneralModel model;
  LiveFinishContests(this.model);
  @override
  _LiveFinishContestsState createState() => _LiveFinishContestsState();
}

class _LiveFinishContestsState extends State<LiveFinishContests>
    with SingleTickerProviderStateMixin {
  int _currentIndex = 0;
  int _currentMatchIndex = 0;
  List<Widget> tabs = <Widget>[];
  List<Team> teamList = [];
  List<MultiSportsPlayerPointItem> pointsList = [];
  List<LiveFinishedContestData> contestList = [];
  List<int> fantasyTypes=[];
  late TabController _tabController;
  String userId='0';
  String team1Score='0/0 (0)';
  String team2Score='0/0 (0)';
  String tvWinningText='';
  bool loading=false;
  RefreshScoreItem refreshScoreItem=new RefreshScoreItem();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = new TabController(length: 3, vsync: this);
    _tabController.animation!
      ..addListener(() {
        setState(() {
          _currentMatchIndex = (_tabController.animation!.value).round();
        });
      });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getMatchScores();
      })
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: Themecolor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.dark) /* set Status bar icon color in iOS. */
        );
    // TODO: implement build
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset: false,
      body: new Container(
        height: MediaQuery.of(context).size.height*1,
        width: MediaQuery.of(context).size.width*1,
        child: new Column(
          children: [
            new Container(
              height: 225,
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                  colors: [
                     Themecolor,
                    Themecolor,
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(0.0, 1.0),
                ),
              ),
              child: new Container(
                margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
                child: new Column(
                  children: [
                    new Container(
                      color: Colors.black,
                      child: Row(
                        children: [
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () => {Navigator.pop(context)},
                            child: new Container(
                              padding: EdgeInsets.all(15),
                              alignment: Alignment.centerLeft,
                              child: new Container(
                                width: 16,
                                height: 16,
                                child: Image(
                                    image:
                                    AssetImage(AppImages.backImageURL),
                                    fit: BoxFit.fill),
                              ),
                            ),
                          ),
                          new Container(
                            child: new Text(widget.model.teamVs!,
                                style: TextStyle(
                                    fontFamily: AppConstants.textBold,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 18)),
                          ),
                        ],
                      ),
                    ),
                    new Container(
                      padding:
                      EdgeInsets.only(left: 10, right: 10, top: 15),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Flexible(child: new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height:
                                  40,
                                  width:
                                  40,
                                  alignment: Alignment.topRight,
                                  child: CachedNetworkImage(

                                    imageUrl: widget.model.team1Logo!,
                                    placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                    errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                  ),
                                ),
                                SizedBox(height: 5,),
                                new Text(widget.model.team1Name!,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12)),
                                new Container(
                                  margin: EdgeInsets.only(top:5),
                                  child: new Text(team1Score,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16)),
                                ),
                              ],
                            ),
                          ),flex: 1,),
                          new Flexible(child: new Container(
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                new Container(
                                  height: 20,
                                  width: 20,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(image: AssetImage(widget.model.isFromLive!?'assets/images/completeLive.png':'assets/images/liveimage.png')),
                                  ),
                                ),
                                SizedBox(height: 10,),
                                new Container(
                                  alignment: Alignment.center,
                                  child: new Text(widget.model.isFromLive!?'Live':'Completed',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14)),
                                ),
                              ],
                            ),
                          ),flex: 1,),
                          new Flexible(child: new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height:
                                  40,
                                  width:
                                  40,
                                  alignment: Alignment.topRight,
                                  child: CachedNetworkImage(

                                    imageUrl: widget.model.team2Logo!,
                                    placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                    errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                  ),
                                ),
                                SizedBox(height: 5,),
                                new Text(widget.model.team2Name!,
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 1,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontSize: 12)),
                                new Container(
                                  margin: EdgeInsets.only(top:5),
                                  child: new Text(team2Score,
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16)),
                                ),
                              ],
                            ),
                          ),flex: 1,),
                        ],
                      ),
                    ),
                    tvWinningText.isNotEmpty?new Container(
                      alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.only(top: 20, bottom: 0),
                      child: new Text(tvWinningText ,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w400,
                              fontSize: 12)),
                    ):new Container(),
                  ],
                ),
              ),
            ),
            Expanded(
           //   flex: 1,
                child: new Container(
                  color: Colors.white,
                  child: DefaultTabController(
                    length: getFantasyTabCount(),
                    child: new Container(
                      child: new Column(
                        children: [
                          /*new Container(
                            color: Colors.white,
                            child: TabBar(
                              labelPadding: EdgeInsets.all(0),
                              onTap: (int index) {
                                setState(() {
                                  _currentIndex = index;
                                  widget.model.fantasyType=fantasyTypes[_currentIndex];
                                  getMatchScores();
                                });
                              },
                              indicatorWeight: 2,
                              indicatorSize: TabBarIndicatorSize.label,
                              indicatorColor: primaryColor,
                              // indicator: ShapeDecoration(
                              //     shape: RoundedRectangleBorder(borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10))),
                              //     color: Colors.black
                              // ),
                              isScrollable: false,
                              tabs: getTabs(),
                            ),
                          ),*/
                          new Container(
                            // margin: EdgeInsets.fromLTRB(5, 0, , 5),
                            decoration: BoxDecoration(
                              color:Colors.white,
                                // border: Border.all(
                                //     color: bgColorDark, width: 1.5),
                                // borderRadius:
                                // BorderRadius.all(Radius.circular(5))
                        ),

                            child: Padding(
                              padding: const EdgeInsets.only(left: 5,right: 5),
                              child: TabBar(

                                controller: _tabController,
                                labelPadding: EdgeInsets.all(0),
                                onTap: (int index) {
                                  setState(() {
                                    _currentMatchIndex = index;
                                    if(_currentMatchIndex==2){
                                      getPlayerPoints();
                                    }else{
                                      getMatchScores();
                                    }
                                  });
                                },
                                indicatorWeight: 2,
                                indicatorSize: TabBarIndicatorSize.label,
                                indicatorColor: primaryColor,
                                labelColor: primaryColor,
                                unselectedLabelColor: Colors.grey,
                                isScrollable: false,

                          tabs: [
                              Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 40,
                                  alignment: Alignment.center,
                                  child: Text('My Contests ('+contestList.length.toString()+')',style: TextStyle(fontSize: 13,fontFamily: 'noway'),),


                              ),
                              Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 40,
                                    alignment: Alignment.center,
                                    child: Text( 'My Teams ('+ teamList.length.toString()+')',style: TextStyle(
                                        fontSize: 13,fontFamily: 'noway'),)
                              ),
                              Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 40,
                                  alignment: Alignment.center,
                                  child: Text( 'Player Stats',style: TextStyle(
                                      fontSize: 13,fontFamily: 'noway')),
                              ),
                          ]


                                // tabs: [
                                //   new ClipRRect(
                                //     // borderRadius: BorderRadius.horizontal(
                                //     //     left: Radius.circular(5)),
                                //     child: new Container(
                                //       height: 45,
                                //       alignment: Alignment.center,
                                //       margin: EdgeInsets.zero,
                                //       decoration: BoxDecoration(
                                //         border: Border(
                                //             right: BorderSide(
                                //               color: bgColorDark,
                                //               width: 1,
                                //               style: BorderStyle.solid,
                                //             )),
                                //         color: _currentMatchIndex == 0
                                //             ? Colors.white
                                //             : Colors.white,
                                //       ),
                                //       child: Text(
                                //         'My Contests ('+contestList.length.toString()+')',
                                //         style: TextStyle(
                                //             fontFamily: AppConstants.textBold,
                                //             color: _currentMatchIndex == 0
                                //                 ? primaryColor
                                //                 : Colors.grey,
                                //             fontWeight: FontWeight.normal,
                                //             fontSize: 13),
                                //       ),
                                //     ),
                                //   ),
                                //   new Container(
                                //     height: 45,
                                //     alignment: Alignment.center,
                                //     padding: EdgeInsets.all(0),
                                //     margin: EdgeInsets.all(0),
                                //     decoration: BoxDecoration(
                                //       border: Border(
                                //         right: BorderSide(
                                //             color: bgColorDark,
                                //             width: 1,
                                //             style: BorderStyle.solid),
                                //       ),
                                //       color: _currentMatchIndex == 1
                                //           ? primaryColor
                                //           : Colors.white,
                                //     ),
                                //     child: Text(
                                //       'My Teams ('+ teamList.length.toString()+')',
                                //       style: TextStyle(
                                //           fontFamily: AppConstants.textBold,
                                //           color: _currentMatchIndex == 1
                                //               ? Colors.white
                                //               : Colors.grey,
                                //           fontWeight: FontWeight.normal,
                                //           fontSize: 13),
                                //     ),
                                //   ),
                                //   new ClipRRect(
                                //     borderRadius: BorderRadius.horizontal(
                                //         right: Radius.circular(5)),
                                //     child: new Container(
                                //       height: 45,
                                //       alignment: Alignment.center,
                                //       decoration: BoxDecoration(
                                //         border: Border(
                                //             right: BorderSide(
                                //               color: bgColorDark,
                                //               width: 1,
                                //               style: BorderStyle.solid,
                                //             )),
                                //         color: _currentMatchIndex == 2
                                //             ? primaryColor
                                //             : Colors.white,
                                //       ),
                                //       child: Text(
                                //         'Stats',
                                //         style: TextStyle(
                                //             fontFamily: AppConstants.textBold,
                                //             color: _currentMatchIndex == 2
                                //                 ? Colors.white
                                //                 : Colors.grey,
                                //             fontWeight: FontWeight.normal,
                                //             fontSize: 13),
                                //       ),
                                //     ),
                                //   )
                                // ],
                              ),
                            ),
                          ),
                          !loading?Expanded(
                            child: new Container(
                              color:Colors.white,
                              child: SingleChildScrollView(
                                child: new Column(
                                  children: [
                                    ( _currentMatchIndex == 0||_currentMatchIndex == 1)&&!widget.model.isFromLive!?new Container(
                                      child: new Card(
                                        clipBehavior: Clip.antiAlias,
                                        elevation: 5,
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(3)),
                                        margin: EdgeInsets.only(left: 10,right: 10,top: 8,bottom: 8),
                                        // child: Container(
                                        //   padding: EdgeInsets.all(0),
                                        //   child: new Container(
                                        //     child: new Column(
                                        //       children: [
                                        //         new Container(
                                        //           margin: EdgeInsets.only(left: 10,right: 10,top: 5,bottom: 5),
                                        //           padding: EdgeInsets.all(5),
                                        //           child: new Row(
                                        //             mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        //             children: [
                                        //               new Container(
                                        //                 child: new Column(
                                        //                   crossAxisAlignment: CrossAxisAlignment.start,
                                        //                   children: [
                                        //                     Text(
                                        //                       'Investment',
                                        //                       style: TextStyle(
                                        //                           color: Colors.grey,
                                        //                           fontWeight: FontWeight.w500,
                                        //                           fontSize: 11),
                                        //                     ),
                                        //                     new Container(
                                        //                       height: 25,
                                        //                       alignment: Alignment.centerLeft,
                                        //                       child: Text(
                                        //                         '₹'+refreshScoreItem.total_investment.toString(),
                                        //                         style: TextStyle(
                                        //                             color: Colors.red,
                                        //                             fontWeight: FontWeight.w800,
                                        //                             fontSize: 15),
                                        //                       ),
                                        //                     ),
                                        //
                                        //                   ],
                                        //                 ),
                                        //               ),
                                        //               new Container(
                                        //                 child: new Column(
                                        //                   crossAxisAlignment: CrossAxisAlignment.center,
                                        //                   children: [
                                        //                     Text(
                                        //                       'Winnings',
                                        //                       style: TextStyle(
                                        //                           color: Colors.grey,
                                        //                           fontWeight: FontWeight.w500,
                                        //                           fontSize: 11),
                                        //                     ),
                                        //                     new Container(
                                        //                       height: 25,
                                        //                       alignment: Alignment.center,
                                        //                       child: Text(
                                        //                         '₹'+refreshScoreItem.total_winning.toString(),
                                        //                         style: TextStyle(
                                        //                             color: orangeColor,
                                        //                             fontWeight: FontWeight.w800,
                                        //                             fontSize: 15),
                                        //                       ),
                                        //                     ),
                                        //
                                        //
                                        //                   ],
                                        //                 ),
                                        //               ),
                                        //               new Container(
                                        //                 child: new Column(
                                        //                   crossAxisAlignment: CrossAxisAlignment.end,
                                        //                   children: [
                                        //                     Text(
                                        //                       'Profit',
                                        //                       style: TextStyle(
                                        //                           color: double.parse((refreshScoreItem.total_profit??0).toString())>0?greenColor:primaryColor,
                                        //                           fontWeight: FontWeight.w500,
                                        //                           fontSize: 11),
                                        //                     ),
                                        //                     new Container(
                                        //                       height: 25,
                                        //                       alignment: Alignment.centerLeft,
                                        //                       child: Text(
                                        //                         '₹'+(refreshScoreItem.total_profit??'').toString(),
                                        //                         style: TextStyle(
                                        //                             color: double.parse(refreshScoreItem.total_profit!.toString())>0?greenColor:primaryColor,
                                        //                             fontWeight: FontWeight.w800,
                                        //                             fontSize: 15),
                                        //                       ),
                                        //                     ),
                                        //
                                        //
                                        //                   ],
                                        //                 ),
                                        //               ),
                                        //             ],
                                        //           ),
                                        //         ),
                                        //       ],
                                        //     ),
                                        //   ),
                                        // ),
                                      ),
                                    ):new Container(),
                                    _currentMatchIndex == 0
                                        ? new Container(
                                      child: contestList.length>0?new ListView.builder(
                                          padding: EdgeInsets.only(top: 0),
                                          physics:
                                          NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          itemCount: contestList.length,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            return new LiveFinishContestAdapter(widget.model,contestList[index]);
                                          }):new Container(
                                        margin: EdgeInsets.only(top: 20),
                                        width: MediaQuery.of(context).size.width,
                                        alignment: Alignment.center,
                                        child: Text(
                                          "You haven't joined any challenge for this match.",
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14),
                                        ),
                                      ),
                                    )
                                        : _currentMatchIndex == 1
                                        ? new Container(
                                      child: teamList.length>0?new ListView.builder(
                                          padding: EdgeInsets.only(top: 0),
                                          physics:
                                          NeverScrollableScrollPhysics(),
                                          shrinkWrap: true,
                                          scrollDirection: Axis.vertical,
                                          itemCount: teamList.length,
                                          itemBuilder:
                                              (BuildContext context,
                                              int index) {
                                            return Container(margin: EdgeInsets.all(10),child: new TeamItemAdapter(widget.model,teamList[index],false,index));
                                          }):new Container(
                                        margin: EdgeInsets.only(top: 20),
                                        width: MediaQuery.of(context).size.width,
                                        alignment: Alignment.center,
                                        child: Text(
                                          "You haven't created any team yet for this fantasy.",
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 14),
                                        ),
                                      ),
                                    )
                                        : new PlayerPoints(pointsList,widget.model)
                                  ],
                                ),
                              ),
                            ),
                          ):new Container(),
                        ],
                      ),
                    ),
                  ),
                )),
          ],
        ),
      ),
    );
  }

  List<Widget> getTabs() {
    tabs.clear();
    fantasyTypes.clear();
    tabs.add(getFullMatchTab());
    fantasyTypes.add(0);
    if(widget.model.battingFantasy==1){
      tabs.add(getBattingTab());
      fantasyTypes.add(2);
    }
    if(widget.model.bowlingFantasy==1){
      tabs.add(getBowlingTab());
      fantasyTypes.add(3);
    }
    if(widget.model.reverseFantasy==1){
      tabs.add(getReverseTab());
      fantasyTypes.add(5);
    }
    return tabs;
  }
  int getFantasyTabCount(){
    int count=1;
    if(widget.model.battingFantasy==1){
      count+=1;
    }
    if(widget.model.bowlingFantasy==1){
      count+=1;
    }
    if(widget.model.reverseFantasy==1){
      count+=1;
    }
    return count;
  }

  Widget getFullMatchTab() {
    return new Container(
      height: 45,
      alignment: Alignment.center,
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Text(
        'Full Match',
        style: TextStyle(
            fontFamily: AppConstants.textBold,
            color: _currentIndex == 0 ? primaryColor : Colors.grey,
            fontWeight: FontWeight.w400,
            fontSize: 14),
      ),
    );
  }

  Widget getBattingTab() {
    return new Container(
      height: 45,
      alignment: Alignment.center,
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Text(
        'Batting',
        style: TextStyle(
            fontFamily: AppConstants.textBold,
            color: _currentIndex == 1 ? primaryColor : Colors.grey,
            fontWeight: FontWeight.w400,
            fontSize: 14),
      ),
    );
  }

  Widget getBowlingTab() {
    return new Container(
      height: 45,
      alignment: Alignment.center,
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Text(
        'Bowling',
        style: TextStyle(
            fontFamily: AppConstants.textBold,
            color: _currentIndex == 2 ? primaryColor : Colors.grey,
            fontWeight: FontWeight.w400,
            fontSize: 14),
      ),
    );
  }

  Widget getReverseTab() {
    return new Container(
      height: 45,
      alignment: Alignment.center,
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: Text(
        'Reverse',
        style: TextStyle(
            fontFamily: AppConstants.textBold,
            color: _currentIndex == 3 ? primaryColor : Colors.grey,
            fontWeight: FontWeight.w400,
            fontSize: 14),
      ),
    );
  }
  void getMatchScores() async {
    setState(() {
      loading=true;
    });
    AppLoaderProgress.showLoader(context);
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.slotId.toString());
    final client = ApiClient(AppRepository.dio);
    RefreshScoreResponse response = await client.getMatchScores(contestRequest);
    print("response ${jsonDecode(jsonEncode(response))}");
    if (response.status == 1) {
          refreshScoreItem=response.result!;
          contestList=response.result!.contest??[];
          teamList=response.result!.teams??[];
          LiveFinishedScoreItem scoreItem=refreshScoreItem.matchruns![0];
          team1Score=widget.model.sportKey=='CRICKET'?scoreItem.Team1_Totalruns.toString() + "/" + scoreItem.Team1_Totalwickets.toString() + " (" + scoreItem.Team1_Totalovers.toString()+ ")":scoreItem.Team1_Totalruns.toString();
          team2Score=widget.model.sportKey=='CRICKET'?scoreItem.Team2_Totalruns.toString() + "/" + scoreItem.Team2_Totalwickets.toString() + " (" + scoreItem.Team2_Totalovers.toString()+ ")":scoreItem.Team2_Totalruns.toString();
          tvWinningText=scoreItem.Winning_Status??'';
          widget.model.team1Score=team1Score;
          widget.model.team2Score=team2Score;
          widget.model.winningText=tvWinningText;
         print("widget.model.sportKey : ${scoreItem.Team1_Totalruns.toString()}");
    }
    setState(() {
      loading=false;
      AppLoaderProgress.hideLoader(context);
    });
  }
  void getPlayerPoints() async {
    AppLoaderProgress.showLoader(context);
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.slotId.toString());
    final client = ApiClient(AppRepository.dio);
    PlayerPointsResponse response = await client.getPlayerPoints(contestRequest);
    if (response.status == 1) {
      pointsList=response.result!;
    }
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
  }
}
