import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:package_info/package_info.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/views/Login.dart';
import 'package:EverPlay/views/LoginNew.dart';

class More extends StatefulWidget {
  PackageInfo _packageInfo;
  More(this._packageInfo);
  @override
  _MoreState createState() => _MoreState();
}

class _MoreState extends State<More> {
  bool logout_dialog = false;
  int isVisiblePromoterLeaderboard=0;
  int isVisibleAffiliate=0;
  int isVisiblePromote=0;
  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.IS_VISIBLE_PROMOTER_LEADERBOARD)
        .then((value) =>
    {
      setState(() {
        isVisiblePromoterLeaderboard = int.parse('0');
      })
    });
    AppPrefrence.getInt(AppConstants.IS_VISIBLE_AFFILIATE,0)
        .then((value) =>
    {
      setState(() {
        isVisibleAffiliate = value;
      })
    });
    AppPrefrence.getInt(AppConstants.IS_VISIBLE_PROMOTE,0)
        .then((value) =>
    {
      setState(() {
        isVisiblePromote = value;
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.light) /* set Status bar icon color in iOS. */
    );
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(

        padding: EdgeInsets.only(top: 35),
        child: new Stack(
          children: [
            new SingleChildScrollView(
              child: new Stack(
                children: [
                  new Column(
                    children: [
                      SizedBox(height: 10),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          height: 50,
                          color: Color(0xffF8F6F4),
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreProfileIcon),
                                    ),
                                    new Text('Profile',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToUserProfile(context)
                        },
                      ),
                      SizedBox(height: 5),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreReferIcon),
                                    ),
                                    new Text('Refer & Earn',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToReferAndEarn(context)
                        },
                      ),
                      SizedBox(height: 5),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreVerifiedIcon),
                                    ),
                                    new Text('Verify Account',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToVerifyAccount(context)
                        },
                      ),
                      SizedBox(height: 5),
                      isVisiblePromoterLeaderboard==1?new Column(
                        children: [
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              color: Color(0xffF8F6F4),
                              height: 50,
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  new Container(
                                    padding:EdgeInsets.all(5),
                                    child: new Row(
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(left: 10,right: 15),
                                          height: 30,
                                          width: 30,
                                          child: Image.asset(AppImages.moreLeaderboardIcon),
                                        ),
                                        new Text('Promoter Leaderboard',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(right: 15),
                                    height: 10,
                                    width: 10,
                                    child: Image.asset(AppImages.moreForwardIcon),
                                  ),
                                ],
                              ),
                            ),
                            onTap: (){
                              navigateToPromoterLeaderboard(context);
                            },
                          ),
                          new Divider(
                            height: 5,
                            thickness: 1,
                          )
                        ],
                      ):new Container(),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreFantasyPointIcon),
                                    ),
                                    new Text('Fantasy Point System',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToVisionWebView(context,'Fantasy Point System',AppConstants.fantasy_point_url)
                        },
                      ),
                      SizedBox(height: 5),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreReferListIcon),
                                    ),
                                    new Text('Refer List',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToReferList(context)
                        },
                      ),
                      SizedBox(height: 5),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreResponsiblePlayIcon),
                                    ),
                                    new Text('Responsible play',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToVisionWebView(context,'Responsible play',AppConstants.responsible_play)
                        },
                      ),
                      SizedBox(height: 5),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.morePrivacyIcon),
                                    ),
                                    new Text('Privacy Policy',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToVisionWebView(context,'Privacy Policy',AppConstants.privacy_url)
                        },
                      ),
                      SizedBox(height: 5),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreDocsIcon),
                                    ),
                                    new Text('Terms & Condition',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToVisionWebView(context,'Terms & Condition',AppConstants.terms_url)
                        },
                      ),

                      SizedBox(height: 5),

                      new Visibility(
                        visible: true,
                        child: new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            color: Color(0xffF8F6F4),
                            height: 50,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Container(
                                  padding:EdgeInsets.all(5),
                                  child: new Row(
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(left: 10,right: 15),
                                        height: 30,
                                        width: 30,
                                        child: Image.asset(AppImages.moreLegalityIcon),
                                      ),
                                      new Text('Legality',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                    ],
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(right: 15),
                                  height: 10,
                                  width: 10,
                                  child: Image.asset(AppImages.moreForwardIcon),
                                ),
                              ],
                            ),
                          ),
                          onTap: ()=>{
                            navigateToVisionWebView(context,'Legality',AppConstants.legality_url)
                          },
                        ),
                      ),

                      new Visibility(
                        visible: true,
                        child: new SizedBox(height: 5),
                      ),

                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreInformationIcon),
                                    ),
                                    new Text('About Us',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToVisionWebView(context,'About Us',AppConstants.about_us_url)
                        },
                      ),
                      SizedBox(height: 5),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreFindIcon),
                                    ),
                                    new Text('How to Play',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToVisionWebView(context,'How to Play',AppConstants.how_to_play_url)
                        },
                      ),
                      SizedBox(height: 5),

                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreCallIcon),
                                    ),
                                    new Text('Contact Us',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: ()=>{
                          navigateToContactUs(context)
                        },
                      ),
                      SizedBox(height: 5),
                      isVisibleAffiliate==1?new Column(
                        children: [
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              color: Color(0xffF8F6F4),
                              height: 50,
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  new Container(
                                    padding:EdgeInsets.all(5),
                                    child: new Row(
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(left: 10,right: 15),
                                          height: 18,
                                          width: 18,
                                          child: Image.asset(AppImages.moreAffiliateMarketIcon),
                                        ),
                                        new Text('Affiliate',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(right: 15),
                                    height: 10,
                                    width: 10,
                                    child: Image.asset(AppImages.moreForwardIcon),
                                  ),
                                ],
                              ),
                            ),
                            onTap: (){
                              navigateToAffiliateProgram(context);
                            },
                          ),
                          SizedBox(height: 5),
                        ],
                      ):new Container(),
                      isVisiblePromote==1?new Column(
                        children: [
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              color: Color(0xffF8F6F4),
                              height: 50,
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  new Container(
                                    padding:EdgeInsets.all(5),
                                    child: new Row(
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(left: 10,right: 15),
                                          height: 18,
                                          width: 18,
                                          child: Image.asset(AppImages.morePromoteIcon),
                                        ),
                                        new Text('Promote App',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(right: 15),
                                    height: 10,
                                    width: 10,
                                    child: Image.asset(AppImages.moreForwardIcon),
                                  ),
                                ],
                              ),
                            ),
                            onTap: (){
                              navigateToPromoteApp(context);
                            },
                          ),
                          SizedBox(height: 5),
                        ],
                      ):new Container(),
                      SizedBox(height: 10),

                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          color: Color(0xffF8F6F4),
                          height: 50,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Container(
                                padding:EdgeInsets.all(5),
                                child: new Row(
                                  children: [
                                    new Container(
                                      margin: EdgeInsets.only(left: 10,right: 15),
                                      height: 30,
                                      width: 30,
                                      child: Image.asset(AppImages.moreLogoutIcon),
                                    ),
                                    new Text('Logout',style: TextStyle(color: Colors.black,fontSize: 14,fontWeight: FontWeight.w500),),
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.only(right: 15),
                                height: 10,
                                width: 10,
                                child: Image.asset(AppImages.moreForwardIcon),
                              ),
                            ],
                          ),
                        ),
                        onTap: (){
                          setState(() {
                            logout_dialog=true;
                          });
                        },
                      ),
                      SizedBox(height: 5),
                      new Container(
                        width: MediaQuery.of(context).size.width,
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(top: 10,bottom: 40,left: 10,right: 10),
                        child: new Text('Version '+widget._packageInfo.version+'('+widget._packageInfo.buildNumber+')',style: TextStyle(color: Colors.grey,fontSize: 14,fontWeight: FontWeight.w500),),
                      )
                    ],
                  ),
                ],
              ),
            ),
            logout_dialog
                ? new Container(
              color: Colors.black38,
              child: AlertDialog(
                title: Text("Logout"),
                content: Text("Do you want to logout?"),
                actions: [
                  cancelButton(context),
                  continueButton(context),
                ],
              ),
            )
                : new Container(),
          ],
        ),
      ),
    );
  }
  Widget cancelButton(context) {
    return TextButton(
      child: Text("NO"),
      onPressed: () {
        setState(() {
          logout_dialog = false;
        });
      },
    );
  }

  Widget continueButton(context) {
    return TextButton(
      child: Text("YES"),
      onPressed: () {
        AppPrefrence.clearPrefrence();
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (context) => new LoginNew()
            ),
            ModalRoute.withName("/main")
        );
      },
    );
  }
}
