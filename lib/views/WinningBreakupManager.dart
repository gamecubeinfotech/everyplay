
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/dataModels/JoinChallengeDataModel.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/category_contest_response.dart';
import 'package:EverPlay/repository/model/create_private_contest_request.dart';
import 'package:EverPlay/repository/model/create_private_contest_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class WinningBreakupManager extends StatefulWidget {
  CreatePrivateContestRequest request;
  GeneralModel model;
  Function onTeamCreated;
  WinningBreakupManager(this.request,this.model,this.onTeamCreated);
  @override
  _WinningBreakupManagerState createState() => new _WinningBreakupManagerState();
}

class _WinningBreakupManagerState extends State<WinningBreakupManager>{

  TextEditingController numberController = TextEditingController();
  bool _enable=false;
  List<WinnersList> winnersList=[];
  List<WinnerBreakUpData> breakupData=[];

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Themecolor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.light) /* set Status bar icon color in iOS. */
    );
  }


  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            // padding: EdgeInsets.only(top: 40),
            color: Themecolor,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.white,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Create Contest',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Stack(
          alignment: Alignment.bottomCenter,
          children: [
            new Container(
              height: MediaQuery.of(context).size.height,
              child: new SingleChildScrollView(
                padding: EdgeInsets.only(bottom: 50),
                child: new Container(
                  child: new Column(
                    children: [
                      new MatchHeader(widget.model.teamVs!,widget.model.headerText!,widget.model.isFromLive??false,widget.model.isFromLiveFinish??false),

                      new Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: bordercolor),
                            borderRadius: BorderRadius.all(Radius.circular(2)),color: TextFieldColor
                        ),
                        padding: EdgeInsets.all(10),
                       // color: bgColor,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Container(
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  new Container(
                                    margin: EdgeInsets.only(bottom: 5),
                                    alignment: Alignment.center,
                                    child: new Text('Prize Pool',
                                      style: TextStyle(
                                          fontFamily: AppConstants.textRegular,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12,decoration: TextDecoration.none),),
                                  ),
                                  new Container(
                                    height: 30,
                                    padding: EdgeInsets.only(right: 12),
                                    alignment: Alignment.centerLeft,
                                    child: new Text('₹'+widget.request.win_amount!,
                                      style: TextStyle(
                                          fontFamily: AppConstants.textSemiBold,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18,decoration: TextDecoration.none),),
                                  )
                                ],
                              ),
                            ),
                            new Container(
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  new Container(
                                    margin: EdgeInsets.only(bottom: 5),
                                    alignment: Alignment.center,
                                    child: new Text('Contest Size',
                                      style: TextStyle(
                                          fontFamily: AppConstants.textRegular,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12,decoration: TextDecoration.none),),
                                  ),
                                  new Container(
                                    height: 30,
                                    alignment: Alignment.center,
                                    child: new Text(widget.request.maximum_user!,
                                      style: TextStyle(
                                          fontFamily: AppConstants.textSemiBold,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18,decoration: TextDecoration.none),),
                                  )
                                ],
                              ),
                            ),
                            new Container(
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  new Container(
                                    margin: EdgeInsets.only(bottom: 5),
                                    alignment: Alignment.center,
                                    padding: EdgeInsets.only(left: 30),
                                    child: new Text('Entry',
                                      style: TextStyle(
                                          fontFamily: AppConstants.textRegular,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12,decoration: TextDecoration.none),),
                                  ),
                                  new Container(
                                    alignment: Alignment.centerRight,
                                    child: new Card(
                                      color: yellowdark,
                                      child: new Container(
                                        height: 30,
                                        width: 65,
                                        alignment: Alignment.center,
                                        child: new Text(
                                          '₹'+double.parse(widget.request.entryfee!).toStringAsFixed(2),
                                          style: TextStyle(fontFamily: 'noway',fontSize: 14,fontWeight: FontWeight.w500,color: Colors.white,),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.only(left: 10,top: 20),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Choose total number of winners',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.all(10),
                        child: new Stack(
                          alignment: Alignment.centerRight,
                          children: [
                            new Container(
                              height: 45,
                              child: TextField(
                                controller: numberController,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  counterText: "",
                                  fillColor: TextFieldColor,
                                  filled: true,
                                  hintStyle: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                  ),
                                  hintText: 'Number of winners',
                                  enabledBorder:  OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderSide:
                                     BorderSide(color: bordercolor, width: 0.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderSide:  BorderSide(color: bordercolor, width: 0.0),
                                  ),
                                ),
                              ),
                            ),
                            new GestureDetector(
                              child: new Container(
                                alignment: Alignment.centerRight,
                                child: new Card(
                                  color: yellowdark,
                                  child: new Container(
                                    height: 35,
                                    width: 70,
                                    alignment: Alignment.center,
                                    child: new Text(
                                      'SET',
                                      style: TextStyle(fontFamily: 'noway',fontSize: 16,fontWeight: FontWeight.w500,color: Colors.white,),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ),
                              ),
                              onTap: (){
                                if (numberController.text.isNotEmpty) {
                                  FocusScope.of(context).unfocus();
                                  if (int.parse(numberController.text.toString()) > int.parse(widget.request.maximum_user!)) {
                                    MethodUtils.showError(context, "Number of winners can't be more than number of participants.");
                                  }
                                  else{
                                    winnersList.clear();
                                    for(int i=0; i<int.parse(numberController.text.toString()); i++){
                                      TextEditingController percentController = TextEditingController();
                                      // percentController..addListener(() {
                                      //
                                      // });
                                      double percent=100/double.parse(numberController.text.toString());
                                      percentController.text=percent.toStringAsFixed(2);
                                      String amount=(double.parse(widget.request.win_amount!)/double.parse(numberController.text.toString())).toStringAsFixed(2);
                                      winnersList.add(WinnersList(i,amount,percentController));
                                    }
                                    setState(() {});
                                  }
                                }
                                else{
                                  MethodUtils.showError(context, 'Please enter challenges count');
                                }
                              },
                            ),
                          ],
                        ),
                      ),
                      new Container(
                        padding: EdgeInsets.only(top: 10,bottom: 20),
                        alignment: Alignment.center,
                        child: Text(
                          'If you want change winning breakup change winning %',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.grey,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                      new Divider(height: 1,thickness: 1,color: bordercolor,),
                      new Container(
                        height: 30,
                        color: Colors.white,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            new Flexible(child: new Container(
                              margin: EdgeInsets.only(left: 10),
                              child: Text(
                                'Rank',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 10),
                              ),
                            ),flex: 1,),
                            new Flexible(child: new Container(
                              alignment: Alignment.centerRight,
                              child: Text(
                                'Winning%',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 10),
                              ),
                            ),flex: 1,),
                            new Flexible(child: new Container(
                              margin: EdgeInsets.only(right: 10,),
                              alignment: Alignment.centerRight,
                              child: Text(
                                'Winning Amount',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 10),
                              ),
                            ),flex: 1,),
                          ],
                        ),
                      ),
                      new Divider(height: 1,thickness: 1,color: bordercolor,),
                      new Column(
                        children: winnersList,
                      )
                    ],
                  ),
                ),
              ),
            ),
            new Container(
                width:
                MediaQuery.of(context).size.width,
                height: 45,
                child: ElevatedButton(
                  onPressed: () {
                    createContest();
                  },
                  style: ElevatedButton.styleFrom(
                    primary: Themecolor,
                    elevation: 0.5,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0),
                    ),
                  ),
                  child: Text(
                    'Create Contest',
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.white,
                    ),
                  ),
                )
            )
          ],
        ),
      ),
    );
  }

  void createContest() async {
    FocusScope.of(context).unfocus();
    breakupData.clear();
    for(int i=0; i<winnersList.length; i++){
      WinnerBreakUpData winnerBreakUpData=new WinnerBreakUpData();
      winnerBreakUpData.rank=(winnersList[i].index+1).toString();
      winnerBreakUpData.winningPer=winnersList[i].percentController.text.toString();
      winnerBreakUpData.winningAmmount=winnersList[i].amount.toString();
      breakupData.add(winnerBreakUpData);
    }
    widget.request.pricecards=breakupData;
    AppLoaderProgress.showLoader(context);

    final client = ApiClient(AppRepository.dio);
    CreatePrivateContestResponse myBalanceResponse = await client.createContest(widget.request);
    AppLoaderProgress.hideLoader(context);
    if (myBalanceResponse.status == 1) {
      Fluttertoast.showToast(msg: 'Contest Created Successfully', toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
      Contest contest=myBalanceResponse.result![0].contest!;
      if(widget.model.teamCount==1){
        MethodUtils.checkBalance(new JoinChallengeDataModel(context, 0, int.parse(widget.request.user_id!), contest.id!, widget.model.fantasyType!, widget.model.slotId!, 1, contest.is_bonus!, contest.win_amount!, contest.maximum_user!, widget.model.teamId.toString(), contest.entryfee.toString(), widget.model.matchKey!, widget.model.sportKey!, widget.model.joinedSwitchTeamId.toString(), false, 0, 0,onJoinContestResult));
      }
      else if(widget.model.teamCount!>0){
        Navigator.pop(context);
        Navigator.pop(context);
        navigateToMyJoinTeams(context,widget.model,contest,onJoinContestResult);
      }
      else{
        widget.model.onJoinContestResult=onJoinContestResult;
        widget.model.contest=contest;
        navigateToCreateTeam(context,widget.model);
      }
      return;
    }
    Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
  }
  void onJoinContestResult(int isJoined,String referCode){
    if(widget.model.teamCount==0||widget.model.teamCount==1){
      Navigator.pop(context);
      Navigator.pop(context);
    }
    widget.model.teamCount=widget.model.teamCount!+1;
    widget.onTeamCreated();
  }
}

class WinnersList extends StatelessWidget{
  int index;
  String amount;
  TextEditingController percentController;
  WinnersList(this.index,this.amount,this.percentController);
  @override
  Widget build(BuildContext context) {
      return new Container(
        height: 45,
        padding: EdgeInsets.all(5),
        child: new Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            new Container(
              margin: EdgeInsets.only(left: 10),
              child: Text(
                '#'+(index+1).toString(),
                style: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.w500,
                    fontSize: 13),
              ),
            ),
            new Container(
              width: 100,
              height: 45,
               margin: EdgeInsets.only(left: 22),
              child: TextField(
                cursorColor: Colors.black,
                cursorWidth: 1,
                enabled: false,
                controller: percentController,
                keyboardType: TextInputType.number,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 13,
                  color: Colors.grey,
                  fontStyle: FontStyle.normal,
                  fontWeight: FontWeight.normal,
                ),
                decoration: InputDecoration(
                  counterText: "",
                  fillColor: Colors.white,
                  filled: true,
                  hintStyle: TextStyle(
                    fontSize: 12,
                    color: Colors.grey,
                    fontStyle: FontStyle.normal,
                    fontWeight: FontWeight.normal,
                  ),
                  hintText: '',
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                  ),
                ),
              ),
            ),
            new Container(
              margin: EdgeInsets.only(right: 10,left: 0),
              alignment: Alignment.center,
              child: Text(
                '₹'+amount,
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    fontSize: 13),
              ),
            ),
          ],
        ),
      );
  }
}