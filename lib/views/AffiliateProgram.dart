import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/promoter_total_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class AffiliateProgram extends StatefulWidget {
  @override
  _AffiliateProgramState createState() => new _AffiliateProgramState();
}

class _AffiliateProgramState extends State<AffiliateProgram>{

  TextEditingController codeController = TextEditingController();
  bool _enable=false;
  String start_date='';
  String end_date='';
  late String userId='0';
  PromoterTotalResult result=new PromoterTotalResult(winning: '0',deposit: '0',matches: '0',team_join: '0',aff_bal: '0');
  DateFormat dateFormat=new DateFormat("dd-MM-yyyy");
  DateFormat apDateFormat=new DateFormat("yyyy-MM-dd");
  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
      })
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: primaryColor,
            // padding: EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.white,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Affiliate Program',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Container(
          child: new Column(
            children: [
              new Container(
                margin: EdgeInsets.only(top: 30),
                child: new Row(
                  children: [
                    new Flexible(child: new Container(child: new Column(
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            alignment: Alignment.center,
                            child: new Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: new Container(
                                height: 60,
                                width: 150,
                                alignment: Alignment.center,
                                child: new Text(
                                  start_date.isEmpty?'Start Date':start_date,
                                  style: TextStyle(fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black,),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                          onTap: () async {
                            DateTime date = DateTime(1900);

                            date = (await showDatePicker(
                            context: context,
                            initialDate:DateTime.now(),
                            firstDate:DateTime(1900),
                            lastDate: DateTime.now()))!;

                            start_date = dateFormat.format(date);
                            setState(() {});
                          },
                        ),
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            alignment: Alignment.center,
                            child: new Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: new Container(
                                height: 60,
                                width: 150,
                                alignment: Alignment.center,
                                child: new Text(
                                  end_date.isEmpty?'End Date':end_date,
                                  style: TextStyle(fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black,),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                          onTap: () async {
                            DateTime date = DateTime(1900);

                            date = (await showDatePicker(
                                context: context,
                                initialDate:DateTime.now(),
                                firstDate:DateTime(1900),
                                lastDate: DateTime.now()))!;

                            end_date = dateFormat.format(date);
                            setState(() {});
                          },
                        ),
                      ],
                    ),),flex: 1,),
                    new Flexible(child: new Container(child: new Column(
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            margin: EdgeInsets.only(left: 5),
                            alignment: Alignment.center,
                            child: new Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              color: primaryColor,
                              child: new Container(
                                height: 60,
                                width: 150,
                                alignment: Alignment.center,
                                child: new Text(
                                  'Get Details',
                                  style: TextStyle(fontSize: 14,fontWeight: FontWeight.w400,color: Colors.white,),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                          onTap: (){
                              if(start_date.isEmpty){
                                MethodUtils.showError(context, "Please set a start date");
                              }else if(end_date.isEmpty){
                                MethodUtils.showError(context, "Please set an end date");
                              }else{
                                getAffiliationTotal();
                              }
                          },
                        ),
                      ],
                    ),),flex: 1,)

                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 40),
                child: new Row(
                  children: [
                    new Flexible(child: new Container(child: new Column(
                      children: [
                        new Container(
                          height: 115,
                          margin: EdgeInsets.only(left: 10,right: 5),
                          alignment: Alignment.center,
                          child: new Stack(
                            alignment: Alignment.bottomCenter,
                            children: [
                              new Container(
                                margin: EdgeInsets.only(bottom: 10),
                                child: new Card(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      new Container(
                                        alignment: Alignment.center,
                                        child: new Text(
                                          'Match',
                                          style: TextStyle(fontFamily:'roboto',fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black,),
                                          textAlign: TextAlign.center,
                                        ),
                                      ),
                                      new Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(top: 10),
                                        child: new Text(
                                          result.matches!.toString(),
                                          style: TextStyle(fontFamily:'roboto',fontSize: 16,fontWeight: FontWeight.w500,color: Colors.black,),
                                          textAlign: TextAlign.center,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              // new Container(
                              //   height: 30,
                              //   width: 100,
                              //   margin: EdgeInsets.only(left: 5),
                              //   alignment: Alignment.center,
                              //   child: new Card(
                              //     shape: RoundedRectangleBorder(
                              //       borderRadius: BorderRadius.circular(10.0),
                              //     ),
                              //     color: orangeColor,
                              //     child:  new GestureDetector(
                              //       behavior: HitTestBehavior.translucent,
                              //       child: new Container(
                              //         alignment: Alignment.center,
                              //         child: new Text(
                              //           'View Details',
                              //           style: TextStyle(fontSize: 12,fontWeight: FontWeight.w500,color: Colors.white,),
                              //           textAlign: TextAlign.center,
                              //         ),
                              //       ),
                              //       onTap: (){
                              //         navigateToAffiliateMatches(context,apDateFormat.format(dateFormat.parse(start_date)),apDateFormat.format(dateFormat.parse(end_date)));
                              //       },
                              //     ),
                              //   ),
                              // ),
                            ],
                          ),
                        ),

                      ],
                    ),),flex: 1,),
                    new Flexible(child: new Container(child: new Column(
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            height: 115,
                            margin: EdgeInsets.only(left: 5,right: 10),
                            alignment: Alignment.center,
                            child: new Container(
                              margin: EdgeInsets.only(bottom: 10),
                              child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    new Container(
                                      alignment: Alignment.center,
                                      child: new Text(
                                        'Team Join',
                                        style: TextStyle(fontFamily:'roboto',fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black,),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.center,
                                      margin: EdgeInsets.only(top: 10),
                                      child: new Text(
                                        result.team_join!.toString(),
                                        style: TextStyle(fontFamily:'roboto',fontSize: 16,fontWeight: FontWeight.w500,color: Colors.black,),
                                        textAlign: TextAlign.center,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          onTap: (){

                          },
                        ),
                      ],
                    ),),flex: 1,)

                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.only(top: 5),
                child: new Row(
                  children: [
                    new Flexible(child: new Container(child: new Column(
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            height: 115,
                            margin: EdgeInsets.only(left: 10,right: 5),
                            alignment: Alignment.center,
                            child: new Stack(
                              alignment: Alignment.bottomCenter,
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(bottom: 10),
                                  child: new Card(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        new Container(
                                          alignment: Alignment.center,
                                          child: new Text(
                                            'Deposit',
                                            style: TextStyle(fontFamily:'roboto',fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black,),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        new Container(
                                          alignment: Alignment.center,
                                          margin: EdgeInsets.only(top: 10),
                                          child: new Text(
                                            '₹'+result.deposit!.toString(),
                                            style: TextStyle(fontFamily:'roboto',fontSize: 16,fontWeight: FontWeight.w500,color: Colors.black,),
                                            textAlign: TextAlign.center,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: (){
                          },
                        ),
                      ],
                    ),),flex: 1,),
                    new Flexible(child: new Container(child: new Column(
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            height: 115,
                            margin: EdgeInsets.only(left: 5,right: 10),
                            alignment: Alignment.center,
                            child: new Container(
                              margin: EdgeInsets.only(bottom: 10),
                              child: new Card(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    new Container(
                                      alignment: Alignment.center,
                                      child: new Text(
                                        'Winning',
                                        style: TextStyle(fontFamily:'roboto',fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black,),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                    new Container(
                                      alignment: Alignment.center,
                                      margin: EdgeInsets.only(top: 10),
                                      child: new Text(
                                        '₹'+result.winning!.toString(),
                                        style: TextStyle(fontFamily:'roboto',fontSize: 16,fontWeight: FontWeight.w500,color: Colors.black,),
                                        textAlign: TextAlign.center,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          onTap: (){
                          },
                        ),
                      ],
                    ),),flex: 1,)

                  ],
                ),
              ),
              new GestureDetector(
                behavior: HitTestBehavior.translucent,
                child: new Container(
                  margin: EdgeInsets.only(left: 10,right: 10,top: 20),
                  alignment: Alignment.center,
                  child: new Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    color: primaryColor,
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        new Container(
                          margin: EdgeInsets.only(left: 20),
                          height: 60,
                          alignment: Alignment.center,
                          child: new Text(
                            'Affiliate Balance:',
                            style: TextStyle(fontFamily:'roboto',fontSize: 14,fontWeight: FontWeight.w800,color: Colors.white,),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        new Container(
                          height: 60,
                          margin: EdgeInsets.only(right: 20),
                          alignment: Alignment.center,
                          child: new Text(
                            '₹'+result.aff_bal!.toString(),
                            style: TextStyle(fontFamily:'roboto',fontSize: 14,fontWeight: FontWeight.w800,color: Colors.white,),
                            textAlign: TextAlign.center,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                onTap: (){
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
  void getAffiliationTotal() async {

    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,start_date: apDateFormat.format(dateFormat.parse(start_date)), end_date:apDateFormat.format(dateFormat.parse(end_date)));
    final client = ApiClient(AppRepository.dio);
    PromoterTotalResponse loginResponse = await client.getAffiliationTotal(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      result=loginResponse.result!;
      setState(() {

      });
    }
  }
}