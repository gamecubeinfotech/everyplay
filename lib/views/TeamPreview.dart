

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:EverPlay/adapter/CandVcPlayerItemAdapter.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/ScrollingText.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/team_preview_point_request.dart';
import 'package:EverPlay/repository/model/team_preview_point_response.dart';
import 'package:EverPlay/repository/model/team_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:EverPlay/views/CreateTeamPager.dart';

class TeamPreview extends StatefulWidget {
  GeneralModel model;
  int? index;
  TeamPreview(this.model,{this.index});
  @override
  _TeamPreviewState createState() => new _TeamPreviewState();
}

class _TeamPreviewState extends State<TeamPreview> {
  List<Widget> wkList=<Widget>[];
  List<Widget> batList=<Widget>[];
  List<Widget> arList=<Widget>[];
  List<Widget> bowlList=<Widget>[];
  List<Widget> cList=<Widget>[];
  double credits = 0.0;
  double points = 0.0;
  int team1Counts=0;
  int team2Counts=0;
  String userId='0';
  String teamName='';
  String teamNumber='0';
  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        if(widget.model.isForLeaderBoard??false){
          getPreviewPlayers();
        }else{
          createPlayers();
        }
      })
    });

  }

  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Colors.black,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
          body: new WillPopScope(
              child:new Container(
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(widget.model.sportKey==AppConstants.TAG_CRICKET?AppImages.previewBgIcon:widget.model.sportKey==AppConstants.TAG_FOOTBALL?AppImages.footballPreviewBgIcon:widget.model.sportKey==AppConstants.TAG_BASEBALL?AppImages.baseballPreviewBgIcon:widget.model.sportKey==AppConstants.TAG_HOCKEY?AppImages.hockeyPreviewBgIcon:widget.model.sportKey==AppConstants.TAG_HANDBALL?AppImages.handballPreviewBgIcon:widget.model.sportKey==AppConstants.TAG_BASKETBALL?AppImages.basketballPreviewBgIcon:AppImages.kabbaddiPreviewBgIcon),
                        fit: BoxFit.fill)),
                child: new Stack(
                  children: [
                    new Column(
                      children: [
                        new Container(
                          padding: EdgeInsets.only(bottom: 5),
                          color: Themecolor,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                              widget.model.isFromLive==true?GestureDetector(
                              onTap:(){
              Navigator.pop(context);
              },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Icon(Icons.clear,color: Colors.white,),
                  )): Container(
                                color: Colors.black,
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      GestureDetector(
                                          onTap:(){
                                            Navigator.pop(context);
                                          },
                                          child: Icon(Icons.clear,color: Colors.white,)),
                                      // SizedBox(width: 10,),
                                      Container(
                                        padding: EdgeInsets.only(left: 10),
                                        child: Text(teamName.toString(),style: TextStyle(
                                            color: Colors.white,fontSize: 16
                                        ),
                                        ),
                                      ),
                                      // new GestureDetector(
                                      //    behavior: HitTestBehavior.translucent,
                                      //    child: new Container(
                                      //      // width: 80,
                                      //      decoration: BoxDecoration(
                                      //          color: Colors.transparent,
                                      //          border: Border.all(color: Colors.white),
                                      //          borderRadius: BorderRadius.circular(3)
                                      //      ),
                                      //
                                      //      child:Padding(
                                      //        padding: const EdgeInsets.only(right: 10,left: 10,top: 5,bottom: 5),
                                      //        child: Row(
                                      //          children: [
                                      //            Container(
                                      //              height: 10,
                                      //              width: 10,
                                      //              child: Image(
                                      //                image: AssetImage(
                                      //                  AppImages.teamEditIcon,),
                                      //                color: Colors.white,),
                                      //            ),
                                      //            SizedBox(width: 5,),
                                      //            Text('Edit Team',style: TextStyle(
                                      //                fontSize: 11,color: Colors.white
                                      //            ),)
                                      //          ],
                                      //        ),
                                      //      ),
                                      //    ),
                                      //    onTap: (){
                                      //      widget.model.isFromEditOrClone = true;
                                      //      navigateToCreateTeam(context, widget.model);
                                      //    },
                                      //  ),
                                    ],
                                  ),
                                ),
                              ),
                              // SizedBox(height: 10,),
                              Padding(
                                padding: const EdgeInsets.only(right: 8,left: 8,top: 5,bottom: 5),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    new Column(

                                      children: [

                                        new Container(
                                          margin: EdgeInsets.only(bottom: 2),
                                          // alignment: Alignment.topLeft,
                                          child: Text(
                                            "${team1Counts+team2Counts}"+"/"+ '${(widget.model.fantasyType==0||widget.model.fantasyType==5) ? "8" :'8'}',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 18),
                                          ),
                                        ),
                                        new Container(
                                          alignment: Alignment.topLeft,
                                          child: Text('Players',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12),
                                          ),
                                        ),
                                      ],crossAxisAlignment: CrossAxisAlignment.start,
                                    ),
                                    SizedBox(width: 2,),
                                    Row(
                                      children: [
                                        new Container(
                                          height: 24,
                                          width: 24,
                                          margin: EdgeInsets.only(bottom: 2),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              border: Border.all(color: Colors.white),
                                              borderRadius: BorderRadius.circular(12),
                                              color: Colors.white
                                          ),
                                          child: new Text(team2Counts.toString(),
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12),textAlign: TextAlign.center,),
                                        ),
                                        SizedBox(width: 10,),
                                        new Container(
                                          // width: 50,
                                          alignment: Alignment.center,
                                          child: Text(
                                            widget.model.teamVs!.split(' VS ')[1]+' Vs '+widget.model.teamVs!.split(' VS ')[0],
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12),
                                          ),
                                        ),
                                        SizedBox(width: 10,),
                                        new Container(
                                          height: 24,
                                          width: 24,
                                          margin: EdgeInsets.only(bottom: 2),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                              border: Border.all(color: yellowdark),
                                              borderRadius: BorderRadius.circular(12),
                                              color: yellowdark
                                          ),
                                          child: new Text(team1Counts.toString(),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12),textAlign: TextAlign.center,),
                                        ),
                                      ],
                                    ),
                                    new Column(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(bottom: 2),
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            widget.model.isFromLiveFinish??false?NumberFormat('##.##').parse(points.toString()).toString():NumberFormat('##.##').parse(credits.toString()).toString(),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 18),
                                          ),
                                        ),
                                        new Container(
                                          alignment: Alignment.topLeft,
                                          child: Text(
                                            widget.model.isFromLiveFinish??false?'Total Points':'Credits Used',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 12),
                                          ),
                                        ),


                                      ],
                                    ),

                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Image.asset(AppImages.logoImageURL,scale: 3,),
                        ),
                         SizedBox(height: 30,),
                        Expanded(
                          child: new Container(
                            // height: MediaQuery.of(context).size.height,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.transparent,
                            child: new Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,

                              children: [
                                // widget.model.isFromLive==true?Container():new Container(
                                //     color: Colors.transparent,
                                //     margin: EdgeInsets.only(top: 35),
                                //     child: new Row(
                                //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                //       children: [
                                //         new Flexible(child: widget.model.isForLeaderBoard??false?new Container(
                                //           alignment: Alignment.center,
                                //           padding: EdgeInsets.only(left: 5),
                                //           child: Text(
                                //             widget.model.fantasyType==AppConstants.BATTING_FANTASY_TYPE?teamName+' Batting ' +teamNumber:widget.model.fantasyType==AppConstants.BOWLING_FANTASY_TYPE?teamName+' Bowling ' +teamNumber:teamName+' ' +teamNumber,
                                //             maxLines: 2,
                                //             style: TextStyle(
                                //                 color: Colors.white,
                                //                 fontWeight: FontWeight.w500,
                                //                 fontSize: 14),
                                //           ),
                                //         ):new Container(),flex: 1,),
                                //         new Flexible(child: new Container(
                                //           alignment: Alignment.center,
                                //           child: new Container(
                                //             width: 105,
                                //             margin: EdgeInsets.only(top: 0),
                                //             child:Image(
                                //               image: AssetImage(AppImages.previewLogoIcon),
                                //               fit: BoxFit.fill,
                                //             ),
                                //           ),
                                //         ),flex: 1,),
                                //         new Flexible(child: new Container(
                                //           child: new GestureDetector(
                                //             behavior: HitTestBehavior.translucent,
                                //             child: new Container(
                                //               alignment: Alignment.centerRight,
                                //               child: new Container(
                                //                 child: Icon(
                                //                   Icons.clear,
                                //                   color: Colors.white,
                                //                   size: 35,
                                //                 ),
                                //               ),
                                //             ),
                                //             onTap: () => {
                                //               Navigator.pop(context)
                                //             },
                                //           ),
                                //         ),flex: 1,),
                                //       ],
                                //     ),
                                //   ),
                                //SizedBox(height:wkList.length>1?0:0,),
                                wkList.length>0?new Container(
                                  child: new Column(
                                    children: [
                                      new Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(bottom: 4,),
                                        child: Text(
                                          widget.model.sportKey==AppConstants.TAG_CRICKET?'Wicket Keeper':widget.model.sportKey==AppConstants.TAG_BASEBALL? 'Outfielders':
                                          widget.model.sportKey==AppConstants.TAG_HOCKEY?'Goal Keeper':
                                          widget.model.sportKey==AppConstants.TAG_HANDBALL?'Goal Keeper':
                                          widget.model.sportKey==AppConstants.TAG_KABADDI?'Defender':
                                          widget.model.sportKey==AppConstants.TAG_BASKETBALL?'Point Guard':
                                          'Goal Keeper',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ),
                                      new Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: new Wrap(
                                          alignment: WrapAlignment.spaceAround,
                                          direction: Axis.horizontal,
                                          children: wkList,
                                        ),
                                      ),
                                    ],
                                  ),
                                ):Container(),
                                //  SizedBox(height: 5,),
                                batList.length>0?new Container(
                                  child: new Column(
                                    children: [
                                      new Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(bottom: 0,top: wkList.length>1?5:0),
                                        child: Text(
                                          widget.model.sportKey==AppConstants.TAG_CRICKET?'Batter':widget.model.sportKey==AppConstants.TAG_BASEBALL? 'Infielders':
                                          widget.model.sportKey==AppConstants.TAG_HOCKEY?'Defender':
                                          widget.model.sportKey==AppConstants.TAG_HANDBALL?'Defender':
                                          widget.model.sportKey==AppConstants.TAG_KABADDI?'All Rounder':
                                          widget.model.sportKey==AppConstants.TAG_BASKETBALL?'Shooting-Guard':
                                          'Defender',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ),
                                      batList.length>3?SizedBox(height:20,): SizedBox(height: wkList.length==2?20:wkList.length>3?25:25,),
                                      new Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: new Wrap(
                                          alignment: WrapAlignment.spaceAround,
                                          direction: Axis.horizontal,
                                          children: batList,
                                        ),
                                      ),
                                    ],
                                  ),
                                ):Container(),

                                arList.length>0?new Container(
                                  child: new Column(
                                    children: [
                                      new Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(bottom:4),
                                        child: Text(
                                          widget.model.sportKey==AppConstants.TAG_CRICKET?'All Rounders':widget.model.sportKey==AppConstants.TAG_BASEBALL? 'Pitcher':
                                          widget.model.sportKey==AppConstants.TAG_HOCKEY?'Midfielder':
                                          widget.model.sportKey==AppConstants.TAG_HANDBALL?'State Forward':
                                          widget.model.sportKey==AppConstants.TAG_KABADDI?'Raider':
                                          widget.model.sportKey==AppConstants.TAG_BASKETBALL?'Small Forward':
                                          'Midfielder',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ),

                                      new Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: new Wrap(
                                          alignment: WrapAlignment.spaceAround,
                                          direction: Axis.horizontal,
                                          children: arList,
                                        ),
                                      ),
                                    ],
                                  ),
                                ):Container(),

                                bowlList.length>0?new Container(
                                  child: new Column(
                                    children: [
                                      new Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(top: 7,bottom: 7),
                                        child: Text(
                                          widget.model.sportKey==AppConstants.TAG_CRICKET?'Bowlers':widget.model.sportKey==AppConstants.TAG_BASEBALL? 'Catcher':
                                          widget.model.sportKey==AppConstants.TAG_HOCKEY?'Striker':
                                          widget.model.sportKey==AppConstants.TAG_BASKETBALL?'Power Forward':
                                          'State Forward',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ),
                                      new Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: new Wrap(
                                          alignment: WrapAlignment.spaceAround,
                                          direction: Axis.horizontal,
                                          children: bowlList,
                                        ),
                                      ),
                                    ],
                                  ),
                                ):Container(),

                                cList.length>0?new Container(
                                  child: new Column(
                                    children: [
                                      new Container(
                                        alignment: Alignment.center,
                                        margin: EdgeInsets.only(top: 7,bottom: 7),
                                        child: Text(
                                          'Center',
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ),
                                      new Container(
                                        width: MediaQuery.of(context).size.width,
                                        child: new Wrap(
                                          alignment: WrapAlignment.spaceAround,
                                          direction: Axis.horizontal,
                                          children: cList,
                                        ),
                                      ),
                                    ],
                                  ),
                                ):Container(),
                                SizedBox(height: 10,),
                              ],
                            ),
                          ),
                        ),
                        // new Container(
                        //   padding: EdgeInsets.only(bottom: 5,top: 5),
                        //   color: transBlack,
                        //   width: MediaQuery.of(context).size.width,
                        //   child: new Container(
                        //     alignment: Alignment.center,
                        //     child: Text(
                        //       widget.model.fantasyType==AppConstants.BATTING_FANTASY_TYPE?'Batting':widget.model.fantasyType==AppConstants.BOWLING_FANTASY_TYPE?'Bowling':widget.model.fantasyType==AppConstants.SECOND_INNINGS_FANTASY_TYPE?'2nd Innings':widget.model.fantasyType==AppConstants.LIVE_FANTASY_TYPE?'Live':widget.model.fantasyType==AppConstants.REVERSE_FANTASY?'Reverse':'Full Match',
                        //       style: TextStyle(
                        //           color: Colors.white,
                        //           fontWeight: FontWeight.w500,
                        //           fontSize: 12),
                        //     ),
                        //   ),
                        // ),
                        // new Container(
                        //   padding: EdgeInsets.only(bottom: 12,top: 2),
                        //   color: Colors.black.withOpacity(0.7),
                        //   child: new Row(
                        //     mainAxisAlignment: MainAxisAlignment.spaceAround,
                        //     children: [
                        //       new Column(
                        //         children: [
                        //           new Container(
                        //             height: 24,
                        //             width: 24,
                        //             margin: EdgeInsets.only(bottom: 2),
                        //             alignment: Alignment.center,
                        //             decoration: BoxDecoration(
                        //                 border: Border.all(color: Colors.black),
                        //                 borderRadius: BorderRadius.circular(12),
                        //                 color: Colors.black
                        //             ),
                        //             child: new Text(team1Counts.toString(),
                        //               style: TextStyle(
                        //                   color: Colors.white,
                        //                   fontWeight: FontWeight.w500,
                        //                   fontSize: 12),textAlign: TextAlign.center,),
                        //           ),
                        //           new Container(
                        //             width: 50,
                        //             alignment: Alignment.center,
                        //             child: Text(
                        //               widget.model.teamVs!.split(' VS ')[0],
                        //               style: TextStyle(
                        //                   color: Colors.white,
                        //                   fontWeight: FontWeight.w500,
                        //                   fontSize: 12),
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //       new Column(
                        //         children: [
                        //           new Container(
                        //             margin: EdgeInsets.only(bottom: 2),
                        //             alignment: Alignment.topLeft,
                        //             child: Text(
                        //               widget.model.isFromLiveFinish??false?NumberFormat('##.##').parse(points.toString()).toString():NumberFormat('##.##').parse(credits.toString()).toString(),
                        //               style: TextStyle(
                        //                   color: Colors.white,
                        //                   fontWeight: FontWeight.w500,
                        //                   fontSize: 18),
                        //             ),
                        //           ),
                        //           new Container(
                        //             alignment: Alignment.topLeft,
                        //             child: Text(
                        //               widget.model.isFromLiveFinish??false?'Total Points':'Total Credits',
                        //               style: TextStyle(
                        //                   color: Colors.white,
                        //                   fontWeight: FontWeight.w500,
                        //                   fontSize: 12),
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //       new Column(
                        //         children: [
                        //           new Container(
                        //             height: 24,
                        //             width: 24,
                        //             margin: EdgeInsets.only(bottom: 2),
                        //             alignment: Alignment.center,
                        //             decoration: BoxDecoration(
                        //                 border: Border.all(color: Colors.white),
                        //                 borderRadius: BorderRadius.circular(12),
                        //                 color: Colors.white
                        //             ),
                        //             child: new Text(team2Counts.toString(),
                        //               style: TextStyle(
                        //                   color: Colors.black,
                        //                   fontWeight: FontWeight.w500,
                        //                   fontSize: 12),textAlign: TextAlign.center,),
                        //           ),
                        //           new Container(
                        //             width: 50,
                        //             alignment: Alignment.center,
                        //             child: Text(
                        //               widget.model.teamVs!.split(' VS ')[1],
                        //               style: TextStyle(
                        //                   color: Colors.white,
                        //                   fontWeight: FontWeight.w500,
                        //                   fontSize: 12),
                        //             ),
                        //           ),
                        //         ],
                        //       ),
                        //     ],
                        //   ),
                        // ),
                      ],
                    ),
                    // Positioned(
                    //     left:40,top:wkList.length==2?270:wkList.length>1?wkList.length==4? widget.model.isFromLive==true?220:270:270:250,
                    //     child: Image.asset(AppImages.superBgLogo,scale: 4,color: Colors.white.withAlpha(100))),
                    // Positioned(
                    //
                    //     right:40,top:wkList.length==2?270:wkList.length>1?wkList.length==4?widget.model.isFromLive==true?220:270:270:250,
                    //   child: Image.asset(AppImages.superBgLogo,scale: 4,color: Colors.white.withAlpha(100)),),
                  ],
                ),
              ),
              onWillPop: _onWillPop)),
    );
  }

  Widget getPlayer(Player player,int count){
    return new Container(
      width: 67,
      margin: EdgeInsets.only(left: count>4?20:10,right: count>4?20:10,top: 8),
      child: Stack(
        alignment: Alignment.topRight,
        children: [
          new Column(
            children: [
              new Container(
                child: new Stack(
                  children: [
                    new Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        new Container(
                          height: 40,
                          width: 45,
                          child: CachedNetworkImage(
                            imageUrl: player.image!,
                            placeholder: (context, url) => new Image.asset(AppImages.defaultAvatarIcon),
                            errorWidget: (context, url, error) => new Image.asset(AppImages.defaultAvatarIcon),
                            fit: BoxFit.fill,
                          ),
                        ),
                        new Container(
                          alignment: Alignment.bottomCenter,
                          height: 58,
                          child: new Card(
                            elevation: 0,
                            color: player.team=='team1'?Colors.black:Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(3.0),
                            ),
                            child: new ConstrainedBox(
                              constraints: new BoxConstraints(
                                minWidth: 65,
                                minHeight: 17,
                                maxHeight: 17,
                              ),
                              child: new Container(
                                height: 17,
                                padding: EdgeInsets.only(top: 3),
                                alignment: Alignment.center,
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    new Flexible(child: new Text(
                                      player.getShortName(),
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(fontFamily: 'noway',fontSize: 10,fontWeight: FontWeight.w500,color: player.team=='team1'?Colors.white:Colors.black,),
                                      textAlign: TextAlign.center,
                                    ))
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    player.isCaptain||player.captain==1||player.isVcCaptain||player.vicecaptain==1?new Container(
                      height: 20,
                      width: 20,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.white),
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.black
                      ),
                      child: new Text(player.isCaptain||player.captain==1?'C':player.isVcCaptain||player.vicecaptain==1?'VC':'',
                        style: TextStyle(
                            fontFamily: 'noway',
                            color: Colors.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 10),textAlign: TextAlign.center,),
                    ):Container(),
                  ],
                ),
              ),

              new Container(
                alignment: Alignment.center,
                child: Text(
                  widget.model.isFromLiveFinish??false?NumberFormat('###.#').parse(player.points==null?"0":player.points.toString()).toString()+' Pt.':NumberFormat('###.#').parse(player.credit.toString()).toString()+' Cr.',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w400,
                      fontSize: 10),
                ),
              ),
            ],
          ),
          player.is_playing_show == 1 && player.is_playing == 0
              ? new Container(
            height: 10,
            width: 10,
            alignment: Alignment.center,
            decoration: BoxDecoration(
                border: Border.all(color: Colors.white, width: 1),
                borderRadius: BorderRadius.circular(5),
                color: Colors.red),
          )
              : Container(),
        ],
      ),
    );
  }

  Future<bool> _onWillPop() async {
    Navigator.pop(context);
    return Future.value(false);
  }
  void createPlayers(){
    getCreditAndPoints();
    for(Player player in widget.model.selectedWkList!){
      player.isCaptain=player.isCaptain??false;
      player.isVcCaptain=player.isVcCaptain??false;
      wkList.add(getPlayer(player,widget.model.selectedWkList!.length));
    }
    for(Player player in widget.model.selectedBatLiSt!){
      player.isCaptain=player.isCaptain??false;
      player.isVcCaptain=player.isVcCaptain??false;
      batList.add(getPlayer(player,widget.model.selectedBatLiSt!.length));
    }
    for(Player player in widget.model.selectedArList!){
      player.isCaptain=player.isCaptain??false;
      player.isVcCaptain=player.isVcCaptain??false;
      arList.add(getPlayer(player,widget.model.selectedArList!.length));
    }
    if(widget.model.selectedBowlList!=null) {
      for (Player player in widget.model.selectedBowlList!) {
        player.isCaptain = player.isCaptain ?? false;
        player.isVcCaptain = player.isVcCaptain ?? false;
        bowlList.add(getPlayer(player, widget.model.selectedBowlList!.length));
      }
    }
    if(widget.model.selectedcList!=null){
      for(Player player in widget.model.selectedcList!){
        player.isCaptain=player.isCaptain??false;
        player.isVcCaptain=player.isVcCaptain??false;
        cList.add(getPlayer(player,widget.model.selectedcList!.length));
      }
    }
  }
  void getCreditAndPoints(){
    for (int i = 0; i < widget.model.selectedWkList!.length; i++) {
      credits = credits + double.parse(widget.model.selectedWkList![i].credit);
      points = points + double.parse((widget.model.selectedWkList![i].points??0).toString());
      if ("team1"!=widget.model.selectedWkList![i].team) {
        team2Counts++;
      } else {
        team1Counts++;
      }
    }
    for (int i = 0; i < widget.model.selectedBatLiSt!.length; i++) {
      credits = credits + double.parse(widget.model.selectedBatLiSt![i].credit);
      points = points + double.parse((widget.model.selectedBatLiSt![i].points??0).toString());
      if ("team1"!=widget.model.selectedBatLiSt![i].team) {
        team2Counts++;
      } else {
        team1Counts++;
      }
    }
    for (int i = 0; i < widget.model.selectedArList!.length; i++) {
      credits = credits + double.parse(widget.model.selectedArList![i].credit);
      points = points + double.parse((widget.model.selectedArList![i].points??0).toString());
      if ("team1"!=widget.model.selectedArList![i].team) {
        team2Counts++;
      } else {
        team1Counts++;
      }
    }
    if(widget.model.selectedBowlList!=null) {
      for (int i = 0; i < widget.model.selectedBowlList!.length; i++) {
        credits =
            credits + double.parse(widget.model.selectedBowlList![i].credit);
        points = points + double.parse(
            (widget.model.selectedBowlList![i].points ?? 0).toString());
        if ("team1" != widget.model.selectedBowlList![i].team) {
          team2Counts++;
        } else {
          team1Counts++;
        }
      }
    }
    if(widget.model.selectedcList!=null){
      for (int i = 0; i < widget.model.selectedcList!.length; i++) {
        credits = credits + double.parse(widget.model.selectedcList![i].credit);
        points = points + double.parse((widget.model.selectedcList![i].points??0).toString());
        if ("team1"!=widget.model.selectedcList![i].team) {
          team2Counts++;
        } else {
          team1Counts++;
        }
      }
    }
  }
  void getPreviewPlayers() async {
    AppLoaderProgress.showLoader(context);
    TeamPreviewPointRequest request = new TeamPreviewPointRequest(user_id: userId,teamid:widget.model.teamId.toString(),challenge: widget.model.challengeId.toString(),sport_key: widget.model.sportKey,fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.slotId.toString());
    final client = ApiClient(AppRepository.dio);
    TeamPointPreviewResponse response = await client.getPreviewPlayers(request);
    print("PreviewPlayers ${response.result}");
    if(response.status==1){
      teamName=response.result!.teamname!;
      teamNumber=response.result!.teamnumber.toString();
      if(widget.model.sportKey==AppConstants.TAG_CRICKET){
        widget.model.selectedWkList=response.result!.keeper!;
        widget.model.selectedBatLiSt=response.result!.batsman!;
        widget.model.selectedArList=response.result!.allrounder!;
        widget.model.selectedBowlList=response.result!.bowler!;
      }else if(widget.model.sportKey==AppConstants.TAG_FOOTBALL){
        widget.model.selectedWkList=response.result!.Goalkeeper!;
        widget.model.selectedBatLiSt=response.result!.Defender!;
        widget.model.selectedArList=response.result!.Midfielder!;
        widget.model.selectedBowlList=response.result!.Forward!;
      }else if(widget.model.sportKey==AppConstants.TAG_BASKETBALL){
        widget.model.selectedWkList=response.result!.pgList!;
        widget.model.selectedBatLiSt=response.result!.sgList!;
        widget.model.selectedArList=response.result!.smallForwardList!;
        widget.model.selectedBowlList=response.result!.powerForwardList!;
        widget.model.selectedcList=response.result!.centreList!;
      }else if(widget.model.sportKey==AppConstants.TAG_BASEBALL){
        widget.model.selectedWkList=response.result!.Outfielder!;
        widget.model.selectedBatLiSt=response.result!.Infielder!;
        widget.model.selectedArList=response.result!.Pitcher!;
        widget.model.selectedBowlList=response.result!.Catcher!;
      }else if(widget.model.sportKey==AppConstants.TAG_HANDBALL){
        widget.model.selectedWkList=response.result!.Goalkeeper!;
        widget.model.selectedBatLiSt=response.result!.Defender!;
        widget.model.selectedArList=response.result!.Forward!;
      }else if(widget.model.sportKey==AppConstants.TAG_HOCKEY){
        widget.model.selectedWkList=response.result!.Goalkeeper!;
        widget.model.selectedBatLiSt=response.result!.Defender!;
        widget.model.selectedArList=response.result!.Midfielder!;
        widget.model.selectedBowlList=response.result!.Forward!;
      }else{
        widget.model.selectedWkList=response.result!.Goalkeeper!;
        widget.model.selectedBatLiSt=response.result!.Defender!;
        widget.model.selectedArList=response.result!.Forward!;
      }
      createPlayers();
      setState(() {
        AppLoaderProgress.hideLoader(context);
      });
    }
  }
}
