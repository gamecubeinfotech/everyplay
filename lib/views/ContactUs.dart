import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
// import 'package:zendesk/zendesk.dart';

class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  String _platformVersion = 'Unknown';
  // final Zendesk zendesk = Zendesk();
  var val;
  var groupValue;
  var _dropDownValue;
  String userName = '';
  String mobile = '';
  String email = '';
  String _accountKey = '7UXmll9t3CRqMSsp2u5G7ZPSuF8NEHLo';
  String _applicationId = '098384531b1a603b9e9b339b92dca9986c856ff9580c73a1';

  // String _clientId = 'mobile_sdk_client_7a7410bd52f60ef4427d';
  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_NAME)
        .then((value) => {
              userName = value,
              AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_MOBILE)
                  .then((value) => {
                        mobile = value,
                        AppPrefrence.getString(
                                AppConstants.SHARED_PREFERENCE_USER_EMAIL)
                            .then((value) => {
                                  setState(() {
                                    email = value;
                                    // initZendesk();
                                  })
                                })
                      })
            });
  }

  // Zendesk is asynchronous, so we initialize in an async method.
/*
  Future<void> initZendesk() async {
    zendesk.init(_accountKey, appId: _applicationId).then((r) {
      print('init finished');
    }).catchError((e) {
      print('failed with error $e');
    });
    zendesk
        .setVisitorInfo(
      name: userName,
      phoneNumber: mobile,
      email: email,
    )
        .then((r) {
      print('setVisitorInfo finished');
    }).catchError((e) {
      print('error $e');
    });
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    // But we aren't calling setState, so the above point is rather moot now.
  }
*/

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: primaryColor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.light) /* set Status bar icon color in iOS. */
        );
    return SafeArea(
      child: Scaffold(
        backgroundColor:Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            // padding: EdgeInsets.only(top: 40),
            color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Contact Us',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new SingleChildScrollView(
          child: new Stack(
            children: [
              new Container(
                child: new Column(
                  children: [
                    new Container(
                      //margin:EdgeInsets.only(bottom: 30),
                      decoration: BoxDecoration(

                          borderRadius: BorderRadius.only(
                              bottomRight: Radius.circular(40),
                              bottomLeft: Radius.circular(40)),
                          color: Colors.white
                      ),
                      child: Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: const EdgeInsets.all(30),
                          child: new Image.asset(
                            AppImages.contactUsBgIcon,
                            height: 200,
                          ),
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.only(top: 32,bottom: 10),
                      child: new Text('Feel free to contact us at:',
                          style: TextStyle(
                              fontFamily: "Roboto",
                              color: Colors.black,
                              fontWeight: FontWeight.bold,
                              fontSize: 24)),
                    ),


                    /*                  GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: new Container(
                                          margin: EdgeInsets.only(top: 10, left: 10, right: 10),
                                          child: new Card(
                                            shape: RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(10.0),
                                            ),
                                            child: new Container(
                                              margin: EdgeInsets.all(20),
                                              child: new Row(
                                                children: [
                                                  new Container(
                                                    margin: EdgeInsets.only(right: 20),
                                                    height: 60,
                                                    width: 60,
                                                    child: new Image.asset(
                                                        AppImages.contactChatIcon,
                                                        fit: BoxFit.fill),
                                                  ),
                                                  new Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      new Text(
                                                        'Chat with Us:',
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 16,
                                                            fontWeight: FontWeight.w500),
                                                      ),
                                                      new Container(
                                                        margin: EdgeInsets.only(top: 5),
                                                        child: new Text(
                                                          'We are live and ready to help!',
                                                          style: TextStyle(
                                                              color: Colors.grey,
                                                              fontSize: 16,
                                                              fontWeight: FontWeight.w400),
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                        onTap: () async {
                                          zendesk.setDepartment('Support').then((r) {
                                            print('startChat finished');
                                          }).catchError((e) {
                                            print('error $e');
                                          });
                                          zendesk
                                              .startChat(
                                            isPreChatFormEnabled: false,
                                            isAgentAvailabilityEnabled: true,
                                            isOfflineFormEnabled: false,
                                            isChatTranscriptPromptEnabled: true,
                                          )
                                              .then((r) {
                                            print('startChat finished');
                                          }).catchError((e) {
                                            print('error $e');
                                          });
                                        },
                                      ),*/


                    GestureDetector(
                      onTap: (){
                        _launchURL('mailto:support@EverPlayFantasy.com');
                      },
                      child: new Container(
                        margin: EdgeInsets.all(10),
                        child: new Container(
                          decoration: BoxDecoration(border: Border.all(color: Color(0xffD9D9D9),width: 1),borderRadius: BorderRadius.all(Radius.circular(10))),
                          child: new Container(
                            child: new Row(
                              children: [
                                new Container(
                                  decoration: BoxDecoration(
                                    //color: Colors.red,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(40),
                                          bottomLeft: Radius.circular(40))),
                                  margin: EdgeInsets.only(right: 20),
                                  height: 61,
                                  width: 60,
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: new Image.asset(
                                        AppImages.mailIcon,
                                        fit: BoxFit.fill),
                                  ),
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.only(top: 20.0, bottom: 20),
                                  child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      new Text(
                                        'Email Us:',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontSize: 16,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(top: 5),
                                        child: new Text(
                                          'support@everplayfantasy.com',
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    new Container(
                      // margin: EdgeInsets.all(10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                           GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              margin: EdgeInsets.only(right: 10),
                              height: 40,
                              width: 40,
                              child: new Image.asset(AppImages.instagram,
                                  fit: BoxFit.fill),
                            ),
                            onTap: () {
                              _launchURL(AppConstants.instagram_url);
                            },
                          ),
                           GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              margin: EdgeInsets.only(right: 10),
                              height: 40,
                              width: 40,
                              child: new Image.asset(AppImages.linkDin,
                                  fit: BoxFit.fill),
                            ),
                            onTap: () {
                              _launchURL(AppConstants.linkdin_url);
                            },
                          ),
                           GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              margin: EdgeInsets.only(right: 10),
                              height: 40,
                              width: 40,
                              child: new Image.asset(AppImages.whatApp,
                                  fit: BoxFit.fill),
                            ),
                            onTap: () {
                              // _launchWhatsapp();
                              _launchURL(AppConstants.whatsapp_url);
                            },
                          ),
                           GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              margin: EdgeInsets.only(right: 10),
                              height: 40,
                              width: 40,
                              child: new Image.asset(AppImages.fbImageURL,
                                  fit: BoxFit.fill),
                            ),
                            onTap: () {
                              _launchURL(AppConstants.fb_url);
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }


}
