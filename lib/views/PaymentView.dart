import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../appUtilities/app_constants.dart';
import '../appUtilities/app_images.dart';

class PaymentView extends StatefulWidget {
  String title;
  String url;

  PaymentView(this.title,this.url);

  @override
  _WebViewState createState() => _WebViewState();
}

class _WebViewState extends State<PaymentView> with WidgetsBindingObserver {

  final Completer<WebViewController> _controller =
  Completer<WebViewController>();
  FocusNode? focusNode;
  var _stackToView;
  bool statusFound=false;

  void _handleLoad(String url) {
    if(url.contains('?status=')&&!statusFound){
      statusFound=true;
      if(url.contains('PAYMENT_SUCCESS')){
        // Fluttertoast.showToast(msg: 'Payment Success.');
        Navigator.pop(context,'SUCCESS');
      }else{
        Fluttertoast.showToast(msg: 'Payment Failed.');
        Navigator.pop(context,'FAILED');
      }
    }
    setState(() {
      _stackToView = 0;
    });
  }

  @override
  void initState() {
    super.initState();
    _stackToView=1;
  }


  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top:true,
      bottom: false,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            // padding: EdgeInsets.only(top: 40),
            color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Text(widget.title,
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),

        body: IndexedStack(
          index: _stackToView,
          children: [
            Column(
              children: <Widget>[
                Expanded(
                    child: WebView(
                      initialUrl: widget.url,
                      javascriptMode:JavascriptMode.unrestricted,
                      onPageFinished: _handleLoad,
                      onWebViewCreated: (WebViewController webViewController) {
                        _controller.complete(webViewController);
                      },
                    )),
              ],
            ),
            Container(
                child: Center(child: CircularProgressIndicator(),)
            ),
          ],
        ),
      ),
    );
  }
}