import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/leaderboard_series_response.dart';
import 'package:EverPlay/repository/model/series_leaderboard_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class HomeLeaderboard extends StatefulWidget {
  int seriesId=0;
  HomeLeaderboard({seriesId});
  @override
  _HomeLeaderboardState createState() => new _HomeLeaderboardState();
}

class _HomeLeaderboardState extends State<HomeLeaderboard> {
  ScrollController scrollController=new ScrollController();
  TextEditingController codeController = TextEditingController();
  bool loading = false;
  var _dropDownValue;
  int seriesId=0;
  int total_page=1;
  int currentPage=0;
  String userId='0';
  List<SeriesItem> seriesList=[];
  List<WinningBreakupDataModel> breakupList=[];
  List<SeriesLeaderboardDataModel> leaderboardList=[];
  List<SeriesLeaderboardDataModel> leaderboardRankList=[];

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getSeries();
      })
    });
    scrollController..addListener(() {
      if (scrollController.position.pixels == scrollController.position.maxScrollExtent && total_page>currentPage) {
          currentPage=currentPage+1;
          getSeriesLeaderboard(false);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: primaryColor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.dark) /* set Status bar icon color in iOS. */
        );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: primaryColor,
            // padding: EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Leaderboard',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Container(
          height: MediaQuery.of(context).size.height,
          child: new SingleChildScrollView(
            controller: scrollController,
            child: new Column(
              children: [
                new Container(
                  decoration: new BoxDecoration(
                    gradient: new LinearGradient(
                      colors: [
                        const Color(0xffc61d24),
                        const Color(0xffff6f00),
                      ],
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(0.0, 1.0),
                    ),
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(25),bottomRight: Radius.circular(25))
                  ),

                  child: new Column(
                    children: [
                      new Container(
                        margin: EdgeInsets.all(10),
                        padding: EdgeInsets.all(0),
                        height: 58,
                        child: new DropdownButtonFormField(
                          isDense: true,
                          hint: _dropDownValue == null
                              ? Text('Select Series',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                  ))
                              : Text(
                                  _dropDownValue,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Colors.white,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                          isExpanded: true,
                          iconDisabledColor: Colors.white,
                          iconEnabledColor: Colors.white,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 14,
                          ),
                          dropdownColor: darkPrimaryColor,
                          items: seriesList.map(
                            (SeriesItem val) {
                              return DropdownMenuItem<String>(
                                value: val.id.toString(),
                                child: Text(val.name!),
                              );
                            },
                          ).toList(),
                          onChanged: (val) {
                            setState(
                              () {
                                currentPage=0;
                                breakupList.clear();
                                leaderboardList.clear();
                                seriesId = int.parse(val.toString());
                                getSeriesLeaderboard(false);
                              },
                            );
                          },
                          decoration: InputDecoration(
                            counterText: "",
                            fillColor: darkPrimaryColor,
                            filled: true,
                            hintStyle: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                            ),
                            enabledBorder: OutlineInputBorder(
                              // width: 0.0 produces a thin "hairline" border
                              borderSide:
                                  BorderSide(color: darkPrimaryColor, width: 0.0),
                            ),
                          ),
                        ),
                      ),
                      !loading?new Column(
                        children: [
                          new Container(
                              margin: EdgeInsets.only(left: 5,right: 5),
                              child: new GridView.count(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  crossAxisCount: 2,
                                  childAspectRatio: 8.0 / 1.8,
                                  children: new List<Widget>.generate(breakupList.length, (index) {
                                    return new Container(
                                      child: new Card(
                                          color: Colors.white,
                                          child: new Container(
                                            padding: EdgeInsets.only(left: 5,right: 5),
                                            child: new Row(
                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                              children: [
                                                new Container(
                                                  child: new Text('Rank '+breakupList[index].position.toString(),
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight: FontWeight.w400,
                                                          fontSize: 14)),
                                                ),
                                                new Container(
                                                  child: new Text('₹'+breakupList[index].price.toString(),
                                                      style: TextStyle(
                                                          fontFamily: AppConstants.textBold,
                                                          color: Colors.black,
                                                          fontWeight: FontWeight.w500,
                                                          fontSize: 14)),
                                                ),
                                              ],
                                            ),
                                          )),
                                    );
                                  }))),
                          leaderboardList.length>3?new Container(
                            margin: EdgeInsets.only(bottom: 20),
                            height: 200,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new Container(
                                    alignment: Alignment.bottomCenter,
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        new Stack(
                                          alignment: Alignment.center,
                                          children: [
                                            new Container(
                                              height: 20,
                                              child: new Text('2',
                                                  style: TextStyle(
                                                      color: yellowColor,
                                                      fontWeight: FontWeight.w800,
                                                      fontSize: 20)),
                                            ),
                                            new Container(
                                              height: 20,
                                              width: 35,
                                              alignment: Alignment.topRight,
                                              child: new Text('nd',
                                                  style: TextStyle(
                                                      color: yellowColor,
                                                      fontWeight: FontWeight.w800,
                                                      fontSize: 11)),
                                            ),
                                          ],
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 5),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            border: Border.all(color: primaryColor,width: 4),
                                            borderRadius: BorderRadius.circular(60),
                                          ),
                                          child: ClipOval(
                                            child: CachedNetworkImage(
                                                height: 76,
                                                width: 76,
                                                imageUrl: leaderboardRankList[1].image??'',
                                                placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                                errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                                fit: BoxFit.fill
                                            ),
                                          ),
                                        ),
                                        new Container(
                                          height: 20,
                                          margin: EdgeInsets.only(top: 5),
                                          child: new Text(leaderboardRankList[1].team!,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 14)),
                                        ),
                                        new Container(
                                          child: new Text(leaderboardRankList[1].points!.toString()+' pts',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    navigateToUserStats(context,leaderboardRankList[1].series_id.toString(),leaderboardRankList[1].user_id.toString());
                                  },
                                ),
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new Container(
                                    alignment: Alignment.center,
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        new Stack(
                                          alignment: Alignment.topCenter,
                                          children: [
                                            new Container(
                                              margin: EdgeInsets.only(top: 25),
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                border: Border.all(color: primaryColor,width: 4),
                                                borderRadius: BorderRadius.circular(60),
                                              ),
                                              child: ClipOval(
                                                child: CachedNetworkImage(
                                                    height: 96,
                                                    width: 96,
                                                    imageUrl: leaderboardRankList[0].image??'',
                                                    placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                                    errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                                    fit: BoxFit.fill
                                                ),
                                              ),
                                            ),
                                            new Container(
                                              height: 40,
                                              width: 40,
                                              margin: EdgeInsets.only(top: 5),
                                              alignment: Alignment.center,
                                              child: Image(
                                                image: AssetImage(
                                                  AppImages.firstWinIcon,),),
                                            ),
                                          ],
                                        ),
                                        new Container(
                                          height: 20,
                                          margin: EdgeInsets.only(top: 5),
                                          child: new Text(leaderboardRankList[0].team!,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 14)),
                                        ),
                                        new Container(
                                          child: new Text(leaderboardRankList[0].points!.toString()+' pts',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    navigateToUserStats(context,leaderboardRankList[0].series_id.toString(),leaderboardRankList[0].user_id.toString());
                                  },
                                ),
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new Container(
                                    alignment: Alignment.bottomCenter,
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        new Stack(
                                          alignment: Alignment.center,
                                          children: [
                                            new Container(
                                              height: 20,
                                              child: new Text('3',
                                                  style: TextStyle(
                                                      color: yellowColor,
                                                      fontWeight: FontWeight.w800,
                                                      fontSize: 20)),
                                            ),
                                            new Container(
                                              height: 20,
                                              width: 35,
                                              alignment: Alignment.topRight,
                                              child: new Text('rd',
                                                  style: TextStyle(
                                                      color: yellowColor,
                                                      fontWeight: FontWeight.w800,
                                                      fontSize: 11)),
                                            ),
                                          ],
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 5),
                                          alignment: Alignment.center,
                                          decoration: BoxDecoration(
                                            border: Border.all(color: primaryColor,width: 4),
                                            borderRadius: BorderRadius.circular(60),
                                          ),
                                          child: ClipOval(
                                            child: CachedNetworkImage(
                                                height: 76,
                                                width: 76,
                                                imageUrl: leaderboardRankList[2].image??'',
                                                placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                                errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                                fit: BoxFit.fill
                                            ),
                                          ),
                                        ),
                                        new Container(
                                          height: 20,
                                          margin: EdgeInsets.only(top: 5),
                                          child: new Text(leaderboardRankList[2].team!,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 14)),
                                        ),
                                        new Container(
                                          child: new Text(leaderboardRankList[2].points!.toString()+' pts',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 14)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    navigateToUserStats(context,leaderboardRankList[2].series_id.toString(),leaderboardRankList[2].user_id.toString());
                                  },
                                ),
                              ],
                            ),
                          ):new Container()
                        ],
                      ):new Container()
                    ],
                  ),
                ),
                !loading?new Container(
                  margin: EdgeInsets.only(top: 10),
                  child: new Column(
                    children: List.generate(
                      leaderboardList.length,
                          (index) => new Column(
                        children: [
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Card(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                              child: new Container(
                                decoration: new BoxDecoration(
                                  gradient: new LinearGradient(
                                    colors: [
                                      const Color(0xffc61d24),
                                      const Color(0xffff6f00),
                                    ],
                                    begin: const FractionalOffset(0.0, 0.0),
                                    end: const FractionalOffset(0.0, 1.0),
                                  ),
                                  borderRadius: BorderRadius.circular(30.0),
                                ),

                                padding: EdgeInsets.only(left: 20,right: 10,top: 10,bottom: 10),
                                child: new Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    new Container(
                                      child: new Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          new Container(
                                            width: 70,
                                            child: new Text(
                                              '#'+leaderboardList[index].rank!.toString(),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          new Row(
                                            mainAxisAlignment: MainAxisAlignment.start,
                                            children: [
                                            new Container(
                                              margin: EdgeInsets.only(right: 10),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.circular(20),
                                              ),
                                              child: ClipOval(
                                                child: CachedNetworkImage(

                                                    height: 40,
                                                    width: 40,
                                                    imageUrl: leaderboardList[index].image??'',
                                                    placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                                    errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                                    fit: BoxFit.fill
                                                ),
                                              ),
                                            ),
                                              new Container(
                                                width: 110,
                                                child: new Row(
                                                  children: [
                                                    Expanded(child: new Text(
                                                      leaderboardList[index].team!,
                                                      softWrap: false,
                                                      overflow: TextOverflow.ellipsis,
                                                      maxLines: 1,
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 12,
                                                          fontWeight: FontWeight.w500),
                                                    ))
                                                  ],
                                                ),
                                              ),
                                          ],),
                                        ],
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.only(right: 10),
                                      child: new Text(
                                        leaderboardList[index].points!.toString(),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            onTap: (){
                              navigateToUserStats(context,leaderboardList[index].series_id.toString(),leaderboardList[index].user_id.toString());
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ):new Container()
              ],
            ),
          ),
        ),
      ),
    );
  }
  void getSeries() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest request = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    SeriesResponse response = await client.getSeries(request);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
    if (response.status == 1&&response.result!.length>0) {
        seriesList=response.result!;
        if(widget.seriesId>0){
          for(int i=0; i<seriesList.length; i++){
            if(widget.seriesId==seriesList[i].id){
              _dropDownValue=seriesList[i].name.toString();
              seriesId=seriesList[i].id!;
              break;
            }
          }
        }else{
          _dropDownValue=seriesList[0].name.toString();
          seriesId=seriesList[0].id!;
        }
        getSeriesLeaderboard(true);
    }
  }
  void getSeriesLeaderboard(bool isLoading) async {
    setState(() {
      loading=isLoading;
    });
    AppLoaderProgress.showLoader(context);
    GeneralRequest request = new GeneralRequest(user_id: userId,series_id: seriesId.toString(),page: currentPage.toString());
    final client = ApiClient(AppRepository.dio);
    SeriesLeaderboardResponse response = await client.getSeriesLeaderboard(request);
    if (response.status == 1) {
        total_page=response.total_pages!;
        breakupList=response.result!.wining_breakup!;
        leaderboardList.addAll(response.result!.leaderboard!);
        if(leaderboardList.length>3 && currentPage==0){
          leaderboardRankList.clear();
          for(int i=0; i<3; i++){
            leaderboardRankList.add(leaderboardList[0]);
            leaderboardList.removeAt(0);
          }
        }
    }
    setState(() {
      loading=false;
      AppLoaderProgress.hideLoader(context);
    });
  }
}
