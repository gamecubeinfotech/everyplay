import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:super_tooltip/super_tooltip.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomToolTip.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/repository/model/contest_details_response.dart';

class Leaderboard extends StatefulWidget {

  List<JoinedContestTeam> leaderboardList;
  String userId;
  bool isContestDetails;
  String pdfUrl;
  GeneralModel model;
  Leaderboard(this.leaderboardList,this.userId,this.isContestDetails,this.pdfUrl,this.model);
  @override
  _LeaderboardState createState() => new _LeaderboardState();
}

class _LeaderboardState extends State<Leaderboard> {
  SuperTooltip? tooltip;
  late Offset offset;
  double scale=1.0;
  bool isCompareTeamActive=false;
  GlobalKey btnKey = GlobalKey();
  String team1Id='0';
  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        color: Colors.white,
        child: new Column(
          children: [
            new Divider(height: 1,thickness: 1,),
           widget.model.isFromLiveFinish??false?new Column(
              children: [
                new Container(
                  height: 35,
                  color: Colors.white,
                  padding: EdgeInsets.only(left: 10,right: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // widget.model.isFromLiveFinish??false&&widget.leaderboardList.length>1?new GestureDetector(
                      //   behavior: HitTestBehavior.translucent,
                      //   child: Opacity(opacity: scale,child: new Container(
                      //     child: new Row(
                      //       children: [
                      //         new Container(
                      //           margin: EdgeInsets.only(left: 5,right: 5),
                      //           height: 16,
                      //           width: 16,
                      //           child: Image(
                      //             image: AssetImage(
                      //               AppImages.compareIcon,),),
                      //         ),
                      //         new Text('Compare Teams',
                      //             style: TextStyle(
                      //                 color: primaryColor,
                      //                 fontWeight: FontWeight.normal,
                      //                 fontSize: 12)),
                      //
                      //       ],
                      //     ),
                      //   ),),
                      //   onTap:onTap,
                      // ):new Container(),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          child: new Row(
                            children: [
                              new Container(
                                margin: EdgeInsets.only(left: 5,right: 5),
                                height: 13,
                                width: 13,
                                child: Image(
                                  image: AssetImage(
                                    AppImages.downloadIcon,),color: Themecolor,),
                              ),
                              new Text('Download',
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontWeight: FontWeight.normal,
                                      fontSize: 12)),

                            ],
                          ),
                        ),
                        onTap: ()=>{
                           print('pdf '),
                          print(widget.pdfUrl),
                              if(widget.pdfUrl.isEmpty){
                                MethodUtils.showError(context, 'PDF Not Ready Yet')
                              }
                              else{
                                _launchURL(widget.pdfUrl)
                              }
                        },
                      ),
                    ],
                  ),
                ),
                new Divider(height: 1,thickness: 1,),
              ],
            ):new Container(),
            new Container(
              height: 40,
              color: Colors.white,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  new Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Text(
                      'All Teams('+widget.leaderboardList.length.toString()+')',
                      style: TextStyle(
                          color: textGrey,
                          fontWeight: FontWeight.w500,
                          fontSize: 12),
                    ),
                  ),
                  widget.model.isFromLiveFinish??false?new Row(
                    children: [
                      new Container(
                        width: 50,
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Points',
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.w400,
                              fontSize: 12),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only(right: 10,left: 0),
                        width: 50,
                        alignment: Alignment.center,
                        child: Text(
                          '#Rank',
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.w400,
                              fontSize: 12),
                        ),
                      ),
                    ],
                  ):new Container(),
                ],
              ),
            ),
            new Divider(height: 1,thickness: 1,),

            new Expanded(
              child: new ListView.builder(
                  padding: EdgeInsets.all(0),
                  key: btnKey,
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: widget.leaderboardList.length,
                  itemBuilder: (BuildContext context, int index) {
                    if(widget.userId==widget.leaderboardList[index].userid.toString()){
                      team1Id=widget.leaderboardList[index].team_id.toString();
                    }
                    return new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: new Container(

                        margin: EdgeInsets.only(bottom: 5 ),
                        height: 57,
                        color: widget.userId==widget.leaderboardList[index].userid.toString()?TextFieldColor:teamBg,
                        child: new Opacity(opacity: widget.userId==widget.leaderboardList[index].userid.toString()?scale:1,child: new Column(
                          children: [
                            new Container(
                              padding: EdgeInsets.only(left: 10,right: 0,top: 10,bottom: 10),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  new Row(
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(right: 10),
                                        child: new ClipRRect(
                                          borderRadius: BorderRadius.all(Radius.circular(18)),
                                          child: new Container(
                                            height: 36,
                                            width: 36,
                                            child: CachedNetworkImage(
                                                imageUrl: widget.leaderboardList[index].user_image!,
                                                placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                                errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                                fit: BoxFit.fill
                                            ),
                                          ),
                                        ),
                                      ),
                                      new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          new Text(
                                            widget.leaderboardList[index].name!,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 13,
                                                fontWeight: FontWeight.w500),
                                          ),
                                          widget.leaderboardList[index].win_amount!=0?new Container(
                                            margin: EdgeInsets.only(top: 2),
                                            child: new Text(
                                              widget.userId==widget.leaderboardList[index].userid.toString()?'YOU WON ₹'+widget.leaderboardList[index].win_amount.toString():'WON ₹'+widget.leaderboardList[index].win_amount.toString(),
                                              style: TextStyle(
                                                  color: Colors.green,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ):new Container(),
                                          widget.leaderboardList[index].win_amount==0&&widget.leaderboardList[index].is_winning_zone!=0
                                          ?new Container(
                                            margin: EdgeInsets.only(top: 2),
                                            child: new Text(
                                              'In winning zone',
                                              style: TextStyle(
                                                  color: greenColor,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ):new Container(),
                                        ],
                                      )
                                    ],
                                  ),
                                  widget.model.isFromLiveFinish??false?new Row(
                                    children: [
                                      new Container(
                                        // margin: EdgeInsets.only(left: 30),
                                         width: 50,
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          widget.leaderboardList[index].getPoints()!,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12),
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(right: 20,left: 0),
                                         width: 30,
                                        alignment: Alignment.center,
                                        child: Text(
                                          widget.leaderboardList[index].userrank!,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w400,
                                              fontSize: 12),
                                        ),
                                      ),
                                    ],
                                  ):widget.leaderboardList[index].isjoined!?Visibility(
                                    visible: widget.model.isFromLive==true?false:true,
                                    child: new GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      child: new Container(
                                        margin: EdgeInsets.only(right: 20),
                                        height: 25,
                                        width: 25,
                                        padding: EdgeInsets.all(5),
                                        child: new Image.asset(AppImages.switchTeamIcon,
                                            fit: BoxFit.fill),
                                      ),
                                      onTap: (){
                                          widget.model.onSwitchTeam!(widget.leaderboardList[index].join_id);
                                      },
                                    ),
                                  ):new Container(),
                                ],
                              ),
                            ),
                            // new Divider(height: 1,thickness: 1,),
                          ],
                        ),),
                      ),
                      onTap: (){
                        if(isCompareTeamActive){
                          if(widget.userId!=widget.leaderboardList[index].userid.toString()){
                            navigateToCompareTeam(context, widget.model.matchKey!, widget.model.sportKey!, team1Id, widget.leaderboardList[index].team_id.toString(), widget.model.fantasyType.toString(), widget.model.slotId.toString(), widget.leaderboardList[index].challenge_id.toString());
                          }
                        }
                        else{
                          if(widget.userId==widget.leaderboardList[index].userid.toString() || (widget.model.isFromLiveFinish??false)){
                            widget.model.isForLeaderBoard=true;
                            widget.model.teamId=widget.leaderboardList[index].team_id;
                            widget.model.challengeId=widget.leaderboardList[index].challenge_id;
                            widget.model.teamname=widget.leaderboardList[index].teamname;


                            widget.model.isEditable=false;
                            navigateToTeamPreview(context,widget.model);
                          }
                          else{
                            Fluttertoast.showToast(msg: 'Sorry!! Team preview is available after match started.', toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
                          }
                        }

                      },
                    );
                  }
              ),
            ),
          ],
        )
    );
  }

  void onTap() {
    if(!isCompareTeamActive){
      setState(() {
        scale=0.3;
      });
      CustomToolTip popup = CustomToolTip(context,
          text: 'Now tap on a team to start comparing',
          textStyle: TextStyle(color: Colors.white,fontSize: 12),
          height: 35,
          width: 230,
          backgroundColor: primaryColor,
          padding: EdgeInsets.all(10.0),
          borderRadius: BorderRadius.circular(5.0),
          shadowColor: Colors.transparent,
      );
      popup.show(
        widgetKey: btnKey,
      );
      isCompareTeamActive=true;
    }else{
      setState(() {
        isCompareTeamActive=false;
        scale=1;
      });
    }

  }
  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

}