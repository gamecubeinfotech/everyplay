import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/transactions_response.dart';
import 'package:EverPlay/repository/model/transactions_request.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class RecentTransactions extends StatefulWidget {
  @override
  _RecentTransactionsState createState() => new _RecentTransactionsState();
}

class _RecentTransactionsState extends State<RecentTransactions>{

  TextEditingController codeController = TextEditingController();
  List<TransactionItem> transactionList=[];
  DateFormat dateFormat=new DateFormat("dd-MM-yyyy");
  late String userId='0';
  String filterType='All';
  DateTime startDateTime=DateTime(1900);
  String start_date='';
  String end_date='';
  String current_date=DateFormat('MMM dd,yyyy').format(DateTime.now());
  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getMyTransaction();
      })
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: whiteColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: Colors.black,
            // padding: EdgeInsets.only(top: 40),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.white,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Recent Transactions',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Container(
          height: MediaQuery.of(context).size.height,
          child: new Column(
            children: [
              // new Divider(height: 1,thickness: 1,),
              // new Container(
              //   height: 60,
              //   child: new Row(
              //     mainAxisAlignment: MainAxisAlignment.spaceBetween,
              //     children: [
              //       new GestureDetector(
              //         behavior: HitTestBehavior.translucent,
              //         child: new Container(
              //           margin: EdgeInsets.only(left: 10),
              //           child: new Row(
              //             children: [
              //               new Container(
              //                 margin: EdgeInsets.only(left: 5,right: 5),
              //                 height: 18,
              //                 width: 18,
              //                 child: Image(
              //                   image: AssetImage(
              //                     AppImages.filterIcon,),color: Colors.black,),
              //               ),
              //               new Text('Filters',
              //                   style: TextStyle(
              //                       color: Colors.black,
              //                       fontWeight: FontWeight.w500,
              //                       fontSize: 18)),
              //
              //             ],
              //           ),
              //         ),
              //         onTap: (){
              //           _modalBottomSheetMenu();
              //         },
              //       ),
              //       new Container(
              //           width: 150,
              //           height: 45,
              //           margin: EdgeInsets.all(10),
              //           child: ElevatedButton(
              //             textColor: Colors.white,
              //             elevation: .5,
              //             color: greenColor,
              //             child: Text(
              //               'Download',
              //               style: TextStyle(fontSize: 14,color: Colors.white),
              //             ),
              //             shape: RoundedRectangleBorder(
              //                 borderRadius:
              //                 BorderRadius.circular(5)),
              //             onPressed: () {
              //               transactionDownload();
              //             },
              //           ))
              //     ],
              //   ),
              // ),
              new Divider(height: 1,thickness: 1,),
              new Container(
                height: 35,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                alignment: Alignment.center,
                child: new Text(current_date,
                    style: TextStyle(
                        fontFamily: AppConstants.textBold,
                        color: Color(0xFF8e9193),
                        fontWeight: FontWeight.w500,
                        fontSize: 12)),
              ),

              Expanded(child: Container(child: SingleChildScrollView(
                child: new Container(
                  child: new ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: transactionList.length,
                      physics: NeverScrollableScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          child: new Container(
                            margin: EdgeInsets.fromLTRB(10, 16, 10, 0),
                            decoration: BoxDecoration(color: TextFieldColor,borderRadius: BorderRadius.circular(10)),
                            child: new Container(
                              padding: EdgeInsets.all(10),
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  new Container(
                                    margin: EdgeInsets.only(top: 10,bottom: 10),
                                    padding: EdgeInsets.fromLTRB(10,15,10,15),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.white
                                    ),
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        new Container(
                                          width: 100,
                                          child: new Text(double.parse(transactionList[index].deduct_amount!)>0?'- ₹'+transactionList[index].deduct_amount!:double.parse(transactionList[index].deduct_amount!)==0?double.parse(transactionList[index].add_amount!)>0?'+ ₹'+transactionList[index].add_amount!:'₹'+transactionList[index].deduct_amount!:'₹'+transactionList[index].add_amount!,
                                              style: TextStyle(
                                                  fontFamily: 'roboto',
                                                  color: double.parse(transactionList[index].deduct_amount!)>0?Colors.red:double.parse(transactionList[index].deduct_amount!)==0?double.parse(transactionList[index].add_amount!)>0?Themecolor:yellowColor:Themecolor,
                                                  fontWeight: FontWeight.w800,
                                                  fontSize: 16)),
                                        ),
                                        Flexible(
                                          child: new Text(transactionList[index].transaction_type!,
                                              style: TextStyle(
                                                  fontFamily: 'roboto',
                                                  color: TextFieldTextColor,
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 13)),
                                        )
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(top: 10),
                                    child:  new Text(
                                      'Transaction Id: '+transactionList[index].transaction_id!,
                                      style: TextStyle(
                                          fontFamily: 'roboto',
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(top: 5,bottom: 10),
                                    child:  new Text(
                                      'Transaction Date: '+transactionList[index].created!,
                                      style: TextStyle(
                                          fontFamily: 'roboto',
                                          color: Colors.black,
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),

                          onTap: (){
                            transactionDetails(transactionList[index]);
                          },
                        );

                      }
                  ),
                ),
              ),)),
            ],
          ),
        ),
      ),
    );
  }
  void _modalBottomSheetMenu() {
    showModalBottomSheet(
        context: context,
        isDismissible: false,
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(0), topRight: Radius.circular(0))),
        builder: (builder) {
          return StatefulBuilder(builder: (context,setState)=>Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new Container(
                height: 50,
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.only(left: 10, right: 10),
                color: mediumGrayColor,
                child: new Row(
                  children: [
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: new Container(
                        height: 40,
                        alignment: Alignment.center,
                        child: new Container(
                          child: Icon(
                            Icons.clear,
                            color: Colors.black,
                          ),
                        ),
                      ),
                      onTap: () => {
                        Navigator.pop(context)
                      },
                    ),
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: new Container(
                        width: MediaQuery.of(context).size.width - 50,
                        alignment: Alignment.center,
                        child: new Text('Filter',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 16)),
                      ),
                      onTap: () => {},
                    ),
                  ],
                ),
              ),
              InkWell(
                child: new Container(
                  padding: EdgeInsets.only(left: 10,right: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Container(
                        alignment: Alignment.center,
                        child: new Text('All',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 14)),
                      ),
                      Radio(
                        value: 'All',
                        groupValue: filterType,
                        activeColor: primaryColor,
                        onChanged: (value) {
                          setState(() {
                            filterType=value.toString();
                          });
                        },
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  setState(() {
                    filterType='All';
                  });
                },
              ),
              new Divider(
                height: 2,
                thickness: 1,
              ),
              InkWell(
                child: new Container(
                  padding: EdgeInsets.only(left: 10,right: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Container(
                        alignment: Alignment.center,
                        child: new Text('Deposits',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 14)),
                      ),
                      Radio(
                        value: 'Deposit',
                        groupValue: filterType,
                        activeColor: primaryColor,
                        onChanged: (value) {
                          setState(() {
                            filterType=value.toString();
                          });
                        },
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  setState(() {
                    filterType='Deposit';
                  });
                },
              ),
              new Divider(
                height: 2,
                thickness: 1,
              ),
              InkWell(
                child: new Container(
                  padding: EdgeInsets.only(left: 10,right: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Container(
                        alignment: Alignment.center,
                        child: new Text('Winnings',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 14)),
                      ),
                      Radio(
                        value: 'Winning',
                        groupValue: filterType,
                        activeColor: primaryColor,
                        onChanged: (value) {
                          setState(() {
                            filterType=value.toString();
                          });
                        },
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  setState(() {
                    filterType='Winning';
                  });
                },
              ),
              new Divider(
                height: 2,
                thickness: 1,
              ),
              InkWell(
                child: new Container(
                  padding: EdgeInsets.only(left: 10,right: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Container(
                        alignment: Alignment.center,
                        child: new Text('Withdraw',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 14)),
                      ),
                      Radio(
                        value: 'Withdraw',
                        groupValue: filterType,
                        activeColor: primaryColor,
                        onChanged: (value) {
                          setState(() {
                            filterType=value.toString();
                          });
                        },
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  setState(() {
                    filterType='Withdraw';
                  });
                },
              ),
              new Divider(
                height: 2,
                thickness: 1,
              ),
              InkWell(
                child: new Container(
                  padding: EdgeInsets.only(left: 10,right: 10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Container(
                        alignment: Alignment.center,
                        child: new Text('Affiliate Commission',
                            style: TextStyle(
                                fontFamily: AppConstants.textBold,
                                color: Colors.black,
                                fontWeight: FontWeight.w500,
                                fontSize: 14)),
                      ),
                      Radio(
                        value: 'Affiliate',
                        groupValue: filterType,
                        activeColor: primaryColor,
                        onChanged: (value) {
                          setState(() {
                            filterType=value.toString();
                          });
                        },
                      ),
                    ],
                  ),
                ),
                onTap: (){
                  setState(() {
                    filterType='Affiliate';
                  });
                },
              ),
              new Divider(
                height: 2,
                thickness: 1,
              ),
              new Container(
                margin: EdgeInsets.only(top: 10,left: 10,right: 10),
                child: new Row(
                  children: [
                    Flexible(child: new Container(
                      margin: EdgeInsets.only(right: 5),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          new Container(
                            alignment: Alignment.centerLeft,
                            child: new Text('From Date',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12)),
                          ),
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              margin: EdgeInsets.only(top: 5,bottom: 10),
                              padding: EdgeInsets.all(8),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  border: Border.all(color: mediumGrayColor,),
                                  borderRadius: BorderRadius.circular(2),
                                  color: Colors.white
                              ),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  new Container(
                                    child: new Text(start_date.isEmpty?'Select Date':start_date,
                                        style: TextStyle(
                                            fontFamily: 'roboto',
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14)),
                                  ),
                                  new Container(
                                    child: Icon(Icons.keyboard_arrow_down,size: 30,),
                                  )
                                ],
                              ),
                            ),
                            onTap: () async {

                              startDateTime = (await showDatePicker(
                                  context: context,
                                  initialDate:DateTime.now(),
                                  firstDate:DateTime(1900),
                                  lastDate: DateTime.now()))!;

                              start_date = dateFormat.format(startDateTime);
                              setState(() {});
                            },
                          )
                        ],
                      ),
                    ),flex: 1,),
                    Flexible(child: new Container(
                      margin: EdgeInsets.only(left: 5),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          new Container(
                            alignment: Alignment.centerLeft,
                            child: new Text('End Date',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w400,
                                    fontSize: 12)),
                          ),
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(
                              margin: EdgeInsets.only(top: 5,bottom: 10),
                              padding: EdgeInsets.all(8),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  border: Border.all(color: mediumGrayColor,),
                                  borderRadius: BorderRadius.circular(2),
                                  color: Colors.white
                              ),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  new Container(
                                    child: new Text(end_date.isEmpty?'Select Date':end_date,
                                        style: TextStyle(
                                            fontFamily: 'roboto',
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14)),
                                  ),
                                  new Container(
                                    child: Icon(Icons.keyboard_arrow_down,size: 30),
                                  )
                                ],
                              ),
                            ),
                            onTap: () async {
                              DateTime date = DateTime(1900);

                              date = (await showDatePicker(
                                  context: context,
                                  initialDate:DateTime.now(),
                                  firstDate:startDateTime,
                                  lastDate: DateTime.now()))!;

                              end_date = dateFormat.format(date);
                              setState(() {});
                            },
                          )
                        ],
                      ),
                    ),flex: 1,)
                  ],
                ),
              ),
              new Container(
                  width: MediaQuery.of(context).size.width,
                  height: 45,
                  margin: EdgeInsets.all(10),
                  child: ElevatedButton(
                    onPressed: () {
                      if (start_date.isEmpty) {
                        MethodUtils.showError(context, 'Please select from date');
                      } else if (end_date.isEmpty) {
                        MethodUtils.showError(context, 'Please select end date');
                      } else {
                        Navigator.pop(context);
                        getMyTransaction();
                      }
                    },
                    style: ElevatedButton.styleFrom(
                      primary: greenColor,
                      elevation: 0.5,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                    ),
                    child: Text(
                      'Apply',
                      style: TextStyle(fontSize: 14, color: Colors.white),
                    ),
                  )

              )
            ],
          ));
        });
  }
  void getMyTransaction() async {
    transactionList.clear();
    AppLoaderProgress.showLoader(context);
    TransactionRequest loginRequest = new TransactionRequest(user_id: userId,page: 0,filter_type: filterType,start_date: start_date,end_date: end_date);
    final client = ApiClient(AppRepository.dio);
    TransactionsResponse transactionsResponse = await client.getMyTransaction(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if(transactionsResponse.status==1){
      //current_date=transactionsResponse.result!.data!.length>0?DateFormat('MMM dd,yyyy').format(DateFormat('yyyy-MM-dd HH:mm:ss').parse(transactionsResponse.result!.data![0].date!)):DateFormat('MMM dd,yyyy').format(DateTime.now());
      transactionList=transactionsResponse.result!.data!;
    }
    setState(() {});

  }
  void transactionDownload() async {
    AppLoaderProgress.showLoader(context);
    TransactionRequest loginRequest = new TransactionRequest(user_id: userId,page: 0,filter_type: filterType,start_date: start_date,end_date: end_date);
    final client = ApiClient(AppRepository.dio);
    GeneralResponse transactionsResponse = await client.transactionDownload(loginRequest);
    AppLoaderProgress.hideLoader(context);
    Fluttertoast.showToast(msg: transactionsResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {});

  }

  void transactionDetails(TransactionItem transactionList) {

    showDialog(
      context: context,

      builder: (BuildContext context) {
        // var height = MediaQuery.of(context).size.height*0.20;
        // var width = MediaQuery.of(context).size.width*0.90;
        return Dialog(
          insetPadding: EdgeInsets.all(10),
          child:  Container(
            height: 200 ,
            // width: width,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white
            ),

            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: Text(transactionList.transaction_type.toString(),

                      style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold,),maxLines: 2,overflow: TextOverflow.ellipsis,)),
                    InkWell(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Icon(
                          Icons.clear_rounded, color: Colors.black,size: 20,
                        ),
                      ),
                    ),
                  ],
                ),

                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Amount :',style: TextStyle(color: Colors.grey,fontSize: 15),),
                      new Container(
                        //width: 100,
                        child: new Text(double.parse(transactionList.deduct_amount!)>0?'- ₹'+transactionList.deduct_amount!:double.parse(transactionList.deduct_amount!)==0?double.parse(transactionList.add_amount!)>0?'+ ₹'+transactionList.add_amount!:'₹'+transactionList.deduct_amount!:'₹'+transactionList.add_amount!,
                            style: TextStyle(
                                fontFamily: 'roboto',
                                color: double.parse(transactionList.deduct_amount!)>0?Colors.grey:double.parse(transactionList.deduct_amount!)==0?double.parse(transactionList.add_amount!)>0?darkGrayColor:darkGrayColor:darkGrayColor,
                                fontWeight: FontWeight.w800,
                                fontSize: 15)),
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Match :',style: TextStyle(color: Colors.grey,fontSize: 15),),
                      Flexible(

                        child: Padding(
                          padding: const EdgeInsets.only(left: 48.0),
                          child: Text(transactionList.matchname==null?"-":transactionList.matchname.toString(),maxLines:3,
                            style: TextStyle(color: Colors.grey,fontSize: 15,
                              overflow: TextOverflow.fade,),softWrap: true,),
                        ),
                      )
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Contest :',style: TextStyle(color: Colors.grey,fontSize: 15),),
                      Expanded(child: Text(transactionList.challengename??'-',style: TextStyle(color: Colors.grey,fontSize: 15),))
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Available Balance',style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold),),
                      Text('₹'+transactionList.available.toString(),style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold),)
                    ],
                  ),
                ),
              ],
            ),

          ),
        );

      },
    );
  }
}