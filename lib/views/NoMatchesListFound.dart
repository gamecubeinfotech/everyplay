import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';

class NoMatchesListFound extends StatelessWidget {
  String? sportKey;
  NoMatchesListFound({this.sportKey});
  @override
  Widget build(BuildContext context) {
    return new Container(
      margin: EdgeInsets.only(top: 30),
      alignment: Alignment.center,
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          new Container(
            margin: EdgeInsets.only(top: 5),
            child: new Text(
              "You haven't joined a contest yet!",
              style: TextStyle(
                  color: Title_Color_1,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
          ),
          new Container(
            padding: EdgeInsets.all(20),
            child: new ConstrainedBox(
              constraints: new BoxConstraints(
                maxHeight: 200.0,
                maxWidth: 200.0,
              ),
              child: Image(
                image:sportKey==null?AssetImage(AppImages.noContestJoinedIcon):AssetImage(sportKey=='CRICKET'?AppImages.cricPlayerIcon:
                sportKey=='LIVE'?AppImages.cricPlayerIcon: AppImages.footPlayerIcon),
              ),
            ),
          ),
          new Container(
            margin: EdgeInsets.only(top: 5),
            child: new Text(
              'Find a contest to join and start winning.',
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: textGrey,
                  fontSize: 14,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ],
      ),
    );
  }
}