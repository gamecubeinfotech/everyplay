import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/data.dart';
import 'package:EverPlay/repository/model/login_request.dart';
import 'package:EverPlay/repository/model/register_request.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:http/http.dart' as http;

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool _passwordVisible = false;
  bool _confirmPasswordVisible = false;
  bool _isMobileLogin = true;
  TextEditingController codeController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  bool checkedValue=false;
  String fcmToken='';
  int version=0;
  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
    _confirmPasswordVisible = false;
    FirebaseMessaging _messaging = FirebaseMessaging.instance;
    _messaging.getToken().then((token){
      fcmToken=token!;
    });
    deviceVersion();
  }
  void deviceVersion() async {
    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      setState(() {version = int.parse(iosInfo.systemVersion.split('.')[0]);});
    }
  }
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: primaryColor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.dark) /* set Status bar icon color in iOS. */
        );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: Colors.white,
        body: new SingleChildScrollView(
          child: new Stack(
            children: [
              new Container(
                height: MediaQuery.of(context).size.height,
                child: new Column(
                  children: [
                    new Container(
                      height: MediaQuery.of(context).size.height / 3,
                      decoration: new BoxDecoration(
                        gradient: new LinearGradient(
                          colors: [
                            const Color(0xff190304),
                            const Color(0xff630f12),
                          ],
                          begin: const FractionalOffset(0.0, 0.0),
                          end: const FractionalOffset(0.0, 1.0),
                        ),
                      ),
                    ),
                    new Container(
                      height: MediaQuery.of(context).size.height -
                          MediaQuery.of(context).size.height / 3,
                      color: bgColor,
                    )
                  ],
                ),
              ),
              new Container(
                margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
                child: Column(
                  children: [
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => {Navigator.pop(context)},
                      child: new Container(
                        padding: EdgeInsets.all(15),
                        alignment: Alignment.centerLeft,
                        child: new Container(
                          width: 16,
                          height: 16,
                          child: Image(
                              image: AssetImage(AppImages.backImageURL),
                              fit: BoxFit.fill),
                        ),
                      ),
                    ),
                    new Card(
                      margin: EdgeInsets.only(
                          left: 15, right: 15, bottom: 15, top: 10),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: new Container(
                        padding: EdgeInsets.all(5),
                        child: new Column(
                          children: [
                            new Container(
                              padding: EdgeInsets.all(10),
                              child: new Row(
                                children: [
                                  new GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    child: new Container(
                                      child: new Column(
                                        children: [
                                          Text(
                                            'Create Account',
                                            textAlign: TextAlign.end,
                                            style: TextStyle(
                                              fontSize: 20,
                                              color: _isMobileLogin
                                                  ? Colors.black
                                                  : Colors.grey,
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ],
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                      ),
                                    ),
                                    onTap: () => {},
                                  ),
                                ],
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.only(top: 20),
                              child: new Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  new Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      'Refer Code',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    padding: EdgeInsets.all(10),
                                    child: TextField(
                                      controller: codeController,
                                      decoration: InputDecoration(
                                        counterText: "",
                                        fillColor: textFieldBgColor,
                                        filled: true,
                                        hintStyle: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        hintText: 'Refer code',
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            width: 0,
                                            style: BorderStyle.none,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      'Mobile Number',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    padding: EdgeInsets.all(10),
                                    child: TextField(
                                      controller: mobileController,
                                      keyboardType: TextInputType.number,
                                      maxLength: 10,
                                      decoration: InputDecoration(
                                        counterText: "",
                                        fillColor: textFieldBgColor,
                                        filled: true,
                                        hintStyle: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        hintText: 'Mobile number',
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            width: 0,
                                            style: BorderStyle.none,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      'Email Address',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                    padding: EdgeInsets.all(10),
                                    child: TextField(
                                      controller: emailController,
                                      decoration: InputDecoration(
                                        counterText: "",
                                        fillColor: textFieldBgColor,
                                        filled: true,
                                        hintStyle: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        hintText: 'Email Address',
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            width: 0,
                                            style: BorderStyle.none,
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      'Date of Birth',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    child: new Container(
                                      margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                                      padding: EdgeInsets.all(10),
                                      child: TextField(
                                        enabled: false,
                                        readOnly: true,
                                        controller: dobController,
                                        decoration: InputDecoration(
                                          counterText: "",
                                          fillColor: textFieldBgColor,
                                          filled: true,
                                          hintStyle: TextStyle(
                                            fontSize: 12,
                                            color: Colors.grey,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.bold,
                                          ),
                                          hintText: 'Date of Birth',
                                          border: OutlineInputBorder(
                                            borderSide: BorderSide(
                                              width: 0,
                                              style: BorderStyle.none,
                                            ),
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                        ),

                                      ),
                                    ),
                                    onTap: () async{
                                      FocusScope.of(context).requestFocus(new FocusNode());
                                      await showDatePicker(
                                          context: context,
                                          initialDate:DateTime(DateTime.now().year-18,DateTime.now().month,DateTime.now().day),
                                          firstDate:DateTime(1900),
                                          lastDate: DateTime(DateTime.now().year-18,DateTime.now().month,DateTime.now().day)).then((value) => {
                                                  dobController.text = DateFormat("dd/MM/yyyy").format(value!)
                                          });
                                      },
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      'Password',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    padding: EdgeInsets.all(10),
                                    child: TextField(
                                      obscureText: !_passwordVisible,
                                      // controller: passwordController,
                                      decoration: InputDecoration(
                                        fillColor: textFieldBgColor,
                                        filled: true,
                                        hintStyle: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        hintText: 'Password',
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            width: 0,
                                            style: BorderStyle.none,
                                          ),
                                          borderRadius:
                                          BorderRadius.circular(10.0),
                                        ),
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            // Based on passwordVisible state choose the icon
                                            _passwordVisible
                                                ? Icons.visibility
                                                : Icons.visibility_off,
                                            color: Colors.grey,
                                          ),
                                          onPressed: () {
                                            // Update the state i.e. toogle the state of passwordVisible variable
                                            setState(() {
                                              _passwordVisible =
                                              !_passwordVisible;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      'Confirm Password',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 12,
                                        color: Colors.grey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    padding: EdgeInsets.all(10),
                                    child: TextField(
                                      obscureText: !_confirmPasswordVisible,
                                      controller: confirmPasswordController,
                                      decoration: InputDecoration(
                                        fillColor: textFieldBgColor,
                                        filled: true,
                                        hintStyle: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.bold,
                                        ),
                                        hintText: 'Confirm Password',
                                        border: OutlineInputBorder(
                                          borderSide: BorderSide(
                                            width: 0,
                                            style: BorderStyle.none,
                                          ),
                                          borderRadius:
                                          BorderRadius.circular(10.0),
                                        ),
                                        suffixIcon: IconButton(
                                          icon: Icon(
                                            // Based on passwordVisible state choose the icon
                                            _confirmPasswordVisible
                                                ? Icons.visibility
                                                : Icons.visibility_off,
                                            color: Colors.grey,
                                          ),
                                          onPressed: () {
                                            // Update the state i.e. toogle the state of passwordVisible variable
                                            setState(() {
                                              _confirmPasswordVisible =
                                              !_confirmPasswordVisible;
                                            });
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(left: 10),
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        new Container(
                                          width: 20,
                                          child: Checkbox(
                                            value: checkedValue,
                                            activeColor: Colors.black,
                                            onChanged: (value) {
                                              setState(() {
                                                checkedValue = value!;
                                              });
                                            },
                                          ),
                                        ),
                                        Expanded(child: new Container(
                                          child: new Html(data: "By registering, I agree to <a href="+AppConstants.terms_url+"><font color='#000000'><u><strong>T&C</strong></u></font></a>",
                                            onLinkTap: (link,_,__){
                                              if(link!.contains("terms_and_conditions")) {
                                                navigateToVisionWebView(context, 'Terms & Condition',link);
                                              }
                                            },
                                            style: {
                                              'html': Style(textAlign: TextAlign.start,color: Colors.grey,textDecoration: TextDecoration.none)
                                            },
                                          ),
                                        )),
                                      ],
                                    ),
                                  ),
                                  new Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: 50,
                                      margin: EdgeInsets.all(10),
                                      child: ElevatedButton(
                                        onPressed: () {
                                          userRegisterNew();
                                        },
                                        style: ElevatedButton.styleFrom(
                                          primary: primaryColor,
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(10),
                                          ),
                                        ),
                                        child: Text(
                                          'CREATE ACCOUNT',
                                          style: TextStyle(fontSize: 15),
                                        ),
                                      )
                                  ),
                                  new Container(
                                    margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                    child: new Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        new Flexible(
                                          child: new Container(
                                            decoration: BoxDecoration(
                                              border: Border(
                                                bottom: BorderSide(
                                                  color: textFieldBgColor,
                                                  width:
                                                      1.5, // This would be the width of the underline
                                                ),
                                              ),
                                            ),
                                          ),
                                          flex: 2,
                                        ),
                                        new Flexible(
                                          child: new Text(
                                            'OR',
                                            style: TextStyle(
                                                color: Colors.grey, fontSize: 14),
                                          ),
                                          flex: 1,
                                        ),
                                        new Flexible(
                                          child: new Container(
                                            decoration: BoxDecoration(
                                              border: Border(
                                                bottom: BorderSide(
                                                  color: textFieldBgColor,
                                                  width:
                                                      1.5, // This would be the width of the underline
                                                ),
                                              ),
                                            ),
                                          ),
                                          flex: 2,
                                        )
                                      ],
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        new GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          child: new Container(
                                            height: 65,
                                            width: 65,
                                            padding: EdgeInsets.all(5),
                                            child: new ClipOval(
                                              child: Image.asset(
                                                AppImages.fbImageURL,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                          onTap: (){
                                            initiateFacebookLogin();
                                          },
                                        ),
                                        new GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          child: new Container(
                                            height: 65,
                                            width: 65,
                                            padding: EdgeInsets.all(5),
                                            child: new ClipOval(
                                              child: Image.asset(
                                                AppImages.googleImageURL,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                          onTap: (){
                                            signup(context);
                                          },
                                        ),
                                        version>=13?new GestureDetector(
                                          behavior: HitTestBehavior.translucent,
                                          child: new Container(
                                            height: 65,
                                            width: 65,
                                            padding: EdgeInsets.all(5),
                                            child: new ClipOval(
                                              child: Image.asset(
                                                AppImages.appleImageURL,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ),
                                          onTap: () async {
                                            final credential = await SignInWithApple.getAppleIDCredential(
                                              scopes: [
                                                AppleIDAuthorizationScopes.email,
                                                AppleIDAuthorizationScopes.fullName,
                                              ],
                                              webAuthenticationOptions: WebAuthenticationOptions(
                                                // TODO: Set the `clientId` and `redirectUri` arguments to the values you entered in the Apple Developer portal during the setup
                                                clientId:
                                                'com.rg.ios.EverPlay',
                                                redirectUri: Uri.parse(
                                                  'https://flutter-sign-in-with-apple-example.glitch.me/callbacks/sign_in_with_apple',
                                                ),
                                              ),
                                              // TODO: Remove these if you have no need for them
                                              nonce: 'example-nonce',
                                              state: 'example-state',
                                            );
                                            userLoginSocial(new LoginRequest(name: credential.givenName??'' ,email: credential.email??'',image: '',idToken: credential.identityToken??'',deviceId: '',social_id: credential.userIdentifier,socialLoginType: 'apple',fcmToken: fcmToken));
                                          },
                                        ):new Container(),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    new Container(
                      margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          new Container(
                            child: Text("Already have an account? ",
                                style: TextStyle(
                                    fontFamily: AppConstants.textBold,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 14)),
                          ),
                          new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            onTap: () => {

                              navigateToLogin(context),
                              Navigator.pop(context),
                            },
                            child: new Container(
                              child: Text("Sign In",
                                  style: TextStyle(
                                      fontFamily: AppConstants.textBold,
                                      color: primaryColor,
                                      decoration: TextDecoration.underline,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14)),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
  void userRegisterNew() async {
    if(mobileController.text.isEmpty||mobileController.text.length<10){
      Fluttertoast.showToast(
          msg: 'Please enter valid mobile number.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }else if(emailController.text.isEmpty){
      Fluttertoast.showToast(
          msg: 'Please enter valid email address',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }else if(!emailController.text.contains("@")||!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailController.text)){
      Fluttertoast.showToast(
          msg: 'Please enter valid email address',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }else if(dobController.text.isEmpty){
      Fluttertoast.showToast(
          msg: 'Please select date of birth.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }else if(passwordController.text.isEmpty){
      Fluttertoast.showToast(
          msg: 'Please enter valid password.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }else if(passwordController.text.length<4){
      Fluttertoast.showToast(
          msg: 'Password should be 4 to 16 char long.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }else if(confirmPasswordController.text.isEmpty){
      Fluttertoast.showToast(
          msg: 'Please enter confirm password.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }else if(passwordController.text.toString()!=confirmPasswordController.text.toString()){
      Fluttertoast.showToast(
          msg: 'Password & Confirm password not matched.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }else if(checkedValue == false){
      Fluttertoast.showToast(
          msg: 'Please accept EverPlay T&Cs.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }
    AppLoaderProgress.showLoader(context);
    RegisterRequest loginRequest = new RegisterRequest(codeController.text.toString(), emailController.text.toString(), fcmToken, '',passwordController.text.toString(), mobileController.text.toString(),dobController.text.toString().trim());
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse = await client.userRegisterNew(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      AppPrefrence.putString(AppConstants.LOGIN_REGISTER_TYPE, "manual_register");
      AppPrefrence.putString(AppConstants.FROM, "register");
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID, loginResponse.result!.user_id.toString());
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE, loginResponse.result!.mobile.toString());
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_EMAIL, loginResponse.result!.email);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_DOB, loginResponse.result!.dob);
      navigateToOtpVerify(context);

    }
    Fluttertoast.showToast(
        msg: loginResponse.message!,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1);
  }

  void userLoginSocial(LoginRequest loginRequest) async{
    AppLoaderProgress.showLoader(context);
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse = await client.userLoginSocial(loginRequest);
    AppPrefrence.putString(AppConstants.LOGIN_REGISTER_TYPE, "social_register");
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID, loginResponse.result!.user_id.toString());
    // AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_NAME, loginResponse.result!.username);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE, loginResponse.result!.mobile.toString());
    // AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_EMAIL, loginResponse.result!.email);
    // AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TOKEN, loginResponse.result!.custom_user_token);
    // AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_REFER_CODE, loginResponse.result!.refercode);
    // AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME, loginResponse.result!.team);
    // AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_STATE_NAME, loginResponse.result!.state);
    // AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_PIC, loginResponse.result!.user_profile_image);
    // AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, loginResponse.result!.bank_verify);
    // AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, loginResponse.result!.pan_verify);
    // AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, loginResponse.result!.mobile_verify);
    // AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, loginResponse.result!.email_verify);
    // AppPrefrence.putString(AppConstants.AUTHTOKEN, loginResponse.result!.custom_user_token);
    // AppConstants.token=loginResponse.result!.custom_user_token!;
    if(loginResponse.result!.mobile_verify==1){
      AppPrefrence.putString(AppConstants.FROM, "login");
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID, loginResponse.result!.user_id.toString());
      navigateToOtpVerify(context);
    }else{
      navigateToVerifyEmailOrMobile(context, 'mobile', emailViewRefresh);
    }

    AppLoaderProgress.hideLoader(context);
  }
  Future<void> signup(BuildContext context) async {
    final FirebaseAuth auth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final GoogleSignInAccount? googleSignInAccount = await googleSignIn.signIn();
    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
      await googleSignInAccount.authentication;
      final AuthCredential authCredential = GoogleAuthProvider.credential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);
      // Getting users credential
      UserCredential result = await auth.signInWithCredential(authCredential);

      if (result != null) {
        User user = result.user!;
        userLoginSocial(new LoginRequest(name: user.displayName,email: user.email,image: user.photoURL??'',idToken: user.refreshToken,deviceId: '',social_id: user.uid,socialLoginType:'gmail',fcmToken: fcmToken));
        googleSignIn.signOut();
      }  // if result not null we simply call the MaterialpageRoute,
      // for go to the HomePage screen
    }
  }

  void emailViewRefresh(String email,int type){

  }
  Future<void> initiateFacebookLogin() async {
    final LoginResult result = await FacebookAuth.instance.login(permissions: ["public_profile", "email", "user_friends"]);
    switch (result.status) {
      case  LoginStatus.failed:
        print("Error");

        break;
      case LoginStatus.cancelled:
        print("CancelledByUser");

        break;
      case LoginStatus.success:
        var graphResponse = await http.get(Uri.parse('https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token='+result.accessToken!.token));
        var profile = json.decode(graphResponse.body);
        userLoginSocial(new LoginRequest(name: profile['name'] ,email: profile['email'],image: profile['picture']??'',idToken: '',deviceId: '',social_id: profile['id'],socialLoginType: 'facebook',fcmToken: fcmToken));
        break;
    }
  }
}
