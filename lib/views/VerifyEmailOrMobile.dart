import 'dart:math';

import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:otp_autofill/otp_autofill.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/OtpWidgetModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/data.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class VerifyEmailOrMobile extends StatefulWidget {
  String type='mobile';
  String otp='none';
  Function emailViewRefresh;
  VerifyEmailOrMobile(this.type,this.emailViewRefresh);

  @override
  _VerifyEmailOrMobileState createState() => new _VerifyEmailOrMobileState();
}

class _VerifyEmailOrMobileState extends State<VerifyEmailOrMobile>{
  List<OtpWidgetModel> mobileOtpFields = <OtpWidgetModel>[];
  List<OtpWidgetModel> emailOtpFields = <OtpWidgetModel>[];
  TextEditingController mobileController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  CustomTimerController _mController = new CustomTimerController();
  CustomTimerController _eController = new CustomTimerController();
  bool _enable=false;
  late String userId='0';
  String mOtp = '';
  late OTPInteractor _otpInteractor;

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
      })
    });
    for (int i = 0; i < 6; i++) {
      late TextEditingController otpController = new TextEditingController();
      OtpWidgetModel model = new OtpWidgetModel();
      model.widget = _otpTextField(context, false, otpController);
      model.otpController = otpController;
      mobileOtpFields.add(model);
    }
    for (int i = 0; i < 6; i++) {
      late TextEditingController otpController = new TextEditingController();
      OtpWidgetModel model = new OtpWidgetModel();
      model.widget = _otpTextField(context,false, otpController);
      model.otpController = otpController;
      emailOtpFields.add(model);
    }
    if(widget.type=='mobile'){
      otpListener();
    }
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(

            child:  Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Verification',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.black,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),

              ],
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: new Column(
            children: [

              Image.asset(AppImages.phoneVerify,scale: 2,height: 200),

              widget.type=='mobile'?new Container(
                child: new SingleChildScrollView(
                  child: new Container(
                    child: new Column(
                      children: [
                        new Container(
                          alignment: Alignment.center,
                          child: Text(
                            'Verify Your Number',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontSize: 25,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                        new Container(
                          padding: EdgeInsets.only(top: 20,bottom: 20),
                          alignment: Alignment.center,
                          child: Text(
                            'Please enter your phone number',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.grey,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                          padding: EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Mobile Number',
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: textGrey,
                                    fontStyle:
                                    FontStyle.normal,
                                    fontWeight:
                                    FontWeight.w500,letterSpacing: 0.7,
                                  )),
                              SizedBox(height: 5,),
                              TextField(
                                controller: mobileController,
                                keyboardType: TextInputType.number,
                                maxLength: 10,
                                decoration: InputDecoration(
                                  counterText: "",
                                  fillColor: Colors.white,
                                  filled: true,
                                  prefixIcon: SizedBox(
                                    child: new Container(
                                      height: 60,
                                      width: 60,
                                      margin: EdgeInsets.only(right: 10),
                                      decoration: BoxDecoration(
                                        border: Border(right: BorderSide(width: 1,color: TextFieldBorderColor))
                                        // Border(left: BorderSide(color: bgColor,width: 1),top: BorderSide(color: bgColor,width: 1),bottom: BorderSide(color: bgColor,width: 1),right: BorderSide(color: bgColor,width: 1)),
                                      ),
                                      child: Center(
                                        widthFactor: 0.0,
                                        child: Text('+91',
                                            style: TextStyle(
                                              fontSize: 16,
                                              color: Colors.black,
                                              fontStyle:
                                              FontStyle.normal,
                                              fontWeight:
                                              FontWeight.bold,
                                            )),
                                      ),
                                    ),
                                  ),
                                  hintStyle: TextStyle(
                                    fontSize: 16,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  hintText: 'Enter Your Mobile Number',
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    borderSide: BorderSide(color: TextFieldBorderColor, width: 1,),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15),
                                    borderSide: BorderSide(color: TextFieldBorderColor, width: 1),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        new Container(
                            width:
                            MediaQuery.of(context).size.width,
                            height: 50,
                            margin: EdgeInsets.fromLTRB(80, 45, 80, 10),
                            child: ElevatedButton(
                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),elevation: MaterialStateProperty.all(.5) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)))) ,
                              child: Text(
                                'Send',
                                style: TextStyle(fontSize: 14,color: Colors.white),
                              ),
                              onPressed: () {
                                if(mobileController.text.isEmpty||mobileController.text.length<10){
                                  MethodUtils.showError(context, "Enter a valid mobile number.");
                                  return;
                                }
                                verifyMobile();
                              },
                            ))
                      ],
                    ),
                  ),
                ),
              ):new Container(),
              widget.type=='email'?new Container(
                child: new SingleChildScrollView(
                  child: new Container(
                    child: new Column(
                      children: [
                        new Container(
                          padding: EdgeInsets.only(top: 20),
                          alignment: Alignment.center,
                          child: Text(
                            'Verify Your Email',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontSize: 25,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w800,
                            ),
                          ),
                        ),
                        new Container(
                          padding: EdgeInsets.only(top: 20,bottom: 30),
                          alignment: Alignment.center,
                          child: Text(
                            'Please enter your email',
                            textAlign: TextAlign.start,
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.grey,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
                          padding: EdgeInsets.all(10),
                          child: TextField(
                            controller: emailController,
                            keyboardType: TextInputType.emailAddress,
                            decoration: InputDecoration(
                              counterText: "",
                              fillColor: Colors.white,
                              filled: true,
                              hintStyle: TextStyle(
                                fontSize: 17,
                                color: Colors.grey,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.bold,
                              ),
                              hintText: 'Email',
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: bgColor, width: 2),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: bgColor, width: 2),
                              ),
                            ),
                          ),
                        ),
                        new Container(
                            width:
                            MediaQuery.of(context).size.width,
                            height: 50,
                            margin: EdgeInsets.fromLTRB(40, 20, 40, 10),
                            child: ElevatedButton(
                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),elevation: MaterialStateProperty.all(.5) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(3)))) ,
                              child: Text(
                                'Send',
                                style: TextStyle(fontSize: 14,color: Colors.white),
                              ),
                              onPressed: () {
                                if(emailController.text.isEmpty){
                                  MethodUtils.showError(context, "Enter a valid email id.");
                                  return;
                                }else if(!emailController.text.contains("@")||!RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailController.text)){
                                  MethodUtils.showError(context, "Invalid Email Id.");
                                  return;
                                }else{
                                  verifyEmailStatus();
                                }
                              },
                            ))
                      ],
                    ),
                  ),
                ),
              ):new Container(),
              widget.otp=='mobile'?new Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  new Container(
                    child: new Row(
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                new Container(
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.only(top: 20),
                                  alignment: Alignment.center,
                                  child: Text(
                                    'Verify Mobile Number',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 25,
                                      color: Colors.black,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w800,
                                    ),
                                  ),
                                ),
                                new Container(
                                  width: MediaQuery.of(context).size.width,
                                  alignment: Alignment.center,
                                  margin: EdgeInsets.only(top: 5),
                                  child: new Text("We have sent an OTP on your number",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Colors.grey,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(top: 15),
                                  child: new Text(
                                    "+91" + mobileController.text.toString(),
                                    textAlign: TextAlign.end,
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: Themecolor,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.bold,
                                      decoration:
                                      TextDecoration.underline,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () => {

                          },
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 20),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          padding: EdgeInsets.all(10),
                          child: Wrap(
                              alignment: WrapAlignment.start,
                              spacing: 4,
                              direction: Axis.horizontal,
                              runSpacing: 10,
                              children: List.generate(
                                mobileOtpFields.length,
                                    (index) => mobileOtpFields[index].widget,
                              )),
                        ),
                        new Container(
                            width:
                            MediaQuery
                                .of(context)
                                .size
                                .width,
                            height: 50,
                            margin:
                            EdgeInsets.fromLTRB(10, 30, 10, 10),
                            child: ElevatedButton(
                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)))) ,
                              child: Text(
                                'CONTINUE',
                                style: TextStyle(fontSize: 15),
                              ),
                              onPressed: () {
                                mOtp='';
                                for (int i = 0; i < 6; i++) {
                                  if (mobileOtpFields[i].otpController.text.isEmpty) {
                                    Fluttertoast.showToast(
                                        msg: 'Please enter your six digit otp',
                                        toastLength: Toast.LENGTH_SHORT,
                                        timeInSecForIosWeb: 1);
                                    return;
                                  }
                                  else {
                                    mOtp = mOtp + mobileOtpFields[i].otpController.text.toString();
                                    print("my otp is ");
                                    print(mOtp);

                                  }
                                }
                                verifyMobileOtp();
                              },
                            )),
                        new Container(
                          margin:
                          EdgeInsets.fromLTRB(10, 0, 10, 10),
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          child: new Column(
                            children: [
                              new Container(
                                margin: EdgeInsets.only(
                                    bottom: 10, top: 10),
                                child: Text(
                                    "Didn't received the OTP?",
                                    style: TextStyle(
                                        fontFamily:
                                        AppConstants.textBold,
                                        color: Colors.grey,
                                        fontWeight:
                                        FontWeight.normal,
                                        fontSize: 14)),
                              ),
                              new Container(
                                child: CustomTimer(
                                  from: Duration(seconds: 60),
                                  to: Duration(seconds: 0),
                                  controller: _mController,
                                  onBuildAction: CustomTimerAction.auto_start,
                                  builder: (CustomTimerRemainingTime remaining) {
                                    return new GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      child: Text(
                                        remaining.seconds=='00'?"Resend OTP ?":"${remaining.minutes}:${remaining.seconds}",
                                        style: TextStyle(
                                            fontFamily:
                                            AppConstants.textBold,
                                            color: Themecolor,
                                            decoration: remaining.seconds=='00'?TextDecoration.underline:TextDecoration.none,
                                            fontWeight:
                                            FontWeight.bold,
                                            fontSize: 14),
                                      ),
                                      onTap: (){
                                        if(remaining.seconds=='00'){
                                          resendOTP();
                                        }
                                      },
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ):new Container(),
              widget.otp=='email'?new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(top: 20),
                    alignment: Alignment.center,
                    child: Text(
                      'Verify Email',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 25,
                        color: Colors.black,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                  ),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(top: 10,bottom: 20),
                    child: new Text(
                      "We have sent an OTP on your email address",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.grey,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 10),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
                          padding: EdgeInsets.all(10),
                          child: Wrap(
                              alignment: WrapAlignment.start,
                              spacing: 4,
                              direction: Axis.horizontal,
                              runSpacing: 10,
                              children: List.generate(
                                emailOtpFields.length,
                                    (index) =>
                                emailOtpFields[index].widget,
                              )),
                        ),
                        new Container(
                            width:
                            MediaQuery
                                .of(context)
                                .size
                                .width,
                            height: 50,
                            margin:
                            EdgeInsets.fromLTRB(10, 30, 10, 10),
                            child: ElevatedButton(
                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)))) ,
                              child: Text(
                                'CONTINUE',
                                style: TextStyle(fontSize: 15),
                              ),
                              onPressed: () {
                                mOtp='';
                                for (int i = 0; i < 6; i++) {
                                  if (emailOtpFields[i].otpController.text.isEmpty) {
                                    Fluttertoast.showToast(
                                        msg: 'Please enter your six digit otp',
                                        toastLength: Toast.LENGTH_SHORT,
                                        timeInSecForIosWeb: 1);
                                    return;
                                  }
                                  else {
                                    mOtp = mOtp + emailOtpFields[i].otpController.text.toString();
                                  }
                                }
                                verifyEmailOtp();
                              },
                            )),
                        new Container(
                          margin:
                          EdgeInsets.fromLTRB(10, 0, 10, 10),
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          child: new Column(
                            children: [
                              new Container(
                                margin: EdgeInsets.only(
                                    bottom: 10, top: 10),
                                child: Text(
                                    "Didn't received the OTP?",
                                    style: TextStyle(
                                        fontFamily:
                                        AppConstants.textBold,
                                        color: Colors.grey,
                                        fontWeight:
                                        FontWeight.normal,
                                        fontSize: 14)),
                              ),
                              new Container(
                                child:CustomTimer(
                                  controller: _eController,
                                  from: Duration(seconds: 60),
                                  to: Duration(seconds: 0),
                                  onBuildAction: CustomTimerAction.auto_start,
                                  builder: (CustomTimerRemainingTime remaining) {
                                    return new GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      child: Text(
                                        remaining.seconds=='00'?"Resend OTP ?":"${remaining.minutes}:${remaining.seconds}",
                                        style: TextStyle(
                                            fontFamily:
                                            AppConstants.textBold,
                                            color: Themecolor,
                                            decoration: remaining.seconds=='00'?TextDecoration.underline:TextDecoration.none,
                                            fontWeight:
                                            FontWeight.bold,
                                            fontSize: 14),
                                      ),
                                      onTap: (){
                                        if(remaining.seconds=='00'){
                                          verifyEmailStatus();
                                        }
                                      },
                                    );
                                  },
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ):new Container()
            ],
          ),
        ),
      ),
    );
  }

  Widget _otpTextField(BuildContext context, bool autoFocus,
      TextEditingController otpController) {
    return Container(
      padding: EdgeInsets.all(0),
      height: 40,
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
        shape: BoxShape.rectangle,
      ),
      child: AspectRatio(
        aspectRatio: 1,
        child: TextField(
          controller: otpController,
          autofocus: autoFocus,
          decoration: InputDecoration(
            enabledBorder: const OutlineInputBorder(
              // width: 0.0 produces a thin "hairline" border
              borderSide:
              const BorderSide(color: Colors.black, width: 0.0),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide:
              const BorderSide(color: Colors.black, width: 0.0),
            ),
            contentPadding: EdgeInsets.only(bottom: 10.0),
          ),
          textAlign: TextAlign.center,
          keyboardType: TextInputType.number,
          style: TextStyle(),
          maxLines: 1,
          onChanged: (value) {
            if (value.length == 1) {
              FocusScope.of(context).nextFocus();
            } else {
              FocusScope.of(context).previousFocus();
            }
          },
        ),
      ),
    );
  }

  void otpListener(){
    _otpInteractor = OTPInteractor();
    _otpInteractor
        .getAppSignature()
    //ignore: avoid_print
        .then((value) => print('signature - $value'));
    OTPTextEditController(
      codeLength: 6,
      //ignore: avoid_print
      onCodeReceive: (code) => {
        for (int i = 0; i < 6; i++) {
          mobileOtpFields[i].otpController.text=code.substring(i,i+1),
          mOtp = mOtp + code.substring(i,i+1),
          FocusScope.of(context).unfocus()
        },
        verifyMobileOtp()
      },
      otpInteractor: _otpInteractor,
    )..startListenUserConsent(
          (code) {
        final exp = RegExp(r'(\d{6})');
        return exp.stringMatch(code ?? '') ?? '';
      },
    );
  }

  void resendOTP() async {

    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(mobile:mobileController.text.toString(),user_id: userId,type: '2', verification_type:"mobile",fcmToken: '');
    final client = ApiClient(AppRepository.dio);
    GeneralResponse loginResponse = await client.sendOTPNew(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      otpListener();
      _mController.reset();
      _mController.start();
      MethodUtils.showSuccess(context, loginResponse.message!);
    }
    MethodUtils.showError(context, loginResponse.message!);
  }
  void verifyEmailStatus() async {

    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(email:emailController.text.toString(),user_id: userId);
    final client = ApiClient(AppRepository.dio);
    GeneralResponse loginResponse = await client.verifyEmailStatus(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, 0);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_EMAIL, emailController.text.toString());
      widget.type='none';
      widget.otp='email';
      widget.emailViewRefresh(emailController.text.toString(),0);
      setState(() {});
      _eController.reset()!;
      _eController.start()!;
      MethodUtils.showSuccess(context, loginResponse.message!);
    }
    MethodUtils.showError(context, loginResponse.message!);
  }
  void verifyEmailOtp() async {

    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(email:emailController.text.toString(),user_id: userId,otp: mOtp);
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse = await client.verifyEmailOtp(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      Navigator.pop(context);
      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, loginResponse.result!.email_verify);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_EMAIL, loginResponse.result!.email);
      MethodUtils.showSuccess(context, loginResponse.message!);
      widget.emailViewRefresh(loginResponse.result!.email,loginResponse.result!.email_verify);
    }
    MethodUtils.showError(context, loginResponse.message!);
  }

  void verifyMobile() async {

    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(mobile:mobileController.text.toString(),user_id: userId,otp: mOtp,type: '2',verification_type: 'mobile');
    final client = ApiClient(AppRepository.dio);
    GeneralResponse loginResponse = await client.sendOTPNew(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {

      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 0);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE, mobileController.text.toString());
      MethodUtils.showSuccess(context, loginResponse.message!);
      widget.type='none';
      widget.otp='mobile';
      setState(() {});
      _eController.reset()!;
      _eController.start()!;
    }
    MethodUtils.showError(context, loginResponse.message!);
  }

  void verifyMobileOtp() async {

    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(mobile:mobileController.text.toString(),user_id: userId,otp: mOtp,type: 'mobile');
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse = await client.verifyOtpNew(loginRequest);
    AppLoaderProgress.hideLoader(context);

    if (loginResponse.status == 1) {
      Navigator.pop(context);
      AppPrefrence.putBoolean(AppConstants.SHARED_PREFERENCES_IS_LOGGED_IN, true);
      AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, loginResponse.result!.mobile_verify);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE, loginResponse.result!.mobile.toString());
      MethodUtils.showSuccess(context, loginResponse.message!);
      navigateToHomePage(context);
    }
    MethodUtils.showError(context, loginResponse.message!);
  }
}