import 'dart:convert';
import 'dart:io';

import 'package:built_value/json_object.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/bank_details_response.dart';
import 'package:EverPlay/repository/model/bank_verification_request.dart';
import 'package:EverPlay/repository/model/bank_verify_response.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/pan_details_response.dart';
import 'package:EverPlay/repository/model/pan_verification_request.dart';
import 'package:EverPlay/repository/model/verify_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:EverPlay/repository/retrofit/apis.dart';

import '../repository/model/base_response.dart';

class VerifyAccount extends StatefulWidget {
  @override
  _VerifyAccountState createState() => _VerifyAccountState();
}

class _VerifyAccountState extends State<VerifyAccount> {
  TextEditingController bNameController = TextEditingController();
  TextEditingController mobileController = TextEditingController();
  TextEditingController pNameController = TextEditingController();
  TextEditingController panController = TextEditingController();
  TextEditingController confPanController = TextEditingController();
  TextEditingController accNumController = TextEditingController();
  TextEditingController confAccNumController = TextEditingController();
  TextEditingController ifscController = TextEditingController();
  TextEditingController bankNameController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  int _currentMatchIndex = 0;
  int _tabCount = 1;
  bool isLoading = false;
  bool isPanLoading = false;
  bool isBankLoading = false;
  bool panImageUpload = false;
  var title = 'Home';
  bool back_dialog = false;
  List<Widget> tabs = <Widget>[];
  late String userId = '0';
  late String userEmail = '';
  late String userMobile = '';
  late String panImage = '';
  late String bankImage = '';
  late int emailStatus = 0;
  late int mobileStatus = 0;
  AllVerifyItem allVerifyItem = new AllVerifyItem();
  PanDetailItem panDetailItem = new PanDetailItem();
  BankDetailItem bankDetailItem = new BankDetailItem();
  ImagePicker _imagePicker = new ImagePicker();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_EMAIL)
        .then((value) => {
              setState(() {
                userEmail = value;
              })
            });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_MOBILE)
        .then((value) => {
              setState(() {
                userMobile = value;
              })
            });
    AppPrefrence.getInt(
            AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, 0)
        .then((value) => {
              setState(() {
                emailStatus = value;
              })
            });
    AppPrefrence.getInt(
            AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 0)
        .then((value) => {
              setState(() {
                mobileStatus = value;
              })
            });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) => {
              setState(() {
                userId = value;
                allVerify();
              })
            });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
            statusBarColor: primaryColor,
            /* set Status bar color in Android devices. */

            statusBarIconBrightness: Brightness.light,
            /* set Status bar icons color in Android devices.*/

            statusBarBrightness:
                Brightness.light) /* set Status bar icon color in iOS. */
        );

    // TODO: implement build
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          body: new WillPopScope(
              child: new Stack(
                children: [
                  new Scaffold(
                    backgroundColor: Colors.white,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(100), // Set this height
                      child: new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          new Container(
                            height: 50,
                            color: Colors.black,
                            // margin: EdgeInsets.only(top: 40),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: () => {Navigator.pop(context)},
                                  child: new Container(
                                    padding: EdgeInsets.all(15),
                                    alignment: Alignment.centerLeft,
                                    child: new Container(
                                      width: 16,
                                      height: 16,
                                      child: Image(
                                        image:
                                            AssetImage(AppImages.backImageURL),
                                        fit: BoxFit.fill,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                                new Container(
                                  child: new Text('Verify Account',
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 18)),
                                ),
                              ],
                            ),
                          ),
                          !isLoading
                              ? new Container(
                                  height: 50,
                                  alignment: Alignment.bottomCenter,
                                  child: new Container(
                                    height: 40,
                                    child: new DefaultTabController(
                                        length: 3,
                                        child: new Container(
                                          child: TabBar(
                                            labelPadding: EdgeInsets.all(0),
                                            onTap: (int index) {
                                              setState(() {
                                                _currentMatchIndex = index;
                                                if (_currentMatchIndex == 1) {
                                                  getPainDetail();
                                                } else if (_currentMatchIndex ==
                                                    2) {
                                                  getBankDetail();
                                                }
                                              });
                                            },
                                            indicatorWeight: 4,
                                            indicatorSize:
                                                TabBarIndicatorSize.label,
                                            indicator: ShapeDecoration(
                                                shape: RoundedRectangleBorder(
                                                    borderRadius:
                                                        BorderRadius.only(
                                                            topRight: Radius
                                                                .circular(10),
                                                            topLeft:
                                                                Radius.circular(
                                                                    10))),
                                                color: Themecolor),
                                            isScrollable: true,
                                            tabs: getTabs(),
                                          ),
                                        )),
                                  ),
                                )
                              : new Container()
                        ],
                      ),
                    ),
                    body: new Container(
                      color: Colors.white,
                      height: MediaQuery.of(context).size.height,
                      child: new Stack(
                        children: [
                          new SingleChildScrollView(
                            child: new Container(
                              child: _currentMatchIndex == 0
                                  ? getEmailMobileView()
                                  : _currentMatchIndex == 1
                                      ? getPanVerificationView()
                                      : _currentMatchIndex == 2
                                          ? getBankVerificationView()
                                          : new Container(),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              onWillPop: _onWillPop)),
    );
  }

  List<Widget> getTabs() {
    tabs.clear();
    tabs.add(_tabCount <= 3 ? getMobileTab() : new Container());
    tabs.add(_tabCount > 1 ? getPanTab() : new Container());
    tabs.add(_tabCount > 2 ? getBankTab() : new Container());
    return tabs;
  }

  int getCount() {
    // return 3;
    if ((allVerifyItem.mobile_verify == 1) &&
        (allVerifyItem.email_verify == 1) &&
        (allVerifyItem.pan_verify == 1 || allVerifyItem.pan_verify == 0)) {
      return 3;
    } else if ((allVerifyItem.mobile_verify == 1) &&
        (allVerifyItem.email_verify == 1)) {
      return 2;
    } else {
      return 1;
    }
  }

  Widget getMobileTab() {
    return new ClipRRect(
      borderRadius: BorderRadius.horizontal(left: Radius.circular(0)),
      child: new Container(
        height: 45,
        alignment: Alignment.center,
        padding: EdgeInsets.only(left: 10, right: 10),
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Text(
          'EMAIL & MOBILE',
          style: TextStyle(
              fontFamily: AppConstants.textBold,
              color: _currentMatchIndex == 0 ? primaryColor : Colors.grey,
              fontWeight: FontWeight.w600,
              fontSize: 14),
        ),
      ),
    );
  }

  Widget getPanTab() {
    return new Container(
      height: 45,
      alignment: Alignment.center,
      padding: EdgeInsets.only(left: 30, right: 30),
      margin: EdgeInsets.all(0),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border(
            right: BorderSide(
              color: Colors.white,
              width: 0,
              style: BorderStyle.solid,
            )),
      ),
      child: Text(
        'PAN CARD',
        style: TextStyle(
            fontFamily: AppConstants.textBold,
            color: _currentMatchIndex == 1 ? primaryColor : Colors.grey,
            fontWeight: FontWeight.w600,
            fontSize: 14),
      ),
    );
  }

  Widget getBankTab() {
    return new Container(
      height: 45,
      padding: EdgeInsets.only(left: 30, right: 30),
      alignment: Alignment.center,
      decoration: BoxDecoration(
        border: Border(
            right: BorderSide(
          color: Colors.white,
          width: 0,
          style: BorderStyle.solid,
        )),
        color: Colors.white,
      ),
      child: Text(
        'BANK',
        style: TextStyle(
            fontFamily: AppConstants.textBold,
            color: _currentMatchIndex == 2 ? primaryColor : Colors.grey,
            fontWeight: FontWeight.w600,
            fontSize: 14),
      ),
    );
  }

  Widget getEmailMobileView() {
    return new Container(
      color: Colors.white,
      child: new Column(
        children: [
          emailStatus != 1
              ? new Container(
                  margin:
                      EdgeInsets.only(left: 15, right: 15, bottom: 15, top: 10),
                  decoration: BoxDecoration(
                    color: Color(0xffF8F6F4),
                    border: Border.all(color: Color(0xffD9D9D9), width: 1),
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: new Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(5),
                    child: new Column(
                      children: [
                        new Container(
                          margin: EdgeInsets.only(top: 20),
                          height: 70,
                          child: Image(
                              image:
                                  AssetImage(AppImages.verificationEmailIcon),
                              fit: BoxFit.fill),
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Text(
                            'Email Verification',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 15, left: 16, right: 16),
                          child: new Text(
                            "Click on the verification link in the mail we sent you to verify your email.",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 14,
                              color: Colors.grey,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 10),
                          child: new Text(
                            userEmail.toString(),
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 14,
                              color: Themecolor,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Divider(color: Colors.grey),
                        new Container(
                            width: MediaQuery.of(context).size.width,
                            height: 50,
                            margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            child: ElevatedButton(
                              onPressed: () {
                                verifyEmailStatus();
                              },
                              style: ElevatedButton.styleFrom(
                                primary: primaryColor,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(40),
                                ),
                              ),
                              child: Text(
                                'Verify',
                                style: TextStyle(
                                    fontSize: 15, color: Colors.white),
                              ),
                            )),
                        Divider(color: Colors.grey),
                        new Container(
                          margin: EdgeInsets.only(
                              left: 20, top: 10, bottom: 10, right: 20),
                          child: new Text(
                            "In case you don't receive the email, be sure to check the Span/Junk folder on your mailbox.",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.grey,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              : getMobileVerified('Email'),
          // mobileStatus!=1?new Card(
          //   margin: EdgeInsets.only(left: 15,right: 15,bottom: 15,top: 10),
          //   shape: RoundedRectangleBorder(
          //     borderRadius: BorderRadius.circular(10.0),
          //   ),
          //   child: new Container(
          //     width: MediaQuery.of(context).size.width,
          //     padding: EdgeInsets.all(5),
          //     child: new Column(
          //       children: [
          //         new Container(
          //           margin: EdgeInsets.only(top: 20),
          //           width: 70,
          //           height: 70,
          //           child: Image(
          //               image: AssetImage(AppImages.verifiedEmailIcon),
          //               fit: BoxFit.fill),
          //         ),
          //         new Container(
          //           margin: EdgeInsets.only(top: 20),
          //           child: Text(
          //             'Mobile Verification',
          //             textAlign: TextAlign.center,
          //             style: TextStyle(
          //               fontSize: 16,
          //               color: Colors.black,
          //               fontStyle: FontStyle.normal,
          //               fontWeight: FontWeight.bold,
          //             ),
          //           ),
          //         ),
          //         new Container(
          //           margin: EdgeInsets.only(top: 15),
          //           child: new Text(
          //             "Otp will be sent on Registered Mobile Number.",
          //             textAlign: TextAlign.center,
          //             style: TextStyle(
          //               fontSize: 12,
          //               color: Colors.grey,
          //               fontStyle: FontStyle.normal,
          //               fontWeight: FontWeight.normal,
          //             ),
          //           ),
          //         ),
          //         Container(
          //           margin: EdgeInsets.fromLTRB(30, 0, 30, 0),
          //           padding: EdgeInsets.all(10),
          //           child: TextField(
          //             controller: mobileController,
          //             keyboardType: TextInputType.number,
          //             maxLength: 10,
          //             decoration: InputDecoration(
          //               counterText: "",
          //               fillColor: Colors.white,
          //               filled: true,
          //               prefixIcon: SizedBox(
          //                 child: new Container(
          //                   height: 60,
          //                   width: 30,
          //                   margin: EdgeInsets.only(right: 10),
          //                   color: bgColor,
          //                   child: Center(
          //                     widthFactor: 0.0,
          //                     child: Text('+91',
          //                         style: TextStyle(
          //                           fontSize: 17,
          //                           color: Colors.black,
          //                           fontStyle:
          //                           FontStyle.normal,
          //                           fontWeight:
          //                           FontWeight.bold,
          //                         )),
          //                   ),
          //                 ),
          //               ),
          //               hintStyle: TextStyle(
          //                 fontSize: 17,
          //                 color: Colors.grey,
          //                 fontStyle: FontStyle.normal,
          //                 fontWeight: FontWeight.bold,
          //               ),
          //               hintText: '',
          //               enabledBorder: OutlineInputBorder(
          //                 borderSide: BorderSide(color: bgColor, width: 1),
          //               ),
          //               focusedBorder: OutlineInputBorder(
          //                 borderSide: BorderSide(color: bgColor, width: 1),
          //               ),
          //             ),
          //           ),
          //         ),
          //
          //         new Container(
          //             width: MediaQuery.of(context).size.width,
          //             height: 50,
          //             margin: EdgeInsets.fromLTRB(10, 30, 10, 10),
          //             child: RaisedButton(
          //               textColor: Colors.white,
          //               color: Colors.red,
          //               child: Text(
          //                 'Send OTP',
          //                 style: TextStyle(fontSize: 15),
          //               ),
          //               shape: RoundedRectangleBorder(
          //                   borderRadius:
          //                   BorderRadius.circular(5)),
          //               onPressed: () {
          //                 if(mobileController.text.isEmpty||mobileController.text.length<10){
          //                   MethodUtils.showError(context, "Enter a valid mobile number.");
          //                   return;
          //                 }
          //                  // navigateToVerifyEmailOrMobile(context,type: 'mobile',mobileViewRefresh:mobileViewRefresh);
          //               },
          //             )),
          //       ],
          //     ),
          //   ),
          // ):getMobileVerified('Mobile'),
          //  getMobileVerified('Mobile')
          getMobileVerified('Mobile')
        ],
      ),
    );
  }

  void mobileViewRefresh(String mobile, int status) {
    setState(() {
      userMobile = mobile;
      mobileStatus = status;
    });
  }

  Widget getMobileVerified(type) {
    return new Container(
      child: new Column(
        children: [
          new Container(
            decoration: BoxDecoration(
              color: Color(0xffF8F6F4),
              border: Border.all(color: Color(0xffD9D9D9), width: 1),
              borderRadius: BorderRadius.circular(10.0),
            ),
            margin: EdgeInsets.only(left: 15, right: 15, bottom: 15, top: 10),
            child: new Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(5),
              child: new Stack(
                alignment: Alignment.topRight,
                children: [
                  type == 'Email'
                      ? new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: new Container(
                            margin: EdgeInsets.only(right: 10, top: 10),
                            height: 18,
                            width: 18,
                            // child: Image(
                            //   image: AssetImage(
                            //     AppImages.teamEditIcon,
                            //   ),
                            //   color: Colors.grey,
                            // ),
                          ),
                          onTap: () {
                            navigateToVerifyEmailOrMobile(
                                context, 'email', emailViewRefresh);
                          },
                        )
                      : new Container(),
                  new Container(
                    width: MediaQuery.of(context).size.width,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        new Container(
                          margin: EdgeInsets.only(top: 20),
                          height: 70,
                          child: Image(
                              image: AssetImage(type == 'Mobile' ?AppImages.verifiedMobileIcon : type == 'Email' ?AppImages.verifiedEmailIcon : type == 'Pan' ?AppImages.verifiedPanIcon:AppImages.verifiedBankIcon),
                              fit: BoxFit.fill),
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 20),
                          child: Text(
                            type == 'Mobile'
                                ? 'Mobile Number Verified'
                                : type == 'Email'
                                    ? 'Email Address Verified'
                                    : type == 'Pan'
                                        ? panDetailItem.status == 0
                                            ? 'Your PAN details are sent for verification'
                                            : 'PAN Card Verified'
                                        : panDetailItem.status == 0
                                            ? 'Your Bank details are sent for verification'
                                            : 'Your Bank details is verified',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 15, bottom: 20),
                          child: new Text(
                            type == 'Mobile'
                                ? userMobile
                                : type == 'Email'
                                    ? userEmail
                                    : type == 'Pan'
                                        ? panDetailItem.status == 0
                                            ? 'Under Review'
                                            : panDetailItem.pannumber!
                                        : bankDetailItem.status == 0
                                            ? 'Under Review'
                                            : bankDetailItem.accno.toString(),

                            //bankDetailItem.accno ?? '',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 14,
                              color: primaryColor,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getPanVerificationView() {
    return new Container(
      child: new Column(
        children: [
          panDetailItem.status == 2
              ? new Card(
                  margin:
                      EdgeInsets.only(left: 15, right: 15, bottom: 15, top: 10),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  color: primaryColor,
                  child: new Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(5),
                    child: new Container(
                      alignment: Alignment.centerLeft,
                      child: new Html(
                        data: "<strong>Verification Reject - </strong>" +
                            (panDetailItem.comment ?? ''),
                        style: {
                          'html': Style(
                              textAlign: TextAlign.start,
                              fontSize: FontSize.medium,
                              color: Colors.white,
                              textDecoration: TextDecoration.none)
                        },
                      ),
                    ),
                  ),
                )
              : new Container(),
          panDetailItem.status == 2 || panDetailItem.status == -1
              ? new Container(
                  width: MediaQuery.of(context).size.width,
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.all(5),
                        margin: EdgeInsets.only(
                            left: 15, right: 15, bottom: 15, top: 10),
                        child: Text(
                          'ENTER YOUR PAN Card DETAILS',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 14,
                            color: Colors.black,
                            fontStyle: FontStyle.normal,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                              height: 130,
                              width: MediaQuery.of(context).size.width,
                              child: ElevatedButton(
                                onPressed: () async {
                                  panImageUpload = true;

                                  if (await Permission.camera
                                      .request()
                                      .isGranted) {
                                    if (await Permission.mediaLibrary
                                        .request()
                                        .isGranted) {
                                      showOptionsDialog(context);
                                    } else {
                                      Fluttertoast.showToast(
                                          msg:
                                              "Storage Permission is not granted");
                                    }
                                  } else {
                                    Fluttertoast.showToast(
                                        msg:
                                            "Camera Permission is not granted");
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                  primary: backgroundColor,
                                  elevation: 0,
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Image(
                                      image: AssetImage(
                                          AppImages.UploadDocVerifyIcon),
                                      height: 50,
                                      width: 50,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(
                                        'Upload PAN Card Image',
                                        style: TextStyle(
                                            color: textGrey,
                                            fontSize: 15,
                                            fontWeight: FontWeight.w400),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                        ],
                      ),
                      Container(
                          padding: EdgeInsets.all(5),
                          margin:
                              EdgeInsets.only(left: 5, right: 5, bottom: 15),
                          child: Column(
                            children: [
                              new Container(
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(left: 10, top: 20),
                                child: new Row(
                                  children: [
                                    Text(
                                      'Full Name',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: textGrey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.only(left: 5),
                                      child: Text(
                                        '(Same as on PAN Card)',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.all(10),
                                height: 45,
                                child: TextField(
                                  controller: pNameController,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  decoration: InputDecoration(
                                    counterText: "",
                                    fillColor: Colors.white,
                                    filled: true,
                                    hintStyle: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                    hintText: 'Enter Your Full Name',
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      // width: 0.0 produces a thin "hairline" border
                                      borderSide: BorderSide(
                                          color: TextFieldBorderColor,
                                          width: 1.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          color: TextFieldBorderColor,
                                          width: 1.0),
                                    ),
                                  ),
                                ),
                              ),
                              new Container(
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(left: 10, top: 4),
                                child: new Row(
                                  children: [
                                    Text(
                                      'PAN Card Number',
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: textGrey,
                                        fontStyle: FontStyle.normal,
                                        fontWeight: FontWeight.w500,
                                      ),
                                    ),
                                    new Container(
                                      margin: EdgeInsets.only(left: 5),
                                      child: Text(
                                        '(10 Digits)',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                          fontSize: 12,
                                          color: Colors.grey,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              /*   new Container(
                      alignment: Alignment.centerLeft,
                      padding: EdgeInsets.only(left: 10),
                      child: Text(
                        'PAN Number',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.black,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),

                    new Container(
                      margin: EdgeInsets.only(left: 5),
                      child: Text(
                        '(PAN Number (10 Digit PAN no.))',
                        textAlign: TextAlign.start,
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.grey,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ),*/

                              new Container(
                                margin: EdgeInsets.all(10),
                                height: 45,
                                child: TextField(
                                  maxLength: 10,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  controller: panController,
                                  decoration: InputDecoration(
                                    counterText: "",
                                    fillColor: TextFieldColor,
                                    filled: true,
                                    hintStyle: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                    hintText: 'PAN Number',
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      // width: 0.0 produces a thin "hairline" border
                                      borderSide: BorderSide(
                                          color: TextFieldColor, width: 1.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          color: TextFieldColor, width: 1.0),
                                    ),
                                  ),
                                ),
                              ),
                              new Container(
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(left: 10),
                                child: Text(
                                  'Confirm PAN Number',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: textGrey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              new Container(
                                margin: EdgeInsets.all(10),
                                height: 45,
                                child: TextField(
                                  maxLength: 10,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  controller: confPanController,
                                  decoration: InputDecoration(
                                    counterText: "",
                                    fillColor: TextFieldColor,
                                    filled: true,
                                    hintStyle: TextStyle(
                                      fontSize: 12,
                                      color: Colors.grey,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                    hintText: 'Confirm PAN Number',
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      // width: 0.0 produces a thin "hairline" border
                                      borderSide: BorderSide(
                                          color: TextFieldColor, width: 0.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          color: TextFieldColor, width: 0.0),
                                    ),
                                  ),
                                ),
                              ),
                              new Container(
                                alignment: Alignment.centerLeft,
                                padding: EdgeInsets.only(left: 10),
                                child: Text(
                                  'Date of Birth',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: textGrey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new IgnorePointer(
                                    child: new Container(
                                      margin: EdgeInsets.all(10),
                                      height: 45,
                                      child: TextField(
                                        readOnly: true,
                                        controller: dobController,
                                        decoration: InputDecoration(
                                          counterText: "",
                                          fillColor: TextFieldColor,
                                          filled: true,
                                          hintStyle: TextStyle(
                                            fontSize: 12,
                                            color: Colors.grey,
                                            fontStyle: FontStyle.normal,
                                            fontWeight: FontWeight.w500,
                                          ),
                                          hintText: 'Date of Birth',
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            // width: 0.0 produces a thin "hairline" border
                                            borderSide: BorderSide(
                                                color: TextFieldColor,
                                                width: 0.0),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                color: TextFieldColor,
                                                width: 0.0),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  onTap: () async {
                                    {
                                      FocusScope.of(context)
                                          .requestFocus(new FocusNode());
                                      await showDatePicker(
                                              context: context,
                                              initialDate: DateTime(
                                                  DateTime.now().year - 18,
                                                  DateTime.now().month,
                                                  DateTime.now().day),
                                              firstDate: DateTime(1900),
                                              lastDate: DateTime(
                                                  DateTime.now().year - 18,
                                                  DateTime.now().month,
                                                  DateTime.now().day))
                                          .then((value) => {
                                                dobController.text =
                                                    DateFormat("dd/MM/yyyy")
                                                        .format(value!)
                                              });
                                    }
                                  }),
                              Container(
                                padding: EdgeInsets.only(left: 10),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  '* All Fields Are Mandatory',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                    fontSize: 14,
                                    color: Themecolor,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              new Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 45,
                                  margin: EdgeInsets.fromLTRB(40, 20, 40, 10),
                                  child: ElevatedButton(
                                    onPressed: () {
                                      if (pNameController.text.length < 4) {
                                        MethodUtils.showError(context,
                                            'Please enter a valid name.');
                                      } else if (panController.text.length !=
                                              10 ||
                                          !RegExp("(([A-Za-z]{5})([0-9]{4})([a-zA-Z]))")
                                              .hasMatch(panController.text)) {
                                        MethodUtils.showError(context,
                                            'Please enter a valid PAN Number.');
                                      } else if (panController.text !=
                                          confPanController.text) {
                                        MethodUtils.showError(context,
                                            'PAN and Confirm PAN Number not matched.');
                                      } else if (panImage.isEmpty) {
                                        MethodUtils.showError(
                                            context, 'Upload PAN Card Image.');
                                      } else if (dobController.text.isEmpty) {
                                        Fluttertoast.showToast(
                                          msg: 'Please select date of birth.',
                                          toastLength: Toast.LENGTH_SHORT,
                                          timeInSecForIosWeb: 1,
                                        );
                                        return;
                                      } else {
                                        verifyPanDetail();
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: Themecolor,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40),
                                      ),
                                    ),
                                    child: Text(
                                      'Submit for Verification',
                                      style: TextStyle(fontSize: 15),
                                    ),
                                  )),
                            ],
                          ))
                    ],
                  ),
                )
              : new Container(),
          panDetailItem.status == 0 || panDetailItem.status == 1
              ? getMobileVerified('Pan')
              : new Container()
        ],
      ),
    );
  }

  Widget getBankVerificationView() {
    return new Container(
      child: new Column(
        children: [
          bankDetailItem.status == 2
              ? new Card(
                  margin:
                      EdgeInsets.only(left: 15, right: 15, bottom: 15, top: 10),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  color: primaryColor,
                  child: new Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.all(5),
                    child: new Container(
                      alignment: Alignment.centerLeft,
                      child: new Html(
                        data: "<strong>Verification Reject - </strong>" +
                            (bankDetailItem.comment ?? ''),
                        style: {
                          'html': Style(
                              textAlign: TextAlign.start,
                              fontSize: FontSize.medium,
                              color: Colors.white,
                              textDecoration: TextDecoration.none)
                        },
                      ),
                    ),
                  ),
                )
              : new Container(),
          bankDetailItem.status == 2 || bankDetailItem.status == -1
              ? new Container(
                  child: new Container(
                    width: MediaQuery.of(context).size.width,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        new Container(
                          alignment: Alignment.centerLeft,
                          padding: EdgeInsets.all(5),
                          margin: EdgeInsets.only(
                              left: 15, right: 15, bottom: 15, top: 10),
                          child: Text(
                            'Bank Account Verification',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                              fontStyle: FontStyle.normal,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        new Container(
                            height: 120,
                            width: MediaQuery.of(context).size.width,
                            child: ElevatedButton(
                              onPressed: () async {
                                panImageUpload = false;
                                if (await Permission.camera
                                    .request()
                                    .isGranted) {
                                  if (await Permission.mediaLibrary
                                      .request()
                                      .isGranted) {
                                    showOptionsDialog(context);
                                  } else {
                                    Fluttertoast.showToast(
                                        msg:
                                        "Storage Permission is not granted");
                                  }
                                } else {
                                  Fluttertoast.showToast(
                                      msg:
                                      "Camera Permission is not granted");
                                }
                              },
                              style: ElevatedButton.styleFrom(
                                primary: backgroundColor,
                                elevation: 0,
                              ),
                              child: Column(
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image(
                                    image: AssetImage(
                                        AppImages.UploadDocVerifyIcon),
                                    height: 50,
                                    width: 50,
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      'Upload Bank Passbook /Cheque Image',
                                      style: TextStyle(
                                          color: textGrey,
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                ],
                              ),
                            )),
                        Container(
                          padding: EdgeInsets.all(5),
                          margin:
                              EdgeInsets.only(left: 5, right: 5, bottom: 15),
                          child: Column(children: [

                            new Container(
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(left: 10, top: 20),
                              child: new Row(
                                children: [
                                  Text(
                                    'Account Holder Name',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontSize: 14,
                                      color: textGrey,
                                      fontStyle: FontStyle.normal,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.all(10),
                              height: 45,
                              child: TextField(
                                controller: bNameController,
                                decoration: InputDecoration(
                                  counterText: "",
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintStyle: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  hintText: 'Account Holder Name',
                                  enabledBorder: OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        color: bordercolor, width: 1.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        color: bordercolor, width: 1.0),
                                  ),
                                ),
                              ),
                            ),
                            new Container(
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(left: 10),
                              child: Text(
                                'Account Number',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: textGrey,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.all(10),
                              height: 45,
                              child: TextField(
                                controller: accNumController,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  counterText: "",
                                  fillColor: TextFieldColor,
                                  filled: true,
                                  hintStyle: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  hintText: 'Account Number',
                                  enabledBorder: OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        color: TextFieldColor, width: 1.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        color: TextFieldColor, width: 1.0),
                                  ),
                                ),
                              ),
                            ),
                            new Container(
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(left: 10),
                              child: Text(
                                'Confirm Account Number',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: textGrey,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.all(10),
                              height: 45,
                              child: TextField(
                                controller: confAccNumController,
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  counterText: "",
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintStyle: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  hintText: 'Confirm Account Number',
                                  enabledBorder: OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        color: bordercolor, width: 1.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        color: bordercolor, width: 1.0),
                                  ),
                                ),
                              ),
                            ),
                            new Container(
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(left: 10),
                              child: Text(
                                'IFSC Code',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: textGrey,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.all(10),
                              height: 45,
                              child: TextField(
                                controller: ifscController,
                                decoration: InputDecoration(
                                  counterText: "",
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintStyle: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  hintText: 'IFSC Code',
                                  enabledBorder: OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        color: bordercolor, width: 1.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        color: bordercolor, width: 1.0),
                                  ),
                                ),
                              ),
                            ),
                            new Container(
                              alignment: Alignment.centerLeft,
                              padding: EdgeInsets.only(left: 10),
                              child: Text(
                                'Bank Name',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                  fontSize: 14,
                                  color: textGrey,
                                  fontStyle: FontStyle.normal,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.all(10),
                              height: 45,
                              child: TextField(
                                controller: bankNameController,
                                decoration: InputDecoration(
                                  counterText: "",
                                  fillColor: Colors.white,
                                  filled: true,
                                  hintStyle: TextStyle(
                                    fontSize: 12,
                                    color: Colors.grey,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  hintText: 'Bank Name',
                                  enabledBorder: OutlineInputBorder(
                                    // width: 0.0 produces a thin "hairline" border
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        color: bordercolor, width: 1.0),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    borderSide: BorderSide(
                                        color: bordercolor, width: 1.0),
                                  ),
                                ),
                              ),
                            ),
                            new Container(
                                width: MediaQuery.of(context).size.width,
                                height: 50,
                                margin: EdgeInsets.fromLTRB(40, 10, 40, 10),
                                child: ElevatedButton(
                                  onPressed: () {
                                    if (bNameController.text.length < 1) {
                                      MethodUtils.showError(context,
                                          "Please enter valid account holder name.");
                                    } else if (accNumController.text.length <
                                        1) {
                                      MethodUtils.showError(context,
                                          "Please enter valid account number.");
                                    } else if (confAccNumController
                                            .text.length <
                                        1) {
                                      MethodUtils.showError(context,
                                          "Please enter valid confirm account number.");
                                    } else if (accNumController.text !=
                                        confAccNumController.text) {
                                      MethodUtils.showError(context,
                                          "Your account number and verify account number not matched.");
                                    } else if (ifscController.text.length !=
                                            11 ||
                                        !isIfscCodeValid()) {
                                      MethodUtils.showError(context,
                                          "Please enter valid IFSC Code.");
                                    } else if (bankNameController.text.length <
                                        1) {
                                      MethodUtils.showError(context,
                                          "Please enter valid bank name.");
                                    } else if (bankImage.isEmpty) {
                                      MethodUtils.showError(context,
                                          "Upload Bank Passbook /Cheque Image");
                                    } else {
                                      verifyBankDetail();
                                    }
                                  },
                                  style: ElevatedButton.styleFrom(
                                    primary: Themecolor,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(40),
                                    ),
                                  ),
                                  child: Text(
                                    'Submit for Verification',
                                    style: TextStyle(fontSize: 15),
                                  ),
                                )),
                          ]),
                        )
                      ],
                    ),
                  ),
                )
              : new Container(),
          bankDetailItem.status == 0 || bankDetailItem.status == 1
              ? getMobileVerified('Bank')
              : new Container()
        ],
      ),
    );
  }

  Future<bool> _onWillPop() async {
    Navigator.pop(context);
    return Future.value(false);
  }

  void allVerify() async {
    isLoading = false;
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    AllVerifyResponse referBonusListResponse =
        await client.allVerify(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (referBonusListResponse.status == 1) {
      allVerifyItem = referBonusListResponse.result!;
      emailStatus = allVerifyItem.email_verify!;
      mobileStatus = allVerifyItem.mobile_verify!;
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS, 1);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS, 1);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS, 1);
      AppPrefrence.putInt(
          AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS, 1);
      _tabCount = getCount();
      isLoading = false;
      setState(() {});
    }
  }

  void getPainDetail() async {
    isPanLoading = true;
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    PanDetailsResponse referBonusListResponse =
        await client.getPainDetail(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (referBonusListResponse.status == 1) {
      panDetailItem = referBonusListResponse.result!;
      pNameController.text = panDetailItem.panname!;
      panController.text = panDetailItem.pannumber!;
      confPanController.text = panDetailItem.pannumber!;
      isPanLoading = false;
      setState(() {});
    }
  }

  void verifyEmailStatus() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest =
        new GeneralRequest(email: userEmail, user_id: userId);
    final client = ApiClient(AppRepository.dio);
    GeneralResponse loginResponse = await client.sendNewEmail(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      MethodUtils.showSuccess(context, loginResponse.message!);
    }

    MethodUtils.showError(context, loginResponse.message!);
  }

  void getBankDetail() async {
    isBankLoading = true;
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    BankDetailResponse referBonusListResponse =
        await client.getBankDetails(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (referBonusListResponse.status == 1) {
      bankDetailItem = referBonusListResponse.result![0];
      bNameController.text = bankDetailItem.ac_holder_name ?? '';
      accNumController.text = bankDetailItem.accno ?? '';
      confAccNumController.text = bankDetailItem.accno ?? '';
      ifscController.text = bankDetailItem.ifsc ?? '';
      bankNameController.text = bankDetailItem.bankname ?? '';
      isBankLoading = false;
      setState(() {});
    }
  }

  void verifyBankDetail() async {
    AppLoaderProgress.showLoader(context);
    BankVerifyRequest loginRequest = new BankVerifyRequest(
        user_id: userId,
        image: bankImage,
        ifsc: ifscController.text.toString(),
        bankname: bankNameController.text.toString(),
        ac_holder_name: accNumController.text.toString(),
        accno: accNumController.text.toString());
    final client = ApiClient(AppRepository.dio);
    BankVerifyResponse referBonusListResponse =
        await client.verifyBankDetail(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (referBonusListResponse.status == 1) {
      if (referBonusListResponse.result!.status == 1) {
        MethodUtils.showSuccess(
            context, "Your Bank details are sent for verification.");
        bankDetailItem.status = 0;
        _tabCount = 3;
        setState(() {});
      } else {
        MethodUtils.showError(context, referBonusListResponse.message!);
      }
    } else {
      MethodUtils.showError(context, referBonusListResponse.message!);
    }
  }

  void verifyPanDetail() async {
    FocusScope.of(context).unfocus();
    AppLoaderProgress.showLoader(context);
    PanVerificationRequest loginRequest = new PanVerificationRequest(
        user_id: userId,
        pan_name: pNameController.text.toString(),
        pan_number: panController.text.toString(),
        image: panImage,
        pan_dob: dobController.text.toString());
    final client = ApiClient(AppRepository.dio);
    PanDetailsResponse referBonusListResponse =
        await client.verifyPanDetail(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (referBonusListResponse.result!.status == 1) {
      MethodUtils.showSuccess(context, referBonusListResponse.result!.message);
      panDetailItem = referBonusListResponse.result!;
      panDetailItem.status = 0;
      _tabCount = 3;
      setState(() {});
    } else {
      MethodUtils.showError(context, referBonusListResponse.message!);
    }
  }

  Future<void> showOptionsDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              "Select Profile Picture",
              style: TextStyle(color: Colors.black, fontSize: 16),
            ),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  GestureDetector(
                    child: Text(
                      "Take Photo",
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      _getImage(context, ImageSource.camera);
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  GestureDetector(
                    child: Text(
                      "Choose from Gallery",
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                      _getImage(context, ImageSource.gallery);
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  GestureDetector(
                    child: Text(
                      "Cancel",
                      style: TextStyle(color: Colors.black, fontSize: 16),
                    ),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  void _getImage(BuildContext context, ImageSource source) {
    FocusScope.of(context).unfocus();
    _imagePicker
        .pickImage(source: source, imageQuality: 70)
        .then((value) => {sendFile(File(value!.path))});
  }

  void sendFile(File file) async {
    AppLoaderProgress.showLoader(context);
    FormData formData = new FormData.fromMap({
      "user_id": userId,
      'file': await MultipartFile.fromFile(
        file.path,
        filename: file.path.split('/').last,
      )
    });
    Dio dio = new Dio();
    dio.options.headers["Content-Type"] = "multipart/form-data";
    var response = await dio
        .post(
          AppRepository.dio.options.baseUrl +
              (panImageUpload ? Apis.uploadPanImage : Apis.uploadBankImage),
          data: formData,
        )
        .catchError((e) => debugPrint(e.response.toString()));
    AppLoaderProgress.hideLoader(context);
    var jsonObject = json.decode(response.toString());
    debugPrint(AppRepository.dio.options.baseUrl +
        (panImageUpload ? Apis.uploadPanImage : Apis.uploadBankImage) +
        " // " +
        response.toString());
    if (jsonObject['status'] == 1) {
      if (jsonObject['result'][0]['status'] == 2) {
        MethodUtils.showError(context, 'Please upload less than 4MB file.');
      } else {
        if (panImageUpload) {
          panImage = jsonObject['result'][0]['image'];
        } else {
          bankImage = jsonObject['result'][0]['image'];
        }
        MethodUtils.showSuccess(context, jsonObject['message']);
        setState(() {});
      }
    }
  }

  void emailViewRefresh(String email, int status) {
    setState(() {
      userEmail = email;
      emailStatus = status;
    });
  }

  bool isIfscCodeValid() {
    bool isvalid = false;
    if (ifscController.text.length > 0) {
      isvalid =
          RegExp(r"^[A-Za-z]{4}0[A-Z0-9]{6}$").hasMatch(ifscController.text);
    }
    return isvalid;
  }
}
