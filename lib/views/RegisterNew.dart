import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/data.dart';
import 'package:EverPlay/repository/model/login_request.dart';
import 'package:EverPlay/repository/model/register_request.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:http/http.dart' as http;
import 'package:sign_in_with_apple/sign_in_with_apple.dart';

class RegisterNew extends StatefulWidget {
  @override
  _RegisterNewState createState() => _RegisterNewState();
}

class _RegisterNewState extends State<RegisterNew> {
  bool _passwordVisible = false;
  bool _confirmPasswordVisible = false;
  TextEditingController mobileController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController codeController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  String fcmToken = '';
  int version = 0;

  bool checkedValue = false;

  @override
  void initState() {
    super.initState();

    FirebaseMessaging _messaging = FirebaseMessaging.instance;
    _messaging.getToken().then((token) {
      fcmToken = token!;
    });
    deviceVersion();
  }

  void deviceVersion() async {
    if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;
      setState(() {
        version = 0;
        // version = int.parse(iosInfo.systemVersion.split('.')[0]);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/
        statusBarBrightness: Brightness.light));
    return Container(
      color: whiteColor,
      /* decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(AppImages.main_bg),
          fit: BoxFit.fill,
        ),
      ),*/
      child: SafeArea(
        child: new Scaffold(
            backgroundColor: whiteColor,
            body: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 16.0),
                      child: Text('Create Your Account',
                          style: TextStyle(
                              fontFamily: 'roboto',
                              color: Title_Color_1,
                              fontWeight: FontWeight.w700,
                              fontSize: 22)),
                    ),
                    margin: EdgeInsets.only(left: 16),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 16.0, top: 8),
                    child: new Text(
                        'Create an account to EverPlay Cricket to get all features.',
                        style: TextStyle(
                            fontFamily: AppConstants.textBold,
                            color: Text_Color,
                            fontWeight: FontWeight.w500,
                            fontSize: 14)),
                  ),
                  Container(
                    padding: EdgeInsets.fromLTRB(
                      10,
                      20,
                      10,
                      10,
                    ),
                    width: MediaQuery.of(context).size.width,
                    child: Container(
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // new Container(
                          //   padding: EdgeInsets.only(left: 16,top: 18),
                          //   child: Text(
                          //     'Refer Code (Optional) ',
                          //     textAlign: TextAlign.start,
                          //     style: TextStyle(
                          //       fontSize: 12,
                          //       fontFamily: AppConstants.textBold,
                          //       color: Text_Color,
                          //       fontWeight: FontWeight.normal,
                          //     ),
                          //   ),
                          // ),
                          //
                          // Container(
                          //   margin: EdgeInsets.fromLTRB(10, 4, 10, 0),
                          //   width: MediaQuery
                          //       .of(context)
                          //       .size
                          //       .width,
                          //   //padding: new EdgeInsets.all(10.0),
                          //   decoration: BoxDecoration(
                          //     borderRadius: BorderRadius.all(
                          //         Radius.circular(10)),
                          //     color: TextFieldColor,
                          //   ),
                          //   child: Padding(
                          //     padding: const EdgeInsets.only(
                          //         left: 16.0, right: 8),
                          //     child: TextField(
                          //       style: TextStyle(color: Colors.black),
                          //       controller: codeController,
                          //       decoration: InputDecoration(
                          //
                          //         border: InputBorder.none,
                          //         hintText: 'Refer code',
                          //         hintStyle: TextStyle(
                          //           color: TextFieldTextColor, fontSize: 14,
                          //           fontStyle: FontStyle.normal,
                          //           fontWeight: FontWeight.normal,),
                          //
                          //       ),
                          //     ),
                          //   ),
                          //
                          // ),

                          new Container(
                            padding: EdgeInsets.only(left: 16, top: 10),
                            child: Text(
                              'Mobile Number',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 12,
                                color: Text_Color,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            width: MediaQuery.of(context).size.width,
                            //padding: new EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Colors.white,
                              border: Border.all(color: editbgcolor),
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 10.0, right: 8),
                              child: TextField(
                                textAlignVertical: TextAlignVertical.center,
                                enabled: true,
                                style: TextStyle(color: Colors.black),
                                controller: mobileController,
                                inputFormatters: [
                                  LengthLimitingTextInputFormatter(10),
                                ],
                                decoration: InputDecoration(
                                  contentPadding: EdgeInsets.all(3),
                                  prefixIcon: Container(
                                    margin: EdgeInsets.only(right: 8),
                                    padding: EdgeInsets.only(top: 3, bottom: 3),
                                    decoration: BoxDecoration(
                                        border: Border(
                                            right: BorderSide(
                                                color: Colors.grey.shade300))),
                                    child: SizedBox(
                                      child: Center(
                                        widthFactor: 0.0,
                                        child: Text('+91',
                                            style: TextStyle(
                                              fontSize: 12,
                                              color: Title_Color_1,
                                              fontStyle: FontStyle.normal,
                                              fontWeight: FontWeight.bold,
                                            )),
                                      ),
                                    ),
                                  ),
                                  border: InputBorder.none,
                                  hintText: 'Enter your mobile number',
                                  hintStyle: TextStyle(
                                    color: Text_Color,
                                    fontSize: 14,
                                    fontStyle: FontStyle.normal,
                                    fontWeight: FontWeight.normal,
                                  ),
                                ),
                                keyboardType: TextInputType.number,
                                obscureText: false,
                              ),
                            ),
                          ),

                          Row(
                            //TODO to edit employee details
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: 16.0, right: 16, top: 4, bottom: 8),
                                child: Text("Email",
                                    style: TextStyle(
                                        fontFamily: AppConstants.textBold,
                                        color: Text_Color,
                                        fontWeight: FontWeight.normal,
                                        fontSize: 12)),
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              /* Expanded(
                                flex: 1,
                                child: Container(
                                  margin: EdgeInsets.only(left: 16),
                                  padding: new EdgeInsets.all(10.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(4)),
                                    color: Colors.transparent,
                                    border: Border.all(color: Colors.white),
                                  ),


                                  child: Text('+91', textAlign: TextAlign.center,
                                    style: TextStyle(color: Colors.white),),
                                ),
                              ),*/

                              Expanded(
                                flex: 1,
                                child: Container(
                                  margin: EdgeInsets.fromLTRB(10, 0, 10, 4),
                                  width: MediaQuery.of(context).size.width,
                                  //padding: new EdgeInsets.all(10.0),
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    color: TextFieldColor,
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 16.0,
                                        right: 8,
                                        top: 5,
                                        bottom: 5),
                                    child: TextField(
                                      style: TextStyle(color: Colors.black),
                                      controller: emailController,
                                      decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: 'Enter your email',
                                        hintStyle: TextStyle(
                                          color: TextFieldTextColor,
                                          fontSize: 14,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,
                                        ),
                                      ),
                                      keyboardType: TextInputType.emailAddress,
                                      obscureText: false,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),

                          new Container(
                            padding: EdgeInsets.only(left: 16, top: 10),
                            child: Text(
                              'Date of Birth',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 12,
                                fontFamily: AppConstants.textBold,
                                color: Text_Color,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          new GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              child: new IgnorePointer(
                                child: new Container(
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    color: TextFieldColor,
                                  ),
                                  padding: EdgeInsets.only(
                                      left: 10, top: 5, bottom: 5),
                                  margin: EdgeInsets.all(10),
                                  child: TextField(
                                    readOnly: true,
                                    controller: dobController,
                                    decoration: InputDecoration(
                                        counterText: "",
                                        hintStyle: TextStyle(
                                          fontSize: 12,
                                          color: TextFieldTextColor,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.w500,
                                        ),
                                        hintText: 'Date of Birth',
                                        border: InputBorder.none),
                                  ),
                                ),
                              ),
                              onTap: () async {
                                /* if (userDetailValue.dobfreeze != 1) {*/
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                await showDatePicker(
                                        context: context,
                                        initialDate: DateTime(
                                            DateTime.now().year - 18,
                                            DateTime.now().month,
                                            DateTime.now().day),
                                        firstDate: DateTime(1900),
                                        lastDate: DateTime(
                                            DateTime.now().year - 18,
                                            DateTime.now().month,
                                            DateTime.now().day))
                                    .then((value) => {
                                          dobController.text =
                                              DateFormat("dd/MM/yyyy")
                                                  .format(value!)
                                        });
                                // }
                              }),

                          new Container(
                            margin: EdgeInsets.fromLTRB(16, 4, 10, 10),
                            child: Text(
                              'Password',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 12,
                                color: Text_Color,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            width: MediaQuery.of(context).size.width,

                            //padding: new EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: TextFieldColor,
                            ),
                            padding: const EdgeInsets.only(
                                left: 16.0, right: 8, top: 8),
                            child: TextField(
                              style: TextStyle(color: Colors.black),
                              obscureText: !_passwordVisible,
                              controller: passwordController,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Password',
                                hintStyle: TextStyle(color: TextFieldTextColor),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                      // Based on passwordVisible state choose the icon
                                      _passwordVisible
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: _passwordVisible
                                          ? yellowdark
                                          : Colors.grey),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _passwordVisible = !_passwordVisible;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),

                          new Container(
                            margin: EdgeInsets.fromLTRB(16, 14, 10, 10),
                            child: Text(
                              'Confirm Password',
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                fontSize: 12,
                                color: Text_Color,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ),
                          new Container(
                            margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
                            width: MediaQuery.of(context).size.width,

                            //padding: new EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: TextFieldColor,
                            ),
                            padding: const EdgeInsets.only(
                                left: 16.0, right: 8, top: 10),
                            child: TextField(
                              style: TextStyle(color: Colors.black),
                              obscureText: !_confirmPasswordVisible,
                              controller: confirmPasswordController,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Confirm Password',
                                hintStyle: TextStyle(color: TextFieldTextColor),
                                suffixIcon: IconButton(
                                  icon: Icon(
                                      // Based on passwordVisible state choose the icon
                                      _confirmPasswordVisible
                                          ? Icons.visibility
                                          : Icons.visibility_off,
                                      color: _confirmPasswordVisible
                                          ? yellowdark
                                          : Colors.grey),
                                  onPressed: () {
                                    // Update the state i.e. toogle the state of passwordVisible variable
                                    setState(() {
                                      _confirmPasswordVisible =
                                          !_confirmPasswordVisible;
                                    });
                                  },
                                ),
                              ),
                            ),
                          ),

                          new Container(
                            margin: EdgeInsets.only(left: 16, top: 8),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                new Container(
                                  width: 20,
                                  child: Checkbox(
                                    side: BorderSide(color: darkgray),
                                    value: checkedValue,
                                    activeColor: Themecolor,
                                    onChanged: (value) {
                                      setState(() {
                                        checkedValue = value!;
                                      });
                                    },
                                  ),
                                ),
                                Expanded(
                                    child: new Container(
                                  child: new Html(
                                    data: "I Clarify that I am above 18 years by registering, I agree to <a href=" +
                                        AppConstants.terms_url +
                                        "><font color='#AA100E'><u><strong>T&C</strong></u></font></a>",
                                    onLinkTap: (link, _, __) {
                                      if (link!.contains("terms")) {
                                        navigateToVisionWebView(
                                            context, 'Terms & Condition', link);
                                      }
                                    },
                                    style: {
                                      'html': Style(
                                          textAlign: TextAlign.start,
                                          fontSize: FontSize(12),
                                          color: Title_Color_1,
                                          textDecoration: TextDecoration.none)
                                    },
                                  ),
                                )),
                              ],
                            ),
                          ),

                          new Container(
                              width: MediaQuery.of(context).size.width,
                              height: 45,
                              margin: EdgeInsets.fromLTRB(10, 8, 10, 20),
                              child: ElevatedButton(
                                style: ButtonStyle(
                                    backgroundColor:
                                        MaterialStateProperty.all(Themecolor),
                                    shape: MaterialStateProperty.all(
                                        RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(25)))),
                                child: Text(
                                  'Create Account',
                                  style: TextStyle(
                                      fontSize: 15, color: Colors.white),
                                ),
                                onPressed: () {
                                  FocusScope.of(context).unfocus();
                                  userRegisterNew();
                                },
                              )),

                          new Container(
                            width: MediaQuery.of(context).size.width,
                            padding: const EdgeInsets.only(
                              left: 13,
                              right: 13,
                            ),
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Flexible(
                                  child: new Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          color: textFieldBgColor,
                                          width:
                                              1, // This would be the width of the underline
                                        ),
                                      ),
                                    ),
                                  ),
                                  flex: 3,
                                ),
                                new Flexible(
                                  child: new Text(
                                    'OR',
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 14),
                                  ),
                                  flex: 1,
                                ),
                                new Flexible(
                                  child: new Container(
                                    decoration: BoxDecoration(
                                      border: Border(
                                        bottom: BorderSide(
                                          color: textFieldBgColor,
                                          width:
                                              1, // This would be the width of the underline
                                        ),
                                      ),
                                    ),
                                  ),
                                  flex: 3,
                                )
                              ],
                            ),
                          ),

                          Container(
                            // margin: EdgeInsets.only(top: 10),
                            child: Column(
                              children: [
                                // Expanded(
                                //   flex: 1,
                                //   child: InkWell(
                                //     child: Container(
                                //       height: 45,
                                //       margin: EdgeInsets.all(12),
                                //       // margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                //       decoration: BoxDecoration(
                                //           borderRadius: BorderRadius.all(
                                //               Radius.circular(4)),
                                //           color: const Color(0xff3b5999)
                                //       ),
                                //       child: Row(
                                //         mainAxisAlignment: MainAxisAlignment
                                //             .center,
                                //         children: [
                                //           Image.asset(
                                //               AppImages.fb_image, width: 24,
                                //               height: 24),
                                //           Text(
                                //             'Facebook',
                                //             textAlign: TextAlign.center,
                                //             style: TextStyle(
                                //                 color: Colors.white),)
                                //         ],
                                //       ),
                                //
                                //     ),
                                //     onTap: (){
                                //       initiateFacebookLogin();
                                //     },
                                //   ),
                                //
                                // ),

                                InkWell(
                                    child: Container(
                                      height: 45,
                                      margin: EdgeInsets.all(12),
                                      //margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                      //padding: const EdgeInsets.only(top: 10,bottom: 10),
                                      decoration: BoxDecoration(
                                          border:
                                              Border.all(color: Themecolor),
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(40)),
                                          color: const Color(0xffffffff)),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(AppImages.gmail_image,
                                              width: 24, height: 24),
                                          SizedBox(
                                            width: 10,
                                          ),
                                          Text(
                                            ' Register with Google',
                                            textAlign: TextAlign.center,
                                            style: TextStyle(color: Themecolor,fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                                    onTap: () {
                                      if (!checkedValue) {
                                        Fluttertoast.showToast(
                                            msg:
                                                'Please accept EverPlay T&Cs.',
                                            toastLength: Toast.LENGTH_SHORT,
                                            timeInSecForIosWeb: 1);
                                        return;
                                      } else {
                                        signup(context);
                                      }
                                    }),
                                version >= 13
                                    ? new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: Container(
                                    height: 45,
                                    margin: EdgeInsets.all(12),
                                    //margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                    //padding: const EdgeInsets.only(top: 10,bottom: 10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(25)),
                                        color: const Color(0xffffffff),
                                        border: Border.all(color: Themecolor)),
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.center,
                                      children: [
                                        Image.asset(AppImages.appleImageURL,
                                            width: 24, height: 24),
                                        SizedBox(width: 10,),
                                        Text(
                                          '  Register with Apple',
                                          textAlign: TextAlign.center,
                                          style: TextStyle(color: Themecolor,fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: () async {
                                    final credential =
                                    await SignInWithApple
                                        .getAppleIDCredential(
                                      scopes: [
                                        AppleIDAuthorizationScopes
                                            .email,
                                        AppleIDAuthorizationScopes
                                            .fullName,
                                      ],
                                      webAuthenticationOptions:
                                      WebAuthenticationOptions(
                                        // TODO: Set the `clientId` and `redirectUri` arguments to the values you entered in the Apple Developer portal during the setup
                                        clientId: 'com.everplay',
                                        redirectUri: Uri.parse(
                                          'https://flutter-sign-in-with-apple-example.glitch.me/callbacks/sign_in_with_apple',
                                        ),
                                      ),
                                      // TODO: Remove these if you have no need for them
                                      nonce: 'example-nonce',
                                      state: 'example-state',
                                    );
                                    userLoginSocial(new LoginRequest(
                                        name:
                                        credential.givenName ?? '',
                                        email: credential.email ?? '',
                                        image: '',
                                        authorizationCode: credential
                                            .authorizationCode ??
                                            '',
                                        idToken:
                                        credential.identityToken ??
                                            '',
                                        deviceId: '',
                                        social_id:
                                        credential.userIdentifier,
                                        socialLoginType: 'apple',
                                        fcmToken: fcmToken));
                                  },
                                )
                                    : new Container(),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  /*  new Container(
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        new Container(
                          child: Text("By registering, I agree to gamecubefantasy",
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: Colors.white,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 14)),
                        ),
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () =>
                          {
                            navigateToVisionWebView(context,"Terms & Condition",AppConstants.terms_url),
                            Navigator.pop(context),
                          },
                          child: new Container(
                            margin: EdgeInsets.fromLTRB(6, 0, 0, 0),
                            child: Text("T&C",
                                style: TextStyle(
                                    fontFamily: AppConstants.textBold,
                                    color: greenColor,
                                    decoration: TextDecoration.underline,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14)),
                          ),
                        ),
                      ],
                    ),
                  ),*/
                  new Container(
                    margin: EdgeInsets.fromLTRB(10, 10, 10, 20),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        new Container(
                          child: Text("Already have an account? ",
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: Title_Color_1,
                                  fontWeight: FontWeight.normal,
                                  fontSize: 14)),
                        ),
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () => {
                            Navigator.pop(context),
                            navigateToLogin(context),
                          },
                          child: new Container(
                            child: Text("Sign In Here ",
                                style: TextStyle(
                                    fontFamily: AppConstants.textBold,
                                    color: Themecolor,
                                    //  decoration: TextDecoration.underline,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 14)),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(55), // Set this height
              child: Container(
                //height: 500,
                color: Colors.transparent,
                // padding: EdgeInsets.only(top: 28),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => {Navigator.pop(context)},
                      child: new Container(
                        padding: EdgeInsets.all(15),
                        alignment: Alignment.centerLeft,
                        child: new Container(
                          width: 16,
                          height: 16,
                          child: Image(
                            image: AssetImage(AppImages.backImageURL),
                            fit: BoxFit.fill,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    new Container(
                      child: new Text('Register',
                          style: TextStyle(
                              fontFamily: AppConstants.textBold,
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 18)),
                    ),
                  ],
                ),
              ),
            )),
      ),
    );
  }

  void userLogin() async {
    if (mobileController.text.isEmpty || mobileController.text.length < 10) {
      Fluttertoast.showToast(
          msg: 'Enter a valid mobile number.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }
  }

  Future<void> signup(BuildContext context) async {
    final FirebaseAuth auth = FirebaseAuth.instance;
    final GoogleSignIn googleSignIn = GoogleSignIn();
    final GoogleSignInAccount? googleSignInAccount =
        await googleSignIn.signIn();
    if (googleSignInAccount != null) {
      final GoogleSignInAuthentication googleSignInAuthentication =
          await googleSignInAccount.authentication;
      final AuthCredential authCredential = GoogleAuthProvider.credential(
          idToken: googleSignInAuthentication.idToken,
          accessToken: googleSignInAuthentication.accessToken);
      // Getting users credential
      UserCredential result = await auth.signInWithCredential(authCredential);

      if (result != null) {
        User user = result.user!;
        userLoginSocial(new LoginRequest(
            name: user.displayName,
            email: user.email,
            image: user.photoURL ?? '',
            idToken: user.refreshToken,
            deviceId: '',
            social_id: user.uid,
            socialLoginType: 'gmail',
            fcmToken: fcmToken));
        googleSignIn.signOut();
      } // if result not null we simply call the MaterialpageRoute,
      // for go to the HomePage screen
    }
  }

  void userLoginSocial(LoginRequest loginRequest) async {
    AppLoaderProgress.showLoader(context);
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse = await client.userLoginSocial(loginRequest);

    AppLoaderProgress.hideLoader(context);

    AppPrefrence.putString(AppConstants.LOGIN_REGISTER_TYPE, "social_register");
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID,
        loginResponse.result!.user_id.toString());
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_NAME,
        loginResponse.result!.username);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE,
        loginResponse.result!.mobile.toString());
    AppPrefrence.putString(
        AppConstants.SHARED_PREFERENCE_USER_EMAIL, loginResponse.result!.email);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TOKEN,
        loginResponse.result!.custom_user_token);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_REFER_CODE,
        loginResponse.result!.refercode);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME,
        loginResponse.result!.team);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_STATE_NAME,
        loginResponse.result!.state);
    AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_PIC,
        loginResponse.result!.user_profile_image);
    AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS,
        loginResponse.result!.bank_verify);
    AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS,
        loginResponse.result!.pan_verify);
    AppPrefrence.putInt(
        AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS,
        loginResponse.result!.mobile_verify);
    AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS,
        loginResponse.result!.email_verify);
    AppPrefrence.putString(
        AppConstants.AUTHTOKEN, loginResponse.result!.custom_user_token);
    AppConstants.token = loginResponse.result!.custom_user_token!;

    /* if(loginResponse.result!.mobile_verify==1){
      AppPrefrence.putString(AppConstants.FROM, "login");
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID, loginResponse.result!.user_id.toString());
      navigateToOtpVerify(context);
    }else{
      navigateToVerifyEmailOrMobile(context, 'mobile', emailViewRefresh);
    }*/

    navigateToHomePage(context);

  }

  Future<void> initiateFacebookLogin() async {
    final LoginResult result = await FacebookAuth.instance
        .login(permissions: ["public_profile", "email", "user_friends"]);
    switch (result.status) {
      case LoginStatus.failed:
        print("Error");

        break;
      case LoginStatus.cancelled:
        print("CancelledByUser");

        break;
      case LoginStatus.success:
        var graphResponse = await http.get(Uri.parse(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email,picture.height(200)&access_token=' +
                result.accessToken!.token));
        var profile = json.decode(graphResponse.body);
        userLoginSocial(new LoginRequest(
            name: profile['name'],
            email: profile['email'],
            image: profile['picture'] ?? '',
            idToken: '',
            deviceId: '',
            social_id: profile['id'],
            socialLoginType: 'facebook',
            fcmToken: fcmToken));
        break;
    }
  }

  void userRegisterNew() async {
    if (mobileController.text.isEmpty || mobileController.text.length < 10) {
      Fluttertoast.showToast(
          msg: 'Please enter valid mobile number.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    } else if (emailController.text.isEmpty) {
      Fluttertoast.showToast(
          msg: 'Please enter valid email address',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    } else if (!emailController.text.contains("@") ||
        !RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
            .hasMatch(emailController.text)) {
      Fluttertoast.showToast(
          msg: 'Please enter valid email address',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }

    /*else if(dobController.text.isEmpty){
      Fluttertoast.showToast(
          msg: 'Please select date of birth.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }*/

    else if (passwordController.text.isEmpty) {
      Fluttertoast.showToast(
          msg: 'Please enter valid password.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    } else if (passwordController.text.length < 4) {
      Fluttertoast.showToast(
          msg: 'Password should be 4 to 16 char long.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }
    /*else if(confirmPasswordController.text.isEmpty){
      Fluttertoast.showToast(
          msg: 'Please enter confirm password.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }else if(passwordController.text.toString()!=confirmPasswordController.text.toString()){
      Fluttertoast.showToast(
          msg: 'Password & Confirm password not matched.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }*/

    else if (!checkedValue) {
      Fluttertoast.showToast(
          msg: 'Please accept EverPlay T&Cs.',
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }

    AppLoaderProgress.showLoader(context);
    RegisterRequest loginRequest = new RegisterRequest(
        '',
        emailController.text.toString(),
        fcmToken,
        '',
        passwordController.text.toString(),
        mobileController.text.toString(),
        '');
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse = await client.userRegisterNew(loginRequest);
    print("register response");
    print(loginResponse);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      AppPrefrence.putString(
          AppConstants.LOGIN_REGISTER_TYPE, "manual_register");
      AppPrefrence.putString(AppConstants.FROM, "register");
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID,
          loginResponse.result!.user_id.toString());
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE,
          loginResponse.result!.mobile.toString());
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME,
          loginResponse.result!.team.toString());
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_EMAIL,
          loginResponse.result!.email);
      navigateToOtpVerify(context);
    }
    Fluttertoast.showToast(
        msg: loginResponse.message!,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1);
  }
}
