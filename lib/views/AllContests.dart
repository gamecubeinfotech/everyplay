import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/ShimmerContestItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/category_contest_response.dart';
import 'package:EverPlay/repository/model/contest_request.dart';
import 'package:EverPlay/repository/model/contest_response.dart';
import 'package:EverPlay/repository/model/score_card_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class AllContests extends StatefulWidget {
  GeneralModel model;
  AllContests(this.model);
  @override
  _AllContestsState createState() => new _AllContestsState();
}

class _AllContestsState extends State<AllContests>{
  String userId='0';
  bool contestLoading=false;
  bool checked1=false,checked2=false,checked3=false,checked4=false,checked5=false,checked6=false,checked7=false,checked8=false;
  bool checked9=false,checked10=false,checked11=false,checked12=false,checked13=false,checked14=false,checked15=false,checked16=false,checked17=false;
  int isWinnings = 0, isTeam = 0, isEntry = 0, isWinner = 0;
  List<Contest> contestList=[];
  List<String> entryFee = [];
  List<String> winning = [];
  List<String> contest_type = [];
  List<String> contest_size = [];
  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        if (widget.model.categoryId == 111) {
          getAllContest();
        } else {
          getAllContestByCategoryId();
        }
      })
    });
  }
  Future<void> _pullRefresh() async {
    if (widget.model.categoryId == 111) {
      getAllContest();
    } else {
      getAllContestByCategoryId();
    }
  }

  @override
  void dispose() {

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness: Brightness.light) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
          body: new WillPopScope(
              child: new Stack(
                children: [
                  new Scaffold(
                    backgroundColor: Colors.white,
                    appBar: PreferredSize(
                      preferredSize: Size.fromHeight(55), // Set this height
                      child: Container(
                        color: Themecolor,
                        // padding: EdgeInsets.only(top: 40),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new Row(
                              children: [
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  onTap: ()=>{
                                    Navigator.pop(context)
                                  },
                                  child: new Container(
                                    padding: EdgeInsets.fromLTRB(15,15,25,15),
                                    alignment: Alignment.centerLeft,
                                    child: new Container(
                                      width: 16,
                                      height: 16,
                                      child: Image(
                                        image: AssetImage(AppImages.backImageURL),
                                        fit: BoxFit.fill,
                                        color: Colors.white,),
                                    ),
                                  ),
                                ),
                                new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    new Text("Contests",
                                        style: TextStyle(
                                            fontFamily: AppConstants.textBold,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w500,
                                            fontSize: 16)),
                                  ],
                                ),
                              ],
                            ),
                            new Container(
                              margin: EdgeInsets.only(top: 0, right: 20),
                              alignment: Alignment.center,
                              child: new Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.end,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  new GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    child: new Container(
                                      child: new Row(
                                        children: [
                                          new Container(
                                            margin: EdgeInsets.only(left: 5),
                                            height: 20,
                                            width: 20,
                                            child: Image(
                                              image: AssetImage(
                                                  AppImages.filterIcon),color: Colors.white,),
                                          )
                                        ],
                                      ),
                                    ),
                                    onTap: (){
                                        setState(() {
                                          AppConstants.isFilter=true;
                                        });
                                    },
                                  ),

                                ],
                              ),
                            ),

                          ],
                        ),
                      ),
                    ),
                    body: new Stack(
                      children: [
                        new Container(
                          height: MediaQuery.of(context).size.height,
                          color: Colors.white,
                          child: new Column(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              new MatchHeader(widget.model.teamVs!,widget.model.headerText!,widget.model.isFromLive??false,widget.model.isFromLiveFinish??false),
                              new Container(
                                color: TextFieldColor,
                                width: MediaQuery.of(context).size.width,
                                height: 30,

                                margin: EdgeInsets.only(top: 10,bottom: 5),
                                child: new Container(
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      new GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: new Row(
                                          children: [
                                            new Container(
                                              margin: EdgeInsets.only(left: 10,right: 2),
                                              alignment: Alignment.centerLeft,
                                              child:  Text(
                                                'PRIZE POOL',
                                                style: TextStyle(
                                                    fontFamily: 'noway',
                                                    color: Colors.black54,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12),
                                              ),
                                            ),
                                            isWinnings!=0?new Container(
                                              height: 13,
                                              width: 5,
                                              child: Image(
                                                image: AssetImage(isWinnings==1?AppImages.upSort:AppImages.downSort,),
                                                color: Colors.black,
                                              ),
                                            ):new Container(),
                                          ],
                                        ),
                                        onTap:(){
                                          prizePoolSort();
                                        },
                                      ),
                                      new GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: new Row(
                                          children: [
                                            new Container(
                                              margin: EdgeInsets.only(left: 10,right: 2),
                                              alignment: Alignment.centerLeft,
                                              child:  Text(
                                                'SPOTS',
                                                style: TextStyle(
                                                    color: Colors.black54,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12),
                                              ),
                                            ),
                                            isTeam!=0?new Container(
                                              height: 13,
                                              width: 5,
                                              child: Image(
                                                image: AssetImage(isTeam==1?AppImages.upSort:AppImages.downSort,),
                                                color: Colors.black,
                                              ),
                                            ):new Container(),
                                          ],
                                        ),
                                        onTap: (){
                                          spotsSort();
                                        },
                                      ),
                                      new GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: new Row(
                                          children: [
                                            new Container(
                                              margin: EdgeInsets.only(left: 10,right: 2),
                                              alignment: Alignment.centerLeft,
                                              child:  Text(
                                                'WINNERS',
                                                style: TextStyle(
                                                    color: Colors.black54,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12),
                                              ),
                                            ),
                                            isWinner!=0?new Container(
                                              height: 13,
                                              width: 5,
                                              child: Image(
                                                image: AssetImage(isWinner==1?AppImages.upSort:AppImages.downSort,),
                                                color: Colors.black,
                                              ),
                                            ):new Container(),
                                          ],
                                        ),
                                        onTap: (){
                                          winnersSort();
                                        },
                                      ),
                                      new GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: new Row(
                                          children: [
                                            new Container(
                                              margin: EdgeInsets.only(left: 10,right: 2),
                                              alignment: Alignment.centerLeft,
                                              child:  Text(
                                                'ENTRY',
                                                style: TextStyle(
                                                    color: Colors.black54,
                                                    fontWeight: FontWeight.w400,
                                                    fontSize: 12),
                                              ),
                                            ),
                                            isEntry!=0?new Container(
                                              height: 13,
                                              width: 5,
                                              child: Image(
                                                image: AssetImage(isEntry==1?AppImages.upSort:AppImages.downSort,),
                                                color: Colors.black,
                                              ),
                                            ):new Container(),
                                          ],
                                        ),
                                        onTap: (){
                                          entrySort();
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              !contestLoading?new Expanded(child: RefreshIndicator(child: new Container(
                                padding: EdgeInsets.all(8),
                                color: Colors.white,
                                child: new ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.vertical,
                                    itemCount: contestList.length,
                                    itemBuilder: (BuildContext context, int index) {
                                      return new ContestItemAdapter(widget.model,contestList[index],onJoinContestResult,userId,getWinnerPriceCard);
                                    }
                                ),
                              ),onRefresh: _pullRefresh,)):new Expanded(child: new Container(
                                padding: EdgeInsets.all(8),
                                color: bgColor,
                                child: new ListView.builder(
                                    shrinkWrap: true,
                                    scrollDirection: Axis.vertical,
                                    itemCount: 7,
                                    itemBuilder: (BuildContext context, int index) {
                                      return new ShimmerContestItemAdapter();
                                    }
                                ),
                              ))
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  AppConstants.isFilter?new Container(
                    // margin: EdgeInsets.only(top: 28),
                    color: Colors.white,
                    height: MediaQuery.of(context).size.height,
                    child: new Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        SingleChildScrollView(
                          child: new Container(
                            height: MediaQuery.of(context).size.height,
                            child: new Column(
                              children: [
                                new Container(
                                  height: 40,
                                  width: MediaQuery.of(context).size.width,
                                  padding: EdgeInsets.only(left: 10, right: 10),
                                  color: primaryColor,
                                  child: new Stack(
                                    alignment: Alignment.centerRight,
                                    children: [
                                      new GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: new Container(
                                          child: new Text('Close',
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 16)),
                                        ),
                                        onTap: (){
                                          setState(() {
                                            AppConstants.isFilter=false;
                                          });
                                        },
                                      ),
                                      new Container(
                                        alignment: Alignment.center,
                                        width: MediaQuery.of(context).size.width,
                                        child: new Text('Filter',
                                            style: TextStyle(
                                                fontFamily: AppConstants.textBold,
                                                color: Colors.white,
                                                fontWeight: FontWeight.w500,
                                                fontSize: 16)),
                                      )
                                    ],
                                  ),
                                ),
                                new Container(
                                  height: MediaQuery.of(context).size.height-70,
                                  child: new SingleChildScrollView(
                                    padding: EdgeInsets.only(bottom: 70),
                                    child: new Column(
                                      children: [
                                        new Container(
                                          height: 40,
                                          width: MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.only(left: 10, right: 10),
                                          color: bgColor,
                                          child: new Stack(
                                            alignment: Alignment.centerLeft,
                                            children: [
                                              new Container(
                                                width: MediaQuery.of(context).size.width,
                                                child: new Text('Entry',
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w400,
                                                        fontSize: 14)),
                                              )
                                            ],
                                          ),
                                        ),
                                        new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Flexible(child: new Row(
                                              children: [
                                                Checkbox(
                                                  activeColor: primaryColor,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      checked1=value!;
                                                      if(checked1){
                                                        entryFee.add('1');
                                                      }else{
                                                        entryFee.remove('1');
                                                      }
                                                    });
                                                  }, value: checked1,
                                                ),
                                                Text(
                                                  '₹1 to ₹100',
                                                  style: new TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ),flex: 1,),
                                            new Flexible(
                                              child: new Row(
                                                children: [
                                                  Checkbox(
                                                    activeColor: primaryColor,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        checked2=value!;
                                                        if(checked2){
                                                          entryFee.add('2');
                                                        }else{
                                                          entryFee.remove('2');
                                                        }
                                                      });
                                                    }, value: checked2,
                                                  ),
                                                  Text(
                                                    '₹101 to ₹1000',
                                                    style: new TextStyle(
                                                        fontSize: 14.0,
                                                        color: Colors.grey,
                                                        fontWeight: FontWeight.w400),
                                                  ),
                                                ],
                                              ),
                                              flex: 1,
                                            )
                                          ],
                                        ),
                                        new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Flexible(child: new Row(
                                              children: [
                                                Checkbox(
                                                  activeColor: primaryColor,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      checked3=value!;
                                                      if(checked3){
                                                        entryFee.add('3');
                                                      }else{
                                                        entryFee.remove('3');
                                                      }
                                                    });
                                                  }, value: checked3,
                                                ),
                                                Text(
                                                  '₹1001 to ₹5000',
                                                  style: new TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ),flex: 1,),
                                            new Flexible(
                                              child: new Row(
                                                children: [
                                                  Checkbox(
                                                    activeColor: primaryColor,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        checked4=value!;
                                                        if(checked4){
                                                          entryFee.add('4');
                                                        }else{
                                                          entryFee.remove('4');
                                                        }
                                                      });
                                                    }, value: checked4,
                                                  ),
                                                  Text(
                                                    '₹5000 & more',
                                                    style: new TextStyle(
                                                        fontSize: 14.0,
                                                        color: Colors.grey,
                                                        fontWeight: FontWeight.w400),
                                                  ),
                                                ],
                                              ),
                                              flex: 1,
                                            )
                                          ],
                                        ),
                                        new Container(
                                          height: 40,
                                          width: MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.only(left: 10, right: 10),
                                          color: bgColor,
                                          child: new Stack(
                                            alignment: Alignment.centerLeft,
                                            children: [
                                              new Container(
                                                width: MediaQuery.of(context).size.width,
                                                child: new Text('Winnings',
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w400,
                                                        fontSize: 14)),
                                              )
                                            ],
                                          ),
                                        ),
                                        new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Flexible(child: new Row(
                                              children: [
                                                Checkbox(
                                                  activeColor: primaryColor,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      checked5=value!;
                                                      if(checked5){
                                                        winning.add('1');
                                                      }else{
                                                        winning.remove('1');
                                                      }
                                                    });
                                                  }, value: checked5,
                                                ),
                                                Text(
                                                  '₹1 to ₹1000',
                                                  style: new TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ),flex: 1,),
                                            new Flexible(
                                              child: new Row(
                                                children: [
                                                  Checkbox(
                                                    activeColor: primaryColor,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        checked6=value!;
                                                        if(checked6){
                                                          winning.add('2');
                                                        }else{
                                                          winning.remove('2');
                                                        }
                                                      });
                                                    }, value: checked6,
                                                  ),
                                                  Text(
                                                    '₹1001 to ₹50000',
                                                    style: new TextStyle(
                                                        fontSize: 14.0,
                                                        color: Colors.grey,
                                                        fontWeight: FontWeight.w400),
                                                  ),
                                                ],
                                              ),
                                              flex: 1,
                                            )
                                          ],
                                        ),
                                        new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Flexible(child: new Row(
                                              children: [
                                                Checkbox(
                                                  activeColor: primaryColor,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      checked7=value!;
                                                      if(checked7){
                                                        winning.add('3');
                                                      }else{
                                                        winning.remove('3');
                                                      }
                                                    });
                                                  }, value: checked7,
                                                ),
                                                Text(
                                                  '₹50001 to ₹1 Lakh',
                                                  style: new TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ),flex: 1,),
                                            new Flexible(
                                              child: new Row(
                                                children: [
                                                  Checkbox(
                                                    activeColor: primaryColor,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        checked8=value!;
                                                        if(checked8){
                                                          winning.add('4');
                                                        }else{
                                                          winning.remove('4');
                                                        }
                                                      });
                                                    }, value: checked8,
                                                  ),
                                                  Text(
                                                    '₹1 Lakh & more',
                                                    style: new TextStyle(
                                                        fontSize: 14.0,
                                                        color: Colors.grey,
                                                        fontWeight: FontWeight.w400),
                                                  ),
                                                ],
                                              ),
                                              flex: 1,
                                            )
                                          ],
                                        ),
                                        new Container(
                                          height: 40,
                                          width: MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.only(left: 10, right: 10),
                                          color: bgColor,
                                          child: new Stack(
                                            alignment: Alignment.centerLeft,
                                            children: [
                                              new Container(
                                                width: MediaQuery.of(context).size.width,
                                                child: new Text('Contest Type',
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w400,
                                                        fontSize: 14)),
                                              )
                                            ],
                                          ),
                                        ),
                                        new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Flexible(child: new Row(
                                              children: [
                                                Checkbox(
                                                  activeColor: primaryColor,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      checked9=value!;
                                                      if(checked9){
                                                        contest_type.add('1');
                                                      }else{
                                                        contest_type.remove('1');
                                                      }
                                                    });
                                                  }, value: checked9,
                                                ),
                                                Text(
                                                  'Multi Entry',
                                                  style: new TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ),flex: 1,),
                                            new Flexible(
                                              child: new Row(
                                                children: [
                                                  Checkbox(
                                                    activeColor: primaryColor,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        checked10=value!;
                                                        if(checked10){
                                                          contest_type.add('3');
                                                        }else{
                                                          contest_type.remove('3');
                                                        }
                                                      });
                                                    }, value: checked10,
                                                  ),
                                                  Text(
                                                    '100% Bonus',
                                                    style: new TextStyle(
                                                        fontSize: 14.0,
                                                        color: Colors.grey,
                                                        fontWeight: FontWeight.w400),
                                                  ),
                                                ],
                                              ),
                                              flex: 1,
                                            )
                                          ],
                                        ),
                                        new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Flexible(child: new Row(
                                              children: [
                                                Checkbox(
                                                  activeColor: primaryColor,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      checked11=value!;
                                                      if(checked11){
                                                        contest_type.add('2');
                                                      }else{
                                                        contest_type.remove('2');
                                                      }
                                                    });
                                                  }, value: checked11,
                                                ),
                                                Text(
                                                  'Confirmed',
                                                  style: new TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ),flex: 1,),
                                          ],
                                        ),
                                        new Container(
                                          height: 40,
                                          width: MediaQuery.of(context).size.width,
                                          padding: EdgeInsets.only(left: 10, right: 10),
                                          color: bgColor,
                                          child: new Stack(
                                            alignment: Alignment.centerLeft,
                                            children: [
                                              new Container(
                                                width: MediaQuery.of(context).size.width,
                                                child: new Text('Contest Size',
                                                    style: TextStyle(
                                                        fontFamily: AppConstants.textBold,
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.w400,
                                                        fontSize: 14)),
                                              )
                                            ],
                                          ),
                                        ),
                                        new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Flexible(child: new Row(
                                              children: [
                                                Checkbox(
                                                  activeColor: primaryColor,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      checked12=value!;
                                                      if(checked12){
                                                        contest_size.add('1');
                                                      }else{
                                                        contest_size.remove('1');
                                                      }
                                                    });
                                                  }, value: checked12,
                                                ),
                                                Text(
                                                  '2',
                                                  style: new TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ),flex: 1,),
                                            new Flexible(
                                              child: new Row(
                                                children: [
                                                  Checkbox(
                                                    activeColor: primaryColor,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        checked13=value!;
                                                        if(checked13){
                                                          contest_size.add('2');
                                                        }else{
                                                          contest_size.remove('2');
                                                        }
                                                      });
                                                    }, value: checked13,
                                                  ),
                                                  Text(
                                                    '3 to 10',
                                                    style: new TextStyle(
                                                        fontSize: 14.0,
                                                        color: Colors.grey,
                                                        fontWeight: FontWeight.w400),
                                                  ),
                                                ],
                                              ),
                                              flex: 1,
                                            )
                                          ],
                                        ),
                                        new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Flexible(child: new Row(
                                              children: [
                                                Checkbox(
                                                  activeColor: primaryColor,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      checked14=value!;
                                                      if(checked14){
                                                        contest_size.add('3');
                                                      }else{
                                                        contest_size.remove('3');
                                                      }
                                                    });
                                                  }, value: checked14,
                                                ),
                                                Text(
                                                  '11 to 20',
                                                  style: new TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ),flex: 1,),
                                            new Flexible(
                                              child: new Row(
                                                children: [
                                                  Checkbox(
                                                    activeColor: primaryColor,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        checked15=value!;
                                                        if(checked15){
                                                          contest_size.add('4');
                                                        }else{
                                                          contest_size.remove('4');
                                                        }
                                                      });
                                                    }, value: checked15,
                                                  ),
                                                  Text(
                                                    '21 to 200',
                                                    style: new TextStyle(
                                                        fontSize: 14.0,
                                                        color: Colors.grey,
                                                        fontWeight: FontWeight.w400),
                                                  ),
                                                ],
                                              ),
                                              flex: 1,
                                            )
                                          ],
                                        ),
                                        new Row(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: <Widget>[
                                            new Flexible(child: new Row(
                                              children: [
                                                Checkbox(
                                                  activeColor: primaryColor,
                                                  onChanged: (value) {
                                                    setState(() {
                                                      checked16=value!;
                                                      if(checked16){
                                                        contest_size.add('5');
                                                      }else{
                                                        contest_size.remove('5');
                                                      }
                                                    });
                                                  }, value: checked16,
                                                ),
                                                Text(
                                                  '101 to 1000',
                                                  style: new TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w400),
                                                ),
                                              ],
                                            ),flex: 1,),
                                            new Flexible(
                                              child: new Row(
                                                children: [
                                                  Checkbox(
                                                    activeColor: primaryColor,
                                                    onChanged: (value) {
                                                      setState(() {
                                                        checked17=value!;
                                                        if(checked17){
                                                          contest_size.add('6');
                                                        }else{
                                                          contest_size.remove('6');
                                                        }
                                                      });
                                                    }, value: checked17,
                                                  ),
                                                  Text(
                                                    '1001 to 10000',
                                                    style: new TextStyle(
                                                        fontSize: 14.0,
                                                        color: Colors.grey,
                                                        fontWeight: FontWeight.w400),
                                                  ),
                                                ],
                                              ),
                                              flex: 1,
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: new Row(
                            children: [
                              new Flexible(
                                child: new Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 40,
                                    margin: EdgeInsets.only(left: 20, right: 10),
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        primary: primaryColor,
                                        elevation: 0.5,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(40),
                                        ),
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          AppConstants.isFilter = false;
                                          checked1 = false;
                                          checked2 = false;
                                          checked3 = false;
                                          // ... Repeat for other checked variables
                                          entryFee.clear();
                                          winning.clear();
                                          contest_size.clear();
                                          contest_type.clear();
                                        });

                                        if (widget.model.categoryId == 111) {
                                          getAllContest();
                                        } else {
                                          getAllContestByCategoryId();
                                        }
                                      },
                                      child: Text(
                                        'Clear all filters',
                                        style: TextStyle(fontSize: 14, color: Colors.white),
                                      ),
                                    )
                                ),
                                flex: 1,
                              ),
                              new Flexible(
                                child: new Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 40,
                                    margin: EdgeInsets.only(left: 10, right: 20),
                                    child: ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        primary: yellowdark,
                                        elevation: 0.5,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(40),
                                        ),
                                      ),
                                      onPressed: () {
                                        setState(() {
                                          AppConstants.isFilter = false;
                                        });

                                        if (widget.model.categoryId == 111) {
                                          getAllContest();
                                        } else {
                                          getAllContestByCategoryId();
                                        }
                                      },
                                      child: Text(
                                        'Apply',
                                        style: TextStyle(fontSize: 14, color: Colors.white),
                                      ),
                                    )
                                ),
                                flex: 1,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ):new Container()
                ],
              ),
              onWillPop: _onWillPop)),
    );
  }
  Future<bool> _onWillPop() async {
    if(!AppConstants.isFilter) {
      setState(() {
        Navigator.pop(context);
      });
    }
    return Future.value(false);
  }
  void getAllContestByCategoryId() async {
    setState(() {
      contestLoading=true;
    });
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,category_id: widget.model.categoryId.toString(),fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.slotId.toString(),entryfee: '',winning: '',contest_type: '',contest_size: '');
    final client = ApiClient(AppRepository.dio);
    ContestResponse response = await client.getContestByCategoryId(contestRequest);
    if (response.status == 1) {
        contestList=response.result!.contest!;
    }
    // Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
      contestLoading=false;
    });
  }
  void getAllContest() async {
    setState(() {
      contestLoading=true;
    });
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.slotId.toString(),entryfee: entryFee.join(","),winning: winning.join(","),contest_type: contest_type.join(","),contest_size: contest_size.join(","));
    final client = ApiClient(AppRepository.dio);
    ContestResponse response = await client.getContests(contestRequest);
    if (response.status == 1) {
        contestList=response.result!.contest!;
        if(widget.model.filterType=='contest_size'){
          spotsSort();
        }else if(widget.model.filterType=='entry_fee'){
          entrySort();
        }
    }
    // Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
      contestLoading=false;
    });
  }
  void onJoinContestResult(int isJoined,String referCode){
    if(isJoined==1){
      if (widget.model.categoryId == 111) {
        getAllContest();
      } else {
        getAllContestByCategoryId();
      }
    }
  }
  void prizePoolSort(){
    setState(() {
      if(isWinnings==2||isWinnings==0){
        isWinnings=1;
        contestList.sort((a,b) => a.win_amount!.compareTo(b.win_amount!));
      }else{
        isWinnings=2;
        contestList.sort((a,b) => b.win_amount!.compareTo(a.win_amount!));
      }
      isTeam=0;
      isEntry=0;
      isWinner=0;
    });
  }

  void spotsSort(){
    setState(() {
      if(isTeam==2||isTeam==0){
        isTeam=1;
        contestList.sort((a,b) => a.getLeftSpots().compareTo(b.getLeftSpots()));
      }else{
        isTeam=2;
        contestList.sort((a,b) => b.getLeftSpots().compareTo(a.getLeftSpots()));
      }
      isWinnings=0;
      isEntry=0;
      isWinner=0;
    });
  }
  void winnersSort(){
    setState(() {
      if(isWinner==2||isWinner==0){
        isWinner=1;
        contestList.sort((a,b) => a.totalWinners().compareTo(b.totalWinners()));
      }else{
        isWinner=2;
        contestList.sort((a,b) => b.totalWinners().compareTo(a.totalWinners()));
      }
      isWinnings=0;
      isEntry=0;
      isTeam=0;
    });
  }
  void entrySort(){
    setState(() {
      if(isEntry==2||isEntry==0){
        isEntry=1;
        contestList.sort((a,b) => double.parse(a.entryfee.toString()).compareTo(double.parse(b.entryfee.toString())));
      }else{
        isEntry=2;
        contestList.sort((a,b) => double.parse(b.entryfee.toString()).compareTo(double.parse(a.entryfee.toString())));
      }
      isWinnings=0;
      isWinner=0;
      isTeam=0;
    });
  }
  void getWinnerPriceCard(int id,String winAmount) async {
    AppLoaderProgress.showLoader(context);
    ContestRequest request = new ContestRequest(user_id: userId,challenge_id: id.toString(),matchkey: widget.model.matchKey,);
    final client = ApiClient(AppRepository.dio);
    ScoreCardResponse response = await client.getWinnersPriceCard(request);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
    if (response.status == 1) {
      MethodUtils.showWinningPopup(context,response.result!,winAmount);
    }
  }
}