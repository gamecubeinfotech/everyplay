import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/banner_response.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/matchlist_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:EverPlay/views/NoMatchesListFound.dart';

class AffiliateMatches extends StatefulWidget {
  String start_date;
  String end_date;
  AffiliateMatches(this.start_date,this.end_date);
  @override
  _AffiliateMatchesState createState() => new _AffiliateMatchesState();
}

class _AffiliateMatchesState extends State<AffiliateMatches>{
  late List<MatchDetails> matchList=[];
  TextEditingController codeController = TextEditingController();
  bool _enable=false;
  late String userId='0';

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getAffiliateMatchData();
      })
    });
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: primaryColor,
            // padding: EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,color: Colors.white,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Affiliate',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Stack(
          children: [
            matchList.length>0?new Container(
              margin: EdgeInsets.all(8),
              child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  itemCount: matchList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: Card(
                        clipBehavior: Clip.antiAlias,
                        elevation: 1,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          padding: EdgeInsets.all(0),
                          child: new Container(
                            child: new Column(
                              children: [
                                new Container(
                                  height: 20,
                                  margin: EdgeInsets.only(top: 5),
                                  child: new Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(
                                            left: 10, top: 5),
                                        child: Text(
                                          matchList[index].name!,
                                          style: TextStyle(
                                              fontFamily: 'noway',
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                new Divider(),
                                new Container(
                                  padding: EdgeInsets.all(0),
                                  margin:
                                  EdgeInsets.only(top: 8, bottom: 13),
                                  child: new Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      new Container(
                                        child: new Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.start,
                                          children: [
                                            new Container(
                                              child: new Stack(
                                                alignment:
                                                Alignment
                                                    .centerLeft,
                                                children: [
                                                  new Container(
                                                    height:
                                                    16,
                                                    width:
                                                    43,
                                                    child:
                                                    Image(image: AssetImage(AppImages.leftTeamIcon),fit: BoxFit.fill,color: MethodUtils.hexToColor(matchList[index].team1_color!),),
                                                  ),
                                                  new Container(
                                                    height:
                                                    25,
                                                    width:
                                                    40,
                                                    child:
                                                    CachedNetworkImage(
                                                      imageUrl: matchList[index].team1logo!,
                                                      placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                                      errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            new Container(
                                              margin:
                                              EdgeInsets.only(left: 5),
                                              child: new Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [

                                                  new Container(
                                                    margin: EdgeInsets.only(
                                                        top: 2),
                                                    child: new Text(
                                                      matchList[index].team1display!,
                                                      textAlign:
                                                      TextAlign.left,
                                                      style: TextStyle(
                                                          fontFamily:
                                                          'noway',
                                                          color:
                                                          Colors.black,
                                                          fontWeight:
                                                          FontWeight
                                                              .w600,
                                                          fontSize: 14),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(top: 2),
                                        child: new Text(
                                          'Winner Declare',
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontFamily: 'noway',
                                              color: primaryColor,
                                              fontWeight: FontWeight.w600,
                                              fontSize: 12),
                                        ),
                                      ),
                                      new Container(
                                        child: new Row(
                                          children: [
                                            new Container(
                                              margin:
                                              EdgeInsets.only(right: 5),
                                              child: new Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment
                                                    .start,
                                                children: [

                                                  new Container(
                                                    margin: EdgeInsets.only(
                                                        top: 2),
                                                    child: new Text(
                                                      matchList[index].team2display!,
                                                      textAlign:
                                                      TextAlign.left,
                                                      style: TextStyle(
                                                          fontFamily:
                                                          'noway',
                                                          color:
                                                          Colors.black,
                                                          fontWeight:
                                                          FontWeight
                                                              .w600,
                                                          fontSize: 14),
                                                    ),
                                                  )
                                                ],
                                              ),
                                            ),
                                            new Stack(
                                              alignment:
                                              Alignment
                                                  .centerLeft,
                                              children: [
                                                new Container(
                                                  height:
                                                  16,
                                                  width:
                                                  43,
                                                  child:
                                                  Image(image: AssetImage(AppImages.rightTeamIcon),fit: BoxFit.fill,color: MethodUtils.hexToColor(matchList[index].team2_color!),),
                                                ),
                                                new Container(
                                                  height:
                                                  25,
                                                  width:
                                                  40,
                                                  child:
                                                  CachedNetworkImage(
                                                    imageUrl: matchList[index].team2logo!,
                                                    placeholder: (context,url)=> new Image.asset(AppImages.logoPlaceholderURL),
                                                    errorWidget: (context, url, error) => new Image.asset(AppImages.logoPlaceholderURL),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                new Divider(height: 1,),
                                new Container(
                                  height: 30,
                                  child: new Container(
                                    child: new Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.end,
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.only(right: 10),
                                          child: new Text(
                                            'Total Affiliation: ₹'+matchList[index].total_earned!.toString(),
                                            style: TextStyle(
                                                color: greenColor,
                                                fontSize: 12,
                                                fontWeight:
                                                FontWeight.bold),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      onTap: (){
                           navigateToEarnContests(context,matchList[index].matchkey!,matchList[index].short_name!);
                      },
                    );
                  }),
            ):NoMatchesListFound()
          ],
        ),
      ),
    );
  }
  void getAffiliateMatchData() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,start_date:widget.start_date,end_date:widget.end_date ,page: '0');
    final client = ApiClient(AppRepository.dio);
    MatchListResponse matchListResponse = await client.getAffiliateMatchData(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (matchListResponse.status == 1) {
      matchList=matchListResponse.result!;
      setState(() {});
    }
  }
}