import 'dart:io';

import 'package:app_installer/app_installer.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:custom_timer/custom_timer.dart';
import 'package:dio/dio.dart' as Dio;
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:package_info/package_info.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:EverPlay/adapter/MatchItemAdapter.dart';
import 'package:EverPlay/adapter/ShimmerMatchItemAdapter.dart';
import 'package:EverPlay/adapter/UserMyMatchesItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/banner_response.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/finish_matchlist_response.dart';
import 'package:EverPlay/repository/model/live_data_response.dart';
import 'package:EverPlay/repository/model/matchlist_response.dart';
import 'package:EverPlay/repository/model/my_balance_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';
import 'package:EverPlay/views/LoginNew.dart';
import 'package:EverPlay/views/More.dart';
import 'package:EverPlay/views/MyMatches.dart';

import '../GetXController/HomeMatchesGetController.dart';
import '../GetXController/UpcomingMyMatchesGetController.dart';
import 'AccountNNew.dart';


class HomePage extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomePage> with SingleTickerProviderStateMixin {
  final homeMatchesGetController=Get.put(HomeMatchesGetController());

  late List<MatchDetails> userMatchList = [];
  late List<MatchDetails> matchList = [];
  late List<BannerListItem> bannerList = [];
  late List<SportType> sportsList = [];
  late MatchListResponse matchListResponsee = new MatchListResponse();
  late FinishMatchListResponse finishMatchListResponse = new FinishMatchListResponse();
  LevelDataResponse levelDataResponse = new LevelDataResponse();
  late TabController controller;
  int _currentSportIndex = 0;
  int _currentIndex = 0;
  int _currentMatchIndex = 0;
  var title = 'Home';
  bool back_dialog = false;
  bool logout_dialog = false;
  bool isDownloading = false;
  var scaffoldKey = GlobalKey<ScaffoldState>();
  bool isLoading = true;
  bool isMatchLoading = false;
  bool isMyMatchLoading = false;
  bool isAccountLoading = false;
  int notification = 0;
  int onlineVersion = 0;
  late String userId = '0';
  late String balance = '0';
  late String level = '0';
  String userName = '';
  String mobile = '';
  String email = '';
  String teamName = '';
  String userPic = '';
  String userReferCode = '';
  String fullPath='';
  String progress = '0';
  late int previousTime;
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );
  final List<CustomTimerController> _controller =[];
  var dio = Dio.Dio();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _initPackageInfo();
    AppConstants.context = context;
    AppPrefrence.getString(AppConstants.AUTHTOKEN)
        .then((value) => {AppConstants.token = value});
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) => {
      setState(() {
        userId = value;
        getBannerList();
        getUserBalance();
      })
    });

    getData();
  }

  void getData() {
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_NAME)
        .then((value) => {
      userName = value,
      AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_MOBILE)
          .then((value) => {
        mobile = value,
        AppPrefrence.getString(
            AppConstants.SHARED_PREFERENCE_USER_EMAIL)
            .then((value) => {
          setState(() {
            email = value;
          })
        })
      })
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME)
        .then((value) => {
      setState(() {
        teamName = value;
      })
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_PIC)
        .then((value) => {
      setState(() {
        userPic = value;
      })
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_REFER_CODE)
        .then((value) => {
      setState(() {
        userReferCode = value;
      })
    });
    AppPrefrence.getPreviousDate(AppConstants.PREVIOUS_DATE).then((value) => {
      setState(() {
        previousTime = value;
      })
    });
    // getMatchList();
  }

  Future<void> _initPackageInfo() async {
    final PackageInfo info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
      print(AppConstants.versionCode);
      AppConstants.versionCode = _packageInfo.buildNumber;
    });
  }

  Future<void> _myMatchPullRefresh(int index) async {
    getMyUpcomingMatchList(index);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Themecolor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Stack(
        children: [
          Scaffold(
            appBar: _currentIndex == 1 ||  _currentIndex == 2 || _currentIndex ==3
                ? null
                : AppBar(
              automaticallyImplyLeading: false,
              backgroundColor: Colors.black,
              centerTitle: false,
              title: Container(
                // width: 20,
                height: 30,
                // margin: EdgeInsets.only(top: 35,),
                child: new Image(
                  image: AssetImage(AppImages.logoImageURL),
                  fit: BoxFit.fill,
                ),
              ),
              actions: [
                Container(
                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          margin: EdgeInsets.only(right: 15, left: 5),
                          child: new Row(
                            children: [
                              /* Text(
                                      "₹ $balance",
                                      style: TextStyle(color: Colors.white),
                                    ),*/
                              // Text("₹"+myBalanceResultItem.totalamount.toString(),style: TextStyle(color: Colors.white),),
                              new Container(
                                margin: EdgeInsets.only(left: 5),
                                height: 20,
                                width: 20,
                                child: Image(
                                    image:
                                    AssetImage(AppImages.walletImageURL)),
                              ),
                            ],
                          ),
                        ),
                        onTap: () => {navigateToWallet(context)},
                      ),
                      new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new Container(
                          margin: EdgeInsets.only(right: 10, left: 5),
                          height: 20,
                          width: 20,
                          child: Image(
                              image: AssetImage(
                                AppImages.notificationImageURL,
                              )
                            //notification==0?AppImages.notificationImageURL:AppImages.notificationDotIcon),
                          ),
                        ),
                        onTap: () => {navigateToUserNotifications(context)},
                      )
                    ],
                  ),
                )
              ],
            ),
            key: scaffoldKey,
            body: new Stack(
              children: [
                Scaffold(
                  backgroundColor: bgColorDark,
                  bottomNavigationBar: BottomNavigationBar(
                    backgroundColor: Colors.white,
                    onTap: onTabTapped,
                    // new
                    currentIndex: _currentIndex,
                    //
                    selectedItemColor: Themecolor,
                    unselectedItemColor: Colors.grey,
                    // new
                    type: BottomNavigationBarType.fixed,
                    items: [
                      new BottomNavigationBarItem(
                        icon: Image.asset(AppImages.homeInActiveIcon,height: 25,color: _currentIndex==0?Themecolor:null,),
                        label: 'Home',
                      ),
                      new BottomNavigationBarItem(
                        icon: Image.asset(AppImages.matchInActiveIcon,height: 25,color: _currentIndex==1?Themecolor:null,),

                        label: 'My Contest',
                      ),
                      new BottomNavigationBarItem(
                          icon: Image.asset(AppImages.accountInActiveIcon,height: 25,color: _currentIndex==2?Themecolor:null,),

                          label: 'Account'),
                      new BottomNavigationBarItem(
                          icon: Image.asset(AppImages.moreInActiveIcon,width: 25,height:25,      color: _currentIndex==3?Themecolor:null,),

                          label: 'More'),
                    ],
                  ),
                  body: !isLoading
                      ? _currentIndex == 0 || _currentIndex == 1
                      ? new Stack(
                    children: [
                      sportsList.length > 0
                          ? Scaffold(
                        backgroundColor: _currentIndex == 0?whiteColor:accentColor,
                        body: _currentIndex == 0
                            ? new RefreshIndicator(
                          child: new SingleChildScrollView(
                            physics:
                            AlwaysScrollableScrollPhysics(),
                            child: Padding(
                              padding:
                              const EdgeInsets.all(8.0),
                              child: new Column(
                                children: [
                                  userMatchList.length > 0
                                      ? new Container(
                                    child: new Stack(
                                      children: [
                                        new Container(
                                          width: MediaQuery.of(
                                              context)
                                              .size
                                              .width,
                                          child: Image(
                                              image: AssetImage(
                                                  AppImages
                                                      .bgMyMatchesIcon)),
                                        ),
                                        new Column(
                                          children: [
                                            new Container(
                                              height:
                                              30,
                                              child:
                                              new Row(
                                                mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                                children: [
                                                  new Container(
                                                    margin:
                                                    EdgeInsets.only(left: 10, top: 5),
                                                    child:
                                                    Text(
                                                      'My Matches',
                                                      style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 15),
                                                    ),
                                                  ),
                                                  new GestureDetector(
                                                    behavior:
                                                    HitTestBehavior.translucent,
                                                    child:
                                                    new Container(
                                                      margin: EdgeInsets.only(top: 5),
                                                      alignment: Alignment.center,
                                                      child: new Row(
                                                        children: [
                                                          new Container(
                                                            child: new Text('View All', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 12)),
                                                          ),
                                                          new Container(
                                                            margin: EdgeInsets.only(left: 5, right: 5),
                                                            height: 10,
                                                            width: 10,
                                                            child: Image(
                                                              image: AssetImage(AppImages.moreForwardIcon),
                                                              color: Colors.white,
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    onTap:
                                                        () {
                                                      onTabTapped(1);
                                                    },
                                                  )
                                                ],
                                              ),
                                            ),
                                            new Container(
                                              width: MediaQuery.of(
                                                  context)
                                                  .size
                                                  .width,
                                              height:
                                              128,
                                              child: PageView
                                                  .builder(
                                                itemCount:
                                                userMatchList.length,
                                                controller:
                                                PageController(
                                                  initialPage:
                                                  0,
                                                ),
                                                itemBuilder:
                                                    (BuildContext context,
                                                    int itemIndex) {
                                                  return new UserMyMatchesItemAdapter(
                                                      userMatchList[itemIndex]);
                                                },
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  )
                                      : new Container(),
                                  getBannerWidget(),
                                  SizedBox(
                                    height: 10,
                                  ),



                                  !isMatchLoading
                                      ? matchList.length > 0
                                      ? new Container(
                                    // margin: EdgeInsets.all(8),
                                    child: GetBuilder<HomeMatchesGetController>(
                                        builder: (controller) {
                                          return ListView.builder(
                                              physics:
                                              NeverScrollableScrollPhysics(),
                                              shrinkWrap:
                                              true,
                                              scrollDirection:
                                              Axis
                                                  .vertical,
                                              itemCount:
                                              matchList
                                                  .length,
                                              itemBuilder:
                                                  (BuildContext context,
                                                  int index) {
                                                controller.HomeMinuteTestTimingVariable.add('');
                                                controller.HomeSecTestTimingVariable.add('');
                                                _controller.add(CustomTimerController());

                                                return Visibility(
                                                    visible: "${controller.HomeMinuteTestTimingVariable[index]}:${controller.HomeSecTestTimingVariable[index]}"=="00:00"?false: true,
                                                    child: new MatchItemAdapter(matchList[index],onMatchTimeUp,index,_controller[index],controller));
                                              });


                                          /*return Visibility(
                                                                                    visible:  _currentIndex==0?("${controller.HomeMinuteTestTimingVariable[index]}:${controller.HomeSecTestTimingVariable[index]}"=="00:-1"
                                                                                        ?false: true):true,
                                                                                    child: new MatchItemAdapter(
                                                                                        matchList,
                                                                                        matchList[index],
                                                                                        onMatchTimeUp,
                                                                                        index,_controller[index],controller
                                                                                    ),
                                                                                  );*/

                                        }
                                    ),
                                  )
                                      : new Container(
                                    margin: EdgeInsets
                                        .only(
                                        top:
                                        30),
                                    child:
                                    new Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment
                                          .center,
                                      children: [
                                        // new Container(
                                        //   margin: EdgeInsets
                                        //       .only(
                                        //           top: 5),
                                        //   child:
                                        //       new Text(
                                        //     'Thrilling action and big winning will return!',
                                        //     style: TextStyle(
                                        //         color: Colors
                                        //             .grey,
                                        //         fontSize:
                                        //             14,
                                        //         fontWeight:
                                        //             FontWeight.w400),
                                        //   ),
                                        // ),
                                        new Container(
                                          padding:
                                          EdgeInsets.all(
                                              20),
                                          child:
                                          new ConstrainedBox(
                                            constraints:
                                            new BoxConstraints(
                                              maxHeight:
                                              200.0,
                                              maxWidth:
                                              200.0,
                                            ),
                                            child:
                                            Image(
                                              image: AssetImage(sportsList[_currentSportIndex].sport_key == 'CRICKET'
                                                  ? AppImages.cricPlayerIcon
                                                  : sportsList[_currentSportIndex].sport_key == 'LIVE'
                                                  ? AppImages.cricPlayerIcon
                                                  : sportsList[_currentSportIndex].sport_key == 'FOOTBALL'
                                                  ? AppImages.footPlayerIcon
                                                  : sportsList[_currentSportIndex].sport_key == 'BASKETBALL'
                                                  ? AppImages.baskPlayerIcon
                                                  : sportsList[_currentSportIndex].sport_key == 'BASEBALL'
                                                  ? AppImages.basePlayerIcon
                                                  : sportsList[_currentSportIndex].sport_key == 'HANDBALL'
                                                  ? AppImages.handPlayerIcon
                                                  : sportsList[_currentSportIndex].sport_key == 'HOCKEY'
                                                  ? AppImages.hockeyPlayerIcon
                                                  : AppImages.kabPlayerIcon),
                                            ),
                                          ),
                                        ),
                                        new Container(
                                          margin: EdgeInsets
                                              .only(
                                              top: 5),
                                          child:
                                          new Text(
                                            'There are no upcoming matches as of now',
                                            textAlign:
                                            TextAlign.center,
                                            style: TextStyle(
                                                color: Colors
                                                    .grey,
                                                fontSize:
                                                14,
                                                fontWeight:
                                                FontWeight.w400),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                      : new Container(
                                    margin:
                                    EdgeInsets.all(
                                        8),
                                    child: ListView
                                        .builder(
                                        physics:
                                        NeverScrollableScrollPhysics(),
                                        shrinkWrap:
                                        true,
                                        scrollDirection:
                                        Axis
                                            .vertical,
                                        itemCount:
                                        7,
                                        itemBuilder:
                                            (BuildContext
                                        context,
                                            int index) {
                                          return new ShimmerMatchItemAdapter();
                                        }),
                                  )
                                ],
                              ),
                            ),
                          ),
                          onRefresh: _pullRefresh,
                        )
                            : new MyMatches(
                            pullRefresh: _myMatchPullRefresh,
                            matchListResponse: matchListResponsee,
                            finishMatchListResponse: finishMatchListResponse,
                            isMyMatchLoading: isMyMatchLoading,
                            getMyMatchFinishList: getMyMatchFinishList),
                      )
                          : new Container(),
                    ],
                  )
                      : _currentIndex == 2
                      ? !isAccountLoading
                      ? new AccountNNew(userId)
                      : new Container()
                      : _currentIndex == 3
                      ? new More(_packageInfo)
                      : new Container()
                      : new Container(),
                ),
                _currentIndex == 0 && back_dialog
                    ? AlertDialog(
                  title: Text("Exit"),
                  content: Text("Do you want to Exit?"),
                  actions: [
                    cancelButton(context),
                    continueButton(context),
                  ],
                )
                    : new Container(),
                _currentIndex == 0 && logout_dialog
                    ? new Container(
                  color: Colors.black38,
                  child: AlertDialog(
                    title: Text("Logout"),
                    content: Text("Do you want to logout?"),
                    actions: [
                      cancelLogButton(context),
                      continueLogButton(context),
                    ],
                  ),
                )
                    : new Container(),
                isLoading ? AppLoaderProgress() : new Container(),

              ],
            ),
          ),
          isDownloading?Scaffold(backgroundColor:Colors.transparent,body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width ,
            color: Colors.black.withOpacity(0.6),
            padding: EdgeInsets.all(30),
            alignment: Alignment.center,
            child: Container(
              height: 110,

              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(bottom: 30),
                    child: Text('Downloading file. Please wait...',style: TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 16),),
                  ),
                  LinearProgressIndicator(value: double.parse(progress)<10?double.parse('.0'+progress):double.parse('.'+progress),backgroundColor: mediumGrayColor,valueColor: AlwaysStoppedAnimation<Color>(Themecolor,),),
                  Container(
                    margin: EdgeInsets.only(top: 5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(progress+'%',style: TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 16),),
                        ),
                        Container(
                          child: Text(progress+'/100',style: TextStyle(color: Colors.black,fontWeight: FontWeight.w400,fontSize: 16),),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),):Container()
        ],
      ),
    );
  }

  _onShare(BuildContext context) async {
    String shareBody = "Here's " +
        AppConstants.rupee +
        " " +
        AppConstants.invite_bonus +
        " to play fantasy cricket with me On " +
        _packageInfo.appName +
        " Click " +
        AppConstants.apk_url +
        " to download " +
        _packageInfo.appName +
        " app & use My code " +
        userReferCode +
        " To Register";
    final RenderBox box = context.findRenderObject() as RenderBox;
    await Share.share(shareBody,
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }

  void onTabTapped(int index) {
    if (index == 0&&_currentIndex!=0) {
      title = '';
      getData();
      getUserBalance();
    }
    else if (index == 1&&_currentIndex!=1) {
      title = 'My Contest';
      getMyUpcomingMatchList(0);
    }
    /* else if (index == 2) {
      return;
    } */
    else if (index == 2&&_currentIndex!=2) {
      isLoading = false;
      title = 'Account';
      // getLevelData();
    } else if (index == 3&&_currentIndex!=3) {
      isLoading = false;
      title = 'More';
    }
    setState(() {
      _currentIndex = index;
    });
  }

  Widget cancelButton(context) {
    return TextButton(
      child: Text("NO"),
      onPressed: () {
        setState(() {
          back_dialog = false;
        });
      },
    );
  }

  Widget continueButton(context) {
    return TextButton(
      child: Text("YES"),
      onPressed: () {
        SystemNavigator.pop();
      },
    );
  }

  Widget cancelLogButton(context) {
    return TextButton(
      child: Text("NO"),
      onPressed: () {
        setState(() {
          logout_dialog = false;
        });
      },
    );
  }

  Widget continueLogButton(context) {
    return TextButton(
      child: Text("YES"),
      onPressed: () {
        AppPrefrence.clearPrefrence();
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => new LoginNew()),
            ModalRoute.withName("/main"));
      },
    );
  }

  Future<bool> _onWillPop() async {
    if (_currentIndex == 0 && !isDownloading) {
      setState(() {
        back_dialog = true;
      });
    }
    return Future.value(false);
  }

  Widget getBannerWidget() {

    return new Container(
      // margin: EdgeInsets.only(top: 8),
        child: CarouselSlider.builder(
          options: CarouselOptions(
            height: 90,
            viewportFraction: 1,
            autoPlay: true,
          ),
          itemCount: bannerList.length,
          itemBuilder: (context, itemIndex, realIndex) {
            return Builder(
              builder: (BuildContext context) {
                return Container(
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    child: GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: new ClipRRect(
                          borderRadius: BorderRadius.circular(5),
                          child: new Container(
                            color: backgroundColor,
                            child: Image.network(
                              bannerList[itemIndex].image!,
                              fit: BoxFit.fill,
                              width: MediaQuery.of(context).size.width,
                              height: 60,
                            ),
                          ),
                        ),
                        onTap: () {
                          if (bannerList[itemIndex].type == 'addcash') {
                            navigateToAddBalance(
                                context, bannerList[itemIndex].offer_code!);
                          } else if (bannerList[itemIndex].type == 'match') {
                            navigateToUpcomingContests(
                                context,
                                new GeneralModel(
                                    bannerImage: bannerList[itemIndex]
                                        .match_details!
                                        .banner_image,
                                    matchKey: bannerList[itemIndex]
                                        .match_details!
                                        .matchkey,
                                    teamVs: (bannerList[itemIndex]
                                        .match_details!
                                        .team1display! +
                                        ' VS ' +
                                        bannerList[itemIndex]
                                            .match_details!
                                            .team2display!),
                                    firstUrl: bannerList[itemIndex]
                                        .match_details!
                                        .team1logo,
                                    secondUrl: bannerList[itemIndex]
                                        .match_details!
                                        .team2logo,
                                    headerText: bannerList[itemIndex]
                                        .match_details!
                                        .time_start,
                                    sportKey: bannerList[itemIndex]
                                        .match_details!
                                        .sport_key,
                                    battingFantasy: bannerList[itemIndex]
                                        .match_details!
                                        .battingfantasy,
                                    bowlingFantasy: bannerList[itemIndex]
                                        .match_details!
                                        .bowlingfantasy,
                                    liveFantasy: bannerList[itemIndex]
                                        .match_details!
                                        .livefantasy,
                                    secondInningFantasy: bannerList[itemIndex]
                                        .match_details!
                                        .secondinning,
                                    reverseFantasy: bannerList[itemIndex]
                                        .match_details!
                                        .reversefantasy,
                                    fantasySlots:
                                    bannerList[itemIndex].match_details!.slotes,
                                    team1Name: bannerList[itemIndex]
                                        .match_details!
                                        .team1name,
                                    team2Name: bannerList[itemIndex]
                                        .match_details!
                                        .team2name,
                                    team1Logo: bannerList[itemIndex]
                                        .match_details!
                                        .team1logo,
                                    team2Logo: bannerList[itemIndex]
                                        .match_details!
                                        .team2logo));
                          } else if (bannerList[itemIndex].type == 'leaderboard') {
                            navigateToHomeLeaderboard(context,
                                seriesId: int.parse(
                                    (bannerList[itemIndex].series_id ?? 0)
                                        .toString()));
                          } else if (bannerList[itemIndex].type == 'withdraw') {
                            navigateToWithdrawCash(context, type: 'paytm_instant');
                          } else if (bannerList[itemIndex].link!.isNotEmpty) {
                            _launchURL(bannerList[itemIndex].link!);
                          } else if (bannerList[itemIndex].type!.isNotEmpty) {
                            navigateToReferAndEarn(context);
                          }
                        }));
              },
            );
          },
        ));
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void getUserBalance() async {
    GeneralRequest loginRequest =
    new GeneralRequest(user_id: userId, fcmToken: '');
    final client = ApiClient(AppRepository.dio);
    MyBalanceResponse myBalanceResponse =
    await client.getUserBalance(loginRequest);
    if (myBalanceResponse.status == 1) {
      MyBalanceResultItem myBalanceResultItem = myBalanceResponse.result![0];
      AppPrefrence.putString(AppConstants.KEY_USER_BALANCE,
          myBalanceResultItem.balance.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_WINING_AMOUNT,
          myBalanceResultItem.winning.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_BONUS_BALANCE,
          myBalanceResultItem.bonus.toString());
      AppPrefrence.putString(AppConstants.KEY_USER_TOTAL_BALANCE,
          myBalanceResultItem.total.toString());
      balance = myBalanceResultItem.total.toString();
      level = myBalanceResultItem.level.toString();
    }
    // Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {});
  }

  void getBannerList() async {



    setState(() {
      isLoading = true;
    });
    GeneralRequest loginRequest =
    new GeneralRequest(user_id: userId, fcmToken: '');
    final client = ApiClient(AppRepository.dio);
    BannerListResponse bannerListResponse = await client.getBannerList(loginRequest);
    debugPrint('My_Data_2');
    // print('My response is');
    // print(bannerListResponse)
    if (bannerListResponse.status == 1) {
      bannerList = bannerListResponse.result!;
      sportsList = bannerListResponse.visible_sports!;
      notification = bannerListResponse.notification!;
      onlineVersion = bannerListResponse.version!;
      AppConstants.apk_refer_url =bannerListResponse.refer_url!;
      AppConstants.apk_url = bannerListResponse.app_download_url!;
      AppPrefrence.putInt(AppConstants.IS_VISIBLE_AFFILIATE,
          bannerListResponse.is_visible_affiliate!);
      AppPrefrence.putInt(AppConstants.IS_VISIBLE_PROMOTE,
          bannerListResponse.is_visible_promote);
      AppPrefrence.putString(AppConstants.IS_VISIBLE_PROMOTER_LEADERBOARD,
          bannerListResponse.is_visible_promoter_leaderboard.toString());
      AppPrefrence.putInt(AppConstants.IS_VISIBLE_PROMOTER_REQUEST,
          bannerListResponse.is_visible_promoter_requested);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_TEAM_NAME,
          bannerListResponse.team);
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_STATE_NAME,
          bannerListResponse.state);
      controller = TabController(vsync: this, length: sportsList.length);
      if (bannerListResponse.popup_status == 1) {
        if (isBannerShow(bannerListResponse.poup_time ?? 0) == 1) {
          _showPopImage(
              bannerListResponse.popup_image,
              bannerListResponse.is_popup_redirect,
              bannerListResponse.popup_link,
              bannerListResponse.popup_redirect_type,
              bannerListResponse.popup_type,
              bannerListResponse.match_details,
              bannerListResponse.popup_offer_code,
              bannerListResponse.popup_series_id);
        }
      }


      if(int.parse(AppConstants.versionCode) < onlineVersion && Platform.isAndroid){
        _UpdateDialog(onlineVersion);
      }

      if (bannerListResponse.team!.isEmpty || bannerListResponse.state!.isEmpty) {
        navigateToSetTeamName(context);
      }
      // if(Platform.isAndroid){
      //   if(int.parse(AppConstants.versionCode) < bannerListResponse.version_code!){
      //     //bannerListResponse.version_code!
      //     if (await Permission.storage.request().isGranted) {
      //       _UpdateDialog(bannerListResponse.version_code!);
      //     }
      //
      //   }
      // }
      getMatchList();
    }
    // Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
      isLoading = false;
    });
  }

  int isBannerShow(int? poup_time) {
    if (previousTime != 0) {
      DateTime previous = DateTime.fromMillisecondsSinceEpoch(previousTime);
      if (MethodUtils.getDateTime().difference(previous).inHours > poup_time!) {
        AppPrefrence.putInt(AppConstants.PREVIOUS_DATE,
            MethodUtils.getDateTime().millisecondsSinceEpoch);
        return 1;
      }
      return 0;
    }
    AppPrefrence.putInt(AppConstants.PREVIOUS_DATE,
        MethodUtils.getDateTime().millisecondsSinceEpoch);
    return 1;
  }

  void getMatchList() async {

    setState(() {
      isMatchLoading = true;
    });
    homeMatchesGetController.HomeMinuteTestTimingVariable.clear();
    homeMatchesGetController.HomeSecTestTimingVariable.clear();

    GeneralRequest loginRequest = new GeneralRequest(
        user_id: userId, sport_key: sportsList[_currentSportIndex].sport_key);
    final client = ApiClient(AppRepository.dio);
    MatchListResponse matchListResponse =
    await client.getMatchList(loginRequest);
    if (matchListResponse.status == 1) {
      matchList = matchListResponse.result!;
      userMatchList = matchListResponse.users_matches!;
    }
    // Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
      isLoading = false;

      isMatchLoading = false;
    });
  }

  Future<void> _pullRefresh() async {
    getMatchList();
  }

  void _showPopImage(
      String? popup_image,
      int? is_popup_redirect,
      String? popup_link,
      String? popup_redirect_type,
      String? popup_type,
      MatchDetails? matchDetails,
      var popup_offer_code,
      var series_id) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return new Container(
            alignment: Alignment.center,
            height: MediaQuery.of(context).size.height * 0.65,
            child: new Stack(
              alignment: Alignment.topRight,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                      margin: EdgeInsets.only(left: 10, right: 10, top: 20),
                      child: new Card(
                        child: CachedNetworkImage(
                          fit: BoxFit.contain,
                          height: MediaQuery.of(context).size.height * 0.65,
                          width: MediaQuery.of(context).size.width * 0.85,
                          imageUrl: popup_image!,
                          placeholder: (context, url) => Image.asset(
                            AppImages.popupAvatar,
                            fit: BoxFit.contain,
                            height: 100,
                          ),
                          errorWidget: (context, url, error) =>
                              Image.asset(
                                AppImages.popupAvatar,
                                fit: BoxFit.contain,
                                height: 100,
                              )
                        ),
                      )),
                  onTap: () {
                    if (is_popup_redirect == 1) {
                      if (popup_redirect_type == 'addcash') {
                        navigateToAddBalance(context, popup_offer_code);
                      }
                      else if (popup_redirect_type == 'match') {
                        navigateToUpcomingContests(
                            context,
                            new GeneralModel(
                                bannerImage: matchDetails!.banner_image,
                                matchKey: matchDetails.matchkey,
                                teamVs: (matchDetails.team1display! +
                                    ' VS ' +
                                    matchDetails.team2display!),
                                firstUrl: matchDetails.team1logo,
                                secondUrl: matchDetails.team2logo,
                                headerText: matchDetails.time_start,
                                sportKey: matchDetails.sport_key,
                                battingFantasy: matchDetails.battingfantasy,
                                bowlingFantasy: matchDetails.bowlingfantasy,
                                liveFantasy: matchDetails.livefantasy,
                                secondInningFantasy: matchDetails.secondinning,
                                reverseFantasy: matchDetails.reversefantasy,
                                fantasySlots: matchDetails.slotes,
                                team1Name: matchDetails.team1name,
                                team2Name: matchDetails.team2name,
                                team1Logo: matchDetails.team1logo,
                                team2Logo: matchDetails.team2logo));
                      } else if (popup_redirect_type == 'leaderboard') {
                        navigateToHomeLeaderboard(context,
                            seriesId: int.parse((series_id ?? 0).toString()));
                      } else if (popup_redirect_type == 'withdraw') {
                        navigateToWithdrawCash(context, type: 'paytm_instant');
                      } else if (popup_link!.isNotEmpty) {
                        navigateToVisionWebView(context, '', popup_link);
                      } else if (popup_redirect_type!.isNotEmpty) {
                        navigateToReferAndEarn(context);
                      }
                    }
                  },
                ),
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  child: new Container(
                    height: 40,
                    width: 40,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 2),
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.black),
                    child: Icon(
                      Icons.clear,
                      color: Colors.white,
                    ),
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
              ],
            ),
          );
        });
  }

  void getMyMatchFinishList(int index) async {
    _currentMatchIndex = index;
    setState(() {
      isMyMatchLoading = true;
    });

    GeneralRequest loginRequest = new GeneralRequest(
        user_id: userId,
        sport_key: sportsList[_currentSportIndex].sport_key,
        page: "0"
    );

    final client = ApiClient(AppRepository.dio);

    finishMatchListResponse = await client.getMyMatchFinishList(loginRequest);

    matchListResponsee.total_live_match = finishMatchListResponse.total_live_match;
    matchListResponsee.result = finishMatchListResponse.result!.data;
    setState(() {
      isMyMatchLoading = false;
    });
  }

  // void getLevelData() async {
  //   setState(() {
  //     isAccountLoading = true;
  //   });
  //   AppLoaderProgress.showLoader(context);
  //   GeneralRequest loginRequest = new GeneralRequest(user_id: userId);
  //   final client = ApiClient(AppRepository.dio);
  //   levelDataResponse = await client.getLevelData(loginRequest);
  //   AppLoaderProgress.hideLoader(context);
  //   AppPrefrence.putString(AppConstants.KEY_USER_BALANCE,
  //       levelDataResponse.result!.balance_history!.balance);
  //   AppPrefrence.putString(AppConstants.KEY_USER_WINING_AMOUNT,
  //       levelDataResponse.result!.balance_history!.winning);
  //   AppPrefrence.putString(AppConstants.KEY_USER_BONUS_BALANCE,
  //       levelDataResponse.result!.balance_history!.bonus);
  //   AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_BANK_VERIFY_STATUS,
  //       levelDataResponse.result!.balance_history!.bank_verify);
  //   AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_PAN_VERIFY_STATUS,
  //       levelDataResponse.result!.balance_history!.pan_verify);
  //   AppPrefrence.putInt(
  //       AppConstants.SHARED_PREFERENCE_USER_MOBILE_VERIFY_STATUS,
  //       levelDataResponse.result!.balance_history!.mobile_verify);
  //   AppPrefrence.putInt(AppConstants.SHARED_PREFERENCE_USER_EMAIL_VERIFY_STATUS,
  //       levelDataResponse.result!.balance_history!.email_verify);
  //   isAccountLoading = false;
  //   setState(() {});
  // }

  void getMyUpcomingMatchList(int index) async {
    _currentMatchIndex = index;
    setState(() {
      isMyMatchLoading = true;
    });
    final upcomingMyMatchesGetController=Get.put(UpcomingMyMatchesGetController());
    upcomingMyMatchesGetController.SecTestTimingVariable.clear();
    upcomingMyMatchesGetController.MinuteTestTimingVariable.clear();
    GeneralRequest loginRequest = new GeneralRequest(
        user_id: userId, sport_key: sportsList[_currentSportIndex].sport_key);
    final client = ApiClient(AppRepository.dio);
    MatchListResponse matchListResponse =
    await client.getMyMatchLiveList(loginRequest);
    matchListResponsee = transform(matchListResponse, index);
    // Fluttertoast.showToast(msg: myBalanceResponse.message!, toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
    setState(() {
      isMyMatchLoading = false;
    });
  }

  MatchListResponse transform(MatchListResponse response, int index) {
    int liveMatchCount = 0;
    MatchListResponse matchListResponse = new MatchListResponse();
    matchListResponse.status = response.status;
    matchListResponse.message = response.message;
    List<MatchDetails> matches = [];
    for (int i = 0; i < response.result!.length; i++) {
      if (index == 0
          ? response.result![i].match_status_key ==
          AppConstants.KEY_UPCOMING_MATCH
          : response.result![i].match_status_key ==
          AppConstants.KEY_LIVE_MATCH) {
        matches.add(response.result![i]);
      }
      if (response.result![i].match_status_key == AppConstants.KEY_LIVE_MATCH) {
        liveMatchCount++;
      }
    }
    matchListResponse.total_live_match = liveMatchCount;
    matchListResponse.result = matches;
    return matchListResponse;
  }

  void onMatchTimeUp() {
    setState(() {
    });
  }



  void _UpdateDialog(int versionCode) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return WillPopScope(
            child: Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Container(
                height: 140,
                padding: EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: Text(
                        'New Version Code ' +
                            (versionCode.toString()) +
                            ' Is Available For Download. Kindly Update For Latest Features.',
                        style: TextStyle(
                          height: 1.6,
                            color: Colors.black,
                            fontWeight: FontWeight.w400,
                            fontSize: 16),
                      ),
                    ),
                    Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: Container(
                            decoration: BoxDecoration(color: Themecolor, borderRadius: BorderRadius.circular(40)),
                            
                            padding: EdgeInsets.symmetric(horizontal: 35,vertical: 10),
                            margin: EdgeInsets.only(top: 20),
                            child: Text(
                              'Update',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500,
                                fontSize: 16,
                              ),
                            ),
                          ),
                          onTap: () async {
                            setState(() {
                              isDownloading = true;
                            });
                            var tempDir = await getTemporaryDirectory();
                            fullPath = tempDir.path + "/everplay.apk";
                            download2(dio, AppConstants.apk_url + 'everplay.apk', fullPath);
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            onWillPop: () async {
              return false;
            });
      },
    );
  }


  Future download2(Dio.Dio dio, String url, String savePath) async {
    try {
      print(url);
      Dio.Response response = await dio.get(
        url,
        onReceiveProgress: showDownloadProgress,
        //Received data with List<int>
        options: Dio.Options(
            responseType: Dio.ResponseType.bytes,
            followRedirects: false,
            validateStatus: (status) {
              return (status ?? 0) < 500;
            }),
      );
      print(response.headers);
      File file = File(savePath);
      // File file = File(savePath);
      var raf = file.openSync(mode: FileMode.write);
      // response.data is List<int> type
      raf.writeFromSync(response.data);
      await raf.close();
    } catch (e) {
      print(e);
    }
  }

  void showDownloadProgress(received, total) async{
    if (total != -1) {
      setState(() {
        progress = (received / total * 100).toStringAsFixed(0);
        if (double.parse(progress) >= 100) {
          isDownloading = false;
        }
      });
      if (!isDownloading) {
        if (await Permission.mediaLibrary.request().isGranted) {
          AppInstaller.installApk(fullPath);
        }
      }
    }
  }
}

