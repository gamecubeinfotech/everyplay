

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/widgets.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/refer_list_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

import 'AffiliateProgram.dart';

class ReferList extends StatefulWidget {
  @override
  _ReferListState createState() => _ReferListState();
}

class _ReferListState extends State<ReferList> {
  TextEditingController searchController = TextEditingController();
  late String userId='0';
  String? totalEarn='0';
  late String totalUser='0';
  List<ReferLIstItem> referList=[];
  var val;
  var groupValue;
  bool searchVisible=false;
  int selected=0;
  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getReferBonusList();
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: darkPrimaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.light) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color:Colors.black,
            // padding: EdgeInsets.only(top: 40),
            child: new Stack(
              children: [
                new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () => {Navigator.pop(context)},
                          child: new Container(
                            padding: EdgeInsets.all(15),
                            alignment: Alignment.centerLeft,
                            child: new Container(
                              width: 16,
                              height: 16,
                              child: Image(
                                image: AssetImage(AppImages.backImageURL),
                                fit: BoxFit.fill,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        new Container(
                          child: new Text('Refer List',
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 16)),
                        ),
                      ],
                    ),
                    // new GestureDetector(
                    //   behavior: HitTestBehavior.translucent,
                    //   onTap: () => {
                    //     setState((){
                    //       searchVisible=true;
                    //     })
                    //   },
                    //   child: new Container(
                    //     padding: EdgeInsets.all(15),
                    //     alignment: Alignment.centerLeft,
                    //     child: new Container(
                    //       width: 20,
                    //       height: 20,
                    //       child: Icon(Icons.search,color: Colors.black,),
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
                searchVisible?new Container(
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(10),
                  child: new Container(
                    decoration: new BoxDecoration (
                        borderRadius: new BorderRadius.all(new Radius.circular(30.0)),
                        color: bgColor
                    ),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () => {Navigator.pop(context)},
                          child: new Container(
                            margin: EdgeInsets.only(left: 10,bottom: 5),
                            alignment: Alignment.centerLeft,
                            child: new Container(
                              width: 20,
                              height: 20,
                              child: Icon(Icons.search,color: Colors.grey,),
                            ),
                          ),
                        ),
                        new Container(
                          width: MediaQuery.of(context).size.width-80,
                          height: 40,
                          child: TextField(
                            controller: searchController,
                            textInputAction: TextInputAction.search,
                            onSubmitted: (value) {
                              getReferBonusList();
                            },
                            decoration: InputDecoration(
                              counterText: "",
                              fillColor: Colors.transparent,
                              filled: true,
                              hintStyle: TextStyle(
                                fontSize: 12,
                                color: Colors.grey,
                                fontStyle: FontStyle.normal,
                                fontWeight: FontWeight.normal,
                              ),
                              hintText: 'Search',
                              enabledBorder: const OutlineInputBorder(
                                // width: 0.0 produces a thin "hairline" border
                                borderSide:
                                const BorderSide(color: Colors.transparent, width: 0.0),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                const BorderSide(color: Colors.transparent, width: 0.0),
                              ),
                            ),
                          ),
                        ),
                        new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: Container(
                              height: 50,
                              width: 20,
                              margin: EdgeInsets.only(right: 10),
                              child: Icon(Icons.clear, size: 20,color: Colors.black,), ),
                            onTap: () {
                              setState(() {
                                if(searchController.text==''){
                                  FocusScope.of(context).unfocus();
                                  searchVisible=false;
                                }else{
                                  searchController.text='';
                                  getReferBonusList();
                                }
                              });
                            }
                        )
                      ],
                    ),
                  ),
                ):new Container()
              ],
            ),
          ),
        ),
        body: new Container(
          child: new Column(
            children: [
              /* new Container(
                color:Colors.white,
                // margin: EdgeInsets.all(10),
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap:(){
                          if(selected!=0){
                            selected=0;
                            getReferBonusList();
                            setState(() {

                            });
                          }
                        },
                        child: new Container(
                          width:150,
                          height: 40,
                          decoration: BoxDecoration(
                              color:selected==0?Colors.blue:Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: selected==0?Colors.blue:Colors.grey)

                          ),
                          child: Center(
                            child: new Text('Refer List',
                                style: TextStyle(
                                    fontFamily: AppConstants.textBold,
                                    color:selected==0?Colors.white:Colors.grey,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14)),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          selected=1;
                          setState(() {

                          });
                        },
                        child: new Container(
                          width:150,
                          height: 40,
                          decoration: BoxDecoration(
                              color:selected==1?Colors.blue:Colors.white,
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: selected==1?Colors.blue:Colors.grey)

                          ),
                          child: Center(
                            child: new Text('Earning',
                                style: TextStyle(
                                    fontFamily: AppConstants.textBold,
                                    color:selected==1?Colors.white:Colors.grey,
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14)),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),*/
              //SizedBox(height: 15,),
              selected==0? Expanded(
                child: Column(
                  children: [
                    SizedBox(height: 15,),
                    Container(
                      decoration: BoxDecoration(color: Themecolor),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10,left: 16,right: 16,bottom: 10),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Container(
                                  child: new Text('Earned Through Friends',
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: whiteColor,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 16)),
                                ),
                                new Container(
                                  child: new Text('₹'+totalEarn!,
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: whiteColor,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14)),
                                ),
                              ],
                            ),
                          ),
                          Container(margin: EdgeInsets.symmetric(horizontal: 13),child: new Divider(height: 2,thickness: 1,color : whiteColor)),
                          Padding(
                            padding: const EdgeInsets.only(top: 10,left: 16,right: 16,bottom: 10),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Container(
                                  child: new Text('Total Refer',
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: whiteColor,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14)),
                                ),
                                new Container(
                                  child: new Text(totalUser,
                                      style: TextStyle(
                                          fontFamily: AppConstants.textBold,
                                          color: whiteColor,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 14)),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),

                    Divider(height: 2,thickness: 2),
                    SizedBox(height: 10,),
                    Expanded(
                      child: SingleChildScrollView(
                        child: new Column(
                          children: List.generate(

                            referList.length,
                                (index) => new Column(
                              children: [
                                new Container(

                                  padding: EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 10),
                                  decoration: BoxDecoration(
                                      color:Colors.white,
                                      border: Border(

                                          bottom: BorderSide(
                                              color: Colors.grey.shade300
                                          )
                                      )
                                  ),
                                  child: new Row(
                                    children: [
                                      new Image.asset(AppImages.defaultAvatarIcon,
                                        fit: BoxFit.fill,scale: 3,),
                                      SizedBox(width: 10,),
                                      new Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          new Text(
                                            referList[index].name==''?referList[index].email!:referList[index].name!,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                                // Divider(height: 0,color: Colors.grey,),
                              ],
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ):Expanded(child: AffiliateProgram()),
            ],
          ),
        ),
      ),
    );
  }
  void getReferBonusList() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,search_text: searchController.text.toString(),page: '0');
    final client = ApiClient(AppRepository.dio);
    ReferBonusListResponse referBonusListResponse = await client.getReferBonusList(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if(referBonusListResponse.status==1){
      totalEarn=referBonusListResponse.total_amount.toString();
      referList=referBonusListResponse.result!;
      totalUser=referBonusListResponse.total_user!.toString();
      setState(() {});
    }

  }
}