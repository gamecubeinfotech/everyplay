import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/dataModels/GeneralModel.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/contest_request.dart';
import 'package:EverPlay/repository/model/player_points_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class PlayerPoints extends StatefulWidget {
  List<MultiSportsPlayerPointItem> list;
  GeneralModel model;
  PlayerPoints(this.list,this.model);

  @override
  _PlayerPointsState createState() => new _PlayerPointsState();
}

class _PlayerPointsState extends State<PlayerPoints> {


  @override
  void initState() {
    super.initState();
  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Container(
        child: new Column(
          children: [
            // new Container(
            //   height: 30,
            //   padding: EdgeInsets.only(left: 10,right: 10),
            //   alignment: Alignment.center,
            //   child: new Text('Showing Player Stats by Match',
            //       style: TextStyle(
            //           color: Colors.grey,
            //           fontWeight: FontWeight.normal,
            //           fontSize: 12)),
            // ),
            // SizedBox(height: 5,),
             new Divider(height: 1,thickness: 1,),
            new Container(
              height: 40,
              color: Color(0xFFf8f9fb),
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  new Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Text(
                      'Players',
                      style: TextStyle(
                          color: Colors.grey,
                          fontWeight: FontWeight.w500,
                          fontSize: 12),
                    ),
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      // new Container(
                      //   width: 1,
                      //   height: 40,
                      //   margin: EdgeInsets.only(right: 20),
                      //   alignment: Alignment.center,
                      //   color: lightGrayColor3,
                      // ),
                      new Container(
                        margin: EdgeInsets.only(right: 16,left: 0),
                        width: 50,

                        alignment: Alignment.centerLeft,
                        child: Text(
                          '% Sel By',
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.w400,
                              fontSize: 11),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only(right: 10,left: 0),
                        width: 50,
                        alignment: Alignment.center,
                        child: Text(
                          'Points ',
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.w400,
                              fontSize: 11),
                        ),
                      ),

                    ],
                  ),
                ],
              ),
            ),
            new Divider(height: 1,thickness: 1,),
            new ListView.builder(
                padding: EdgeInsets.only(top: 0),
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                // scrollDirection: Axis.vertical,
                itemCount: widget.list.length,
                itemBuilder: (BuildContext context, int index) {
                  return new GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    child: new Column(
                      children: [
                        new Container(
                          padding: EdgeInsets.symmetric(vertical: 10),
                          color: widget.list[index].isSelected==1?TextFieldColor:Colors.white,
                          child: new Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  new Container(
                                    margin: EdgeInsets.only(right: 5,left: 5,top: 0),
                                    height: 45,
                                    width: 45,
                                    child:  CachedNetworkImage(
                                      imageUrl: widget.list[index].image!,
                                      placeholder: (context,url)=> new Image.asset(AppImages.userAvatarIcon,fit: BoxFit.fill,),
                                      errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,fit: BoxFit.fill,),
                                    ),
                                  ),
                                  new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      new Container(
                                          width: 150,
                                          padding: EdgeInsets.only(top: 0,bottom: 0),
                                          child: new Text(
                                            widget.list[index].player_name!,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500),
                                          )),
                                      new Row(
                                        children: [
                                          new Container(
                                            margin: EdgeInsets.only(top: 2,right: 7),
                                            child: new Text(
                                              widget.list[index].short_name!=null&&widget.list[index].role!=null?widget.list[index].short_name!+'-'+widget.list[index].role!:'',
                                              style: TextStyle(
                                                  color: Colors.grey,
                                                  fontSize: 12,
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                        ],
                                      ),
                                     Row(
                                       children: [
                                         widget.list[index].isSelected==1? new Container(
                                           margin: EdgeInsets.only(right: 5,top: 2,bottom: 2),
                                           height: 16,
                                           width: 16,
                                           child: Image(
                                             image: AssetImage(
                                               AppImages.circleTickIcon,),),
                                         ):new Container(),
                                         // Row(
                                         //   children: [
                                         //     widget.list[index].isSelected==1? new Container(
                                         //       margin: EdgeInsets.only(right: 5,top: 2,bottom: 2),
                                         //       height: 16,
                                         //       width: 16,
                                         //       child: Image(
                                         //         image: AssetImage(
                                         //           AppImages.circleTickIcon,),),
                                         //     ):new Container(),
                                         //   ],
                                         // ),
                                       ],
                                     ),




                                    ],
                                  )
                                ],
                              ),

                              new Row(
                                children: [
                                  // new Container(
                                  //   width: 1,
                                  //   height: 60,
                                  //   margin: EdgeInsets.only(right: 20),
                                  //   alignment: Alignment.center,
                                  //   color: lightGrayColor3,
                                  // ),

                                  new Container(
                                    margin: EdgeInsets.only(right: 10,left: 0),
                                    width: 50,
                                    alignment: Alignment.centerLeft,
                                    child: Text(
                                      widget.list[index].selected_by!,
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontWeight: FontWeight.w400,
                                          fontSize: 12),
                                    ),
                                  ),

                                  new Container(
                                    margin: EdgeInsets.only(right: 10,left: 0),
                                    width: 50,

                                    alignment: Alignment.center,
                                    child: Text(
                                      widget.list[index].points.toString(),
                                      style: TextStyle(
                                          color: Colors.black,
                                          // fontWeight: FontWeight.bold,
                                          fontSize: 12),
                                    ),
                                  ),


                                ],
                              ),
                            ],
                          ),
                        ),
                        new Divider(height: 1,thickness: 1,),
                      ],
                    ),
                    onTap: (){
                      navigateToBreakupPlayerPoints(context,widget.list[index]);
                    },
                  );
                }
            ),
          ],
        )
    );
  }
}



























// import 'package:cached_network_image/cached_network_image.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:EverPlay/appUtilities/app_colors.dart';
// import 'package:EverPlay/appUtilities/app_constants.dart';
// import 'package:EverPlay/appUtilities/app_images.dart';
// import 'package:EverPlay/appUtilities/app_navigator.dart';
// import 'package:EverPlay/repository/model/player_points_response.dart';
//
// class PlayerPoints extends StatefulWidget {
//   List<MultiSportsPlayerPointItem> list;
//   PlayerPoints(this.list);
//
//   @override
//   _PlayerPointsState createState() => new _PlayerPointsState();
// }
//
// class _PlayerPointsState extends State<PlayerPoints> {
//
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//
//   @override
//   void dispose() {
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: new Container(
//           child: new Column(
//             children: [
//               new Container(
//                 height: 30,
//                 padding: EdgeInsets.only(left: 10,right: 10),
//                 alignment: Alignment.center,
//                 child: new Text('Showing Player Stats by Match',
//                     style: TextStyle(
//                         color: Colors.grey,
//                         fontWeight: FontWeight.normal,
//                         fontSize: 12)),
//               ),
//               new Divider(height: 1,thickness: 1,),
//               new Container(
//                 height: 30,
//                 color: Colors.white,
//                 child: new Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: [
//                     new Container(
//                       margin: EdgeInsets.only(left: 10),
//                       child: Text(
//                         'Players',
//                         style: TextStyle(
//                             color: Colors.grey,
//                             fontWeight: FontWeight.w500,
//                             fontSize: 12),
//                       ),
//                     ),
//                     new Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         new Container(
//                           width: 1,
//                           height: 30,
//                           margin: EdgeInsets.only(right: 20),
//                           alignment: Alignment.center,
//                           color: lightGrayColor3,
//                         ),
//                         new Container(
//                           width: 50,
//                           alignment: Alignment.centerLeft,
//                           child: Text(
//                             'Points',
//                             style: TextStyle(
//                                 color: Colors.black,
//                                 fontWeight: FontWeight.w500,
//                                 fontSize: 12),
//                           ),
//                         ),
//                         new Container(
//                           margin: EdgeInsets.only(right: 10,left: 0),
//                           width: 50,
//                           alignment: Alignment.center,
//                           child: Text(
//                             '% SEL BY',
//                             style: TextStyle(
//                                 color: Colors.grey,
//                                 fontWeight: FontWeight.w400,
//                                 fontSize: 12),
//                           ),
//                         ),
//                       ],
//                     ),
//                   ],
//                 ),
//               ),
//               new Divider(height: 1,thickness: 1,),
//               new ListView.builder(
//                   padding: EdgeInsets.only(top: 0),
//                   physics: NeverScrollableScrollPhysics(),
//                   shrinkWrap: true,
//                   scrollDirection: Axis.vertical,
//                   itemCount: widget.list.length,
//                   itemBuilder: (BuildContext context, int index) {
//                     return new GestureDetector(
//                       behavior: HitTestBehavior.translucent,
//                       child: new Column(
//                         children: [
//                           new Container(
//                             color: widget.list[index].isSelected==1?pointPlayerBg:Colors.white,
//                             child: new Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                               children: [
//                                 new Row(
//                                   children: [
//                                     new Container(
//                                       margin: EdgeInsets.only(right: 10),
//                                       height: 40,
//                                       width: 40,
//                                       child:  CachedNetworkImage(
//                                         imageUrl: widget.list[index].image!,
//                                         placeholder: (context,url)=> new Image.asset(AppImages.userAvatarIcon),
//                                         errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon),
//                                       ),
//                                     ),
//                                     new Column(
//                                       crossAxisAlignment: CrossAxisAlignment.start,
//                                       children: [
//                                         new Container(
//                                           width: 150,
//                                             child: new Text(
//                                           widget.list[index].player_name!,
//                                           overflow: TextOverflow.ellipsis,
//                                           style: TextStyle(
//                                               color: Colors.black,
//                                               fontSize: 14,
//                                               fontWeight: FontWeight.w500),
//                                         )),
//                                        new Row(
//                                          children: [
//                                            new Container(
//                                              margin: EdgeInsets.only(top: 2,right: 7),
//                                              child: new Text(
//                                                widget.list[index].short_name!=null&&widget.list[index].role!=null?widget.list[index].short_name!+'-'+widget.list[index].role!:'',
//                                                style: TextStyle(
//                                                    color: Colors.grey,
//                                                    fontSize: 12,
//                                                    fontWeight: FontWeight.w400),
//                                              ),
//                                            ),
//                                            widget.list[index].isSelected==1?new Container(
//                                              margin: EdgeInsets.only(right: 5),
//                                              height: 18,
//                                              width: 18,
//                                              child: Image(
//                                                image: AssetImage(
//                                                  AppImages.circleTickIcon,),),
//                                            ):new Container(),
//                                          ],
//                                        ),
//                                       ],
//                                     )
//                                   ],
//                                 ),
//                                 new Row(
//                                   children: [
//                                     new Container(
//                                       width: 1,
//                                       height: 60,
//                                       margin: EdgeInsets.only(right: 20),
//                                       alignment: Alignment.center,
//                                       color: lightGrayColor3,
//                                     ),
//                                     new Container(
//                                       width: 50,
//                                       alignment: Alignment.centerLeft,
//                                       child: Text(
//                                         widget.list[index].points.toString(),
//                                         style: TextStyle(
//                                             color: Colors.black,
//                                             fontWeight: FontWeight.w400,
//                                             fontSize: 12),
//                                       ),
//                                     ),
//                                     new Container(
//                                       margin: EdgeInsets.only(right: 10,left: 0),
//                                       width: 50,
//                                       alignment: Alignment.center,
//                                       child: Text(
//                                         widget.list[index].selected_by!,
//                                         style: TextStyle(
//                                             color: Colors.black,
//                                             fontWeight: FontWeight.w400,
//                                             fontSize: 12),
//                                       ),
//                                     ),
//                                   ],
//                                 ),
//                               ],
//                             ),
//                           ),
//                           new Divider(height: 1,thickness: 1,),
//                         ],
//                       ),
//                       onTap: (){
//                         navigateToBreakupPlayerPoints(context,widget.list[index]);
//                       },
//                     );
//                   }
//               ),
//             ],
//           )
//       ),
//     );
//   }
// }