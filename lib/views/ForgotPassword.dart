import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/data.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  bool _passwordVisible=false;
  bool _isMobileLogin=true;
  OutlineInputBorder border=OutlineInputBorder(

    borderSide: BorderSide(
        color: Colors.white
    ),
    borderRadius: BorderRadius.circular(5.0),

  );
  TextEditingController controller = TextEditingController();
  late String userId='0';

  @override
  void initState() {
    super.initState();
    _passwordVisible=false;
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        print("my user id is ");
        print(value);
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    // SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
    //     statusBarColor: primaryColor,
    //     /* set Status bar color in Android devices. */
    //
    //     statusBarIconBrightness: Brightness.light,
    //     /* set Status bar icons color in Android devices.*/
    //
    //     statusBarBrightness:
    //     Brightness.dark) /* set Status bar icon color in iOS. */
    // );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: whiteColor,
        body: SafeArea(
          child: Container(
            height: MediaQuery.of(context).size.height*1,
            width: MediaQuery.of(context).size.width*1,

            // decoration: new BoxDecoration(
            //   gradient: new LinearGradient(
            //     colors: [
            //       const Color(0xff190304),
            //       const Color(0xff630f12),
            //     ],
            //     begin: const FractionalOffset(0.0, 0.0),
            //     end: const FractionalOffset(0.0, 1.0),
            //   ),
            // ),
            child: SingleChildScrollView(
              child: new Column(
                children: [

                  Column(
                    children: [
                      Container(
                        //height: 500,
                        color: Colors.transparent,
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            new GestureDetector(
                              behavior: HitTestBehavior.translucent,
                              onTap: () => {Navigator.pop(context)},
                              child: new Container(
                                padding: EdgeInsets.all(15),
                                alignment: Alignment.centerLeft,
                                child: new Container(
                                  width: 16,
                                  height: 16,
                                  child: Image(
                                    image: AssetImage(AppImages.backImageURL),
                                    fit: BoxFit.fill,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ),
                            new Container(
                              child: new Text('Forgot Password',
                                  style: TextStyle(
                                      fontFamily: AppConstants.textBold,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18)),
                            ),

                          ],
                        ),
                      ),
                      Image.asset(AppImages.forgotPassword,scale: 2,height: 200),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                            child: Padding(
                              padding: const EdgeInsets.only(top: 16.0),
                              child: Row(mainAxisAlignment:MainAxisAlignment.center,
                                children: [
                                  new Text('Forgot Your Password?',
                                      style: TextStyle(
                                          fontFamily: 'roboto',
                                          color: Title_Color_1,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 24)),
                                ],
                              ),
                            )),

                        Padding(

                          padding: const EdgeInsets.only(top: 8),

                          child: new Text(' We are here to help you so please enter the email address you provided at registration.',textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: darkgray,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 14)),
                        ),

                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child:  new Container(
                            margin: EdgeInsets.only(top: 10),
                            child: new Column(crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                new Container(
                                  padding: EdgeInsets.only(left: 8,top: 20),
                                  child: Text(
                                    'Enter Email Address',
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                      fontSize: 13,
                                      fontFamily: AppConstants.textBold,
                                      color: Text_Color,
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                ),

                                Container(
                                  margin: EdgeInsets.fromLTRB(8, 4, 8, 0),
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width,
                                  //padding: new EdgeInsets.all(10.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10)),
                                    color: Colors.white,
                                    border: Border.all(color: editbgcolor),
                                  ),
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        left: 16.0, right: 8,bottom: 3),
                                    child: TextField(
                                      style: TextStyle(color: Colors.black),
                                      controller: controller,
                                      decoration: InputDecoration(

                                        border: InputBorder.none,
                                        hintText: 'Enter Email Id',
                                        hintStyle: TextStyle(
                                          color: Text_Color, fontSize: 14,
                                          fontStyle: FontStyle.normal,
                                          fontWeight: FontWeight.normal,),

                                      ),
                                    ),
                                  ),

                                ),
                                new Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 50,
                                    margin: EdgeInsets.fromLTRB(40, 30, 40, 10),
                                    child: ElevatedButton(
                                      style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))),
                                      child: Text(
                                        'NEXT',
                                        style: TextStyle(fontSize: 15,color: Colors.white),
                                      ),

                                      onPressed: () {
                                        forgotPasswordNew();
                                      },
                                    )),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }


  void forgotPasswordNew() async {
    if(controller.text.toString().isEmpty){
      Fluttertoast.showToast(
          msg: "Please enter correct email/ mobile number",
          toastLength: Toast.LENGTH_SHORT,
          timeInSecForIosWeb: 1);
      return;
    }
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(email:controller.text.toString(),user_id: userId);
    final client = ApiClient(AppRepository.dio);
    LoginResponse loginResponse = await client.forgotPasswordNew(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      if(loginResponse.result!.mobile==1){
        AppPrefrence.putString(AppConstants.FROM,'forgot');
        AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_MOBILE, controller.text.toString());
        AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_ID, loginResponse.result!.user_id.toString());
        navigateToOtpVerify(context);
        Navigator.pop(context);
      }else{
        Navigator.pop(context);
      }

    }
    Fluttertoast.showToast(
        msg: loginResponse.message!,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1);
  }
}
