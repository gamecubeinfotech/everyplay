import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:EverPlay/adapter/LiveFinishContestAdapter.dart';
import 'package:EverPlay/adapter/LiveFinishContestAdapterNew.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/contest_request.dart';
import 'package:EverPlay/repository/model/player_points_response.dart';
import 'package:EverPlay/repository/model/refresh_score_response.dart';
import 'package:EverPlay/repository/model/team_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

import '../appUtilities/app_colors.dart';
import '../appUtilities/app_constants.dart';
import '../appUtilities/app_images.dart';
import '../appUtilities/app_navigator.dart';
import '../appUtilities/date_utils.dart';
import '../customWidgets/MatchHeader.dart';
import '../dataModels/GeneralModel.dart';
import '../localStoage/AppPrefrences.dart';
import 'PlayerPoints.dart';

class LiveFinishedContestsNew extends StatefulWidget {
  GeneralModel model;

  LiveFinishedContestsNew(this.model);

  @override
  _LiveFinishedContestsNew createState() => _LiveFinishedContestsNew();
}

class _LiveFinishedContestsNew extends State<LiveFinishedContestsNew> {
  String balance = '0';
  String userId='0';
  bool loading=false;
  List<LiveFinishedContestData> contestList = [];
  int _currentIndex = 0;
  int _currentMatchIndex = 0;
  List<Widget> tabs = <Widget>[];
  List<Team> teamList = [];
  List<MultiSportsPlayerPointItem> pointsList = [];
  List<int> fantasyTypes=[];
  late TabController _tabController;
  String team1Score='0/0 (0)';
  String team2Score='0/0 (0)';
  String tvWinningText='';

  void getMatchScores() async {
    setState(() {
      loading=true;
    });
    AppLoaderProgress.showLoader(context);
    ContestRequest contestRequest = new ContestRequest(user_id: userId,matchkey: widget.model.matchKey,sport_key: widget.model.sportKey,fantasy_type: widget.model.fantasyType.toString(),slotes_id: widget.model.slotId.toString());
    final client = ApiClient(AppRepository.dio);
    RefreshScoreResponse response = await client.getMatchScores(contestRequest);
    if (response.status == 1) {
      // refreshScoreItem=response.result!;
      contestList=response.result!.contest??[];
      teamList=response.result!.teams??[];
      // LiveFinishedScoreItem scoreItem=refreshScoreItem.matchruns![0];
      // team1Score=widget.model.sportKey=='CRICKET'?scoreItem.Team1_Totalruns.toString() + "/" + scoreItem.Team1_Totalwickets.toString() + " (" + scoreItem.Team1_Totalovers.toString()+ ")":scoreItem.Team1_Totalruns.toString();
      // team2Score=widget.model.sportKey=='CRICKET'?scoreItem.Team2_Totalruns.toString() + "/" + scoreItem.Team2_Totalwickets.toString() + " (" + scoreItem.Team2_Totalovers.toString()+ ")":scoreItem.Team2_Totalruns.toString();
      // tvWinningText=scoreItem.Winning_Status??'';
      widget.model.team1Score=team1Score;
      widget.model.team2Score=team2Score;
      widget.model.winningText=tvWinningText;

    }
    setState(() {
      loading=false;
      AppLoaderProgress.hideLoader(context);
    });
  }

  @override
  void initState() {
    super.initState();
    print(widget.model);
    AppPrefrence.getString(AppConstants.KEY_USER_BALANCE).then((value) => {
          setState(() {
            balance = value;
          })
        });

    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getMatchScores();
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: Colors.white,
            // padding: EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new Row(
                  children: [
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      onTap: () => {Navigator.pop(context)},
                      child: new Container(
                        padding: EdgeInsets.fromLTRB(15, 15, 25, 15),
                        alignment: Alignment.centerLeft,
                        child: new Container(
                          width: 16,
                          height: 16,
                          child: Image(
                            image: AssetImage(AppImages.backImageURL),
                            fit: BoxFit.fill,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                    new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          width: 150,
                          child: new Text('My Contest',
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w800,
                                  fontSize: 18)),
                        ),
                      ],
                    ),
                  ],
                ),
                new Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    new GestureDetector(
                      behavior: HitTestBehavior.translucent,
                      child: new Container(
                        child: new Row(
                          children: [
                            new Text('₹' + balance,
                                style: TextStyle(
                                    fontFamily: AppConstants.textBold,
                                    color: Colors.white,
                                    fontWeight: FontWeight.normal,
                                    fontSize: 15)),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: new Container(
                                margin: EdgeInsets.only(left: 5),
                                height: 20,
                                width: 20,
                                child: Image(
                                  image: AssetImage(AppImages.walletImageURL),
                                  color: red,
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      onTap: () => {navigateToWallet(context)},
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
          body: Container(
            child: SingleChildScrollView(
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    color: Colors.white,
                    child: Column(
                      children: [
                        new Container(
                          height: 40,
                          margin: EdgeInsets.all(10),
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.black12),
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white
                          ),
                          child: new Container(
                            height: 20,
                            alignment: Alignment.center,
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                new Container(
                                  height: 20,
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          widget.model.teamVs.toString().split(' VS ')[0],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          'Vs',
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: Text(
                                          widget.model.teamVs.toString().split(' VS ')[1],
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontWeight: FontWeight.w500,
                                              fontSize: 14),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.only(right: 10),
                                  child: GestureDetector(
                                    behavior: HitTestBehavior.translucent,
                                    child: Text(
                                      widget.model.isFromLive! ?"In Progress":"Completed",
                                      style: TextStyle(
                                          color: Title_Color_1,
                                          decoration: TextDecoration.none,
                                          fontWeight: FontWeight.w700,
                                          fontSize: 12),
                                    ),
                                    onTap: () {},
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: new Container(
                              color: Colors.white,
                              width: MediaQuery.of(context).size.width,
                              height: 40,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  primary: greenColor,
                                  elevation: 0.5,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => PlayerPoints(pointsList, widget.model),
                                    ),
                                  );
                                },
                                child: Padding(
                                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                                  child: Text(
                                    'My Team Player Stats',
                                    style: TextStyle(fontSize: 15, color: Colors.white),
                                  ),
                                ),
                              )
                          ),
                        ),
                      ],
                    ),
                  ),

                  _currentMatchIndex == 0
                      ? new Container(
                    child: contestList.length>0?new ListView.builder(
                        padding: EdgeInsets.only(top: 0),
                        physics:
                        NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: contestList.length,
                        itemBuilder: (BuildContext context,
                            int index) {
                          return new LiveFinishContestAdapterNew(widget.model,contestList[index]);
                        }):new Container(
                      margin: EdgeInsets.only(top: 20),
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: Text(
                        "You haven't joined any challenge for this match.",
                        style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w400,
                            fontSize: 14),
                      ),
                    ),
                  )
                      : _currentMatchIndex == 1
                      ? new Container(
                    child: teamList.length>0?new ListView.builder(
                        padding: EdgeInsets.only(top: 0),
                        physics:
                        NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.vertical,
                        itemCount: teamList.length,
                        itemBuilder:
                            (BuildContext context,
                            int index) {
                          return new TeamItemAdapter(widget.model,teamList[index],false,index);
                        }):new Container(
                      margin: EdgeInsets.only(top: 20),
                      width: MediaQuery.of(context).size.width,
                      alignment: Alignment.center,
                      child: Text(
                        "You haven't created any team yet for this fantasy.",
                        style: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.w400,
                            fontSize: 14),
                      ),
                    ),
                  )
                      : new PlayerPoints(pointsList,widget.model)

                ],
              ),
            ),
          ),

      ),
    );
  }
}
