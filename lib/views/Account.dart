import 'dart:convert';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomToolTip.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/model/live_data_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

import 'package:EverPlay/repository/retrofit/apis.dart';


import '../repository/model/live_data_response.dart';
import '../repository/model/transactions_response.dart';

class Account extends StatefulWidget {
  LevelDataResponse levelDataResponse;
  String userId;
  Account(this.levelDataResponse,this.userId);

  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  String userPic = '';
  ImagePicker _imagePicker=new ImagePicker();
  GlobalKey btnKey = GlobalKey();
  GlobalKey btnKey1 = GlobalKey();
  GlobalKey btnKey2 = GlobalKey();
  @override
  void initState() {
    super.initState();
    print("testing" + widget.levelDataResponse.result!.playing_history!.total_winning.toString());
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_PIC).then((value) =>
    {
      setState(() {
        userPic = value;
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return new Scaffold(
      backgroundColor: whiteColor,
      body: new SingleChildScrollView(
        child: new Stack(
          children: [

            new Container(
              height: 200,
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(AppImages.levelIcon),
                      fit: BoxFit.fill)),
            ),
            new Container(
              padding: EdgeInsets.all(10),
              child: new Column(
                children: [
                  new Container(
                    child: new Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 200.0),
                          child: Text('Account'),
                        ),
                        new GestureDetector(
                            behavior: HitTestBehavior.translucent,
                            child: new Container(

                              margin: EdgeInsets.only(right: 10),
                              child: new ClipRRect(
                                borderRadius: BorderRadius.all(Radius.circular(30)),
                                child: new Container(
                                  height: 60,
                                  width: 60,
                                  child: CachedNetworkImage(
                                      imageUrl: userPic,
                                      placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                      errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                      fit: BoxFit.cover
                                  ),
                                ),
                              ),
                            ),
                            onTap:()async{
                              if (await Permission.camera.request().isGranted) {
                                if(await Permission.storage.request().isGranted){
                                  showOptionsDialog(context);
                                }
                              }
                            }
                        ),
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Text(
                              'Level '+widget.levelDataResponse.result!.currentlevel.toString(),
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w500),
                            ),
                            new Container(
                              margin: EdgeInsets.only(top: 5),
                              child: new Text(
                                'Completed!!',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 20),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Text(
                              'Level',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400),
                            ),
                            new Container(
                              margin: EdgeInsets.only(top: 5),
                              child: new Text(
                                widget.levelDataResponse.result!.currentlevel.toString(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ],
                        ),
                        new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            new Text(
                              'Level',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 14,
                                  fontWeight: FontWeight.w400),
                            ),
                            new Container(
                              margin: EdgeInsets.only(top: 5),
                              child: new Text(
                                widget.levelDataResponse.result!.nextlevel.toString(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 10),
                    child: LinearProgressIndicator(
                      backgroundColor: bgColor,
                      valueColor: AlwaysStoppedAnimation<Color>(
                        Colors.amber,
                      ),
                      value: widget.levelDataResponse.result!.progressbar,
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 10),
                    child: new Stack(
                      alignment: Alignment.topCenter,
                      children: [
                        new Container(
                          height: 19,
                          width: 15,
                          child: new Image.asset(
                            AppImages.levelArrowIcon,
                          ),
                        ),
                        new Container(
                          margin: EdgeInsets.only(top: 9),
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          child: new Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            ),
                            child: new Column(
                              children: [
                                new Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: new Text(
                                    'To progress to a level '+widget.levelDataResponse.result!.nextlevel.toString()+' Champion',
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 14,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.fromLTRB(15, 15, 15, 5),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: mediumGrayColor, width: 1),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                  child: new Column(
                                    children: [
                                      new Row(
                                        children: [
                                          new Container(
                                            margin: EdgeInsets.all(20),
                                            child: new Column(
                                              children: [
                                                new Container(
                                                  margin:
                                                  EdgeInsets.only(top: 5),
                                                  child: new Text(
                                                    '₹'+widget.levelDataResponse.result!.nextlevel_reward_cash.toString(),
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 25,
                                                        fontWeight:
                                                        FontWeight.w500),
                                                  ),
                                                ),
                                                new Text(
                                                  'Bonus',
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 14,
                                                      fontWeight:
                                                      FontWeight.w400),
                                                ),
                                              ],
                                            ),
                                          ),
                                          new Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              widget.levelDataResponse.result!.is_visible_contest_text==1?new Row(
                                                children: [
                                                  new Container(
                                                    height: 20,
                                                    width: 20,
                                                    child: new Image.asset(
                                                      AppImages.cashContestIcon,
                                                    ),
                                                  ),
                                                  new Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: new Text(
                                                      'Join',
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.w400),
                                                    ),
                                                  ),
                                                  new Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: new Text(
                                                      widget.levelDataResponse.result!.contest.toString(),
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.w400),
                                                    ),
                                                  ),
                                                  new Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: new Text(
                                                      'more cash contest',
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.w400),
                                                    ),
                                                  ),
                                                ],
                                              ):new Container(),
                                              widget.levelDataResponse.result!.is_visible_contest_text==1&&widget.levelDataResponse.result!.is_visible_cash_text==1?new Container(
                                                padding: EdgeInsets.all(5),
                                                child: new Row(
                                                  mainAxisSize:
                                                  MainAxisSize.max,
                                                  mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                                  children: [
                                                    new Container(
                                                      width: 60,
                                                      margin: EdgeInsets.only(
                                                          right: 5),
                                                      decoration: BoxDecoration(
                                                        border: Border(
                                                          bottom: BorderSide(
                                                            color:
                                                            mediumGrayColor,
                                                            width:
                                                            1.5, // This would be the width of the underline
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                    new Text(
                                                      widget.levelDataResponse.result!.type=='and'?'AND':'OR',
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          fontSize: 10),
                                                    ),
                                                    new Container(
                                                      width: 60,
                                                      margin: EdgeInsets.only(
                                                          left: 5),
                                                      decoration: BoxDecoration(
                                                        border: Border(
                                                          bottom: BorderSide(
                                                            color:
                                                            mediumGrayColor,
                                                            width:
                                                            1.5, // This would be the width of the underline
                                                          ),
                                                        ),
                                                      ),
                                                    )
                                                  ],
                                                ),
                                              ):new Container(),
                                              widget.levelDataResponse.result!.is_visible_cash_text==1?new Row(
                                                children: [
                                                  new Container(
                                                    height: 20,
                                                    width: 20,
                                                    child: new Image.asset(
                                                      AppImages.rupeeIcon,
                                                    ),
                                                  ),
                                                  new Container(
                                                    margin: EdgeInsets.only(
                                                        left: 10),
                                                    child: new Text(
                                                      'Play for',
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.w400),
                                                    ),
                                                  ),
                                                  new Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: new Text(
                                                      '₹'+widget.levelDataResponse.result!.nextlevel_cash.toString(),
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.w400),
                                                    ),
                                                  ),
                                                  new Container(
                                                    margin: EdgeInsets.only(
                                                        left: 5),
                                                    child: new Text(
                                                      'more',
                                                      style: TextStyle(
                                                          color: Colors.grey,
                                                          fontSize: 12,
                                                          fontWeight:
                                                          FontWeight.w400),
                                                    ),
                                                  ),
                                                ],
                                              ):new Container(),
                                            ],
                                          )
                                        ],
                                      ),
                                      new Container(
                                        height: 30,
                                        width:
                                        MediaQuery
                                            .of(context)
                                            .size
                                            .width,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.vertical(
                                              bottom: Radius.circular(10)),
                                          color: lightGrayColor,
                                        ),
                                        child: new Container(
                                          child: new Text(
                                            'Use this cash bonus to join paid contest and win',
                                            style: TextStyle(
                                                color: darkGrayColor,
                                                fontSize: 12,
                                                fontWeight: FontWeight.normal),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                new Container(
                                  margin: EdgeInsets.fromLTRB(15, 15, 15, 15),
                                  decoration: BoxDecoration(
                                      border: Border.all(
                                          color: mediumGrayColor, width: 1),
                                      borderRadius:
                                      BorderRadius.all(Radius.circular(5))),
                                  child: new Row(
                                    children: [
                                      new Container(
                                        margin: EdgeInsets.all(10),
                                        child: new Stack(
                                          children: [
                                            new Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                              children: [
                                                new Container(
                                                  child: new Text(
                                                    '₹'+widget.levelDataResponse.result!.currentlevel_reward_cash.toString(),
                                                    style: TextStyle(
                                                        color: mediumGrayColor,
                                                        fontSize: 25,
                                                        fontWeight:
                                                        FontWeight.w500),
                                                  ),
                                                ),
                                                new Text(
                                                  'Bonus',
                                                  style: TextStyle(
                                                      color: mediumGrayColor,
                                                      fontSize: 14,
                                                      fontWeight:
                                                      FontWeight.w500),
                                                ),
                                              ],
                                            ),
                                            new Container(
                                              height: 50,
                                              width: 70,
                                              padding: EdgeInsets.all(5),
                                              child: new Image.asset(
                                                AppImages.completedIcon,
                                                fit: BoxFit.fill,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(left: 10),
                                        child: new Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: [
                                            new Text(
                                              'Level '+widget.levelDataResponse.result!.currentlevel.toString(),
                                              style: TextStyle(
                                                  color: darkGrayColor,
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                            new Container(
                                              margin: EdgeInsets.only(top: 5),
                                              child: new Text(
                                                'Completed!!',
                                                style: TextStyle(
                                                    color: greenColor,
                                                    fontSize: 14,
                                                    fontWeight:
                                                    FontWeight.w500),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 9),
                    width: MediaQuery
                        .of(context)
                        .size
                        .width,
                    child: new Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: new Container(
                        padding: EdgeInsets.all(10),
                        child: new Column(
                          children: [
                            new Container(
                              child: new Text(
                                'Total Balance',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            new Container(
                              margin: EdgeInsets.only(top: 7),
                              child: new Text(
                                '₹'+widget.levelDataResponse.result!.balance_history!.totalamount.toString(),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            new Container(
                                width: 90,
                                height: 35,
                                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                child: ElevatedButton(
                                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(greenColor),elevation: MaterialStateProperty.all(.5) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)))),
                                  child: Text(
                                    'Add Cash',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  onPressed: () {
                                    navigateToAddBalance(context,'');
                                  },
                                )
                            ),
                            new Divider(),
                            new Container(
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: [
                                  new Container(
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment
                                          .start,
                                      children: [
                                        new Container(
                                          child: new Text(
                                            'Deposited',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 7),
                                          child: new Text(
                                            '₹'+widget.levelDataResponse.result!.balance_history!.balance.toString(),
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  new GestureDetector(behavior: HitTestBehavior.translucent,
                                    child: new Container(
                                      key: btnKey,
                                      height: 20,
                                      width: 20,
                                      child: new Image.asset(
                                        AppImages.informationIcon,
                                      ),
                                    ),
                                    onTap: (){
                                      CustomToolTip popup = CustomToolTip(context,
                                        text: "Money deposited by you that you can use to join any contest but can't withdraw",
                                        textStyle: TextStyle(color: Colors.white,fontSize: 12),
                                        height: 65,
                                        backgroundColor: Colors.black,
                                        padding: EdgeInsets.all(10.0),
                                        borderRadius: BorderRadius.circular(5.0),
                                        shadowColor: Colors.transparent,
                                      );
                                      popup.show(
                                        widgetKey: btnKey,
                                      );
                                    },),
                                ],
                              ),
                            ),
                            new Divider(),
                            new Container(
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: [
                                  new Container(
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment
                                          .start,
                                      children: [
                                        new Container(
                                          child: new Text(
                                            'Winnings',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 7),
                                          child: new Text(
                                            '₹'+widget.levelDataResponse.result!.balance_history!.winning.toString(),
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  new GestureDetector(behavior: HitTestBehavior.translucent,
                                    child: new Container(
                                      key: btnKey1,
                                      height: 20,
                                      width: 20,
                                      child: new Image.asset(
                                        AppImages.informationIcon,
                                      ),
                                    ),
                                    onTap: (){
                                      CustomToolTip popup = CustomToolTip(context,
                                        text: "Money that you can withdraw re-use to join any contests",
                                        textStyle: TextStyle(color: Colors.white,fontSize: 12),
                                        height: 50,
                                        backgroundColor: Colors.black,
                                        padding: EdgeInsets.all(10.0),
                                        borderRadius: BorderRadius.circular(5.0),
                                        shadowColor: Colors.transparent,
                                      );
                                      popup.show(
                                        widgetKey: btnKey1,
                                      );
                                    },),
                                ],
                              ),
                            ),
                            !isVerified()?new Container(
                              alignment: Alignment.centerLeft,
                              margin: EdgeInsets.only(top: 10),
                              child: new Text(
                                'Verify your account to be eligible to withdraw',
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400),
                              ),
                            ):new Container(),
                            new Container(
                                width: 130,
                                height: 35,
                                margin: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                child: ElevatedButton(
                                  style: ButtonStyle(backgroundColor: MaterialStateProperty.all(!isVerified()?greenColor:Themecolor,),elevation: MaterialStateProperty.all(.5) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)))),
                                  child: Text(
                                    !isVerified()?'Verify Now':'Withdraw',
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  onPressed: () {
                                    if(!isVerified()) {
                                      navigateToVerifyAccount(context);
                                    }else{
                                      navigateToWithdrawCash(context);
                                    }
                                  },
                                )
                            ),
                            new Divider(),
                            new Container(
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: [
                                  new Container(
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment
                                          .start,
                                      children: [
                                        new Container(
                                          child: new Text(
                                            'Bonus',
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 7),
                                          child: new Text(
                                            '₹'+widget.levelDataResponse.result!.balance_history!.bonus.toString()  ,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  new GestureDetector(behavior: HitTestBehavior.translucent,
                                    child: new Container(
                                      key: btnKey2,
                                      height: 20,
                                      width: 20,
                                      child: new Image.asset(
                                        AppImages.informationIcon,
                                      ),
                                    ),
                                    onTap: (){
                                      CustomToolTip popup = CustomToolTip(context,
                                        text: "Money that you can use to join any public contests",
                                        textStyle: TextStyle(color: Colors.white,fontSize: 12),
                                        height: 50,
                                        backgroundColor: Colors.black,
                                        padding: EdgeInsets.all(10.0),
                                        borderRadius: BorderRadius.circular(5.0),
                                        shadowColor: Colors.transparent,
                                      );
                                      popup.show(
                                        widgetKey: btnKey2,
                                      );
                                    },),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  new Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 10),
                    child: new Text(
                      'Playing History',
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 10),
                    child: new Row(
                      children: [
                        new Flexible(child: new Card(
                          child: new Container(
                              height: 80,
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(10),
                              child:new Row(
                                children: [
                                  new Container(
                                    height: 35,
                                    width: 35,
                                    padding: EdgeInsets.all(5),
                                    child: new Image.asset(
                                      AppImages.matchPlayedIcon,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(left: 15,top: 10),
                                    child: new Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        new Text(
                                          widget.levelDataResponse.result!.playing_history!.total_match_play.toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 5),
                                          child: new Text(
                                            'Match Played',
                                            style: TextStyle(
                                                color: darkGrayColor,
                                                fontSize: 12,
                                                fontWeight:
                                                FontWeight.w400),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              )
                          ),
                        ),flex: 1,),
                        new Flexible(child: new Card(
                          child: new Container(
                              height: 80,
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(10),
                              child:new Row(
                                children: [
                                  new Container(
                                    height: 35,
                                    width: 35,
                                    padding: EdgeInsets.all(5),
                                    child: new Image.asset(
                                      AppImages.contestPlayedIcon,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(left: 15,top: 10),
                                    child: new Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        new Text(
                                          widget.levelDataResponse.result!.playing_history!.total_league_play.toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 5),
                                          child: new Text(
                                            'Contests Joined',
                                            style: TextStyle(
                                                color: darkGrayColor,
                                                fontSize: 12,
                                                fontWeight:
                                                FontWeight.w400),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              )
                          ),
                        ),flex: 1,),
                      ],
                    ),
                  ),
                  new Container(
                    margin: EdgeInsets.only(top: 10,bottom: 20),
                    child: new Row(
                      children: [
                        new Flexible(child: new Card(
                          child: new Container(
                              height: 80,
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(10),
                              child:new Row(
                                children: [
                                  new Container(
                                    height: 35,
                                    width: 35,
                                    padding: EdgeInsets.all(5),
                                    child: new Image.asset(
                                      AppImages.contestWinIcon,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(left: 15,top: 10),
                                    child: new Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        new Text(
                                          widget.levelDataResponse.result!.playing_history!.total_contest_win.toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 5),
                                          child: new Text(
                                            'Contest Won',
                                            style: TextStyle(
                                                color: darkGrayColor,
                                                fontSize: 12,
                                                fontWeight:
                                                FontWeight.w400),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              )
                          ),
                        ),flex: 1,),
                        new Flexible(child: new Card(
                          child: new Container(
                              height: 80,
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(10),
                              child:new Row(
                                children: [
                                  new Container(
                                    height: 35,
                                    width: 35,
                                    padding: EdgeInsets.all(5),
                                    child: new Image.asset(
                                      AppImages.totalWinningIcon,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  new Container(
                                    margin: EdgeInsets.only(left: 15,top: 10),
                                    child: new Column(
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        new Text(
                                          '₹'+widget.levelDataResponse.result!.playing_history!.total_winning.toString(),
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 16,
                                              fontWeight: FontWeight.w500),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.only(top: 5),
                                          child: new Text(
                                            'Total Winnings',
                                            style: TextStyle(
                                                color: darkGrayColor,
                                                fontSize: 12,
                                                fontWeight:
                                                FontWeight.w400),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              )
                          ),
                        ),flex: 1,),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  void showbannerDialog(TransactionItem transactionList) {

    showDialog(
      context: context,

      builder: (BuildContext context) {
        // var height = MediaQuery.of(context).size.height*0.20;
        // var width = MediaQuery.of(context).size.width*0.90;
        return Dialog(
          insetPadding: EdgeInsets.all(10),
          child:  Container(
            height: 200 ,
            // width: width,
            margin: EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(20)),
                color: Colors.white
            ),

            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(child: Text(transactionList.transaction_type.toString(),

                      style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold,),maxLines: 2,overflow: TextOverflow.ellipsis,)),
                    InkWell(
                      onTap: (){
                        Navigator.pop(context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Icon(
                          Icons.clear_rounded, color: Colors.black,size: 20,
                        ),
                      ),
                    ),
                  ],
                ),

                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Amount :',style: TextStyle(color: Colors.grey,fontSize: 15),),
                      new Container(
                        //width: 100,
                        child: new Text(double.parse(transactionList.deduct_amount!)>0?'- ₹'+transactionList.deduct_amount!:double.parse(transactionList.deduct_amount!)==0?double.parse(transactionList.add_amount!)>0?'+ ₹'+transactionList.add_amount!:'₹'+transactionList.deduct_amount!:'₹'+transactionList.add_amount!,
                            style: TextStyle(
                                fontFamily: 'roboto',
                                color: double.parse(transactionList.deduct_amount!)>0?Colors.grey:double.parse(transactionList.deduct_amount!)==0?double.parse(transactionList.add_amount!)>0?darkGrayColor:darkGrayColor:darkGrayColor,
                                fontWeight: FontWeight.w800,
                                fontSize: 15)),
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Match :',style: TextStyle(color: Colors.grey,fontSize: 15),),
                      Flexible(

                        child: Padding(
                          padding: const EdgeInsets.only(left: 48.0),
                          child: Text(transactionList.matchname.toString(),maxLines:3,
                            style: TextStyle(color: Colors.grey,fontSize: 15,
                              overflow: TextOverflow.fade,),softWrap: true,),
                        ),
                      )
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Contest :',style: TextStyle(color: Colors.grey,fontSize: 15),),
                      Text("_",style: TextStyle(color: Colors.grey,fontSize: 15),)
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Available Balance',style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold),),
                      Text('₹'+transactionList.available.toString(),style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold),)
                    ],
                  ),
                ),
              ],
            ),

          ),
        );

      },
    );
  }
  bool isVerified(){
    return widget.levelDataResponse.result!.balance_history!.mobile_verify==1
        && widget.levelDataResponse.result!.balance_history!.email_verify==1
        && widget.levelDataResponse.result!.balance_history!.pan_verify==1
        && widget.levelDataResponse.result!.balance_history!.bank_verify==1;
  }
  Future<void> showOptionsDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Select Profile Picture",style: TextStyle(color: Colors.black,fontSize: 16),),
            content: SingleChildScrollView(
              child: ListBody(
                children: [
                  GestureDetector(
                    child: Text("Take Photo",style: TextStyle(color: Colors.black,fontSize: 16),),
                    onTap: () {
                      Navigator.pop(context);
                      _getImage(context,ImageSource.camera);
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  GestureDetector(
                    child: Text("Choose from Gallery",style: TextStyle(color: Colors.black,fontSize: 16),),
                    onTap: () {
                      Navigator.pop(context);
                      _getImage(context,ImageSource.gallery);
                    },
                  ),Padding(padding: EdgeInsets.all(10)),
                  GestureDetector(
                    child: Text("Remove Photo",style: TextStyle(color: Colors.black,fontSize: 16),),
                    onTap: () {
                      Navigator.pop(context);
                      removeProfilePhoto();
                    },
                  ),
                  Padding(padding: EdgeInsets.all(10)),
                  GestureDetector(
                    child: Text("Cancel",style: TextStyle(color: Colors.black,fontSize: 16),),
                    onTap: () {
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
  void _getImage(BuildContext context, ImageSource source) {
    _imagePicker.pickImage(source: source, imageQuality: 60).then((value) => {
      sendFile(File(value!.path))
    });
  }
  void sendFile(File file) async {
    AppLoaderProgress.showLoader(context);
    FormData formData = new FormData.fromMap({
      "user_id": widget.userId,
      'file':await MultipartFile.fromFile(file.path, filename: file.path.split('/').last,)
    });
    Dio dio=new Dio();
    dio.options.headers["Content-Type"] = "multipart/form-data";
    var response = await dio.post(AppRepository.dio.options.baseUrl+Apis.uploadProfileImage, data: formData,).catchError((e) => debugPrint(e.response.toString()));
    AppLoaderProgress.hideLoader(context);
    var jsonObject=json.decode(response.toString());
    if(jsonObject['status']==1){
      userPic=jsonObject['result'][0]['image'];
      AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_PIC, userPic);
      MethodUtils.showSuccess(context, 'Profile Photo Uploaded Successfully.');
      setState(() {});
    }else{
      MethodUtils.showError(context, 'Error in image uploading.');
    }
  }

  void removeProfilePhoto() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: widget.userId);
    final client = ApiClient(AppRepository.dio);
    GeneralResponse response = await client.removeProfilePhoto(loginRequest);
    if (response.status == 1) {
        userPic='';
        AppPrefrence.putString(AppConstants.SHARED_PREFERENCE_USER_PIC, '');
        setState(() {
          AppLoaderProgress.hideLoader(context);
        });
        MethodUtils.showSuccess(context, 'Profile Photo Removed Successfully.');
    }
  }
}
