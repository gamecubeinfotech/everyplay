import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/adapter/ContestItemAdapter.dart';
import 'package:EverPlay/adapter/TeamItemAdapter.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';
import 'package:EverPlay/customWidgets/CustomProgressIndicator.dart';
import 'package:EverPlay/customWidgets/MatchHeader.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/leaderboard_series_response.dart';
import 'package:EverPlay/repository/model/series_leaderboard_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class PromoterLeaderboard extends StatefulWidget {
  @override
  _PromoterLeaderboardState createState() => new _PromoterLeaderboardState();
}

class _PromoterLeaderboardState extends State<PromoterLeaderboard> {
  TextEditingController codeController = TextEditingController();
  ScrollController scrollController=new ScrollController();
  bool loading = false;
  int seriesId=0;
  int total_page=1;
  int currentPage=0;
  String userId='0';
  String promoterSeriesUrl = "";
  String promterSeriesName = "";
  List<SeriesItem> seriesList=[];
  List<SeriesLeaderboardDataModel> leaderboardList=[];

  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getSeries();
      })
    });
    scrollController..addListener(() {
      if (scrollController.position.pixels == scrollController.position.maxScrollExtent && total_page>currentPage) {
        currentPage=currentPage+1;
        getSeriesLeaderboard(false);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            color: primaryColor,
            // padding: EdgeInsets.only(top: 28),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                        image: AssetImage(AppImages.backImageURL),
                        fit: BoxFit.fill,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Promoter Leaderboard',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new Container(
          height: MediaQuery.of(context).size.height,
          child: new SingleChildScrollView(
            controller: scrollController,
            child: new Column(
              children: [
                new Container(
                  padding: EdgeInsets.all(0),
                  height: 58,
                  child: new DropdownButtonFormField(
                    isDense: true,
                    hint: promterSeriesName.isEmpty
                        ? Text('Select Series',
                        style: TextStyle(
                          fontSize: 14,
                          color: Colors.white,
                          fontStyle: FontStyle.normal,
                          fontWeight: FontWeight.normal,
                        ))
                        : Text(
                      promterSeriesName,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                    isExpanded: true,
                    iconDisabledColor: Colors.white,
                    iconEnabledColor: Colors.white,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 14,
                    ),
                    dropdownColor: darkPrimaryColor,
                    items: seriesList.map(
                          (SeriesItem val) {
                        return DropdownMenuItem<String>(
                          value: val.id.toString(),
                          child: Text(val.name!),
                        );
                      },
                    ).toList(),
                    onChanged: (val) {
                      setState(() {
                        seriesId = int.parse(val.toString());
                        getSeriesLeaderboard(false);
                        },
                      );
                    },
                    decoration: InputDecoration(
                      counterText: "",
                      fillColor: darkPrimaryColor,
                      filled: true,
                      hintStyle: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                        fontStyle: FontStyle.normal,
                        fontWeight: FontWeight.bold,
                      ),
                      enabledBorder: OutlineInputBorder(
                        // width: 0.0 produces a thin "hairline" border
                        borderRadius: BorderRadius.circular(0),
                        borderSide:
                        BorderSide(color: darkPrimaryColor, width: 0.0),
                      ),
                    ),
                  ),
                ),
                new Stack(
                  children: [
                    new Container(
                      height: 280,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(AppImages.winnerBgIcon),
                              fit: BoxFit.fill)),
                    ),
                    new Container(
                      child: new Column(
                        children: [
                          new Container(
                            child: new Column(
                              children: [
                                new GestureDetector(
                                  behavior: HitTestBehavior.translucent,
                                  child: new Container(
                                    height: 40,
                                    alignment: Alignment.center,
                                    margin: EdgeInsets.all(10),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(7),
                                      color: Colors.black54,
                                    ),
                                    child: new Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        new Container(
                                          margin: EdgeInsets.all(10),
                                          child: new Text('Leaderboard Prizes',
                                              style: TextStyle(
                                                  fontFamily: AppConstants.textBold,
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w500,
                                                  fontSize: 14)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  onTap: (){
                                    navigateToVisionWebView(context,getSeriesName(seriesId),promoterSeriesUrl);
                                  },
                                ),
                                !loading&&leaderboardList.length>0?new Container(
                                  height: 180,
                                  child: new Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    children: [
                                      new GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: new Container(
                                          alignment: Alignment.center,
                                          child: new Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              new Stack(
                                                alignment: Alignment.center,
                                                children: [
                                                  new Container(
                                                    height: 20,
                                                    child: new Text('2',
                                                        style: TextStyle(
                                                            color: yellowColor,
                                                            fontWeight: FontWeight.w800,
                                                            fontSize: 20)),
                                                  ),
                                                  new Container(
                                                    height: 20,
                                                    width: 35,
                                                    alignment: Alignment.topRight,
                                                    child: new Text('nd',
                                                        style: TextStyle(
                                                            color: yellowColor,
                                                            fontWeight: FontWeight.w800,
                                                            fontSize: 11)),
                                                  ),
                                                ],
                                              ),
                                              new Container(
                                                margin: EdgeInsets.only(top: 5),
                                                alignment: Alignment.center,
                                                decoration: BoxDecoration(
                                                  border: Border.all(color: primaryColor,width: 4),
                                                  borderRadius: BorderRadius.circular(60),
                                                ),
                                                child: ClipOval(
                                                  child: CachedNetworkImage(
                                                      height: 76,
                                                      width: 76,
                                                      imageUrl: leaderboardList[1].image??'',
                                                      placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                                      errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                                      fit: BoxFit.fill
                                                  ),
                                                ),
                                              ),

                                            ],
                                          ),
                                        ),
                                        onTap: (){
                                          navigateToPromoterUserStats(context,leaderboardList[1].series_id.toString(),leaderboardList[1].user_id.toString());
                                        },
                                      ),
                                      new GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: new Container(
                                          alignment: Alignment.center,
                                          child: new Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              new Stack(
                                                alignment: Alignment.topCenter,
                                                children: [
                                                  new Container(
                                                    margin: EdgeInsets.only(top: 40),
                                                    alignment: Alignment.center,
                                                    decoration: BoxDecoration(
                                                      border: Border.all(color: primaryColor,width: 4),
                                                      borderRadius: BorderRadius.circular(60),
                                                    ),
                                                    child: ClipOval(
                                                      child: CachedNetworkImage(
                                                          height: 96,
                                                          width: 96,
                                                          imageUrl: leaderboardList[0].image??'',
                                                          placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                                          errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                                          fit: BoxFit.fill
                                                      ),
                                                    ),
                                                  ),
                                                  new Container(
                                                    height: 40,
                                                    width: 40,
                                                    margin: EdgeInsets.only(top: 5),
                                                    alignment: Alignment.center,
                                                    child: Image(
                                                      image: AssetImage(
                                                        AppImages.firstWinIcon,),),
                                                  ),
                                                ],
                                              ),

                                            ],
                                          ),
                                        ),
                                        onTap: (){
                                          navigateToPromoterUserStats(context,leaderboardList[0].series_id.toString(),leaderboardList[0].user_id.toString());
                                        },
                                      ),
                                      new GestureDetector(
                                        behavior: HitTestBehavior.translucent,
                                        child: new Container(
                                          alignment: Alignment.center,
                                          child: new Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              new Stack(
                                                alignment: Alignment.center,
                                                children: [
                                                  new Container(
                                                    height: 20,
                                                    child: new Text('3',
                                                        style: TextStyle(
                                                            color: yellowColor,
                                                            fontWeight: FontWeight.w800,
                                                            fontSize: 20)),
                                                  ),
                                                  new Container(
                                                    height: 20,
                                                    width: 35,
                                                    alignment: Alignment.topRight,
                                                    child: new Text('rd',
                                                        style: TextStyle(
                                                            color: yellowColor,
                                                            fontWeight: FontWeight.w800,
                                                            fontSize: 11)),
                                                  ),
                                                ],
                                              ),
                                              new Container(
                                                margin: EdgeInsets.only(top: 5),
                                                alignment: Alignment.center,
                                                decoration: BoxDecoration(
                                                  border: Border.all(color: primaryColor,width: 4),
                                                  borderRadius: BorderRadius.circular(60),
                                                ),
                                                child: ClipOval(
                                                  child: CachedNetworkImage(
                                                      height: 76,
                                                      width: 76,
                                                      imageUrl: leaderboardList[2].image??'',
                                                      placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                                      errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                                      fit: BoxFit.fill
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        onTap: (){
                                          navigateToPromoterUserStats(context,leaderboardList[2].series_id.toString(),leaderboardList[2].user_id.toString());
                                        },
                                      ),
                                    ],
                                  ),
                                ):new Container()
                              ],
                            ),
                          ),
                          !loading&&leaderboardList.length>0?new Container(
                            child: new Column(
                              children: List.generate(
                                leaderboardList.length,
                                    (index) => new Column(
                                  children: [
                                    new GestureDetector(
                                      behavior: HitTestBehavior.translucent,
                                      child: new Card(
                                        color: leaderboardList[index].rank!.toString()=='1'?MethodUtils.hexToColor('#f0a228'):
                                        leaderboardList[index].rank!.toString()=='2'?MethodUtils.hexToColor('#e14c14'):
                                        leaderboardList[index].rank!.toString()=='3'?MethodUtils.hexToColor('#df7308'):
                                        leaderboardList[index].user_id.toString()==userId?MethodUtils.hexToColor('#fef6e1'):MethodUtils.hexToColor('#f5f5f5'),
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(10.0),
                                        ),
                                        margin: EdgeInsets.only(left: 15,right: 15,bottom: 7),
                                        child: new Container(
                                          padding: EdgeInsets.only(left: 20,right: 10,top: 5,bottom: 5),
                                          child: new Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              new Container(
                                                child: new Row(
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    new Container(
                                                      width: 70,
                                                      child: new Text(
                                                        '#'+leaderboardList[index].rank!.toString(),
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontSize: 12,
                                                            fontWeight: FontWeight.w500),
                                                      ),
                                                    ),
                                                    new Row(
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        new Container(
                                                          margin: EdgeInsets.only(right: 10),
                                                          decoration: BoxDecoration(
                                                            borderRadius: BorderRadius.circular(20),
                                                          ),
                                                          child: ClipOval(
                                                            child: CachedNetworkImage(
                                                                height: 40,
                                                                width: 40,
                                                                imageUrl: leaderboardList[index].image??'',
                                                                placeholder: (context,url)=>new Image.asset(AppImages.userAvatarIcon,),
                                                                errorWidget: (context, url, error) => new Image.asset(AppImages.userAvatarIcon,),
                                                                fit: BoxFit.fill
                                                            ),
                                                          ),
                                                        ),
                                                        new Container(
                                                          width: 110,
                                                          child: new Row(
                                                            children: [
                                                              Expanded(child: new Text(
                                                                leaderboardList[index].team!,
                                                                softWrap: false,
                                                                overflow: TextOverflow.ellipsis,
                                                                maxLines: 1,
                                                                style: TextStyle(
                                                                    color: Colors.black,
                                                                    fontSize: 12,
                                                                    fontWeight: FontWeight.w500),
                                                              ))
                                                            ],
                                                          ),
                                                        ),
                                                      ],),
                                                  ],
                                                ),
                                              ),
                                              new Container(
                                                margin: EdgeInsets.only(right: 10),
                                                child: new Text(
                                                  leaderboardList[index].points!.toString(),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 12,
                                                      fontWeight: FontWeight.w500),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                      onTap: (){
                                        navigateToPromoterUserStats(context,leaderboardList[index].series_id.toString(),leaderboardList[index].user_id.toString());
                                      },
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ):new Container()
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
  void getSeries() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest request = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    SeriesResponse response = await client.getPromoterSeries(request);
    setState(() {
      AppLoaderProgress.hideLoader(context);
    });
    if (response.status == 1&&response.result!.length>0) {
      seriesList=response.result!;
      promterSeriesName=seriesList[0].name.toString();
      seriesId=seriesList[0].id!;
      promoterSeriesUrl=response.url??''+seriesId.toString();
      getSeriesLeaderboard(true);
    }
  }

  void getSeriesLeaderboard(bool isLoading) async {
    setState(() {
      loading=isLoading;
    });
    AppLoaderProgress.showLoader(context);
    GeneralRequest request = new GeneralRequest(user_id: userId,series_id: seriesId.toString(),page: currentPage.toString());
    final client = ApiClient(AppRepository.dio);
    SeriesLeaderboardResponse response = await client.getPromoterLeaderboard(request);
    if (response.status == 1) {
      total_page=response.total_pages!;
      leaderboardList.addAll(response.result!.leaderboard!);
    }
    setState(() {
      loading=false;
      AppLoaderProgress.hideLoader(context);
    });
  }
  String getSeriesName(int id){
    for(SeriesItem seriesItem in seriesList){
      if(seriesId==id){
        return seriesItem.name!;
      }
    }
    return '';
  }
}
