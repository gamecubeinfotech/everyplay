import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/appUtilities/date_utils.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/notification_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class UserNotifications extends StatefulWidget {
  @override
  _UserNotificationsState createState() => _UserNotificationsState();
}

class _UserNotificationsState extends State<UserNotifications> {
  late String userId='0';
  List<NotificationItem> notificationList=[];
  @override
  void initState() {
    super.initState();
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID)
        .then((value) =>
    {
      setState(() {
        userId = value;
        getNotification();
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: primaryColor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.light) /* set Status bar icon color in iOS. */
    );
    return SafeArea(
      child: new Scaffold(
        backgroundColor: bgColor,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(55), // Set this height
          child: Container(
            // padding: EdgeInsets.only(top: 28),
            color: Colors.black,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                new GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: () => {Navigator.pop(context)},
                  child: new Container(
                    padding: EdgeInsets.all(15),
                    alignment: Alignment.centerLeft,
                    child: new Container(
                      width: 16,
                      height: 16,
                      child: Image(
                          image: AssetImage(AppImages.backImageURL),
                          fit: BoxFit.fill,color: Colors.white,),
                    ),
                  ),
                ),
                new Container(
                  child: new Text('Your Notifications',
                      style: TextStyle(
                          fontFamily: AppConstants.textBold,
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 18)),
                ),
              ],
            ),
          ),
        ),
        body: new SingleChildScrollView(
          child: new Stack(
            children: [
                new Column(
                  children: List.generate(
                    notificationList.length,
                        (index) => new Column(
                          children: [
                            new Container(
                              color: Colors.white,
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.only(left: 20,right: 10,top: 10,bottom: 10),
                              child: new Row(
                                children: [
                                  new Container(
                                    margin: EdgeInsets.only(right: 10),
                                    height: 30,
                                    width: 30,
                                    child: new Image.asset(AppImages.notificationIcon,
                                        fit: BoxFit.fill),
                                  ),
                                  new Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      new Container(
                                        width: MediaQuery.of(context).size.width-80,
                                        child: new Text(
                                          notificationList[index].title!,
                                          softWrap: true,
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      new Container(
                                        margin: EdgeInsets.only(top: 5),
                                        child: new Text(
                                          SuperDateFormat.getNotificationDateFormat(SuperDateFormat.getDateFormat(notificationList[index].created_at!)!),
                                          style: TextStyle(
                                              color: Colors.grey,
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                            new Divider(height: 2,)
                          ],
                        ),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
  void getNotification() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId);
    final client = ApiClient(AppRepository.dio);
    GetNotificationResponse notificationResponse = await client.getUserNotification(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if(notificationResponse.status==1){
      notificationList=notificationResponse.result!;
      if(notificationList.length>0){
        setState(() {});
      }else{
        Fluttertoast.showToast(msg: "No Notification you got yet", toastLength: Toast.LENGTH_SHORT, timeInSecForIosWeb: 1);
      }
    }

  }
}
