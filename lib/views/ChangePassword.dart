import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';
import 'package:EverPlay/appUtilities/app_images.dart';
import 'package:EverPlay/appUtilities/app_navigator.dart';
import 'package:EverPlay/customWidgets/app_circular_loader.dart';
import 'package:EverPlay/customWidgets/app_decorations.dart';
import 'package:flutter/services.dart';
import 'package:EverPlay/localStoage/AppPrefrences.dart';
import 'package:EverPlay/repository/app_repository.dart';
import 'package:EverPlay/repository/model/base_request.dart';
import 'package:EverPlay/repository/model/base_response.dart';
import 'package:EverPlay/repository/retrofit/api_client.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  bool _oldPasswordVisible=false;
  bool _newPasswordVisible=false;
  bool _confirmNewPasswordVisible=false;
  bool _isMobileLogin=true;
  TextEditingController oldController = TextEditingController();
  TextEditingController newController = TextEditingController();
  TextEditingController confirmNewController = TextEditingController();
  String from = 'profile';
  String userId= '0';
  @override
  void initState() {
    super.initState();
    _oldPasswordVisible=false;
    _newPasswordVisible=false;
    _confirmNewPasswordVisible=false;
    AppPrefrence.getString(AppConstants.FROM).then((value) =>
    {
      setState(() {
        from = value;
      })
    });
    AppPrefrence.getString(AppConstants.SHARED_PREFERENCE_USER_ID).then((value) =>
    {
      setState(() {
        userId = value;
      })
    });
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
        statusBarColor: Themecolor,
        /* set Status bar color in Android devices. */

        statusBarIconBrightness: Brightness.light,
        /* set Status bar icons color in Android devices.*/

        statusBarBrightness:
        Brightness.dark) /* set Status bar icon color in iOS. */
    );
    return Container(

      /* decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage(AppImages.main_bg),
          fit: BoxFit.fill,
        ),
      ),*/
      child: SafeArea(
        child: new Scaffold(
          backgroundColor: whiteColor,
          body: new SingleChildScrollView(
            child: new Stack(
              children: [
                // new Container(
                //   height: MediaQuery.of(context).size.height,
                //   child: new Column(
                //     children: [
                //       new Container(
                //         height: MediaQuery.of(context).size.height / 3,
                //         decoration: new BoxDecoration(
                //           gradient: new LinearGradient(
                //             colors: [
                //               const Color(0xff190304),
                //               const Color(0xff630f12),
                //             ],
                //             begin: const FractionalOffset(0.0, 0.0),
                //             end: const FractionalOffset(0.0, 1.0),
                //           ),
                //         ),
                //       ),
                //       new Container(
                //         height: MediaQuery.of(context).size.height -
                //             MediaQuery.of(context).size.height / 3,
                //         color: bgColor,
                //       )
                //     ],
                //   ),
                // ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        new GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          onTap: () => {Navigator.pop(context)},
                          child: new Container(
                            padding: EdgeInsets.all(15),
                            alignment: Alignment.centerLeft,
                            child: new Container(
                              width: 16,
                              height: 16,
                              child: Image(
                                image: AssetImage(AppImages.backImageURL),
                                fit: BoxFit.fill,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ),
                        new Container(
                          child: new Text('Reset Password',
                              style: TextStyle(
                                  fontFamily: AppConstants.textBold,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 18)),
                        ),

                      ],
                    ),
                    Image.asset(AppImages.changePassword,scale: 1.7,height: 200),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          new Container(
                              alignment: Alignment.center,
                              margin: EdgeInsets.only(left: 6,right: 10,top: 10),
                              child: new Text('Reset Your Password',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: "roboto",
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700,
                                      fontSize: 20))),
                        ],
                      ),
                    ),

                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(left: 13,right: 10),
                      child: new Text('Change password for your EverPlay Fantasy Cricket account.',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: AppConstants.textBold,
                              color: darkgray,
                              fontWeight: FontWeight.w900,
                              fontSize: 14)),
                    ),

                    SizedBox(height: 10,),

                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: new Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),),
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: new Container(
                            margin: EdgeInsets.only(top: 10),
                            child: new Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [

                                Center(
                                  child: new Container(
                                    child: new Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        from!='otp'?new Container(
                                          child: new Column(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              new Container(
                                                padding: EdgeInsets.only(left: 10),
                                                child: Text(
                                                  'Old Password',
                                                  textAlign: TextAlign.start,
                                                  style: TextStyle(
                                                    fontSize: 12,
                                                    fontFamily: AppConstants.textBold,
                                                    color: darkgray,
                                                    fontWeight: FontWeight.normal,
                                                  ),
                                                ),
                                              ),
                                              new Container(
                                                margin: EdgeInsets.fromLTRB(8, 4, 8, 0),
                                                width: MediaQuery
                                                    .of(context)
                                                    .size
                                                    .width,
                                                //padding: new EdgeInsets.all(10.0),

                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.all(
                                                      Radius.circular(10)),
                                                  color: TextFieldColor,

                                                ),

                                                child: TextField(

                                                  obscureText: !_oldPasswordVisible,
                                                  controller: oldController,
                                                  //obscureText: !_passwordVisible,
                                                  style: TextStyle(color: Colors.black),
                                                  textAlign: TextAlign.left,
                                                  textAlignVertical: TextAlignVertical.center,
                                                  // controller: passwordController,
                                                  decoration: InputDecoration(
                                                    contentPadding: EdgeInsets.only(left: 10,right: 10),
                                                    border: InputBorder.none,
                                                    hintText: 'Old Password',
                                                    hintStyle: TextStyle(color: Colors.grey),
                                                    suffixIcon: IconButton(
                                                      icon: Icon(
                                                        // Based on passwordVisible state choose the icon
                                                        _oldPasswordVisible
                                                            ? Icons.visibility
                                                            : Icons.visibility_off,
                                                        color: _oldPasswordVisible?yellowdark:textGrey,
                                                      ),
                                                      onPressed: () {
                                                        // Update the state i.e. toogle the state of passwordVisible variable
                                                        setState(() {
                                                          _oldPasswordVisible =
                                                          !_oldPasswordVisible;
                                                        });
                                                      },
                                                    ),
                                                  ),
                                                ),
                                              ),


                                              // new Container(
                                              //   padding: EdgeInsets.all(10),
                                              //   child: TextField(
                                              //     obscureText: !_oldPasswordVisible,
                                              //     controller: oldController,
                                              //     decoration: InputDecoration(
                                              //       //fillColor: textFieldBgColor,
                                              //       filled: true,
                                              //       hintStyle: TextStyle(
                                              //         fontSize: 12,
                                              //         color: Colors.grey,
                                              //         fontStyle: FontStyle.normal,
                                              //         fontWeight: FontWeight.bold,
                                              //       ),
                                              //       hintText: 'Old Password',
                                              //       border: OutlineInputBorder(
                                              //         borderSide: BorderSide(
                                              //           width: 0,color: Colors.white,
                                              //           style: BorderStyle.none,
                                              //         ),
                                              //         borderRadius:
                                              //         BorderRadius.circular(15.0),
                                              //       ),
                                              //       suffixIcon: IconButton(
                                              //         icon: Icon(
                                              //           // Based on passwordVisible state choose the icon
                                              //           _oldPasswordVisible
                                              //               ? Icons.visibility
                                              //               : Icons.visibility_off,
                                              //           color: Colors.grey,
                                              //         ),
                                              //         onPressed: () {
                                              //           // Update the state i.e. toogle the state of passwordVisible variable
                                              //           setState(() {
                                              //             _oldPasswordVisible =
                                              //             !_oldPasswordVisible;
                                              //           });
                                              //         },
                                              //       ),
                                              //     ),
                                              //   ),
                                              // ),
                                            ],
                                          ),
                                        ):new Container(),
                                        // new Container(
                                        //   padding: EdgeInsets.only(left: 10),
                                        //   child: Text(
                                        //     'New Password',
                                        //     textAlign: TextAlign.start,
                                        //     style: TextStyle(
                                        //       fontSize: 12,
                                        //       color: Colors.grey,
                                        //       fontStyle: FontStyle.normal,
                                        //       fontWeight: FontWeight.normal,
                                        //     ),
                                        //   ),
                                        // ),
                                        // new Container(
                                        //   padding: EdgeInsets.all(10),
                                        //   child: TextField(
                                        //     obscureText: !_newPasswordVisible,
                                        //     controller: newController,
                                        //     decoration: InputDecoration(
                                        //       //fillColor: textFieldBgColor,
                                        //       filled: true,
                                        //       hintStyle: TextStyle(
                                        //         fontSize: 12,
                                        //         color: Colors.grey,
                                        //         fontStyle: FontStyle.normal,
                                        //         fontWeight: FontWeight.bold,
                                        //       ),
                                        //       hintText: 'New Password',
                                        //       border: OutlineInputBorder(
                                        //         borderSide: BorderSide(
                                        //           width: 0,
                                        //           style: BorderStyle.none,color: Colors.white
                                        //         ),
                                        //         borderRadius:
                                        //         BorderRadius.circular(10.0),
                                        //       ),
                                        //       suffixIcon: IconButton(
                                        //         icon: Icon(
                                        //           // Based on passwordVisible state choose the icon
                                        //           _newPasswordVisible
                                        //               ? Icons.visibility
                                        //               : Icons.visibility_off,
                                        //           color: Colors.grey,
                                        //         ),
                                        //         onPressed: () {
                                        //           // Update the state i.e. toogle the state of passwordVisible variable
                                        //           setState(() {
                                        //             _newPasswordVisible =
                                        //             !_newPasswordVisible;
                                        //           });
                                        //         },
                                        //       ),
                                        //     ),
                                        //   ),
                                        // ),

                                        new Container(
                                          padding: EdgeInsets.only(left: 14,top: 15,),

                                          child: Text(
                                            'New Password',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: AppConstants.textBold,
                                              color: darkgray,
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                        ),
                                        new Container(
                                          margin: EdgeInsets.fromLTRB(8, 4, 8, 0),
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,

                                          //padding: new EdgeInsets.all(10.0),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                            color: Colors.white,
                                            border: Border.all(color: editbgcolor),
                                          ),
                                          child: TextField(
                                            obscureText: !_newPasswordVisible,
                                            controller: newController,
                                            //obscureText: !_passwordVisible,
                                            style: TextStyle(color: Colors.black),
                                            textAlign: TextAlign.left,
                                            textAlignVertical: TextAlignVertical.center,
                                            // controller: passwordController,
                                            decoration: InputDecoration(
                                              contentPadding: EdgeInsets.only(left: 10,right: 10),
                                              border: InputBorder.none,
                                              hintText: 'New Password',
                                              hintStyle: TextStyle(color: Colors.grey),
                                              suffixIcon: IconButton(
                                                icon: Icon(
                                                  // Based on passwordVisible state choose the icon
                                                  _newPasswordVisible
                                                      ? Icons.visibility
                                                      : Icons.visibility_off,
                                                  color: _newPasswordVisible?yellowdark:textGrey,
                                                ),
                                                onPressed: () {
                                                  // Update the state i.e. toogle the state of passwordVisible variable
                                                  setState(() {
                                                    _newPasswordVisible =
                                                    !_newPasswordVisible;
                                                  });
                                                },
                                              ),
                                            ),
                                          ),
                                        ),

                                        new Container(
                                          padding: EdgeInsets.only(left: 14,top: 15,),
                                          child: Text(
                                            'Confirm New Password',
                                            textAlign: TextAlign.start,
                                            style: TextStyle(
                                              fontSize: 12,
                                              fontFamily: AppConstants.textBold,
                                              color: darkgray,
                                              fontWeight: FontWeight.normal,
                                            ),
                                          ),
                                        ),

                                        new Container(
                                          margin: EdgeInsets.fromLTRB(8, 4, 8, 0),
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,
                                          //padding: new EdgeInsets.all(10.0),
                                          decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10)),
                                            color: Colors.white,
                                            border: Border.all(color: editbgcolor),
                                          ),
                                          child: TextField(
                                            obscureText: !_confirmNewPasswordVisible,
                                            controller: confirmNewController,
                                            //obscureText: !_passwordVisible,
                                            style: TextStyle(color: Colors.black),
                                            // controller: passwordController,
                                            textAlign: TextAlign.left,
                                            textAlignVertical: TextAlignVertical.center,
                                            // controller: passwordController,
                                            decoration: InputDecoration(
                                              contentPadding: EdgeInsets.only(left: 10,right: 10),
                                              border: InputBorder.none,
                                              hintText: 'Confirm New Password',
                                              hintStyle: TextStyle(color: Colors.grey),
                                              suffixIcon: IconButton(
                                                icon: Icon(
                                                  // Based on passwordVisible state choose the icon
                                                  _confirmNewPasswordVisible
                                                      ? Icons.visibility
                                                      : Icons.visibility_off,
                                                  color: _confirmNewPasswordVisible?yellowColor:textGrey,
                                                ),
                                                onPressed: () {
                                                  // Update the state i.e. toogle the state of passwordVisible variable
                                                  setState(() {
                                                    _confirmNewPasswordVisible =
                                                    !_confirmNewPasswordVisible;
                                                  });
                                                },
                                              ),
                                            ),
                                          ),
                                        ),

                                        new Container(
                                            width: MediaQuery.of(context).size.width,
                                            height: 50,
                                            margin: EdgeInsets.fromLTRB(10, 30, 10, 10),
                                            child: ElevatedButton(
                                              style: ButtonStyle(backgroundColor: MaterialStateProperty.all(Themecolor),elevation: MaterialStateProperty.all(.5) ,shape: MaterialStateProperty.all(RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)))),
                                              child: Text(
                                                'SAVE PASSWORD',
                                                style: TextStyle(fontSize: 15,color: Colors.white),
                                              ),
                                              onPressed: () {
                                                if(from=='otp'){
                                                  if(newController.text.isEmpty||newController.text.length<4){
                                                    Fluttertoast.showToast(
                                                        msg: 'Please enter valid new password',
                                                        toastLength: Toast.LENGTH_SHORT,
                                                        timeInSecForIosWeb: 1);
                                                    return;
                                                  }else if(confirmNewController.text.isEmpty||confirmNewController.text.length<4){
                                                    Fluttertoast.showToast(
                                                        msg: 'Please enter valid confirm password',
                                                        toastLength: Toast.LENGTH_SHORT,
                                                        timeInSecForIosWeb: 1);
                                                    return;
                                                  }else if(newController.text.toString()!=confirmNewController.text.toString()){
                                                    Fluttertoast.showToast(
                                                        msg: 'Your new password and confirm password must be same',
                                                        toastLength: Toast.LENGTH_SHORT,
                                                        timeInSecForIosWeb: 1);
                                                    return;
                                                  }
                                                  changePasswordAfterForgot();
                                                }else{
                                                  if(oldController.text.isEmpty||oldController.text.length<4){
                                                    Fluttertoast.showToast(
                                                        msg: 'Please enter valid old password',
                                                        toastLength: Toast.LENGTH_SHORT,
                                                        timeInSecForIosWeb: 1);
                                                    return;
                                                  }else if(newController.text.isEmpty||newController.text.length<4){
                                                    Fluttertoast.showToast(
                                                        msg: 'Please enter valid new password',
                                                        toastLength: Toast.LENGTH_SHORT,
                                                        timeInSecForIosWeb: 1);
                                                    return;
                                                  }else if(confirmNewController.text.isEmpty||confirmNewController.text.length<4){
                                                    Fluttertoast.showToast(
                                                        msg: 'Please enter valid confirm password',
                                                        toastLength: Toast.LENGTH_SHORT,
                                                        timeInSecForIosWeb: 1);
                                                    return;
                                                  }else if(newController.text.toString()!=confirmNewController.text.toString()){
                                                    Fluttertoast.showToast(
                                                        msg: 'Your new password and confirm password must be same',
                                                        toastLength: Toast.LENGTH_SHORT,
                                                        timeInSecForIosWeb: 1);
                                                    return;
                                                  }
                                                  changePassword();
                                                }
                                              },
                                            )),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
  void changePassword() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id:userId,oldpassword: oldController.text.toString(),newpassword:newController.text.toString(),fcmToken: '');
    final client = ApiClient(AppRepository.dio);
    GeneralResponse loginResponse = await client.changePassword(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      Navigator.pop(context);
    }
    Fluttertoast.showToast(
        msg: loginResponse.message!,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1);
  }
  void changePasswordAfterForgot() async {
    AppLoaderProgress.showLoader(context);
    GeneralRequest loginRequest = new GeneralRequest(user_id: userId,newpassword:newController.text.toString(),fcmToken: '');
    final client = ApiClient(AppRepository.dio);
    GeneralResponse loginResponse = await client.changePasswordAfterForgot(loginRequest);
    AppLoaderProgress.hideLoader(context);
    if (loginResponse.status == 1) {
      Navigator.pop(context);
    }
    Fluttertoast.showToast(
        msg: loginResponse.message!,
        toastLength: Toast.LENGTH_SHORT,
        timeInSecForIosWeb: 1);
  }
}
