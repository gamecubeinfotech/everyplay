import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_constants.dart';

class AppDecoration {
  static getInputDecorationPlain(String labelHint, {Widget? sufixIcon}) =>
      sufixIcon == null
          ? InputDecoration(
              labelText: labelHint,
              filled: true,
              fillColor: Colors.white,
              focusColor: Colors.white,
              hoverColor: Colors.white,
              contentPadding: EdgeInsets.fromLTRB(2.0, 6.0, 2.0, 6.0),
            )
          : InputDecoration(
              labelText: labelHint,
              filled: true,
              suffixIcon: sufixIcon,
              fillColor: Colors.white,
              focusColor: Colors.white,
              hoverColor: Colors.white,
              contentPadding: EdgeInsets.fromLTRB(2.0, 6.0, 0, 6.0),
            );

  static getInputDecoration(String labelHint,
          {Widget? sufixIcon, double borderRadius = 0}) =>
      sufixIcon == null
          ? InputDecoration(
              labelText: labelHint,
              alignLabelWithHint: true,
              filled: true,
              fillColor: Colors.white,
              focusColor: Colors.white,
              hoverColor: Colors.white,
              contentPadding: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
              border: borderRadius < 0
                  ? OutlineInputBorder()
                  : OutlineInputBorder(
                      borderRadius: BorderRadius.circular(borderRadius)),
              focusedBorder:
                  const OutlineInputBorder(borderSide: BorderSide(width: 1.2)))
          : InputDecoration(
              labelText: labelHint,
              suffixIcon: sufixIcon,
              alignLabelWithHint: true,
              filled: true,
              fillColor: Colors.white,
              focusColor: Colors.white,
              hoverColor: Colors.white,
              contentPadding: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 12.0),
              border: const OutlineInputBorder(),
              focusedBorder:
                  const OutlineInputBorder(borderSide: BorderSide(width: 1.2)));

  static getOutLineBoxDecoration([
    Color? color,
  ]) =>
      BoxDecoration(
          border: Border.all(
        color: color == null ? Colors.black87 : color,
      ));

  static getBoxGradientDecoration() => BoxDecoration(
      // Box decoration takes a gradient
      gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Colors.green, Colors.white]));

  static getBoxRoundDecoration({Color bColor = Colors.black45}) =>
      BoxDecoration(
          border: Border.all(width: 1, color: bColor),
          borderRadius: BorderRadius.circular(4),
          color: Colors.white);

  static getRoundRectangleDecoration({double radius = 4}) =>
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(radius));

  static getRoundRectangleDecorationColor({required Color borderColor}) =>
      RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
          side: BorderSide(color: borderColor));
}

class AppStyles {
  static getTextStyleColor(bool isSemiBold, double fontSize,
          {Color textColor = Colors.black,
          FontStyle fontStyle = FontStyle.normal}) =>
      TextStyle(
        fontFamily: AppConstants.textRegular,
        color: textColor,
        fontWeight: isSemiBold ? FontWeight.w600 : FontWeight.normal,
        fontStyle: fontStyle,
        fontSize: fontSize,
      );

  static getTextStrikeThrough(bool isSemiBold, double fontSize,
          {Color? textColor}) =>
      TextStyle(
          fontFamily:
              isSemiBold ? AppConstants.textSemiBold : AppConstants.textRegular,
          color: textColor,
          fontWeight: isSemiBold ? FontWeight.w600 : FontWeight.normal,
          fontSize: fontSize,
          decoration: TextDecoration.lineThrough);

  static getTextStyle(bool isSemiBold, double fontSize) =>
      getTextStyleColor(isSemiBold, fontSize, textColor: Colors.black);

  static getTextStyleGreen(bool isSemiBold, double fontSize) =>
      getTextStyleColor(isSemiBold, fontSize, textColor: Colors.green);

  static getTextStyleRed(bool isSemiBold, double fontSize) =>
      getTextStyleColor(isSemiBold, fontSize, textColor: Colors.redAccent);

  static getTextStyleWhite(bool isSemiBold, double fontSize) =>
      getTextStyleColor(isSemiBold, fontSize, textColor: Colors.white);

  static getTextStyleDefaultColor(bool isSemiBold, double fontSize) =>
      getTextStyleColor(isSemiBold, fontSize);
}
