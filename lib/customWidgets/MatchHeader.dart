import 'package:custom_timer/custom_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:EverPlay/appUtilities/app_colors.dart';
import 'package:EverPlay/appUtilities/date_utils.dart';
import 'package:EverPlay/appUtilities/method_utils.dart';

class MatchHeader extends StatefulWidget {
  String teamVs;
  String headerText;
  bool isFromLive;
  bool isFromLiveFinish;

  MatchHeader(this.teamVs, this.headerText, this.isFromLive, this.isFromLiveFinish);

  @override
  _MatchHeaderState createState() => new _MatchHeaderState();
}

class _MatchHeaderState extends State<MatchHeader> {
CustomTimerController controller=CustomTimerController();

  @override
  void initState() {
    super.initState();

  }


  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Center(
        child: new Container(
          height: 40,
          margin: EdgeInsets.all(12),
          width: MediaQuery
              .of(context)
              .size
              .width,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black12),
              borderRadius: BorderRadius.circular(5)
          ),
          child: new Container(
            height: 20,

            alignment: Alignment.center,
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new Container(
                  height: 20,
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          widget.teamVs.split(' VS ')[0],
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 14),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          'Vs',
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight: FontWeight.w500,
                              fontSize: 14),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Text(
                          widget.teamVs.split(' VS ')[1],
                          style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontSize: 14),
                        ),
                      )
                    ],
                  ),
                ),
                new Container(
                  margin: EdgeInsets.only(right: 10),
                  child: !widget.isFromLiveFinish ? CustomTimer(
                     from: Duration(milliseconds: SuperDateFormat.getFormattedDateObj(widget.headerText)!.millisecondsSinceEpoch - MethodUtils.getDateTime().millisecondsSinceEpoch),
                    // from: Duration(milliseconds: 20000),
                    to: Duration(milliseconds: 0),
                    controller: controller,
                    onBuildAction: CustomTimerAction.auto_start,
                    builder: (CustomTimerRemainingTime remaining) {
                      if(remaining.minutes=='00'&& remaining.seconds=='-1')
                      {
                        controller.pause();

                      }
                      return new GestureDetector(
                        behavior: HitTestBehavior.translucent,
                        child: Text(
                          int.parse(remaining.days)>=1? int.parse(remaining.days)>1?"${remaining.days} Days":"${remaining.days} Day"+" Left":
                          int.parse(remaining.hours)>=1?("${remaining.hours}h : ${remaining.minutes}m"+" Left"):
                          "${remaining.minutes}m :${remaining.seconds=='-1'?"00":remaining.seconds}s"+" Left",

                          style: TextStyle(
                              color: Title_Color_1,
                              decoration: TextDecoration.none,
                              fontWeight:
                              FontWeight.w500,
                              fontSize: 12),
                        ),
                        onTap: () {},
                      );
                    },
                  ) : Text(
                    widget.isFromLive ?"In Progress":"Completed",
                    style: TextStyle(
                        fontFamily: 'noway',
                        color: Title_Color_1,
                        decoration: TextDecoration.none,
                        fontWeight:
                        FontWeight.w500,
                        fontSize: 12),
                  ),
                ),
              ],
            ),
          ),

        )
    );
  }
}