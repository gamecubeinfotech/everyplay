import UIKit
import Flutter
import Firebase
import FirebaseMessaging
import UserNotifications
import SabPaisa_IOS_Sdk
@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    var firebaseToken : String = "";
    
    var initUrl="https://securepay.sabpaisa.in/SabPaisa/sabPaisaInit?v=1"
    var baseUrl="https://securepay.sabpaisa.in"
    var transactionEnqUrl="https://txnenquiry.sabpaisa.in"

    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
      
      let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
              let batteryChannel = FlutterMethodChannel(name: "com.everplay/sabpaisa",
                                                        binaryMessenger: controller.binaryMessenger)
             
                        batteryChannel.setMethodCallHandler({
                            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
                        
                        // Note: this method is invoked on the UI thread.
                        guard call.method == "callSabPaisaSdk" else {
                         result(FlutterMethodNotImplemented)
                         return
                        }
                            if let dict = call.arguments as? [String] {
                                print("hello swift")
                                self?.openSdk(dict: dict,result1: result)
                            }
                           
                    })

      
    Messaging.messaging().delegate = self;
    if FirebaseApp.app() == nil {
        FirebaseApp.configure()
    }
    GeneratedPluginRegistrant.register(with: self)
    Messaging.messaging().isAutoInitEnabled = true;
    self.registerForFirebaseNotification(application: application);
    return super.application(application, didFinishLaunchingWithOptions: launchOptions);
   // GeneratedPluginRegistrant.register(with: self)
    
  }
    
    private func openSdk(dict:[String],result1: @escaping FlutterResult) {
           
           
           let bundle = Bundle(identifier: "com.eywa.ios.SabPaisa-IOS-Sdk")
           let storyboard = UIStoryboard(name: "Storyboard", bundle: bundle)
           
           
           let vc = storyboard.instantiateViewController(withIdentifier: "InitialLoadViewController_Identifier") as! InitialLoadViewController
        vc.sdkInitModel=SdkInitModel(firstName: dict[0], lastName: dict[1], secKey: dict[8], secInivialVector: dict[7], transUserName: dict[9], transUserPassword: dict[10], clientCode: dict[6], amount: Float(dict[4])!,emailAddress: dict[2],mobileNumber: dict[3],isProd: true,baseUrl: baseUrl, initiUrl: initUrl,transactionEnquiryUrl: transactionEnqUrl,transactionId:dict[5])
           
           
           vc.callback =  { (response:TransactionResponse)  in
               print("---------------Final Response To USER------------")
               vc.dismiss(animated: true)
               
               result1([response.status,response.clientTxnId])
           }
           if let topController = UIApplication.shared.keyWindow?.rootViewController {
               topController.present(vc, animated: true, completion: nil)
           }
           
       }

    
   override func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken : Data){
    print("X__APNS: \(String(describing: deviceToken))")
       Messaging.messaging().apnsToken = deviceToken;
    //Messaging.messaging().setAPNSToken(deviceToken, type:MessagingAPNSTokenType.prod )
    }
    
    override func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("X_ERR",error);
    }
    
     func registerForFirebaseNotification(application : UIApplication){
    //    Messaging.messaging().delegate     = self;
        if #available(iOS 10.0, *) {
            //UNUserNotificationCenter.current().delegate = self ;
            let authOpt : UNAuthorizationOptions = [.alert, .badge, .sound];
            UNUserNotificationCenter.current().requestAuthorization(options: authOpt, completionHandler: {_, _ in})
            UNUserNotificationCenter.current().delegate = self ;
        }else{
            let settings : UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings);
        }
        application.registerForRemoteNotifications();
    }
}

extension AppDelegate : MessagingDelegate{
    func messaging(_messagin : Messaging, didRecieveRegistrationToken fcmToken : String){
        self.firebaseToken = fcmToken;
        print("X__FCM: \(String(describing: fcmToken))")
    }
    func messaging(_messagin : Messaging, didRecieve remoteMessage : Any){
        //self.firebaseToken = fcmToken;
        print("REMOTE__MSG: \(remoteMessage)")
    }
    func application(_ application : UIApplication,didRecieveRemoteNotification uinfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void){
        print("WITH__APN: \(uinfo)")
    }
}
